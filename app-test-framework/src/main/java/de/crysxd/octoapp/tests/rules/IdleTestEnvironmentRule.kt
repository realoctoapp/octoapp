package de.crysxd.octoapp.tests.rules

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.disconnectPrinter
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.setVirtualPrinterEnabled
import io.github.aakira.napier.Napier
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import kotlin.time.Duration.Companion.seconds

class IdleTestEnvironmentRule(private vararg val envs: PrinterConfigurationV3) : TestRule {

    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            makeIdle()
            base.evaluate()
        }
    }

    fun makeIdle() {
        val timeout = 15.seconds

        try {
            runBlocking {
                withTimeout(timeout) {
                    envs.map {
                        it to BaseInjector.get().octoPrintProvider().createAdHocPrinter(it, logTag = "IdleTestEnvironmentRule")
                    }.forEach { (info, octoprint) ->
                        //region Safe mode check
                        Napier.i("Ensuring ${info.webUrl} is not in safe mode")
                        val safeMode = octoprint.systemApi.getSystemInfo().safeMode
                        require(!safeMode) { "${info.webUrl} is in safe mode, cannot proceed with test!" }
                        //endregion
                        //region Disconnect printer
                        Napier.i("Making ${info.webUrl} idle")
                        info.disconnectPrinter()
                        //endregion
                        //region Turn virtual printer on
                        Napier.i("Turning virtual printer on for ${info.webUrl}")
                        info.setVirtualPrinterEnabled(true)
                        //endregion
                    }
                }
            }
        } catch (e: TimeoutCancellationException) {
            Napier.e("Failed to idle ${envs.map { it.webUrl }} within $timeout")
        } catch (e: Exception) {
            throw java.lang.IllegalStateException("Failed to idle environments: ${envs.joinToString { it.webUrl.toString() }} (${e::class.simpleName}: ${e.message}", e)
        }
    }
}