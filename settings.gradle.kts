enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://jitpack.io")
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        maven("https://jitpack.io")
    }
}

rootProject.name = "OctoApp"
include(":feature-sign-in")
include(":base")
include(":app-phone")
include(":feature-help")
include(":base-ui")
include(":feature-plugin-support")
include(":app-wear")
include(":app-benchmark-wear")
include(":app-benchmark-phone")
include(":app-test-framework")
include(":shared-engines")
include(":shared-common")
include(":shared-base")
include(":shared-external-apis")
include(":shared-menus")
include(":app-ios-base")
include(":shared-viewmodels")
