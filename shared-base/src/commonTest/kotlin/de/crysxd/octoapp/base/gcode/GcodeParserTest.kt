package de.crysxd.octoapp.base.gcode

import de.crysxd.octoapp.base.models.GcodeLayer
import io.ktor.utils.io.ByteReadChannel
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class GcodeParserTest {

    @Test
    fun WHEN_parsing_CR_LF_file_THEN_position_is_correctly_counted() = runBlocking {
        //region GIVEN
        val progressUpdates = mutableListOf<Float>()
        val layers = mutableListOf<GcodeLayer>()
        val crLfGcode = gcode.replace("\n", "\r\n")
        val target = GcodeParser(
            content = ByteReadChannel(crLfGcode.encodeToByteArray()),
            progressUpdate = { progress -> progressUpdates += progress },
            layerSink = { layer -> layers += layer; layer },
            totalSize = crLfGcode.length.toLong()
        )
        //endregion
        //region WHEN
        target.parseFile()
        //endregion
        //region THEN
        assertEquals(
            expected = 728,
            actual = layers[0].moves.minOf { it.value.first.firstOrNull()?.positionInFile ?: Int.MAX_VALUE },
            message = "Expected first move to be at position 885, honoring \\r"
        )
        assertEquals(
            expected = 48,
            actual = layers.flatMap { l -> l.moves.map { it.value.first.size } }.sum(),
            message = "Expected all moves to be found"
        )
        assertEquals(
            expected = 6,
            actual = layers.size,
            message = "Expected all layers to be found"
        )
        //endregion
    }

    @Test
    fun WHEN_parsing_LF_file_THEN_position_is_correctly_counted() = runBlocking {
        //region GIVEN
        val progressUpdates = mutableListOf<Float>()
        val layers = mutableListOf<GcodeLayer>()
        val target = GcodeParser(
            content = ByteReadChannel(gcode.encodeToByteArray()),
            progressUpdate = { progress -> progressUpdates += progress },
            layerSink = { layer -> layers += layer; layer },
            totalSize = gcode.length.toLong()
        )
        //endregion
        //region WHEN
        target.parseFile()
        //endregion
        //region THEN
        assertEquals(
            expected = 702,
            actual = layers[0].moves.minOf { it.value.first.firstOrNull()?.positionInFile ?: Int.MAX_VALUE },
            message = "Expected first move to be at position 851"
        )
        assertEquals(
            expected = 48,
            actual = layers.flatMap { l -> l.moves.map { it.value.first.size } }.sum(),
            message = "Expected all moves to be found"
        )
        assertEquals(
            expected = 6,
            actual = layers.size,
            message = "Expected all layers to be found"
        )
        //endregion
    }

    @Test
    fun WHEN_parsing_with_z_detection_THEN_layers_is_correctly_counted() = runBlocking {
        //region GIVEN
        val progressUpdates = mutableListOf<Float>()
        val layers = mutableListOf<GcodeLayer>()
        val target = GcodeParser(
            content = ByteReadChannel(gcode.encodeToByteArray()),
            progressUpdate = { progress -> progressUpdates += progress },
            layerSink = { layer -> layers += layer; layer },
            totalSize = gcode.length.toLong(),
            detectLayersFromZ = true
        )
        //endregion
        //region WHEN
        target.parseFile()
        //endregion
        //region THEN
        // Skips 2 moves because they do not have extrusion
        assertEquals(
            expected = 851,
            actual = layers[0].moves.minOf { it.value.first.firstOrNull()?.positionInFile ?: Int.MAX_VALUE },
            message = "Expected first move to be at position 851"
        )
        assertEquals(
            expected = 46,
            actual = layers.flatMap { l -> l.moves.map { it.value.first.size } }.sum(),
            message = "Expected all moves to be found"
        )
        assertEquals(
            expected = 4,
            actual = layers.size,
            message = "Expected all layers to be found"
        )
        //endregion
    }

    private val gcode = """
        ;FLAVOR:Marlin
        ;TIME:19428
        ;Filament used: 16.6476m
        ;Layer height: 0.2
        ;MINX:10.147
        ;MINY:11.05
        ;MINZ:0.2
        ;MAXX:223.95
        ;MAXY:223.95
        ;MAXZ:1
        ;Generated with Cura_SteamEngine main
        M82 ;absolute extrusion mode
        M201 X500.00 Y500.00 Z100.00 E5000.00 ;Setup machine max acceleration
        M203 X500.00 Y500.00 Z10.00 E50.00 ;Setup machine max feedrate
        M204 P500.00 R1000.00 T500.00 ;Setup Print/Retract/Travel acceleration
        M205 X8.00 Y8.00 Z0.40 E5.00 ;Setup Jerk
        M220 S100 ;Reset Feedrate
        M221 S100 ;Reset Flowrate

        M140 S60 ; start heating the bed to what is set in Cura
        M104 S200 &#65279;T0 ; start heating hotend to what is set in Cura and WAIT

        G28 ;Home
        M420 S1 ; Enable bed leveling

        G92 E0 ;Reset Extruder
        G1 Z2.0 F3000 ;Move Z Axis up

        M109 S200 &#65279;T0 ; wait for hotend
        M190 S60 ; wait for bed

        G29

        G1 X70 Y15 Z0.28 F5000.0 ;Move to start position
        G1 X180 Y15.0 Z0.28 F1500.0 E15 ;Draw the first line
        G1 X180 Y15.4 Z0.28 F5000.0 ;Move to side a little
        G1 X70 Y15.4 Z0.28 F1500.0 E30 ;Draw the second line
        G92 E0 ;Reset Extruder
        G1 Z2.0 F3000 ;Move Z Axis up
        G92 E0
        G92 E0
        G1 F600 E-2.5
        ;LAYER_COUNT:5
        ;LAYER:0
        ;LAYER_CHANGE   <- shouldn't be picked up
        ;LAYER_CHANGE   <- shouldn't be picked up
        ;LAYER_CHANGE   <- shouldn't be picked up
        ;LAYER_CHANGE   <- shouldn't be picked up
        M107
        @Object (Unsaved).stl(96)
        G0 F6000 X11.75 Y11.75 Z0.2
        ;TYPE:WALL-INNER
        G1 F600 E0
        G1 F1500 X11.75 Y20.8 E0.37626
        G1 X11.75 Y29.85 E0.75251
        G1 X29.85 Y29.85 E1.50502
        G1 X29.85 Y11.75 E2.25753
        G1 X11.75 Y11.75 E3.01004
        G0 F6000 X11.25 Y11.25
        ;TYPE:WALL-OUTER
        G1 F1500 X11.25 Y20.8 E3.40709
        G1 X11.25 Y30.35 E3.80413
        G1 X30.35 Y30.35 E4.59821
        G1 X30.35 Y11.25 E5.3923
        G1 X11.25 Y11.25 E6.18639
        G0 F6000 X11.57 Y11.57
        ;LAYER:1
        G0 X11.9 Y12.2 Z0.4
        G0 X12.2 Y12.2
        ;TYPE:SKIN
        G1 F1500 X12.2 Y29.4 E6.90148
        G1 X29.4 Y29.4 E7.61657
        G1 X29.4 Y12.2 E8.33166
        G1 X12.2 Y12.2 E9.04676
        G0 F6000 X11.9 Y12.2
        G0 X29.628 Y11.972
        G0 X29.7 Y12.886
        G0 X29.149 Y12.886
        G1 F1500 X28.713 Y12.45 E9.07239
        G0 F6000 X28.006 Y12.45
        G1 F1500 X29.149 Y13.593 E9.1396
        ;LAYER:2
        G0 F6000 X29.149 Y14.3 Z0.6
        G1 F1500 X27.299 Y12.45 E9.24837
        G0 F6000 X26.592 Y12.45
        G1 F1500 X29.149 Y15.007 E9.39871
        G0 F6000 X29.149 Y15.714
        G1 F1500 X25.885 Y12.45 E9.59062
        G0 F6000 X25.177 Y12.45
        G1 F1500 X29.149 Y16.421 E9.82413
        G0 F6000 X29.149 Y17.128
        ;LAYER:3
        G1 F1500 X24.47 Y12.45 E10.09921
        G0 F6000 X23.763 Y12.45
        G1 F1500 X29.149 Y17.836 E10.41588
        ;LAYER:4
        G0 F6000 X29.149 Y18.543
        G1 F1500 X23.056 Y12.45 E10.77413
    """.trimIndent()
}