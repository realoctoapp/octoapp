package de.crysxd.octoapp.base

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first

actual object OctoPushMessaging {

    var getPushTokenAdapter: ((String?) -> Unit) -> Unit = { it(null) }

    actual suspend fun getPushToken(): String? {
        val result = MutableStateFlow<String?>(null)
        getPushTokenAdapter {
            result.value = it
        }
        return result.filterNotNull().first().takeUnless { it.isEmpty() }
    }
}