package de.crysxd.octoapp.base

import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

actual object OctoConfig {

    private var remoteConfig: DarwinRemoteConfig = object : DarwinRemoteConfig {
        override fun getLong(key: String): Long? = null
        override fun getString(key: String): String? = null
        override suspend fun fetchAndActivate(): Boolean = false
    }

    fun link(rc: DarwinRemoteConfig) {
        remoteConfig = rc
    }

    actual suspend fun fetchAndActivate(): Boolean = withContext(Dispatchers.SharedIO) {
        remoteConfig.fetchAndActivate()
    }

    actual fun get(field: OctoConfigField<Long>): Long = remoteConfig.getLong(field.key) ?: field.default
    actual fun get(field: OctoConfigField<String>): String = remoteConfig.getString(field.key) ?: field.default
    actual fun get(field: OctoConfigField<Boolean>): Boolean = when (remoteConfig.getString(field.key)) {
        "true" -> true
        "false" -> false
        else -> field.default
    }
}

interface DarwinRemoteConfig {
    fun getLong(key: String): Long?
    fun getString(key: String): String?
    suspend fun fetchAndActivate(): Boolean
}