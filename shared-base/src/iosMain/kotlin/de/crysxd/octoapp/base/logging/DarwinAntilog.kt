package de.crysxd.octoapp.base.logging

import io.github.aakira.napier.Antilog
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.LogLevel.ASSERT
import io.github.aakira.napier.LogLevel.DEBUG
import io.github.aakira.napier.LogLevel.ERROR
import io.github.aakira.napier.LogLevel.INFO
import io.github.aakira.napier.LogLevel.VERBOSE
import io.github.aakira.napier.LogLevel.WARNING
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSLocale
import platform.Foundation.NSLock
import platform.Foundation.NSTimeZone
import platform.Foundation.autoupdatingCurrentLocale
import platform.Foundation.localTimeZone

object DarwinAntilog : Antilog() {
    private const val tagLength = 28
    private const val lineLength = 150
    private val buffer = " ".repeat(tagLength)
    private val lock = NSLock()
    private val loggers = mutableListOf<(String, String, String) -> Unit>()

    private val dateFormatter = NSDateFormatter().apply {
        timeZone = NSTimeZone.localTimeZone
        locale = NSLocale.autoupdatingCurrentLocale
        dateFormat = "HH:mm:ss.SSS"
    }

    fun addLogger(logger: (String, String, String) -> Unit) {
        loggers.add(logger)
    }

    override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        try {
            lock.lock()
            val level = when (priority) {
                VERBOSE -> "V"
                DEBUG -> "D"
                INFO -> "I"
                WARNING -> "W"
                ERROR -> "E"
                ASSERT -> "A"
            }

            throwable?.stackTraceToString()?.replace(Regex("\\s{5}0x[a-f\\d]+\\s+"), "   ")?.let { stack ->
                loggers.forEach { logger -> logger(level, tag ?: "main", listOfNotNull(message, stack).joinToString(":\n")) }
            } ?: message?.let {
                loggers.forEach { logger -> logger(level, tag ?: "main", message) }
            }
        } finally {
            lock.unlock()
        }
    }
}