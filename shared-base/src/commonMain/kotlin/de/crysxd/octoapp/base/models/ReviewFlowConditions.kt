package de.crysxd.octoapp.base.models

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class ReviewFlowConditions(
    val appLaunchCounter: Int = 0,
    val firstStart: Instant = Clock.System.now(),
    val printWasActive: Boolean = false,
    val lastRequest: Instant = Instant.DISTANT_PAST
)