package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.event.HistoricTemperatureData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

@OptIn(ExperimentalCoroutinesApi::class)
class TemperatureDataRepository(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) {

    companion object {
        const val MAX_ENTRIES = 350
    }

    private val tag = "TemperatureDataRepository"

    private val caches = mutableMapOf<String, List<HistoricTemperatureData>>()
    private val mutexes = mutableMapOf<String, Mutex>()
    private val flows = mutableMapOf<String, MutableSharedFlow<List<TemperatureSnapshot>>>()
    private val componentComparator = compareByDescending<TemperatureSnapshot> {
        // Group by whether we can control
        it.canControl
    }.thenByDescending {
        // Group by "category" of component: hotend, bed, chamber, anything else
        when {
            it.component.startsWith("tool") -> 3
            it.component.startsWith("bed") -> 2
            it.component.startsWith("chamber") -> 1
            else -> 0
        }
    }.thenBy {
        // Stable order guarantee
        it.component
    }

    init {
        AppScope.launch(Dispatchers.Default) {
            Napier.i(tag = tag, message = "Collecting temperatures")

            printerConfigurationRepository.allInstanceInformationFlow()
                .distinctUntilChangedBy { m ->
                    m.values.map {
                        it.appSettings?.hiddenTemperatureComponents.hashCode() + it.activeProfile.hashCode()
                    }
                }
                .flatMapLatest { instances ->
                    listOf(instances.values).flatten().map {
                        printerEngineProvider.passiveCurrentMessageFlow(
                            tag = "temperature-repository",
                            instanceId = it.id
                        ).extractTemperatures(it)
                    }.merge()
                }.retry {
                    Napier.e(tag = tag, message = "Exception in temperature flow", throwable = it)
                    delay(1000)
                    true
                }.collect()
        }
    }

    private fun getFlow(instanceId: String) = flows.getOrPut(instanceId) { MutableSharedFlow(replay = 1) }
    private fun getCache(instanceId: String) = caches.getOrPut(instanceId) { mutableListOf() }
    private fun getMutex(instanceId: String) = mutexes.getOrPut(instanceId) { Mutex() }
    private fun putCache(instanceId: String, cache: List<HistoricTemperatureData>) = caches.put(instanceId, cache)

    private fun Flow<Message.Current>.extractTemperatures(instance: PrinterConfigurationV3) = buffer(capacity = 3).map { message ->
        getMutex(instance.id).withLock {
            val flow = getFlow(instance.id)
            val data = getCache(instance.id)

            val (snapshot, newData) = createSnapshot(data, message, instance)
            putCache(instance.id, newData)
            snapshot?.let { flow.emit(it) }
        }
    }.retry {
        Napier.e(tag = tag, message = "Exception in temperature flow (2)", throwable = it)
        delay(1000)
        true
    }

    private fun createSnapshot(
        data: List<HistoricTemperatureData>,
        message: Message.Current,
        instance: PrinterConfigurationV3?
    ): Pair<List<TemperatureSnapshot>?, List<HistoricTemperatureData>> {
        val settings = printerConfigurationRepository.get(instance?.id)?.appSettings
        val profile = printerConfigurationRepository.get(instance?.id)?.activeProfile

        // We create a true copy of the data to prevent a random crash on iOS
        val mutableData = data.toMutableList()

        // Clear data if we received a history message
        if (message.isHistoryMessage) {
            Napier.i(tag = tag, message = "Received history message, clearing data")
            mutableData.clear()
        }

        // Attach all
        mutableData.addAll(message.temps)

        // Too many data points? Drop
        if (mutableData.size > MAX_ENTRIES) {
            repeat(mutableData.size - MAX_ENTRIES) {
                mutableData.removeAt(0)
            }
        }

        // We have any data?
        return if (data.isNotEmpty()) {
            // Manual flat map (addAll used by flatMap randomly crashed on iOS)
            data.flatMap { i ->
                i.components.keys
            }.distinct().mapNotNull { component ->
                val lastData = data.last().components[component] ?: return@mapNotNull null

                // Check if this component should be included
                val isChamber = component == "chamber" && profile?.heatedChamber != false
                val isBed = component == "bed" && profile?.heatedBed != false
                val isTool = profile?.extruderHeatingComponents?.any { it == component } == true
                // This is a fix for a "feature" in the Prusa firmware. Hotend temperatures sometimes come with random leading 'O', drop them.
                // https://github.com/prusa3d/Prusa-Firmware/issues/3507
                val isPrusaJunk = component.length >= 2 && component.dropLast(1).uppercase().all { it == 'O' } && component.last() == 'T'
                val isOther = component != "chamber" && component != "bed" && !component.startsWith("tool") && !isPrusaJunk
                val include = isOther || isTool || isChamber || isBed
                if (!include) {
                    return@mapNotNull null
                }

                val history = data.map {
                    TemperatureHistoryPoint(
                        time = it.time,
                        temperature = it.components[component]?.actual ?: 0f
                    )
                }

                TemperatureSnapshot(
                    history = history.sortedBy { it.time },
                    component = component,
                    current = lastData,
                    canControl = message.tempEditables.contains(component),
                    offset = message.offsets?.get(component),
                    isHidden = settings?.hiddenTemperatureComponents?.contains(component) == true,
                    componentLabel = message.tempLabels[component] ?: component,
                    maxTemp = message.tempMaxs[component] ?: 100f,
                )
            }.sortedWith(componentComparator)
        } else {
            null
        } to mutableData
    }

    fun flow(instanceId: String) = flow {
        emitAll(getFlow(instanceId))
    }

    data class TemperatureSnapshot(
        val component: String,
        val componentLabel: String,
        val current: ComponentTemperature,
        val history: List<TemperatureHistoryPoint>,
        val canControl: Boolean,
        val offset: Float?,
        val isHidden: Boolean,
        val maxTemp: Float,
    )

    data class TemperatureHistoryPoint(
        val temperature: Float,
        val time: Long,
    )
}