package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.UniqueId

class ActivateMaterialUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<ActivateMaterialUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        printerEngineProvider.printer(param.instanceId).materialsApi.activateMaterial(
            uniqueMaterialId = param.uniqueMaterialId,
            extruderComponent = param.extruderComponent,
        )
    }

    data class Params(
        val uniqueMaterialId: UniqueId,
        val extruderComponent: String,
        val instanceId: String?
    )
}