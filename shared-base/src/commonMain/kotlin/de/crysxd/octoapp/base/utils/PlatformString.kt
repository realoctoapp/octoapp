package de.crysxd.octoapp.base.utils

import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.isDarwin
import de.crysxd.octoapp.sharedcommon.isIPadOs
import de.crysxd.octoapp.sharedcommon.isIos

typealias PlatformString = String

fun PlatformString.isForPlatform(platform: Platform): Boolean {
    val android = contains("android") && platform.isAndroid()
    val iOS = contains("ios") && platform.isIos()
    val iPadOs = contains("ipados") && platform.isIPadOs()
    val darwin = contains("darwin") && platform.isDarwin()
    return android || iOS || iPadOs || darwin
}
