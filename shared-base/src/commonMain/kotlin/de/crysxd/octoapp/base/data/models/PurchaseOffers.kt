package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.base.utils.PlatformString
import de.crysxd.octoapp.base.utils.isForPlatform
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds


@Serializable
data class PurchaseOffers(
    val baseConfig: PurchaseConfig,
    val saleConfigs: List<PurchaseConfig>?,
    val purchaseSku: List<String>?,
    val subscriptionSku: List<String>?,
) {

    companion object {
        val Default = PurchaseOffers(
            baseConfig = PurchaseConfig(
                advertisement = null,
                offers = null,
                validFrom = null,
                validTo = null,
                sellingPoints = listOf(
                    SellingPoint(
                        title = "Gcode preview",
                        description = "See a preview of Gcode files and a live Gcode view during prints",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                    ),
                    SellingPoint(
                        title = "Multiple printers",
                        description = "Connect multiple printers to OctoApp and switch between them using the Quick Switch menu, home screen widgets, home screen shortcuts and notifications",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_quick_switch.webp?alt=media&token=152de0c4-6346-4a2b-ada5-2d95cf8272b6"
                    ),
                    SellingPoint(
                        title = "Automatic lights",
                        description = "Let OctoApp automatically turn on and off your lights when you look at your webcam. Supports any light which has 'on' and 'off' controls in OctoApp",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_auto_lights.webp?alt=media&token=10d451a8-cc4a-41ad-975f-964a3f97fa0a"
                    ),
                    SellingPoint(
                        title = "Infinite app widgets",
                        description = "Create as many widgets and shortcuts on your Android home screen as you want",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_inifite_widgets.webp?alt=media&token=26bfe707-5f95-4bea-a41e-8317212c0541"
                    ),
                    SellingPoint(
                        title = "Advanced webcam",
                        description = "Make use of the advanced webcam formats RTSP and HLS for higher video quality and lower bandwidth (only works if your OctoPi is configured for those)",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_advanced_webcam.webp?alt=media&token=33939c08-9c1c-44bd-b2e0-f85827d1159c"
                    ),
                    SellingPoint(
                        title = "File management",
                        description = "Search, upload, copy, rename, download and share files stored on your OctoPrint's storage",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_files_2.webp?alt=media&token=3424be2f-d5cc-4687-b951-13f61ac5ae7b"
                    ),
                    SellingPoint(
                        title = "File management",
                        description = "Search, upload, copy, rename, download and share files stored on your OctoPrint's storage",
                        imageUrl = "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_files_1.webp?alt=media&token=614688d8-ec6e-42fa-9984-7e5e651703ab"
                    ),
                    SellingPoint(
                        title = "Much more to come!",
                        description = "OctoApp receives a big update every 4-8 weeks with new feature and improvements!",
                        imageUrl = "party"
                    ),
                ),
                texts = Texts(
                    highlightBanner = null,
                    purchaseScreenTitle = "Thank you for<br>supporting OctoApp!",
                    purchaseScreenContinueCta = "Support OctoApp",
                    purchaseScreenDescription = "Hi, I'm Chris! Creating OctoApp and supporting all of you takes a lot of my time. To be able to continue this effort, I decided to make more advanced features exclusive for my supporters.</u><br/><br/><b>I will never start showing ads in this app nor will I take away features that are already available.</b><br/><br/>Following features will be unlocked:",
                    skuListTitle = "High Eight! Thanks for your support! You rock!",
                    launchPurchaseScreenCta = "Support OctoApp",
                    purchaseScreenMoreFeatures = "and much more to come!",
                    launchPurchaseScreenHighlight = null,
                )
            ),
            saleConfigs = null,
            purchaseSku = null,
            subscriptionSku = null,
        )
    }

    @Serializable
    @OptIn(ExperimentalSerializationApi::class)
    data class PurchaseConfig(
        val advertisement: Advertisement?,
        val offers: Map<String, Offer>?,
        val texts: Texts,
        @JsonNames("sellingPoints2", "sellingPoints") val sellingPoints: List<SellingPoint>,
        val validFrom: String?,
        val validTo: String?,
    ) {
        companion object {
            private const val COUNTDOWN_PLACEHOLDER = "{{countdown}}"
            private val platform by lazy {
                try {
                    SharedCommonInjector.get().platform
                } catch (e: Exception) {
                    null
                }
            }
        }

        val validFromDate get() = validFrom?.let { Instant.parse(it + "Z") }
        val validToDate get() = validTo?.let { Instant.parse(it + "Z") }

        val filteredSellingPoints = sellingPoints.filter { sp ->
            platform?.let { sp.platform.isForPlatform(it) } ?: true
        }

        private val countDown: String
            get() {
                fun String.padded() = if (length == 1) "0$this" else this
                val now = Clock.System.now()
                val until = validToDate ?: Instant.DISTANT_FUTURE
                val left = (until - now).coerceAtLeast(0.seconds)
                val days = left.inWholeDays
                val hours = (left - days.days).inWholeHours
                val minutes = (left - days.days - hours.hours).inWholeMinutes
                val seconds = (left - days.days - hours.hours - minutes.minutes).inWholeSeconds
                return listOfNotNull(
                    "${days}d".takeIf { days > 0 },
                    "${hours}h".padded().takeIf { days > 0 },
                    "${minutes}m".padded().takeIf { days > 0 },
                    "${seconds}s".padded().takeIf { days > 0 },
                ).joinToString(" ")
            }

        val textsWithData
            get() = texts.copy(
                highlightBanner = texts.highlightBanner?.replace(COUNTDOWN_PLACEHOLDER, countDown),
                launchPurchaseScreenHighlight = texts.launchPurchaseScreenHighlight?.replace(COUNTDOWN_PLACEHOLDER, countDown),
            )
        val advertisementWithData get() = advertisement?.copy(message = advertisement.message.replace(COUNTDOWN_PLACEHOLDER, countDown))
    }

    @Serializable
    data class Advertisement(
        val id: String,
        val message: String,
    )

    @Serializable
    data class Texts(
        val highlightBanner: String? = null,
        val purchaseScreenTitle: String,
        val purchaseScreenContinueCta: String,
        val purchaseScreenDescription: String,
        val purchaseScreenMoreFeatures: String,
        val skuListTitle: String,
        val launchPurchaseScreenCta: String,
        val launchPurchaseScreenHighlight: String?,
    )

    @Serializable
    data class Offer(
        val badge: Badge? = null,
        val dealFor: String? = null,
        val label: String? = null,
        val description: String? = null,
    )

    @Serializable
    data class SellingPoint(
        val title: String? = null,
        val description: String? = null,
        val imageUrl: String? = null,
        val platform: PlatformString = "android,ios,ipados"
    )

    @Serializable
    enum class Badge {
        @SerialName("popular")
        Popular,

        @SerialName("best_value")
        BestValue,

        @SerialName("sale")
        Sale
    }

    val activeConfig
        get() = saleConfigs?.firstOrNull {
            val from = it.validFromDate
            val to = it.validToDate
            from != null && to != null && Clock.System.now() in from..to
        } ?: saleConfigs?.firstOrNull {
            val from = it.validFromDate
            val to = it.validToDate
            to == null && from != null && Clock.System.now() > from
        } ?: baseConfig
}