package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.models.JobHistoryItem
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.mapData
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.flatten
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerJobList
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.updateAndGet

class PrintHistoryRepository(
    private val printerEngineProvider: PrinterEngineProvider,
    private val fileListRepository: FileListRepository
) {

    private val cache = MutableStateFlow(mapOf<String, StateFlow<FlowState<List<JobHistoryItem>>>>())
    private val tag = "FileHistoryRepository"

    fun observeJobHistory(instanceId: String, skipCache: Boolean) = flow {
        val nested = cache.updateAndGet {
            val current = it[instanceId]
            if (current != null && !skipCache && current.value !is FlowState.Error) {
                it
            } else {
                it + (instanceId to createFlow(instanceId))
            }
        }[instanceId].let {
            requireNotNull(it) { "Just created, not found: $instanceId" }
        }

        emitAll(nested)
    }

    private suspend fun createFlow(instanceId: String): StateFlow<FlowState<List<JobHistoryItem>>> = when (val engine = printerEngineProvider.printer(instanceId)) {
        is OctoPrintEngine -> createOctoPrintFlow(instanceId)
        is MoonrakerEngine -> createMoonrakerFlow(instanceId, engine)
        else -> throw IllegalStateException("Unsupported engine: $engine")
    }.stateIn(AppScope, started = SharingStarted.Lazily, initialValue = FlowState.Loading())

    private fun createOctoPrintFlow(instanceId: String): Flow<FlowState<List<JobHistoryItem>>> = fileListRepository.observeTree(
        instanceId = instanceId,
        fileOrigin = FileOrigin.Gcode,
    ).mapData { list ->
        list.files.flatten().mapNotNull { ref ->
            // We know OctoPrint always returns full files
            (ref as? FileObject.File)?.prints?.last?.let { print ->
                JobHistoryItem(
                    path = ref.path,
                    fileOrigin = ref.origin,
                    time = print.date,
                    success = print.success,
                    fileId = null,
                )
            }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun createMoonrakerFlow(instanceId: String, engine: MoonrakerEngine) = printerEngineProvider.passiveEventFlow(instanceId = instanceId)
        .filter { (it as? Event.MessageReceived)?.message is Message.Event.PrintHistoryUpdated }
        .map { }
        .onStart { emit(Unit) }
        .flatMapLatest {
            Napier.i(tag = tag, message = "Loading print history")
            flow {
                emit(FlowState.Loading())
                val history = engine.jobApi.getJobHistory(
                    limit = 50,
                    ascending = false,
                ).jobs?.mapNotNull { job ->
                    JobHistoryItem(
                        path = job.filename ?: return@mapNotNull null,
                        fileOrigin = FileOrigin.Gcode,
                        time = job.startTime ?: return@mapNotNull null,
                        success = job.status == MoonrakerJobList.Job.Status.Completed,
                        fileId = job.metadata?.uuid,
                    )
                } ?: emptyList()
                emit(FlowState.Ready(history))
            }
        }.catch { e ->
            Napier.i(tag = tag, message = "Failed to load history", throwable = e)
            emit(FlowState.Error(e))
        }
}