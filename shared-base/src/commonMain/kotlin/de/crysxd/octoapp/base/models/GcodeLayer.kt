package de.crysxd.octoapp.base.models

@kotlinx.serialization.Serializable
data class GcodeLayer(
    val moves: Map<GcodeMove.Type, Pair<List<GcodeMove>, FloatArray>>,
    val info: GcodeLayerInfo
)