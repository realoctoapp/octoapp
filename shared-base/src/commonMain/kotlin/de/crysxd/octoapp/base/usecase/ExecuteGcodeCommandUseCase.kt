package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull

class ExecuteGcodeCommandUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val gcodeHistoryRepository: GcodeHistoryRepository,
    private val serialCommunicationLogsRepository: SerialCommunicationLogsRepository
) : UseCase2<ExecuteGcodeCommandUseCase.Param, List<ExecuteGcodeCommandUseCase.Response>>() {

    override suspend fun doExecute(param: Param, logger: Logger): List<Response> {
        val result = if (param.recordResponse) {
            when (param.command) {
                is GcodeCommand.Single -> listOf(param.command)
                is GcodeCommand.Batch -> param.command.commands.map { GcodeCommand.Single(it) }
            }.map {
                executeAndRecordResponse(it, param.instanceId, param.fromUser, logger, param.recordTimeoutMs)
            }
        } else {
            execute(param.command, param.instanceId, param.fromUser, logger)
            when (param.command) {
                is GcodeCommand.Single -> listOf(Response.DroppedResponse)
                is GcodeCommand.Batch -> param.command.commands.map { Response.DroppedResponse }
            }
        }

        if (param.fromUser) {
            gcodeHistoryRepository.recordGcodeSend(
                when (param.command) {
                    is GcodeCommand.Single -> param.command.command
                    is GcodeCommand.Batch -> param.command.commands.joinToString("\n")
                }
            )
        }

        return result
    }

    private suspend fun executeAndRecordResponse(
        command: GcodeCommand.Single,
        instanceId: String?,
        fromUser: Boolean,
        logger: Logger,
        timeoutMs: Long
    ) = withContext(Dispatchers.Default) {
        val readJob = async {
            var sendLineFound = false
            var wasExecuted = false
            val sendLinePattern = Regex("^Send:.*%%COMMAND%%.*".replace("%%COMMAND%%", command.command))
            val responseEndLinePattern = Regex(OctoConfig.get(OctoConfigField.GcodeResponseEndLinePattern))
            val spamMessagePattern = Regex(OctoConfig.get(OctoConfigField.GcodeResponseSpamPattern))

            // Collect all lines from when we see `Send: $command` until we see `Recv: ok`
            val list = serialCommunicationLogsRepository
                .activeFlow(instanceId = instanceId, includeOld = false)
                .map { it.content }
                .onEach {
                    if (!wasExecuted) {
                        logger.i("Receiving first logs, executing $command now")
                        wasExecuted = true
                        execute(command, instanceId, fromUser, logger)
                    }
                }
                .filter {
                    // Filter undesired "Spam"
                    !spamMessagePattern.matches(it)
                }
                .filter {
                    // Skip until send line was found
                    if (!sendLineFound) {
                        sendLineFound = sendLinePattern.matches(it)
                        if (sendLineFound) {
                            logger.v("Send line was found: $it")
                        } else {
                            logger.v("Skipping $it")
                        }
                        sendLineFound
                    } else {
                        logger.v("Passing $it")
                        true
                    }
                }
                .takeWhile {
                    logger.v("Recording $it")
                    !responseEndLinePattern.matches(it)
                }
                .toList()

            logger.v("Recorded response for ${command.command}: $list")
            Response.RecordedResponse(
                sendLine = list.first(),
                responseLines = list.subList(1, list.size)
            )
        }

        // Wait until response is read
        return@withContext withTimeoutOrNull(timeoutMs) {
            readJob.await()
        } ?: let {
            logger.w("Response recording timed out")
            readJob.cancel()
            Response.DroppedResponse
        }
    }

    private suspend fun execute(command: GcodeCommand, instanceId: String?, fromUser: Boolean, logger: Logger) {
        when (command) {
            is GcodeCommand.Single -> logExecuted(command.command, fromUser)
            is GcodeCommand.Batch -> command.commands.forEach { logExecuted(it, fromUser) }
        }

        logger.v("Executing $command")
        printerEngineProvider.printer(instanceId = instanceId).printerApi.executeGcodeCommand(command)
    }

    private suspend fun logExecuted(command: String, fromUser: Boolean) {
        serialCommunicationLogsRepository.addInternalLog(
            log = "[OctoApp] Send: $command",
            fromUser = fromUser
        )
    }

    sealed class Response {
        data class RecordedResponse(
            val sendLine: String,
            val responseLines: List<String>,
        ) : Response()

        data object DroppedResponse : Response()
    }

    data class Param(
        val command: GcodeCommand,
        val fromUser: Boolean,
        val instanceId: String? = null,
        val recordTimeoutMs: Long = 30_000L,
        val recordResponse: Boolean = false
    )
}