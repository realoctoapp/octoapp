package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.base.utils.AppScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit

class HandleAutomaticLightEventUseCase(
    private val getPowerDevicesUseCase: GetPowerDevicesUseCase,
    private val octoPreferences: OctoPreferences
) : UseCase2<HandleAutomaticLightEventUseCase.Event, Boolean>() {

    companion object {
        private var lastJob: Job? = null
        private var keepOnCounter = 0
        private val lock = Semaphore(1)
    }

    override suspend fun doExecute(param: Event, logger: Logger): Boolean = lock.withPermit {
        try {
            if (!BillingManager.isFeatureEnabled(FEATURE_AUTOMATIC_LIGHTS)) {
                logger.i("Automatic lights disabled, skipping any action")
                return false
            }

            val autoIds = octoPreferences.automaticLights
            val lights = getPowerDevicesUseCase.execute(GetPowerDevicesUseCase.Params(instanceId = param.instanceId)).results
                .filter { autoIds.contains(it.device.id) }

            if (lights.isEmpty()) {
                logger.i("No automatic lights set up, skipping any action")
                return false
            }

            // Determine new state
            val prevCounter = keepOnCounter
            keepOnCounter = when (param) {
                is Event.WebcamVisible -> keepOnCounter + 1
                is Event.WebcamGone -> keepOnCounter - 1
            }.coerceAtLeast(0)

            val newState = when {
                prevCounter == 0 && keepOnCounter > 0 -> true
                prevCounter > 0 && keepOnCounter == 0 -> false
                else -> {
                    logger.d("No action taken, still $keepOnCounter references")
                    null
                }
            }

            // Push new state
            if (newState != null) {
                lastJob?.cancelAndJoin()
                lastJob = AppScope.launch {
                    try {
                        if (param.delayAction) {
                            delay(5000)
                        }

                        if (lastJob?.isCancelled == true) {
                            logger.i("Action cancelled: $newState")
                            return@launch
                        }

                        lights.forEach { result ->
                            when (newState) {
                                true -> {
                                    logger.i("☀️ Turning ${lights.size} lights on to illuminate webcam")
                                    result.device.turnOn()
                                }

                                false -> {
                                    logger.i("🌙 Turning ${lights.size} lights off")
                                    result.device.turnOff()
                                }
                            }
                        }
                    } catch (e: Exception) {
                        logger.e(e = e, message = "Failed to handle light event")
                    }
                }
            }

            // Wait for light to be on, but don't wait for it to be off
            if (param is Event.WebcamVisible) {
                lastJob?.join()
            }

            true
        } catch (e: Exception) {
            logger.e(e = e, message = "Failed to handle light event")
            false
        }
    }

    sealed class Event {
        abstract val source: String
        abstract val delayAction: Boolean
        abstract val instanceId: String?

        data class WebcamVisible(
            override val source: String,
            override val delayAction: Boolean = false,
            override val instanceId: String? = null
        ) : Event()

        data class WebcamGone(
            override val source: String,
            override val delayAction: Boolean = false,
            override val instanceId: String? = null
        ) : Event()
    }
}
