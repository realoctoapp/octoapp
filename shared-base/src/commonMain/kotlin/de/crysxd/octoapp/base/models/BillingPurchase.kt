package de.crysxd.octoapp.base.models

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class BillingPurchase(
    val productId: String,
    val isSubscription: Boolean,
    val token: String?,
    val transactionId: String,
    @Transient val nativePurchase: Any? = null,
)