package de.crysxd.octoapp.base.utils

import io.github.aakira.napier.Napier
import kotlinx.datetime.Clock

inline fun <T> measureTime(traceName: String, block: () -> T): T {
    Napier.v(tag = "Performance", message = "[$traceName] started")
    val start = Clock.System.now()
    return try {
        block()
    } finally {
        val end = Clock.System.now()
        Napier.v(tag = "Performance", message = "[$traceName] completed in ${end - start}")
    }
}