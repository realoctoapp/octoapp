package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.framework.urlFromEncodedPath
import de.crysxd.octoapp.sharedexternalapis.http.ExternalApiHttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.http.URLBuilder
import io.ktor.http.appendEncodedPathSegments
import io.ktor.http.appendPathSegments
import io.ktor.utils.io.core.Input
import io.ktor.utils.io.core.readBytes

class GenericDownloadUseCase(
    private val engineProvider: PrinterEngineProvider,
) : UseCase2<GenericDownloadUseCase.Params, ByteArray>() {

    companion object {
        private val externalHttpClient by lazy { ExternalApiHttpClient() }
    }

    init {
        suppressLogging = true
    }

    override suspend fun doExecute(param: Params, logger: Logger): ByteArray = when (param) {
        is Params.FromPrinter -> loadFromPrinter(path = param.path, pathEncoded = param.pathEncoded, instanceId = param.instanceId)
        is Params.FromPublicWeb -> loadFromWeb(url = param.url)
    }

    private suspend fun loadFromWeb(
        url: String
    ) = externalHttpClient
        .get(url)
        .body<Input>()
        .readBytes()

    private suspend fun loadFromPrinter(
        path: String,
        pathEncoded: Boolean,
        instanceId: String
    ): ByteArray {
        // Convert the path into a valid URL, just wrong host
        val parts = path.split("?")
        val tempUrl = URLBuilder("http://localhost?${parts.getOrNull(1) ?: ""}").apply {
            val segments = parts.first().split("/")
            if (pathEncoded) {
                appendEncodedPathSegments(segments)
            } else {
                appendPathSegments(segments)
            }
        }.build()

        val copyParams: URLBuilder.() -> Unit = {
            tempUrl.parameters.forEach { key, value ->
                parameters.appendAll(key, value)
            }
        }

        return engineProvider.printer(instanceId = instanceId).genericRequest { baseUrl, httpClient ->
            httpClient.get {
                // Copy the url path and query. Path is now always encoded
                urlFromEncodedPath(baseUrl = baseUrl, pathSegments = tempUrl.encodedPath.split("/"), additional = copyParams)
            }.body<Input>().readBytes()
        }
    }

    sealed class Params {
        data class FromPrinter(
            val path: String,
            val pathEncoded: Boolean,
            val instanceId: String,
        ) : Params()

        data class FromPublicWeb(
            val url: String
        ) : Params()
    }
}