package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.get
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.ActionType
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.AvatarState
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase.UiState
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase.PowerState.Unknown
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.exceptions.PrintBootingException
import de.crysxd.octoapp.engine.models.connection.Connection
import de.crysxd.octoapp.engine.models.connection.ConnectionState
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.framework.withoutBasicAuth
import de.crysxd.octoapp.sharedcommon.http.framework.withoutQuery
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import okio.IOException
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class ConnectPrinterUseCaseOctoPrintCore(
    private val autoConnectPrinterUseCase: AutoConnectPrinterUseCase,
    private val getPrinterConnectionUseCase: GetPrinterConnectionUseCase,
    private val getPowerDevicesUseCase: GetPowerDevicesUseCase,
    private val octoPreferences: OctoPreferences,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
    private val activeHttpUrlUseCase: GetActiveHttpUrlUseCase,
) : ConnectPrinterUseCase.Core {
    companion object {
        private val MIN_LOADING_DELAY = 2.seconds
        private val NOT_AVAILABLE_TIMEOUT = 5.seconds
        private val ERROR_RETRY_DELAY = 3.seconds
        private val PORT_POLLING_INTERVAL = 1.seconds
        private val PSU_POLLING_INTERVAL = 3.seconds
    }

    private val tag = "ConnectPrinterUseCase/OctoPrint"
    private val connectionTimeout = OctoConfig.get(OctoConfigField.PrinterConnectionTimeout)
    private var lastConnectionAttempt = Instant.DISTANT_PAST
    private var startedAt = Clock.System.now()

    private var supervisorJob: Job? = null
    private var scope: CoroutineScope? = null

    private val instanceIdFlow = MutableStateFlow<String?>(null)
    private val userAllowedConnectionAtFlow = MutableStateFlow(Instant.DISTANT_PAST)
    private val manualTriggerFlow = MutableSharedFlow<Unit>()
    private val psuCycledStateFlow = MutableStateFlow<PsuCyclingState>(PsuCyclingState.NotCycled)
    private val manualPsuStateFlow = MutableStateFlow<Boolean?>(null)

    private val isPrintingFlow = instanceIdFlow.filterNotNull().flatMapLatest { instanceId ->
        flow {
            // Default to "we are printing" to make sure we never connect unless we know we are not printing
            emit(true)
            emitAll(printerEngineProvider.passiveCurrentMessageFlow(tag = "connect-printer", instanceId = instanceId).map { it.state.flags.isPrinting() })
        }
    }

    private val activeHttpUrlFlow = instanceIdFlow.filterNotNull().flatMapLatest { instanceId ->
        printerEngineProvider.printer(instanceId).baseUrl
    }

    private val psuSupportedFlow = instanceIdFlow.filterNotNull().flatMapLatest { instanceId ->
        printerConfigurationRepository.instanceInformationFlow(instanceId)
    }.flatMapLatest { instance ->
        flow {
            instance ?: return@flow emit(null)

            // Check if the default device is turned on or off or use the only device available
            val devicesParams = GetPowerDevicesUseCase.Params(
                instanceId = instance.id,
                requiredCapabilities = listOf(PowerDevice.Capability.ControlPrinterPower)
            )
            val devices = getPowerDevicesUseCase.execute(devicesParams).results
            val default = instance.appSettings?.defaultPowerDevices?.get(AppSettings.DEFAULT_POWER_DEVICE_PSU)
                ?: devices.firstOrNull().takeIf { devices.size == 1 }?.device?.uniqueId

            if (default == AppSettings.DEFAULT_POWER_DEVICE_VALUE_NONE) {
                Napier.i(tag = tag, message = "Default power device is none")
                emit(PsuState(psuIsOn = null, psuNeedsConfiguration = false, psuIsAvailable = false))
            } else {
                Napier.i(tag = tag, message = "Default power device: $default")
                default?.let { deviceId ->
                    val params = GetPowerDevicesUseCase.Params(onlyGetDeviceWithUniqueId = deviceId, instanceId = instance.id)
                    val state = getPowerDevicesUseCase.execute(params).results.firstOrNull()?.state?.invoke()?.map { state ->
                        Napier.d(tag = tag, message = "Processing PSU state: $state")
                        PsuState(
                            psuIsOn = state.takeUnless { it is Unknown }?.isOn,
                            psuNeedsConfiguration = false,
                            psuIsAvailable = true
                        )
                    }?.onEach {
                        // Reset manual, we now updated the real one
                        manualPsuStateFlow.emit(null)
                    } ?: flowOf(null)
                    emitAll(state)
                } ?: let {
                    Napier.i(tag = tag, message = "Default power device is null")
                    emit(PsuState(psuIsOn = null, psuNeedsConfiguration = devices.isNotEmpty(), psuIsAvailable = false))
                }
            }
        }.onCompletion {
            Napier.i(tag = tag, message = "Finish PSU check")
        }.onStart {
            Napier.i(tag = tag, message = "Start PSU check")
            emit(PsuState(psuIsOn = null, psuNeedsConfiguration = false, psuIsAvailable = false))
        }
    }.retry {
        Napier.e(tag = tag, message = "Error in PSU support", throwable = it)
        delay(ERROR_RETRY_DELAY)
        true
    }.flowOn(Dispatchers.SharedIO)

    private val availableSerialConnectionsFlow = instanceIdFlow.filterNotNull().flatMapLatest { instanceId ->
        flow {
            emit(null)
            var lastWasSuccess = false
            while (currentCoroutineContext().isActive) {
                try {
                    // It's not unused...
                    @Suppress("UNUSED_VALUE")
                    lastWasSuccess = false

                    emit(ConnectionResult.Success(getPrinterConnectionUseCase.execute(GetPrinterConnectionUseCase.Params(instanceId))))
                    lastWasSuccess = true
                    delay(PORT_POLLING_INTERVAL)
                } catch (e: CancellationException) {
                    // Ignore, usually the child got cancelled randomly
                    // If the main flow got cancelled the while loop will exit
                    Napier.w(tag = tag, message = "Ignoring exception in connection result: ${e::class.qualifiedName}: ${e.message} ")
                } catch (e: Exception) {
                    // Do not log full exception to not litter logs!
                    Napier.e(tag = tag, message = "Connection result failed with: ${e::class.qualifiedName}: ${e.message}")

                    // Only delay and emit failure if last attempt was not successful, so we get one
                    // "free" failure
                    if (!lastWasSuccess) {
                        emit(ConnectionResult.Failure(e))
                        delay(ERROR_RETRY_DELAY)
                        lastWasSuccess = false
                    }
                }
            }
        }
    }.retry {
        Napier.e(tag = tag, message = "Error in serial connection flow", throwable = it)
        delay(ERROR_RETRY_DELAY)
        true
    }.onStart {
        startedAt = Clock.System.now()
    }.flowOn(Dispatchers.SharedIO)

    override suspend fun doExecute(param: ConnectPrinterUseCase.Params, logger: UseCase2.Logger): Flow<UiState> {
        instanceIdFlow.emit(param.instanceId)
        return combine(
            listOf(
                flowOf(flowOf(Unit), octoPreferences.updatedFlow, manualTriggerFlow).flattenMerge(),
                availableSerialConnectionsFlow,
                psuCycledStateFlow,
                psuSupportedFlow,
                userAllowedConnectionAtFlow,
                manualPsuStateFlow,
                instanceIdFlow,
                isPrintingFlow,
                activeHttpUrlFlow
            )
        ) {
            computeUiState(
                connectionResult = it[1] as ConnectionResult?,
                psuCyclingState = it[2] as PsuCyclingState,
                psuState = it[3] as PsuState?,
                userAllowedConnectionAt = it[4] as Instant,
                manualPsuState = it[5] as Boolean?,
                instanceId = it[6] as String,
                isPrinting = it[7] as Boolean,
                url = it[8] as Url,
                logger = logger,
            )
        }.onStart {
            rebuildCoroutineScope()
            Napier.d(tag = tag, message = "On Start")
        }.onCompletion {
            Napier.d(tag = tag, message = "On completion: $it")
            supervisorJob?.cancel()
        }
    }

    private fun rebuildCoroutineScope() {
        supervisorJob?.cancel()
        supervisorJob = SupervisorJob()
        scope = CoroutineScope(supervisorJob!! + Dispatchers.SharedIO + CoroutineExceptionHandler { _, throwable ->
            Napier.e(tag = tag, message = "Caught NON-CONTAINED exception in ConnectPrinterUseCase#scope!", throwable = throwable)
            ExceptionReceivers.dispatchException(throwable)
        })
    }

    // ===================
    // State determination
    // ===================

    private fun computeUiState(
        connectionResult: ConnectionResult?,
        psuCyclingState: PsuCyclingState,
        psuState: PsuState?,
        userAllowedConnectionAt: Instant,
        manualPsuState: Boolean?,
        instanceId: String,
        isPrinting: Boolean,
        logger: UseCase2.Logger,
        url: Url
    ): UiState {
        try {
            // Connection
            val connectionResponse = (connectionResult as? ConnectionResult.Success)?.response
            val exception = (connectionResult as? ConnectionResult.Failure)?.exception
                ?: IOException("No response within $NOT_AVAILABLE_TIMEOUT, still trying to connect")

            // PSU
            val psuNeedsConfiguration = psuState?.psuNeedsConfiguration == true
            val psuIsAvailable = psuState?.psuIsAvailable == true
            val psuIsOn = manualPsuState ?: psuState?.psuIsOn

            // Are we allowed to automatically connect the printer?
            val manuallyAllowedAutoConnect = (Clock.System.now() - userAllowedConnectionAt) < 3.minutes
            val isAutoConnect = octoPreferences.isAutoConnectPrinter || manuallyAllowedAutoConnect

            val activeSince = Clock.System.now() - startedAt
            Napier.d(
                tag = tag,
                message = """
                    ----
                      ConnectionResponse: $connectionResponse
                      PsuNeedsConfiguration: $psuNeedsConfiguration
                      PsuIsAvailable:  $psuIsAvailable
                      PsuCycled: $psuCyclingState
                      PsuState: $psuIsOn
                      activeSince: $activeSince
                      isAutoConnect: $isAutoConnect
                      instanceId: $instanceId
                      isPrinting: $isPrinting
                      url: $url
                    ----
                """.trimIndent()
            )


            if (activeSince > MIN_LOADING_DELAY) {
                return when {
                    isOctoPrintUnavailable(connectionResult) -> createOctoPrintNotAvailableState(exception, url)
                    isNotAvailable(connectionResult) -> createOctoPrintNotAvailableState(exception, url)
                    isOctoPrintStarting(connectionResult) -> createOctoPrintStartingState()
                    isPsuBeingCycled(psuCyclingState) -> createCyclingState()
                    connectionResponse == null -> createInitializingState()
                    isPrinterOffline(connectionResponse, psuCyclingState) -> createOfflineState(psuIsAvailable)
                    isPrinterConnecting(connectionResponse) -> createConnectingState()
                    isPrinterConnected(connectionResponse) -> createConnectingState()
                    !isAutoConnect -> createWaitingForUserState(octoPreferences.wasAutoConnectPrinterInfoShown)
                    isNoPrinterAvailable(connectionResponse) -> createWaitingForPrinterToComeOnlineState(
                        psuIsOn = psuIsOn,
                        psuIsAvailable = psuIsAvailable,
                        psuNeedsConfiguration = psuNeedsConfiguration
                    )

                    isPrinting -> createConnectingState()
                    else -> {
                        // Printer ready to connect
                        autoConnect(connectionResponse, instanceId = instanceId, logger = logger)
                        createWaitingForPrinterToComeOnlineState(
                            psuIsOn = psuIsOn,
                            psuIsAvailable = psuIsAvailable,
                            psuNeedsConfiguration = psuNeedsConfiguration
                        )
                    }
                }
            }
        } catch (e: CancellationException) {
            throw e
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Exception while computing state", throwable = e)
        }

        return createInitializingState()
    }

    private fun isNotAvailable(connectionResult: ConnectionResult?) =
        connectionResult == null && (Clock.System.now() - startedAt) > NOT_AVAILABLE_TIMEOUT

    private fun isPsuBeingCycled(psuCycledState: PsuCyclingState) =
        psuCycledState == PsuCyclingState.Cycling

    private fun isOctoPrintUnavailable(result: ConnectionResult?) =
        result is ConnectionResult.Failure && result.exception !is CancellationException

    private fun isOctoPrintStarting(result: ConnectionResult?) =
        (result as? ConnectionResult.Failure)?.exception is PrintBootingException

    private fun isNoPrinterAvailable(connectionResponse: ConnectionState) =
        connectionResponse.options.ports.isEmpty()

    private fun isPrinterOffline(
        connectionResponse: ConnectionState,
        psuState: PsuCyclingState
    ) = connectionResponse.options.ports.isNotEmpty() &&
            (isInErrorState(connectionResponse) || isConnectionAttemptTimedOut(connectionResponse)) &&
            psuState != PsuCyclingState.Cycled

    private fun isConnectionAttemptTimedOut(connectionResponse: ConnectionState) =
        isPrinterConnecting(connectionResponse) && (Clock.System.now() - lastConnectionAttempt) > connectionTimeout

    private fun isInErrorState(connectionResponse: ConnectionState) = listOf(
        Connection.State.MAYBE_UNKNOWN_ERROR,
        Connection.State.MAYBE_CONNECTION_ERROR
    ).contains(connectionResponse.current.state)

    private fun isPrinterConnecting(connectionResponse: ConnectionState) = listOf(
        Connection.State.MAYBE_CONNECTING,
        Connection.State.MAYBE_DETECTING_SERIAL_PORT,
        Connection.State.MAYBE_DETECTING_SERIAL_CONNECTION,
        Connection.State.MAYBE_DETECTING_BAUDRATE
    ).contains(connectionResponse.current.state)


    // =============
    // Auto connect
    // =============

    private fun autoConnect(connectionResponse: ConnectionState, instanceId: String, logger: UseCase2.Logger) {
        if (connectionResponse.options.ports.isNotEmpty() && !didJustAttemptToConnect() && !isPrinterConnecting(connectionResponse)) {
            // Record now so we do not attempt again in case of error
            recordConnectionAttempt()

            Napier.i(tag = tag, message = "Attempting auto connect")
            requireNotNull(scope) { "Coroutine scope not ready!" }.launch(Dispatchers.SharedIO) {
                val result = try {
                    autoConnectPrinterUseCase.execute(
                        if (connectionResponse.current.state == Connection.State.MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT) {
                            OctoAnalytics.logEvent(OctoAnalytics.Event.PrinterAutoConnectFailed)
                            AutoConnectPrinterUseCase.Params(
                                instanceId = instanceId,
                                port = connectionResponse.options.ports.firstOrNull() ?: AutoConnectPrinterUseCase.AUTO_PORT
                            )
                        } else {
                            AutoConnectPrinterUseCase.Params(instanceId = instanceId)
                        }
                    )
                } catch (e: Exception) {
                    logger.e(e)
                    null
                }

                // Did we trigger the connection? If not reset connection attempt so we can try again
                when (result) {
                    null -> Unit
                    AutoConnectPrinterUseCase.Result.ConnectionTriggered -> recordConnectionAttempt()
                    AutoConnectPrinterUseCase.Result.PrinterBusy -> resetConnectionAttempt()
                }

                psuCycledStateFlow.emit(PsuCyclingState.NotCycled)
            }
        }
    }

    private fun didJustAttemptToConnect() =
        (Clock.System.now() - lastConnectionAttempt) < 10.seconds

    private fun recordConnectionAttempt() {
        lastConnectionAttempt = Clock.System.now()
    }

    private fun isPrinterConnected(connectionResponse: ConnectionState) = connectionResponse.current.port != null ||
            connectionResponse.current.baudrate != null

    private fun resetConnectionAttempt() {
        lastConnectionAttempt = Instant.DISTANT_PAST
    }

    // ======
    // Triggers
    // ======

    private suspend fun getDefaultPowerDevice(): PowerDevice? {
        val id = printerConfigurationRepository.get(instanceIdFlow.value)?.appSettings?.defaultPowerDevices?.get(AppSettings.DEFAULT_POWER_DEVICE_PSU)
        val params = GetPowerDevicesUseCase.Params(instanceId = instanceIdFlow.value, onlyGetDeviceWithUniqueId = id)
        return getPowerDevicesUseCase.execute(params).results.firstOrNull()?.device
    }

    override suspend fun executeAction(actionType: ActionType) = when (actionType) {
        ActionType.BeginConnect -> beginConnect()
        ActionType.AutoConnectTutorial -> Unit
        ActionType.TurnPsuOn -> turnPsuOn()
        ActionType.TurnPsuOff -> turnPsuOff()
        ActionType.ConfigurePsu -> Unit
        ActionType.CyclePsu -> cyclePsu()
        ActionType.Retry -> retryConnectionFromOfflineState()
    }

    private suspend fun cyclePsu() {
        getDefaultPowerDevice()?.turnOn()
        manualPsuStateFlow.emit(true)
        psuCycledStateFlow.emit(PsuCyclingState.Cycled)
        resetConnectionAttempt()
    }

    private suspend fun turnPsuOff() {
        getDefaultPowerDevice()?.turnOff()
        manualPsuStateFlow.emit(false)
        resetConnectionAttempt()
    }

    private suspend fun turnPsuOn() {
        getDefaultPowerDevice()?.turnOn()
        manualPsuStateFlow.emit(true)
        resetConnectionAttempt()
    }

    private suspend fun retryConnectionFromOfflineState() {
        lastConnectionAttempt = Instant.DISTANT_PAST
        psuCycledStateFlow.emit(PsuCyclingState.Cycled)
        userAllowedConnectionAtFlow.emit(Clock.System.now())
    }

    private suspend fun beginConnect() {
        Napier.i(tag = tag, message = "Connection initiated")
        userAllowedConnectionAtFlow.emit(Clock.System.now())
    }

    // ======
    // Models
    // ======

    private sealed class ConnectionResult {
        data class Failure(val exception: Throwable) : ConnectionResult()
        data class Success(val response: ConnectionState) : ConnectionResult()
    }

    private sealed class PsuCyclingState {
        data object NotCycled : PsuCyclingState()
        data object Cycled : PsuCyclingState()
        data object Cycling : PsuCyclingState()
    }

    private data class PsuState(
        val psuIsOn: Boolean?,
        val psuIsAvailable: Boolean,
        val psuNeedsConfiguration: Boolean
    )

    private fun createInitializingState() = UiState.Initializing.copy(
        title = getString("connect_printer___searching_for_octoprint_title")
    )

    private fun createOctoPrintStartingState() = UiState(
        title = getString("connect_printer___octoprint_starting_title"),
        detail = null,
        avatarState = AvatarState.Swim,
    )

    private fun createOctoPrintNotAvailableState(exception: Throwable, url: Url) = UiState(
        title = getString("connect_printer___octoprint_not_available_title"),
        detail = "${url.withoutBasicAuth().withoutQuery()}\n\n" + getString("connect_printer___octoprint_not_available_detail"),
        avatarState = AvatarState.Idle,
        exception = exception,
        isNotAvailable = true,
    )

    private fun createWaitingForPrinterToComeOnlineState(psuIsOn: Boolean?, psuIsAvailable: Boolean, psuNeedsConfiguration: Boolean) = UiState(
        title = getString("connect_printer___waiting_for_printer_title"),
        detail = null,
        avatarState = AvatarState.Swim,
        primaryAction = when (psuIsOn) {
            true -> false
            else -> true
        },
        action = when (psuNeedsConfiguration) {
            true -> getString("connect_printer___action_configure_psu")
            false -> when (psuIsAvailable) {
                false -> null
                true -> when (psuIsOn) {
                    true -> getString("connect_printer___action_turn_psu_off")
                    null, false -> getString("connect_printer___action_turn_psu_on")
                }
            }
        },
        actionType = when (psuNeedsConfiguration) {
            true -> ActionType.ConfigurePsu
            false -> when (psuIsAvailable) {
                false -> null
                true -> when (psuIsOn) {
                    true -> ActionType.TurnPsuOff
                    null, false -> ActionType.TurnPsuOn
                }
            }
        }
    )

    private fun createWaitingForUserState(wasAutoConnectInfoShown: Boolean) = UiState(
        title = getString("connect_printer___waiting_for_user_title"),
        detail = getString("connect_printer___waiting_for_user_subtitle"),
        avatarState = AvatarState.Idle,
        action = getString("connect_printer___begin_connection"),
        actionType = when (wasAutoConnectInfoShown) {
            true -> ActionType.BeginConnect
            else -> ActionType.AutoConnectTutorial
        }
    )

    private fun createConnectingState() = UiState(
        title = getString("connect_printer___printer_is_connecting_title"),
        detail = null,
        avatarState = AvatarState.Swim,
    )

    private fun createOfflineState(psuSupported: Boolean) = UiState(
        title = getString("connect_printer___printer_offline_title"),
        detail = getString(if (psuSupported) "connect_printer___printer_offline_detail_with_psu" else "connect_printer___printer_offline_detail"),
        avatarState = AvatarState.Idle,
        action = when (psuSupported) {
            true -> getString("connect_printer___action_cycle_psu")
            false -> getString("connect_printer___action_retry")
        },
        actionType = when (psuSupported) {
            true -> ActionType.CyclePsu
            false -> ActionType.Retry
        }
    )

    private fun createCyclingState() = UiState(
        title = getString("connect_printer___psu_cycling_title"),
        detail = null,
        avatarState = AvatarState.Swim,
    )

    private fun createConnectedState() = UiState(
        title = getString("connect_printer___printer_connected_title"),
        detail = getString("connect_printer___printer_connected_detail_1"),
        avatarState = AvatarState.Party,
    )
}