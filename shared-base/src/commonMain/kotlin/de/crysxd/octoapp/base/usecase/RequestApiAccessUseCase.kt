package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.BackendType
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyStatus
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.ktor.http.Url
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.isActive

class RequestApiAccessUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<RequestApiAccessUseCase.Params, Flow<RequestApiAccessUseCase.State>>() {

    companion object {
        private const val POLLING_INTERVAL = 500L
        private const val MAX_FAILURES_BEFORE_RESET = 2
    }

    override suspend fun doExecute(param: Params, logger: Logger): Flow<State> {
        val octoprint = printerEngineProvider.createAdHocPrinter(
            instance = PrinterConfigurationV3(id = "adhoc", webUrl = param.webUrl, apiKey = "", type = param.type),
            logTag = logger.httpTag,
        )
        val api = octoprint.asOctoPrint().applicationKeysApi

        return flow {
            emit(State.Pending())

            // Check if supportedr
            if (!api.probe()) {
                emit(State.Failed(IllegalStateException("Unable to probe AccessRequest plugin")))
                return@flow
            }

            while (currentCoroutineContext().isActive) {
                val request = api.request(getString("app_name"))
                logger.i("authUrl=${request.authUrl}")
                emit(State.Pending(authUrl = request.authUrl))

                var consecutiveFailures = 0
                while (currentCoroutineContext().isActive) {
                    // Polling interval
                    delay(POLLING_INTERVAL)

                    try {
                        when (val status = api.checkStatus(request.appToken)) {
                            ApplicationKeyStatus.Pending -> Unit

                            ApplicationKeyStatus.DeniedOrTimedOut -> {
                                // Wait a polling interval and restart
                                delay(POLLING_INTERVAL)
                                break
                            }

                            is ApplicationKeyStatus.Granted -> {
                                emit(State.AccessGranted(apiKey = status.apiKey))
                                return@flow
                            }
                        }
                        consecutiveFailures = 0
                    } catch (e: Exception) {
                        consecutiveFailures++
                        if (consecutiveFailures >= MAX_FAILURES_BEFORE_RESET) {
                            delay(POLLING_INTERVAL)
                            break
                        }
                    }
                }
            }
        }.retry(2).catch {
            emit(State.Failed(it))
        }
    }

    data class Params(
        val webUrl: Url,
        val type: BackendType,
    )

    sealed class State {
        data class Pending(val authUrl: String? = null) : State()
        data class Failed(val exception: Throwable) : State()
        data class AccessGranted(val apiKey: String) : State()
    }
}