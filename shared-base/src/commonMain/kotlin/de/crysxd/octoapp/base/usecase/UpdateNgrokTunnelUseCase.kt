package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol

class UpdateNgrokTunnelUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<UpdateNgrokTunnelUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        val tunnelHost = (param as? Params.Tunnel)?.tunnel?.let {
            if (it.startsWith("http")) it.toUrl().host else it
        } ?: printerEngineProvider.printer(param.instanceId).asOctoPrint().ngrokApi.getActiveTunnel().tunnel
        val setting = printerConfigurationRepository.get(param.instanceId)?.settings?.plugins?.ngrok
            ?: printerEngineProvider.printer(param.instanceId).settingsApi.getSettings().plugins.ngrok
            ?: let {
                logger.w("No ngrok config found, aborting")
                return@doExecute
            }
        val tunnelUrl = tunnelHost?.takeUnless { it.isBlank() }?.let { th ->
            URLBuilder().apply {
                protocol = URLProtocol.HTTPS
                host = th
                user = setting?.authName
                password = setting?.authPassword
            }.build()
        }
        logger.i("$tunnelHost => $tunnelUrl")
        require(tunnelUrl == null || tunnelUrl.isNgrokUrl()) { "Whatever we did...it's wrong. Result not an ngrok URL" }

        printerConfigurationRepository.update(id = param.instanceId) {
            // We only update if we have no remote connection or it's already a ngrok URL
            if (it.alternativeWebUrl == null || it.alternativeWebUrl.isNgrokUrl()) {
                logger.i("Update ngrok tunnel to $tunnelUrl")
                OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "ngrok")
                it.copy(
                    alternativeWebUrl = tunnelUrl,
                    octoEverywhereConnection = null,
                    remoteConnectionFailure = it.remoteConnectionFailure.takeIf { tunnelUrl == null },
                    alternativeWebUrlPlugin = OctoPlugins.Ngrok,
                )
            } else {
                logger.i("Not configured for ngrok, will not touch (url=${it.alternativeWebUrl})")
                it
            }
        }
    }

    sealed class Params {
        abstract val instanceId: String

        data class FetchConfig(override val instanceId: String) : Params()
        data class Tunnel(val tunnel: String?, override val instanceId: String) : Params()
    }
}