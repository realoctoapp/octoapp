package de.crysxd.octoapp.base.models

import kotlinx.serialization.Serializable

@Serializable
data class GcodePoint(
    val x: Float,
    val y: Float
)