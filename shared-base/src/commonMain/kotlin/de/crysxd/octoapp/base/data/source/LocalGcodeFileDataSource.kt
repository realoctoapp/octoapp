package de.crysxd.octoapp.base.data.source

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.models.Gcode
import de.crysxd.octoapp.base.models.GcodeLayer
import de.crysxd.octoapp.base.models.GcodeLayerInfo
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.io.FileManager
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.io.getSerializableOrNull
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import okio.IOException
import okio.buffer
import okio.use
import kotlin.math.absoluteValue

@OptIn(ExperimentalSerializationApi::class)
class LocalGcodeFileDataSource(
    private val fileManager: FileManager,
    private val settingsStore: SettingsStore,
    private val octoPreferences: OctoPreferences,
) : GcodeFileDataSource {
    private val tag = "LocalGcodeFileDataSource"
    private val cacheVersion = 1
    private val files = createFileManagerNameSpace(cacheVersion)
    private val index = createIndex(cacheVersion)
    private val mutex = Mutex()
    private val memoryCache = mutableMapOf<Pair<String, Int>, Pair<Instant, GcodeLayer>>()
    private val serializer = ProtoBuf {}
    private val initJob: Job
    private val sizeCheck = LocalCacheSizeCheck(
        files = files,
        maxSize = { octoPreferences.gcodeCacheSize },
        tag = tag,
        ageForKey = { key -> getCacheEntry(key)?.cachedAt },
        index = {
            // We got some junk from iOS, so we need to filter
            index.keys.filter { key -> key.all { it.isDigit() } }
        },
        removeFromCache = { key -> removeFromCache(key) },
        validFile = { name ->
            val key = name.split(".").firstOrNull() ?: return@LocalCacheSizeCheck false
            val indexExists = files.exists(key.indexFileName)
            val dataExists = files.exists(key.dataFileName)

            if (indexExists && dataExists) {
                true
            } else {
                Napier.w(tag = tag, message = "[GCD] Missing files for $key: indexExists=$indexExists dataExists=$dataExists")
                false
            }
        }
    )

    init {
        //region Clean up old caches and ensure size
        initJob = AppScope.launch {
            (0 ..< cacheVersion).forEach { cacheVersion ->
                Napier.v(tag = tag, message = "Clearing data for v$cacheVersion")
                createFileManagerNameSpace(cacheVersion).clear()
                createIndex(cacheVersion).clear()
            }

            AppScope.launch(Dispatchers.SharedIO) {
                checkCacheSize()
            }
        }
        //endregion
    }

    fun createCacheForFile(file: FileReference.File) = CacheContext(file, this)

    fun canLoadFile(file: FileReference.File): Boolean {
        val cacheKey = getCacheKey(file, logCacheKey = true)
        val cached = getCacheEntry(cacheKey)?.let {
            // Cache hit if the file exists and the file was not changed at the server since it was cached
            files.exists(cacheKey.dataFileName) && files.exists(cacheKey.indexFileName) && it.fileDate == file.date.toEpochMilliseconds()
        } == true

        Napier.i(tag = tag, message = "[GCD] Can load $cacheKey: $cached")
        return cached
    }

    fun loadFile(file: FileReference.File): Flow<GcodeFileDataSource.LoadState> = flow {
        val cacheKey = getCacheKey(file)
        Napier.i(tag = tag, message = "[GCD] Loading file for: $cacheKey")
        try {
            emit(GcodeFileDataSource.LoadState.Loading(0f))
            initJob.join()

            val gcode = try {
                readIndexFile(cacheKey)
            } catch (e: Error) {
                // We've seen an OutOfMemeoryError here before
                throw IOException("Failed to load index: ${e::class.qualifiedName}: ${e.message}")
            }

            Napier.i(
                tag = tag,
                message = "[GCD] Metadata for $cacheKey: layerCount=${gcode.layers.size} maxX=${gcode.maxX} minX=${gcode.minX} maxY=${gcode.maxY} minY=${gcode.minY}"
            )

            emit(GcodeFileDataSource.LoadState.Ready(gcode))
        } catch (e: CancellationException) {
            // Nothing to do..
        } catch (e: Exception) {
            // Cache most likely corrupted, clean out
            Napier.e(tag = tag, message = "[GCD] Error while loading cache entry for $cacheKey", throwable = e)
            removeFromCache(file)
            emit(GcodeFileDataSource.LoadState.Failed(e))
        }
    }.flowOn(Dispatchers.SharedIO)

    suspend fun loadLayer(cacheKey: CacheKey, layerInfo: GcodeLayerInfo) = mutex.withLock {
        try {
            val key = cacheKey to layerInfo.positionInFile
            val layer = memoryCache.getOrPut(key) {
                Napier.d(tag = tag, message = "[GCD] Loading layer: $cacheKey/${layerInfo.zHeight}mm")
                files.readCacheFile(cacheKey.dataFileName).use { f ->
                    val bytes = ByteArray(layerInfo.lengthInCacheFile)
                    f.read(fileOffset = layerInfo.positionInCacheFile, array = bytes, byteCount = layerInfo.lengthInCacheFile, arrayOffset = 0)
                    Clock.System.now() to serializer.decodeFromByteArray(bytes)
                }
            }.second

            // Drop entries from cache until at most 3 are left
            while (memoryCache.size > 3) {
                memoryCache.remove(memoryCache.entries.minBy { (_, value) -> value.first }.key)
            }

            // Update last used time
            memoryCache[key] = Clock.System.now() to layer

            layer
        } catch (e: Exception) {
            removeFromCacheWithLock(cacheKey)
            Napier.e(tag = tag, message = "[GCD] Failed to load $cacheKey @ ${layerInfo.zHeight}mm, removing", throwable = e)
            throw e
        }
    }

    suspend fun removeFromCache(file: FileReference.File) = mutex.withLock {
        removeFromCache(getCacheKey(file))
    }

    suspend fun removeFromCache(cacheKey: CacheKey) = mutex.withLock {
        removeFromCacheWithLock(cacheKey)
    }

    private fun removeFromCacheWithLock(cacheKey: CacheKey) {
        Napier.i(tag = tag, message = "[GCD] Removing from cache: $cacheKey")
        files.deleteCacheFile(cacheKey.dataFileName)
        files.deleteCacheFile(cacheKey.indexFileName)
        index.remove(cacheKey)
    }

    fun clear() {
        Napier.i(tag = tag, message = "[GCD] Clearing cache")
        files.clear()
        index.clear()
    }

    fun totalSize(): Long = files.totalSize()

    suspend fun checkCacheSize() = sizeCheck.checkSize()

    private fun readIndexFile(cacheKey: CacheKey): Gcode = files.readCacheFile(cacheKey.indexFileName).use { indexFile ->
        val indexBytes = ByteArray(indexFile.size().toInt())
        indexFile.read(fileOffset = 0, array = indexBytes, arrayOffset = 0, byteCount = indexBytes.size)
        serializer.decodeFromByteArray(indexBytes)
    }

    private fun getCacheEntry(cacheKey: CacheKey): CacheEntry? = index.getSerializableOrNull(cacheKey)

    private fun getCacheKey(file: FileReference.File, logCacheKey: Boolean = false): String {
        val cacheKey = "${file.origin}:${file.path}:${file.date}".hashCode().absoluteValue.toString()
        if (logCacheKey) {
            Napier.i(tag = tag, message = "${file.origin}:${file.path}:${file.date} => $cacheKey")
        }
        return cacheKey
    }

    private fun createFileManagerNameSpace(cacheVersion: Int) = fileManager.forNameSpace("gcode-cache-v$cacheVersion")
    private fun createIndex(cacheVersion: Int) = settingsStore.forNameSpace("gcode-cache-v$cacheVersion")

    @Serializable
    data class CacheEntry(
        val cachedAt: Long,
        val fileDate: Long,
    )

    data class CacheContext(
        private val file: FileReference.File,
        private val dataSource: LocalGcodeFileDataSource
    ) {
        private val tag = "CacheContext"
        private var dataBytesWritten = 0L
        private val cacheKey = dataSource.getCacheKey(file)
        private val dataFile = dataSource.files.writeCacheFile(cacheKey.dataFileName)
        private val dataSink = dataFile.sink(fileOffset = 0).buffer()

        init {
            val cacheEntry = CacheEntry(cachedAt = Clock.System.now().toEpochMilliseconds(), fileDate = file.date.toEpochMilliseconds())
            dataSource.index.putSerializable(cacheKey, cacheEntry)
            Napier.i(tag = tag, message = "[GCD] Cache context for ${file.path} initialized")
        }

        fun cacheLayer(layer: GcodeLayer): GcodeLayer {
            val positionInCacheFile = dataBytesWritten
            val bytes = dataSource.serializer.encodeToByteArray(layer)
            dataSink.write(bytes)
            dataBytesWritten += bytes.size

            // Upgrade layer with info where in the cache the file is
            return layer.copy(info = layer.info.copy(positionInCacheFile = positionInCacheFile, lengthInCacheFile = bytes.size))
        }

        private suspend fun finalize(gcode: Gcode): Gcode {
            dataSink.close()
            dataFile.close()

            val upgradedGcode = gcode.copy(cacheKey = cacheKey)
            dataSource.files.writeCacheFile(cacheKey.indexFileName).use {
                val data = dataSource.serializer.encodeToByteArray(upgradedGcode)
                it.write(fileOffset = 0, array = data, arrayOffset = 0, byteCount = data.size)
                it.resize(data.size.toLong())
            }

            Napier.i(tag = tag, message = "[GCD] Added to cache: ${file.path} (cacheKey=$cacheKey)")
            dataSource.checkCacheSize()
            return upgradedGcode
        }

        private suspend fun abort() {
            dataSink.close()
            dataFile.close()
            dataSource.removeFromCache(file)
        }

        suspend fun use(block: suspend (CacheContext) -> Gcode): Gcode {
            try {
                try {
                    dataSource.initJob.join()
                    return finalize(block(this))
                } catch (e: Error) {
                    // We've seen an OutOfMemeoryError here before
                    throw IOException("[GCD] Failed to load index: ${e::class.qualifiedName}: ${e.message}")
                }
            } catch (e: Exception) {
                abort()
                Napier.e(tag = tag, message = "[GCD] Failed to use CacheContext", throwable = e)
                throw e
            }
        }
    }
}

typealias CacheKey = String

private val CacheKey.indexFileName get() = "$this.index"
private val CacheKey.dataFileName get() = "$this.bin"