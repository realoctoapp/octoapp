package de.crysxd.octoapp.base.models

sealed class BillingEvent {
    data object PurchaseCompleted : BillingEvent()
}