package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.ext.getStackAddresses
import io.github.aakira.napier.LogLevel
import kotlinx.datetime.TimeZone
import org.koin.ext.getFullName

object FirebaseAntiLog : BaseAntiLog() {

    override val name = "FirebaseAntilLog"
    override val writeTime = false
    override val writeExceptionAsText = false
    override val minPriority = LogLevel.INFO
    override val timeZone get() = TimeZone.currentSystemDefault()
    override val lineLength: Int = Int.MAX_VALUE
    private var adapter: Adapter? = null

    fun initAdapter(adapter: Adapter) {
        this.adapter = adapter
    }

    override suspend fun writeLine(line: String) {
        adapter?.log(line)
    }

    @OptIn(kotlin.experimental.ExperimentalNativeApi::class)
    override suspend fun writeException(t: Throwable) {
        adapter?.log(
            throwableClassName = t::class.getFullName(),
            stackTraceAddresses = t.getStackAddresses(),
            throwable = t
        )
    }

    interface Adapter {
        fun log(line: String)
        fun log(throwableClassName: String, stackTraceAddresses: List<Long>, throwable: Throwable)
    }
}