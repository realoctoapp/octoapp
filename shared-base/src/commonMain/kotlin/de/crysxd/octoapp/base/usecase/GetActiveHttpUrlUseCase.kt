package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map

class GetActiveHttpUrlUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<PrinterConfigurationV3, Flow<Url>>() {

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun doExecute(param: PrinterConfigurationV3, logger: Logger): Flow<Url> {
        val instanceId = param.id
        val octoPrint = printerEngineProvider.printerFlow(param.id)
        return octoPrint.flatMapLatest {
            it ?: return@flatMapLatest emptyFlow()
            // Probe connection to ensure primary or alternative URL is active
            if (printerEngineProvider.getCurrentConnection(instanceId) == null) {
                logger.d("OctoPrint not connected, probing connection")
                it.versionApi.getVersion()
            } else {
                logger.d("OctoPrint connected, relying on active web url")
            }
            it.baseUrl
        }.map {
            it
        }
    }
}