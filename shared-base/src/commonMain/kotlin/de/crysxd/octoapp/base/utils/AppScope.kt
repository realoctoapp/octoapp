package de.crysxd.octoapp.base.utils

import de.crysxd.octoapp.base.logging.AlwaysLoggedIllegalStateException
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import de.crysxd.octoapp.sharedcommon.isDarwin
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

private val appJob = SupervisorJob()
val AppScope = CoroutineScope(appJob + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, throwable ->
    if (throwable !is CancellationException && throwable !is SuppressedException && throwable.cause !is CancellationException && throwable.cause !is SuppressedException) {
        Napier.e(tag = "AppScope", throwable = throwable, message = "Caught NON-CONTAINED exception in AppScope!")

        try {
            // On iOS this should never happen, Android will follow soon
            if (SharedCommonInjector.get().platform.isDarwin()) {
                Napier.e(
                    tag = "AppScope",
                    throwable = AlwaysLoggedIllegalStateException("Uncontained exception in AppScope", throwable),
                    message = "Failed to report non-contained exception in AppScope"
                )
            }
        } catch (e: Exception) {
            Napier.e(tag = "AppScope", throwable = e, message = "Failed to report non-contained exception in AppScope")

        }

        ExceptionReceivers.dispatchException(throwable)
    }
})