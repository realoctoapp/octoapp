package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand

class EmergencyStopUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<EmergencyStopUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        printerEngineProvider.printer(param.instanceId).printerApi.executeGcodeCommand(GcodeCommand.Single("M112"))
    }

    data class Params(
        val instanceId: String
    )
}