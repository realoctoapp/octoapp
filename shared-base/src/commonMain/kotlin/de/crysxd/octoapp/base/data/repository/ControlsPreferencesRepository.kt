package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.models.ControlsPreferences
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.io.getSerializableOrNull
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine

class ControlsPreferencesRepository(
    settingsStore: SettingsStore,
) {
    private val tag = "WidgetPreferencesRepository"
    private val settings = settingsStore.forNameSpace("controls")

    companion object {
        const val LIST_PRINT = "print"
        const val LIST_PREPARE = "prepare"
        const val LIST_CONNECT = "connect"
    }

    private val flows = mutableMapOf<String, MutableStateFlow<ControlsPreferences?>>()

    init {
        migrateQuickAccess(LIST_PREPARE)
        migrateQuickAccess(LIST_PRINT)

        getFlow(LIST_PRINT).value = getWidgetOrder(LIST_PRINT)
        getFlow(LIST_PREPARE).value = getWidgetOrder(LIST_PREPARE)
        getFlow(LIST_CONNECT).value = getWidgetOrder(LIST_CONNECT)
    }

    private fun migrateQuickAccess(listId: String) = getWidgetOrder(listId)?.let { current ->
        @Suppress("DEPRECATION")
        fun List<ControlType>.map() = map {
            if (it in listOf(ControlType.PrintQuickAccessWidget, ControlType.PrepareQuickAccessWidget)) ControlType.QuickAccessWidget else it
        }

        current.copy(
            hidden = current.hidden.map(),
            items = current.items.map()
        )
    }?.let { new ->
        setWidgetOrder(listId, new)
    }

    fun getWidgetOrder(listId: String): ControlsPreferences? =
        settings.getSerializableOrNull<ControlsPreferences>(listId)

    fun getWidgetOrderFlow(listId: String) = getFlow(listId).asStateFlow()

    fun getWidgetOrderFlow() = combine(
        getWidgetOrderFlow(LIST_CONNECT),
        getWidgetOrderFlow(LIST_PREPARE),
        getWidgetOrderFlow(LIST_PRINT),
    ) { connect, prepare, print ->
        mapOf(
            LIST_CONNECT to connect,
            LIST_PREPARE to prepare,
            LIST_PRINT to print,
        )
    }

    private fun getFlow(listId: String) = flows.getOrPut(listId) { MutableStateFlow(getWidgetOrder(listId)) }

    fun setWidgetOrder(listId: String, preferences: ControlsPreferences) {
        Napier.i(tag = tag, message = "Updating widget preferences for $listId: $preferences")
        settings.putSerializable(listId, preferences)
        getFlow(listId).value = preferences
    }
}

fun PrinterState.Flags.asDestinationId() = when {
    isPrinting() -> ControlsPreferencesRepository.LIST_PRINT
    isOperational() -> ControlsPreferencesRepository.LIST_PREPARE
    else -> ControlsPreferencesRepository.LIST_CONNECT
}
