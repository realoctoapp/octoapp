package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withTimeoutOrNull

suspend fun PrinterEngineProvider.awaitFileChange(instanceId: String?, timeoutMs: Long = 1000) = withTimeoutOrNull(timeoutMs) {
    passiveEventFlow(instanceId = instanceId).first {
        it is Event.MessageReceived && it.message is Message.Event.UpdatedFiles
    }
} != null