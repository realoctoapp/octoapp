package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.source.LocalCacheSizeCheck
import de.crysxd.octoapp.base.usecase.ClearPublicFilesUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.io.FileManager
import io.github.aakira.napier.Napier
import io.ktor.utils.io.ByteReadChannel
import kotlinx.coroutines.launch
import okio.BufferedSource
import okio.Path
import okio.buffer
import okio.use
import kotlin.math.absoluteValue

class MediaFileRepository(
    fileManager: FileManager,
    val preferences: OctoPreferences,
) {
    private val tag = "MediaFileRepository"
    private val privateFiles = fileManager.forNameSpace(nameSpace = "media-files")
    private val eternalFiles = fileManager.forNameSpace(nameSpace = "eternal-media-files")
    private val publicFiles = fileManager.forNameSpace(nameSpace = ClearPublicFilesUseCase.PublicFilesNameSpace)
    private val sizeCheck = LocalCacheSizeCheck(
        files = privateFiles,
        maxSize = { preferences.mediaCacheSize },
        tag = tag,
        ageForKey = { key -> privateFiles.metadataOrNull(key)?.lastAccessedAtMillis },
        index = { privateFiles.list() },
        removeFromCache = { key -> privateFiles.deleteCacheFile(key) },
        validFile = { true }
    )

    val totalSize get() = privateFiles.totalSize()

    init {
        // Reset public files and clean up private
        AppScope.launch {
            publicFiles.clear()
            checkCacheSize()
        }
    }

    fun clear() {
        privateFiles.clear()
        publicFiles.clear()
    }

    suspend fun checkCacheSize() = sizeCheck.checkSize()

    suspend fun addFile(key: String, byteReadChannel: ByteReadChannel, eternal: Boolean = false) {
        val files = if (eternal) eternalFiles else privateFiles

        try {
            Napier.i(tag = tag, message = "Adding file $key (${key.cacheFileName})")
            var counter = 0L

            files.deleteCacheFile(key.cacheFileName)
            files.writeCacheFile(key.cacheFileName).appendingSink().buffer().use { sink ->
                val buffer = ByteArray(8192)
                do {
                    byteReadChannel.awaitContent()
                    val byteCount = byteReadChannel.readAvailable(buffer, 0, buffer.size)
                    counter += byteCount
                    sink.write(source = buffer, offset = 0, byteCount = byteCount.coerceAtLeast(0))
                } while (byteCount >= 0)
            }

            Napier.i(tag = tag, message = "File $key added with ${counter.formatAsFileSize()} (${key.cacheFileName})")
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to add file $key (${key.cacheFileName})", throwable = e)
            files.deleteCacheFile(key.cacheFileName)
            throw e
        } finally {
            checkCacheSize()
        }
    }

    fun isCached(key: String) = privateFiles.exists(key.cacheFileName) || eternalFiles.exists(key.cacheFileName)

    fun getFilePath(key: String): Path? = when {
        eternalFiles.exists(key.cacheFileName) -> eternalFiles.getPath(key.cacheFileName)
        privateFiles.exists(key.cacheFileName) -> privateFiles.getPath(key.cacheFileName)
        else -> null
    }

    suspend fun readFile(key: String, block: suspend (BufferedSource) -> Unit) {
        val files = if (eternalFiles.exists(key.cacheFileName)) eternalFiles else privateFiles
        Napier.i(tag = tag, message = "Reading $key (${key.cacheFileName})")
        files.readCacheFile(key.cacheFileName).source().buffer().use { block(it) }
    }

    suspend fun prepareFileForPublicSharing(key: String, name: String): Path = try {
        Napier.i(tag = tag, message = "Preparing file for public sharing: $key -> $name (${key.cacheFileName})")
        publicFiles.clear()

        // Copy to public
        publicFiles.writeCacheFile(name).appendingSink().use { sink ->
            readFile(key = key) { source ->
                source.readAll(sink)
            }
            sink.flush()
        }

        Napier.i(tag = tag, message = "File $key (${key.cacheFileName}) is ready")
        publicFiles.getPath(name)
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to prepare file for sharing $key (${key.cacheFileName})", throwable = e)
        publicFiles.clear()
        throw e
    }

    private val String.cacheFileName get() = hashCode().absoluteValue.toString()
}