package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.io.getSerializable
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

class MacroGroupsRepository(
    settingsStore: SettingsStore,
) {
    companion object {
        const val GroupHidden = "__hidden"
        const val GroupDefault = "__default"
    }

    private val tag = "MacroGroupsRepository"
    private val groupIndexKey = "index"
    private val macroKey = "macro:"
    private val defaults = listOf(GroupDefault, GroupHidden)
    private val separator = "^"
    private val settings = settingsStore.forNameSpace("macro-groups")
    private val comparator = compareBy<String> { it != GroupDefault }.thenBy { it }
    private val mutableUpdatedFlow = MutableStateFlow(0)
    val updateFlow = mutableUpdatedFlow.asStateFlow().map { }

    fun getGroups(): List<String> = settings.getSerializable(groupIndexKey, defaults).sortedWith(
        compareBy<String> {
            it.splitToLabel().first
        }.thenBy {
            it.splitToLabel().second
        }
    )

    fun setGroups(groups: List<String>) {
        Napier.i(tag = tag, message = "Settings groups: $groups")
        settings.putSerializable(groupIndexKey, groups)
        mutableUpdatedFlow.update { it + 1 }
    }

    fun getGroup(macroId: String): String = settings.getString("$macroKey$macroId", GroupDefault)

    fun setGroup(macroId: String, group: String) {
        Napier.i(tag = tag, message = "Settings group: $macroId -> $group")
        settings.putString("$macroKey$macroId", group)
        mutableUpdatedFlow.update { it + 1 }
    }

    fun getAllMacros() = settings.keys.filter {
        it.startsWith(macroKey)
    }.map { key ->
        key.removePrefix(macroKey) to settings.getString(key, "")
    }.filter { (_, value) ->
        value.isNotBlank()
    }

    fun isReservedGroup(group: String) = group in defaults

    fun getLabelFor(group: String) = when (group) {
        GroupHidden -> getString("widget_gcode_send___group_hidden")
        GroupDefault -> getString("widget_gcode_send___group_default")
        else -> group
    }.splitToLabel()

    private fun String.splitToLabel(): Pair<Int, String> {
        val regex = Regex("(#-?\\d+ +)?(.+)")
        val result = regex.find(this)?.groupValues
        val label = result?.getOrNull(2) ?: this
        val index = result?.getOrNull(1)?.trim('#', ' ')?.toIntOrNull() ?: Int.MAX_VALUE
        return index to label
    }
}