package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.api.PrinterApi

class SetTargetTemperaturesUseCase(
    printerEngineProvider: PrinterEngineProvider,
    getCurrentPrinterProfileUseCase: GetCurrentPrinterProfileUseCase,
) : BaseChangeTemperaturesUseCase(
    printerEngineProvider = printerEngineProvider,
    getCurrentPrinterProfileUseCase = getCurrentPrinterProfileUseCase
) {
    override suspend fun applyTemperatures(printerApi: PrinterApi, temperatures: Map<String, Float>) {
        printerApi.setTemperatureTargets(targets = temperatures)
    }
}
