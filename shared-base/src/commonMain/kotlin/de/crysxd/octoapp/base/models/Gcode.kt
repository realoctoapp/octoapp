package de.crysxd.octoapp.base.models

import kotlinx.serialization.Serializable

@Serializable
data class Gcode(
    val layers: List<GcodeLayerInfo>,
    val cacheKey: String,
    val minX: Float,
    val maxX: Float,
    val minY: Float,
    val maxY: Float,
) {
    val bounds get() = GcodeRect(minX, maxY, maxX, minY)
}