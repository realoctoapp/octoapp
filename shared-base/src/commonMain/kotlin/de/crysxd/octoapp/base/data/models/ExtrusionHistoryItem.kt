package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class ExtrusionHistoryItem(
    val distanceMm: Int,
    val lastUsed: Long = 0,
    val isFavorite: Boolean = false,
    val usageCount: Int = 0,
) : CommonParcelable