package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.JobCommand

class TogglePausePrintJobUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<TogglePausePrintJobUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        val octoPrint = printerEngineProvider.printer(param.instanceId)
        val isPaused = printerEngineProvider.getLastCurrentMessage(param.instanceId)?.state?.flags?.paused == true
        octoPrint.jobApi.executeJobCommand(if (isPaused) JobCommand.ResumeJobCommand else JobCommand.PauseJobCommand)
    }

    data class Params(
        val instanceId: String? = null
    )
}