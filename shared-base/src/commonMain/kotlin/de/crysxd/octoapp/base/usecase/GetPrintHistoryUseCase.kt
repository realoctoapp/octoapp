package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrintHistoryRepository
import de.crysxd.octoapp.base.models.JobHistoryItem
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.FlowState
import kotlinx.coroutines.flow.Flow

class GetPrintHistoryUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printHistoryRepository: PrintHistoryRepository,
) : UseCase2<GetPrintHistoryUseCase.Params, Flow<FlowState<List<JobHistoryItem>>>>() {

    override suspend fun doExecute(param: Params, logger: Logger) = printHistoryRepository.observeJobHistory(
        instanceId = param.instanceId,
        skipCache = param.skipCache
    )

    data class Params(
        val instanceId: String,
        val skipCache: Boolean,
    )
}