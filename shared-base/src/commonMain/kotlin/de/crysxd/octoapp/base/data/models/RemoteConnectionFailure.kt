package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class RemoteConnectionFailure(
    val errorMessage: String,
    val errorMessageStack: String,
    val stackTrace: String,
    val remoteServiceName: String,
    val dateMillis: Long = 0,
    val message: String? = null,
    val learnMoreUrl: String? = null,
)
