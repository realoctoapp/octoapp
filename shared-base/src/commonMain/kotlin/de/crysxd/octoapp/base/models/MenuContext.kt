package de.crysxd.octoapp.base.models

import de.crysxd.octoapp.engine.models.printer.PrinterState

enum class MenuContext {
    Connect,
    Prepare,
    Print,
    PrintPaused,
    Widget;

    companion object {
        fun fromFlags(flags: PrinterState.Flags?) = when {
            flags?.isPrinting() == true -> if (flags.paused) PrintPaused else Print
            flags?.isOperational() == true -> Prepare
            else -> Connect
        }
    }
}
