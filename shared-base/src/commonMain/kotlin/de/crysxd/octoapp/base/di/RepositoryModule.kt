package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.data.repository.MacroGroupsRepository
import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.data.repository.PrintHistoryRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.data.repository.TutorialsRepository
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedexternalapis.tutorials.TutorialApi
import org.koin.dsl.module


class RepositoryModule : BaseModule {

    override val koinModule = module {
        single { ExtrusionHistoryRepository(get(), get()) }
        single { GcodeHistoryRepository(get(), get()) }
        single { PrinterConfigurationRepository(get(), get(), get(), get<Platform>().run { !isAndroid() && slaveApp }) }
        single { OctoPreferences(get(), get()) }
        single { ControlsPreferencesRepository(get()) }
        single { FileListRepository(get(), get()) }
        single { PrintHistoryRepository(get(), get()) }
        single { SerialCommunicationLogsRepository(get(), get()) }
        single { TemperatureDataRepository(get(), get()) }
        single { TutorialsRepository(TutorialApi(), get(), get()) }
        single { PinnedMenuItemRepository(get()) }
        single { TimelapseRepository(get(), get()) }
        single { GcodeFileRepository(get(), get()) }
        single { MediaFileRepository(get(), get()) }
        single { MacroGroupsRepository(get()) }
    }
}