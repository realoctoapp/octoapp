package de.crysxd.octoapp.base.billing

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.BillingProduct
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.isDarwin
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.post
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

private const val Host = "https://europe-west1-octoapp-4e438.cloudfunctions.net"
private const val Tag = "BillingPurchaseTransfer"

private val http
    get() = DefaultHttpClient(HttpClientSettings(logTag = "$Tag/HTTP", logLevel = LogLevel.Debug)) {
        install(ContentNegotiation) {
            json(Json)
        }
    }

private val platform get() = if (SharedCommonInjector.get().platform.isDarwin()) "darwin" else "android"

private fun BillingManager.getTransferablePurchase() = state.value.purchases.firstOrNull {
    !it.isSubscription && it.productId != "tip" && !it.productId.contains("transfer")
}

fun BillingManager.canTransfer(): Boolean = getTransferablePurchase()?.token != null

suspend fun BillingManager.announceTransfer(): BillingProductTransferAnnounceResult = try {
    Napier.i(tag = Tag, message = "Announcing transfer")
    //region Announce transfer
    val purchase = getTransferablePurchase() ?: throw UnableToTransferSubscriptionException()
    requireNotNull(purchase.token)
    val response = http.post("$Host/transferPurchaseAnnounce") {
        setJsonBody(
            BillingProductTransferAnnounceRequestBody(
                sourcePlatform = platform,
                productId = purchase.productId,
                token = purchase.token,
                transactionId = purchase.transactionId
            )
        )
    }
    val transferId = when (response.status) {
        HttpStatusCode.OK -> response.body<BillingPurchaseTransferAnnounceResponseBody>().transferId
        HttpStatusCode.Forbidden -> throw PurchaseAlreadyTransferredException()
        else -> throw IllegalStateException("${response.status} is not a valid response code")
    }.also {
        Napier.i(tag = Tag, message = "Native product: $it")
    }
    //endregion
    Napier.i(tag = Tag, message = "Creating transfer url: $transferId")
    BillingProductTransferAnnounceResult.Success(UriLibrary.getPurchaseUri(transferId = transferId).toString())
} catch (e: PurchaseMustBeTransferredToOtherPlatformException) {
    Napier.w(tag = Tag, message = "Attempting to transfer purchase to same platform", throwable = e)
    BillingProductTransferAnnounceResult.Error(e)
} catch (e: PurchaseAlreadyTransferredException) {
    Napier.w(tag = Tag, message = "Attempting to transfer purchase twice", throwable = e)
    BillingProductTransferAnnounceResult.Error(e)
} catch (e: UnableToTransferSubscriptionException) {
    Napier.w(tag = Tag, message = "Attempting to transfer subscription", throwable = e)
    BillingProductTransferAnnounceResult.Error(e)
} catch (e: Exception) {
    Napier.e(tag = Tag, message = "Failed to transfer product", throwable = e)
    BillingProductTransferAnnounceResult.Error(PurchaseTransferFailedException(e))
}

suspend fun BillingManager.redeemTransfer(transferId: String): BillingProductTransferRedeemResult = try {
    Napier.i(tag = Tag, message = "Transferring product: $transferId")
    //region Redeem transfer
    val http = DefaultHttpClient(HttpClientSettings(logTag = "$Tag/HTTP", logLevel = LogLevel.Debug)) {
        install(ContentNegotiation) {
            json(Json)
        }
    }
    val response = http.post("$Host/transferPurchaseRedeem") {
        setJsonBody(
            BillingProductTransferRedeemRequestBody(
                transferId = transferId,
                targetPlatform = platform
            )
        )
    }
    val transferProductId = when (response.status) {
        HttpStatusCode.OK -> response.body<BillingPurchaseTransferRedeemResponseBody>().transferProductId
        HttpStatusCode.Forbidden -> throw PurchaseAlreadyTransferredException()
        else -> throw IllegalStateException("${response.status} is not a valid response code")
    }.also {
        Napier.i(tag = Tag, message = "Native product: $it")
    }
    val product = state.value.products.firstOrNull { it.productId == transferProductId } ?: let {
        Napier.e(
            tag = Tag,
            message = "Failed to transfer product",
            throwable = IllegalStateException("No such product: $transferProductId (${state.value.products.map { it.productId }})")
        )
        // Throw up generic
        throw IllegalStateException()
    }
    //endregion
    SharedBaseInjector.get().preferences.didReceiveTransfer = true
    BillingProductTransferRedeemResult.Success(product = product)
} catch (e: PurchaseMustBeTransferredToOtherPlatformException) {
    Napier.w(tag = Tag, message = "Attempting to transfer purchase to same platform", throwable = e)
    BillingProductTransferRedeemResult.Error(e)
} catch (e: PurchaseAlreadyTransferredException) {
    Napier.w(tag = Tag, message = "Attempting to transfer purchase twice", throwable = e)
    BillingProductTransferRedeemResult.Error(e)
} catch (e: Exception) {
    Napier.e(tag = Tag, message = "Failed to transfer product", throwable = e)
    BillingProductTransferRedeemResult.Error(PurchaseTransferFailedException(e))
}

@Serializable
private data class BillingProductTransferRedeemRequestBody(
    val targetPlatform: String,
    val transferId: String
)

@Serializable
private data class BillingPurchaseTransferRedeemResponseBody(
    val success: Boolean,
    val transferProductId: String,
)

@Serializable
private data class BillingProductTransferAnnounceRequestBody(
    val sourcePlatform: String,
    val productId: String,
    val token: String,
    val transactionId: String,
)

@Serializable
private data class BillingPurchaseTransferAnnounceResponseBody(
    val success: Boolean,
    val transferId: String,
)

sealed class BillingProductTransferAnnounceResult {
    data class Error(val exception: Throwable) : BillingProductTransferAnnounceResult()
    data class Success(val url: String) : BillingProductTransferAnnounceResult()
}

sealed class BillingProductTransferRedeemResult {
    data class Error(val exception: Throwable) : BillingProductTransferRedeemResult()
    data class Success(val product: BillingProduct) : BillingProductTransferRedeemResult()
}

private class PurchaseAlreadyTransferredException : SuppressedIllegalStateException("Product is already transferred"), UserMessageException {
    override val userMessage = getString("billing_transfer___error_already_transferred")
}

private class PurchaseMustBeTransferredToOtherPlatformException : SuppressedIllegalStateException("Purchases must be transferred to another platform"),
    UserMessageException {
    override val userMessage = getString("billing_transfer___error_same_platform")
}

private class PurchaseTransferFailedException(cause: Exception) : Exception(cause), UserMessageException {
    override val userMessage = getString("billing_transfer___error_general")
}

private class UnableToTransferSubscriptionException : Exception(), UserMessageException {
    override val userMessage = getString("billing_transfer___error_subscription")
}