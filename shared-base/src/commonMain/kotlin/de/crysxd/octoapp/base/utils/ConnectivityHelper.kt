package de.crysxd.octoapp.base.utils

import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.onEach

class ConnectivityHelper {

    private val tag = "ConnectivityHelper"
    val connectivityState = connectivityFlow.onEach {
        Napier.i(tag = tag, message = "Connection updated: $it")
    }

    enum class ConnectivityType {
        Metered, Unmetered
    }
}

internal expect val connectivityFlow: Flow<ConnectivityHelper.ConnectivityType>