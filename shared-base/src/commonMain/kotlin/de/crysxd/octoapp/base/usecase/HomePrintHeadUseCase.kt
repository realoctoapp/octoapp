package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand

class HomePrintHeadUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<HomePrintHeadUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        printerEngineProvider.printer(param.instanceId).printerApi.executePrintHeadCommand(param.axis.printHeadCommand)
    }

    sealed class Axis(val printHeadCommand: PrintHeadCommand) {
        data object All : Axis(PrintHeadCommand.HomeAllAxisPrintHeadCommand)
        data object XY : Axis(PrintHeadCommand.HomeXYAxisPrintHeadCommand)
        data object Z : Axis(PrintHeadCommand.HomeZAxisPrintHeadCommand)
    }

    data class Params(
        val axis: Axis,
        val instanceId: String?,
    )
}