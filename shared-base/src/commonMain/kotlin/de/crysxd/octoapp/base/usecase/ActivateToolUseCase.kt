package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider

class ActivateToolUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<ActivateToolUseCase.Params, String?>() {

    override suspend fun doExecute(param: Params, logger: Logger): String? {
        val instance = printerConfigurationRepository.get(param.instanceId) ?: return let {
            logger.w("No information found for ${param.instanceId}")
            null
        }
        val toolCount = instance.activeProfile?.extruderMotorComponents?.size ?: 1
        val activeTool = (instance.appSettings?.activeToolIndex)
        val toolComponent = activeTool?.let { instance.activeProfile?.extruderMotorComponents?.get(it) }

        if (activeTool == null) {
            logger.v("Skipping activation of tool $activeTool as activation is null")
        } else if (toolComponent == null) {
            logger.w("Active tool is $toolComponent but only following are available: ${instance.activeProfile?.extruderMotorComponents}")
            return null
        } else if (toolCount > 1) {
            logger.d("Activating tool $activeTool of $toolCount: $toolComponent")
            printerEngineProvider.printer(instanceId = param.instanceId).printerApi.selectExtruder(component = toolComponent)
        } else {
            logger.v("Skipping activation of tool $activeTool as activation is not required(toolCount=${toolCount}")
        }

        return toolComponent
    }

    data class Params(
        val instanceId: String
    )
}