package de.crysxd.octoapp.base.models

import kotlinx.serialization.Serializable

@Serializable
data class GcodeRect(
    val top: Float,
    val left: Float,
    val right: Float,
    val bottom: Float,
)