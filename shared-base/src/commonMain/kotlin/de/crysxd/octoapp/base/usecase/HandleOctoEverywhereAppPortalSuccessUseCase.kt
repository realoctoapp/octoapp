package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.OctoEverywhereConnection
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase.Companion.INSTANCE_ID_PATH_PREFIX
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import io.ktor.http.Url
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

class HandleOctoEverywhereAppPortalSuccessUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val testNewUrlUseCase: TestNewUrlUseCase,
) : UseCase2<Url, Unit>() {

    companion object {
        private val busyCount = MutableStateFlow(0)
        val loadingFlow = busyCount.map { it > 0 }
    }

    override suspend fun doExecute(param: Url, logger: Logger) = try {
        busyCount.update { it + 1 }
        try {
            //region Extract data from param
            logger.i("Handling connection result for OctoEverywhere")
            val isSuccess = param.parameters["success"]?.toBoolean() == true
            if (!isSuccess) {
                throw IllegalStateException("Connection was not successful")
            }
            val connectionId = param.parameters["id"] ?: throw IllegalStateException("No connection id given")
            val url = param.parameters["url"]?.toUrl() ?: throw IllegalStateException("No url given")
            val authUser = param.parameters["authbasichttpuser"] ?: throw IllegalStateException("No auth user given")
            val authPw = param.parameters["authbasichttppassword"] ?: throw IllegalStateException("No auth pw given")
            val authToken = param.parameters["authBearerToken"] ?: throw IllegalStateException("No auth token given")
            val apiToken = param.parameters["appApiToken"] ?: throw IllegalStateException("No api token given")
            val fullUrl = url.withBasicAuth(user = authUser, password = authPw)
            val instanceId = param.pathSegments.firstOrNull { it.startsWith(INSTANCE_ID_PATH_PREFIX) }?.removePrefix(INSTANCE_ID_PATH_PREFIX)
                ?: throw IllegalStateException("No instance id found in URL")
            logger.i("Handling for instance id $instanceId")
            //endregion
            //region Ensure remote connection is valid
            testNewUrlUseCase.execute(
                param = TestNewUrlUseCase.Params(
                    instanceId = instanceId,
                    newUrl = fullUrl,
                    fullTest = false,
                )
            )
            //endregion
            //region Update
            printerConfigurationRepository.update(id = instanceId) {
                it.copy(
                    alternativeWebUrl = fullUrl,
                    remoteConnectionFailure = null,
                    alternativeWebUrlPlugin = OctoPlugins.OctoEverywhere,
                    octoEverywhereConnection = OctoEverywhereConnection(
                        connectionId = connectionId,
                        apiToken = apiToken,
                        basicAuthPassword = authPw,
                        basicAuthUser = authUser,
                        bearerToken = authToken,
                    ),
                )
            }
            //endregion
        } catch (e: Exception) {
            OctoAnalytics.logEvent(
                event = OctoAnalytics.Event.OctoEverywhereConnectFailed,
                params = mapOf("reason" to e::class.simpleName)
            )
            throw e
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.OctoEverywhereConnected)
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.OctoEverywhereUser, "true")
        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "octoeverywhere")
        logger.i("Stored connection info for OctoEverywhere")
    } finally {
        busyCount.update { it - 1 }
    }
}