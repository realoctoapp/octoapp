package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.ext.awaitFileChange
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.files.FileObject

class DeleteFileUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<DeleteFileUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        printerEngineProvider.printer(instanceId = param.instanceId).filesApi.deleteFile(param.file)

        // Await changes to take affect
        printerEngineProvider.awaitFileChange(instanceId = param.instanceId)
    }

    data class Params(
        val file: FileObject,
        val instanceId: String?,
    )
}