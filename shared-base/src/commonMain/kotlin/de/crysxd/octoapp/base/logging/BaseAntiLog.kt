package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Antilog
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.LogLevel.ASSERT
import io.github.aakira.napier.LogLevel.DEBUG
import io.github.aakira.napier.LogLevel.ERROR
import io.github.aakira.napier.LogLevel.INFO
import io.github.aakira.napier.LogLevel.VERBOSE
import io.github.aakira.napier.LogLevel.WARNING
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
abstract class BaseAntiLog : Antilog() {

    abstract val name: String
    abstract val writeTime: Boolean
    abstract val writeExceptionAsText: Boolean
    abstract val minPriority: LogLevel
    abstract val timeZone: TimeZone

    private val mutex = Mutex()
    private val context by lazy { newSingleThreadContext(name) }
    private val tagLength = 28
    open val lineLength = 150
    private val exceptionFilter = ExceptionFilter()
    private var firstLog = true

    abstract suspend fun writeLine(line: String)
    abstract suspend fun writeException(t: Throwable)

    override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        if (priority < minPriority) return

        AppScope.launch(context) {
            try {
                mutex.withLock {
                    performLogInner(priority, tag, throwable, message)
                }
            } catch (e: Throwable) {
                println("WARNING! Exception in logging:")
                e.printStackTrace()
            }
        }
    }

    private suspend fun performLogInner(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        if (firstLog) {
            firstLog = false
            performLogInner(
                INFO,
                tag = "BaseAntiLog",
                message = "System time zone is ${TimeZone.currentSystemDefault()}, logging in $timeZone",
                throwable = null
            )
        }

        val time = if (writeTime) {
            val date = Clock.System.now()
            date.toLocalDateTime(timeZone).time.formatted() + ".${date.nanosecondsOfSecond.toString().take(3)}"
        } else {
            null
        }
        val tagShortened = (tag.takeUnless { it == "TimberAntiLog" } ?: "???").take(tagLength)
        val tagPadded = tagShortened + " ".repeat(tagLength - tagShortened.length)
        val prefix = listOfNotNull(priority.char, time, tagPadded, "").joinToString(" | ")
        val noMessage = message.isNullOrBlank()
        val maskedMessage = message?.takeUnless { noMessage }?.let { SensitiveDataMask.mask(it) }


        maskedMessage?.chunked(lineLength)?.forEach { s ->
            s.trim(' ', '\n', '\t').split("\n").forEach { ss ->
                writeLine("$prefix${ss.trimStackTraceLine()}")
            }
        }

        throwable?.let {
            when (exceptionFilter.filter(it)) {
                ExceptionFilter.Result.Drop -> return@let
                ExceptionFilter.Result.Suppress -> it.writeSuppressed(prefix)
                ExceptionFilter.Result.Log -> when {
                    priority < ERROR -> it.writeSuppressed(prefix)
                    writeExceptionAsText -> it.stackTraceToString().split("\n").forEach { s ->
                        writeLine("$prefix${mask(it, s).trim(' ', '\n').trimStackTraceLine()}")
                    }
                    else -> {
                        writeException(it)
                        writeLine("$prefix${it::class.qualifiedName}: ${mask(it, it.message ?: "").trimStackTraceLine()}")
                    }
                }
            }
        }
    }

    private suspend fun Throwable.writeSuppressed(prefix: String) {
        writeLine("$prefix${this::class.qualifiedName}: ${SensitiveDataMask.mask(message ?: "")}")
        stackTraceToString().split("\n").filter { it.startsWith("Caused by") }.forEach { s ->
            writeLine("$prefix${s.trimStackTraceLine()}")
        }
    }

    private suspend fun mask(e: Throwable, s: String): String = if (SensitiveDataMask.needsMasking(e)) {
        SensitiveDataMask.mask(s)
    } else {
        s
    }

    private fun LocalTime.formatted() = "${hour.toDigitString()}:${minute.toDigitString()}:${second.toDigitString()}"

    private fun Int.toDigitString(digits: Int = 2): String {
        val s = "$this"
        val prefix = "0".repeat(digits - s.length)
        return "$prefix$s"
    }

    private fun String.trimStackTraceLine() = trim(' ', '\n').replace("\t", "    ")

    private val LogLevel.char
        get() = when (this) {
            VERBOSE -> "🤍"
            DEBUG -> "💙"
            INFO -> "💚"
            WARNING -> "💛"
            ERROR -> "🧡"
            ASSERT -> "❤️"
        }
}