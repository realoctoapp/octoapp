package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.SerialCommunication
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.EventSource.Config.Companion.ALL_LOGS
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock


class SerialCommunicationLogsRepository(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) {

    companion object {
        const val MAX_COMMUNICATION_ENTRIES = 1000
    }

    private val tag = "SerialCommunicationLogsRepository"
    private val allLogs = mutableMapOf<String?, MutableList<SerialCommunication>>()
    private val allMutex = mutableMapOf<String?, Mutex>()
    private val allFlows = mutableMapOf<String?, MutableSharedFlow<List<SerialCommunication>>>()
    private val collectionJobs = mutableMapOf<String?, Job>()
    private var generatedCounter = -1

    init {
        AppScope.launch(Dispatchers.Default) {
            collectForInstance(null)

            // Keep a tap on the instances and make sure to always passively collect all of them
            printerConfigurationRepository.allInstanceInformationFlow().collectLatest { all ->
                // Cancel outdated ones if a instance is deleted
                collectionJobs.filter { !all.containsKey(it.key) }.forEach {
                    it.value.cancel()
                    collectionJobs.remove(it.key)
                }

                // Start any new jobs
                all.filter { !collectionJobs.containsKey(it.key) }.forEach {
                    collectionJobs[it.key] = collectForInstance(it.value.id)
                }
            }
        }
    }

    private fun collectForInstance(instanceId: String?) = AppScope.launch(Dispatchers.Default) {
        val logs = getLogs(instanceId)
        val flow = getFlow(instanceId)
        val mutex = getMutex(instanceId)
        var counter = 0

        Napier.i(tag = "TerminalViewModelCore", message = "Init for $instanceId")
        printerEngineProvider.passiveEventFlow(instanceId = instanceId)
            .mapNotNull { it as? Event.MessageReceived }
            .mapNotNull { it.message as? Message.Current }
            .onEach {
                mutex.withLock {
                    if (it.isHistoryMessage) {
                        Napier.i(tag = tag, message = "History message received, clearing cache")
                        logs.clear()
                    }

                    val newLogs = it.logs.mapIndexed { index, log ->
                        SerialCommunication(
                            resetPoint = index == 0 && it.isHistoryMessage,
                            content = log,
                            date = Clock.System.now(),
                            serverDate = it.serverTime,
                            source = SerialCommunication.Source.OctoPrint,
                            id = counter++,
                        )
                    }

                    logs.addAll(newLogs)
                    flow.emit(newLogs)

                    if (logs.size > MAX_COMMUNICATION_ENTRIES) {
                        logs.removeAll(logs.take(logs.size - MAX_COMMUNICATION_ENTRIES).toSet())
                    }
                }
            }
            .retry {
                Napier.e(tag = tag, throwable = it, message = "Exception in serial communication flow")
                delay(1000)
                true
            }
            .collect()
    }

    private fun getFlow(instanceId: String?) = allFlows.getOrPut(instanceId) { MutableSharedFlow(0, 10, BufferOverflow.SUSPEND) }

    private fun getLogs(instanceId: String?) = allLogs.getOrPut(instanceId) { mutableListOf() }

    private fun getMutex(instanceId: String?) = allMutex.getOrPut(instanceId) { Mutex() }

    suspend fun addInternalLog(log: String, fromUser: Boolean, instanceId: String? = null) {
        val item = SerialCommunication(
            content = log,
            serverDate = Clock.System.now(),
            date = Clock.System.now(),
            source = if (fromUser) SerialCommunication.Source.User else SerialCommunication.Source.OctoAppInternal,
            id = generatedCounter--,
            resetPoint = false,
        )

        getLogs(instanceId).add(item)
        getFlow(instanceId).emit(listOf(item))
    }

    fun passiveFlow(includeOld: Boolean = false, instanceId: String? = null) = flow {
        passiveFlowChunked(includeOld = includeOld, instanceId = instanceId).onEach { chunk ->
            chunk.forEach { emit(it) }
        }.collect()
    }.buffer()

    fun passiveFlowChunked(includeOld: Boolean = false, instanceId: String? = null): Flow<List<SerialCommunication>> = getFlow(instanceId).onStart {
        if (includeOld) {
            emit(all(instanceId = instanceId))
        }
    }

    fun activeFlow(includeOld: Boolean = false, instanceId: String?) = flow {
        activeFlowChunked(includeOld = includeOld, instanceId = instanceId).onEach { chunk ->
            chunk.forEach { emit(it) }
        }.collect()
    }.buffer()

    @OptIn(ExperimentalCoroutinesApi::class)
    fun activeFlowChunked(includeOld: Boolean = false, instanceId: String?): Flow<List<SerialCommunication>> {
        return printerEngineProvider.eventFlow(
            tag = "activeTerminalFlow",
            instanceId = instanceId,
            config = EventSource.Config(requestTerminalLogs = listOf(ALL_LOGS))
        ).map {
            // We only need to keep this flow alive, not interested in events as we get those from the getFlow(...)
        }.distinctUntilChanged().flatMapLatest {
            getFlow(instanceId)
        }.onStart {
            if (includeOld) {
                emit(all(instanceId = instanceId))
            }
        }.shareIn(
            // We "share" this flow in the AppScope to keep it alive for 1s after it is completed. This is important to
            // prevent the underlying network infrastructure from already sending the "Some logs" config and we then reconnect with a new flow
            // a split second later, causing all logs to be send again
            scope = AppScope,
            started = SharingStarted.WhileSubscribedOctoDelay
        ).buffer()
    }

    suspend fun all(instanceId: String? = null) = getMutex(instanceId).withLock {
        getLogs(instanceId).toList()
    }
}