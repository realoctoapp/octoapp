package de.crysxd.octoapp.base

expect object OctoPushMessaging {
    suspend fun getPushToken(): String?
}