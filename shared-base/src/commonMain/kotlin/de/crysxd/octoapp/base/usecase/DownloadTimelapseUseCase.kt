package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.framework.urlFromEncodedPath
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.sharedcommon.Constants
import io.ktor.client.plugins.onDownload
import io.ktor.client.request.header
import io.ktor.client.request.prepareGet
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.HttpHeaders

class DownloadTimelapseUseCase(
    private val mediaFileRepository: MediaFileRepository,
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<DownloadTimelapseUseCase.Param, DownloadTimelapseUseCase.Result>() {

    override suspend fun doExecute(param: Param, logger: Logger): Result {
        logger.i("Preparing to download ${param.timelapseFile.downloadPath}")

        if (!mediaFileRepository.isCached(param.downloadPath)) {
            download(param)
        }

        return Result(
            filePath = mediaFileRepository.prepareFileForPublicSharing(
                key = param.downloadPath,
                name = param.timelapseFile.name
            ).toString()
        )
    }

    private suspend fun download(param: Param) = printerEngineProvider.printer(instanceId = param.instanceId).genericRequest { baseUrl, httpClient ->
        httpClient.prepareGet {
            urlFromEncodedPath(baseUrl = baseUrl, pathSegments = param.downloadPath.split("/"))
            onDownload { bytesSentTotal, contentLength ->
                val total = contentLength.takeIf { it > 0 } ?: param.timelapseFile.bytes
                val progressPercent = (bytesSentTotal / total.toFloat()) * 100
                param.progressUpdate(progressPercent.toInt().coerceIn(0..100))
            }

            // There is an issue with KTOR cache as it saves the entire download in memory at first, we can't have this of course
            // with potentially huge files. Also suppress logging, also clogging memory.
            header(HttpHeaders.CacheControl, "no-store, no-cache")
            attributes.put(Constants.SuppressLogging, true)
        }.execute { response ->
            mediaFileRepository.addFile(
                key = param.downloadPath,
                byteReadChannel = response.bodyAsChannel()
            )
        }
    }

    private val Param.downloadPath get() = requireNotNull(timelapseFile.downloadPath) { "Can't download file without path" }

    data class Param(
        val timelapseFile: TimelapseFile,
        val instanceId: String,
        val progressUpdate: (Int) -> Unit
    )

    data class Result(
        val filePath: String,
    )
}