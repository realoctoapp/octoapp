package de.crysxd.octoapp.base.exceptions

import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.utils.getString

class WebcamDisabledException(
    override val userMessage: String
) : SuppressedIllegalStateException(), UserMessageException {
    constructor(systemInfo: SystemInfo?) : this(getString("please_configure_your_webcam_in_octoprint", systemInfo?.interfaceType.label))
}