package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider

class ChangeZOffsetUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<ChangeZOffsetUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        val engine = printerEngineProvider.printer(instanceId = param.instanceId)
        engine.printerApi.changeZOffset(param.changeMm)
    }

    data class Param(
        val changeMm: Float,
        val instanceId: String,
    )
}