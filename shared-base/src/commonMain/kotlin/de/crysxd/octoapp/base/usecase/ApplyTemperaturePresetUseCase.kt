package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase.Params.Companion.CoolDownName
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.models.settings.Settings

class ApplyTemperaturePresetUseCase(
    private val printerRepository: PrinterConfigurationRepository,
    private val executeGcodeCommandUseCase: ExecuteGcodeCommandUseCase,
    private val setTargetTemperaturesUseCase: SetTargetTemperaturesUseCase,
) : UseCase2<ApplyTemperaturePresetUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        val settings = printerRepository.get(param.instanceId)?.settings
        val preset = if (param.profileName == CoolDownName) {
            settings?.coolDownProfile
        } else {
            settings?.temperaturePresets?.firstOrNull { it.name == param.profileName }
        }
        val printerProfile = printerRepository.get(param.instanceId)?.activeProfile ?: PrinterProfile()
        requireNotNull(preset) { "No profile ${param.profileName}" }
        logger.i("Applying profile: $preset")

        setTargetTemperaturesUseCase.execute(
            param = BaseChangeTemperaturesUseCase.Params(
                instanceId = param.instanceId,
                temps = preset.components.filter {
                    val bed = it.isBed && printerProfile.heatedBed
                    val chamber = it.isChamber && printerProfile.heatedChamber
                    val extruder = it.isExtruder
                    bed || chamber || extruder
                }.filter {
                    param.componentFilter(it)
                }.map { component ->
                    BaseChangeTemperaturesUseCase.Temperature(component = component.key, temperature = component.temperature)
                }
            )
        )

        if (param.executeGcode) preset.gcode?.let { gcode ->
            executeGcodeCommandUseCase.execute(
                param = ExecuteGcodeCommandUseCase.Param(
                    instanceId = param.instanceId,
                    command = GcodeCommand.Batch(commands = gcode.split("\n")),
                    fromUser = false,
                )
            )
        }
    }

    data class Params(
        val instanceId: String,
        val profileName: String,
        val executeGcode: Boolean,
        val componentFilter: (Settings.TemperaturePreset.Component) -> Boolean = { true }
    ) {
        companion object {
            const val CoolDownName = "___cooldown"
        }
    }
}