package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.data.repository.MacroGroupsRepository
import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.data.repository.TutorialsRepository
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteGcodeFileDataSource
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.ActivateMaterialUseCase
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.base.usecase.ApplyWebcamTransformationsUseCase
import de.crysxd.octoapp.base.usecase.AutoConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.CancelObjectUseCase
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.ChangeZOffsetUseCase
import de.crysxd.octoapp.base.usecase.ClearPublicFilesUseCase
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.CreateFolderUseCase
import de.crysxd.octoapp.base.usecase.CyclePsuUseCase
import de.crysxd.octoapp.base.usecase.DeleteFileUseCase
import de.crysxd.octoapp.base.usecase.DeleteTimelapseUseCase
import de.crysxd.octoapp.base.usecase.DisconnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import de.crysxd.octoapp.base.usecase.DownloadFileUseCase
import de.crysxd.octoapp.base.usecase.DownloadTimelapseUseCase
import de.crysxd.octoapp.base.usecase.EmergencyStopUseCase
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.ExecuteSystemCommandUseCase
import de.crysxd.octoapp.base.usecase.ExtrudeFilamentUseCase
import de.crysxd.octoapp.base.usecase.GenerateOctoEverywhereLiveLinkUseCase
import de.crysxd.octoapp.base.usecase.GenericDownloadUseCase
import de.crysxd.octoapp.base.usecase.GetActiveHttpUrlUseCase
import de.crysxd.octoapp.base.usecase.GetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.GetCurrentPrinterProfileUseCase
import de.crysxd.octoapp.base.usecase.GetExtrusionShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetGcodeShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetMaterialsUseCase
import de.crysxd.octoapp.base.usecase.GetOctoPrintWebUrlUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetPrintHistoryUseCase
import de.crysxd.octoapp.base.usecase.GetPrinterConnectionUseCase
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.GetTuneStateUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSettingsUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.GetWidgetDataUseCase
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.usecase.HandleObicoAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HomePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.JogPrintHeadUseCase
import de.crysxd.octoapp.base.usecase.LoadFileReferencesUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.MoveFileUseCase
import de.crysxd.octoapp.base.usecase.MovePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.PerformPendingSettingsSave
import de.crysxd.octoapp.base.usecase.RefreshBedMeshUseCase
import de.crysxd.octoapp.base.usecase.RegisterWithCompanionPluginUseCase
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.ResetZOffsetUseCase
import de.crysxd.octoapp.base.usecase.SaveZOffsetToConfigUseCase
import de.crysxd.octoapp.base.usecase.SelectMmu2FilamentUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.base.usecase.SetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.TestNewUrlUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.usecase.TunePrintUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.base.usecase.UploadFileUseCase
import de.crysxd.octoapp.sharedcommon.di.BaseComponent
import de.crysxd.octoapp.sharedcommon.di.SharedCommonComponent
import de.crysxd.octoapp.sharedcommon.http.cache.HttpDiskCache
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import org.koin.core.component.get
import org.koin.core.component.inject


class SharedBaseComponent(
    sharedCommonComponent: SharedCommonComponent,
    repositoryModule: RepositoryModule = RepositoryModule(),
    loggingModule: LoggingModule = LoggingModule(),
    networkModule: NetworkModule,
    useCaseModule: UseCaseModule = UseCaseModule(),
    dataSourceModule: DataSourceModule = DataSourceModule(),
) : BaseComponent(
    dependencies = listOf(
        sharedCommonComponent,
    ),
    modules = listOf(
        repositoryModule,
        loggingModule,
        networkModule,
        useCaseModule,
        dataSourceModule
    )
) {
    // RepositoryModule
    val extrusionHistoryRepository: ExtrusionHistoryRepository by inject()
    val pinnedMenuItemRepository: PinnedMenuItemRepository by inject()
    val gcodeHistoryRepository: GcodeHistoryRepository by inject()
    val printerConfigRepository: PrinterConfigurationRepository by inject()
    val controlsPreferencesRepository: ControlsPreferencesRepository by inject()
    val preferences: OctoPreferences by inject()
    val fileListRepository: FileListRepository by inject()
    val serialCommunicationLogsRepository: SerialCommunicationLogsRepository by inject()
    val temperatureDataRepository: TemperatureDataRepository by inject()
    val tutorialsRepository: TutorialsRepository by inject()
    val timelapseRepository: TimelapseRepository by inject()
    val gcodeFileRepository: GcodeFileRepository by inject()
    val mediaFileRepository: MediaFileRepository = get()
    val macroGroupsRepository: MacroGroupsRepository = get()

    // NetworkModule
    val printerEngineProvider: PrinterEngineProvider by inject()
    val dnsResolver: CachedDns by inject()
    val keyStoreProvider: KeyStoreProvider by inject()
    val httpCache: HttpDiskCache = get()
    fun httpClientSettings(): HttpClientSettings = get()

    // LoggingModule
    val sensitiveDataMask: SensitiveDataMask by inject()

    // DataSourceModule
    val localGcodeFileDataSource: LocalGcodeFileDataSource by inject()
    val remoteGcodeFileDataSource: RemoteGcodeFileDataSource by inject()

    // UseCases
    fun getWebcamSettingsUseCase(): GetWebcamSettingsUseCase = get()
    fun getActiveHttpUrlUseCase(): GetActiveHttpUrlUseCase = get()
    fun applyWebcamTransformationsUseCase(): ApplyWebcamTransformationsUseCase = get()
    fun getPowerDevicesUseCase(): GetPowerDevicesUseCase = get()
    fun handleAutomaticLightEventUseCase(): HandleAutomaticLightEventUseCase = get()
    fun testFullNetworkStackUseCase(): TestFullNetworkStackUseCase = get()
    fun discoverOctoPrintUseCase(): DiscoverOctoPrintUseCase = get()
    fun requestApiAccessUseCase(): RequestApiAccessUseCase = get()
    fun getAppLanguageUseCase(): GetAppLanguageUseCase = get()
    fun updateInstanceCapabilitiesUseCase(): UpdateInstanceCapabilitiesUseCase = get()
    fun getWebcamSnapshotUseCase(): GetWebcamSnapshotUseCase = get()
    fun togglePausePrintJobUseCase(): TogglePausePrintJobUseCase = get()
    fun cancelPrintJobUseCase(): CancelPrintJobUseCase = get()
    fun setTargetTemperaturesUseCase(): SetTargetTemperaturesUseCase = get()
    fun applyTemperaturePresetUseCase(): ApplyTemperaturePresetUseCase = get()
    fun setTemperatureOffsetUseCase(): SetTemperatureOffsetUseCase = get()
    fun getCurrentPrinterProfileUseCase(): GetCurrentPrinterProfileUseCase = get()
    fun loadFilesUseCase(): LoadFilesUseCase = get()
    fun loadFileReferencesUseCase(): LoadFileReferencesUseCase = get()
    fun getPrintHistoryUseCase(): GetPrintHistoryUseCase = get()
    fun startPrintJobUseCase(): StartPrintJobUseCase = get()
    fun createBugReportUseCase(): CreateBugReportUseCase = get()
    fun getRemoteServiceConnectUrlUseCase(): GetRemoteServiceConnectUrlUseCase = get()
    fun handleOctoEverywhereAppPortalSuccessUseCase(): HandleOctoEverywhereAppPortalSuccessUseCase = get()
    fun handleObicoAppPortalSuccessUseCase(): HandleObicoAppPortalSuccessUseCase = get()
    fun setAlternativeWebUrlUseCase(): SetAlternativeWebUrlUseCase = get()
    fun updateNgrokTunnelUseCase(): UpdateNgrokTunnelUseCase = get()
    fun connectPrinterUseCase(): ConnectPrinterUseCase = get()
    fun disconnectPrinterUseCase(): DisconnectPrinterUseCase = get()
    fun autoConnectPrinterUseCase(): AutoConnectPrinterUseCase = get()
    fun getPrinterConnectionUseCase(): GetPrinterConnectionUseCase = get()
    fun setAppLanguageUseCase(): SetAppLanguageUseCase = get()
    fun executeSystemCommandUseCase(): ExecuteSystemCommandUseCase = get()
    fun getOctoPrintWebUrlUseCase(): GetOctoPrintWebUrlUseCase = get()
    fun emergencyStopUseCase(): EmergencyStopUseCase = get()
    fun getMaterialsUseCase(): GetMaterialsUseCase = get()
    fun activateMaterialUseCase(): ActivateMaterialUseCase = get()
    fun cyclePsuUseCase(): CyclePsuUseCase = get()
    fun homePrintHeadUseCase(): HomePrintHeadUseCase = get()
    fun jogPrintHeadUseCase(): JogPrintHeadUseCase = get()
    fun movePrintHeadUseCase(): MovePrintHeadUseCase = get()
    fun extrudeFilamentUseCase(): ExtrudeFilamentUseCase = get()
    fun getWidgetDataUseCase(): GetWidgetDataUseCase = get()
    fun getExtrusionShortcutsUseCase(): GetExtrusionShortcutsUseCase = get()
    fun testNewUrlUseCase(): TestNewUrlUseCase = get()
    fun executeGcodeCommandUseCase(): ExecuteGcodeCommandUseCase = get()
    fun refreshBedMeshUseCase(): RefreshBedMeshUseCase = get()
    fun getGcodeShortcutsUseCase(): GetGcodeShortcutsUseCase = get()
    fun tunePrintUseCase(): TunePrintUseCase = get()
    fun changeZOffsetUseCase(): ChangeZOffsetUseCase = get()
    fun resetZOffsetUseCase(): ResetZOffsetUseCase = get()
    fun saveZOffsetToConfigUseCase(): SaveZOffsetToConfigUseCase = get()
    fun getTuneStateUseCase(): GetTuneStateUseCase = get()
    fun cancelObjectUseCase(): CancelObjectUseCase = get()
    fun triggerInitialCancelObjectMessageUseCase(): TriggerInitialCancelObjectMessageUseCase = get()
    fun selectMmu2FilamentUseCase(): SelectMmu2FilamentUseCase = get()
    fun generateOctoEverywhereLiveLinkUseCase(): GenerateOctoEverywhereLiveLinkUseCase = get()
    fun registerWithCompanionPluginUseCase(): RegisterWithCompanionPluginUseCase = get()
    fun downloadTimelapseUseCase(): DownloadTimelapseUseCase = get()
    fun deleteTimelapseUseCase(): DeleteTimelapseUseCase = get()
    fun clearPublicFilesUseCase(): ClearPublicFilesUseCase = get()
    fun moveFileUseCase(): MoveFileUseCase = get()
    fun deleteFileUseCase(): DeleteFileUseCase = get()
    fun downloadFileUseCase(): DownloadFileUseCase = get()
    fun createFolderUseCase(): CreateFolderUseCase = get()
    fun uploadFileUseCase(): UploadFileUseCase = get()
    fun performPendingSettingsSave(): PerformPendingSettingsSave = get()
    fun genericDownloadUseCase(): GenericDownloadUseCase = get()
}
