package de.crysxd.octoapp.base.models

import de.crysxd.octoapp.engine.models.files.FileOrigin
import kotlinx.datetime.Instant

data class JobHistoryItem(
    val path: String,
    val fileId: String?,
    val fileOrigin: FileOrigin,
    val time: Instant,
    val success: Boolean,
)