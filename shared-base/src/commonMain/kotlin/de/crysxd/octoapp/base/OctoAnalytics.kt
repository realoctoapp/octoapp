package de.crysxd.octoapp.base

import io.github.aakira.napier.Napier

expect class AnalyticsAdapter() {
    fun logEvent(event: String, params: Map<String, Any?>)
    fun setUserProperty(property: String, value: String?)
}

data object OctoAnalytics {

    private const val FIREBASE_SCREEN_VIEW = "screen_view"
    private const val FIREBASE_LOGIN = "login"

    private val adapter = AnalyticsAdapter()

    fun logEvent(event: Event, params: Map<String, Any?> = emptyMap()) {
        Napier.i(tag = "Analytics/Event", message = "${event.name} / $params")
        adapter.logEvent(event.name, params)
    }

    fun setUserProperty(property: UserProperty, value: String?) {
        Napier.i(tag = "Analytics/Property", message = "${property.name}=$value")
        adapter.setUserProperty(property.name, value)
    }

    sealed class UserProperty(val name: String) {
        data object OctoPrintVersion : UserProperty("octoprint_server_version")
        data object WebCamAvailable : UserProperty("webcam_available")
        data object AppLanguage : UserProperty("app_language")
        data object PremiumUser : UserProperty("premium_user")
        data object PremiumSubUser : UserProperty("premium_sub_user")
        data object BillingStatus : UserProperty("billing_status")
        data object AndroidBuild : UserProperty("android_build")
        data object OctoEverywhereUser : UserProperty("octoeverywhere_user")
        data object ObicoUser : UserProperty("spaghetti_detective_user")
        data object RemoteAccess : UserProperty("remote_access_configured")
        data object HasKlipper : UserProperty("has_klipper")
        data object HasOctoPrint : UserProperty("has_octoprint")

        data object PrinterFirmwareName : UserProperty("printer_firmware_name")
        data object PrinterMachineType : UserProperty("printer_machine_type")
        data object PrinterExtruderCount : UserProperty("printer_extruder_count")
    }

    sealed class Event(val name: String) {
        // App events
        data object OctoprintConnected : Event("octoprint_connected")
        data object SignInSuccess : Event("sign_in_success")
        data object Login : Event(FIREBASE_LOGIN)
        data object PrinterAutoConnectFailed : Event("auto_connect_failed")
        data object PrintStartedByApp : Event("print_started_by_app")

        // Environment
        class PluginDetected(pluginName: String) : Event("zz_has_plugin_${pluginName.lowercase()}".take(40 /* Limit for event name length */))
        data object PrinterFirmwareData : Event("printer_firmware_data")
        data object AppLanguageChanged : Event("app_language_changed")

        // Sign in
        data object ManualApiKeyUsed : Event("sign_in_manual_api_key_used")
        data object UpnpServiceSelected : Event("sign_in_upnp_service_selected")
        data object DnsServiceSelected : Event("sign_in_dnssd_service_selected")
        data object ManualUrlSelected : Event("sign_in_manual_url_selected")
        data object SignInHelpOpened : Event("sign_in_help_opened")

        // Purchase
        data object PurchaseScreenScroll : Event("purchase_screen_scroll")
        data object PurchaseScreenOpen : Event("purchase_screen_open")
        data object PurchaseMissingSku : Event("purchase_missing_sku")
        data object PurchaseOptionsShown : Event("purchase_options_shown")
        data object PurchaseUspInteraction : Event("purchase_usp_interaction")
        data object PurchaseIntroShown : Event("purchase_intro_shown")
        data object PurchaseScreenClosed : Event("purchase_screen_closed")
        data object PurchaseOptionSelected : Event("purchase_option_selected")
        data object PurchaseFlowCompleted : Event("purchase_billing_flow_completed")
        data object PurchaseFlowCancelled : Event("purchase_billing_flow_cancelled")
        data object PurchaseFlowFailed : Event("purchase_billing_flow_failed")
        data object DisabledFeatureHidden : Event("disabled_feature_hidden")

        // Remote connection
        data object OctoEverywhereConnectStarted : Event("octoeverywhere_connect_started")
        data object OctoEverywhereConnected : Event("octoeverywhere_connected")
        data object OctoEverywhereConnectFailed : Event("octoeverywhere_connect_failed")
        data object OctoEverywherePluginMissing : Event("octoeverywhere_plugin_missing")
        data object OctoEverywhereServicePresented : Event("octoeverywhere_service_presented")
        data object OctoEverywhereServiceAtPosition1 : Event("octoeverywhere_service_pos_1")
        data object OctoEverywhereBroken : Event("octoeverywhere_broken")
        data object ObicoConnectStarted : Event("spaghetti_detective_connect_started")
        data object ObicoConnected : Event("spaghetti_detective_connected")
        data object ObicoConnectFailed : Event("spaghetti_detective_connect_failed")
        data object ObicoPluginMissing : Event("spaghetti_detective_plugin_missing")
        data object ObicoServicePresented : Event("spaghetti_detective_service_presented")
        data object ObicoServiceAtPosition1 : Event("spaghetti_detective_service_pos_1")
        data object ObicoBroken : Event("spaghetti_detective_broken")
        data object NgrokServicePresented : Event("ngrok_service_presented")
        data object NgrokBroken : Event("ngrok_broken")
        data object RemoteConfigScreenOpened : Event("remote_config_screen_opened")
        data object RemoteConfigManuallySet : Event("remote_config_manually_set")
        data object RemoteConfigManuallySetFailed : Event("remote_config_manually_set_failed")
        data object RemoteConfigCleared : Event("remote_config_cleared")

        // Android only
        data object SupportFromErrorDetails : Event("support_from_error_details")
        data object BackupDnsResolveSuccess : Event("backup_dns_resolve_success")
        data object MDnsResolveSuccess : Event("mdns_resolve_success")
        data object UpnpDnsResolveSuccess : Event("upnp_dns_resolve_success")
        data object QrCodeCompleted : Event("sign_in_qr_code_login_completed")
        data object QrCodeStarted : Event("sign_in_qr_code_login")
        data object PrintCancelledByApp : Event("print_canceled_by_app")
        data object AppUpdateAvailable : Event("app_update_available")
        data object AppUpdatePresented : Event("app_update_presented")
        data object ScreenShown : Event(FIREBASE_SCREEN_VIEW)
        data object LoginWorkspaceShown : Event("workspace_shown_login")
        data object ConnectWorkspaceShown : Event("workspace_shown_connect")
        data object PrePrintWorkspaceShown : Event("workspace_shown_pre_print")
        data object PrintWorkspaceShown : Event("workspace_shown_print")
        data object TerminalWorkspaceShown : Event("workspace_shown_terminal")

        // Review flow
        data object ReviewFlowLaunched : Event("review_flow_launched")
        data object ReviewFlowConsidered : Event("review_flow_considered")
        data object ReviewFlowFailed : Event("review_flow_failed")
    }
}
