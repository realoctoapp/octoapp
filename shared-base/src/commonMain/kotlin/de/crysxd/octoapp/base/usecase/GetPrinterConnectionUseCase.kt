package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.models.connection.ConnectionState

class GetPrinterConnectionUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<GetPrinterConnectionUseCase.Params, ConnectionState>() {

    override suspend fun doExecute(param: Params, logger: Logger) =
        printerEngineProvider.printer(param.instanceId).asOctoPrint().connectionApi.getConnection()

    data class Params(
        val instanceId: String? = null
    )
}