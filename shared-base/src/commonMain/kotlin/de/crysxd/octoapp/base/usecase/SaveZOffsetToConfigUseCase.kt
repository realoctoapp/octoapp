package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider

class SaveZOffsetToConfigUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
) : UseCase2<SaveZOffsetToConfigUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, logger: Logger) {
        val engine = printerEngineProvider.printer(instanceId = param.instanceId)
        engine.printerApi.saveZOffset()
    }

    data class Param(
        val instanceId: String,
    )
}