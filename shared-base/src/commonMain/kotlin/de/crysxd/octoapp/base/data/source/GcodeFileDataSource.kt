package de.crysxd.octoapp.base.data.source

import de.crysxd.octoapp.base.models.Gcode

interface GcodeFileDataSource {
    sealed class LoadState {
        data class Loading(val progress: Float) : LoadState()
        data class FailedLargeFileDownloadRequired(val downloadSize: Long) : LoadState()
        data class Ready(val gcode: Gcode) : LoadState()
        data class Failed(val exception: Throwable) : LoadState()
    }
}