package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.sharedcommon.utils.getString


enum class MenuId {
    MainMenu,
    ConnectWorkspace,
    PrintWorkspace,
    PrePrintWorkspace,
    Widget,
    Other;

    companion object {
        fun fromFlags(flags: PrinterState.Flags?) = when {
            flags?.isPrinting() == true -> PrintWorkspace
            flags?.isOperational() == true -> PrePrintWorkspace
            else -> ConnectWorkspace
        }
    }

    fun canPin(canRunWithAppInBackground: Boolean) = (this != Widget || canRunWithAppInBackground) && this != Other

    val label
        get() = when (this) {
            MainMenu -> getString("menu_controls___main")
            ConnectWorkspace -> getString("menu_controls___connect_workspace")
            PrintWorkspace -> getString("menu_controls___print_workspace")
            PrePrintWorkspace -> getString("menu_controls___prepare_workspace")
            Widget -> getString("menu_controls___widget")
            Other -> getString("string.menu_controls___other")
        }
}