package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.sharedexternalapis.octoeverywhere.OctoEverywhereAppConnectionApi
import kotlin.time.Duration

class GenerateOctoEverywhereLiveLinkUseCase(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<GenerateOctoEverywhereLiveLinkUseCase.Params, String>() {

    override suspend fun doExecute(param: Params, logger: Logger): String {
        val connection =
            requireNotNull(printerConfigurationRepository.get(param.instanceId)?.octoEverywhereConnection) { "No OctoEverywhere connection for ${param.instanceId}" }
        val api = OctoEverywhereAppConnectionApi(appToken = connection.apiToken)
        return requireNotNull(api.createLiveLink(param.validity)) { "Failed to create live link, result is null" }
    }

    data class Params(
        val validity: Duration,
        val instanceId: String
    )
}