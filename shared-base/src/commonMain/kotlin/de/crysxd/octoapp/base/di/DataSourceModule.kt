package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteGcodeFileDataSource
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.ConnectivityHelper
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import de.crysxd.octoapp.sharedcommon.io.FileManager
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import org.koin.dsl.module


class DataSourceModule : BaseModule {

    private fun provideLocalGcodeFileDataSource(
        settings: SettingsStore,
        fileManager: FileManager,
        octoPreferences: OctoPreferences,
    ) = LocalGcodeFileDataSource(
        settingsStore = settings,
        fileManager = fileManager,
        octoPreferences = octoPreferences,
    )

    private fun provideRemoteGcodeFileDataSource(
        localGcodeFileDataSource: LocalGcodeFileDataSource,
        printerEngineProvider: PrinterEngineProvider,
        printerConfigurationRepository: PrinterConfigurationRepository,
        octoPreferences: OctoPreferences,
    ) = RemoteGcodeFileDataSource(
        localDataSource = localGcodeFileDataSource,
        printerEngineProvider = printerEngineProvider,
        printerConfigurationRepository = printerConfigurationRepository,
        connectivityHelper = ConnectivityHelper(),
        octoPreferences = octoPreferences,
    )


    override val koinModule = module {
        single { provideLocalGcodeFileDataSource(get(), get(), get()) }
        single { provideRemoteGcodeFileDataSource(get(), get(), get(), get()) }
    }
}