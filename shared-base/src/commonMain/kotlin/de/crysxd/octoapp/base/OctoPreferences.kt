package de.crysxd.octoapp.base

import com.benasher44.uuid.uuid4
import com.russhwolf.settings.Settings
import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.base.data.models.ControlCenterSettings
import de.crysxd.octoapp.base.data.models.FileManagerSettings
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.models.ReviewFlowConditions
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.io.activeInstanceIdKey
import de.crysxd.octoapp.sharedcommon.io.getSerializableOrNull
import de.crysxd.octoapp.sharedcommon.io.preferencesNamespace
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.datetime.Instant


@OptIn(OctoPreferences.RawAccess::class)
class OctoPreferences(settingsStore: SettingsStore, private val platform: Platform) {

    @RequiresOptIn("Do not use raw access to the underlying settings")
    @Retention(AnnotationRetention.BINARY)
    @Target(AnnotationTarget.PROPERTY)
    annotation class RawAccess

    companion object {
        private const val SET_BARRIER = "%%%barreir%%%"

        private const val KEY_PRINT_NOTIFICATION_ENABLED = "print_notification_enabled"
        private const val KEY_APP_THEME = "app_theme"
        private const val KEY_KEEP_SCREEN_ON = "keep_screen_on"
        private const val KEY_APP_LANGUAGE = "app_language"
        private const val KEY_ALLOW_APP_ROTATION = "allow_app_rotation"
        private const val KEY_NO_SWIPE_CONFIRMATIONS = "no_swipe_confirmations"
        private const val KEY_ALLOW_NOTIFICATION_BATTERY_SAVER = "allow_notification_battery_saver"
        private const val KEY_HIDE_THUMBNAIL_HINT_UNTIL = "hide_thumbnail_hin_until"
        private const val KEY_AUTO_CONNECT_PRINTER = "auto_connect_printer"
        private const val KEY_AUTO_CONNECT_PRINTER_INFO_SHOWN = "auto_connect_printer_info_shown"
        private const val KEY_CRASH_REPORTING = "crash_reporting_enabled"
        private const val KEY_ANALYTICS = "analytics_enabled"
        private const val KEY_PRINT_NOTIFICATION_WAS_DISCONNECTED = "print_notification_was_disconnected"
        private const val KEY_PRINT_NOTIFICATION_WAS_DISABLED_UNTIL_NEXT_LAUNCH = "print_notification_was_disabled_until_next_launch"
        private const val KEY_AUTO_LIGHTS = "auto_lights"
        private const val KEY_CONFIRM_POWER_OFF_DEVICES = "confirm_power_off_devices"
        private const val KEY_AUTO_LIGHTS_FOR_WIDGET_REFRESH = "auto_lights_for_widget_refresh"
        private const val KEY_SHOW_WEBCAM_RESOLUTION = "show_webcam_resolution"
        private const val KEY_SHOW_WEBCAM_NAME = "show_webcam_name"
        private const val KEY_OVERRIDE_ROTATION_LOCK_FOR_WEBCAM = "override_rotation_lock_for_webcam"
        private const val KEY_WEBCAM_ASPECT_RATIO_SOURCE = "webcam_aspect_ratio_source"
        private const val KEY_SUPPRESS_M115 = "suppress_m115_request"
        private const val KEY_REMOTE_ACCESS_ANNOUNCEMENT_HIDDEN_AT = "octoeverywhere_announcemenyt_hidden_at"
        private const val KEY_TUTORIALS_SEEN_AT = "tutorials_seen_at"
        private const val KEY_ALLOW_TERMINAL_DURING_PRINT = "allow_terminal_during_print"
        private const val KEY_SUPPRESS_REMOTE_NOTIFICATIONS_INIT = "suppress_remote_notification_init"
        private const val KEY_DEBUG_NETWORK_LOGGING = "debug_network_logging"
        private const val KEY_ENFORCE_IP_V4 = "enforce_ip_v4"
        private const val KEY_RECORD_WEBCAM_FOR_DEBUG = "record_webcam_for_debug"
        private const val KEY_COMPACT_LAYOUT = "compact_layout"
        private const val KEY_EMERGENCY_STOP_IN_BOTTOM_BAR = "emergency_stop_in_bottom_bar"
        private const val KEY_GCODE_PREVIEW = "gcode_preview"
        private const val KEY_FILE_MANAGER = "file_manager"
        private const val KEY_PROGRESS_WIDGET = "progress_widget"
        private const val KEY_PROGRESS_FULLSCREEN_WIDGET = "progress_widget_webcam_fullscreen"
        private const val KEY_ASK_FOR_TIMELAPSE_BEFORE_PRINTING = "ask_for_timelapse_before_printing"
        private const val KEY_LEFT_HAND_MODE = "left_hand_mode"
        private const val KEY_MOVE_DURING_PRINT = "move_during_print"
        private const val KEY_MEDIA_CACHE_SIZE = "media_cache_size"
        private const val KEY_GCODE_CACHE_SIZE = "gcode_cache_size"
        private const val KEY_HTTP_CACHE_SIZE = "http_cache_size"
        private const val KEY_NOTIFY_PRINTER_BEEP = "notify_printer_beep"
        private const val KEY_NOTIFY_FOR_LAYERS = "notify_for_layer_completion"
        private const val KEY_NOTIFY_FOR_USER_INTERACTION = "notify_for_user_interaction"
        private const val KEY_DATA_SAVER_WEBCAM_INTERVAL = "data_saver_webcam_interval"
        private const val KEY_DATA_USE_ON_MOBILE_NETWORK_ONLY = "data_saver_use_on_mobile_network_only"
        private const val KEY_ASK_NOTIFICATION_PERMISSION_NOT_BEFORE = "ask_notification_permission_not_before_2"
        private const val KEY_ASK_COMPANION_INSTALL_NOT_BEFORE = "ask_companion_install_not_before_2"
        private const val KEY_NOTIFICATION_PERMISSION_REQUEST_STATE = "notification_permission_request_state_2"
        private const val KEY_SHOWACTIVE_INSTANCE_IN_STATUS = "show_active_instance_in_status_bar"
        private const val KEY_SHOW_ALL_MATERIAL_PROPERTIES = "show_all_material_properties"
        private const val KEY_GCODE_HIDDEN_IOS = "gcode_hidden_ios_2"
        private const val KEY_DID_RECEIVE_TRANSFER = "did_receive_transfer"
        private const val KEY_NOTIFICATIONS_LAYOUT = "notifications_layout"
        private const val KEY_WEBSOCKET_TRANSPORT_ALLOWED = "websocket_transport_allowed"
        private const val KEY_ACTIVE_PLUGIN_INTEGRATIONS = "active_plugin_integrations"
        private const val KEY_LONG_TIMEOUTS = "long_timeouts"
        private const val KEY_MDNS_SYSTEM = "mdns_system"
        private const val KEY_CONTROL_CENTER = "control_center"
        private const val KEY_INSTALL_ID = "install_id"
        private const val REVIEW_FLOW_CONDITIONS = "app_review_trigger_conditions"
        private const val DETECT_LAYERS_FROM_SLICER = "layers_from_slicer"

        const val VALUE_WEBCAM_ASPECT_RATIO_SOURCE_OCTOPRINT = "octprint"
        const val VALUE_WEBCAM_ASPECT_RATIO_SOURCE_IMAGE = "native_image"
        const val VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_PROGRESS = "snapshot_progress"
        const val VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_SUBTITLE = "snapshot_subtitle"
        const val VALUE_NOTIFICATIONS_LAYOUT_SUBTITLE_PROGRESS = "subtitle_progress"
    }

    private val parsedItemCache = mutableMapOf<String, Any>()
    private val updatedChannel = MutableStateFlow(0)

    @RawAccess
    val settings = settingsStore.forNameSpace(nameSpace = SettingsStore.preferencesNamespace)


    private fun edit(block: Settings.() -> Unit) {
        settings.block()
        updatedChannel.value++
    }


    fun reset() = edit { clear() }

    private inline fun <reified T : Any> getCachedJsonItem(key: String, load: (String) -> T) = parsedItemCache.getOrPut(key) { load(key) } as T

    val updatedFlow
        get() = flow {
            emit(Unit)
            emitAll(updatedChannel.asStateFlow().map { })
        }

    val updatedFlow2
        get() = flow {
            emit(this@OctoPreferences)
            emitAll(updatedChannel.asStateFlow().map { this@OctoPreferences })
        }

    init {
        // Upgrade dark mode
        if (settings.getBoolean("manual_dark_mode_enabled", false)) {
            edit {
                putString(KEY_APP_THEME, AppTheme.DARK.name)
            }
        }

        // Delete legacy
        edit {
            remove("print_notification_was_paused")
            remove("manual_dark_mode_enabled")
        }
    }

    var wasPrintNotificationDisconnected: Boolean
        get() = settings.getBoolean(KEY_PRINT_NOTIFICATION_WAS_DISCONNECTED, false)
        set(value) {
            edit { putBoolean(KEY_PRINT_NOTIFICATION_WAS_DISCONNECTED, value) }
        }

    var wasPrintNotificationDisabledUntilNextLaunch: Boolean
        get() = settings.getBoolean(KEY_PRINT_NOTIFICATION_WAS_DISABLED_UNTIL_NEXT_LAUNCH, false)
        set(value) {
            edit { putBoolean(KEY_PRINT_NOTIFICATION_WAS_DISABLED_UNTIL_NEXT_LAUNCH, value) }
        }

    var isAnalyticsEnabled: Boolean
        get() = settings.getBoolean(KEY_ANALYTICS, true)
        set(value) {
            edit { putBoolean(KEY_ANALYTICS, value) }
        }

    var isCrashReportingEnabled: Boolean
        get() = settings.getBoolean(KEY_CRASH_REPORTING, true)
        set(value) {
            edit { putBoolean(KEY_CRASH_REPORTING, value) }
        }

    var activeInstanceId: String?
        get() = settings.getStringOrNull(SettingsStore.activeInstanceIdKey)
        set(value) {
            edit { value?.let { putString(SettingsStore.activeInstanceIdKey, it) } ?: remove(SettingsStore.activeInstanceIdKey) }
        }

    var remoteAccessAnnouncementHiddenAt: Instant?
        get() = settings.getLong(KEY_REMOTE_ACCESS_ANNOUNCEMENT_HIDDEN_AT, 0).takeIf { it > 0 }?.let { Instant.fromEpochMilliseconds(it) }
        set(value) {
            edit { putLong(KEY_REMOTE_ACCESS_ANNOUNCEMENT_HIDDEN_AT, value?.toEpochMilliseconds() ?: 0) }
        }

    var tutorialsSeenAt: Instant?
        get() = settings.getLong(KEY_TUTORIALS_SEEN_AT, 0).takeIf { it > 0 }?.let { Instant.fromEpochMilliseconds(it) }
        set(value) {
            edit { putLong(KEY_TUTORIALS_SEEN_AT, value?.toEpochMilliseconds() ?: 0) }
        }

    var isKeepScreenOnDuringPrint
        get() = settings.getBoolean(KEY_KEEP_SCREEN_ON, false)
        set(value) {
            edit { putBoolean(KEY_KEEP_SCREEN_ON, value) }
        }

    var isAutoConnectPrinter
        get() = settings.getBoolean(KEY_AUTO_CONNECT_PRINTER, false)
        set(value) {
            edit { putBoolean(KEY_AUTO_CONNECT_PRINTER, value) }
        }

    var wasAutoConnectPrinterInfoShown
        get() = settings.getBoolean(KEY_AUTO_CONNECT_PRINTER_INFO_SHOWN, false)
        set(value) {
            edit { putBoolean(KEY_AUTO_CONNECT_PRINTER_INFO_SHOWN, value) }
        }

    var isLivePrintNotificationsEnabled
        get() = settings.getBoolean(KEY_PRINT_NOTIFICATION_ENABLED, true)
        set(value) {
            edit { putBoolean(KEY_PRINT_NOTIFICATION_ENABLED, value) }
        }

    var appTheme
        get() = AppTheme.valueOf(settings.getStringOrNull(KEY_APP_THEME) ?: AppTheme.AUTO.name)
        set(value) {
            edit { putString(KEY_APP_THEME, value.name) }
        }

    var appLanguage
        get() = settings.getStringOrNull(KEY_APP_LANGUAGE)
        set(value) {
            edit { value?.let { putString(KEY_APP_LANGUAGE, it) } ?: remove(KEY_APP_LANGUAGE) }
        }

    var allowAppRotation
        get() = settings.getBoolean(KEY_ALLOW_APP_ROTATION, false)
        set(value) {
            edit { putBoolean(KEY_ALLOW_APP_ROTATION, value) }
        }

    var swipeConfirmations
        get() = settings.getBoolean(KEY_NO_SWIPE_CONFIRMATIONS, true)
        set(value) {
            edit { putBoolean(KEY_NO_SWIPE_CONFIRMATIONS, value) }
        }

    var allowNotificationBatterySaver
        get() = settings.getBoolean(KEY_ALLOW_NOTIFICATION_BATTERY_SAVER, true)
        set(value) {
            edit { putBoolean(KEY_ALLOW_NOTIFICATION_BATTERY_SAVER, value) }
        }

    var hideThumbnailHintUntil: Instant
        get() = Instant.fromEpochMilliseconds(settings.getLong(KEY_HIDE_THUMBNAIL_HINT_UNTIL, 0))
        set(value) {
            edit { putLong(KEY_HIDE_THUMBNAIL_HINT_UNTIL, value.toEpochMilliseconds()) }
        }

    var automaticLights: Set<String>
        get() = settings.getString(KEY_AUTO_LIGHTS, "").split(SET_BARRIER).toSet()
        set(value) {
            edit { putString(KEY_AUTO_LIGHTS, value.joinToString(SET_BARRIER)) }
        }

    var confirmPowerOffDevices: Set<String>
        get() = settings.getString(KEY_CONFIRM_POWER_OFF_DEVICES, "").split(SET_BARRIER).toSet()
        set(value) {
            edit { putString(KEY_CONFIRM_POWER_OFF_DEVICES, value.joinToString(SET_BARRIER)) }
        }

    var automaticLightsForWidgetRefresh
        get() = settings.getBoolean(KEY_AUTO_LIGHTS_FOR_WIDGET_REFRESH, false)
        set(value) {
            edit { putBoolean(KEY_AUTO_LIGHTS_FOR_WIDGET_REFRESH, value) }
        }

    var isShowWebcamResolution
        get() = settings.getBoolean(KEY_SHOW_WEBCAM_RESOLUTION, true)
        set(value) {
            edit { putBoolean(KEY_SHOW_WEBCAM_RESOLUTION, value) }
        }

    var isShowWebcamName
        get() = settings.getBoolean(KEY_SHOW_WEBCAM_NAME, true)
        set(value) {
            edit { putBoolean(KEY_SHOW_WEBCAM_NAME, value) }
        }

    var isOverrideRotationLockForWebcam
        get() = settings.getBoolean(KEY_OVERRIDE_ROTATION_LOCK_FOR_WEBCAM, false)
        set(value) {
            edit { putBoolean(KEY_OVERRIDE_ROTATION_LOCK_FOR_WEBCAM, value) }
        }

    var webcamAspectRatioSource
        get() = settings.getString(KEY_WEBCAM_ASPECT_RATIO_SOURCE, VALUE_WEBCAM_ASPECT_RATIO_SOURCE_IMAGE)
        set(value) {
            edit { putString(KEY_WEBCAM_ASPECT_RATIO_SOURCE, value) }
        }

    var suppressM115Request
        get() = settings.getBoolean(KEY_SUPPRESS_M115, false)
        set(value) {
            edit { putBoolean(KEY_SUPPRESS_M115, value) }
        }

    var allowTerminalDuringPrint
        get() = settings.getBoolean(KEY_ALLOW_TERMINAL_DURING_PRINT, false)
        set(value) {
            edit { putBoolean(KEY_ALLOW_TERMINAL_DURING_PRINT, value) }
        }

    var suppressRemoteMessageInitialization
        get() = settings.getBoolean(KEY_SUPPRESS_REMOTE_NOTIFICATIONS_INIT, false)
        set(value) {
            edit { putBoolean(KEY_SUPPRESS_REMOTE_NOTIFICATIONS_INIT, value) }
        }

    var debugNetworkLogging
        get() = settings.getBoolean(KEY_DEBUG_NETWORK_LOGGING, false)
        set(value) {
            edit { putBoolean(KEY_DEBUG_NETWORK_LOGGING, value) }
        }

    var enforceIPv4
        get() = settings.getBoolean(KEY_ENFORCE_IP_V4, false)
        set(value) {
            edit { putBoolean(KEY_ENFORCE_IP_V4, value) }
        }

    var recordWebcamForDebug
        get() = settings.getBoolean(KEY_RECORD_WEBCAM_FOR_DEBUG, false)
        set(value) {
            edit { putBoolean(KEY_RECORD_WEBCAM_FOR_DEBUG, value) }
        }

    var notifyPrinterBeep
        get() = settings.getBoolean(KEY_NOTIFY_PRINTER_BEEP, false)
        set(value) {
            edit { putBoolean(KEY_NOTIFY_PRINTER_BEEP, value) }
        }

    var notifyForLayers
        get() = settings.getString(KEY_NOTIFY_FOR_LAYERS, "1,3").split(",").mapNotNull { it.trim().toIntOrNull() }
        set(value) {
            edit { putString(KEY_NOTIFY_FOR_LAYERS, value = value.joinToString(",")) }
        }

    var notifyForUserInteraction
        get() = settings.getBoolean(KEY_NOTIFY_FOR_USER_INTERACTION, true)
        set(value) {
            edit { putBoolean(KEY_NOTIFY_FOR_USER_INTERACTION, value = value) }
        }

    var compactLayout
        get() = settings.getBoolean(KEY_COMPACT_LAYOUT, false)
        set(value) {
            edit { putBoolean(KEY_COMPACT_LAYOUT, value) }
        }

    var emergencyStopInBottomBar
        get() = settings.getBoolean(KEY_EMERGENCY_STOP_IN_BOTTOM_BAR, false)
        set(value) {
            edit { putBoolean(KEY_EMERGENCY_STOP_IN_BOTTOM_BAR, value) }
        }

    var askForTimelapseBeforePrinting
        get() = settings.getBoolean(KEY_ASK_FOR_TIMELAPSE_BEFORE_PRINTING, false)
        set(value) {
            edit { putBoolean(KEY_ASK_FOR_TIMELAPSE_BEFORE_PRINTING, value) }
        }

    var leftHandMode
        get() = settings.getBoolean(KEY_LEFT_HAND_MODE, false)
        set(value) {
            edit { putBoolean(KEY_LEFT_HAND_MODE, value) }
        }

    var moveDuringPrint
        get() = settings.getBoolean(KEY_MOVE_DURING_PRINT, false)
        set(value) {
            edit { putBoolean(KEY_MOVE_DURING_PRINT, value) }
        }

    var gcodePreviewSettings: GcodePreviewSettings
        get() = getCachedJsonItem(KEY_GCODE_PREVIEW) { key ->
            settings.getSerializableOrNull<GcodePreviewSettings>(key) ?: GcodePreviewSettings()
        }
        set(value) {
            parsedItemCache.remove(KEY_GCODE_PREVIEW)
            edit { putSerializable(KEY_GCODE_PREVIEW, value) }
        }

    var fileManagerSettings: FileManagerSettings
        get() = getCachedJsonItem(KEY_FILE_MANAGER) { key ->
            settings.getSerializableOrNull<FileManagerSettings>(key) ?: FileManagerSettings()
        }
        set(value) {
            parsedItemCache.remove(KEY_FILE_MANAGER)
            edit { putSerializable(KEY_FILE_MANAGER, value) }
        }

    var controlCenterSettings: ControlCenterSettings
        get() = getCachedJsonItem(KEY_CONTROL_CENTER) { key ->
            settings.getSerializableOrNull<ControlCenterSettings>(key) ?: ControlCenterSettings()
        }
        set(value) {
            parsedItemCache.remove(KEY_CONTROL_CENTER)
            edit { putSerializable(KEY_CONTROL_CENTER, value) }
        }

    var progressWidgetSettings: ProgressWidgetSettings
        get() = getCachedJsonItem(KEY_PROGRESS_WIDGET) { key ->
            settings.getSerializableOrNull(key) ?: ProgressWidgetSettings()
        }
        set(value) {
            parsedItemCache.remove(KEY_PROGRESS_WIDGET)
            edit { putSerializable(KEY_PROGRESS_WIDGET, value) }
        }

    var webcamFullscreenProgressWidgetSettings: ProgressWidgetSettings
        get() = getCachedJsonItem(KEY_PROGRESS_FULLSCREEN_WIDGET) { key ->
            settings.getSerializableOrNull(key) ?: ProgressWidgetSettings(printNameStyle = ProgressWidgetSettings.PrintNameStyle.None)
        }
        set(value) {
            parsedItemCache.remove(KEY_PROGRESS_FULLSCREEN_WIDGET)
            edit { putSerializable(KEY_PROGRESS_FULLSCREEN_WIDGET, value) }
        }

    var mediaCacheSize
        get() = settings.getLong(KEY_MEDIA_CACHE_SIZE, 128 * 1024 * 1024L /* 128 MiB */)
        set(value) {
            edit { putLong(KEY_MEDIA_CACHE_SIZE, value) }
        }

    var gcodeCacheSize
        get() = settings.getLong(KEY_GCODE_CACHE_SIZE, 128 * 1024 * 1024L /* 128 MiB */)
        set(value) {
            edit { putLong(KEY_GCODE_CACHE_SIZE, value) }
        }

    var askNotificationPermissionNotBefore: Instant
        get() = Instant.fromEpochMilliseconds(settings.getLong(KEY_ASK_NOTIFICATION_PERMISSION_NOT_BEFORE, 0L))
        set(value) {
            edit { putLong(KEY_ASK_NOTIFICATION_PERMISSION_NOT_BEFORE, value.toEpochMilliseconds()) }
        }

    var askCompanionInstallNotBefore: Instant
        get() = Instant.fromEpochMilliseconds(settings.getLong(KEY_ASK_COMPANION_INSTALL_NOT_BEFORE, 0L))
        set(value) {
            edit { putLong(KEY_ASK_COMPANION_INSTALL_NOT_BEFORE, value.toEpochMilliseconds()) }
        }

    var notificationPermissionRequestState
        get() = settings.getInt(KEY_NOTIFICATION_PERMISSION_REQUEST_STATE, 0)
        set(value) {
            edit { putInt(KEY_NOTIFICATION_PERMISSION_REQUEST_STATE, value) }
        }

    var httpCacheSize
        get() = settings.getLong(KEY_HTTP_CACHE_SIZE, 128 * 1024 * 1024L /* 128 MiB */)
        set(value) {
            edit { putLong(KEY_HTTP_CACHE_SIZE, value) }
        }

    var dataSaverWebcamInterval
        get() = settings.getLong(KEY_DATA_SAVER_WEBCAM_INTERVAL, 0L /* Off */)
        set(value) {
            edit { putLong(KEY_DATA_SAVER_WEBCAM_INTERVAL, value) }
        }

    var dataSaverUseOnMobileNetworkOnly
        get() = settings.getBoolean(KEY_DATA_USE_ON_MOBILE_NETWORK_ONLY, true)
        set(value) {
            edit { putBoolean(KEY_DATA_USE_ON_MOBILE_NETWORK_ONLY, value) }
        }

    var showActiveInstanceInStatusBar
        get() = settings.getBoolean(KEY_SHOWACTIVE_INSTANCE_IN_STATUS, false)
        set(value) {
            edit { putBoolean(KEY_SHOWACTIVE_INSTANCE_IN_STATUS, value) }
        }

    var websocketTransportAllowed
        get() = settings.getBoolean(KEY_WEBSOCKET_TRANSPORT_ALLOWED, true)
        set(value) {
            edit { putBoolean(KEY_WEBSOCKET_TRANSPORT_ALLOWED, value) }
        }

    var activePluginIntegrations
        get() = settings.getBoolean(KEY_ACTIVE_PLUGIN_INTEGRATIONS, true)
        set(value) {
            edit { putBoolean(KEY_ACTIVE_PLUGIN_INTEGRATIONS, value) }
        }

    var longTimeouts
        get() = settings.getBoolean(KEY_LONG_TIMEOUTS, false)
        set(value) {
            edit { putBoolean(KEY_LONG_TIMEOUTS, value) }
        }

    var showAllMaterialProperties
        get() = settings.getBoolean(KEY_SHOW_ALL_MATERIAL_PROPERTIES, false)
        set(value) {
            edit { putBoolean(KEY_SHOW_ALL_MATERIAL_PROPERTIES, value) }
        }

    var gcodeHiddenIos
        get() = settings.getBoolean(KEY_GCODE_HIDDEN_IOS, false)
        set(value) {
            edit { putBoolean(KEY_GCODE_HIDDEN_IOS, value) }
        }

    var detectLayersFromSlicer
        get() = settings.getBoolean(DETECT_LAYERS_FROM_SLICER, true)
        set(value) {
            edit { putBoolean(DETECT_LAYERS_FROM_SLICER, value) }
        }

    var didReceiveTransfer
        get() = settings.getBoolean(KEY_DID_RECEIVE_TRANSFER, false)
        set(value) {
            edit { putBoolean(KEY_DID_RECEIVE_TRANSFER, value) }
        }

    var notificationsLayout
        get() = settings.getString(KEY_NOTIFICATIONS_LAYOUT, VALUE_NOTIFICATIONS_LAYOUT_SNAPSHOT_PROGRESS)
        set(value) {
            edit { putString(KEY_NOTIFICATIONS_LAYOUT, value) }
        }

    var mDnsSystem: String?
        get() = settings.getStringOrNull(KEY_MDNS_SYSTEM)
        set(value) {
            edit { value?.let { putString(KEY_MDNS_SYSTEM, it) } ?: remove(KEY_MDNS_SYSTEM) }
        }

    val installId: String
        get() = settings.getStringOrNull(KEY_INSTALL_ID) ?: let {
            val id = uuid4().toString()
            settings.putString(KEY_INSTALL_ID, id)
            id
        }

    var reviewFlowConditions: ReviewFlowConditions
        get() = getCachedJsonItem(REVIEW_FLOW_CONDITIONS) { key ->
            settings.getSerializableOrNull(key) ?: ReviewFlowConditions()
        }
        set(value) {
            parsedItemCache.remove(REVIEW_FLOW_CONDITIONS)
            edit { putSerializable(REVIEW_FLOW_CONDITIONS, value) }
        }

}