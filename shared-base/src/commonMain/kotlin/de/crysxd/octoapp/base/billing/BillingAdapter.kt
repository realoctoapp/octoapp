package de.crysxd.octoapp.base.billing

import de.crysxd.octoapp.base.models.BillingProduct
import de.crysxd.octoapp.base.models.BillingPurchase

interface BillingAdapter {
    @Throws(Throwable::class)
    suspend fun initBilling(purchasesUpdated: (List<BillingPurchase>) -> Unit, notifyPurchaseCompleted: () -> Unit)

    @Throws(Throwable::class)
    suspend fun destroyBilling()

    @Throws(Throwable::class)
    suspend fun loadProducts(productIds: List<String>): List<BillingProduct>

    @Throws(Throwable::class)
    suspend fun loadPurchases(): List<BillingPurchase>

    fun isAvailable(): Boolean
}