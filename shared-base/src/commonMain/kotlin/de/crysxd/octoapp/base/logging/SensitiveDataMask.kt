package de.crysxd.octoapp.base.logging

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.http.framework.redactLoggingString
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.client.call.NoTransformationFoundException
import io.ktor.http.Url
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

object SensitiveDataMask {

    private val lock = Mutex()
    private val masks = mutableMapOf<String, (String) -> String>()

    suspend fun registerWebUrl(webUrl: Url?) = lock.withLock {
        masks[webUrl.toString()] = {
            webUrl?.redactLoggingString(it) ?: it
        }
    }

    suspend fun registerApiKey(apiKey: String) = lock.withLock {
        if (apiKey.isNotBlank()) {
            masks[apiKey] = {
                it.replace(apiKey, "\${api_key}")
            }
        }
    }

    suspend fun registerInstance(instance: PrinterConfigurationV3) {
        registerWebUrl(instance.webUrl)
        registerWebUrl(instance.alternativeWebUrl)
        registerApiKey(instance.apiKey)
        instance.settings?.webcam?.webcams?.forEach { webcam ->
            registerWebUrl(webcam.snapshotUrl?.toUrlOrNull())
            registerWebUrl(webcam.streamUrl?.toUrlOrNull())
        }
        instance.settings?.plugins?.multiCamSettings?.webcams?.forEach { webcam ->
            registerWebUrl(webcam.streamUrl?.toUrlOrNull())
            registerWebUrl(webcam.streamUrl?.toUrlOrNull())
        }
        instance.settings?.plugins?.ngrok?.let { ngrok ->
            ngrok.authName?.let { n ->
                masks[n] = {
                    it.replace(n, "\${ngrokAuthName}")
                }
            }
            ngrok.authPassword?.let { n ->
                masks[n] = {
                    it.replace(n, "\${ngrokAuthPassword}")
                }
            }
        }
    }

    suspend fun mask(input: String): String = lock.withLock {
        var output = input

        masks.forEach {
            output = it.value(output)
        }

        // Special cases
        if (output.startsWith("Expected URL scheme")) {
            output = "<<Masked: URL scheme mismatch>>"
        }

        return output
    }

    suspend fun needsMasking(throwable: Throwable?): Boolean {
        // Already masked
        if (throwable == null) return false
        if (throwable is NetworkException) return false

        // Known offenders
        if (throwable is NoTransformationFoundException) return true
        if (throwable is IllegalArgumentException) return true

        // Test
        return mask(throwable.message ?: "") != throwable.message || needsMasking(throwable.cause)
    }
}