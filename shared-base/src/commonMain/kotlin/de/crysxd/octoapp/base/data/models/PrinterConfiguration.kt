package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.base.data.models.ControlCenterSettings.SortBy.Color
import de.crysxd.octoapp.base.data.models.ControlCenterSettings.SortBy.Label
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.version.VersionInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.utils.UrlSerializer
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.serialization.Serializable
import kotlin.reflect.KClass

@Serializable
data class PrinterConfigurationV3(
    val id: String,
    val type: BackendType = BackendType.OctoPrint,
    val version: VersionInfo? = null,
    val capabilitiesFetchedAt: Long? = null,
    val notificationId: Int? = null,
    @Serializable(with = UrlSerializer::class) val webUrl: Url,
    @Serializable(with = UrlSerializer::class) val alternativeWebUrl: Url? = null,
    val alternativeWebUrlPlugin: String? = null,
    val apiKey: String,
    val m115Response: String? = null,
    val systemInfo: SystemInfo? = null,
    val settings: Settings? = null,
    val macros: List<Macro>? = null,
    val activeProfile: PrinterProfile? = null,
    val systemCommands: List<SystemCommand>? = null,
    val appSettings: AppSettings? = null,
    val availablePlugins: Map<String, String?>? = null,
    val octoEverywhereConnection: OctoEverywhereConnection? = null,
    val remoteConnectionFailure: RemoteConnectionFailure? = null,
    val lastModifiedAt: Long? = null,
    val affectedByMoonrakerDetectionChange: Boolean? = null,
) : Comparable<PrinterConfigurationV3> {

    companion object {
        private val colorComparator = compareBy<PrinterConfigurationV3> { it.colors.light.main.hue }.thenBy { it.label }.thenBy { it.id }
        private val labelComparator = compareBy<PrinterConfigurationV3> { it.label }.thenBy { it.colors.light.main.hue }.thenBy { it.id }
        private val comparator: () -> Comparator<PrinterConfigurationV3> = {
            when (SharedBaseInjector.getOrNull()?.preferences?.controlCenterSettings?.sortBy ?: ControlCenterSettings().sortBy) {
                Color -> colorComparator
                Label -> labelComparator
            }
        }
    }

    val label
        get() = settings?.appearance?.name?.takeIf {
            it.isNotBlank()
        } ?: webUrl.let { url ->
            val host = url.host
            val port = ":${url.port}".takeIf { url.protocol.defaultPort != url.port } ?: ""
            if (host.startsWith(UPNP_ADDRESS_PREFIX)) {
                "OctoPrint via UPnP (${host.hashCode().toString(radix = 16).take(3)})"
            } else {
                "$host$port"
            }
        }

    fun hasCompanionPlugin() = settings?.plugins?.let {
        compareVersions(
            plugin = OctoPlugins.OctoApp,
            hasPlugin = it.octoAppCompanion != null,
            version = it.octoAppCompanion?.version,
            minVersion = if (systemInfo?.interfaceType == SystemInfo.Interface.OctoPrint) "1.3.0" else "2.0.0",
            maxVersion = null,
            log = true
        )
    }

    fun hasPlugin(plugin: String, minVersion: String? = null, maxVersion: String? = null, log: Boolean = false): Boolean {
        // Special sauce for OctoApp
        if (plugin == OctoPlugins.OctoApp) {
            return hasCompanionPlugin() == true
        }

        // No OctoPrint, no plugins
        if (systemInfo?.interfaceType != SystemInfo.Interface.OctoPrint) {
            return when (plugin) {
                OctoPlugins.OctoEverywhere -> settings?.plugins?.octoEverywhere != null
                OctoPlugins.Obico -> settings?.plugins?.obico != null
                OctoPlugins.Spoolman -> settings?.plugins?.spoolman != null
                else -> false
            }
        }

        // We don't have a real plugin list...fall back on settings check
        // This can be the case with OctoPrint 1.8 and older if the plugin manager permission is missing
        if (availablePlugins == null) {
            Napier.e(tag = "PrinterConfiguration", message = "Missing available plugins!")
            return false
        }

        // Use plugin list for check
        return compareVersions(
            plugin = plugin,
            hasPlugin = availablePlugins.containsKey(plugin),
            version = availablePlugins[plugin],
            minVersion = minVersion,
            maxVersion = maxVersion,
            log = log
        )
    }

    private fun compareVersions(plugin: String, hasPlugin: Boolean, version: String?, minVersion: String?, maxVersion: String?, log: Boolean): Boolean {
        // Lenient version check...we assume it's ok if we don't know the version (shouldn't happen)
        val matchesMinVersion = when {
            minVersion == null -> true
            version == null -> true
            else -> version.asVersion() >= minVersion.asVersion()
        }

        val matchesMaxVersion = when {
            maxVersion == null -> true
            version == null -> true
            else -> maxVersion.asVersion() >= version.asVersion()
        }

        if (log) {
            Napier.i(tag = "hasPlugin", message = "Using plugin list (plugin=$plugin minVersion=$minVersion maxVersion=$maxVersion)")
            Napier.i(
                tag = "hasPlugin",
                message = "hasPlugin=$hasPlugin version=$hasPlugin, matchesMinVersion=$matchesMinVersion, matchesMaxVersion=$matchesMaxVersion pluginList=$availablePlugins"
            )
        }

        return hasPlugin && matchesMinVersion && matchesMaxVersion
    }

    override fun compareTo(other: PrinterConfigurationV3) = comparator().compare(this, other)

    override fun toString() = StringBuilder().also {
        it.append("OctoPrintInstanceInformationV3(")
        it.append("id=$id ")
        it.append("webUrl=$webUrl ")
        it.append("alternativeWebUrl=$alternativeWebUrl ")
        it.append("notificationId=$notificationId ")
        it.append("apiKey=$apiKey ")
        it.append("m115Response=${if (m115Response == null) null else "..."} ")
        it.append("settings=$settings ")
        it.append("activeProfile=$activeProfile ")
        it.append("systemCommands=${if (systemCommands == null) null else "..."} ")
        it.append("appSettings=$appSettings ")
        it.append("octoEverywhereConnection=${if (octoEverywhereConnection == null) null else "..."}\" ")
        it.append("remoteConnectionFailure=${remoteConnectionFailure.toString().take(64)}")
        it.append("availablePlugins=$availablePlugins")
        it.append(")")
    }.toString()

}

val PrinterConfigurationV3?.colors get() = this?.settings?.appearance?.colors ?: Settings.Appearance.Colors()

fun PrinterConfigurationV3?.hasPlugin(plugin: KClass<out Settings.PluginSettings>) = when (plugin) {
    Settings.Obico::class -> this?.settings?.plugins?.obico != null
    Settings.CancelObject::class -> this?.settings?.plugins?.cancelObject != null
    Settings.Mmu::class -> this?.settings?.plugins?.mmu != null
    Settings.MultiCam::class -> this?.settings?.plugins?.multiCamSettings != null
    Settings.Ngrok::class -> this?.settings?.plugins?.ngrok != null
    Settings.UploadAnything::class -> this?.settings?.plugins?.uploadAnything != null
    Settings.OctoAppCompanion::class -> this?.settings?.plugins?.octoAppCompanion != null
    Settings.OctoEverywhere::class -> this?.settings?.plugins?.octoEverywhere != null
    else -> false
}