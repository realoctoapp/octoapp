package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.sharedcommon.io.FileManager

class DeleteTimelapseUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val fileManager: FileManager,
    private val timelapseRepository: TimelapseRepository,
) : UseCase2<DeleteTimelapseUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        printerEngineProvider.printer(instanceId = param.instanceId).timelapseApi.delete(timelapseFile = param.timelapseFile)
        timelapseRepository.fetchLatest(param.instanceId)
        fileManager.forNameSpace(ClearPublicFilesUseCase.PublicFilesNameSpace).clear()
    }

    data class Params(
        val instanceId: String,
        val timelapseFile: TimelapseFile,
    )
}