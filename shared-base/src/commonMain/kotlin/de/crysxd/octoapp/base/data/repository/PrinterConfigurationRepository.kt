package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoAnalytics.UserProperty.HasKlipper
import de.crysxd.octoapp.base.OctoAnalytics.UserProperty.HasOctoPrint
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.engine.models.system.isMoonraker
import de.crysxd.octoapp.engine.models.system.isOctoPrint
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.io.getSerializableOrNull
import de.crysxd.octoapp.sharedcommon.io.printerConfigsKey
import de.crysxd.octoapp.sharedcommon.io.printerConfigsNamespace
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import de.crysxd.octoapp.sharedcommon.url.isBasedOn
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class PrinterConfigurationRepository(
    settingsStore: SettingsStore,
    private val sensitiveDataMask: SensitiveDataMask,
    private val octoPreferences: OctoPreferences,
    private val forceReadFromDisk: Boolean,
) {

    private val Tag = "PrinterConfigRepository"
    private val settings = settingsStore.forNameSpace(SettingsStore.printerConfigsNamespace)
    private val instanceInformationFlow = MutableStateFlow<Map<String, PrinterConfigurationV3>>(emptyMap())
    private val lock = Mutex()

    init {
        postInstances()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun instanceInformationFlow(instanceId: String? = null) = instanceInformationFlow.flatMapLatest { map ->
        if (instanceId == null) {
            octoPreferences.updatedFlow.map { octoPreferences.activeInstanceId }.distinctUntilChanged()
        } else {
            flowOf(instanceId)
        }.map { id ->
            id?.let { map[it] }
        }
    }

    fun allInstanceInformationFlow() = instanceInformationFlow

    fun getActiveInstanceSnapshot() = octoPreferences.activeInstanceId?.let { instanceInformationFlow.value[it] }

    private fun postInstances() = runBlocking {
        instanceInformationFlow.value = load()
            .onEach { sensitiveDataMask.registerInstance(it) }
            .associateBy { it.id }

        Napier.i(tag = Tag, message = "Posting ${instanceInformationFlow.value.size} instances: ${instanceInformationFlow.value.keys.joinToString()}")
    }

    private fun storeOctoprintInstanceInformation(id: String, instance: PrinterConfigurationV3?) {
        Napier.i(tag = Tag, message = "Updating $id to $instance")
        val updated = getAll().filter { it.id != id }.toMutableList()
        instance?.copy(
            affectedByMoonrakerDetectionChange = instance.affectedByMoonrakerDetectionChange ?: false
        )?.let { updated.add(it) }
        store(updated)
        postInstances()
    }

    fun add(instance: PrinterConfigurationV3, trigger: String) {
        Napier.i(tag = Tag, message = "Adding but inactive: ${instance.webUrl} (trigger=$trigger)")
        storeOctoprintInstanceInformation(instance.id, instance)
    }

    fun setActive(instance: PrinterConfigurationV3, trigger: String) {
        Napier.i(tag = Tag, message = "Setting as active: ${instance.webUrl} (trigger=$trigger, id=${instance.id})")
        storeOctoprintInstanceInformation(instance.id, instance)
        octoPreferences.activeInstanceId = instance.id
    }

    fun setActive(instanceId: String, trigger: String) {
        get(instanceId)?.let { setActive(it, trigger = trigger) } ?: let {
            Napier.e(tag = Tag, message = "Failed to activate $instanceId, not found")
            clearActive()
        }
    }

    suspend fun update(id: String, block: suspend (PrinterConfigurationV3) -> PrinterConfigurationV3?) {
        lock.withLock {
            get(id)?.let {
                val new = block(it)
                if (new != it) {
                    Napier.i(tag = Tag, message = "Updating instance with $id")
                    storeOctoprintInstanceInformation(it.id, new)
                } else {
                    Napier.v(tag = Tag, message = "Drop update, no changes")
                }
            }
        }
    }

    suspend fun updateAppSettings(id: String, block: suspend (AppSettings) -> AppSettings) {
        update(id) {
            it.copy(appSettings = block(it.appSettings ?: AppSettings()))
        }
    }

    fun clearActive() {
        Napier.i(tag = Tag, message = "Clearing active")
        octoPreferences.activeInstanceId = null
    }

    fun clear() {
        clearActive()
        store(emptyList())
        postInstances()
    }

    fun remove(id: String) {
        Napier.i(tag = Tag, message = "Removing $id")
        val all = getAll().filter { it.id != id }
        store(all)
        postInstances()
    }

    fun get(id: String?) = if (id == null) {
        getActiveInstanceSnapshot()
    } else {
        getAll().firstOrNull { it.id == id }
    }

    suspend fun atomicChange(block: suspend PrinterConfigurationRepository.() -> Unit) = lock.withLock {
        block(this)
    }

    fun getAll(): List<PrinterConfigurationV3> {
        if (forceReadFromDisk) {
            postInstances()
        }

        return instanceInformationFlow.value.values.toList().sorted()
    }

    fun setAll(all: List<PrinterConfigurationV3>) {
        store(all)
        postInstances()
    }

    @Deprecated("Do not use")
    fun findInstances(url: Url?) = getAll().mapNotNull {
        val u = url?.toString()?.toUrlOrNull() ?: return@mapNotNull null

        val webUrl = it.webUrl
        val alternativeWebUrl = it.alternativeWebUrl
        when {
            u.isBasedOn(webUrl) -> it to false
            u.isBasedOn(alternativeWebUrl) -> it to true
            else -> null
        }
    }

    fun importRaw(json: String) {
        settings.putString("configs", json)
        octoPreferences.activeInstanceId = null
        postInstances()
    }

    private fun load(): List<PrinterConfigurationV3> =
        (settings.getSerializableOrNull<List<PrinterConfigurationV3>>(SettingsStore.printerConfigsKey) ?: emptyList())

    private fun store(value: List<PrinterConfigurationV3>) {
        settings.putSerializable(SettingsStore.printerConfigsKey, value)
        OctoAnalytics.setUserProperty(HasOctoPrint, value.any { it.systemInfo?.interfaceType?.isOctoPrint == true }.toString())
        OctoAnalytics.setUserProperty(HasKlipper, value.any { it.systemInfo?.interfaceType?.isMoonraker == true }.toString())
    }
}