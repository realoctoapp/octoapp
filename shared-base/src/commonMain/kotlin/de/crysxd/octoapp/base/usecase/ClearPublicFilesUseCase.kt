package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.sharedcommon.io.FileManager

class ClearPublicFilesUseCase(
    private val fileManager: FileManager,
) : UseCase2<Unit, Unit>() {

    companion object {
        const val PublicFilesNameSpace = "public"
    }

    override suspend fun doExecute(param: Unit, logger: Logger) {
        fileManager.forNameSpace(PublicFilesNameSpace).also {
            logger.i("Deleting ${it.totalCount()} files")
            it.clear()
        }
    }
}