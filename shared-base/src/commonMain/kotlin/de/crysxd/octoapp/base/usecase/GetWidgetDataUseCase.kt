package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import de.crysxd.octoapp.sharedcommon.io.FileManager
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedexternalapis.mjpeg.JpegCoder
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsChannel
import io.ktor.util.toByteArray
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withTimeout
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import okio.use
import kotlin.time.Duration.Companion.seconds

class GetWidgetDataUseCase(
    private val getWebcamSnapshotUseCase: () -> GetWebcamSnapshotUseCase,
    private val printerConfigRepository: () -> PrinterConfigurationRepository,
    private val printerEngineProvider: () -> PrinterEngineProvider,
    private val platform: () -> Platform,
    fileManager: () -> FileManager
) : UseCase2<GetWidgetDataUseCase.Params, GetWidgetDataUseCase.Result>() {

    companion object {
        private val cache = mutableMapOf<String, Pair<Instant, Result>>()
        private val snapshotCache = mutableMapOf<String, Instant>()
        private val locks = mutableMapOf<String, Mutex>()

        fun invalidateCache() = cache.clear()
    }

    private val files = fileManager().forNameSpace("widget-snapshots")
    private val configRepo by lazy { printerConfigRepository() }
    private val engineProvider by lazy { printerEngineProvider() }

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val config = configRepo.get(param.instanceId) ?: configRepo.getActiveInstanceSnapshot() ?: return Result(
            snapshotPath = getBackupSnapshot(param.instanceId),
            snapshotSuccess = false,
            statusText = "Printer ${param.instanceId} not found",
            label = param.instanceId,
            instanceId = param.instanceId,
            errors = "Printer not found, relink widget"
        )

        return try {
            withTimeout(60.seconds) {
                getLock(param.instanceId).withLock {
                    logger.i("Acquired lock for ${param.instanceId}")
                    val data = if (param.loadData) {
                        cache.get(key = param.instanceId)?.takeIf { (instant, _) ->
                            !param.skipCache && (Clock.System.now() - instant) < 60.seconds
                        }?.let { (instant, data) ->
                            logger.i("Returning cached data for ${param.instanceId} (${instant})")
                            data
                        } ?: performLoad(config, param, logger)
                    } else {
                        Result(
                            errors = null,
                            statusText = getString("app_widget___no_data"),
                            label = config.label,
                            instanceId = config.id,
                        )
                    }

                    val (snapshotPath, snapshotException) = if (param.loadSnapshot) {
                        loadSnapshot(config, logger)
                    } else {
                        null to null
                    }

                    val (thumbnailPath, thumbnailException) = if (param.loadThumbnail) {
                        loadThumbnail(config, data.job, logger)
                    } else {
                        null to null
                    }

                    data.copy(
                        snapshotPath = snapshotPath,
                        snapshotSuccess = snapshotException == null,
                        thumbnailPath = thumbnailPath,
                        errors = listOfNotNull(
                            snapshotException?.composedError?.let { "Snapshot error: $it" },
                            thumbnailException?.composedError?.let { "Thumbnail error: $it" },
                        ).takeIf { it.isNotEmpty() }?.joinToString("\n\n")
                    )
                }
            }
        } catch (e: Exception) {
            logger.e("Error while getting widget data", e)
            Result(
                snapshotPath = getBackupSnapshot(param.instanceId),
                snapshotSuccess = false,
                statusText = getString("app_widget___update_failed"),
                colors = config.settings?.appearance?.colors,
                label = config.label,
                instanceId = config.id,
                errors = e.composedError
            )
        } finally {
            if (platform().slaveApp) {
                logger.d("Triggering GC")
                engineProvider.destroyAll()
                Platform.gc()
            }
        }
    }

    private fun getLock(instanceId: String) = locks.getOrPut(instanceId) { Mutex() }

    private suspend fun performLoad(config: PrinterConfigurationV3, param: Params, logger: Logger): Result {
        val job = printerEngineProvider().printer(instanceId = config.id)
            .jobApi
            .getJob()

        return Result(
            snapshotPath = null,
            snapshotSuccess = false,
            state = job.state,
            statusText = job.statusText,
            progress = job.progress,
            label = config.label,
            instanceId = config.id,
            colors = config.settings?.appearance?.colors,
            job = job.info,
            errors = null,
        ).also {
            cache[param.instanceId] = Clock.System.now() to it
        }
    }

    private suspend fun loadSnapshot(config: PrinterConfigurationV3, logger: Logger): Pair<String?, Throwable?> {
        val lastSnapshotTime = snapshotCache[config.id] ?: Instant.DISTANT_PAST
        if (Clock.System.now() - lastSnapshotTime < 60.seconds && files.exists(config.id.snapshotFileName)) {
            logger.i("Reusing existing snapshot")
            return files.getPath(config.id.snapshotFileName).toString() to null
        }

        if (files.exists(config.id.snapshotFileName)) {
            logger.i("Moving snapshot to backup location")
            files.rename(config.id.snapshotFileName, config.id.snapshotFileNameBackup)
        }

        logger.i("Loading snapshot")
        return try {
            getWebcamSnapshotUseCase().execute(
                param = GetWebcamSnapshotUseCase.Params(
                    instanceId = config.id,
                    webcamIndex = config.appSettings?.activeWebcamIndex ?: 0,
                    illuminateIfPossible = true,
                    interval = null,
                    maxSize = 960,
                )
            ).first().let { snapshot ->
                val encoder = JpegCoder(usePool = false, logTag = "JpegDecoder", maxImageSize = null)
                val bytes = encoder.encode(snapshot.bitmap, quality = 0.3f)
                files.writeCacheFile(config.id.snapshotFileName).use {
                    it.write(fileOffset = 0, array = bytes, arrayOffset = 0, byteCount = bytes.size)
                }
                snapshotCache[config.id] = Clock.System.now()
                files.getPath(config.id.snapshotFileName).toString() to null
            }
        } catch (e: Exception) {
            logger.e(message = "Failed to load snpashot", e)
            getBackupSnapshot(config.id) to e
        }
    }

    private suspend fun loadThumbnail(config: PrinterConfigurationV3, job: JobInformation?, logger: Logger): Pair<String?, Throwable?> {
        val jobFile = job?.file ?: return null to null

        // Check for existing file
        val prefix = "${config.id}-thumb-"
        val fileName = "$prefix${jobFile.thumbnailFileName}.jpeg"
        if (files.exists(fileName)) {
            logger.i("Rerturning existing thumbnail")
            return files.getPath(fileName).toString() to null
        }

        // Delete old files
        files.list().filter { it.startsWith(prefix) }.forEach {
            logger.i("Deleting old file: $it")
            files.deleteCacheFile(it)
        }

        return try {
            val engine = printerEngineProvider().printer(instanceId = config.id)
            val file = engine.filesApi.getFile(origin = jobFile.origin, path = jobFile.path, isDirectory = false) as FileObject.File
            val thumbnailPath = file.mediumThumbnail?.path ?: let {
                logger.w(message = "No thumbnail found")
                return null to null
            }

            logger.i("Loading thumbnail from $thumbnailPath")
            val bytes = engine.genericRequest { url, httpClient ->
                httpClient.get(url.resolve(thumbnailPath))
            }.bodyAsChannel().toByteArray()

            files.writeCacheFile(fileName).use {
                it.write(fileOffset = 0, array = bytes, arrayOffset = 0, byteCount = bytes.size)
            }

            files.getPath(fileName).toString() to null
        } catch (e: Exception) {
            logger.e(message = "Failed to load thumbnail", e)
            null to e
        }
    }

    private fun getBackupSnapshot(configId: String) = if (files.exists(configId.snapshotFileNameBackup)) {
        files.getPath(configId.snapshotFileNameBackup).toString()
    } else {
        null
    }

    private val FileReference.File.thumbnailFileName get() = "$name-${date.toEpochMilliseconds()}".hashCode().toString()

    private val String.snapshotFileName get() = "$this.jpeg"

    private val String.snapshotFileNameBackup get() = "$this.backup.jpeg"

    private val Throwable.composedError
        get() = (this as? UserMessageException)?.userMessage?.toString()?.let {
            "$it (${this::class.simpleName}: ${message})"
        } ?: message?.let {
            "${this::class.simpleName}: $message"
        } ?: "${this::class.simpleName}: ${getString("error_general")}"

    data class Result(
        val snapshotPath: String? = null,
        val snapshotSuccess: Boolean = false,
        val thumbnailPath: String? = null,
        val state: PrinterState.State? = null,
        val progress: ProgressInformation? = null,
        val colors: Settings.Appearance.Colors? = null,
        val job: JobInformation? = null,
        val statusText: String,
        val label: String,
        val instanceId: String,
        val errors: String?,
    )

    data class Params(
        val instanceId: String,
        val loadData: Boolean,
        val loadSnapshot: Boolean,
        val loadThumbnail: Boolean,
        val skipCache: Boolean,
    )
}