package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.ext.awaitFileChange
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.files.FileObject

class CreateFolderUseCase(
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<CreateFolderUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        // Trigger creation, the server will complete the action async
        printerEngineProvider.printer(param.instanceId).filesApi.createFolder(
            name = param.name,
            parent = param.parent,
        )

        // Await changes
        printerEngineProvider.awaitFileChange(instanceId = param.instanceId)
    }

    data class Params(
        val parent: FileObject.Folder,
        val name: String,
        val instanceId: String? = null
    )
}