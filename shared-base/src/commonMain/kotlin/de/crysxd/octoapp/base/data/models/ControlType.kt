package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
enum class ControlType {

    @SerialName("AnnouncementWidget")
    AnnouncementWidget,

    @SerialName("ControlTemperatureWidget")
    ControlTemperatureWidget,

    @SerialName("ExtrudeWidget")
    ExtrudeWidget,

    @SerialName("GcodePreviewWidget")
    GcodePreviewWidget,

    @SerialName("MoveToolWidget")
    MoveToolWidget,

    @SerialName("ProgressWidget")
    ProgressWidget,

    @SerialName("QuickAccessWidget")
    QuickAccessWidget,

    @SerialName("SendGcodeWidget")
    SendGcodeWidget,

    @SerialName("TuneWidget")
    TuneWidget,

    @SerialName("WebcamWidget")
    WebcamWidget,

    @SerialName("QuickPrint")
    QuickPrint,

    @SerialName("CancelObject")
    CancelObject,

    @SerialName("MultiTool")
    MultiTool,

    @SerialName("Connect")
    Connect,

    @SerialName("BedMesh")
    BedMesh,

    @SerialName("SaveConfig")
    SaveConfig,

    //region Deprecated
    @Deprecated("Do not use, use QuickAccessWidget")
    @SerialName("PrintQuickAccessWidget")
    PrintQuickAccessWidget,

    @Deprecated("Do not use, use QuickAccessWidget")
    @SerialName("PrepareQuickAccessWidget")
    PrepareQuickAccessWidget,
    //endregion
}