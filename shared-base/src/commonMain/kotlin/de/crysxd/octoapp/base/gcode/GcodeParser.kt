package de.crysxd.octoapp.base.gcode

import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.Bambu
import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.Cura
import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.IdeaMaker
import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.Kirimoto
import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.Prusa
import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.Simplify
import de.crysxd.octoapp.base.gcode.GcodeParser.Slicer.Slic3r
import de.crysxd.octoapp.base.models.Gcode
import de.crysxd.octoapp.base.models.GcodeLayer
import de.crysxd.octoapp.base.models.GcodeLayerInfo
import de.crysxd.octoapp.base.models.GcodeMove
import de.crysxd.octoapp.base.models.GcodePoint
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import io.ktor.utils.io.ByteReadChannel
import kotlinx.datetime.Clock
import kotlin.collections.set
import kotlin.math.PI
import kotlin.math.absoluteValue
import kotlin.math.acos
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

class GcodeParser(
    private val content: ByteReadChannel,
    private val totalSize: Long,
    private val progressUpdate: suspend (Float) -> Unit,
    private val layerSink: suspend (GcodeLayer) -> GcodeLayer,
    private val detectLayersFromZ: Boolean = false
) {

    private val tag = "GcodeParser"
    private var layers: MutableList<GcodeLayerInfo> = mutableListOf()
    private var moves = mutableMapOf<GcodeMove.Type, Pair<MutableList<GcodeMove>, MutableList<Float>>>()
    private var moveCountInLayer = 0
    private var lastPosition: GcodePoint? = null
    private var lastPositionZ = 0f
    private var lastExtrusionZ = 0f
    private var lastLayerChangeAtPositionInFile = 0
    private var isAbsolutePositioningActive = true
    private var minX = Float.MAX_VALUE
    private var maxX = Float.MIN_VALUE
    private var minY = Float.MAX_VALUE
    private var maxY = Float.MIN_VALUE
    private var slicer: Slicer? = null

    suspend fun parseFile(): Gcode {
        layers.clear()
        initNewLayer()
        lastLayerChangeAtPositionInFile = 0
        lastPosition = null
        lastExtrusionZ = 0f
        lastExtrusionZ = 0f
        isAbsolutePositioningActive = true
        minX = Float.MAX_VALUE
        maxX = Float.MIN_VALUE
        minY = Float.MAX_VALUE
        maxY = Float.MIN_VALUE
        var positionInFile = 0
        var lastUpdatePercent = 0
        val bytes = ByteArray(4 * 1024)
        val line = StringBuilder(512)
        val start = Clock.System.now()

        suspend fun parseNextLine() {
            // Get text
            val lineText = line.toString()
            if (!detectLayersFromZ && isLayerChange(lineText)) {
                startNewLayer(positionInFile)
            } else {
                parseLine(lineText.takeWhile { it != ';' && it != '\r' && it != '\n' }, positionInFile)
            }

            // Reset
            positionInFile += lineText.length
            line.clear()
        }

        Napier.i(tag = tag, message = "[GCD] Start parsing $totalSize bytes")

        do {
            // Read data
            content.awaitContent()
            val read = content.readAvailable(bytes, 0, bytes.size)

            // Iterate over all chars
            for (i in 0 until read) {
                val char = bytes[i].toInt()
                line.append(char.toChar())

                // Line ended?
                if ('\n'.code == char) {
                    // Parse line
                    parseNextLine()

                    // Update progress
                    val progress = (positionInFile / totalSize.toFloat())
                    val percent = (progress * 100).roundToInt().coerceIn(0..100)
                    if (lastUpdatePercent != percent) {
                        progressUpdate(progress)
                        lastUpdatePercent = percent
                    }
                }
            }
        } while (read >= 0)

        // Flush last line
        parseNextLine()

        // Flush last layer
        startNewLayer(positionInFile)
        Napier.i(tag = tag, message = "[GCD] Done with Gcode parsing (${layers.size} layers, ${Clock.System.now() - start})")
        if (layers.isEmpty()) {
            throw SuppressedIllegalStateException("Gcode is empty")
        }

        return Gcode(
            layers = layers.toList(),
            cacheKey = "",
            minX = minX,
            maxX = maxX,
            minY = minY,
            maxY = maxY,
        )
    }

    private suspend fun parseLine(line: String, positionInFile: Int) = when {
        isAbsolutePositioningCommand(line) -> isAbsolutePositioningActive = true
        isRelativePositioningCommand(line) -> isAbsolutePositioningActive = false
        isLinearMoveCommand(line) -> parseLinearMove(line, positionInFile)
        isArcMoveCommand(line) -> parseArcMove(line, positionInFile)
        else -> Unit
    }

    private fun extractValue(label: String, line: String): Float? {
        try {
            var start = line.indexOf(label)
            if (start < 0) return null
            while ((start + 1) < label.length && label[start + 1] == ' ') start++
            val mappedStart = if (start < 0) return null else start + label.length
            val end = line.indexOf(' ', startIndex = mappedStart)
            val mappedEnd = if (end < 0) line.length else end
            return line.substring(mappedStart until mappedEnd).toFloat()
        } catch (e: NumberFormatException) {
            Napier.w(tag = tag, message = "Failed to extract `$label` from `$line`", throwable = e)
            return 0f
        }
    }

    private suspend fun parseLinearMove(line: String, positionInFile: Int) {
        // Get positions (don't use regex, it's slower)
        val x = extractValue("X", line)
        val y = extractValue("Y", line)
        val z = extractValue("Z", line) ?: if (isAbsolutePositioningActive) lastPositionZ else 0f
        val e = extractValue("E", line) ?: 0f

        // Convert to absolute position
        // X and Y might be null. If so, we use the last known position as there was no movement
        val (absoluteX, absoluteY, absoluteZ) = toAbsolutePosition(x = x, y = y, z = z)
        val type = handleExtrusion(e = e, absoluteZ = absoluteZ, positionInFile = positionInFile)

        updateMinMax(x = x, y = y)

        val move = GcodeMove.Linear(
            positionInFile = positionInFile,
            positionInArray = 0,
        )
        addMove(
            move = move,
            type = type,
            fromX = lastPosition?.x ?: absoluteX,
            fromY = lastPosition?.y ?: absoluteY,
            toX = absoluteX,
            toY = absoluteY,
            z = absoluteZ
        )
        line.hashCode()
        z.hashCode()
    }

    private suspend fun parseArcMove(line: String, positionInFile: Int) {
        // Get positions (don't use regex, it's slower)
        val x = extractValue("X", line)
        val y = extractValue("Y", line)
        val i = extractValue("I", line) ?: 0f
        val j = extractValue("J", line) ?: 0f
        val r = extractValue("R", line)
        val z = extractValue("Z", line) ?: lastPositionZ
        val e = extractValue("E", line) ?: 0f
        val clockwise = line.startsWith("G2")

        // Convert to absolute position
        // X and Y might be null. If so, we use the last known position as there was no movement
        val (_, _, absoluteZ) = toAbsolutePosition(x = 0f, y = 0f, z = z)
        val type = handleExtrusion(e = e, absoluteZ = absoluteZ, positionInFile = positionInFile)

        val move = when {
            r != null -> parseRFormArcMove(x = x, y = y, r = r, clockwise = clockwise, positionInFile = positionInFile)

            j != 0f || i != 0f -> parseIjFormArcMove(x = x, y = y, i = i, j = j, clockwise = clockwise, positionInFile = positionInFile)

            else -> throw IllegalArgumentException("Arc move without r or j or i value: $line")
        }

        addMove(
            move = move,
            fromX = lastPosition?.x ?: 0f,
            fromY = lastPosition?.y ?: 0f,
            toX = x ?: lastPosition?.x ?: 0f,
            toY = y ?: lastPosition?.y ?: 0f,
            z = absoluteZ,
            type = type,
        )
    }

    private fun parseIjFormArcMove(x: Float?, y: Float?, i: Float, j: Float, clockwise: Boolean, positionInFile: Int): GcodeMove {
        // End positions are either the given X Y (always absolute) or if they are missing the last known ones
        val endX = x ?: lastPosition?.x ?: throw IllegalArgumentException("Missing param X")
        val endY = y ?: lastPosition?.y ?: throw IllegalArgumentException("Missing param Y")
        val startX = lastPosition?.x ?: throw IllegalArgumentException("Missing start X")
        val startY = lastPosition?.y ?: throw IllegalArgumentException("Missing start Y")
        val centerX = startX + i
        val centerY = startY + j
        val radius = sqrt(i.pow(2) + j.pow(2))

        updateMinMax(x = startX, y = startY)
        updateMinMax(x = endX, y = endY)

        @Suppress("UnnecessaryVariable")
        fun getAndroidAngle(centerX: Float, centerY: Float, pointX: Float, pointY: Float, alt: Boolean = false): Float {
            // Control point marking 0deg
            val controlPointX = if (alt) centerX else centerX + radius
            val controlPointY = if (alt) centerY + radius else centerY

            // Law of cosine
            val sideA = radius
            val sideB = radius
            val sideC = sqrt((pointX - controlPointX).pow(2) + (pointY - controlPointY).pow(2))
            val gamma = acos((sideA.pow(2) + sideB.pow(2) - sideC.pow(2)) / (2 * sideA * sideB))
            val gammaDegrees = (gamma * 180f / PI).toFloat() + (if (alt) 90 else 0)

            // If the angle is NaN we need to rotate the triangle using alt flag
            if (gammaDegrees.isNaN() && !alt) {
                return getAndroidAngle(centerX = centerX, centerY = centerY, pointX = pointX, pointY = pointY, alt = true)
            }

            // If the angle in reality would exceed 180 deg, we need to flip the rectangle
            val res = if (pointY > centerY) {
                gammaDegrees
            } else {
                360 - gammaDegrees
            }

            // If alt mode, invert the angles
            return if (alt) {
                360 - res
            } else {
                res
            }
        }

        var startAngle = getAndroidAngle(centerX = centerX, centerY = centerY, pointX = startX, pointY = startY)
        val endAngle = getAndroidAngle(centerX = centerX, centerY = centerY, pointX = endX, pointY = endY)
        if (startAngle > endAngle) {
            startAngle -= 360
        }
        val sweepAngle = if (clockwise) {
            (endAngle - startAngle) - 360
        } else {
            endAngle - startAngle
        }

        // Android Canvas seems to have issues with extremely small angles for arcs.
        // To circumvent this issue we approximate arcs with angles of less than a quarter degree with a line.
        return if (sweepAngle.absoluteValue < 1) {
            GcodeMove.Linear(
                positionInFile = positionInFile,
                positionInArray = 0,
            )
        } else {
            GcodeMove.Arc(
                leftX = (centerX - radius),
                topY = (centerY - radius),
                radius = radius,
                startAngle = startAngle,
                sweepAngle = sweepAngle,
                positionInFile = positionInFile,
            )
        }
    }

    @Suppress("UNUSED_PARAMETER")
    private fun parseRFormArcMove(x: Float?, y: Float?, r: Float, clockwise: Boolean, positionInFile: Int): GcodeMove {
        return GcodeMove.Linear(
            positionInFile = positionInFile,
            positionInArray = 0,
        )
    }

    private suspend fun handleExtrusion(e: Float?, absoluteZ: Float, positionInFile: Int): GcodeMove.Type {
        // Check if a new layer was started
        // A layer is started when we extrude (positive e, negative is retraction)
        // on a height which is different from the last height we extruded at
        if (e == null || e > 0) {
            // If the Z changed since the last extrusion, we have a new layer
            if (absoluteZ != lastExtrusionZ && detectLayersFromZ) {
                startNewLayer(positionInFile)
            }

            // Update last extrusion Z height
            lastExtrusionZ = absoluteZ
        }

        // Get type
        return if (e == 0f) {
            GcodeMove.Type.Travel
        } else {
            GcodeMove.Type.Extrude
        }
    }

    private fun toAbsolutePosition(x: Float?, y: Float?, z: Float): Triple<Float, Float, Float> {
        val absoluteX = x?.let {
            if (isAbsolutePositioningActive) {
                it
            } else {
                (lastPosition?.x ?: 0f) + it
            }
        } ?: lastPosition?.x ?: 0f
        val absoluteY = y?.let {
            if (isAbsolutePositioningActive) {
                it
            } else {
                (lastPosition?.y ?: 0f) + it
            }
        } ?: lastPosition?.y ?: 0f
        val absoluteZ = if (isAbsolutePositioningActive) {
            z
        } else {
            lastPositionZ + z
        }

        return Triple(absoluteX, absoluteY, absoluteZ)
    }

    private fun isLayerChange(line: String): Boolean = when (slicer) {
        null -> {
            slicer = when {
                line.startsWith("; generated by PrusaSlicer", ignoreCase = true) -> Prusa
                line.startsWith("; generated by OrcaSlicer", ignoreCase = true) -> Prusa
                line.startsWith("; generated by SuperSlicer", ignoreCase = true) -> Prusa
                line.startsWith("; BambuStudio", ignoreCase = true) -> Bambu
                line.startsWith(";Generated with Cura", ignoreCase = true) -> Cura
                line.startsWith("; generated by Slic3r", ignoreCase = true) -> Slic3r
                line.startsWith("; Generated by Kiri:Moto", ignoreCase = true) -> Kirimoto
                line.startsWith("; G-Code generated by Simplify3D", ignoreCase = true) -> Simplify
                line.startsWith(";Sliced by ideaMaker", ignoreCase = true) -> IdeaMaker

                // Artillery Slicer (and others?) removed the "generated by" line, this line indicates a PS derivative
                line.startsWith("; external perimeters extrusion width =", ignoreCase = true) -> Prusa

                // Second fallback, this indicates a Cura derivative
                line.startsWith(";LAYER_COUNT:", ignoreCase = true) -> Cura

                // Third fallback, this indicates an old Cura derivative
                line.startsWith(";Layer count:", ignoreCase = true) -> Cura

                else -> null
            }

            // Did we not pick up a slicer but the plugin processed the file? False if a slicer was picked up with this line
            line.startsWith("M118 E1 OCTOAPP_LAYER ")
        }

        Cura -> line.startsWith(";LAYER:")
        IdeaMaker -> line.startsWith(";LAYER:")
        Prusa -> line.startsWith(";LAYER_CHANGE")
        Bambu -> line.startsWith("; CHANGE_LAYER")
        Simplify -> line.startsWith(";; --- layer")
        Kirimoto -> line.startsWith("; layer ")
        Slic3r -> false // Doesn't mark layers
    }.let { isLayer ->
        // If we are on the first layer and it's empty we skip this change. The reason is that we start parsing with a fresh layer already
        // and the first change is already done
        if (isLayer && layers.isEmpty() && moves.values.all { (list, _) -> list.isEmpty() }) {
            false
        } else {
            isLayer
        }
    }

    private fun isLinearMoveCommand(line: String) = isCommand(line = line, command = "G0") || isCommand(line = line, command = "G1")

    private fun isArcMoveCommand(line: String) = isCommand(line = line, command = "G2") || isCommand(line = line, command = "G3")

    private fun isAbsolutePositioningCommand(line: String) = isCommand(line = line, command = "G90")

    private fun isRelativePositioningCommand(line: String) = isCommand(line = line, command = "G91")

    private fun isCommand(line: String, command: String) =
        line.startsWith("$command ", ignoreCase = true) ||
                line.startsWith("$command;", ignoreCase = true) ||
                line.equals(command, ignoreCase = true)

    private suspend fun startNewLayer(positionInFile: Int) {
        // Only add layer if we have any extrusion moves
        if (moves[GcodeMove.Type.Extrude]?.first?.isNotEmpty() == true || !detectLayersFromZ) {
            val info = GcodeLayerInfo(
                zHeight = lastExtrusionZ,
                moveCount = moves.map { it.value.first.size }.sum(),
                positionInFile = lastLayerChangeAtPositionInFile,
                lengthInFile = positionInFile - lastLayerChangeAtPositionInFile
            )
            val layer = GcodeLayer(
                info = info,
                moves = moves.mapValues {
                    Pair(it.value.first, it.value.second.toFloatArray())
                }
            )
            val layerWithCacheInfo = layerSink(layer)
            layers.add(layerWithCacheInfo.info)
            lastLayerChangeAtPositionInFile = positionInFile
        }
        initNewLayer()
    }

    private fun updateMinMax(x: Float?, y: Float?) {
        x?.let {
            minX = it.coerceAtMost(minX)
            maxX = it.coerceAtLeast(maxX)
        }

        y?.let {
            minY = it.coerceAtMost(minY)
            maxY = it.coerceAtLeast(maxY)
        }
    }

    private fun initNewLayer() {
        moves.clear()
        moveCountInLayer = 0
        GcodeMove.Type.entries.forEach {
            moves[it] = Pair(mutableListOf(), mutableListOf())
        }
    }

    private fun addMove(move: GcodeMove, type: GcodeMove.Type, fromX: Float, fromY: Float, toX: Float, toY: Float, z: Float) {
        when (move) {
            is GcodeMove.Arc -> moves[type]?.first?.add(move)
            is GcodeMove.Linear -> moves[type]?.let {
                it.first.add(move.copy(positionInArray = it.second.size))
                it.second.add(fromX)
                it.second.add(fromY)
                it.second.add(toX)
                it.second.add(toY)
            }
        }

        moveCountInLayer++
        lastPosition = GcodePoint(x = toX, y = toY)
        lastPositionZ = z
    }

    private enum class Slicer {
        Cura,
        Slic3r,
        Prusa,
        Bambu,
        Simplify,
        IdeaMaker,
        Kirimoto
    }
}