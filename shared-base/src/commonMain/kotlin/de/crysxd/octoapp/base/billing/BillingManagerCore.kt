package de.crysxd.octoapp.base.billing

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.PREMIUM_FEATURES_EVERYTHING
import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.get
import de.crysxd.octoapp.base.models.BillingPurchase
import de.crysxd.octoapp.base.models.BillingState
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier

class BillingManagerCore {

    private val tag = "BillingManagerCore"
    private val platform by lazy { SharedCommonInjector.get().platform }
    private val preferences by lazy { SharedBaseInjector.get().preferences }
    private val widgetPreferencesRepository by lazy { SharedBaseInjector.get().controlsPreferencesRepository }

    fun isFeatureAvailable(feature: String, purchases: List<BillingPurchase>): Boolean {
        val premiumFeatures = OctoConfig.get(OctoConfigField.PremiumFeatures)
        val isPremiumFeature = premiumFeatures.contains(feature) || PREMIUM_FEATURES_EVERYTHING in premiumFeatures
        val hasPremium = purchases.any { it.productId != "tip" }
        val isBeta = platform.betaBuild && !platform.debugBuild
        return BillingManagerTest.enabledForTest ?: (!isPremiumFeature || hasPremium || isBeta)
    }

    fun shouldAdvertisePremium(state: BillingState): Boolean {
        val isBeta = platform.betaBuild && !platform.debugBuild
        val hasProducts = state.products.isNotEmpty()
        val hasNoPurchases = state.purchases.isEmpty()
        val enabledForTest = BillingManagerTest.enabledForTest == true
        return !isBeta && hasProducts && hasNoPurchases && !enabledForTest
    }

    fun ensureGcodePreviewNotHidden(state: BillingState) {
        try {
            if (isFeatureAvailable(FEATURE_GCODE_PREVIEW, state.purchases)) {
                Napier.i(tag = tag, message = "Ensure Gcode is unhidden")
                preferences.gcodeHiddenIos = false
                widgetPreferencesRepository.apply {
                    getWidgetOrder(ControlsPreferencesRepository.LIST_PRINT)?.let { prefs ->
                        val updated = prefs.copy(hidden = prefs.hidden.filter { it != ControlType.GcodePreviewWidget })
                        setWidgetOrder(ControlsPreferencesRepository.LIST_PRINT, updated)
                    }
                }
            } else {
                Napier.i(tag = tag, message = "Gcode is not unhidden, not available: ${state.purchases}")
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed to ensure Gcode unhidden")
        }
    }
}