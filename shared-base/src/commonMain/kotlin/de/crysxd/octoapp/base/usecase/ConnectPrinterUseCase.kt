package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.BackendType.Moonraker
import de.crysxd.octoapp.base.data.models.BackendType.OctoPrint
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

@OptIn(ExperimentalCoroutinesApi::class)
class ConnectPrinterUseCase(
    private val autoConnectPrinterUseCase: AutoConnectPrinterUseCase,
    private val getPrinterConnectionUseCase: GetPrinterConnectionUseCase,
    private val getPowerDevicesUseCase: GetPowerDevicesUseCase,
    private val octoPreferences: OctoPreferences,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider,
    private val activeHttpUrlUseCase: GetActiveHttpUrlUseCase,
) : UseCase2<ConnectPrinterUseCase.Params, Flow<ConnectPrinterUseCase.UiState>>() {

    private val octoPrintCore by lazy {
        ConnectPrinterUseCaseOctoPrintCore(
            autoConnectPrinterUseCase = autoConnectPrinterUseCase,
            getPrinterConnectionUseCase = getPrinterConnectionUseCase,
            getPowerDevicesUseCase = getPowerDevicesUseCase,
            octoPreferences = octoPreferences,
            printerConfigurationRepository = printerConfigurationRepository,
            printerEngineProvider = printerEngineProvider,
            activeHttpUrlUseCase = activeHttpUrlUseCase,
        )
    }
    private val moonrakerCore by lazy {
        ConnectPrinterUseCaseMoonrakerCore(
            octoPreferences = octoPreferences,
            printerConfigurationRepository = printerConfigurationRepository,
            printerEngineProvider = printerEngineProvider,
        )
    }
    private var activeCore: Core? = null

    override suspend fun doExecute(param: Params, logger: Logger) = printerConfigurationRepository.instanceInformationFlow(param.instanceId).map { info ->
        info?.type
    }.distinctUntilChanged().flatMapLatest { type ->
        activeCore = when (type) {
            null -> octoPrintCore
            OctoPrint -> octoPrintCore
            Moonraker -> moonrakerCore
        }

        activeCore?.doExecute(param, logger) ?: let {
            logger.e(IllegalStateException("Unable to start connection routine, info.type=$type"))
            flowOf(UiState(title = "Internal error", detail = null, avatarState = AvatarState.Idle))
        }
    }

    suspend fun executeAction(actionType: ActionType) = activeCore?.executeAction(actionType) ?: Unit

    internal interface Core {
        suspend fun doExecute(param: Params, logger: Logger): Flow<UiState>
        suspend fun executeAction(actionType: ActionType)
    }

    data class Params(
        val instanceId: String,
    )

    data class UiState(
        val title: String,
        val detail: String?,
        val avatarState: AvatarState,
        val action: String? = null,
        val actionType: ActionType? = null,
        val primaryAction: Boolean = true,
        val exception: Throwable? = null,
        val isNotAvailable: Boolean = false,
    ) {
        companion object {
            val Initializing = UiState(
                title = getString("connect_printer___searching_for_octoprint_title"),
                detail = null,
                avatarState = AvatarState.Swim,
            )
        }
    }

    enum class AvatarState {
        Idle, Swim, Party
    }

    enum class ActionType {
        BeginConnect, AutoConnectTutorial, TurnPsuOn, TurnPsuOff, ConfigurePsu, CyclePsu, Retry
    }
}