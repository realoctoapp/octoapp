package de.crysxd.octoapp.base.ext

import kotlinx.coroutines.flow.SharingStarted

val SharingStarted.Companion.WhileSubscribedOcto
    get() = WhileSubscribed(
        stopTimeoutMillis = 0
    )

val SharingStarted.Companion.WhileSubscribedOctoDelay
    get() = WhileSubscribed(
        stopTimeoutMillis = 3000
    )