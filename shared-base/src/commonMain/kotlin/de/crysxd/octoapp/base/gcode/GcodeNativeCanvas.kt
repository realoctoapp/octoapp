package de.crysxd.octoapp.base.gcode

interface GcodeNativeCanvas {

    // State
    val viewPortWidth: Float
    val viewPortHeight: Float
    val paddingTop: Float
    val paddingLeft: Float
    val paddingRight: Float
    val paddingBottom: Float
    fun invalidate()

    // Transformations
    fun saveState()
    fun restoreState()
    fun scale(x: Float, y: Float)
    fun translate(x: Float, y: Float)
    fun rotate(degrees: Float)

    // Lines
    fun drawLine(xStart: Float, yStart: Float, xEnd: Float, yEnd: Float, paint: Paint)
    fun drawLines(points: FloatArray, offset: Int, count: Int, paint: Paint)

    // Rects
    fun drawRect(left: Float, top: Float, right: Float, bottom: Float, paint: Paint)

    // Arcs
    fun drawArc(centerX: Float, centerY: Float, radius: Float, startDegrees: Float, sweepAngle: Float, paint: Paint)

    // Images
    fun drawImage(image: Image, x: Float, y: Float, width: Float, height: Float)
    fun intrinsicWidth(image: Image): Float
    fun intrinsicHeight(image: Image): Float

    data class Paint(
        var color: Color,
        var mode: Mode,
        var alpha: Float = 1f,
        var strokeWidthCorrection: Float,
        var strokeWidth: Float
    ) {
        enum class Mode {
            Fill, Stroke
        }
    }

    sealed class Color {
        data object MovePreviousLayer : Color()
        data object MoveRemainingLayer : Color()
        data object MoveExtrusion : Color()
        data object MoveTravel : Color()
        data object MoveUnsupported : Color()
        data object PrintHead : Color()
        data object Yellow : Color()
        data object LightGrey : Color()
        data object Accent : Color()
        data object Red : Color()
        data object Green : Color()
        data class Custom(val red: Float, val green: Float, val blue: Float) : Color()
    }

    enum class Image {
        PrintBedEnder,
        PrintBedPrusa,
        PrintBedGeneric,
        PrintBedCreality,
        PrintBedAnycubic,
        PrintBedArtillery
    }
}