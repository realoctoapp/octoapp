package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import de.crysxd.octoapp.sharedcommon.url.isSharedOctoEverywhereUrl
import de.crysxd.octoapp.sharedcommon.utils.getString

class SetAlternativeWebUrlUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : UseCase2<SetAlternativeWebUrlUseCase.Params, SetAlternativeWebUrlUseCase.Result>() {

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val octoPrint = printerEngineProvider.printer(instanceId = param.instanceId)

        val url = if (param.webUrl.isNotEmpty()) {
            try {
                param.webUrl.toUrl().withBasicAuth(user = param.username, password = param.password)
            } catch (e: Exception) {
                logger.w(e)
                OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySetFailed)
                return Result.Failure(getString("configure_remote_acces___manual___error_invalid_url"), e)
            }
        } else {
            null
        }

        if (!param.bypassChecks && url != null) {
            val isOe = url.isOctoEverywhereUrl()
            val isShared = url.isSharedOctoEverywhereUrl()
            when {
                isOe && !isShared -> return Result.Failure(
                    errorMessage = getString("configure_remote_acces___manual___error_normal_octoeverywhere_url"),
                    allowToProceed = false,
                    exception = InvalidAlternativeUrlException("Given URL is a standard OctoEverywhere URL")
                )

                isOe && isShared -> return Result.Failure(
                    errorMessage = getString("configure_remote_acces___manual___error_shared_octoeverywhere_url"),
                    allowToProceed = true,
                    exception = InvalidAlternativeUrlException("Given URL is a shared OctoEverywhere URL")
                )
            }

            val settings = try {
                val config = printerConfigurationRepository.get(param.instanceId) ?: throw IllegalStateException("Didn't find config for ${param.instanceId}")
                val octoprint = printerEngineProvider.createAdHocPrinter(config.copy(webUrl = url, alternativeWebUrl = null), logTag = logger.httpTag)
                config.settings ?: octoprint.settingsApi.getSettings()
            } catch (e: Exception) {
                logger.e(e)
                OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySetFailed)
                return Result.Failure(getString("configure_remote_acces___manual___error_unable_to_connect"), e, allowToProceed = true)
            }

            try {
                val remoteUuid = settings.plugins.discovery?.uuid
                val localUuid = octoPrint.settingsApi.getSettings().plugins.discovery?.uuid

                if (localUuid != remoteUuid) {
                    throw InvalidAlternativeUrlException("Upnp UUIDs for primary and alternate URLs differ: $localUuid <--> $remoteUuid")
                }
            } catch (e: Exception) {
                OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySetFailed)
                return Result.Failure(getString("configure_remote_acces___manual___error_unable_to_verify"), e, allowToProceed = true)
            }
        }

        printerConfigurationRepository.update(param.instanceId) {
            it.copy(alternativeWebUrl = url, octoEverywhereConnection = null, remoteConnectionFailure = null, alternativeWebUrlPlugin = null)
        }

        if (url != null) {
            OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigManuallySet)
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, "manual")
        } else {
            OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigCleared)
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.RemoteAccess, null)
        }
        return Result.Success
    }

    data class Params(
        val instanceId: String,
        val webUrl: String,
        val username: String,
        val password: String,
        val bypassChecks: Boolean
    )

    sealed class Result {
        data object Success : Result()
        data class Failure(
            val errorMessage: String,
            val exception: Exception,
            val allowToProceed: Boolean = false
        ) : Result()
    }

    class InvalidAlternativeUrlException(message: String) : SuppressedIllegalStateException(message)
}