package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GcodePreviewSettings(
    val showPreviousLayer: Boolean = false,
    val showCurrentLayer: Boolean = false,
    @SerialName("layerOffset2") val layerOffset: Int = 1,
    val quality: Quality = Quality.Medium,
) {
    @Serializable
    enum class Quality {
        Low, Medium, Ultra
    }

    val layerCountDisplay: (Int) -> Int get() = { it + (layerOffset - 1) }
    val layerNumberDisplay: (Int) -> Int get() = { it + layerOffset }
}