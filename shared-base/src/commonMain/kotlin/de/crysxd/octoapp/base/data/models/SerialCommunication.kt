package de.crysxd.octoapp.base.data.models

import kotlinx.datetime.Instant

data class SerialCommunication(
    val resetPoint: Boolean,
    val content: String,
    val date: Instant,
    val serverDate: Instant,
    val source: Source,
    val id: Int,
) {

    sealed class Source {
        data object OctoPrint : Source()
        data object OctoAppInternal : Source()
        data object User : Source()
    }
}