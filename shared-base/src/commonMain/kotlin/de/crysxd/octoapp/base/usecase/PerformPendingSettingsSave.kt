package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asMoonraker
import de.crysxd.octoapp.engine.models.commands.GcodeCommand

class PerformPendingSettingsSave(
    private val printerProvider: PrinterEngineProvider
) : UseCase2<PerformPendingSettingsSave.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        printerProvider.printer(param.instanceId).asMoonraker().printerApi.executeGcodeCommand(
            cmd = GcodeCommand.Single(command = "SAVE_CONFIG")
        )
    }

    data class Params(
        val instanceId: String
    )
}