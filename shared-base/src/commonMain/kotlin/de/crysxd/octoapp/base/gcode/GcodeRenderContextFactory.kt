package de.crysxd.octoapp.base.gcode

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.Gcode
import de.crysxd.octoapp.base.models.GcodeLayer
import de.crysxd.octoapp.base.models.GcodeMove
import de.crysxd.octoapp.base.models.GcodePath
import de.crysxd.octoapp.base.models.GcodePoint
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.roundToInt

sealed class GcodeRenderContextFactory {

    companion object {
        private const val tag = "GcodeRenderContextFactory"
    }

    private val preferences = SharedBaseInjector.get().preferences
    protected val ds = SharedBaseInjector.get().localGcodeFileDataSource

    private val GcodePath.priority
        get() = when (type) {
            GcodeMove.Type.Travel -> 2
            GcodeMove.Type.Extrude -> 1
            GcodeMove.Type.Unsupported -> 0
        }

    abstract suspend fun extractMoves(gcode: Gcode, includePreviousLayer: Boolean, includeRemainingCurrentLayer: Boolean): GcodeRenderContext

    protected suspend fun createContext(
        gcode: Gcode,
        layerIndex: Int,
        toPositionInFile: Int,
        includeRemainingCurrentLayer: Boolean,
        includePreviousLayer: Boolean,
    ): GcodeRenderContext {
        if (layerIndex < 0) throw SuppressedIllegalStateException("Invalid layer number: $layerIndex")

        val layerInfo = gcode.layers[layerIndex]
        val layer = ds.loadLayer(gcode.cacheKey, layerInfo)
        val completedCurrentLayer = loadSingleLayer(layer, toPositionInFile = toPositionInFile)
        val remainingCurrentLayer = if (includeRemainingCurrentLayer) {
            loadSingleLayer(layer, fromPositionInFile = toPositionInFile)
        } else {
            null
        }
        val previousLayer = if (includePreviousLayer) {
            gcode.layers.getOrNull(layerIndex - 1)?.let {
                loadSingleLayer(ds.loadLayer(gcode.cacheKey, it))
            }
        } else {
            null
        }

        val completedMoves = completedCurrentLayer.second.sumOf { it.moveCount }
        val allMoves = layer.moves.values.sumOf { it.first.size }

        return GcodeRenderContext(
            previousLayerPaths = previousLayer?.second,
            completedLayerPaths = completedCurrentLayer.second,
            remainingLayerPaths = remainingCurrentLayer?.second,
            printHeadPosition = completedCurrentLayer.first,
            layerCount = gcode.layers.size,
            layerZHeight = layerInfo.zHeight,
            layerNumber = gcode.layers.indexOf(layerInfo),
            layerProgress = completedMoves / allMoves.toFloat(),
            gcodeBounds = gcode.bounds,
            layerCountDisplay = preferences.gcodePreviewSettings.layerCountDisplay,
            layerNumberDisplay = preferences.gcodePreviewSettings.layerNumberDisplay,
        )
    }

    fun loadSingleLayer(
        layer: GcodeLayer,
        fromPositionInFile: Int = 0,
        toPositionInFile: Int = Int.MAX_VALUE,
    ): Pair<GcodePoint?, List<GcodePath>> {
        var lastPosition: Pair<Int, GcodePoint>? = null
        val paths = layer.moves.map { (moveType, value) ->
            val (moves, lines) = value

            // Find last move
            val lastMoveIndex = moves.indexOfLast { m ->
                m.positionInFile <= toPositionInFile
            }.takeIf { it >= 0 }

            // Get last move information
            lastMoveIndex?.let { index ->
                val move = moves[index]
                if (move.positionInFile > (lastPosition?.first ?: -1)) {
                    lastPosition = move.positionInFile to when (move) {
                        is GcodeMove.Arc -> move.endPosition
                        is GcodeMove.Linear -> {
                            val x = lines[move.positionInArray + 2]
                            val y = lines[move.positionInArray + 3]
                            GcodePoint(x, y)
                        }
                    }
                }
            }

            // Find offset for lines array
            val linesOffset = if (fromPositionInFile == 0) {
                0
            } else {
                moves.mapNotNull { m ->
                    m as? GcodeMove.Linear
                }.firstOrNull { i ->
                    i.positionInFile >= fromPositionInFile
                }?.positionInArray ?: 0
            }

            // Find count for lines array
            val linesCount = moves.mapNotNull { m ->
                m as? GcodeMove.Linear
            }.lastOrNull { m ->
                m.positionInFile <= toPositionInFile
            }?.let { m ->
                m.positionInArray + 4
            } ?: 0

            // Create path
            GcodePath(
                arcs = moves.mapNotNull { m -> (m as? GcodeMove.Arc) }.filter { m ->
                    m.positionInFile in fromPositionInFile..toPositionInFile
                },
                type = moveType,
                linesOffset = linesOffset,
                linesCount = linesCount - linesOffset,
                lines = lines,
                moveCount = lastMoveIndex ?: 0
            )
        }.sortedBy {
            it.priority
        }
        return lastPosition?.second to paths
    }

    data class ForFileLocation(val positionInFile: Int) : GcodeRenderContextFactory() {
        override suspend fun extractMoves(
            gcode: Gcode,
            includePreviousLayer: Boolean,
            includeRemainingCurrentLayer: Boolean
        ): GcodeRenderContext = withContext(Dispatchers.SharedIO) {
            try {
                val layerIndex = gcode.layers.indexOfLast { it.positionInFile <= positionInFile }
                createContext(
                    gcode = gcode,
                    layerIndex = layerIndex,
                    toPositionInFile = positionInFile,
                    includeRemainingCurrentLayer = includeRemainingCurrentLayer,
                    includePreviousLayer = includePreviousLayer,
                )
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to load file location", throwable = e)
                ds.removeFromCache(gcode.cacheKey)
                throw e
            }
        }
    }

    data class ForLayerProgress(val layerIndex: Int, val progress: Float) : GcodeRenderContextFactory() {
        override suspend fun extractMoves(
            gcode: Gcode,
            includePreviousLayer: Boolean,
            includeRemainingCurrentLayer: Boolean
        ): GcodeRenderContext = withContext(Dispatchers.SharedIO) {
            if (gcode.layers.isEmpty()) return@withContext GcodeRenderContext(
                previousLayerPaths = null,
                completedLayerPaths = emptyList(),
                remainingLayerPaths = null,
                printHeadPosition = null,
                gcodeBounds = gcode.bounds,
                layerCount = 0,
                layerNumber = 0,
                layerZHeight = 0f,
                layerProgress = 0f,
                layerCountDisplay = { it },
                layerNumberDisplay = { it },
            )

            try {
                val layerInfo = gcode.layers[layerIndex]
                val positionInFile = layerInfo.positionInFile + (layerInfo.lengthInFile * progress).let {
                    if (it.isNaN()) 0 else it.roundToInt()
                }
                createContext(
                    gcode = gcode,
                    layerIndex = layerIndex,
                    toPositionInFile = positionInFile,
                    includeRemainingCurrentLayer = includeRemainingCurrentLayer && 1 > progress,
                    includePreviousLayer = includePreviousLayer,
                ).copy(
                    printHeadPosition = null
                )
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to load layer progress", throwable = e)
                ds.removeFromCache(gcode.cacheKey)
                throw e
            }
        }
    }
}