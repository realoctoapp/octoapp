package de.crysxd.octoapp.base.gcode

import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas.Color
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas.Image
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas.Paint
import de.crysxd.octoapp.base.models.GcodeMove
import de.crysxd.octoapp.base.models.GcodePath
import de.crysxd.octoapp.base.models.GcodeRenderContext
import io.github.aakira.napier.Napier
import kotlin.math.max
import kotlin.math.min

class GcodeRenderer(
    private val canvas: GcodeNativeCanvas,
) {

    companion object {
        private const val MIN_ZOOM = 1f
        private const val MAX_ZOOM = 10f
    }

    var debugLogs = false
    private val tag = "GcodeRenderer"
    private val values = Values()
    private var renderParams: RenderParams? = null
    private var scrollOffsetX = 0f
    private var scrollOffsetY = 0f
    val doubleTapZoom = 5f
    var zoom = 1f
        private set
    var plugin: (GcodeNativeCanvas) -> Unit = {}

    private val paint = Paint(
        color = Color.MovePreviousLayer,
        mode = Paint.Mode.Stroke,
        strokeWidth = 0f,
        strokeWidthCorrection = 1f
    )

    fun submitRenderParams(params: RenderParams?) {
        renderParams = params
        canvas.invalidate()
    }

    fun getPrintBedCoordinateFromViewPortPosition(x: Float, y: Float) = Pair(
        values.pxToMmX(x),
        values.pxToMmY(y)
    )

    fun zoomBy(focusX: Float, focusY: Float, scaleFactor: Float) {
        zoom(focusX = focusX, focusY = focusY, newZoom = zoom * scaleFactor)
    }

    fun zoom(focusX: Float, focusY: Float, newZoom: Float) {
        fun fromViewToPrinter(params: RenderParams, x: Float, y: Float) = Pair(
            (x + -scrollOffsetX) * (params.pxToMmFactor / zoom),
            (y + -scrollOffsetY) * (params.pxToMmFactor / zoom),
        )

        renderParams?.let {
            val focusOnPrinterBefore = fromViewToPrinter(it, focusX, focusY)
            zoom = newZoom.coerceIn(MIN_ZOOM, MAX_ZOOM)
            val focusOnPrinterAfter = fromViewToPrinter(it, focusX, focusY)
            val offsetX = focusOnPrinterAfter.first - focusOnPrinterBefore.first
            val offsetY = focusOnPrinterAfter.second - focusOnPrinterBefore.second
            scrollOffsetX += offsetX * it.mmToPxFactor * zoom
            scrollOffsetY += offsetY * it.mmToPxFactor * zoom
            canvas.invalidate()
        }
    }

    fun moveBy(x: Float, y: Float) {
        scrollOffsetX -= x
        scrollOffsetY -= y
        canvas.invalidate()
    }

    fun move(x: Float, y: Float) {
        scrollOffsetX = x
        scrollOffsetY = y
        canvas.invalidate()
    }

    fun draw() = with(values) {
        val params = renderParams ?: return@with

        // Ensure scroll is in range
        enforceScrollLimits(params)

        // Perform all calculations, will be stored in `values` object
        updateValues(params)
        paint.strokeWidthCorrection = ((MAX_ZOOM - MIN_ZOOM) - (zoom - MIN_ZOOM)) / (MAX_ZOOM - MIN_ZOOM)

        // Draw the background
        drawBackground(params)

        canvas.withGcodeCoordinateSystem(params) {
            // Draw debug
            drawDebugOutline(params = params)

            // Draw Gcode
            drawPaths(
                paths = params.renderContext.previousLayerPaths,
                colorOverride = Color.MovePreviousLayer,
                skipTravel = true,
                type = "previous"
            )
            drawPaths(
                paths = params.renderContext.completedLayerPaths,
                type = "current"
            )
            drawPaths(
                paths = params.renderContext.remainingLayerPaths,
                colorOverride = Color.MoveRemainingLayer,
                skipTravel = true,
                type = "remaining"
            )

            // Draw print head
            drawToolPosition(params)

            // Draw plugin content on top
            canvas.withSave(plugin)
        }
    }

    private fun updateValues(params: RenderParams) = with(values) {
        // Calc offsets, center render if smaller than view
        backgroundWidth = params.edgeLength
        val printBedHeightIntrinsic = canvas.intrinsicHeight(params.printBed)
        val printBedWidthIntrinsic = canvas.intrinsicWidth(params.printBed)
        backgroundHeight = backgroundWidth * (params.printBedHeightMm / params.printBedWidthMm)
        backgroundHeight *= params.printBedVerticalStretch

        totalFactor = params.mmToPxFactor * zoom
        printBedWidth = params.printBedWidthMm * totalFactor
        printBedHeight = params.printBedHeightMm * totalFactor
        xOffset = canvas.paddingLeft + if (printBedWidth < canvas.viewPortWidth) {
            ((paddedWidth - max(printBedWidth, backgroundWidth)) / 2)
        } else {
            scrollOffsetX
        }
        yOffset = canvas.paddingTop + if (printBedHeight < canvas.viewPortHeight) {
            ((paddedHeight - max(printBedHeight, backgroundHeight)) / 2)
        } else {
            scrollOffsetY
        }

        // Store conversions for click handling
        pxToMmX = { (it - xOffset) * (1f / totalFactor) }
        pxToMmY = { (printBedHeight - it + yOffset) * (1f / totalFactor) }

        // Calc line width and do not use less than 2px after the totalFactor is applied
        // Then divide by total as Canvas will apply scale later on the GPU
        val strokeWidth = (params.extrusionWidthMm * totalFactor * 0.8f).coerceAtLeast(2f) / totalFactor
        gcodeOtherStrokeWidth = strokeWidth * 0.5f
        gcodeExtrusionStrokeWidth = strokeWidth

        if (debugLogs) {
            Napier.i(
                tag = tag,
                message = """
                Calculations:
                    width=${canvas.viewPortWidth}
                    height=${canvas.viewPortHeight}
                    printBedWidthMm=${params.printBedWidthMm}
                    printBedHeightMm=${params.printBedHeightMm}
                    printBedHeightIntrinsic=$printBedHeightIntrinsic
                    printBedWidthIntrinsic=$printBedWidthIntrinsic
                    printBedVerticalStretch=${params.printBedVerticalStretch}
                    paddingLeft=${canvas.paddingLeft}
                    paddingTop=${canvas.paddingTop}
                    paddingRight=${canvas.paddingRight}
                    paddingBottom=${canvas.paddingBottom}
                    backgroundWidth=${backgroundWidth}
                    backgroundHeight=${backgroundHeight}
                    totalFactor=${totalFactor}
                    printBedWidth=${printBedWidth}
                    printBedHeight=${printBedHeight}
                    xOffset=${xOffset}
                    yOffset=${yOffset}
                    pxToMmX=${pxToMmX(1f)}
                    pxToMmY=${pxToMmY(1f)}
                    gcodeOtherStrokeWidth=${gcodeOtherStrokeWidth}
                    gcodeExtrusionStrokeWidth=${gcodeExtrusionStrokeWidth}
                """.trimIndent()
            )
        }
    }

    private fun drawBackground(params: RenderParams) = canvas.withSave {
        // Scale and transform so the desired are is visible
        translate(values.xOffset, values.yOffset)
        scale(values.totalFactor, values.totalFactor)
        val width = values.backgroundWidth * params.pxToMmFactor
        val height = values.backgroundHeight * params.pxToMmFactor
        drawImage(
            image = params.printBed,
            x = 0f,
            y = 0f,
            width = width,
            height = height,
        )

        if (debugLogs) {
            Napier.i(
                tag = tag,
                message = """
                    Background:
                        printBed=${params.printBed}
                        xOffset=${values.xOffset}
                        yOffset=${values.yOffset}
                        scale=${values.totalFactor}
                        x=0
                        y=0
                        width=$width
                        height=$height
                    """.trimIndent()
            )
        }
    }

    private fun drawPaths(
        paths: List<GcodePath>?,
        colorOverride: Color? = null,
        skipTravel: Boolean = false,
        type: String,
    ) = paths?.filter { it.type != GcodeMove.Type.Travel || !skipTravel }?.forEach { path ->
        path.type.configurePaint(colorOverride = colorOverride)
        canvas.drawLines(points = path.lines, offset = path.linesOffset, count = path.linesCount, paint = paint)
        canvas.drawArcs(arcs = path.arcs, paint = paint)

        if (debugLogs) {
            Napier.i(
                tag = tag,
                message = """
                    Draw paths $type/${path.type}:
                        linearMoves=${path.linesCount}
                        arcMoves=${path.arcs.size}
                        totalMoves=${path.moveCount}
                    """.trimIndent()
            )
        }
    }

    private fun drawDebugOutline(params: RenderParams) = with(canvas) {
        if (!debugLogs) return

        drawLine(
            xStart = 0f,
            xEnd = params.printBedWidthMm,
            yEnd = 0f,
            yStart = 0f,
            paint = Paint(color = Color.PrintHead, strokeWidth = 3f, strokeWidthCorrection = 0f, mode = Paint.Mode.Stroke)
        )
        drawLine(
            xStart = params.printBedWidthMm,
            xEnd = params.printBedWidthMm,
            yEnd = params.printBedHeightMm,
            yStart = 0f,
            paint = Paint(color = Color.PrintHead, strokeWidth = 1f, strokeWidthCorrection = 0f, mode = Paint.Mode.Stroke)
        )
        drawLine(
            xStart = params.printBedWidthMm,
            xEnd = 0f,
            yEnd = params.printBedHeightMm,
            yStart = params.printBedHeightMm,
            paint = Paint(color = Color.PrintHead, strokeWidth = 1f, strokeWidthCorrection = 0f, mode = Paint.Mode.Stroke)
        )
        drawLine(
            xStart = 0f,
            xEnd = 0f,
            yEnd = 0f,
            yStart = params.printBedHeightMm,
            paint = Paint(color = Color.PrintHead, strokeWidth = 1f, strokeWidthCorrection = 0f, mode = Paint.Mode.Stroke)
        )
    }

    private fun drawToolPosition(params: RenderParams) = params.renderContext.printHeadPosition?.let { position ->
        paint.color = Color.PrintHead
        paint.mode = Paint.Mode.Fill
        paint.strokeWidth = 0f

        val actualRadius = params.extrusionWidthMm * 2
        val minRadius = (params.pxToMmFactor * (params.minPrintHeadDiameterPx / 2)).coerceAtLeast(1f)
        val maxRadius = minRadius * 5
        val radius = actualRadius.coerceIn(minRadius, maxRadius)

        canvas.drawArc(
            centerX = position.x,
            centerY = position.y,
            radius = radius,
            startDegrees = 0f,
            sweepAngle = 360f,
            paint = paint
        )
    }

    private inline fun GcodeNativeCanvas.withGcodeCoordinateSystem(params: RenderParams, block: GcodeNativeCanvas.() -> Unit) = withSave {
        with(values) {
            translate(xOffset, yOffset + printBedHeight)
            scale(totalFactor, -totalFactor)
            if (params.originInCenter) {
                translate(params.printBedWidthMm * 0.5f, params.printBedHeightMm * 0.5f)
            }
        }

        block()
    }

    private fun GcodeNativeCanvas.drawArcs(arcs: List<GcodeMove.Arc>, paint: Paint) = arcs.forEach { arc ->
        drawArc(
            centerX = arc.leftX + arc.radius,
            centerY = arc.topY + arc.radius,
            radius = arc.radius,
            startDegrees = arc.startAngle,
            sweepAngle = arc.sweepAngle,
            paint = paint
        )
    }

    private inline fun GcodeNativeCanvas.withSave(block: GcodeNativeCanvas.() -> Unit) {
        saveState()
        block()
        restoreState()
    }

    private fun GcodeMove.Type.configurePaint(colorOverride: Color? = null) {
        paint.mode = Paint.Mode.Stroke
        paint.strokeWidth = when (this) {
            GcodeMove.Type.Travel -> values.gcodeOtherStrokeWidth
            else -> values.gcodeExtrusionStrokeWidth
        }
        paint.color = colorOverride ?: when (this) {
            GcodeMove.Type.Travel -> Color.MoveTravel
            GcodeMove.Type.Extrude -> Color.MoveExtrusion
            GcodeMove.Type.Unsupported -> Color.MoveUnsupported
        }
    }

    private fun enforceScrollLimits(params: RenderParams) {
        val minXOffset = canvas.viewPortWidth - (params.printBedWidthMm * params.mmToPxFactor * zoom) - canvas.paddingLeft - canvas.paddingRight
        val minYOffset = canvas.viewPortHeight - (params.printBedHeightMm * params.mmToPxFactor * zoom) - canvas.paddingTop - canvas.paddingBottom

        scrollOffsetX = scrollOffsetX.coerceAtLeast(minXOffset).coerceAtMost(0f)
        scrollOffsetY = scrollOffsetY.coerceAtLeast(minYOffset).coerceAtMost(0f)
    }

    private val paddedWidth
        get() = with(canvas) { viewPortWidth - paddingLeft - paddingRight }

    private val paddedHeight
        get() = with(canvas) { viewPortHeight - paddingTop - paddingBottom }

    @Suppress("UnusedReceiverParameter")
    private val RenderParams.edgeLength
        get() = min(paddedWidth, paddedHeight)

    private val RenderParams.mmToPxFactor
        get() = edgeLength / printBedWidthMm

    private val RenderParams.pxToMmFactor
        get() = printBedWidthMm / edgeLength

    private val RenderParams.printBedVerticalStretch: Float
        get() = when (printBed) {
            // For the lip
            Image.PrintBedEnder, Image.PrintBedCreality -> 1.04f
            else -> 1f
        }

    data class Values(
        var xOffset: Float = 0f,
        var yOffset: Float = 0f,
        var totalFactor: Float = 0f,
        var pxToMmX: (Float) -> Float = { it },
        var pxToMmY: (Float) -> Float = { it },
        var gcodeOtherStrokeWidth: Float = 0f,
        var gcodeExtrusionStrokeWidth: Float = 0f,
        var backgroundWidth: Float = 0f,
        var backgroundHeight: Float = 0f,
        var printBedHeight: Float = 0f,
        var printBedWidth: Float = 0f,
    )

    data class RenderParams(
        val renderContext: GcodeRenderContext,
        val printBed: Image,
        val printBedWidthMm: Float,
        val printBedHeightMm: Float,
        val minPrintHeadDiameterPx: Float,
        val extrusionWidthMm: Float = 0.5f,
        val originInCenter: Boolean,
        val quality: GcodePreviewSettings.Quality,
    )
}