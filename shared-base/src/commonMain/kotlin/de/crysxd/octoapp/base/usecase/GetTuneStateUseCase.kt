package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.SerialCommunication
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.tune.TuneState
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.isActive
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class GetTuneStateUseCase(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val serialCommunicationLogsRepository: SerialCommunicationLogsRepository,
    private val executeGcodeCommandUseCase: ExecuteGcodeCommandUseCase,
) : UseCase2<GetTuneStateUseCase.Params, Flow<TuneState>>() {

    //region OctoPrint values
    // Matches either
    // "N1907 M221 S78*96" -> Flow rate command send during print
    // "Recv: echo:E0 Flow: 100%" -> Response to M221 command
    private val flowRatePattern = Regex("(M221 S|Recv.*Flow: )(\\d+)")

    // Matches all of:
    // "N1907 M220 S78*96" -> Feed rate command send during print
    // "Recv: FR:78%" -> Response to M220 command
    private val feedRatePattern = Regex("(M220 S|Recv.*FR:)(\\d+)")

    // Matches all of
    // "M106 S255" -> M106 command to set fan
    // "M107" -> M107 command to turn fan off
    private val fanSpeedPattern = Regex("M106( S(\\d+))?")
    private val fanOffPattern = Regex("M107")

    // Matches all of
    // "Recv: echo:Probe OffsetZ: 0.01" -> Response to offset change
    // "Recv: echo:Probe Offset Z0.01" -> Response to offset change
    private val zOffsetPattern = Regex("Probe Offset\\s*:?\\s*Z:?\\s*(-?\\d+\\.\\d+)")
    private val settingsPollInterval = 30.seconds
    private val settingsPollInitialDelay = 500.milliseconds
    //endregion

    override suspend fun doExecute(param: Params, logger: Logger): Flow<TuneState> {
        return when (printerEngineProvider.printer(instanceId = param.instanceId)) {
            is OctoPrintEngine -> buildOctoPrintTuneStateFlow(instanceId = param.instanceId, logger = logger, activePoll = param.activePoll)
            is MoonrakerEngine -> buildMoonrakerPrintTuneStateFlow(instanceId = param.instanceId)
            else -> throw IllegalStateException("Engine is neither OctoPrint nor Moonraker")
        }.combine(printerConfigurationRepository.instanceInformationFlow(param.instanceId)) { state, instance ->
            state.copy(
                canResetOffsets = SystemInfo.Capability.ResetZOffset in (instance?.systemInfo?.capabilities ?: emptyList()),
                fans = state.fans.sortedBy { it.label + it.component }
            )
        }.rateLimit(rateMs = 500)
    }

    //region OctoPrint
    private fun buildOctoPrintTuneStateFlow(instanceId: String, logger: Logger, activePoll: Boolean): Flow<TuneState> = combine(
        observeCommunicationFlow(instanceId = instanceId, logger = logger),
        pollValuesFlow(instanceId = instanceId, logger = logger, activePoll = activePoll)
    ) { state, _ ->
        state
    }.onStart {
        // Emit the initial value
        emit(extractValues(comm = null, current = TuneState()))
    }

    private fun observeCommunicationFlow(instanceId: String, logger: Logger) = flow {
        // Emit the initial value
        var current = extractValues(comm = null, current = TuneState())

        // Emit all changes
        serialCommunicationLogsRepository.passiveFlow(includeOld = true, instanceId = instanceId)
            .map {
                current = extractValues(it, current)
                current
            }
            .retry {
                logger.e(message = "Failed to parse serial communication", e = it)
                delay(1.seconds)
                true
            }.let {
                emitAll(it)
            }
    }

    private fun pollValuesFlow(logger: Logger, instanceId: String, activePoll: Boolean) = if (activePoll) {
        flow {
            emit(Unit)
            delay(settingsPollInitialDelay)
            while (currentCoroutineContext().isActive) {
                executeGcodeCommandUseCase.execute(
                    ExecuteGcodeCommandUseCase.Param(
                        command = GcodeCommand.Batch(listOf("M220", "M221")),
                        fromUser = false,
                        instanceId = instanceId,
                    )
                )
                delay(settingsPollInterval)
            }
        }.onStart {
            logger.i("Start polling values")
        }.onCompletion {
            logger.i("Stop polling values")
        }
    } else {
        flowOf(Unit)
    }

    private fun extractValues(comm: SerialCommunication?, current: TuneState): TuneState {
        var print = current.print ?: TuneState.Print()
        var fan = current.fans.firstOrNull() ?: TuneState.Fan(component = "fan", label = getString("tune_print___fan_speed_label"), isPartCoolingFan = true)
        var offsets = current.offsets ?: TuneState.Offsets()

        comm?.content?.extractValue(regex = flowRatePattern, groupIndex = 2)?.let { value ->
            print = print.copy(flowRate = value.toFloatOrNull())
        }
        comm?.content?.extractValue(regex = feedRatePattern, groupIndex = 2)?.let { value ->
            print = print.copy(feedRate = value.toFloatOrNull())
        }
        comm?.content?.extractValue(regex = fanSpeedPattern, groupIndex = 2)?.let { value ->
            fan = fan.copy(speed = value.toIntOrNull()?.let { i -> ((i / 255f) * 100f) })
        }
        comm?.content?.extractValue(regex = fanOffPattern, groupIndex = 0)?.let { _ ->
            fan = fan.copy(speed = 0f)
        }
        comm?.content?.extractValue(regex = zOffsetPattern, groupIndex = 1)?.let { value ->
            offsets = offsets.copy(z = value.toFloatOrNull())
        }

        return TuneState(
            isValuesImprecise = true,
            print = print,
            offsets = offsets,
            fans = listOf(fan),
        )
    }

    private fun String.extractValue(regex: Regex, groupIndex: Int): String? = regex.find(this)?.groupValues?.getOrNull(groupIndex)

    //endregion
    //region Moonraker
    private fun buildMoonrakerPrintTuneStateFlow(instanceId: String) = printerEngineProvider.passiveCurrentMessageFlow(
        instanceId = instanceId,
        tag = "GetTuneStateUseCase"
    ).map { message ->
        message.tuneState ?: TuneState()
    }
    //endregion

    data class Params(
        val instanceId: String,
        val activePoll: Boolean,
    )
}