package de.crysxd.octoapp.base.data.source

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.gcode.GcodeParser
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.ConnectivityHelper
import de.crysxd.octoapp.base.utils.measureTime
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.shareIn
import kotlin.time.Duration.Companion.seconds

class RemoteGcodeFileDataSource(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val localDataSource: LocalGcodeFileDataSource,
    private val connectivityHelper: ConnectivityHelper = ConnectivityHelper(),
    private val octoPreferences: OctoPreferences,
) : GcodeFileDataSource {

    private val tag = "RemoteGcodeFileDataSource"
    private var flowCaches = mutableMapOf<String, Flow<GcodeFileDataSource.LoadState>>()

    fun loadFile(file: FileReference.File, allowLargeFileDownloads: Boolean) = flowCaches[file.cacheKey]?.let {
        Napier.i(tag = tag, message = "[GCD] Reusing download flow for ${file.cacheKey}")
        it
    } ?: let {
        Napier.i(tag = tag, message = "[GCD] Starting download flow for ${file.cacheKey} (available: ${flowCaches.keys} @ $this)")
        flow {
            Napier.i(tag = tag, message = "[GCD] Download flow active")
            val onMeteredNetwork = kotlinx.coroutines.withTimeoutOrNull(2.seconds) {
                connectivityHelper.connectivityState.first() == ConnectivityHelper.ConnectivityType.Metered
            } ?: true
            Napier.i(tag = tag, message = "[GCD] Network metered: $onMeteredNetwork")
            val settings = printerConfigurationRepository.getActiveInstanceSnapshot()?.settings ?: printerEngineProvider.printer().settingsApi.getSettings()
            val maxFileSize = settings.plugins.gcodeViewer?.let {
                if (onMeteredNetwork) it.mobileSizeThreshold else it.sizeThreshold
            }
            Napier.i(tag = tag, message = "[GCD] Max file size for direct download is ${maxFileSize?.formatAsFileSize()}, file has ${file.size?.formatAsFileSize()}")

            if (maxFileSize != null && !allowLargeFileDownloads && (file.size ?: Long.MAX_VALUE) > maxFileSize) {
                return@flow emit(GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired(file.size ?: -1))
            }

            emit(GcodeFileDataSource.LoadState.Loading(0f))

            measureTime("Download and parse file") {
                try {
                    Napier.i(tag = tag, message = "[GCD] Downloading and parsing ${file.path}")
                    val gcode = printerEngineProvider.printer().filesApi.downloadFile(file) { input ->
                        Napier.i(tag = tag, message = "[GCD] Received input for ${file.path}")
                        localDataSource.createCacheForFile(file).use { cache ->
                            Napier.i(tag = tag, message = "[GCD] Created cache context for ${file.path}, starting to parse")
                            GcodeParser(
                                content = input,
                                totalSize = file.size ?: Long.MAX_VALUE,
                                progressUpdate = { progress ->
                                    emit(GcodeFileDataSource.LoadState.Loading(progress))
                                    Napier.v(tag = tag, message = "[GCD] Parsing ${file.path}: $progress")
                                },
                                layerSink = { cache.cacheLayer(it) },
                                detectLayersFromZ = !octoPreferences.detectLayersFromSlicer,
                            ).parseFile().also {
                                Napier.i(tag = tag, message = "[GCD] Parsed ${file.path} with ${it.layers.size} layers")
                            }
                        }
                    }
                    emit(GcodeFileDataSource.LoadState.Ready(gcode))
                } catch (e: Exception) {
                    Napier.e(tag = tag, message = "Failed to load Gcode", throwable = e)
                    emit(GcodeFileDataSource.LoadState.Failed(e))
                }
            }
        }.flowOn(Dispatchers.SharedIO).onCompletion {
            Napier.i(tag = tag, message = "[GCD] Removing download flow for ${file.cacheKey}")
            flowCaches.remove(file.cacheKey)
        }.catch { e ->
            Napier.i(tag = tag, message = "[GCD] Failed to download Gcode", throwable = e)
            emit(GcodeFileDataSource.LoadState.Failed(e))
        }.shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay, replay = 1).also {
            Napier.i(tag = tag, message = "[GCD] Storing download flow for ${file.cacheKey}")
            flowCaches[file.cacheKey] = it
        }
    }

    private val FileReference.File.cacheKey get() = "$path-$date"
}