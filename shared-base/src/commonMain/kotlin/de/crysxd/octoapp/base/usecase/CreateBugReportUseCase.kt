package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.framework.urlFromEncodedPath
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngine
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngine
import de.crysxd.octoapp.sharedcommon.JavaSerializable
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.utils.io.core.toByteArray
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.reflect.KClass
import kotlin.time.Duration.Companion.seconds

class CreateBugReportUseCase(
    private val printerConfigRepository: PrinterConfigurationRepository,
    private val getAppLanguageUseCase: GetAppLanguageUseCase,
    private val octoPreferences: OctoPreferences,
    private val printerEngineProvider: PrinterEngineProvider,
    private val platform: Platform,
) : UseCase2<CreateBugReportUseCase.Params, CreateBugReportUseCase.BugReport>() {

    companion object Sources {
        private val sources = mutableMapOf<String, suspend () -> BugReport.File?>()
        fun registerSource(name: String, source: suspend () -> BugReport.File?) {
            sources[name] = source
        }
    }

    override suspend fun doExecute(param: Params, logger: Logger) = withContext(Dispatchers.SharedIO) {
        val files = param.extraFiles.toMutableList()

        if (param.sendLogs) {
            files += createLogFile()
            files += createOtherLogFiles(logger)
            files += createPluginLogFile(logger)
        }

        if (param.sendPhoneInfo) {
            files += createPhoneInfoFile()
            files += createSettingsFile(logger)
        }

        if (param.sendOctoPrintInfo) {
            files += createInstanceInfoFiles()
            files += createSetupInfoFiles(logger)
        }

        files += createBillingFile()

        BugReport(
            files = files,
            appVersion = platform.appVersion,
            platformName = platform.platformName,
        )
    }

    private suspend fun createLogFile(): BugReport.File = BugReport.File(
        bytes = CachedAntiLog.getCache(),
        name = "app.log",
    )

    private suspend fun createSetupInfoFiles(logger: Logger) = downloadFiles(
        logger = logger,
        filesToLoad = { activeInstanceId ->
            listOf(
                Triple(MoonrakerEngine::class, "moonraker-system-info-$activeInstanceId.json", listOf("machine", "system_info")),
                Triple(MoonrakerEngine::class, "octoapp-database-$activeInstanceId.json", listOf("server", "database", "item?namespace=octoapp")),
                Triple(MoonrakerEngine::class, "mainsail-database-$activeInstanceId.json", listOf("server", "database", "item?namespace=mainsail")),
                Triple(MoonrakerEngine::class, "fluidd-database-$activeInstanceId.json", listOf("server", "database", "item?namespace=fluidd")),
                Triple(MoonrakerEngine::class, "fluidd-webcam-database-$activeInstanceId.json", listOf("server", "database", "item?namespace=webcams")),
                Triple(MoonrakerEngine::class, "moonraker-webcams-$activeInstanceId.json", listOf("server", "webcams", "list")),
            )
        }
    )

    private suspend fun createPluginLogFile(logger: Logger) = downloadFiles(
        logger = logger,
        filesToLoad = { activeInstanceId ->
            listOf(
                Triple(OctoPrintEngine::class, "octoapp-plugin-$activeInstanceId.log", listOf("downloads", "logs", "plugin_octoapp.log")),
                Triple(MoonrakerEngine::class, "octoapp-plugin-$activeInstanceId.log", listOf("server", "files", "logs", "octoapp.log")),
            )
        }
    )

    private suspend fun downloadFiles(
        logger: Logger,
        filesToLoad: (String) -> List<Triple<KClass<out PrinterEngine>, String, List<String>>>
    ): List<BugReport.File> {
        // Get active, we only include plugin logs for one
        val activeInstanceId = printerConfigRepository.getActiveInstanceSnapshot()?.id ?: return emptyList()
        val engine = printerEngineProvider.printer(instanceId = activeInstanceId)

        return filesToLoad(activeInstanceId).mapNotNull { (t, path, name) ->
            if (t != engine::class) null else (path to name)
        }.map { (name, path) ->
            try {
                //  Always attempt to download logs as the plugin might not be active/installed right now but might have been
                val text = withTimeout(5.seconds) {
                    engine.genericRequest { baseUrl, httpClient ->
                        httpClient.get {
                            urlFromEncodedPath(baseUrl, path)
                            logger.i("Downloading logs for $activeInstanceId: $url")
                        }
                    }
                }.bodyAsText()
                val bytes = SensitiveDataMask.mask(text).toByteArray()

                // Create file
                BugReport.File(
                    bytes = bytes,
                    name = name
                )
            } catch (e: Exception) {
                logger.w(e)
                BugReport.File(
                    bytes = e.stackTraceToString().toByteArray(),
                    name = name
                )
            }
        }
    }

    private suspend fun createPhoneInfoFile(): BugReport.File {
        val text = StringBuilder()
        val appLanguage = getAppLanguageUseCase.execute(Unit).appLanguage

        text.appendLine("device_model = ${platform.deviceModel}")
        text.appendLine("device_name = ${platform.deviceName}")
        text.appendLine("device_language = ${platform.deviceLanguage}")
        text.appendLine("platform_name = ${platform.platformName}")
        text.appendLine("platform_version = ${platform.platformVersion}")
        text.appendLine("app_version = ${platform.appVersion}")
        text.appendLine("app_build = ${platform.appBuild}")
        text.appendLine("app_language = $appLanguage")

        return BugReport.File(
            bytes = text.toString().encodeToByteArray(),
            name = "device.txt",
        )
    }

    private fun createBillingFile(): BugReport.File = BugReport.File(
        bytes = listOf(
            "shouldAdvertisePremium=${BillingManager.shouldAdvertisePremium()}",
            "gcodePreview=${BillingManager.isFeatureEnabled(FEATURE_GCODE_PREVIEW)}",
            "",
            BillingManager.state.value.purchases.joinToString("\n\n") { "[${it.productId}]\n    transactionId=${it.transactionId}\n    token=${it.token}" },
        ).joinToString("\n").encodeToByteArray(),
        name = "billing.txt",
    )

    @OptIn(OctoPreferences.RawAccess::class)
    private fun createSettingsFile(logger: Logger): BugReport.File {
        val text = StringBuilder()
        val settings = octoPreferences.settings

        fun <T> save(block: () -> T?) = try {
            block()
        } catch (e: Exception) {
            null
        }

        fun getAnyValue(key: String) = try {
            save { settings.getStringOrNull(key) }
                ?: save { settings.getBooleanOrNull(key) }
                ?: save { settings.getIntOrNull(key) }
                ?: save { settings.getLongOrNull(key) }
                ?: save { settings.getDoubleOrNull(key) }
                ?: save { settings.getFloatOrNull(key) }
        } catch (e: Exception) {
            logger.w(e)
            "ERROR"
        }

        settings.keys.forEach {
            text.appendLine("$it = ${getAnyValue(it)}")
        }

        return BugReport.File(
            bytes = text.toString().encodeToByteArray(),
            name = "settings.txt",
        )
    }

    private suspend fun createOtherLogFiles(logger: Logger): List<BugReport.File> = sources.values.mapNotNull { source ->
        try {
            source()
        } catch (e: Exception) {
            logger.e(e)
            null
        }
    }

    private suspend fun createInstanceInfoFiles(): List<BugReport.File> {
        val activeId = printerConfigRepository.getActiveInstanceSnapshot()?.id
        return printerConfigRepository.getAll().map {
            BugReport.File(
                bytes = try {
                    SensitiveDataMask.mask(Json.encodeToString(it))
                } catch (e: Exception) {
                    SensitiveDataMask.mask(e.stackTraceToString())
                }.encodeToByteArray(),
                name = "${if (activeId == it.id) "active_" else ""}${it.id}.json",
            )
        }
    }

    data class Params(
        val sendPhoneInfo: Boolean = true,
        val sendLogs: Boolean = true,
        val sendOctoPrintInfo: Boolean = true,
        val extraFiles: List<BugReport.File> = emptyList()
    )

    @Serializable
    data class BugReport(
        val files: List<File>,
        val appVersion: String,
        val platformName: String,
    ) : JavaSerializable {
        @Serializable
        class File(
            val bytes: ByteArray,
            val name: String,
        ) : JavaSerializable {
            override fun toString() = "BugReport.File(name=$name, bytes=${bytes.size})"
        }
    }
}