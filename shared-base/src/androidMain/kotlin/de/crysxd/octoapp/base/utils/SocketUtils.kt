package de.crysxd.octoapp.base.utils

import java.io.Closeable

fun Closeable.closeQuietly() = try {
    close()
} catch (e: Exception) {
    // Ignore
}