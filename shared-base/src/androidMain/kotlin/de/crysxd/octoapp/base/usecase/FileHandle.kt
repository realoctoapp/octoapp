package de.crysxd.octoapp.base.usecase

import android.content.ContentResolver
import android.net.Uri
import android.provider.OpenableColumns
import de.crysxd.octoapp.sharedcommon.Platform
import io.github.aakira.napier.Napier
import io.ktor.client.request.forms.InputProvider
import io.ktor.utils.io.streams.asInput
import okio.FileSystem
import java.io.File


internal actual fun UploadFileHandle(path: String, platform: Platform, fileSystem: FileSystem): UploadFileUseCase.UploadFileHandle {
    val file = File(path)
    val uri = Uri.parse(path)
    val isUri = File(path).exists().not()
    val contentResolver = platform.context.contentResolver
    val (name, size) = if (isUri) {
        uri.contentSchemeNameAndSize(contentResolver)
    } else {
        file.name to file.length().toInt()
    }

    return UploadFileUseCase.UploadFileHandle(
        name = name,
        withInput = { block ->
            Napier.i(tag = "UploadFileHandle", message = "Opening $path ")

            val input = if (isUri) contentResolver.openInputStream(uri) else file.inputStream()
            requireNotNull(input) { "Unable to open $uri as input" }.use { stream ->
                block(
                    InputProvider(size.toLong()) {
                        stream.asInput()
                    }
                )
            }
            Napier.i(tag = "UploadFileHandle", message = "$path closed")
        }
    )
}

private fun Uri.contentSchemeNameAndSize(contentResolver: ContentResolver): Pair<String, Int> {
    return contentResolver.query(this, null, null, null, null)?.use { cursor ->
        if (!cursor.moveToFirst()) return@use null

        val name = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        val size = cursor.getColumnIndex(OpenableColumns.SIZE)

        cursor.getString(name) to cursor.getInt(size)
    } ?: path?.let { File(it) }?.takeIf { it.exists() }?.let {
        it.name to it.length().toInt()
    } ?: throw IllegalStateException("Unable to get name and size from URI: $this")
}