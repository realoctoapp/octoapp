package de.crysxd.octoapp.base

import com.google.firebase.messaging.FirebaseMessaging
import de.crysxd.octoapp.base.ext.blockingAwait

actual object OctoPushMessaging {
    actual suspend fun getPushToken() = FirebaseMessaging.getInstance().token.blockingAwait()
}