package de.crysxd.octoapp.base.billing

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import com.android.billingclient.api.AcknowledgePurchaseParams
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.PendingPurchasesParams
import com.android.billingclient.api.ProductDetails
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.android.billingclient.api.QueryProductDetailsParams
import com.android.billingclient.api.QueryProductDetailsParams.Product
import com.android.billingclient.api.QueryPurchasesParams
import com.android.billingclient.api.acknowledgePurchase
import com.android.billingclient.api.queryProductDetails
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import de.crysxd.octoapp.base.models.BillingProduct
import de.crysxd.octoapp.base.models.BillingPurchase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlin.time.Duration.Companion.seconds

class PlayBillingAdapter(private val context: Context) : BillingAdapter {

    private val tag = "PlayBillingAdapter"
    private val scope = AppScope
    private var billingClient: BillingClient? = null
    private var purchasesUpdated: (List<BillingPurchase>) -> Unit = {}
    private var notifyPurchaseCompleted: () -> Unit = {}
    private val mutex = Mutex()

    //region Listeners
    private val clientStateListener = object : BillingClientStateListener {
        val billingSetupResult = MutableSharedFlow<BillingResult>()
        override fun onBillingServiceDisconnected() {
            Napier.i(tag = tag, message = "Billing disconnected")
            billingClient = null
        }

        override fun onBillingSetupFinished(p0: BillingResult) {
            Napier.i(tag = tag, message = "Billing setup finished")
            scope.launch {
                billingSetupResult.emit(p0)
            }
        }
    }
    private val purchasesUpdateListener = PurchasesUpdatedListener { result, purchases ->
        logError("Billing result update failed", result)
        if (purchases == null) {
            Napier.e(tag = tag, message = "Refuse to handle null purchase update")
        } else AppScope.launch {
            Napier.i(tag = tag, message = "Received purchase update: ${purchases.map { it.products }.flatten()}")
            handlePurchases(purchases = purchases, type = "mixed")
            purchasesUpdated(purchases.map())
        }
    }
    //endregion

    override suspend fun initBilling(
        purchasesUpdated: (List<BillingPurchase>) -> Unit,
        notifyPurchaseCompleted: () -> Unit
    ) = mutex.withLock {
        //region Link callbacks
        Napier.i(tag = tag, message = "Connecting Play billing")
        this.notifyPurchaseCompleted = notifyPurchaseCompleted
        this.purchasesUpdated = purchasesUpdated
        //endregion
        //region Connect
        billingClient?.endConnection()
        billingClient = BillingClient.newBuilder(context)
            .setListener(purchasesUpdateListener)
            .enablePendingPurchases(PendingPurchasesParams.newBuilder().enablePrepaidPlans().enableOneTimeProducts().build())
            .build()
            .apply { startConnection(clientStateListener) }
        val result = withTimeout(15.seconds) { clientStateListener.billingSetupResult.first() }
        //endregion
        //region Validate connection
        Napier.d(tag = tag, message = "Play billing connection result: ${result.responseCode} -> ${result.debugMessage}")
        if (result.responseCode == BillingClient.BillingResponseCode.OK) {
            Napier.i(tag = tag, message = "Play billing connected")
        } else {
            Napier.w(tag = tag, message = "Play billing not connected: ${result.debugMessage}")
            billingClient?.endConnection()
            billingClient = null
            throw SuppressedIllegalStateException("Failed to connect play billing: ${result.debugMessage} (${result.responseCode})")
        }
        //endregion
    }

    override suspend fun destroyBilling() = mutex.withLock {
        Napier.i(tag = tag, message = "Disconnecting play billing")
        billingClient?.endConnection()
        billingClient = null
        purchasesUpdated = {}
        notifyPurchaseCompleted = {}
        Napier.i(tag = tag, message = "Play billing disconnected")
    }

    override fun isAvailable() = try {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context)
        resultCode == ConnectionResult.SUCCESS
    } catch (e: Exception) {
        Napier.e(throwable = e, message = "Failed", tag = tag)
        false
    }

    override suspend fun loadProducts(productIds: List<String>): List<BillingProduct> {
        val client = requireNotNull(billingClient) { "Billing client is not ready (1)" }
        //region Skip on WearOS
        if (context.packageManager.hasSystemFeature(PackageManager.FEATURE_WATCH)) {
            Napier.i(tag = tag, message = "Skipping products query on WearOS")
            return emptyList()
        }
        //endregion
        Napier.i(tag = tag, message = "Loading products")
        suspend fun loadProducts(type: String): List<BillingProduct> {
            //region Query products
            val products = productIds.filter { product ->
                val sub = product.isSubscriptionProduct()
                (sub && type == BillingClient.ProductType.SUBS) || (!sub && type == BillingClient.ProductType.INAPP)
            }.map { productId ->
                productId to Product.newBuilder().setProductId(productId).setProductType(type).build()
            }
            if (products.isEmpty()) {
                Napier.w(tag = tag, message = "No products for $type")
                return emptyList()
            }
            Napier.i(tag = tag, message = "Loading products for $type: ${products.map { it.first }}")
            val result = client.queryProductDetails(
                QueryProductDetailsParams.newBuilder().setProductList(
                    products.map { it.second }
                ).build()
            )
            //endregion
            //region Validate result and map
            logError("Failed to load products for $type", result.billingResult)
            return result.productDetailsList?.map { product ->
                BillingProduct(
                    productId = product.productId,
                    productName = product.title,
                    nativeProduct = product,
                    price = product.oneTimePurchaseOfferDetails?.formattedPrice
                        ?: product.subscriptionOfferDetails?.first { it.offerId != "freetrial" }?.pricingPhases?.pricingPhaseList?.firstOrNull()?.formattedPrice
                        ?: "",
                    priceOrder = product.oneTimePurchaseOfferDetails?.priceAmountMicros?.toInt()
                        ?: product.subscriptionOfferDetails?.first { it.offerId != "freetrial" }?.pricingPhases?.pricingPhaseList?.firstOrNull()?.priceAmountMicros?.toInt()
                        ?: 0,
                )
            }.also { productList ->
                Napier.i(tag = tag, message = "Received products for $type: ${productList?.map { it.productId + " (${it.price})" }}")
            } ?: emptyList()
            //endregion
        }

        return withContext(Dispatchers.IO) {
            listOf(
                async { loadProducts(BillingClient.ProductType.SUBS) },
                async { loadProducts(BillingClient.ProductType.INAPP) },
            ).map { deferred ->
                deferred.await()
            }.flatten().also { productList ->
                Napier.i(tag = tag, message = "Received products: ${productList.map { it.productId + " (${it.price})" }}")
            }
        }
    }

    override suspend fun loadPurchases(): List<BillingPurchase> = withContext(Dispatchers.SharedIO) {
        val client = requireNotNull(billingClient) { "Billing client is not ready (2)" }
        Napier.i(tag = tag, message = "Loading purchases from Play")
        listOf(BillingClient.ProductType.INAPP, BillingClient.ProductType.SUBS).map { type ->
            //region Query purchase by type
            type to async {
                val resultFlow = MutableSharedFlow<Pair<BillingResult, List<Purchase>>>()
                client.queryPurchasesAsync(QueryPurchasesParams.newBuilder().setProductType(type).build()) { result, list ->
                    scope.launch { resultFlow.emit(result to list) }
                }
                resultFlow.first()
            }
            //endregion
        }.map { (type, deferred) ->
            //region Validate response
            val (result, list) = deferred.await()
            Napier.i(tag = tag, message = "Received purchases for $type: ${list.joinToString { it.orderId ?: "???" }}")
            logError("Failed to load purchases for $type", result)
            type to list
            //endregion
        }.flatMap { (type, purchases) ->
            //region Map Purchases to BillingPurchase
            handlePurchases(purchases = purchases, type = type)
            purchases.map()
            //endregion
        }.also { purchases ->
            val text = purchases.map { "${it.transactionId}/${it.productId}" }
            Napier.i(tag = tag, message = "Received purchases: $text")
        }.filter { purchase ->
            if ((purchase.nativePurchase as? Purchase)?.isAcknowledged == true) {
                true
            } else {
                Napier.w(tag = tag, message = "Ignoring purchase ${purchase.transactionId}, it's not yet acknowledged")
                false
            }
        }
    }

    private fun List<Purchase>.map() = map { purchase ->
        purchase.products.map { product ->
            BillingPurchase(
                productId = product,
                isSubscription = product.isSubscriptionProduct(),
                token = purchase.purchaseToken,
                transactionId = purchase.orderId ?: "none",
                nativePurchase = purchase
            )
        }
    }.flatten()

    private fun String.isSubscriptionProduct() = contains("_sub_")

    private fun logError(description: String, billingResult: BillingResult?) {
        //region Log if error
        fun handle(message: String) {
            Napier.e(tag = tag, message = message)
            throw SuppressedIllegalStateException("$message: ${billingResult?.debugMessage} (${billingResult?.responseCode})")
        }

        when {
            billingResult == null -> handle(message = "Billing result is null, indicating billing connection was paused during an active process")
            billingResult.responseCode == BillingClient.BillingResponseCode.OK -> return
            billingResult.responseCode == 2 -> handle(message = "Service unavailable")
            billingResult.responseCode == 3 -> handle(message = "Billing is unavailable")
            billingResult.responseCode == -2 -> handle(message = "Client does not support ProductDetails")
            billingResult.responseCode == 12 -> handle(message = "Network error")
            billingResult.responseCode == 6 -> handle(message = "Internal error")
            billingResult.responseCode == -1 -> handle(message = "Service was disconnected")
            !isAvailable() -> handle(message = "BillingManager encountered problem but Play Services are not available")
            else -> {
                Napier.e(
                    tag = tag,
                    message = "BillingManager encountered problem but Play Services are available",
                    throwable = Exception("$description. responseCode=${billingResult.responseCode} message=${billingResult.debugMessage} billingResult=${billingResult.let { "non-null" }} playServicesAvailable=${isAvailable()}")
                )
                handle(message = "BillingManager encountered problem but Play Services are available")
            }
        }
        //endregion
    }

    private suspend fun handlePurchases(purchases: List<Purchase>, type: String) {
        try {
            //region  Collect all purchases, we cache purchases in the current session to prevent hick ups
            val allPurchases = purchases + BillingManager.state.value.purchases.mapNotNull { it.nativePurchase as? Purchase }
            val text = allPurchases.map { "${it.orderId}/${it.products.joinToString(",")}" }
            Napier.i(tag = tag, message = "Looking at ${allPurchases.size} purchases of type $type in total: $text")
            //endregion
            //region Activate purchases
            var purchaseEventSent = false
            purchases.forEach { purchase ->
                if (!purchase.isAcknowledged) {
                    if (purchase.orderId == null) {
                        Napier.w(tag = tag, message = "Unable to acknowledge purchase without orderId, purchase not completed: ${purchase.products}")
                    } else {
                        acknowledgePurchase(purchase)
                        if (!purchaseEventSent) {
                            purchaseEventSent = true
                            notifyPurchaseCompleted()
                        }
                    }
                }
            }
            //endregion
        } catch (e: Exception) {
            Napier.e(throwable = e, message = "Failed to handle purchases")
        }
    }

    private suspend fun acknowledgePurchase(purchase: Purchase) {
        Napier.i(tag = tag, message = "${purchase.orderId} is not yet acknowledged")
        val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
            .setPurchaseToken(purchase.purchaseToken)
        val billingResult = billingClient?.acknowledgePurchase(acknowledgePurchaseParams.build())
        logError("Failed to acknowledge purchase ${purchase.orderId}", billingResult)
        Napier.i(tag = tag, message = "Acknowledged purchase ${purchase.orderId}")
    }

    fun purchase(activity: Activity, product: BillingProduct, offerToken: String?) {
        Napier.i(tag = tag, message = "Launching billing flow for $product")
        val client = requireNotNull(billingClient) { "Billing client is not ready (3)" }
        //region client.launchBillingFlow(...)
        client.launchBillingFlow(
            activity,
            BillingFlowParams.newBuilder()
                .setIsOfferPersonalized(false)
                .setProductDetailsParamsList(
                    listOf(
                        BillingFlowParams.ProductDetailsParams.newBuilder()
                            .setProductDetails(product.nativeProduct as ProductDetails)
                            .apply {
                                if (offerToken != null) {
                                    setOfferToken(offerToken)
                                }
                            }
                            .build()
                    )
                )
                .build()
        )
        //endregion
    }
}