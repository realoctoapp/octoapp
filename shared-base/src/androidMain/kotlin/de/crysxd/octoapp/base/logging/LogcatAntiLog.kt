package de.crysxd.octoapp.base.logging

import android.util.Log
import io.github.aakira.napier.Antilog
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.LogLevel.ASSERT
import io.github.aakira.napier.LogLevel.DEBUG
import io.github.aakira.napier.LogLevel.ERROR
import io.github.aakira.napier.LogLevel.INFO
import io.github.aakira.napier.LogLevel.VERBOSE
import io.github.aakira.napier.LogLevel.WARNING

object LogcatAntiLog : Antilog() {

    override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
        when (priority) {
            VERBOSE -> Log.v(tag, message, throwable)
            DEBUG -> Log.d(tag, message, throwable)
            INFO -> Log.i(tag, message, throwable)
            WARNING -> Log.w(tag, message, throwable)
            ERROR -> Log.e(tag, message, throwable)
            ASSERT -> Log.wtf(tag, message, throwable)
        }
    }
}