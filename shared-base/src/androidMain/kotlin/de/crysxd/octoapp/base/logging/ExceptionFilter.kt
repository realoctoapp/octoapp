package de.crysxd.octoapp.base.logging

import com.google.firebase.remoteconfig.FirebaseRemoteConfigClientException
import java.net.URISyntaxException

actual fun Throwable.isPlatformExceptionDropped(): Boolean = this is java.io.IOException || this is URISyntaxException || this is FirebaseRemoteConfigClientException
