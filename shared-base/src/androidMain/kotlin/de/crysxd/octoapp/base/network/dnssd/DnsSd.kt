package de.crysxd.octoapp.base.network.dnssd

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.os.Build
import androidx.core.content.pm.PackageInfoCompat
import com.google.android.gms.common.GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import java.net.InetAddress

interface DnsSd {

    companion object {
        private var instance: DnsSd? = null

        fun create(context: Context): DnsSd {
            instance?.let { return@create it }

            // Google rolled out proper DNS-SD support via a Google Play Services update in late 2021. We check here that we have Android 12 and a GPS that supports .local
            // domains. If both are good we use the newer implementation as the LegacyDnsSd sometimes crashes on Android 12 and up.
            val hasAndroid12 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
            val enforcedType = SharedBaseInjector.get().preferences.mDnsSystem?.let { Type.valueOf(it) } ?: Type.Auto
            val (playServiceVersion, playServiceVersionName) = try {
                val info = context.packageManager.getPackageInfoCompat(GOOGLE_PLAY_SERVICES_PACKAGE, flags = 0)
                PackageInfoCompat.getLongVersionCode(info) to info.versionName
            } catch (e: NameNotFoundException) {
                // Play Services not installed
                0L to "none"
            } catch (e: Exception) {
                Napier.e(tag = "DnsSd", message = "Failed to check Play Services", throwable = e)
                0L to "none"
            }
            val hasRecentPlayServices = playServiceVersion >= 224312044
            val newAndroidDnsEnabled = OctoConfig.get(OctoConfigField.AndroidNewDnsSdEnabled)
            val selectedType = if (hasAndroid12 && hasRecentPlayServices && newAndroidDnsEnabled) {
                Type.Android
            } else {
                Type.Legacy
            }

            @Suppress("KotlinConstantConditions")
            val i = when (enforcedType.takeUnless { it == Type.Auto } ?: selectedType) {
                Type.Android -> {
                    Napier.w(
                        tag = "DnsSd",
                        message = "Using AndroidDnsSd: playServiceVersion=$playServiceVersion playServiceVersionName=$playServiceVersionName enforcedType=$enforcedType"
                    )
                    AndroidDnsSd(context)
                }

                Type.Legacy -> {
                    Napier.w(
                        tag = "DnsSd",
                        message = "Using LegacyDnsSd: playServiceVersion=$playServiceVersion playServiceVersionName=$playServiceVersionName newAndroidDnsEnabled=$newAndroidDnsEnabled enforcedType=$enforcedType"
                    )
                    LegacyDnsSd()
                }

                Type.Auto -> throw java.lang.IllegalStateException("Can't create auto")
            }

            instance = i
            return i
        }

        private fun PackageManager.getPackageInfoCompat(packageName: String, flags: Int = 0): PackageInfo =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                getPackageInfo(packageName, PackageManager.PackageInfoFlags.of(flags.toLong()))
            } else {
                getPackageInfo(packageName, flags)
            }
    }

    suspend fun browse(
        regType: String,
        serviceFound: (ServiceData) -> Unit,
        serviceLost: (ServiceData) -> Unit,
        failure: (Throwable) -> Unit,
    ): Operation

    suspend fun resolve(
        service: ServiceData,
        resolved: (Service) -> Unit,
        failure: (Throwable) -> Unit,
    ): Operation

    suspend fun resolve(
        hostName: String,
        resolved: (InetAddress) -> Unit,
        failure: (Throwable) -> Unit
    ): Operation

    interface Operation {
        fun stop()
    }

    interface ServiceData {
        val description: String
    }

    data class Service(
        val label: String,
        val webUrl: String,
        val port: Int,
        val host: InetAddress,
        val hostname: String,
    )

    enum class Type {
        Legacy, Android, Auto
    }
}
