import de.crysxd.octoapp.buildscript.octoAppMultiplatformLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.kotlinSerialization)
}

kotlin {
    androidTarget()
    iosX64()
    iosArm64()
    iosSimulatorArm64()

    sourceSets {
        commonMain.dependencies {
            api(projects.sharedCommon)
            api(projects.sharedEngines)
            api(projects.sharedExternalApis)
            api(libs.kotlin.math)
        }
        commonTest.dependencies {
            implementation(kotlin("test"))
        }
        androidMain.dependencies {
            api(project.dependencies.platform(libs.firebase.bom))
            api(libs.firebase.config.ktx)
            api(libs.firebase.crashlytics)
            api(libs.firebase.analytics.ktx)
            api(libs.firebase.messaging.ktx)
            api(libs.billing)
            api(libs.billing.ktx)
            api(libs.playservices.wearable)
            implementation(libs.happydns)
            implementation(libs.dnssd)
        }
    }

    //region Opt-in to expect/actual + Kotlin Parcelize
    targets.configureEach {
        compilations.configureEach {
            compileTaskProvider.configure {
                compilerOptions {
                    freeCompilerArgs.addAll(
                        "-opt-in=kotlin.ExperimentalUnsignedTypes,kotlin.RequiresOptIn",
                        "-Xexpect-actual-classes"
                    )
                    freeCompilerArgs.addAll(
                        "-P",
                        "plugin:org.jetbrains.kotlin.parcelize:additionalAnnotation=de.crysxd.octoapp.sharedcommon.CommonParcelize"
                    )
                }
            }
        }
    }
    //endregion
}

octoAppMultiplatformLibrary("sharedbase")
