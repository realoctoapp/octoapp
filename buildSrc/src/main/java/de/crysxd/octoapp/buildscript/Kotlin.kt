package de.crysxd.octoapp.buildscript

import com.android.build.api.dsl.ApplicationBuildType
import com.android.build.api.dsl.ApplicationExtension
import com.android.build.api.dsl.BuildType
import com.android.build.api.dsl.CommonExtension
import com.android.build.api.dsl.DefaultConfig
import com.android.build.api.dsl.LibraryBuildType
import com.android.build.api.dsl.LibraryExtension
import com.android.build.api.dsl.TestExtension
import com.android.build.api.variant.AndroidComponentsExtension
import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.api.plugins.PluginContainer
import org.gradle.kotlin.dsl.get
import org.gradle.kotlin.dsl.getByType
import java.io.File


fun Project.octoAppAndroidPlugins(
    isApp: Boolean = false,
    usesCompose: Boolean = true,
    configure: PluginContainer.() -> Unit = {}
) = plugins.run {
    apply(if (isApp) "com.android.application" else "com.android.library")
    apply("kotlin-parcelize")
    apply("kotlin-kapt")
    apply("kotlin-android")
    apply("androidx.navigation.safeargs.kotlin")
    apply("org.jetbrains.kotlin.plugin.serialization")

    if (usesCompose) {
        apply("org.jetbrains.kotlin.plugin.compose")
    }

    configure()
}

fun Project.octoAppMultiplatformPlugins(isApp: Boolean = false, configure: PluginContainer.() -> Unit = {}) = plugins.run {
    apply(if (isApp) "com.android.application" else "com.android.library")
    apply("org.jetbrains.kotlin.multiplatform")
    apply("com.android.library")
    apply("kotlin-parcelize")
    apply("org.jetbrains.kotlin.plugin.serialization")

    configure()
}

fun Project.octoAppAndroidBenchmark(
    name: String,
    targetProject: String,
    block: TestExtension.() -> Unit = {},
) {
    extensions.getByType(TestExtension::class).run {
        namespace = "${OctoAppBuildConfig.basePackage}.$name"
        compileSdk = OctoAppBuildConfig.compileSdk

        compileOptions {
            sourceCompatibility = OctoAppBuildConfig.javaVersion
            targetCompatibility = OctoAppBuildConfig.javaVersion
        }

        defaultConfig {
            minSdk = maxOf(28, OctoAppBuildConfig.minSdk)
            testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        }

        buildTypes {
            create("benchmark") {
                isDebuggable = true
                signingConfig = signingConfigs.getByName("debug")
                matchingFallbacks += listOf("release")
            }
        }

        @Suppress("UnstableApiUsage")
        experimentalProperties["android.experimental.self-instrumenting"] = true
        targetProjectPath = targetProject

        octoAppDesugaring(project = this@octoAppAndroidBenchmark)

        block()
    }

    extensions.findByType(AndroidComponentsExtension::class.java)?.apply {
        beforeVariants { variantBuilder ->
            if (variantBuilder.buildType == "benchmark") {
                variantBuilder.enable = true
            }
        }
    }

    dependencies.run {
        add("implementation", project(":base"))
        add("implementation", project(":app-test-framework"))
        add("implementation", loadLibs().findLibrary("test-benchmarkMacroJunit4").get())
    }
}


fun Project.octoAppAndroidApp(
    versionCodeToUse: Int,
    block: ApplicationExtension.() -> Unit = {}
) {
    extensions.getByType(ApplicationExtension::class).run {
        octoAppConfig(name = null, usesCompose = true, project = this@octoAppAndroidApp)
        octoAppDesugaring(project = this@octoAppAndroidApp)
        octoAppBuildTypes(project = this@octoAppAndroidApp)

        defaultConfig {
            targetSdk = OctoAppBuildConfig.compileSdk
            versionCode = versionCodeToUse
            versionName = getVersionName()
        }

        packaging {
            resources {
                excludes += "/META-INF/{AL2.0,LGPL2.1}"
                excludes += "/META-INF/INDEX.LIST"
                excludes += "/META-INF/io.netty.versions.properties"
            }
        }

        block()
    }

    dependencies.run {
        add("androidTestImplementation", project(":app-test-framework"))
    }

    octoAppDagger()
}

fun Project.octoAppAndroidLibrary(
    name: String,
    usesCompose: Boolean = true,
    usesDagger: Boolean = true,
    usesTestFramework: Boolean = true,
    block: LibraryExtension.() -> Unit = {}
) {
    octoAppLibrary(name = name, usesCompose = usesCompose) {
        octoAppBuildTypes(project = this@octoAppAndroidLibrary)
        octoAppDesugaring(project = this@octoAppAndroidLibrary)
        block()
    }

    if (usesTestFramework) {
        dependencies.run {
            add("androidTestImplementation", project(":app-test-framework"))
        }
    }

    if (usesDagger) {
        octoAppDagger()
    }
}

fun Project.octoAppMultiplatformLibrary(
    name: String,
    block: LibraryExtension.() -> Unit = {}
) = octoAppLibrary(name = name, usesCompose = false) {
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    sourceSets["main"].res.srcDirs("src/androidMain/res")
    sourceSets["main"].resources.srcDirs("src/commonMain/resources")
    block()
}

private fun Project.octoAppLibrary(
    name: String,
    usesCompose: Boolean,
    block: LibraryExtension.() -> Unit,
) = extensions.getByType(LibraryExtension::class).run {
    octoAppConfig(name, usesCompose, project = this@octoAppLibrary)
    block()
}

private fun <BuildTypeT : BuildType> CommonExtension<*, BuildTypeT, *, *, *, *>.octoAppConfig(
    name: String?,
    usesCompose: Boolean,
    project: Project,
) {
    namespace = "${OctoAppBuildConfig.basePackage}${name?.let { ".$it" } ?: ""}"
    compileSdk = OctoAppBuildConfig.compileSdk
    defaultConfig {
        minSdk = OctoAppBuildConfig.minSdk
        testInstrumentationRunnerArguments["clearPackageData"] = "true"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        sourceCompatibility = OctoAppBuildConfig.javaVersion
        targetCompatibility = OctoAppBuildConfig.javaVersion
    }

    @Suppress("UnstableApiUsage")
    testOptions {
        animationsDisabled = true
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
    }

    buildFeatures {
        viewBinding = true
        compose = usesCompose
        buildConfig = true
    }

    lint {
        showAll = true
        checkDependencies = false
        lintConfig = File(OctoAppBuildConfig.rootDir, OctoAppBuildConfig.project.lintConfig)
    }

    project.dependencies.run {
        add("androidTestUtil", project.loadLibs().findLibrary("test-orchestrator").get())
    }
}

private fun <BuildTypeT : BuildType, DefaultConfigT : DefaultConfig> CommonExtension<*, BuildTypeT, DefaultConfigT, *, *, *>.octoAppBuildTypes(
    project: Project,
    extraReleaseConfig: BuildTypeT.() -> Unit = {},
) {
    (this as? ApplicationExtension)?.let {
        signingConfigs {
            create("releaseSigning") {
                OctoAppBuildConfig.project.releaseSigning(this, project.name)
            }
        }
    }

    defaultConfig {
        (this as? LibraryBuildType)?.consumerProguardFiles("proguard-rules.pro")
    }

    buildTypes {
        val testEnv = localProperties.getProperty("integrationTests.testEnvDoamin", "100.67.24.114")

        getByName("release") {
            buildConfigField("String", "BUILD_TYPE", "String.valueOf(\"release\")")
            buildConfigField("Boolean", "BENCHMARK", "Boolean.valueOf(false)")
            buildConfigField("String", "BENCHMARK_TEST_ENV", "\"\"")

            println("${project.name} -> minify = ${this@octoAppBuildTypes is ApplicationExtension}")
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            manifestPlaceholders["firebaseRemoteServicesEnabled"] = true
            extraReleaseConfig()

            (this as? ApplicationBuildType)?.let {
                signingConfig = signingConfigs.findByName("releaseSigning")
            }
        }

        register("benchmark") {
            initWith(getByName("release"))

            buildConfigField("String", "BUILD_TYPE", "String.valueOf(\"development\")")
            buildConfigField("Boolean", "BENCHMARK", "Boolean.valueOf(true)")
            buildConfigField("String", "BENCHMARK_TEST_ENV", "\"http://$testEnv:5002\"")

            matchingFallbacks += "release"
            manifestPlaceholders["firebaseRemoteServicesEnabled"] = false
        }

        getByName("debug") {
            buildConfigField("String", "BUILD_TYPE", "String.valueOf(\"debug\")")
            buildConfigField("Boolean", "BENCHMARK", "Boolean.valueOf(false)")
            buildConfigField("String", "BENCHMARK_TEST_ENV", "\"http://$testEnv:5002\"")
            manifestPlaceholders["firebaseRemoteServicesEnabled"] = false
        }
    }
}

private fun <BuildTypeT : BuildType> CommonExtension<*, BuildTypeT, *, *, *, *>.octoAppDesugaring(
    project: Project
) {
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
    }

    project.dependencies.run {
        val libs = project.extensions.getByType<VersionCatalogsExtension>().named("libs")
        add("coreLibraryDesugaring", libs.findLibrary("desugar").get())
    }
}

private fun Project.octoAppDagger() = dependencies.run {
    val libs = loadLibs()
    add("kapt", libs.findLibrary("dagger-compiler").get())
    add("implementation", libs.findLibrary("dagger").get())
}

private fun Project.loadLibs() =
    project.extensions.getByType<VersionCatalogsExtension>().named("libs")

//
//
//fun KotlinMultiplatformExtension.configureTargets() {
//    androidTarget {
//        compilations.all {
//            kotlinOptions {
//                jvmTarget = "17"
//            }
//        }
//    }
//    iosArm64()
//    iosSimulatorArm64()
//}
//
//fun KotlinMultiplatformExtension.optInToExpectActual() {
//    targets.configureEach {
//        compilations.configureEach {
//            compilerOptions.configure {
//                freeCompilerArgs.addAll(
//                    "-opt-in=kotlin.ExperimentalUnsignedTypes,kotlin.RequiresOptIn",
//                    "-Xexpect-actual-classes"
//                )
//            }
//        }
//    }
//}