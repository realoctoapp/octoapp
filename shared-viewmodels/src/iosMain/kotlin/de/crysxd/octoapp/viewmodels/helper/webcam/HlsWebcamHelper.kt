package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import io.ktor.http.HttpHeaders
import kotlinx.coroutines.flow.FlowCollector

internal actual class HlsWebcamHelper actual constructor(core: BaseViewModelCore) {

    private val nested = WebPageWebcamHelper(core)

    actual suspend fun emit(
        collector: FlowCollector<WebcamControlsViewModelCore.State>,
        settings: ResolvedWebcamSettings.HlsSettings,
        activeWebcam: Int,
        webcamCount: Int,
    ) = nested.emit(
        collector = collector,
        settings = ResolvedWebcamSettings.WebPageWebcamSettings(
            url = settings.url,
            webcam = settings.webcam,
            extraHeaders = settings.basicAuth?.let {
                mapOf(HttpHeaders.Authorization to it)
            } ?: emptyMap()
        ),
        activeWebcam = activeWebcam,
        webcamCount = webcamCount,
    )
}