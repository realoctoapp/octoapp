package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_HLS_WEBCAM
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebContent
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import io.github.aakira.napier.Napier
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow


@OptIn(ExperimentalCoroutinesApi::class)
internal actual class WebPageWebcamHelper actual constructor(core: BaseViewModelCore) {

    actual suspend fun emit(
        collector: FlowCollector<WebcamControlsViewModelCore.State>,
        settings: ResolvedWebcamSettings.WebPageWebcamSettings,
        activeWebcam: Int,
        webcamCount: Int,
    ) {
        val featureEnabledFlow = BillingManager.isFeatureEnabledFlow(FEATURE_HLS_WEBCAM)
        featureEnabledFlow.flatMapLatest { featureEnabled ->
            flow {
                val state = if (featureEnabled) {
                    Napier.i(tag = "WebPageWebcamHelper", message = "WebRTC streams enabled, constructing player")
                    WebcamControlsViewModelCore.State.WebpageReady(
                        webcamCount = webcamCount,
                        activeWebcam = activeWebcam,
                        content = WebContent(
                            html = WebPageWebcamHelperCore.constructHtml(settings),
                            baseUrl = URLBuilder(settings.url).apply {
                                pathSegments = emptyList()
                                protocol = when (protocol) {
                                    URLProtocol.WS -> URLProtocol.HTTP
                                    URLProtocol.WSS -> URLProtocol.HTTPS
                                    else -> protocol
                                }
                            }.buildString()
                        ),
                        flipH = settings.webcam.flipH,
                        flipV = settings.webcam.flipV,
                        rotate90 = settings.webcam.rotate90,
                        displayName = settings.webcam.displayName,
                        webcamUrl = settings.url.toString(),
                    )
                } else {
                    WebcamControlsViewModelCore.State.FeatureNotAvailable(
                        webcamCount = webcamCount,
                        activeWebcam = activeWebcam,
                        webcamUrl = settings.url.toString(),
                        featureName = "WebRTC",
                        displayName = settings.webcam.displayName,
                    )
                }

                emit(state)
            }
        }.let {
            collector.emitAll(it)
        }
    }
}