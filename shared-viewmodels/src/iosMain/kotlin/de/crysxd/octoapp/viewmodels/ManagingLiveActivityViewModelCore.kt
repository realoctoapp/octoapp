package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.awaitItem
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.models.settings.Settings
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Instant
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.seconds

class ManagingLiveActivityViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "[LA] LiveActivityViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val loadFilesUseCase = SharedBaseInjector.get().loadFilesUseCase()

    private val thumbnailUrl = octoPrintProvider.passiveCurrentMessageFlow(tag = "live-activity-thumbnail", instanceId = instanceId).map {
        it.job?.file.let { f -> f?.path to f?.date }
    }.distinctUntilChanged().map { (path, date) ->
        getThumbnailUrl(path) to createThumbnailKey(path, date)
    }.onStart {
        emit(null to null)
    }.shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay, replay = 1)

    private val hasCompanion = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map {
        it?.settings?.plugins?.octoAppCompanion != null
    }.distinctUntilChanged()


    val state = combine(
        octoPrintProvider.passiveCurrentMessageFlow(tag = "live-activity", instanceId = instanceId),
        printerConfigRepository.instanceInformationFlow(instanceId = instanceId)
    ) { current, instance ->
        current.createData(instance)
    }.combine(thumbnailUrl) { data, (thumbnail, thumbnailKey) ->
        // Copy thumbnail from async source, but only if it matches the path
        data.copy(thumbnailUrl = thumbnail?.takeIf { thumbnailKey == createThumbnailKey(data.filePath, data.fileDate) })
    }.combine(hasCompanion) { data, hasCompanion ->
        data to hasCompanion
    }.filter { (_, hasCompanion) ->
        hasCompanion
    }.map { (data, _) ->
        data
    }.distinctUntilChangedBy {
        it.copy(
            sourceTime = Instant.DISTANT_PAST,
            printTime = it.printTime?.seconds?.inWholeMinutes,
            timeLeft = it.printTime?.seconds?.inWholeMinutes,
        )
    }.rateLimit(3000)

    private fun Message.Current.createData(instance: PrinterConfigurationV3?) = PrintActivityData(
        fileName = job?.file?.display ?: "???",
        filePath = job?.file?.path ?: "???",
        fileDate = job?.file?.date,
        state = state.flags.createState(),
        progress = progress?.completion?.roundToInt() ?: 0,
        sourceTime = serverTime,
        timeLeft = progress?.printTimeLeft?.toLong(),
        thumbnailUrl = null,
        printTime = progress?.printTime?.toLong(),
        colors = instance.colors,
    )

    private suspend fun getThumbnailUrl(filePath: String?): String? = try {
        filePath?.let { path ->
            withTimeoutOrNull(30.seconds) {
                Napier.i(tag = tag, message = "Begin to resolve thumbnail for path $path")
                val fileObject = loadFilesUseCase.execute(
                    LoadFilesUseCase.Params(
                        instanceId = instanceId,
                        path = path,
                        fileOrigin = FileOrigin.Gcode,
                    )
                ).awaitItem()
                val thumbnail = (fileObject as? FileObject.File)?.mediumThumbnail?.path
                Napier.i(tag = tag, message = "Resolved thumbnail for path $path to be $thumbnail")
                thumbnail
            } ?: let {
                Napier.w(tag = tag, message = "Loading files timed out")
                null
            }
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to load files for thumbnail: ${e.message}", throwable = e)
        null
    }

    private fun createThumbnailKey(path: String?, date: Instant?) = if (path != null && date != null) "$path:${date.toEpochMilliseconds()}" else null

    private fun PrinterState.Flags.createState() = when {
        pausing -> State.Printing
        paused -> State.Paused
        cancelling -> State.Printing
        printing -> State.Printing
        resuming -> State.Printing
        else -> State.Idle
    }

    data class PrintActivityData(
        val fileName: String,
        val filePath: String,
        val fileDate: Instant?,
        val state: State,
        val progress: Int,
        val sourceTime: Instant,
        val timeLeft: Long?,
        val printTime: Long?,
        val thumbnailUrl: String?,
        val colors: Settings.Appearance.Colors,
    )

    enum class State {
        Printing,
        Paused,
        Idle,
    }
}