package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.SharedBaseInjector
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

@OptIn(ExperimentalCoroutinesApi::class)
class QuickAccessControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val pinnedMenuItemRepository = SharedBaseInjector.get().pinnedMenuItemRepository

    val state = flow {
        // In case OctoPrint is not connected the single initial emit makes sure we load the connect workspace
        // No current message -> null -> MenuId.ConnectWorkspace
        emit(printerProvider.getLastCurrentMessage(instanceId = instanceId))
        emitAll(printerProvider.passiveCurrentMessageFlow(tag = "quick-access", instanceId = instanceId))
    }.map { current ->
        when {
            current?.state?.flags?.isPrinting() == true -> MenuId.PrintWorkspace
            current?.state?.flags?.isOperational() == true -> MenuId.PrePrintWorkspace
            else -> MenuId.ConnectWorkspace
        }
    }.distinctUntilChanged().flatMapLatest { menuId ->
        pinnedMenuItemRepository.observePinnedMenuItems(menuId).map {
            menuId to it
        }
    }.map { (menuId, items) ->
        State(
            menuId = menuId,
            itemIds = items
        )
    }.distinctUntilChanged()

    data class State(
        val menuId: MenuId,
        val itemIds: Set<String>,
    )
}