package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.PerformPendingSettingsSave
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

class SaveConfigControlsViewModelCore(
    instanceId: String
) : BaseViewModelCore(
    instanceId = instanceId
) {

    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val performSave = SharedBaseInjector.get().performPendingSettingsSave()

    val state = printerProvider.passiveCurrentMessageFlow(tag = "save-config", instanceId = instanceId)
        .map { State(it.pendingSettingsSave) }
        .flowOn(Dispatchers.Default)
        .distinctUntilChanged()

    suspend fun saveConfig() {
        try {
            performSave.execute(PerformPendingSettingsSave.Params(instanceId))
        } catch (e: Exception) {
            reportException(e)
        }
    }

    data class State(
        val saveConfigPending: Boolean
    )
}