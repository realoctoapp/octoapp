package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.DeleteFileUseCase
import de.crysxd.octoapp.base.usecase.DownloadFileUseCase
import de.crysxd.octoapp.base.usecase.MoveFileUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.datetime.Instant

class FileActionsViewModelCore : BaseViewModelCore("") {

    private val mutableState = MutableStateFlow<State>(State.Idle)
    private val tag = "FileCopyAndMoveViewModelCore"
    private val moveUseCase = SharedBaseInjector.get().moveFileUseCase()
    private val deleteUseCase = SharedBaseInjector.get().deleteFileUseCase()
    private val downloadFileUseCase = SharedBaseInjector.get().downloadFileUseCase()
    val state = mutableState.asStateFlow()
    private var activeJob: Job? = null

    fun copy(instanceId: String, path: String, display: String) {
        mutableState.value = State.ReadyToPaste(
            path = path,
            instanceId = instanceId,
            mode = CopyMode.Copy,
            display = display
        )
    }

    fun cut(instanceId: String, path: String, display: String) {
        mutableState.value = State.ReadyToPaste(
            path = path,
            instanceId = instanceId,
            mode = CopyMode.Move,
            display = display,
        )
    }

    fun prepareRename(instanceId: String, path: String, name: String) {
        val suffix = FileObject.File(path = path).extension ?: ""
        mutableState.value = State.Renaming(
            originalName = name.removeSuffix(".$suffix"),
            suffix = suffix,
            path = path,
            fullOriginalName = name,
            instanceId = instanceId,
        )
    }

    fun completeRename(newName: String) {
        val state = state.value as? State.Renaming ?: return
        val newFullName = if (state.suffix.isBlank()) newName.trim() else "${newName.trim()}.${state.suffix}"
        val pathWithoutName = state.path.removeSuffix(state.fullOriginalName)
        doMove(
            fromPath = state.path,
            fromInstanceId = state.instanceId,
            toPath = "$pathWithoutName$newFullName",
            toInstanceId = state.instanceId,
            mode = CopyMode.Move,
        )
    }

    fun download(instanceId: String, display: String, path: String, date: Instant) {
        activeJob?.cancel()
        activeJob = AppScope.launch {
            try {
                mutableState.value = State.Loading()

                val localFilePath = downloadFileUseCase.execute(
                    param = DownloadFileUseCase.Params(
                        file = fileObjectFromPath(path).copy(date = date),
                        instanceId = instanceId,
                        progressUpdate = { progress ->
                            mutableState.value = State.Loading(display = progress.times(100).formatAsPercent(maxDecimals = 0), progress = progress)
                        }
                    )
                ).localFilePath

                mutableState.value = State.ReadyToShare(display = display, localFilePath = localFilePath)
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to move file", throwable = e)
                reportException(e)
                mutableState.value = State.Idle
            }
        }
    }

    fun delete(instanceId: String, path: String) {
        activeJob?.cancel()
        activeJob = AppScope.launch {
            try {
                mutableState.value = State.Loading()
                deleteUseCase.execute(
                    param = DeleteFileUseCase.Params(
                        file = fileObjectFromPath(path = path),
                        instanceId = instanceId,
                    )
                )
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to move file", throwable = e)
                reportException(e)
            } finally {
                mutableState.value = State.Idle
            }
        }
    }

    fun paste(instanceId: String, path: String) {
        val state = state.value as? State.ReadyToPaste ?: return
        doMove(
            fromPath = state.path,
            fromInstanceId = state.instanceId,
            toPath = path,
            toInstanceId = instanceId,
            mode = state.mode,
        )
    }

    fun cancel() {
        mutableState.value = State.Idle
        activeJob?.cancel()
    }

    private fun doMove(
        fromPath: String,
        fromInstanceId: String,
        toPath: String,
        toInstanceId: String,
        mode: CopyMode,
    ) {
        activeJob?.cancel()
        activeJob = AppScope.launch {
            try {
                mutableState.value = State.Loading()
                moveUseCase.execute(
                    param = MoveFileUseCase.Params(
                        file = fileObjectFromPath(path = fromPath),
                        destinationPath = toPath,
                        copyFile = mode == CopyMode.Copy,
                        destinationInstanceId = toInstanceId,
                        progressUpdate = { mutableState.value = State.Loading(display = it.times(100).formatAsPercent(maxDecimals = 0), progress = it) },
                        sourceInstanceId = fromInstanceId,
                        destinationOrigin = FileOrigin.Gcode,
                    )
                )
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to move file", throwable = e)
                reportException(e)
            } finally {
                mutableState.value = State.Idle
            }
        }
    }

    private fun fileObjectFromPath(path: String) = FileObject.File(
        path = path,
        origin = FileOrigin.Gcode,
        name = path.split("/").lastOrNull() ?: "???",
    )

    sealed class State {
        data object Idle : State()

        data class ReadyToShare(
            val display: String,
            val localFilePath: String,
        ) : State()

        data class ReadyToPaste(
            val display: String,
            internal val mode: CopyMode,
            val instanceId: String,
            internal val path: String,
        ) : State()

        data class Renaming(
            val originalName: String,
            internal val path: String,
            internal val fullOriginalName: String,
            internal val suffix: String,
            internal val instanceId: String,
        ) : State()

        data class Loading(
            val display: String = 0f.formatAsPercent(),
            val progress: Float = 0f,
        ) : State()
    }

    enum class CopyMode {
        Copy, Move
    }
}