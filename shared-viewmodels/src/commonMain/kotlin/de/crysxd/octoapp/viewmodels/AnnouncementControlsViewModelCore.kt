package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.get
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.sharedcommon.appVersionMajor
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.utils.NotificationUtils.canUseNotifications
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class AnnouncementControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    companion object {
        const val LinkNotificationSheet = "notification-sheet"
        const val LinkCompanionInstallSheet = "companion-install-sheet"
        const val LinkCompanionRunningMenu = "companion-running-menu"
    }

    private val preferences = SharedBaseInjector.get().preferences
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val platform = SharedCommonInjector.get().platform
    private val updateSourceTrigger = MutableStateFlow(0)
    private val updateSource = updateSourceTrigger.flatMapLatest {
        flow {
            while (currentCoroutineContext().isActive) {
                emit(Unit)
                canNotify.value = canUseNotifications()
                delay(5.seconds)
            }
        }
    }
    private val canNotify = MutableStateFlow<Boolean?>(null)
    private val announceCompanionNotInstalled = combine(
        printerConfigRepository.instanceInformationFlow(instanceId).map { it?.hasCompanionPlugin() },
        canNotify.filterNotNull(),
        preferences.updatedFlow2.map { Clock.System.now() > it.askCompanionInstallNotBefore },
    ) { hasCompanion, canNotify, canAsk ->
        if (hasCompanion == false && canNotify && canAsk) {
            Announcement(
                id = "companion-install",
                text = getString("announcement___companion_plugin"),
                canHide = false,
                learnMoreLink = LinkCompanionInstallSheet,
                learnMoreText = getString("learn_more"),
            )
        } else {
            null
        }
    }

    private val announceCompanionNotRunning = combine(
        announceCompanionNotInstalled,
        canNotify.filterNotNull(),
        printerConfigRepository.instanceInformationFlow(instanceId).map { it?.hasCompanionPlugin() == true },
        printerConfigRepository.instanceInformationFlow(instanceId).map { it?.settings?.plugins?.octoAppCompanion?.running == false },
    ) { companionNotInstalled, canNotify, hasCompanion, companionNotRunning ->
        if (companionNotInstalled == null && canNotify && hasCompanion && companionNotRunning) {
            Announcement(
                id = "companion-running",
                text = getString("moonraker_companion___installed_but_not_running"),
                canHide = false,
                learnMoreLink = LinkCompanionRunningMenu,
                learnMoreText = getString("learn_more"),
            )
        } else {
            null
        }
    }

    private val announceNotificationPermission = combine(
        canNotify.filterNotNull(),
        preferences.updatedFlow2.map { Clock.System.now() > it.askNotificationPermissionNotBefore },
    ) { canNotify, canAsk ->
        if (!canNotify && canAsk) {
            Announcement(
                id = "notification-permission",
                text = getString("notifications_permission___announcement"),
                canHide = false,
                learnMoreLink = LinkNotificationSheet,
                learnMoreText = getString("learn_more"),
            )
        } else {
            null
        }
    }

    private val announcementSale = updateSource.map {
        OctoConfig.get(OctoConfigField.PurchaseOffers)
    }.distinctUntilChanged().combine(BillingManager.shouldAdvertisePremiumFlow()) { ad, shouldAdvertise ->
        ad.activeConfig.advertisementWithData?.takeIf { shouldAdvertise }?.let {
            Announcement(
                id = it.id,
                text = it.message,
                canHide = true,
                redColor = true,
                learnMoreLink = UriLibrary.getPurchaseUri().toString(),
                learnMoreText = getString("learn_more"),
            )
        }
    }

    private val whatsNew = flowOf(
        Announcement(
            id = "announcement_${platform.appVersionMajor}",
            text = getString("announcement___new_version_title", platform.appVersionMajor),
            canHide = true,
            learnMoreLink = UriLibrary.getWhatsNewUri(platform.appVersionMajor).toString(),
            learnMoreText = getString("announcement___new_version_learn_more"),
            confettiBackground = true,
        )
    )

    private val moonrakerMigration = printerConfigRepository.instanceInformationFlow(instanceId).map { instance ->
        Announcement(
            id = "moonraker_detection_change",
            text = "⚠\uFE0F Change in behavior\n\n You (used to) have Fluidd and Mainsail installed. OctoApp used to combine webcam, temperature preset and appearance setting from both.\n\nSince 2.1, OctoApp only uses settings from ${instance?.systemInfo?.interfaceType.label} because the URL you configured in OctoApp points to this interface.",
            canHide = true,
            learnMoreLink = null,
            learnMoreText = null,
            confettiBackground = false,
        ).takeIf {
            instance?.affectedByMoonrakerDetectionChange == true
        }
    }

    private val remote = flow {
        suspend fun doEmit() {
            OctoConfig.get(OctoConfigField.Announcement)
                .takeIf { !it.text.isNullOrBlank() && !it.id.isNullOrBlank() }
                ?.let { model ->
                    Announcement(
                        id = "remote_${model.id}",
                        text = model.text ?: "",
                        learnMoreText = model.learnMoreText ?: getString("learn_more").takeIf { model.learnMoreLink != null },
                        learnMoreLink = model.learnMoreLink,
                        canHide = model.canHide,
                        confettiBackground = model.confettiBackground,
                        redColor = model.redColor,
                    )
                }
                ?.let {
                    emit(it)
                } ?: emit(null)
        }

        doEmit()

        // Emit again after 5s, remote config might have changed after app start
        delay(5.seconds)
        doEmit()
    }.catch {
        Napier.e(tag = "AnnouncementControlsViewModelCore", message = "Failed to load remote", throwable = it)
        emit(null)
    }

    private val controlCenter = combine(
        BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH),
        printerConfigRepository.allInstanceInformationFlow().map { it.size > 1 }
    ) { hasFeature, hasMultipleInstance ->
        Announcement(
            id = "announcement_quick_switch",
            text = getString("control_center___announcement"),
            learnMoreText = null,
            learnMoreLink = null,
            canHide = true
        ).takeIf {
            hasFeature && hasMultipleInstance && platform.isAndroid()
        }
    }

    private val remoteServiceDisconnected = printerConfigRepository.instanceInformationFlow(instanceId).map { instance ->
        instance?.remoteConnectionFailure?.takeIf {
            Instant.fromEpochMilliseconds(it.dateMillis) > Instant.fromEpochSeconds(1718973479)
        }?.let {
            Announcement(
                id = "remote_service_disconnect_${it.dateMillis}",
                text = it.message ?: getString("configure_remote_access___disconnect_announcement", it.remoteServiceName),
                learnMoreText = getString("show_details"),
                learnMoreLink = UriLibrary.getConfigureRemoteAccessUri().toString(),
                canHide = true
            )
        }
    }

    val state = combine(
        listOf(
            announcementSale,
            whatsNew,
            announceCompanionNotInstalled,
            announceCompanionNotRunning,
            announceNotificationPermission,
            controlCenter,
            moonrakerMigration,
            remote,
        )
    ) { announcements ->
        State(announcements.filterNotNull())
    }.distinctUntilChanged()

    fun checkNotificationNow() {
        updateSourceTrigger.update { it + 1 }
    }

    data class State(
        val announcements: List<Announcement> = emptyList()
    )

    data class Announcement(
        val id: String,
        val text: String,
        val learnMoreText: String?,
        val learnMoreLink: String?,
        val canHide: Boolean,
        val redColor: Boolean = false,
        val confettiBackground: Boolean = false,
    )
}