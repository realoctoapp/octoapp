package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.usecase.ChangeZOffsetUseCase
import de.crysxd.octoapp.base.usecase.GetTuneStateUseCase
import de.crysxd.octoapp.base.usecase.ResetZOffsetUseCase
import de.crysxd.octoapp.base.usecase.SaveZOffsetToConfigUseCase
import de.crysxd.octoapp.base.usecase.TunePrintUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.settings.Settings
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update

class TuneControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    companion object {
        private const val tag = "TuneControlsViewModelCore"
    }

    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val changeZOffsetUseCase = SharedBaseInjector.get().changeZOffsetUseCase()
    private val resetZOffsetUseCase = SharedBaseInjector.get().resetZOffsetUseCase()
    private val saveZOffsetToConfigUseCase = SharedBaseInjector.get().saveZOffsetToConfigUseCase()
    private val tunePrintUseCase = SharedBaseInjector.get().tunePrintUseCase()
    private val getTuneStateUseCase = SharedBaseInjector.get().getTuneStateUseCase()
    private val activePoll = MutableStateFlow(false)

    @OptIn(ExperimentalCoroutinesApi::class)
    val state = activePoll.flatMapLatest { activePoll ->
        getTuneStateUseCase.execute(
            param = GetTuneStateUseCase.Params(
                instanceId = instanceId,
                activePoll = activePoll
            )
        )
    }.shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay)

    val babyStepIntervals = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map {
        Settings(
            babyStepIntervals = it?.settings?.babyStepIntervals ?: Settings().babyStepIntervals
        )
    }.distinctUntilChanged()

    suspend fun changeOffset(
        changeMm: Float
    ) = try {
        changeZOffsetUseCase.execute(
            ChangeZOffsetUseCase.Param(
                instanceId = instanceId,
                changeMm = changeMm
            )
        )
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Encountered exception while changing Z offset", throwable = e)
    }

    suspend fun resetOffset() = try {
        resetZOffsetUseCase.execute(
            ResetZOffsetUseCase.Param(
                instanceId = instanceId,
            )
        )
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Encountered exception while resetting Z offset", throwable = e)
    }

    suspend fun saveOffsetToConfig() = try {
        saveZOffsetToConfigUseCase.execute(
            SaveZOffsetToConfigUseCase.Param(
                instanceId = instanceId,
            )
        )
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Encountered exception while resetting Z offset", throwable = e)
    }

    suspend fun applyChanges(
        feedRate: Float?,
        flowRate: Float?,
        fanSpeeds: Map<String, Float?>,
    ) = try {
        tunePrintUseCase.execute(
            TunePrintUseCase.Param(
                feedRate = feedRate?.toInt(),
                flowRate = flowRate?.toInt(),
                fanSpeeds = fanSpeeds,
                instanceId = instanceId,
            )
        )
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Encountered exception while applying value", throwable = e)
    }

    fun setActivePoll(active: Boolean) {
        activePoll.update { active }
    }

    data class Settings(
        val babyStepIntervals: List<Float>
    )
}