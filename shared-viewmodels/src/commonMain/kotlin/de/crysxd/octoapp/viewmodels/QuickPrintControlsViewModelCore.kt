package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetPrintHistoryUseCase
import de.crysxd.octoapp.base.usecase.LoadFileReferencesUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.base.utils.combine
import de.crysxd.octoapp.base.utils.mapData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.models.files.flatten
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.datetime.Instant

@OptIn(ExperimentalCoroutinesApi::class)
class QuickPrintControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "QuickPrintControlsViewModelCore"
    private val startPrintHelper = StartPrintHelper(this)
    private val loadFileReferencesUseCase = SharedBaseInjector.get().loadFileReferencesUseCase()
    private val loadFilesUseCase = SharedBaseInjector.get().loadFilesUseCase()
    private val getPrintHistoryUseCase = SharedBaseInjector.get().getPrintHistoryUseCase()
    private val octoPrintPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private var lastFiles = emptyMap<String, FileObject>()
    private val loadTrigger = MutableStateFlow(0)
    private val maxItems = 5
    private val reloadEventClasses = listOf(
        Message.Event.UpdatedFiles::class,
        Message.Event.PrintDone::class,
        Message.Event.PrintCancelled::class,
        Message.Event.PrintFailed::class,
    )

    private val lastPrintsFlow = loadTrigger.flatMapLatest {
        getPrintHistoryUseCase.execute(
            GetPrintHistoryUseCase.Params(
                instanceId = instanceId,
                skipCache = false,
            )
        )
    }.mapData { items ->
        items.distinctBy { item ->
            item.path
        }.map { item ->
            CandidateFile(
                path = item.path,
                fileOrigin = item.fileOrigin,
                selected = false,
                printDate = item.time,
                printSuccess = item.success,
            )
        }
    }

    private val lastUploadsFlow = flow {
        val nestedFlow = loadFileReferencesUseCase.execute(
            LoadFileReferencesUseCase.Params(
                fileOrigin = FileOrigin.Gcode,
                instanceId = instanceId,
                skipCache = false
            )
        ).mapData { folder ->
            folder.flatten().maxByOrNull { it.date }
        }
        emitAll(nestedFlow)
    }.mapToFlatLinks()

    private val selectedFilesFlow = octoPrintPrintProvider.passiveCurrentMessageFlow(tag = "quick-print", instanceId = instanceId).map {
        it.job?.file
    }.distinctUntilChanged().flatMapLatest { jobFile ->
        jobFile ?: return@flatMapLatest flowOf(FlowState.Ready(null))

        loadFilesUseCase.execute(
            LoadFilesUseCase.Params(
                fileOrigin = jobFile.origin,
                instanceId = instanceId,
                path = jobFile.path,
                skipCache = false
            )
        )
    }.mapToFlatLinks(selected = true)

    private fun Flow<FlowState<out FileReference?>>.mapToFlatLinks(selected: Boolean = false) = mapData { folder ->
        folder?.flatten()?.map { item ->
            CandidateFile(
                path = item.path,
                fileOrigin = FileOrigin.Gcode,
                selected = selected,
                date = item.date,
            )
        } ?: emptyList()
    }

    val state = kotlinx.coroutines.flow.combine(
        lastPrintsFlow,
        lastUploadsFlow,
        selectedFilesFlow
    ) { states ->
        if (states.any { it is FlowState.Loading }) return@combine flowOf<FlowState<State>>(FlowState.Loading())
        states.firstNotNullOfOrNull { it as? FlowState.Error }?.throwable?.let { return@combine flowOf<FlowState<State>>(FlowState.Error(it)) }

        val flatStates = states.map { (it as FlowState.Ready).data }.flatten().groupBy { it.path }.map { (_, pointers) -> pointers.flatten() }
        val selectedFile = listOfNotNull(flatStates.firstOrNull { it.selected })
        val lastUploaded = listOfNotNull(flatStates.filter { !it.isHidden }.filter { it.date != null }.maxByOrNull { it.date ?: Instant.DISTANT_PAST })
        val lastPrints = flatStates.filter { it.printDate != null }.filter { !it.isHidden }.sortedByDescending { it.printDate ?: Instant.DISTANT_PAST }
        val chosen = (selectedFile + lastUploaded + lastPrints).distinctBy { it.path }.take(maxItems)
        val nestedFlows = chosen.map { candidate ->
            loadFilesUseCase.execute(
                LoadFilesUseCase.Params(
                    fileOrigin = candidate.fileOrigin,
                    path = candidate.path,
                    instanceId = instanceId
                )
            ).map {
                when (it) {
                    is FlowState.Loading -> it
                    is FlowState.Ready -> it
                    is FlowState.Error -> {
                        Napier.e(tag = tag, message = "Failed to load a file (1)", throwable = it.throwable)
                        FlowState.Ready(null)
                    }
                }
            }.catch {
                Napier.e(tag = tag, message = "Failed to load a file (2)", throwable = it)
                emit(FlowState.Ready(null))
            }
        }

        combine(*nestedFlows.toTypedArray()) { files ->
            lastFiles = files.filterNotNull().associateBy { it.path }

            files.filterNotNull().map { file ->
                FileLink(
                    fileObject = (file as FileObject.File).copy(
                        prints = file.prints
                        // For Moonraker we need to backfill this information as the file repo doesn't give us the history
                            ?: lastPrints.firstOrNull { it.path == file.path }?.let { f ->
                                FileObject.PrintHistory(
                                    last = FileObject.PrintHistory.LastPrint(
                                        date = f.printDate ?: return@let null,
                                        success = f.printSuccess ?: return@let null,
                                    )
                                )
                            }
                    ),
                    selected = file.path == selectedFile.firstOrNull()?.path
                )
            }.let { State(it) }
        }.onCompletion {
            Napier.e(tag = tag, message = "Completed", throwable = it)
        }
    }.flatMapLatest {
        it
    }.catch {
        Napier.e(tag = tag, message = "Exception QuickPrint controls", throwable = it)
        emit(FlowState.Error(it))
    }.flowOn(Dispatchers.Default)

    fun reload() {
        Napier.d(tag = tag, message = "Reload triggered")
        loadTrigger.value += 1
    }

    suspend fun startPrint(path: String, materialConfirmed: Boolean, timelapseConfirmed: Boolean) = startPrintHelper.startPrint(
        file = requireNotNull(lastFiles[path] as? FileObject.File),
        materialConfirmed = materialConfirmed,
        timelapseConfirmed = timelapseConfirmed
    )

    private fun List<CandidateFile>.flatten(): CandidateFile {
        require(!isEmpty()) { "Candidate file list is empty" }
        val lastPrint = maxByOrNull { it.printDate ?: Instant.DISTANT_PAST }?.takeIf { (it.printDate ?: Instant.DISTANT_PAST) > Instant.DISTANT_PAST }
        return CandidateFile(
            path = first().path,
            fileOrigin = first().fileOrigin,
            selected = any { it.selected },
            date = mapNotNull { it.date }.maxOrNull(),
            printDate = lastPrint?.printDate,
            printSuccess = lastPrint?.printSuccess,
        )
    }

    private data class CandidateFile(
        val path: String,
        val fileOrigin: FileOrigin,
        val selected: Boolean,
        val date: Instant? = null,
        val printDate: Instant? = null,
        val printSuccess: Boolean? = null,
    ) {
        val isHidden = path.split("/").last().startsWith(".")
    }

    data class FileLink(
        val fileObject: FileObject.File,
        val selected: Boolean,
    )

    data class State(
        val files: List<FileLink>
    )
}