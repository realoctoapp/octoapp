package de.crysxd.octoapp.viewmodels.helper.bedmesh

import de.crysxd.octoapp.sharedcommon.utils.HexColor

data class ParsedBedMesh(
    val sectors: List<Sector>,
    val legend: List<Pair<Float, HexColor>>
) {
    data class Sector(
        val left: Float,
        val top: Float,
        val right: Float,
        val bottom: Float,
        val color: HexColor,
    )
}