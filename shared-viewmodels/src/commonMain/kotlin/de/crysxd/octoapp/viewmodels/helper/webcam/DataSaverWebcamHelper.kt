package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.exceptions.WebcamDisabledException
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.sharedexternalapis.mjpeg.heightPx
import de.crysxd.octoapp.sharedexternalapis.mjpeg.widthPx
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.map
import kotlinx.datetime.Clock

internal class DataSaverWebcamHelper(private val core: BaseViewModelCore) {

    private val getWebcamSnapshotUseCase get() = SharedBaseInjector.get().getWebcamSnapshotUseCase()

    suspend fun emit(
        collector: FlowCollector<State>,
        settings: ResolvedWebcamSettings,
        dataSaverInterval: Long,
        activeWebcam: Int,
        webcamCount: Int,
    ) {
        var mediator: LowOverheadMediator<State.MjpegReady.Frame>? = null
        val frameInterval = dataSaverInterval + 250 // 250ms grace period

        val innerFlow = getWebcamSnapshotUseCase.execute(
            param = GetWebcamSnapshotUseCase.Params(
                webcamIndex = activeWebcam,
                interval = dataSaverInterval,
                illuminateIfPossible = true,
                instanceId = core.instanceId,
                maxSize = 720,
            )
        ).map<GetWebcamSnapshotUseCase.Snapshot, State> { snapshot ->
            val frame = State.MjpegReady.Frame(
                image = snapshot.bitmap,
                fps = 1000 / dataSaverInterval.toFloat(),
                frameTime = Clock.System.now().toEpochMilliseconds(),
                frameInterval = frameInterval,
            )

            val m = mediator?.also { it.publishData(frame) } ?: LowOverheadMediator(frame)
            mediator = m

            // The snapshot comes pre-transformed to us, do flip and rotate are always false
            State.MjpegReady(
                webcamCount = webcamCount,
                activeWebcam = activeWebcam,
                frames = m,
                flipH = false,
                flipV = false,
                rotate90 = false,
                frameInterval = frameInterval,
                frameWidth = snapshot.bitmap.widthPx,
                frameHeight = snapshot.bitmap.heightPx,
                displayName = settings.webcam.displayName,
            )
        }.catch {
            if (it is GetWebcamSnapshotUseCase.WebcamNotSupportedException) {
                emit(
                    State.Error(
                        webcamCount = webcamCount,
                        activeWebcam = activeWebcam,
                        exception = WebcamDisabledException(it.userMessage),
                        displayName = settings.webcam.displayName,
                        canRetry = false,
                    )
                )
            } else {
                throw it
            }
        }.distinctUntilChangedBy { (it as? State.MjpegReady)?.frames }

        collector.emitAll(innerFlow)
    }
}