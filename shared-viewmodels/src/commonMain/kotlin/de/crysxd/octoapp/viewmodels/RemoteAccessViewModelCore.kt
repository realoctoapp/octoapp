package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.HandleObicoAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.Manual
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.Ngrok
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.Obico
import de.crysxd.octoapp.viewmodels.RemoteAccessViewModelCore.Service.OctoEverywhere
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

class RemoteAccessViewModelCore(instanceId: String) {

    private val tag = "RemoteAccessViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    private val loadingServices = combine(
        listOf(
            HandleOctoEverywhereAppPortalSuccessUseCase.loadingFlow.map { OctoEverywhere to it },
            HandleObicoAppPortalSuccessUseCase.loadingFlow.map { Obico to it },
        )
    ) { services ->
        Napier.i(tag = tag, message = "Loading state: $services")
        services.mapNotNull { (service, loading) -> if (loading) service else null }
    }.distinctUntilChanged()

    val state = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).combine(loadingServices) { config, loadingServices ->
        val serivces: MutableMap<Service, Int> = mutableMapOf(
            OctoEverywhere to 2_000,
            Obico to 2_000,
            Manual to 0,
        )

        // Only show Ngrok if supported with auto configuration (OctoPrint)
        if (config?.systemInfo?.capabilities?.contains(SystemInfo.Capability.AutoNgrok) == true) {
            serivces += Ngrok to 500
        }

        val installedServices = listOfNotNull(
            Ngrok.takeIf { config?.hasPlugin(OctoPlugins.Ngrok) == true },
            OctoEverywhere.takeIf { config?.hasPlugin(OctoPlugins.OctoEverywhere) == true },
            Obico.takeIf { config?.hasPlugin(OctoPlugins.Obico) == true },
        )
        val connectedServices = listOfNotNull(
            Ngrok.takeIf { config?.alternativeWebUrl?.isNgrokUrl() == true },
            OctoEverywhere.takeIf { config?.alternativeWebUrl?.isOctoEverywhereUrl() == true },
            Obico.takeIf { config?.alternativeWebUrl?.isObicoUrl() == true },
        )

        // Installed service has medium importance
        if (Ngrok in installedServices) serivces[Ngrok] = 5_000
        if (OctoEverywhere in installedServices) serivces[OctoEverywhere] = 10_000
        if (Obico in installedServices) serivces[Obico] = 10_000

        // Connected service has highest importance
        if (Ngrok in connectedServices) serivces[Ngrok] = 100_000
        if (OctoEverywhere in connectedServices) serivces[OctoEverywhere] = 100_000
        if (Obico in connectedServices) serivces[Obico] = 100_000

        val sortedServices = serivces.toList().map { (service, priority) ->
            service to priority + service.bias
        }.sortedByDescending { (_, priority) ->
            priority
        }.also { services ->
            Napier.i(tag = tag, message = "Remote services: $services")
        }.map { (service, _) ->
            service
        }

        State(
            services = sortedServices,
            loadingServices = loadingServices,
            installedServices = installedServices,
            connectedServices = connectedServices,
        )
    }.catch {
        Napier.e(tag = tag, message = "Failed to create services", throwable = it)
        emit(
            State(
                services = Service.entries,
                loadingServices = emptyList(),
                connectedServices = emptyList(),
                installedServices = emptyList(),
            )
        )
    }.distinctUntilChanged()

    data class State(
        val services: List<Service>,
        val loadingServices: List<Service>,
        val installedServices: List<Service>,
        val connectedServices: List<Service>,
    )

    enum class Service {
        OctoEverywhere, Obico, Ngrok, Manual;

        val bias get() = biases[this]!!

        companion object {
            private val biases = entries.associateWith { (0..1_000).random() }
        }
    }
}