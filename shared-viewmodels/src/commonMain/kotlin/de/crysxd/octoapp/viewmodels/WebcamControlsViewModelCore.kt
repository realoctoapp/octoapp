package de.crysxd.octoapp.viewmodels

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings.HlsSettings
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings.MjpegSettings
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings.ObicoSettings
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings.RtspSettings
import de.crysxd.octoapp.base.data.models.ScaleType
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.exceptions.WebcamDisabledException
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.ConnectivityHelper
import de.crysxd.octoapp.base.utils.ConnectivityHelper.ConnectivityType.Metered
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State.Warning.Companion.SwitchWebcam
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State.Warning.Companion.SwitchWebcamFeatureDisabled
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import de.crysxd.octoapp.viewmodels.helper.webcam.DataSaverWebcamHelper
import de.crysxd.octoapp.viewmodels.helper.webcam.HlsWebcamHelper
import de.crysxd.octoapp.viewmodels.helper.webcam.MjpegWebcamHelper
import de.crysxd.octoapp.viewmodels.helper.webcam.ObicoWebcamHelper
import de.crysxd.octoapp.viewmodels.helper.webcam.RtspWebcamHelper
import de.crysxd.octoapp.viewmodels.helper.webcam.WebPageWebcamHelper
import de.crysxd.octoapp.viewmodels.helper.webcam.WebcamSettingsHelper
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class WebcamControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    companion object {
        var instanceCounter = 0
    }

    private val tag = "WebcamControlsViewModel@${instanceCounter++}"
    private val autoLightTag = "WebcamControlsViewModelCore/$instanceId/${uuid4()}"
    private val getWebcamSettingsUseCase get() = SharedBaseInjector.get().getWebcamSettingsUseCase()
    private val configRepository = SharedBaseInjector.get().printerConfigRepository
    private val preferences = SharedBaseInjector.get().preferences
    private val handleAutomaticLightEventUseCase = SharedBaseInjector.get().handleAutomaticLightEventUseCase()

    private val webcamSettingsHelper = WebcamSettingsHelper(core = this)
    private val connectivityHelper = ConnectivityHelper()
    private val mjpegHelper = MjpegWebcamHelper(core = this)
    private val dataSaverHelper = DataSaverWebcamHelper(core = this)
    private val hlsHelper = HlsWebcamHelper(core = this)
    private val rtspHelper = RtspWebcamHelper(core = this)
    private val obicoWebcamHelper = ObicoWebcamHelper(core = this)
    private val webRtcWebcamHelper = WebPageWebcamHelper(core = this)

    private val retrySource = MutableStateFlow(0)
    private var webcamCount: Int = 1
    private var activeWebcam: Int = 1

    private val instance = retrySource.flatMapLatest { _ ->
        configRepository.instanceInformationFlow(instanceId)
    }.distinctUntilChanged()

    private val webcamSettings = retrySource.flatMapLatest { _ ->
        instance.flatMapLatest { instance ->
            when {
                instance == null -> {
                    Napier.w(tag = tag, message = "No instance, skipping settings")
                    flowOf(null)
                }

                instance.settings?.webcam?.webcamEnabled != true -> {
                    Napier.w(tag = tag, message = "Webcam disabled, skipping settings")
                    flowOf(null)
                }

                else -> {
                    Napier.d(tag = tag, message = "Computing new webcam settings")
                    getWebcamSettingsUseCase.execute(instance)
                }
            }
        }
    }.distinctUntilChanged().onEach {
        Napier.d(tag = tag, message = "Received ${it?.size} webcams")
    }

    private val preferencesFlow = preferences.updatedFlow2.onEach {
        Napier.d(tag = tag, message = "Received new preferences")
    }

    private val dataSaverInterval = retrySource.flatMapLatest {
        connectivityHelper.connectivityState.onEach {
            Napier.d(tag = tag, message = "Received connectivity state: $it")
        }.combine(preferencesFlow) { connectivity, preferences ->
            when {
                connectivity == Metered || !preferences.dataSaverUseOnMobileNetworkOnly -> {
                    val interval = preferences.dataSaverWebcamInterval
                    Napier.i(tag = tag, message = "Connection is metered or data saver is always enabled, using data saver interval: $interval")
                    interval
                }

                else -> {
                    Napier.i(tag = tag, message = "Connection is not metered")
                    -1
                }
            }
        }
    }.distinctUntilChanged()

    private val activeWebcamIndex = instance.map {
        it?.appSettings?.activeWebcamIndex ?: 0
    }.distinctUntilChanged()

    private val systemInfo = instance.map {
        it?.systemInfo
    }.distinctUntilChangedBy {
        // The generation time might change, but we don't care about that of course if the rest is the same
        it?.copy(generatedAt = Instant.DISTANT_PAST, printerFirmware = null)
    }

    val configAspectRatio = webcamSettingsHelper.configAspectRatio.map {
        AspectRatio(it)
    }.distinctUntilChanged()

    val state = retrySource.flatMapLatest {
        flow {
            emit(State.Loading(webcamCount = webcamCount, activeWebcam = activeWebcam))
            emitAll(webcamFlowWrapper().flatMapLatest { it })
        }.retry(1) { e ->
            Napier.w(tag = tag, message = "Caught error in webcam, retrying unless disabled")
            e !is WebcamDisabledException
        }.catch { e ->
            Napier.e(tag = tag, message = "Caught error in webcam", throwable = e)
            emit(State.Error(webcamCount = webcamCount, exception = e, activeWebcam = activeWebcam))
        }
    }.onStart {
        Napier.i(tag = tag, message = "Starting")
        emit(State.Loading(webcamCount = webcamCount, activeWebcam = activeWebcam))
    }.onCompletion {
        Napier.i(tag = tag, message = "Done")
    }.withAutomaticLights()

    private fun webcamFlowWrapper() =
        combine(webcamSettings, dataSaverInterval, activeWebcamIndex, systemInfo) { webcamSettings, dataSaverInterval, activeWebcamIndex, systemInfo ->
            if (webcamSettings.isNullOrEmpty()) {
                Napier.i(tag = tag, message = "No webcam settings returned ($systemInfo)")
                flowOf(State.Hidden)
            } else {
                val limitedWebcamIndex = if (activeWebcamIndex in webcamSettings.indices) {
                    activeWebcamIndex
                } else {
                    Napier.i(tag = tag, message = "Webcam #$activeWebcamIndex is active but only ${webcamSettings.size} available -> limiting webcam index")
                    activeWebcamIndex.coerceIn(webcamSettings.indices)
                }
                Napier.i(tag = tag, message = "Creating webcam flow for webcam #$limitedWebcamIndex: ${webcamSettings[limitedWebcamIndex].description}")
                webcamCount = webcamSettings.size
                activeWebcam = limitedWebcamIndex

                webcamFlow(
                    activeSettings = webcamSettings.getOrNull(activeWebcam) ?: webcamSettings.firstOrNull(),
                    dataSaverInterval = dataSaverInterval,
                    systemInfo = systemInfo,
                )
            }
        }

    private fun <T> Flow<T>.withAutomaticLights(): Flow<T> = onStart {
        try {
            handleAutomaticLightEventUseCase.execute(HandleAutomaticLightEventUseCase.Event.WebcamVisible(autoLightTag))
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to turn light on", throwable = e)
        }
    }.onCompletion {
        try {
            // Spin off to main thread, onCompletion doesn't allow async operations to complete as the current scope
            // is about to be terminated
            AppScope.launch {
                handleAutomaticLightEventUseCase.execute(HandleAutomaticLightEventUseCase.Event.WebcamGone(autoLightTag))
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to turn light off", throwable = e)
        }
    }

    fun retry() {
        Napier.i(tag = tag, message = "Starting retry attempt")
        retrySource.update { it + 1 }
    }

    fun nextWebcam() = webcamSettingsHelper.nextWebcam(webcamCount)

    fun getInitialAspectRatio() = webcamSettingsHelper.getInitialAspectRatio()

    fun storeAspectRatio(aspectRatio: Float) = webcamSettingsHelper.storeAspectRatio(ratio = "$aspectRatio:1")

    fun getScaleType(isFullscreen: Boolean, default: ScaleType) = webcamSettingsHelper.getScaleType(isFullscreen, default)

    fun storeScaleType(isFullscreen: Boolean, scaleType: ScaleType) = webcamSettingsHelper.storeScaleType(scaleType, isFullscreen)

    private fun webcamFlow(
        activeSettings: ResolvedWebcamSettings?,
        dataSaverInterval: Long,
        systemInfo: SystemInfo?
    ) = keepAliveOnCompletionFlow(activeSettings?.webcam?.displayName) {
        emit(
            State.Loading(
                activeWebcam = activeWebcam,
                webcamCount = webcamCount,
                displayName = activeSettings?.webcam?.displayName
            )
        )

        when {
            activeSettings == null -> {
                Napier.d(tag = tag, message = "No webcam settings")
                emit(
                    State.Error(
                        webcamCount = webcamCount,
                        canRetry = false,
                        activeWebcam = activeWebcam,
                        exception = WebcamDisabledException(systemInfo = systemInfo),
                        displayName = null,
                    )
                )
            }

            // Obico webcam is not compatible with the data saver because the snapshot
            // url doesn't work. The Obico webcam is very similar to data saver, so we
            // can just ignore.
            dataSaverInterval > 0 && activeSettings !is ObicoSettings -> {
                Napier.d(tag = tag, message = "Using DataSaverWebcamHelper")
                dataSaverHelper.emit(
                    collector = this,
                    settings = activeSettings,
                    activeWebcam = activeWebcam,
                    dataSaverInterval = dataSaverInterval,
                    webcamCount = webcamCount,
                )
            }

            else -> when (activeSettings) {
                is MjpegSettings -> {
                    Napier.d(tag = tag, message = "Using MjpegWebcamHelper")
                    mjpegHelper.emit(
                        collector = this,
                        settings = activeSettings,
                        activeWebcam = activeWebcam,
                        webcamCount = webcamCount,
                    )
                }

                is HlsSettings -> {
                    Napier.d(tag = tag, message = "Using HlsWebcamHelper")
                    hlsHelper.emit(
                        collector = this,
                        settings = activeSettings,
                        activeWebcam = activeWebcam,
                        webcamCount = webcamCount
                    )
                }

                is RtspSettings -> {
                    Napier.d(tag = tag, message = "Using RtspWebcamHelper")
                    rtspHelper.emit(
                        collector = this,
                        settings = activeSettings,
                        activeWebcam = activeWebcam,
                        webcamCount = webcamCount
                    )
                }

                is ObicoSettings -> {
                    Napier.d(tag = tag, message = "Using ObicoWebcamHelper")
                    obicoWebcamHelper.emit(
                        collector = this,
                        settings = activeSettings,
                        activeWebcam = activeWebcam,
                        webcamCount = webcamCount,
                    )
                }

                is ResolvedWebcamSettings.WebPageWebcamSettings -> {
                    Napier.d(tag = tag, message = "Using WebRtcHelper")
                    webRtcWebcamHelper.emit(
                        collector = this,
                        settings = activeSettings,
                        activeWebcam = activeWebcam,
                        webcamCount = webcamCount,
                    )
                }

                is ResolvedWebcamSettings.Unsupported -> emit(
                    State.Error(
                        webcamCount = webcamCount,
                        canRetry = false,
                        canShowDetails = false,
                        activeWebcam = activeWebcam,
                        exception = WebcamDisabledException("${activeSettings.displayName} webcams are not yet supported"),
                        description = "Until then, learn [here](${UriLibrary.getFaqUri("webrtc-mjpeg-setup")}) how to add a second MJPEG webcam for use with OctoApp!",
                        displayName = activeSettings.displayName
                    )
                )
            }
        }
    }

    private fun keepAliveOnCompletionFlow(
        webcamDisplayName: String?,
        inner: suspend FlowCollector<State>.() -> Unit
    ) = flow {
        val innerFlow = flow {
            inner()
        }.retryWhen { cause, attempt ->
            Napier.e(tag = tag, message = "Caught error in inner webcam", throwable = cause)
            emit(State.Error(webcamCount = webcamCount, exception = cause, activeWebcam = activeWebcam, displayName = webcamDisplayName))
            val delay = (30.seconds + (10.seconds * attempt.toInt()).coerceAtMost(120.seconds))
            Napier.e(tag = tag, message = "Will retry webcam in $delay", throwable = cause)
            delay(delay)
            Napier.e(tag = tag, message = "Retrying... (attempt=$attempt)", throwable = cause)
            emit(State.Loading(webcamCount = webcamCount, activeWebcam = activeWebcam))
            delay(1.seconds)
            true
        }

        // Emit entire flow
        emitAll(innerFlow)

        // Flow needs to stay active after error
        delay(999.days)
    }

    data class AspectRatio(val float: Float)

    sealed class State {
        abstract val webcamCount: Int
        abstract val activeWebcam: Int
        abstract val displayName: String?
        open val warning: Warning? = null

        data object Hidden : State() {
            override val webcamCount: Int = 0
            override val activeWebcam: Int = 0
            override val displayName: String? = null
        }

        data class Warning(
            val id: String,
            val text: String,
            val actionText: String? = null,
            val actionUri: Url? = null,
            val dismissible: Boolean,
        ) {
            companion object {
                val SwitchWebcam = Warning(
                    id = "switch_webcam",
                    text = getString("webcam___warning_switch_webcam"),
                    dismissible = true,
                )

                val SwitchWebcamFeatureDisabled
                    get() = Warning(
                        id = "switch_webcam_feature_disabled",
                        text = getString("webcam___warning_switch_webcam_feature_disabled"),
                        dismissible = false,
                    )
            }
        }

        data class Loading(
            override val webcamCount: Int = 0,
            override val activeWebcam: Int = 0,
            override val warning: Warning? = SwitchWebcam.takeIf { webcamCount > 1 },
            override val displayName: String? = null,
        ) : State()

        data class Error(
            override val webcamCount: Int,
            override val activeWebcam: Int,
            override val warning: Warning? = SwitchWebcam.takeIf { webcamCount > 1 },
            override val displayName: String? = null,
            val webcamUrl: String? = null,
            val exception: Throwable,
            val description: String? = null,
            val canRetry: Boolean = exception !is WebcamDisabledException,
            val canShowDetails: Boolean = exception !is WebcamDisabledException,
            val canTroubleshoot: Boolean = exception !is WebcamDisabledException
        ) : State()

        data class FeatureNotAvailable(
            override val webcamCount: Int,
            override val activeWebcam: Int,
            override val warning: Warning? = SwitchWebcamFeatureDisabled.takeIf { webcamCount > 1 },
            override val displayName: String? = null,
            val webcamUrl: String? = null,
            val featureName: String,
        ) : State()

        data class WebpageReady(
            override val webcamCount: Int,
            override val activeWebcam: Int,
            override val warning: Warning? = SwitchWebcam.takeIf { webcamCount > 1 },
            override val displayName: String? = null,
            val webcamUrl: String?,
            val content: WebContent,
            val flipH: Boolean,
            val flipV: Boolean,
            val rotate90: Boolean,
        ) : State() {
            override fun toString() = "WebpageReady(webcamCount=$webcamCount, activeWebcam=$activeWebcam, flipH=$flipH, flipV=$flipV, rotate90=$rotate90)"
        }

        data class VideoReady(
            override val webcamCount: Int,
            override val activeWebcam: Int,
            override val warning: Warning? = SwitchWebcam.takeIf { webcamCount > 1 },
            override val displayName: String? = null,
            val flipV: Boolean,
            val flipH: Boolean,
            val rotate90: Boolean,
            val uri: String,
            val player: VideoPlayer,
            val authHeader: String?,
        ) : State() {
            override fun toString() = "VideoReady(webcamCount=$webcamCount, activeWebcam=$activeWebcam, flipH=$flipH, flipV=$flipV, rotate90=$rotate90)"
        }

        data class MjpegReady(
            override val webcamCount: Int,
            override val activeWebcam: Int,
            override val warning: Warning? = SwitchWebcam.takeIf { webcamCount > 1 },
            val frames: LowOverheadMediator<Frame>,
            override val displayName: String? = null,
            val flipH: Boolean,
            val flipV: Boolean,
            val rotate90: Boolean,
            val frameInterval: Long,
            val frameWidth: Int,
            val frameHeight: Int,
        ) : State() {

            data class Frame(
                val image: Image,
                val fps: Float,
                val frameTime: Long,
                val frameInterval: Long,
            )
        }
    }


}

expect interface VideoPlayer

expect class WebContent