package de.crysxd.octoapp.viewmodels.helper.cancelobject

import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore

class CancelObjectGcodeRendererPlugin(
    val objects: List<CancelObjectViewModelCore.PrintObject>,
) {

    var selectedId: String? = null

    private val activePaintOuter = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Yellow,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
        alpha = 0.5f
    )
    private val activePaintInner = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Yellow,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f
    )
    private val cancelledPaintOuter = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.LightGrey,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
        alpha = 0.5f
    )
    private val cancelledPaintInner = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.LightGrey,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
    )
    private val selectedPaintOuter = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Accent,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
        alpha = 0.5f
    )
    private val selectedPaintInner = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Accent,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
    )

    fun render(canvas: GcodeNativeCanvas) {
        objects.forEach { obj ->
            val (centerX, centerY) = obj.center ?: return@forEach
            val radius = 8f
            val (outerPaint, innerPaint) = when {
                obj.cancelled -> cancelledPaintOuter to cancelledPaintInner
                selectedId == obj.objectId -> selectedPaintOuter to selectedPaintInner
                else -> activePaintOuter to activePaintInner
            }

            canvas.drawArc(
                centerX = centerX,
                centerY = centerY,
                radius = radius,
                startDegrees = 0f,
                sweepAngle = 360f,
                paint = outerPaint,
            )

            if (!obj.cancelled) {
                canvas.drawArc(
                    centerX = centerX,
                    centerY = centerY,
                    radius = radius / 2,
                    startDegrees = 0f,
                    sweepAngle = 360f,
                    paint = innerPaint,
                )
            }
        }
    }
}