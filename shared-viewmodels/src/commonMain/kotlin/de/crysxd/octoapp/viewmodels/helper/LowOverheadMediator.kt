package de.crysxd.octoapp.viewmodels.helper

import com.benasher44.uuid.uuid4
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext

open class LowOverheadMediator<T>(initialData: T) {
    val id = uuid4().toString()

    private var sinks = MutableStateFlow(mapOf<String, Pair<CoroutineDispatcher, (T) -> Unit>>())
    private var lock = Mutex()
    var currentData = initialData
        private set

    suspend fun publishData(data: T) = doPublish { data }

    suspend fun republishData() = doPublish { currentData }

    private suspend fun doPublish(data: () -> T) = lock.withLock {
        val d = data()
        currentData = d
        sinks.value.values.forEach { (dispatcher, sink) ->
            withContext(dispatcher) {
                sink(d)
            }
        }
    }

    fun pushSink(sinkId: String, sink: (T) -> Unit) {
        sinks.update {
            it + (sinkId to (Dispatchers.Default to sink))
        }
        sink(currentData)
    }

    fun pushMainThreadSink(sinkId: String, sink: (T) -> Unit) {
        sinks.update {
            it + (sinkId to (Dispatchers.Main to sink))
        }
        sink(currentData)
    }

    fun popSink(sinkId: String) {
        sinks.update {
            it - sinkId
        }
    }

    override fun equals(other: Any?) = id == (other as? LowOverheadMediator<*>)?.id

    override fun hashCode() = id.hashCode()
}