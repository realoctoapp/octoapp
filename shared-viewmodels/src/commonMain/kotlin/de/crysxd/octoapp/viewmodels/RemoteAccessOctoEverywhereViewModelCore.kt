package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.url.isOctoEverywhereUrl
import io.ktor.http.Url

class RemoteAccessOctoEverywhereViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessOctoEverywhereViewModelCore"
    override val remoteServiceName = RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OCTO_EVERYWHERE
    suspend fun getLoginUrl(): String? = getLoginUrl(GetRemoteServiceConnectUrlUseCase.RemoteService.OctoEverywhere)
    override fun Url.isMyUrl() = isOctoEverywhereUrl()
    override val loadingFlow = HandleOctoEverywhereAppPortalSuccessUseCase.loadingFlow
}