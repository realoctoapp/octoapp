package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.io.getSerializable
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class TemperatureControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "TemperatureControlsViewModelCore"
    private val keyInitialConfig = "initial_config_$instanceId"
    private val settings = SharedCommonInjector.get().settings.forNameSpace("temperature_controls")
    private val temperatureRepository = SharedBaseInjector.get().temperatureDataRepository

    var initialConfiguration: ConfigCache
        get() = settings.getSerializable(
            key = keyInitialConfig,
            default = ConfigCache(listOf(ConfigCache.Item("H", true), ConfigCache.Item("B", true)))
        )
        private set(value) {
            Napier.i(tag = tag, message = "Store default config $value")
            settings.putSerializable(keyInitialConfig, value)
        }

    // A small cache layer to not read/write from.to the settings the entire time
    private var initialConfigurationCache = initialConfiguration
        set(value) {
            if (value != field && value.components.isNotEmpty()) {
                field = value
                initialConfiguration = value
            }
        }

    val temperatures = temperatureRepository.flow(instanceId = instanceId)
        .map { components ->
            components.filter { !it.isHidden }
        }
        .distinctUntilChanged { old, new ->
            val targetChange = old.map { it.current.target } == new.map { it.current.target }
            val offsetChange = old.map { it.offset } == new.map { it.offset }
            val oldTime = old.firstNotNullOfOrNull { it.history.lastOrNull()?.time } ?: 0
            val newTime = new.firstNotNullOfOrNull { it.history.lastOrNull()?.time } ?: 0
            val timeChange = (newTime.milliseconds - oldTime.milliseconds).absoluteValue < 3.seconds
            timeChange && targetChange && offsetChange
        }
        .onEach { components ->
            try {
                initialConfigurationCache = ConfigCache(components.map { ConfigCache.Item(it.component, it.canControl) })
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to store defaults", throwable = e)
            }
        }.map { components ->
            State(components)
        }.onStart {
            Napier.i(tag = tag, message = "Temperature report start")
        }.onCompletion {
            Napier.i(tag = tag, message = "Temperature report end")
        }.distinctUntilChanged()

    @kotlinx.serialization.Serializable
    data class ConfigCache(val components: List<Item>) {
        @kotlinx.serialization.Serializable
        data class Item(val component: String, val canControl: Boolean)
    }

    data class State(
        val components: List<TemperatureDataRepository.TemperatureSnapshot>
    )
}