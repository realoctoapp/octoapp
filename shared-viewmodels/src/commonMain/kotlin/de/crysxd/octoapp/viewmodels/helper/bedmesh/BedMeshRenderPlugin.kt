package de.crysxd.octoapp.viewmodels.helper.bedmesh

import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas

class BedMeshRenderPlugin {

    fun render(canvas: GcodeNativeCanvas, mesh: ParsedBedMesh) {
        mesh.sectors.forEach { sector ->
            canvas.drawRect(
                left = sector.left - 1,
                top = sector.top + 1,
                right = sector.right - 1,
                bottom = sector.bottom + 1,
                paint = GcodeNativeCanvas.Paint(
                    color = GcodeNativeCanvas.Color.Custom(red = sector.color.red, green = sector.color.green, blue = sector.color.blue),
                    mode = GcodeNativeCanvas.Paint.Mode.Fill,
                    strokeWidthCorrection = 1f,
                    strokeWidth = 2f,
                    alpha = 0.8f
                )
            )
        }
    }
}