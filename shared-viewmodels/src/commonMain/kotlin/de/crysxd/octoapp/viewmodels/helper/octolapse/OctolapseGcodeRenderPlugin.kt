package de.crysxd.octoapp.viewmodels.helper.octolapse

import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview

class OctolapseGcodeRenderPlugin(
    val snapshotPlanPreview: OctolapseSnapshotPlanPreview
) {

    var layer = 0

    private val initialPaint = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Red,
        mode = GcodeNativeCanvas.Paint.Mode.Stroke,
        strokeWidth = 2f,
        strokeWidthCorrection = 0f,
        alpha = 0.5f
    )
    private val returnPaint = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Green,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
    )
    private val stepPaintOuter = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Accent,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
        alpha = 0.5f
    )
    private val stepPaintInner = GcodeNativeCanvas.Paint(
        color = GcodeNativeCanvas.Color.Accent,
        mode = GcodeNativeCanvas.Paint.Mode.Fill,
        strokeWidth = 0f,
        strokeWidthCorrection = 0f,
    )

    fun render(canvas: GcodeNativeCanvas) {
        val layer = snapshotPlanPreview.snapshotPlans?.snapshotPlans?.getOrNull(layer) ?: return

        canvas.drawCircle(layer.initialPosition, radius = 10f, paint = initialPaint)
        canvas.drawCircle(layer.returnPosition, radius = 8f, paint = returnPaint)
        layer.steps?.forEach { step ->
            canvas.drawCircle(centerX = step.x, centerY = step.y, radius = 10f, paint = stepPaintOuter)
            canvas.drawCircle(centerX = step.x, centerY = step.y, radius = 5f, paint = stepPaintInner)
        }
    }

    private fun GcodeNativeCanvas.drawCircle(
        position: OctolapseSnapshotPlanPreview.Position?,
        radius: Float,
        paint: GcodeNativeCanvas.Paint
    ) = drawCircle(
        centerX = position?.x,
        centerY = position?.y,
        radius = radius,
        paint = paint,
    )

    private fun GcodeNativeCanvas.drawCircle(
        centerX: Float?,
        centerY: Float?,
        radius: Float,
        paint: GcodeNativeCanvas.Paint
    ) {
        drawArc(
            centerX =  centerX ?: return,
            centerY =  centerY ?: return,
            radius = radius,
            paint = paint,
            sweepAngle = 360f,
            startDegrees = 0f,
        )
    }
}