package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.models.SerialCommunication
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.flow.updateAndGet
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class TerminalViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "TerminalViewModelCore"
    private val logsSamplePeriod = 0.66.seconds
    private val logsCountCap = 2000

    private val executeGcodeCommandUseCase = SharedBaseInjector.get().executeGcodeCommandUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val preferences = SharedBaseInjector.get().preferences
    private val printerProvider = SharedBaseInjector.get().printerEngineProvider
    private val serialCommunicationLogsRepository = SharedBaseInjector.get().serialCommunicationLogsRepository
    private val getGcodeShortcuts = SharedBaseInjector.get().getGcodeShortcutsUseCase()

    private val clearedSince = MutableStateFlow(Instant.DISTANT_PAST)
    private val lastEmitted = MutableStateFlow(Instant.DISTANT_PAST)
    private val cache = MutableStateFlow(mutableListOf<TerminalLogLine>())

    private val commandStartRegex = Regex("^Send:\\s+(.*)$")
    private val commandEndRegex = Regex("^Recv:\\s+ok$")
    private var lastServerTime = Instant.DISTANT_PAST

    //region State
    private val selectedFilters = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map { config ->
        config?.appSettings?.selectedTerminalFilters ?: emptyList()
    }.distinctUntilChanged().map { regexes ->
        regexes.map { Regex(it.regex) }
    }

    private val styledTerminal = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map { config ->
        val supportsStyled = config?.systemInfo?.capabilities?.contains(SystemInfo.Capability.PrettyTerminal) == true
        val active = config?.appSettings?.isStyledTerminal == true && supportsStyled
        active to supportsStyled
    }.distinctUntilChanged()

    private val terminalDuringPrint = preferences.updatedFlow2.map {
        it.allowTerminalDuringPrint
    }.distinctUntilChanged()

    private val rawLogs = combine(selectedFilters, clearedSince) { _, _ -> }.flatMapLatest {
        // Emit the full list again when the filter change
        serialCommunicationLogsRepository.activeFlowChunked(includeOld = true, instanceId = instanceId).onStart {
            Napier.d(tag = tag, message = "Resetting raw logs")
            cache.update { mutableListOf() }
            lastEmitted.update { Instant.DISTANT_PAST }
        }
    }.buffer()

    private val logs = combine(
        rawLogs,
        selectedFilters,
        clearedSince,
    ) { log, filters, cleared ->
        // Check if we need to reset our running collection of logs
        val filtered = log.indexOfLast { it.resetPoint }.takeIf { it >= 0 }?.let { index ->
            cache.update { mutableListOf() }
            log.toMutableList().also { it.removeAll(log.take(index).toSet()) }
        } ?: log

        // Filter and map
        val mapped = filtered
            .filter { it.serverDate > cleared }
            .mapNotNull { it.filter(filters) }
            .toMutableList()

        // Limit size
        cache.updateAndGet { cache ->
            cache.addAll(mapped)
            if (cache.size > logsCountCap) {
                cache.takeLast(logsCountCap / 2).toMutableList()
            } else {
                cache
            }
        }.toList()
    }.map { logs ->
        var commandActive = false
        logs.map {
            if (it.isStart) {
                // Start command
                commandActive = true
                it
            } else if (it.isEnd) {
                // End command
                commandActive = false
                it
            } else if (commandActive) {
                // Neither start nor end but command is active -> middle
                it.copy(isMiddle = true)
            } else {
                // Neither start nor end but also no command active -> nothing
                it
            }
        }
    }.filter {
        // Fancy sample function. We can reset it when the filters change so
        // this update is emitted immediately
        val now = Clock.System.now()
        if ((now - lastEmitted.value) > logsSamplePeriod) {
            lastEmitted.update { now }
            true
        } else {
            false
        }
    }.catch {
        Napier.e(tag = tag, message = "Error in log stream", throwable = it)
    }.flowOn(Dispatchers.Default).buffer()

    private val baseState = combine(
        printerProvider.passiveCurrentMessageFlow(tag = "terminal", instanceId = instanceId),
        terminalDuringPrint,
        styledTerminal,
        selectedFilters,
    ) { current, terminalDuringPrint, (useStyledList, supportsStyledList), filters ->
        State(
            inputEnabled = !current.state.flags.isPrinting() || current.state.flags.paused || terminalDuringPrint,
            logs = emptyList(),
            shortcuts = emptyList(),
            useStyledList = useStyledList,
            filterCount = filters.size,
            supportsStyledList = supportsStyledList,
        )
    }

    val state = combine(
        baseState,
        logs,
        flow { emitAll(getGcodeShortcuts.execute(Unit)) },
    ) { base, logs, shortcuts ->
        base.copy(
            logs = logs,
            shortcuts = shortcuts,
        )
    }.flowOn(Dispatchers.Default)

    private fun SerialCommunication.filter(
        filters: List<Regex>,
    ) = if (filters.none { it.containsMatchIn(content) }) {
        lastServerTime = if (serverDate > lastServerTime) serverDate else lastServerTime

        val isStart = commandStartRegex.matches(content)
        val isEnd = commandEndRegex.matches(content)

        TerminalLogLine(
            id = id,
            text = content,
            textStyled = content.removePrefix("Send:").removePrefix("Recv:").trim(),
            isStart = isStart,
            isMiddle = false, // Determined when looping over the whole list
            isEnd = isEnd,
            date = date,
        )
    } else {
        null
    }

    //endregion
    //region Actions
    fun clear() {
        clearedSince.update { lastServerTime }
    }

    fun getShortcuts() = runBlocking { getGcodeShortcuts.executeBlocking(Unit).first() }

    suspend fun toggleStyledLogs() {
        printerConfigRepository.updateAppSettings(id = instanceId) {
            it.copy(isStyledTerminal = it.isStyledTerminal != true)
        }
    }

    suspend fun executeGcode(gcode: String) = try {
        executeGcodeCommandUseCase.execute(
            param = ExecuteGcodeCommandUseCase.Param(
                command = GcodeCommand.Batch(gcode.split("\n").map { it.trim() }.filter { it.isNotBlank() }),
                fromUser = true,
                instanceId = instanceId
            )
        )

        delay(1.seconds)
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failure while executing Gcode", throwable = e)
    }
    //endregion

    data class State(
        val logs: List<TerminalLogLine>,
        val inputEnabled: Boolean,
        val shortcuts: List<GcodeHistoryItem>,
        val useStyledList: Boolean,
        val filterCount: Int,
        val supportsStyledList: Boolean,
    )

    data class TerminalLogLine(
        val id: Int,
        val date: Instant,
        val text: String,
        val textStyled: String,
        val isStart: Boolean,
        val isEnd: Boolean,
        val isMiddle: Boolean,
    )
}