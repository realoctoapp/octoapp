package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.usecase.CancelObjectUseCase
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.CancelObjectPluginMessage
import de.crysxd.octoapp.viewmodels.ext.printBed
import de.crysxd.octoapp.viewmodels.helper.cancelobject.CancelObjectGcodeRendererPlugin
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class CancelObjectViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "CancelObjectViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPreferences = SharedBaseInjector.get().preferences
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val triggerInitialCancelObjectMessageUseCase = SharedBaseInjector.get().triggerInitialCancelObjectMessageUseCase()
    private val cancelObjectUseCase = SharedBaseInjector.get().cancelObjectUseCase()

    private val retryTrigger = MutableStateFlow(0)
    var stateCache: FlowState<ObjectList> = FlowState.Loading()
        private set

    private val cancelObjectSupported = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map {
        it?.hasPlugin(OctoPlugins.CancelObject) == true
    }.distinctUntilChanged()

    private val printObjectSupported = octoPrintProvider.passiveCurrentMessageFlow(tag = "cancel-object-vm-installed", instanceId = instanceId).map {
        it.printObjects != null
    }.distinctUntilChanged()

    private val renderParams = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map {
        val profile = it?.activeProfile ?: return@map RenderParams.empty
        RenderParams(
            printBed = it.printBed,
            originInCenter = profile.volume.origin == PrinterProfile.Origin.Center,
            printBedWidthMm = profile.volume.width,
            printBedHeightMm = profile.volume.height,
        )
    }.distinctUntilChanged()

    private val objects = combine(printObjectSupported, cancelObjectSupported, retryTrigger) { printObjectSupported, cancelObjectSupported, _ ->
        when {
            printObjectSupported -> printObjectFlow()
            cancelObjectSupported -> octoPrintCancelObjectFlow()
            else -> flowOf(null)
        }
    }.flatMapLatest {
        it
    }.distinctUntilChanged()

    val state = combine(objects, cancelObjectSupported, printObjectSupported, renderParams) { objectList, cancelObject, printObject, renderParams ->
        Napier.d(tag = tag, message = "Publishing $objectList")
        //region If we have too many objects, take 2 before and 2 after the active
        val maxElements = 5
        val elementsBefore = (maxElements - 1) / 2
        val subSet = if (objectList != null && objectList.size > maxElements) {
            val activeIndex = objectList.firstOrNull { it.active }?.let { objectList.indexOf(it) } ?: 0
            var startIndex = (activeIndex - elementsBefore).coerceAtLeast(0)
            val endIndex = (startIndex + maxElements).coerceAtMost(objectList.size)
            startIndex = (endIndex - maxElements).coerceAtLeast(0)

            objectList.subList(startIndex, endIndex)
        } else {
            objectList ?: emptyList()
        }
        Napier.i(tag = tag, message = "Publishing ${objectList?.size} objects (cancelObject=$cancelObject, printObject=$printObject)")
        //endregion
        //region Emit state
        when {
            !cancelObject && !printObject -> FlowStateEmpty
            objectList == null -> FlowState.Loading()
            objectList.isEmpty() -> FlowStateEmpty
            else -> FlowState.Ready(
                data = ObjectList(
                    all = objectList,
                    subset = subSet,
                    renderParams = renderParams,
                    renderPlugin = CancelObjectGcodeRendererPlugin(objectList)
                ),
            )
        }
        //endregion
    }.distinctUntilChanged().onEach {
        stateCache = it
    }.onStart {
        emit(stateCache)
    }.catch {
        if (it is MissingPluginException) {
            emit(FlowStateEmpty)
        } else {
            emit(FlowState.Error(it))
            Napier.e(tag = tag, message = "Failure in CancelObject flow", throwable = it)
        }
    }.shareIn(
        scope = AppScope,
        started = SharingStarted.WhileSubscribedOctoDelay,
        replay = 1
    ).onStart {
        // Delay a tad so more important stuff goes first
        // Do this after share block so objects are requested whenever we resume this flow
        delay(1.seconds)
        triggerInitialMessage()
    }

    private fun octoPrintCancelObjectFlow() = combine(
        // Collect the active object
        octoPrintProvider.passiveCachedMessageFlow(
            tag = "cancel-object-vm-active",
            clazz = CancelObjectPluginMessage.Active::class,
            instanceId = instanceId
        ).onStart { emit(null) }.onEach {
            Napier.d(tag = tag, message = "Received active: ${it?.activeId}")
        },

        // Collect the object list
        octoPrintProvider.passiveCachedMessageFlow(
            tag = "cancel-object-vm-objects",
            clazz = CancelObjectPluginMessage.ObjectList::class,
            instanceId = instanceId
        ).onStart { emit(null) }.onEach {
            Napier.d(tag = tag, message = "Received objects: ${it?.objects}")
        },
    ) { active, objects ->
        objects?.objects?.filter { obj ->
            obj.ignore != true
        }?.mapNotNull { obj ->
            val id = obj.id ?: return@mapNotNull null
            val minX = obj.boundingBox?.minX
            val minY = obj.boundingBox?.minY
            val maxX = obj.boundingBox?.maxX
            val maxY = obj.boundingBox?.maxY
            PrintObject(
                objectId = id.toString(),
                label = obj.label ?: id.toString(),
                active = active?.activeId == id,
                cancelled = obj.cancelled == true,
                // Objects that are not printed yet have an invalid bounding box
                center = if (minX != null && maxX != null && minY != null && maxY != null && maxX >= minX && maxY >= minY) {
                    (minX + (maxX - minX) / 2) to (minY + (maxY - minY) / 2)
                } else {
                    null
                }
            )
        }
    }.onEach {
        Napier.d(tag = tag, message = "Emitting objects: $it")
    }

    private fun printObjectFlow() = octoPrintProvider.passiveCurrentMessageFlow(
        tag = "cancel-object-vm-print-object",
        instanceId = instanceId,
    ).map { message ->
        message.printObjects
    }.distinctUntilChanged().map { objects ->
        objects?.all?.map {
            PrintObject(
                objectId = it.id,
                label = it.label ?: it.id,
                active = objects.currentObject == it.id,
                cancelled = it.id in objects.excluded,
                center = it.center,
            )
        }
    }

    suspend fun triggerInitialMessage() = try {
        when {
            !octoPreferences.activePluginIntegrations -> Napier.i(tag = tag, message = "Skipping CancelObject initial message, no active plugin integrations allowed")
            printerConfigRepository.get(instanceId)?.hasPlugin(OctoPlugins.CancelObject, log = true) != true -> Napier.i(
                tag = tag,
                message = "CancelObject not installed"
            )

            else -> {
                // Delay slightly so other more important requests go first
                Napier.i(tag = tag, message = "Requesting objects")
                triggerInitialCancelObjectMessageUseCase.execute(param = TriggerInitialCancelObjectMessageUseCase.Params(instanceId))
            }
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to trigger initial message", throwable = e)
    }

    private val CancelObjectPluginMessage.ObjectList.Object.boundingBox: Box?
        get() {
            return Box(
                minX = minX ?: return null,
                minY = minY ?: return null,
                maxX = maxX ?: return null,
                maxY = maxY ?: return null,
            )
        }

    suspend fun cancelObject(objectId: String) = try {
        cancelObjectUseCase.execute(CancelObjectUseCase.Params(instanceId = instanceId, objectId = objectId))
        delay(timeMillis = 500)
        triggerInitialMessage()
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failed to cancel object", throwable = e)
    }

    fun retry() {
        stateCache = FlowState.Loading()
        retryTrigger.update { it + 1 }
    }

    data class PrintObject(
        val objectId: String,
        val cancelled: Boolean,
        val active: Boolean,
        val label: String,
        val center: Pair<Float, Float>?,
    )

    data class ObjectList(
        val all: List<PrintObject>,
        val subset: List<PrintObject>,
        val renderParams: RenderParams,
        val renderPlugin: CancelObjectGcodeRendererPlugin,
    )

    data class RenderParams(
        val printBed: GcodeNativeCanvas.Image,
        val printBedWidthMm: Float,
        val printBedHeightMm: Float,
        val originInCenter: Boolean
    ) {
        companion object {
            val empty = RenderParams(
                printBedHeightMm = 200f,
                printBedWidthMm = 200f,
                printBed = GcodeNativeCanvas.Image.PrintBedGeneric,
                originInCenter = false
            )
        }
    }

    data class Box(
        val minX: Float,
        val minY: Float,
        val maxX: Float,
        val maxY: Float,
    )

    private val FlowStateEmpty = FlowState.Ready(
        ObjectList(
            all = emptyList(),
            subset = emptyList(),
            renderParams = RenderParams.empty,
            renderPlugin = CancelObjectGcodeRendererPlugin(emptyList())
        )
    )
}
