package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.isDarwin
import io.ktor.http.Url
import kotlinx.coroutines.flow.map

class PluginsLibraryViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    companion object {
        private val OCTOEVERYWHERE_ORDER = (100..1000).random()
        private val SPAGHETTI_DETECTIVE_ORDER = (100..1000).random()
        private val NGROK_ORDER = (0..100).random()
        private val TAILSCALE_ORDER = (0..100).random()
        private const val configurationNeeded = "**Requires sign in before use**\n\n"
        private val platform get() = SharedCommonInjector.get().platform
        val baseIndex = PluginsIndex(
            //region [...]
            //region [...]
            listOf(
                PluginCategory(
                    name = "Recommended",
                    id = null,
                    plugins = listOf(
                        Plugin(
                            name = "OctoApp Companion",
                            key = OctoPlugins.OctoApp,
                            highlight = true,
                            order = 10_000,
                            description = if (platform.isDarwin()) {
                                "**Required for notifications**\n\nThe Companion plugin is required for some features in the app and unlocks performance improvements."
                            } else {
                                "Install the OctoApp Companion plugin for many performance improvements and better notifications. This plugin is required for some features in the app."
                            },
                            pluginPage = "https://plugins.octoprint.org/plugins/octoapp/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "PSU Control",
                            key = OctoPlugins.PsuControl,
                            highlight = true,
                            order = 9_999,
                            description = "Turn your printer on and off via the webinterface and OctoApp and let your printer turn off automatically after a print is done.",
                            pluginPage = "https://plugins.octoprint.org/plugins/psucontrol/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "OctoEverywhere",
                            highlight = true,
                            key = OctoPlugins.OctoEverywhere,
                            order = OCTOEVERYWHERE_ORDER,
                            description = "${configurationNeeded}Easy remote access to OctoPrint so you can use OctoApp from wherever you are!",
                            pluginPage = "https://plugins.octoprint.org/plugins/octoeverywhere/".toUrlOrNull(),
                            octoAppTutorial = "https://www.youtube.com/watch?v=kvSLAsBHL00".toUrlOrNull(),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "Obico",
                            highlight = true,
                            order = SPAGHETTI_DETECTIVE_ORDER,
                            key = OctoPlugins.Obico,
                            description = "${configurationNeeded}Use Obico's tunnel to use OctoApp when you are not at home!",
                            pluginPage = "https://plugins.octoprint.org/plugins/obico/".toUrlOrNull(),
                            octoAppTutorial = "https://www.youtube.com/watch?v=kOfhlZgye10".toUrlOrNull(),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                    ).sortedByDescending { it.order }
                ),
                PluginCategory(
                    name = "Remote access",
                    id = "remoteAccess",
                    plugins = listOf(
                        Plugin(
                            name = "ngrok",
                            key = OctoPlugins.Ngrok,
                            order = NGROK_ORDER,
                            description = "${configurationNeeded}A plugin to securely access your OctoPrint instance remotely through ngrok",
                            pluginPage = "https://plugins.octoprint.org/plugins/ngrok/".toUrlOrNull(),
                            octoAppTutorial = "https://www.youtube.com/watch?v=Rskcyzujhps".toUrlOrNull(),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "OctoEverywhere",
                            highlight = true,
                            key = OctoPlugins.OctoEverywhere,
                            order = OCTOEVERYWHERE_ORDER,
                            description = "${configurationNeeded}Easy remote access to OctoPrint so you can use OctoApp from wherever you are!",
                            pluginPage = "https://plugins.octoprint.org/plugins/octoeverywhere/".toUrlOrNull(),
                            octoAppTutorial = "https://www.youtube.com/watch?v=kvSLAsBHL00".toUrlOrNull(),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "Obico",
                            highlight = true,
                            order = SPAGHETTI_DETECTIVE_ORDER,
                            key = OctoPlugins.Obico,
                            description = "${configurationNeeded}Use Obico's tunnel to use OctoApp when you are not at home!",
                            pluginPage = "https://plugins.octoprint.org/plugins/obico/".toUrlOrNull(),
                            octoAppTutorial = "https://www.youtube.com/watch?v=kOfhlZgye10".toUrlOrNull(),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                        Plugin(
                            name = "Tailscale",
                            key = "tailscale",
                            order = TAILSCALE_ORDER,
                            description = "Not a plugin — but a great way to easily enabled remote access",
                            octoAppTutorial = "https://www.youtube.com/watch?v=2Ox1JJEEYoU".toUrlOrNull(),
                            configureLink = UriLibrary.getConfigureRemoteAccessUri(),
                        ),
                    ).sortedByDescending { it.order }
                ),
                PluginCategory(
                    name = "Power",
                    id = "power",
                    plugins = listOf(
                        Plugin(
                            name = "PSU Control",
                            key = OctoPlugins.PsuControl,
                            highlight = true,
                            description = "Turn your printer on and off via the webinterface and OctoApp and let your printer turn off automatically after a print is done.",
                            pluginPage = "https://plugins.octoprint.org/plugins/psucontrol/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "GPIO Control",
                            key = OctoPlugins.GpioControl,
                            description = "Control each device connected to your Raspberry Pi from the web interface and OctoApp.",
                            pluginPage = "https://plugins.octoprint.org/plugins/gpiocontrol/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Tasmota",
                            key = OctoPlugins.Tasmota,
                            description = "Simple plugin to control sonoff devices that have been flashed with Tasmota.",
                            pluginPage = "https://plugins.octoprint.org/plugins/tasmota/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "TPLink Smart Plug",
                            key = OctoPlugins.TpLinkSmartPlug,
                            description = "Plugin to control TP-Link Smartplug devices from OctoPrint web interface and OctoApp",
                            pluginPage = "https://plugins.octoprint.org/plugins/tplinksmartplug/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "IKEA Tradfri",
                            key = OctoPlugins.IkeaTradfri,
                            description = "Control Ikea Tradfri outlet from OctoPrint web interface and OctoApp",
                            pluginPage = "https://plugins.octoprint.org/plugins/ikea_tradfri/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "WS281x LED Status",
                            key = OctoPlugins.Ws281xLedStatus,
                            description = "Add some WS281x type RGB LEDs to your printer for a quick status update! OctoApp can control the torch to illuminate your print.",
                            pluginPage = "https://plugins.octoprint.org/plugins/ws281x_led_status/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "MyStrom",
                            key = OctoPlugins.MyStromSwitch,
                            description = "Plugin to integrate myStrom Switch into your OctoPrint installation.",
                            pluginPage = "https://plugins.octoprint.org/plugins/mystromswitch/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "OctoRelay",
                            key = OctoPlugins.OctoRelay,
                            description = "A plugin to control relays or other things on the GPIO pins of your raspberry pi. For example turn the power of printer, the light or a fan ON and OFF via the web interface.",
                            pluginPage = "https://plugins.octoprint.org/plugins/octorelay/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "OctoLight",
                            key = OctoPlugins.OctoLight,
                            description = "A simple plugin, that add's a button to the navbar, toggling GPIO on the RPi. It can be used for turning on and off a light.",
                            pluginPage = "https://plugins.octoprint.org/plugins/octolight/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "OctoLightHA",
                            key = OctoPlugins.OctoLightHA,
                            description = "A simple plugin, that add's a button to the navbar, toggling a light with Home Assistant. It can be used for turning on and off a light.",
                            pluginPage = "https://plugins.octoprint.org/plugins/octolightHA/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Wemo Switch",
                            key = OctoPlugins.WemoSwitch,
                            description = "This plugin allows for the control of Belkin Wemo devices via OctoApp, navbar buttons and gcode commands.",
                            pluginPage = "https://plugins.octoprint.org/plugins/wemoswitch/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "USB Relay Control",
                            key = OctoPlugins.UsbRelayControl,
                            description = "Control each USB Relay connected to your Raspberry Pi from the web interface and OctoApp.",
                            pluginPage = "https://github.com/abudden/OctoPrint-USBRelayControl".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Ophom",
                            key = OctoPlugins.Ophom,
                            description = "Switch a Philips Hue that your printer is connected to on or off. You can set an automatic switch-off based on a minimum temperature.",
                            pluginPage = "https://plugins.octoprint.org/plugins/ophom/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "OctoHue",
                            key = OctoPlugins.OctoHue,
                            description = "Hue lighting control for Octoprint - Illuminate your printer and signal its status using Phillips Hue lights.",
                            pluginPage = "https://plugins.octoprint.org/plugins/octohue/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Wled",
                            key = OctoPlugins.Wled,
                            description = "This plugin allows you to configure a WLED device to connect to OctoPrint to display the status of your prints with ease!",
                            pluginPage = "https://plugins.octoprint.org/plugins/wled/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Enclosure",
                            key = OctoPlugins.Enclosure,
                            description = "Control GPIO of your Raspberry Pi and add additional temperature sensors to OctoApp. Important: OctoApp will only see temperatures that were added to the temperature graph in the plugin's settings.",
                            pluginPage = "https://plugins.octoprint.org/plugins/enclosure/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Tuya Smartplug",
                            key = OctoPlugins.Tuya,
                            description = "Work based on OctoPrint-TPLinkSmartplug and python-tuya. This plugin controlls Tuya-based SmartPlugs.",
                            pluginPage = "https://plugins.octoprint.org/plugins/tuyasmartplug/".toUrlOrNull(),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Materials",
                    id = "materials",
                    plugins = listOf(
                        Plugin(
                            name = "FilamentManager",
                            key = OctoPlugins.FilamentManager,
                            description = "This OctoPrint plugin helps to manage your filament spools.",
                            pluginPage = "https://plugins.octoprint.org/plugins/filamentmanager/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "SpoolManager",
                            key = OctoPlugins.SpoolManager,
                            description = "The OctoPrint-Plugin manages all spool informations and stores it in a database.",
                            pluginPage = "https://plugins.octoprint.org/plugins/SpoolManager/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Spoolman",
                            key = OctoPlugins.Spoolman,
                            description = "An OctoPrint plugin integrating with Spoolman, a universal filament spools inventory manager.",
                            pluginPage = "https://plugins.octoprint.org/plugins/Spoolman/".toUrlOrNull(),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Camera",
                    id = "camera",
                    plugins = listOf(
                        Plugin(
                            name = "go2rtc",
                            key = OctoPlugins.Go2Rtc,
                            description = "This OctoPrint plugin connects to the go2rtc webcam server",
                            pluginPage = "https://plugins.octoprint.org/plugins/go2rtc/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "CameraStreamer Control",
                            key = OctoPlugins.CameraStreamerControl,
                            description = "Make use of smooth, low-latency, low-bandwidth WebRTC streams from the OctoPi 'New camera stack'.",
                            pluginPage = "https://plugins.octoprint.org/plugins/camerastreamer_control/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Multicam",
                            key = OctoPlugins.MultiCam,
                            description = "Extends the Control tab of OctoPrint and the webcam view in OctoApp, allowing the ability to switch between multiple webcam feeds.",
                            pluginPage = "https://plugins.octoprint.org/plugins/multicam/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Octolapse",
                            key = OctoPlugins.Octolapse,
                            description = "Confirm snapshot preview plans in OctoApp and see any Octolapse errors directly in the app",
                            pluginPage = "https://plugins.octoprint.org/plugins/octolapse/".toUrlOrNull(),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Files",
                    id = "files",
                    plugins = listOf(
                        Plugin(
                            name = "Cura Thumbnails",
                            key = OctoPlugins.UltimakerFormatPackage,
                            description = "This plugin adds support for Ultimaker Format Package (.ufp) files.",
                            pluginPage = "https://plugins.octoprint.org/plugins/UltimakerFormatPackage/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Slicer Thumbnails (PrusaSlicer, SuperSlicer, OrcaSlicer)",
                            key = OctoPlugins.PrusaSlicerThumbnails,
                            description = "Extracts various slicer's embedded thumbnails from gcode files.",
                            pluginPage = "https://plugins.octoprint.org/plugins/prusaslicerthumbnails/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Upload Anything",
                            key = OctoPlugins.UploadAnything,
                            description = "Allows custom file types to be uploaded via the web interface and OctoApp.",
                            pluginPage = "https://plugins.octoprint.org/plugins/uploadanything/".toUrlOrNull(),
                            supported = platform.isAndroid(),
                        ),
                    )
                ),
                PluginCategory(
                    name = "Others",
                    id = "others",
                    plugins = listOf(
                        Plugin(
                            name = "PrintTimeGenius",
                            key = OctoPlugins.PrintTimeGenius,
                            highlight = true,
                            description = "Use a gcode pre-analysis to provide better print time estimation. OctoApp can show the improved estimations.",
                            pluginPage = "https://plugins.octoprint.org/plugins/PrintTimeGenius/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Cancelobject",
                            key = OctoPlugins.CancelObject,
                            description = "Cancel single objects during a print based on gcode comment lines",
                            pluginPage = "https://plugins.octoprint.org/plugins/cancelobject/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "ArcWelder",
                            key = OctoPlugins.ArcWelder,
                            description = "Anti-Stutter and GCode Compression. Replaces G0/G1 with G2/G3 where possible. OctoApp can show arcs generated in the Gcode preview.",
                            pluginPage = "https://plugins.octoprint.org/plugins/arc_welder/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Mmu2filamentselect",
                            key = OctoPlugins.Mmu2FilamentSelect,
                            description = "Select filament for the MMU2 in OctoApp\n**Attention: also requires the OctoApp companion plugin for reliable operation!**",
                            pluginPage = "https://plugins.octoprint.org/plugins/mmu2filamentselect/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Prusa MMU",
                            key = OctoPlugins.PrusaMmu,
                            description = "Select filament for the MMU in OctoApp\n**Attention: also requires the OctoApp companion plugin for reliable operation!**",
                            pluginPage = "https://plugins.octoprint.org/plugins/prusammu/".toUrlOrNull(),
                        ),
                        Plugin(
                            name = "Better Grbl Support",
                            key = OctoPlugins.BetterGrblSupport,
                            description = "OctoApp hides useless controls when the plugin is installed",
                            pluginPage = "https://plugins.octoprint.org/plugins/bettergrblsupport/".toUrlOrNull(),
                            supported = platform.isAndroid(),
                        ),
                        Plugin(
                            name = "OctoKlipper",
                            key = OctoPlugins.OctoKlipper,
                            description = "OctoApp will pick up all Macros configured",
                            pluginPage = "https://plugins.octoprint.org/plugins/klipper/".toUrlOrNull(),
                        ),
                    )
                )
            )
            //endregion
        )
    }

    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    val index = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map { config ->
        config ?: return@map baseIndex

        // Check which are installed
        PluginsIndex(
            categories = baseIndex.categories.map { category ->
                category.copy(
                    plugins = category.plugins.map { plugin ->
                        plugin.copy(installed = config.hasPlugin(plugin.key))
                    }
                )
            }
        )
    }

    data class PluginsIndex(
        val categories: List<PluginCategory>
    )

    data class PluginCategory(
        val plugins: List<Plugin>,
        val id: String?,
        val name: String,
    )

    data class Plugin(
        val key: String,
        val name: String,
        val description: String,
        val highlight: Boolean = false,
        val pluginPage: Url? = null,
        val octoAppTutorial: Url? = null,
        val installed: Boolean? = null,
        val order: Int = 0,
        val configureLink: Url? = null,
        val supported: Boolean = true,
    )
}