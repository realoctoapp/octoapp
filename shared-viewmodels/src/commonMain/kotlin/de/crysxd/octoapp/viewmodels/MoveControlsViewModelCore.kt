package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.HomePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.JogPrintHeadUseCase
import de.crysxd.octoapp.base.usecase.MovePrintHeadUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.AbsoluteToolheadMove
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.RealTimeStats
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class MoveControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "MoveControlsViewModelCore"
    private val homePrintHeadUseCase = SharedBaseInjector.get().homePrintHeadUseCase()
    private val jogPrintHeadUseCase = SharedBaseInjector.get().jogPrintHeadUseCase()
    private val movePrintHeadUseCase = SharedBaseInjector.get().movePrintHeadUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val executeGcodeCommandUseCase = SharedBaseInjector.get().executeGcodeCommandUseCase()
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val octoPreferences = SharedBaseInjector.get().preferences

    val state = combine(
        octoPreferences.updatedFlow2,
        octoPrintProvider.passiveCurrentMessageFlow(tag = "MoveControls-visible", instanceId = instanceId).distinctUntilChangedBy {
            listOf(it.realTimeStats, it.state)
        },
        printerConfigRepository.instanceInformationFlow(instanceId = instanceId)
    ) { preferences, current, instance ->
        createState(
            preferences = preferences,
            current = current,
            capabilities = instance?.systemInfo?.capabilities
        )
    }.distinctUntilChanged()

    suspend fun homeXYAxis() = try {
        homePrintHeadUseCase.execute(
            HomePrintHeadUseCase.Params(
                axis = HomePrintHeadUseCase.Axis.XY,
                instanceId = instanceId
            )
        )

        // Ensure applied (just for UI smoothness)
        delay(2.seconds)
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to home XY", throwable = e)
        reportException(e)
    }

    suspend fun homeZAxis() = try {
        homePrintHeadUseCase.execute(
            HomePrintHeadUseCase.Params(
                axis = HomePrintHeadUseCase.Axis.Z,
                instanceId = instanceId
            )
        )
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to home XY", throwable = e)
        reportException(e)
    }

    fun moveAxis(axis: Axis, direction: Direction, distanceMm: Double) = AppScope.launch(coroutineExceptionHandler) {
        val directedDistance = (direction.multiplier * distanceMm).toFloat()
        jogPrintHeadUseCase.execute(
            JogPrintHeadUseCase.Param(
                instanceId = instanceId,
                xDistance = directedDistance.takeIf { axis == Axis.X } ?: 0f,
                yDistance = directedDistance.takeIf { axis == Axis.Y } ?: 0f,
                zDistance = directedDistance.takeIf { axis == Axis.Z } ?: 0f,
            )
        )
    }

    suspend fun moveToPosition(x: Float, y: Float, z: Float) = try {
        movePrintHeadUseCase.execute(
            MovePrintHeadUseCase.Param(
                instanceId = instanceId,
                xPosition = x,
                yPosition = y,
                zPosition = z,
            )
        )
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to home XY", throwable = e)
        reportException(e)
    }


    suspend fun turnMotorsOff() = try {
        executeGcodeCommandUseCase.execute(
            ExecuteGcodeCommandUseCase.Param(
                command = GcodeCommand.Single("M84"),
                fromUser = false,
                instanceId = instanceId,
            )
        )

        // Ensure applied (just for UI smoothness)
        delay(2.seconds)
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failed to turn motors off", throwable = e)
    }

    fun getState() = createState(
        preferences = octoPreferences,
        current = octoPrintProvider.getLastCurrentMessage(instanceId = instanceId),
        capabilities = printerConfigRepository.get(instanceId)?.systemInfo?.capabilities,
    )

    private fun createState(
        preferences: OctoPreferences,
        current: Message.Current?,
        capabilities: List<SystemInfo.Capability>?
    ): State {
        val showDuringPrint = preferences.moveDuringPrint
        val printing = current?.state?.flags?.isPrinting() == true
        val paused = current?.state?.flags?.paused == true
        val xyHomed = current?.realTimeStats?.toolhead?.xyHomed ?: true
        val zHomed = current?.realTimeStats?.toolhead?.zHomed ?: true
        return State(
            visible = when {
                current == null -> false
                paused && showDuringPrint -> true
                printing -> false
                else -> true
            },
            limitedControls = printing,
            position = if (RealTimeStats in (capabilities ?: emptyList())) {
                Position(
                    x = current?.realTimeStats?.toolhead?.positionX?.takeIf { xyHomed },
                    y = current?.realTimeStats?.toolhead?.positionY?.takeIf { xyHomed },
                    z = current?.realTimeStats?.toolhead?.positionZ?.takeIf { zHomed },
                )
            } else {
                null
            },
            xyHomed = xyHomed,
            zHomed = zHomed,
            canMoveToPosition = AbsoluteToolheadMove in (capabilities ?: emptyList()),
            steppersEnabled = current?.realTimeStats?.steppers?.enabled?.filterKeys { component -> "extruder" !in component }?.values?.any { it },
        )
    }

    enum class Axis {
        X, Y, Z
    }

    enum class Direction {
        Positive, Negative, None;

        val multiplier
            get() = when (this) {
                Positive -> 1
                Negative -> -1
                None -> 0
            }
    }

    data class State(
        val visible: Boolean,
        val limitedControls: Boolean,
        val position: Position?,
        val xyHomed: Boolean,
        val zHomed: Boolean,
        val canMoveToPosition: Boolean,
        val steppersEnabled: Boolean?,
    )

    data class Position(
        val x: Float?,
        val y: Float?,
        val z: Float?
    )
}