package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.SharedBaseInjector

class MenuItemViewModelCore {

    private val pinnedMenuItemRepository = SharedBaseInjector.getOrNull()?.pinnedMenuItemRepository

    fun getPinningState(itemId: String): Map<MenuId, Boolean> = pinnedMenuItemRepository?.checkPinnedState(itemId)?.toMap() ?: emptyMap()

    fun setItemPinned(
        itemId: String,
        menuId: MenuId,
        pinned: Boolean
    ) = pinnedMenuItemRepository?.setMenuItemPinned(
        menuId = menuId,
        itemId = itemId,
        pinned = pinned
    )
}