package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn

class Mmu2FilamentSelectViewModelCore(
    instanceId: String
) : BaseViewModelCore(instanceId = instanceId) {

    private val tag = "MmuFilamentSelectViewModel"
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    val events = octoPrintProvider.passiveCachedMessageFlow(tag = "mmu2-filament-select-support", CompanionPluginMessage::class)
        .map { it?.mmuSelectionActive == true }
        .distinctUntilChanged()
        .map {
            Napier.i(tag = tag, message = "MMU change: $it")
            val source = printerConfigRepository.getActiveInstanceSnapshot()?.settings?.plugins?.mmu?.labelSource
                ?: Settings.Mmu.LabelSource.Manual
            when (it) {
                true -> Event.ShowMenu(source)
                false -> Event.CloseMenu
            }
        }.retry {
            Napier.e(tag = tag, message = "Failure in MMU2 flow", throwable = it)
            delay(1000)
            true
        }.shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay, replay = 1)

    sealed class Event {
        data class ShowMenu(val labelSource: Settings.Mmu.LabelSource) : Event()
        data object CloseMenu : Event()
    }
}