package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.utils.AppScope
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

abstract class BaseViewModelCore(val instanceId: String) {

    private val mutableErrors = MutableSharedFlow<Throwable?>(replay = 0, extraBufferCapacity = 1)

    val errors = mutableErrors.asSharedFlow()

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
        reportException(throwable)
    }

    internal fun reportException(e: Throwable) = AppScope.launch {
        if (e !is CancellationException) {
            mutableErrors.emit(e)
        }
    }
}