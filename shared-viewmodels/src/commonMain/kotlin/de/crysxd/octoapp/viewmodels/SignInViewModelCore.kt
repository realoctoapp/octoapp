package de.crysxd.octoapp.viewmodels

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.BackendType
import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.usecase.execute
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.PreConfigurationHost
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class SignInViewModelCore(private val allowNetworkInSplash: Boolean) {

    companion object {
        private val mainProcessDuration = 3.5.seconds
        private val splashUntil = Clock.System.now() + mainProcessDuration
    }

    private val tag = "SignInViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val discoverServicesUseCase = SharedBaseInjector.get().discoverOctoPrintUseCase()
    private val testFullNetworkStackUseCase = SharedBaseInjector.get().testFullNetworkStackUseCase()
    private val requestApiAccessUseCase = SharedBaseInjector.get().requestApiAccessUseCase()
    private val updateInstanceCapabilitiesUseCase = SharedBaseInjector.get().updateInstanceCapabilitiesUseCase()
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val octoPreferences = SharedBaseInjector.get().preferences
    private val sensitiveDataMask = SharedBaseInjector.get().sensitiveDataMask

    private val mutableState = MutableStateFlow<Pair<Int, State>>(0 to State.Initial)
    val state = mutableState.asStateFlow()
        .flatMapLatest { (_, state) ->
            when (state) {
                is State.Initial -> flowOf(State.Initial)
                is State.Splash,
                is State.Discover -> createDiscoverFlow()

                is State.Probe -> createProbeFlow(webUrl = state.urlString, reusedInstanceId = state.reusedInstanceId, apiKey = state.apiKey)
                is State.AccessRequest -> createAccessRequestFlow(webUrl = state.url, reusedInstanceId = state.reusedInstanceId, state.type)
                is State.AccessRequestFailed -> flowOf(state)
                is State.Success -> createSuccessFlow(state)
            }
        }.catch {
            Napier.e(tag = tag, message = "Non-contained exception caught, resetting", throwable = it)
            emit(State.Initial)
        }.onEach {
            Napier.i(tag = tag, message = "Now in state: ${it::class.simpleName}")
        }

    private suspend fun createDiscoverFlow() = flow {
        val discoverFlow = discoverServicesUseCase.execute()
        val configFlow = printerConfigRepository.allInstanceInformationFlow()
        val quickSwitchFlow = BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH)
        val startedAt = Clock.System.now()

        val activeUntil = startedAt + mainProcessDuration
        val splashFlow = configFlow.flatMapLatest { configs ->
            flow {
                // Skip splash if time is over or we already have some instances connected
                val splashTime = splashUntil - Clock.System.now()
                if (splashTime > 0.seconds && configs.isEmpty()) {
                    emit(true)
                    delay(splashTime)
                }

                emit(false)
            }
        }

        val splashDiscoverFlow = combine(splashFlow, configFlow, quickSwitchFlow) { i, j, k -> Triple(i, j, k) }
            .flatMapLatest { (splash, configs, hasQuickSwitch) ->
                // We return the real discover flow if we are no longer in splash, allowed to use network in splash (Android yes, iOS no) or we already have configs
                // which means we skip splash
                if (!splash || allowNetworkInSplash || configs.isNotEmpty()) {
                    // Add little extra delay to let splash finish properly in UI in case we were not allowed to network before
                    if (!allowNetworkInSplash) {
                        delay(500.milliseconds)
                    }

                    // Filter out instances that are already connected. If quick switch is disabled, still list them
                    discoverFlow.map { (services) ->
                        services.filterAlreadyKnown(configs, hasQuickSwitch)
                    }
                } else {
                    // We are not allowed to connect discover flow yet, will trigger network access during splash
                    flowOf(emptyList())
                }
            }

        val combined = combine(splashFlow, splashDiscoverFlow, configFlow, quickSwitchFlow) { splash, discoverServices, configs, hasQuickSwitch ->
            when {
                splash -> State.Splash(
                    until = activeUntil
                )

                else -> State.Discover(
                    services = discoverServices,
                    configs = configs.values.sortedBy { it.label }.toList().sorted(),
                    hasQuickSwitch = hasQuickSwitch
                )
            }
        }

        emitAll(combined)
    }

    private fun List<NetworkService>.filterAlreadyKnown(configs: Map<String, PrinterConfigurationV3>, hasQuickSwitch: Boolean) = filter { service ->
        configs.none { it.value.webUrl == service.webUrl.toUrlOrNull() } || !hasQuickSwitch
    }

    private suspend fun createProbeFlow(webUrl: String, reusedInstanceId: String?, apiKey: String?) = flow {
        emit(State.Probe(urlString = webUrl, finding = null, reusedInstanceId = reusedInstanceId, apiKey = apiKey))
        val start = Clock.System.now()
        var resolvedWebUrl = webUrl
        var resolvedReuseInstanceId = reusedInstanceId
        var resolvedApiKey = apiKey

        // In case this is a magic pre-configuration URL used for app reviews, we need to fetch the actual config
        if (webUrl.toUrlOrNull()?.host == PreConfigurationHost) {
            try {
                val instance = fetchPreConfiguration(webUrl)
                resolvedWebUrl = instance.webUrl.toString()
                resolvedReuseInstanceId = instance.id
                resolvedApiKey = instance.apiKey
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed pre-configuration", throwable = e)
            }
        }

        val finding = try {
            testFullNetworkStackUseCase.execute(TestFullNetworkStackUseCase.Target.Printer(resolvedWebUrl, apiKey = resolvedApiKey ?: ""))
        } catch (e: Exception) {
            TestFullNetworkStackUseCase.Finding.UnexpectedIssue(webUrl = null, exception = e)
        }

        // Let the test take at least x time so the user sees we are very BUSY :D
        delay(mainProcessDuration - (Clock.System.now() - start))

        when (finding) {
            // If the API key is invalid, we passed. Move to access request
            is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> moveToState(
                State.AccessRequest(
                    url = finding.webUrl,
                    reusedInstanceId = resolvedReuseInstanceId,
                    loginUrl = null,
                    type = finding.type
                )
            )


            is TestFullNetworkStackUseCase.Finding.OctoPrintReady -> moveToState(
                State.Success(
                    config = PrinterConfigurationV3(
                        id = resolvedReuseInstanceId ?: uuid4().toString(),
                        webUrl = finding.webUrl,
                        apiKey = finding.apiKey,
                        type = BackendType.OctoPrint,
                    )
                )
            )

            is TestFullNetworkStackUseCase.Finding.MoonrakerReady -> emit(
                State.Success(
                    config = PrinterConfigurationV3(
                        id = resolvedReuseInstanceId ?: uuid4().toString(),
                        webUrl = finding.webUrl,
                        apiKey = finding.apiKey ?: "",
                        type = BackendType.Moonraker,
                    )
                )
            )

            else -> emit(
                State.Probe(
                    urlString = resolvedWebUrl,
                    finding = finding,
                    reusedInstanceId = resolvedReuseInstanceId,
                    apiKey = resolvedApiKey
                )
            )
        }
    }

    private suspend fun createSuccessFlow(state: State.Success) = flow {
        emit(state)

        try {
            // We just connect this flow so that the instance is already connected when the user continues
            // Also "share" it to generate a safe overlap to the normal UI so the event stream stays connected
            printerConfigRepository.add(state.config, trigger = "sign-in-success")
            var firstConnect = true
            val connectFlow = octoPrintProvider.eventFlow(tag = "sign-in-success", instanceId = state.config.id)
                .mapNotNull {
                    // Update instance capabilities once connected, do this in the AppScope as we do not want this process to cancel
                    if (it is Event.Connected && firstConnect) {
                        firstConnect = false
                        AppScope.launch {
                            try {
                                Napier.i(tag = tag, message = "Connected in success state, triggering capability update")
                                updateInstanceCapabilitiesUseCase.execute(UpdateInstanceCapabilitiesUseCase.Params(instanceId = state.config.id))
                            } catch (e: Exception) {
                                Napier.e(tag = tag, message = "Failed to do initial capability update", throwable = e)
                            }
                        }
                    }

                    // Drop all values (as mapNotNull)
                    null
                }
                .distinctUntilChanged()
                .shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay)
            emitAll(connectFlow)
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Exception while eagerly connecting", throwable = e)
        }
    }

    private suspend fun createAccessRequestFlow(webUrl: Url, reusedInstanceId: String?, type: BackendType) = flow {
        emit(State.AccessRequest(url = webUrl, reusedInstanceId = reusedInstanceId, loginUrl = null, type = type))

        if (type == BackendType.OctoPrint) {
            requestApiAccessUseCase.execute(
                param = RequestApiAccessUseCase.Params(webUrl = webUrl, type = type)
            ).mapNotNull { state ->
                val (emit, move) = when (state) {
                    is RequestApiAccessUseCase.State.AccessGranted -> null to State.Success(
                        config = PrinterConfigurationV3(
                            webUrl = webUrl,
                            id = reusedInstanceId ?: uuid4().toString(),
                            apiKey = state.apiKey,
                            type = type,
                        )
                    )

                    is RequestApiAccessUseCase.State.Failed -> null to State.AccessRequestFailed(
                        url = webUrl,
                        reusedInstanceId = reusedInstanceId,
                        exception = state.exception,
                        type = type,
                    )

                    is RequestApiAccessUseCase.State.Pending -> State.AccessRequest(
                        url = webUrl,
                        reusedInstanceId = reusedInstanceId,
                        loginUrl = state.authUrl?.toUrl(),
                        type = type,
                    ) to null
                }

                move?.let { moveToState(it) }
                emit
            }.onStart {
                Napier.i(tag = tag, message = "Starting request access flow")
            }.onCompletion {
                Napier.i(tag = tag, message = "Completing request access flow")
            }.also {
                emitAll(it)
            }
        } else {
            emitAll(flow { delay(Int.MAX_VALUE.days) })
        }
    }

    fun start(repairForInstanceId: String?) = if (repairForInstanceId != null) {
        printerConfigRepository.get(repairForInstanceId)?.let { config ->
            Napier.i(tag = tag, message = "Starting in repair mode for $repairForInstanceId")
            moveToState(State.Probe(urlString = config.webUrl.toString(), reusedInstanceId = repairForInstanceId, finding = null, apiKey = config.apiKey))
        } ?: let {
            val message = "Started in repair mode for $repairForInstanceId but instance was not found!"
            Napier.e(tag = tag, message = message, throwable = IllegalStateException(message))
            moveToState(State.Splash(splashUntil))
        }
    } else {
        moveToState(State.Splash(splashUntil))
    }

    fun activate(config: PrinterConfigurationV3) {
        // Try to upsert an existing instance in case we only updated the API key
        val new = printerConfigRepository.getAll().firstOrNull { it.webUrl == config.webUrl }?.copy(apiKey = config.apiKey) ?: config
        printerConfigRepository.setActive(new.copy(lastModifiedAt = Clock.System.now().toEpochMilliseconds()), trigger = "sign-in")
    }

    fun delete(configId: String) {
        printerConfigRepository.remove(configId)
    }

    fun moveToState(state: State) {
        runBlocking {
            sensitiveDataMask.registerWebUrl(state.urlString?.toUrlOrNull())
        }

        Napier.i(tag = tag, message = "Moving to state: $state")
        mutableState.update { (id, _) ->
            (id + 1) to state
        }
    }

    @OptIn(OctoPreferences.RawAccess::class)
    private suspend fun fetchPreConfiguration(url: String): PrinterConfigurationV3 {
        Napier.w(tag = tag, message = "Importing pre-configuration from $url")
        val json = HttpClient().get(url).bodyAsText()
        Napier.w(tag = tag, message = "Download $url: $json")
        val config: PreConfiguration = SharedCommonInjector.get().json.decodeFromString(json)
        Napier.w(tag = tag, message = "Parsed config, importing to printerConfigRepository")
        printerConfigRepository.importRaw(config.printerConfig)
        Napier.w(tag = tag, message = "Importing settings...")
        config.preferences.forEach {
            Napier.w(tag = tag, message = "  ${it.key} -> ${it.type}:${it.value}")
            when (it.type.lowercase()) {
                "boolean" -> octoPreferences.settings.putBoolean(it.key, it.value.toBoolean())
                "int" -> octoPreferences.settings.putInt(it.key, it.value.toInt())
                "long" -> octoPreferences.settings.putLong(it.key, it.value.toLong())
                "string" -> octoPreferences.settings.putString(it.key, it.value)
                "double" -> octoPreferences.settings.putDouble(it.key, it.value.toDouble())
                "float" -> octoPreferences.settings.putFloat(it.key, it.value.toFloat())
            }
        }
        Napier.w(tag = tag, message = "Pre-configuration import completed")
        val instance = requireNotNull(printerConfigRepository.get(config.activateInstanceId)) {
            "Supposed to enable ${config.activateInstanceId} but not available (available: ${printerConfigRepository.getAll().joinToString(",") { it.id }})"
        }
        Napier.w(tag = tag, message = "Verified instance ${config.activateInstanceId} for ${instance.webUrl}")
        return instance
    }

    @Serializable
    private data class PreConfiguration(
        val printerConfig: String,
        val activateInstanceId: String,
        val preferences: List<Preference>
    ) {
        @Serializable
        data class Preference(
            val key: String,
            val type: String,
            val value: String,
        )
    }

    private data class PreConfigurationResult(
        val webUrl: String,
        val apiKey: String,
        val instanceId: String
    )

    sealed class State(open val urlString: String?) {
        data object Initial : State(null)
        data class Splash(
            val until: Instant
        ) : State(null)

        data class Discover(
            val services: List<NetworkService>,
            val configs: List<PrinterConfigurationV3>,
            val hasQuickSwitch: Boolean
        ) : State(null)

        data class AccessRequest(
            val url: Url,
            val reusedInstanceId: String?,
            val loginUrl: Url?,
            val type: BackendType,
        ) : State(url.toString())

        data class AccessRequestFailed(
            val url: Url,
            val reusedInstanceId: String?,
            val exception: Throwable,
            val type: BackendType,
        ) : State(url.toString())

        data class Success(
            val config: PrinterConfigurationV3
        ) : State(null)

        data class Probe(
            override val urlString: String,
            val apiKey: String?,
            val reusedInstanceId: String?,
            val finding: TestFullNetworkStackUseCase.Finding?
        ) : State(null)
    }
}