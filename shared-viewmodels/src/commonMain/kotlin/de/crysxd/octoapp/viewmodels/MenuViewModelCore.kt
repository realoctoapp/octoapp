package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.printer.PrinterState
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class MenuViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val pinnedMenuItemRepository = SharedBaseInjector.getOrNull()?.pinnedMenuItemRepository

    val context = octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "menu-vm-1").map { current ->
        determineContextFromFlags(current.state.flags)
    }.distinctUntilChanged()

    suspend fun determineContext(): MenuContext {
        val flags = octoPrintProvider.passiveCachedMessageFlow(instanceId = instanceId, tag = "menu-vm-2", clazz = Message.Current::class).first()?.state?.flags
        return determineContextFromFlags(flags)
    }

    internal fun determineContextFromFlags(flags: PrinterState.Flags?) = when {
        flags == null -> MenuContext.Connect
        flags.isPrinting() -> if (flags.paused || flags.pausing) MenuContext.PrintPaused else MenuContext.Print
        flags.isOperational() -> MenuContext.Prepare
        else -> MenuContext.Connect
    }

    fun getPinnedItems(menuId: MenuId) = pinnedMenuItemRepository?.getPinnedMenuItems(menuId)

}