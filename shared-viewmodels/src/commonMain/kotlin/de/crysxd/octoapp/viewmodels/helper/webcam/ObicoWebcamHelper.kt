package de.crysxd.octoapp.viewmodels.helper.webcam

import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedexternalapis.mjpeg.JpegCoder
import de.crysxd.octoapp.sharedexternalapis.mjpeg.heightPx
import de.crysxd.octoapp.sharedexternalapis.mjpeg.widthPx
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import io.github.aakira.napier.Napier
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsChannel
import io.ktor.util.toByteArray
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.isActive
import kotlin.time.Duration.Companion.seconds

internal class ObicoWebcamHelper(private val core: BaseViewModelCore) {

    private val tag = "ObicoWebcamHelper"
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val httpSettings = SharedBaseInjector.get().httpClientSettings()

    suspend fun emit(
        collector: FlowCollector<State>,
        settings: ResolvedWebcamSettings.ObicoSettings,
        activeWebcam: Int,
        webcamCount: Int,
    ) {
        Napier.v(tag = "ObicoWebcamHelper", message = "Received settings: ${settings.webcamIndex}")

        val api = octoPrintProvider.printer(instanceId = core.instanceId).obicoApi
        val httpClient = createHttpClient()
        val jpegCoder = JpegCoder(usePool = false, logTag = "$tag/JPEG", maxImageSize = null)
        val delay = 10.seconds
        val reportedInterval = delay + 1.seconds
        var mediator: LowOverheadMediator<State.MjpegReady.Frame>? = null

        val nested = flow<State> {
            while (currentCoroutineContext().isActive) {
                Napier.v(tag = tag, message = "Preparing to load frame")

                val snapshotUrl = api.getObicoCamFrame(webcamIndex = activeWebcam).snapshot
                if (snapshotUrl == null) {
                    Napier.d(tag = tag, message = "No snapshot, emitting error")
                    collector.emit(
                        State.Error(
                            webcamCount = webcamCount,
                            activeWebcam = activeWebcam,
                            webcamUrl = null,
                            exception = ObicoNotWatchingPrinterException(),
                            canTroubleshoot = false,
                            canRetry = false,
                            canShowDetails = false,
                            displayName = settings.webcam.displayName,
                        )
                    )
                } else {
                    Napier.v(tag = tag, message = "Snapshot URL available, loading image")
                    val bytes = httpClient.get(snapshotUrl).bodyAsChannel().toByteArray()
                    val image = jpegCoder.decode(bytes, bytes.size)
                    val frame = State.MjpegReady.Frame(
                        image = image,
                        fps = 0f,
                        frameTime = 0,
                        frameInterval = reportedInterval.inWholeMilliseconds,
                    )

                    val m = mediator?.also { it.publishData(frame) } ?: LowOverheadMediator(frame)
                    mediator = m

                    Napier.v(tag = tag, message = "Snapshot loaded, emitting image")
                    collector.emit(
                        State.MjpegReady(
                            webcamCount = webcamCount,
                            activeWebcam = activeWebcam,
                            frames = m,
                            frameInterval = reportedInterval.inWholeMilliseconds,
                            rotate90 = settings.webcam.rotate90,
                            flipH = settings.webcam.flipH,
                            flipV = settings.webcam.flipV,
                            frameWidth = image.widthPx,
                            frameHeight = image.heightPx,
                            displayName = settings.webcam.displayName,
                        )
                    )
                }

                delay(delay)
            }
        }.retryWhen { cause, attempt ->
            Napier.e(tag = tag, message = "Failed to get snapshot", throwable = cause)
            attempt <= 2
        }

        collector.emitAll(nested)
    }

    private fun createHttpClient() = DefaultHttpClient(
        settings = httpSettings.copy(
            cache = null,
            logTag = "$tag/HTTP"
        ),
    )

    class ObicoNotWatchingPrinterException : SuppressedIllegalStateException("Obico not watching printer"), UserMessageException {
        override val userMessage = getString("configure_remote_acces___spaghetti_detective___webcam_not_watching")
    }
}