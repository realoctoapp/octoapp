package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class ControlCenterViewModelCore : BaseViewModelCore(instanceId = "") {

    private val tag = "ControlCenterViewModelCore"
    private val configRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val preferences = SharedBaseInjector.get().preferences
    private val getWebcamSnapshotUseCase = { SharedBaseInjector.get().getWebcamSnapshotUseCase() }

    private val baseState = combine(
        configRepository.allInstanceInformationFlow(),
        BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH),
        configRepository.instanceInformationFlow(),
        preferences.updatedFlow2.distinctUntilChangedBy { it.controlCenterSettings },
    ) { instances, enabled, active, _ ->
        State(
            enabled = enabled,
            activeInstanceId = active?.id,
            instances = instances.values.sorted().map { it.map() }
        )
    }.distinctUntilChanged().onEach {
        Napier.i(tag = tag, message = "ControlCenter state: $it")
    }

    private val snapshots = configRepository.allInstanceInformationFlow().flatMapLatest { all ->
        val interval = 5.seconds

        val flows = all.map { (instanceId, _) ->
            flow {
                val params = GetWebcamSnapshotUseCase.Params(
                    instanceId = instanceId,
                    interval = interval.inWholeMilliseconds,
                    maxSize = 256,
                    webcamIndex = 0,
                    illuminateIfPossible = true,
                )

                emit(instanceId to null)
                emitAll(getWebcamSnapshotUseCase().execute(params).map { image -> instanceId to image })
            }.distinctUntilChanged().retry { e ->
                delay(interval)
                Napier.e(tag = tag, message = "Failed to load snapshot for $instanceId", throwable = e)
                true
            }
        }

        combine(flows) { it.toMap() }
    }

    private val messages = configRepository.allInstanceInformationFlow().flatMapLatest { all ->
        val flows = all.map { (instanceId, _) ->
            flow {
                val last = octoPrintProvider.getLastCurrentMessage(instanceId)?.takeIf { last -> (Clock.System.now() - last.localTime) < 5.minutes }
                val nested = octoPrintProvider.eventFlow(
                    tag = "control-center-$instanceId",
                    instanceId = instanceId,
                    config = EventSource.Config(throttle = 6)
                ).mapNotNull { event ->
                    (event as? Event.MessageReceived)?.message as? Message.Current
                }

                emit(instanceId to last)
                emitAll(nested.map { message -> instanceId to message })
            }.distinctUntilChanged()
        }

        combine(flows) { it.toMap() }
    }

    val state = combine(baseState, snapshots, messages) { baseState, snpashots, messages ->
        baseState.copy(
            instances = baseState.instances.map { state ->
                val (status, eta) = messages[state.instanceId].status(state.label)
                state.copy(
                    snapshot = snpashots[state.instanceId]?.bitmap,
                    advertiseCompanion = state.advertiseCompanion,
                    progress = messages[state.instanceId]?.progressPercent,
                    status = status,
                    eta = eta
                )
            }
        )
    }.rateLimit(rateMs = 1000).distinctUntilChanged().onStart {
        Napier.i(tag = tag, message = "Control center active")
    }.onCompletion {
        Napier.i(tag = tag, message = "Control center inactive")
    }

    val stateSnapshot
        get() = State(
            enabled = BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH),
            activeInstanceId = configRepository.getActiveInstanceSnapshot()?.id,
            instances = configRepository.getAll().sorted().map { it.map() }
        )

    private val Message.Current?.progressPercent: Float?
        get() = this?.progress?.completion?.takeIf { this.state.flags.isPrinting() }

    private fun PrinterConfigurationV3.map() = ControlCenterInstance(
        label = label,
        instanceId = id,
        colors = colors,
    )

    private fun Message.Current?.status(instanceLabel: String): Pair<String, Long?> = if (this != null) {
        when {
            state.flags.isPrinting() -> (job?.file?.display ?: "???") to progress?.printTimeLeft
            state.flags.isOperational() -> getString("app_widget___idle", instanceLabel) to null
            else -> getString("app_widget___no_printer") to null
        }
    } else {
        getString("app_widget___no_data") to null
    }

    fun activate(instanceId: String) {
        configRepository.get(id = instanceId)?.let {
            configRepository.setActive(it, trigger = "ControlCenter")
        }
    }

    data class State(
        val instances: List<ControlCenterInstance>,
        val enabled: Boolean,
        val activeInstanceId: String?,
    )

    data class ControlCenterInstance(
        val label: String,
        val instanceId: String,
        val colors: Settings.Appearance.Colors,
        val progress: Float? = null,
        val status: String = "",
        val eta: Long? = null,
        val snapshot: Image? = null,
        val advertiseCompanion: Boolean = false
    )
}