package de.crysxd.octoapp.viewmodels.helper.startprint

enum class StartPrintResult {
    Loading, Failed, MaterialConfirmationRequired, TimelapseConfirmationRequired, Started
}