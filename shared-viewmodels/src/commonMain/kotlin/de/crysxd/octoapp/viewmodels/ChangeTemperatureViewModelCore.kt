package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.TemperatureOffset
import io.github.aakira.napier.Napier

class ChangeTemperatureViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "ChangeTemperatureViewModelCore"
    private val setTargetTemperaturesUseCase = SharedBaseInjector.get().setTargetTemperaturesUseCase()
    private val setTemperatureOffsetUseCase = SharedBaseInjector.get().setTemperatureOffsetUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    val supportsOffset get() = printerConfigRepository.get(instanceId)?.systemInfo?.capabilities?.contains(TemperatureOffset)

    suspend fun setTemperature(
        component: String,
        target: String,
        offset: String
    ) = try {
        Napier.i(tag = tag, message = "Changing temperature for $component to temp=$target offset=$offset")
        val targetFloat = target.ifBlank { "0" }.toFloat()
        val offsetFloat = offset.ifBlank { "0" }.toFloat()

        BaseChangeTemperaturesUseCase.Params(
            instanceId = instanceId,
            temp = BaseChangeTemperaturesUseCase.Temperature(
                temperature = targetFloat,
                component = component,
            )
        ).let { params ->
            setTargetTemperaturesUseCase.execute(params)
        }

        BaseChangeTemperaturesUseCase.Params(
            instanceId = instanceId,
            temp = BaseChangeTemperaturesUseCase.Temperature(
                temperature = offsetFloat,
                component = component,
            )
        ).let { params ->
            setTemperatureOffsetUseCase.execute(params)
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to apply temperatures", throwable = e)
        reportException(e)
    }
}