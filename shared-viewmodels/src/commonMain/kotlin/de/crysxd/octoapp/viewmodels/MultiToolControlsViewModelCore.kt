package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach

class MultiToolControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "MultiToolControlsViewModel"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider

    val initialState get() = printerConfigRepository.get(instanceId).getState()
    val state = printerConfigRepository.instanceInformationFlow(instanceId = instanceId)
        .map { it.getState() }
        .distinctUntilChanged()
        .onEach { state -> Napier.d(tag = tag, message = "state=$state") }
        .combine(octoPrintProvider.passiveCurrentMessageFlow(tag = "mutli-tool", instanceId = instanceId)) { state, current ->
            // While we are printing we hide the controls by only reporting one tool
            if (current.state.flags.isPrinting() && !current.state.flags.paused) {
                State(
                    activeToolIndex = 0,
                    toolCount = 1,
                    mmu = state.mmu,
                )
            } else {
                state
            }
        }
        .distinctUntilChanged()

    // Create the state by looking at the active tool in app settings and also the tool count
    // If the user has the MMU2 plugin installed, we ignore the actual tools count because the MMU does not
    // react to tool changes
    private fun PrinterConfigurationV3?.getState(): State {
        val mmu = this?.hasPlugin(OctoPlugins.Mmu2FilamentSelect) == true ||
                this?.hasPlugin(OctoPlugins.PrusaMmu) == true
        return State(
            activeToolIndex = this?.appSettings?.activeToolIndex,
            toolCount = this?.activeProfile?.extruderMotorComponents?.size ?: 1,
            mmu = mmu
        )
    }

    suspend fun setActiveTool(toolIndex: Int?) = try {
        Napier.i(tag = tag, message = "Activating tool $toolIndex")
        printerConfigRepository.updateAppSettings(id = instanceId) {
            it.copy(activeToolIndex = toolIndex)
        }
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failed to activate index", throwable = e)
    }

    @CommonParcelize
    data class State(
        val activeToolIndex: Int?,
        val toolCount: Int,
        val mmu: Boolean
    ) : CommonParcelable
}