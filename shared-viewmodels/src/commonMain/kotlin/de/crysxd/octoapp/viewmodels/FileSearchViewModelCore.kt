package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.searchInTree
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.base.usecase.LoadFileReferencesUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

@OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
class FileSearchViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val loadFilesUseCase = SharedBaseInjector.get().loadFileReferencesUseCase()

    private val loadTrigger = MutableStateFlow(LoadTrigger(id = 0, skipCache = false))
    private val searchTerm = MutableStateFlow("")
    val searchState = loadTrigger
        .filterNotNull()
        .flatMapLatest {
            loadFilesUseCase.execute(
                param = LoadFileReferencesUseCase.Params(
                    instanceId = instanceId,
                    fileOrigin = FileOrigin.Gcode,
                    skipCache = it.skipCache
                )
            )
        }.combine(searchTerm.debounce(200)) { file, term ->
            file to term
        }.map { (file, term) ->
            when (file) {
                is FlowState.Error -> FlowState.Error(file.throwable)
                is FlowState.Loading -> FlowState.Loading()
                is FlowState.Ready -> FlowState.Ready(
                    file.data.searchInTree(term).sorted().let { files ->
                        SearchResult(
                            files = files.mapNotNull { it as? FileReference.File },
                            folders = files.mapNotNull { it as? FileReference.Folder }
                        )
                    }
                )
            }
        }.catch {
            reportException(it)
            emit(FlowState.Error(it))
        }.flowOn(Dispatchers.Default)

    fun search(term: String, skipCache: Boolean) {
        if (skipCache) {
            loadTrigger.update { LoadTrigger(id = it.id + 1, skipCache = skipCache) }
        }

        searchTerm.value = term
    }

    data class SearchResult(
        val folders: List<FileReference.Folder>,
        val files: List<FileReference.File>,
    )

    private data class LoadTrigger(
        val id: Int,
        val skipCache: Boolean,
    )
}