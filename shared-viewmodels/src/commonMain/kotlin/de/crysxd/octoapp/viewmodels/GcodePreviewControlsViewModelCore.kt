package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource.LoadState.Failed
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource.LoadState.FailedLargeFileDownloadRequired
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource.LoadState.Loading
import de.crysxd.octoapp.base.data.source.GcodeFileDataSource.LoadState.Ready
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.gcode.GcodeRenderContextFactory
import de.crysxd.octoapp.base.models.GcodeMove
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.CommonTypeParceler
import de.crysxd.octoapp.sharedcommon.InstantParceler
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.viewmodels.ext.printBed
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class GcodePreviewControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "GcodePreviewControlsViewModelCore"
    private val octoPrintRepository = SharedBaseInjector.get().printerConfigRepository
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val octoPreferences = SharedBaseInjector.get().preferences
    private val gcodeFileRepository = SharedBaseInjector.get().gcodeFileRepository
    private var lastFile: FileReference.File? = null
    private var layerCount = 0

    init {
        Napier.i(tag = tag, message = "New instance: $instanceId")
    }

    //region Inputs
    private val retryFlow = MutableStateFlow(Instant.DISTANT_PAST)
    private val triggerFlow = MutableStateFlow<Trigger>(Trigger.Live())
    private val largeDownloadsAllowedUntilFlow = MutableStateFlow(Instant.DISTANT_PAST)

    //endregion
    //region Data sources
    // We already filter the current message here with minimal impact. If the trigger won't create a different outcome with the new message, no need to refresh.
    private val currentMessageFlow = octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "gcode-preview-1")
        .combine(triggerFlow) { x, y -> x to y }
        .distinctUntilChangedBy { (current, trigger) -> trigger.createResultId(current) }
        .map { (current, _) -> current }
    private val printingFlow = currentMessageFlow.map { it.state.flags.isPrinting() }.distinctUntilChanged()
    private val preferencesFlow = octoPreferences.updatedFlow.map { octoPreferences.gcodePreviewSettings }
    private val printerProfileFlow = octoPrintRepository.instanceInformationFlow(instanceId = instanceId).map { it?.activeProfile ?: PrinterProfile() }
    private val printBedFlow = octoPrintRepository.instanceInformationFlow(instanceId = instanceId).map { it.printBed }.distinctUntilChanged()
    private val featureEnabledFlow = BillingManager.isFeatureEnabledFlow(FEATURE_GCODE_PREVIEW)
        .distinctUntilChanged()
        .onEach { Napier.i(tag = tag, message = "[GCD] Enabled: $it") }

    //endregion
    //region State computation
    private val manualFileFlow = MutableStateFlow<FileReference.File?>(null)
    private val activeFileFlow = octoPrintProvider.passiveCurrentMessageFlow(instanceId = instanceId, tag = "gcode-preview-2")
        .map { it.job?.file }
        .combine(printingFlow) { file, printing -> if (printing) file else null }
        .combine(manualFileFlow) { auto, manual -> manual ?: auto }
        .distinctUntilChangedBy { it?.path }
        .onEach {
            Napier.i("[GCD] Active file: ${it?.path} (origin=${it?.origin})")
            lastFile = it
        }

    private val gcodeFlow = combine(activeFileFlow, featureEnabledFlow, largeDownloadsAllowedUntilFlow) { file, enabled, largeDownloadsAllowedUntil ->
        Napier.i(tag = tag, message = "[GCD] Trigger load: file=${file?.path} enabled=$enabled largeDownloadsAllowedUntil=$largeDownloadsAllowedUntil")
        flow {
            when {
                file == null -> emit(Loading(progress = 0f))
                !enabled -> emit(Failed(exception = FeatureDisabledException()))
                file.origin != FileOrigin.Gcode -> emit(Failed(exception = PrintingFromSdCardException(file = file)))
                else -> {
                    emit(Loading(0f))
                    emitAll(
                        gcodeFileRepository.loadFile(
                            file = file,
                            allowLargeFileDownloads = largeDownloadsAllowedUntil > Clock.System.now()
                        )
                    )
                }
            }
        }
    }.flatMapLatest { it }.onEach {
        if (it is Ready) {
            Napier.i(
                tag = tag,
                message = "[GCD] Loaded Gcode: layerCount=${it.gcode.layers.size} maxX=${it.gcode.maxX} minX=${it.gcode.minX} maxY=${it.gcode.maxY} minY=${it.gcode.minY}"
            )
        }
    }

    private val baseReadyStateFlow: Flow<State.DataReady> = combine(
        preferencesFlow,
        activeFileFlow,
        printBedFlow,
        printerProfileFlow,
    ) { preferences, _, printBed, printerProfile ->
        State.DataReady(
            renderContext = LowOverheadMediator(GcodeRenderContext.Empty),
            printBed = printBed,
            isTrackingPrintProgress = false,
            exceedsPrintArea = false,
            unsupportedGcode = false,
            settings = preferences,
            printerProfile = printerProfile
        )
    }

    private val gcodeViewStateFlowFactory = {
        combine(
            baseReadyStateFlow,
            gcodeFlow,
            currentMessageFlow,
            printerProfileFlow,
            triggerFlow,
        ) { readyState, gcode, current, printerProfile, trigger ->
            when (gcode) {
                is Loading -> State.Loading(gcode.progress)
                is FailedLargeFileDownloadRequired -> State.LargeFileDownloadRequired(gcode.downloadSize)
                is Failed -> gcode.exception.toViewState()
                is Ready -> {
                    layerCount = gcode.gcode.layers.size
                    val (factoryInstance, fromUser) = trigger.toRenderContextFactory(current)
                    val renderContext = factoryInstance.extractMoves(
                        gcode = gcode.gcode,
                        includePreviousLayer = readyState.settings.showPreviousLayer,
                        includeRemainingCurrentLayer = readyState.settings.showCurrentLayer,
                    )
                    readyState.renderContext.publishData(renderContext)
                    readyState.copy(
                        isTrackingPrintProgress = !fromUser,
                        exceedsPrintArea = exceedsPrintArea(printerProfile, renderContext),
                        unsupportedGcode = containsUnsupportedGcode(renderContext),
                    )
                }
            }
        }.retry(retries = 3) {
            Napier.e(tag = tag, message = "Failed to combined Gcode state", throwable = it)
            it !is FeatureDisabledException && it !is PrintingFromSdCardException
        }.filterNotNull().distinctUntilChanged()
    }

    val state: Flow<State> = retryFlow.flatMapLatest { _ ->
        combine(featureEnabledFlow, printBedFlow) { enabled, printBed ->
            when {
                !enabled -> flowOf(State.FeatureDisabled(printBed = printBed))
                else -> gcodeViewStateFlowFactory()
            }
        }.flatMapLatest {
            it
        }.catch {
            Napier.e(tag = tag, message = "Caught exception in Gcode flow", throwable = it)
            emit(it.toViewState())
        }
    }.flowOn(
        Dispatchers.Default
    ).onStart {
        Napier.i(tag = tag, message = "[GCD] Starting shared Gcode flow")
    }.onCompletion {
        Napier.i(tag = tag, message = "[GCD] Stopping shared Gcode flow")
    }.shareIn(
        scope = AppScope,
        started = SharingStarted.WhileSubscribedOctoDelay,
        replay = 1
    ).onStart {
        Napier.i(tag = tag, message = "[GCD] Starting Gcode flow")
        emit(State.Loading())
    }.onCompletion {
        Napier.i(tag = tag, message = "[GCD] Stopping Gcode flow")
    }

    //endregion
    //region Unsupported Gcode
    private fun containsUnsupportedGcode(context: GcodeRenderContext?) = listOfNotNull(
        context?.completedLayerPaths,
        context?.previousLayerPaths,
        context?.remainingLayerPaths
    ).any {
        it.any { path -> path.type == GcodeMove.Type.Unsupported && path.moveCount > 0 }
    }

    //endregion
    //region Exceeds print area
    private fun exceedsPrintArea(
        printerProfile: PrinterProfile?,
        context: GcodeRenderContext?,
    ): Boolean {
        context ?: return false
        printerProfile ?: return false
        val bounds = context.gcodeBounds
        bounds ?: return false
        val w = printerProfile.volume.width
        val h = printerProfile.volume.depth

        // Give 5% slack to make sure we don't count homing positions as out of bounds
        val graceDistance = minOf(w, h) * 0.05f

        val minX: Float
        val minY: Float
        val maxX: Float
        val maxY: Float

        when (printerProfile.volume.origin) {
            PrinterProfile.Origin.LowerLeft -> {
                minX = -graceDistance
                maxX = w + graceDistance
                maxY = h + graceDistance
                minY = -graceDistance
            }

            PrinterProfile.Origin.Center -> {
                maxX = (w / 2) + graceDistance
                minX = -maxX
                maxY = (h / 2) + graceDistance
                minY = -maxY
            }
        }

        return bounds.left < minX || bounds.top > maxY || bounds.right > maxX || bounds.bottom < minY
    }

    //endregion

    fun useLive() {
        Napier.d(tag = tag, message = "Use live progress")
        manualFileFlow.value = null
        triggerFlow.value = Trigger.Live()
    }

    fun useManualInLiveFile(
        layerIndex: Int,
        layerProgress: Float
    ) {
        val file = lastFile ?: return
        useManual(
            path = file.path,
            origin = file.origin,
            size = file.size,
            date = file.date,
            name = file.name,
            layerIndex = layerIndex,
            layerProgress = layerProgress,
        )
    }

    fun useManual(
        path: String,
        origin: FileOrigin,
        date: Instant,
        size: Long?,
        name: String,
        layerIndex: Int,
        layerProgress: Float
    ) {
        Napier.d(tag = tag, message = "Use manual progress: $path @ $layerIndex/${layerProgress * 100}%")
        manualFileFlow.value = SimpleReference(
            path = path,
            origin = origin,
            display = name,
            date = date,
            size = size,
        )

        triggerFlow.value = Trigger.Manual(
            layerIndex = if (layerCount > 0) layerIndex.coerceIn(0, layerCount - 1) else 0,
            layerProgress = layerProgress
        )
    }

    fun allowLargeDownloads() {
        largeDownloadsAllowedUntilFlow.value = Clock.System.now() + 10.seconds
    }

    fun retry() {
        retryFlow.value = Clock.System.now()
    }

    private suspend fun Throwable.toViewState() = when (this) {
        is FeatureDisabledException -> State.FeatureDisabled(printBed = printBedFlow.first())
        is PrintingFromSdCardException -> State.PrintingFromSdCard(file = file)
        else -> State.Error(this)
    }

    //region Internal classes
    private class FeatureDisabledException : SuppressedIllegalStateException("Feature disabled")
    private class PrintingFromSdCardException(val file: FileReference) : SuppressedIllegalStateException("Printing from SD card")

    private sealed class Trigger {

        companion object {
            private val random = Random(Clock.System.now().epochSeconds)
        }

        abstract fun toRenderContextFactory(currentMessage: Message.Current): Pair<GcodeRenderContextFactory, Boolean>

        abstract fun createResultId(currentMessage: Message.Current): Int

        data class Live(private val seed: Double = random.nextDouble()) : Trigger() {
            override fun toRenderContextFactory(currentMessage: Message.Current) =
                GcodeRenderContextFactory.ForFileLocation(currentMessage.progress?.filepos?.toInt() ?: Int.MAX_VALUE) to false

            override fun createResultId(currentMessage: Message.Current) =
                currentMessage.progress?.filepos?.toInt() ?: -1
        }

        data class Manual(private val layerIndex: Int, private val layerProgress: Float) : Trigger() {
            override fun toRenderContextFactory(currentMessage: Message.Current) =
                GcodeRenderContextFactory.ForLayerProgress(layerIndex = layerIndex, progress = layerProgress) to true

            override fun createResultId(currentMessage: Message.Current) =
                "$layerIndex:$layerProgress".hashCode()
        }
    }
    //endregion


    @CommonParcelize
    private data class SimpleReference(
        override val path: String,
        override val display: String,
        override val origin: FileOrigin,
        override val size: Long? = null,
        override val id: String = path,
        @CommonTypeParceler<Instant, InstantParceler>() override val date: Instant
    ) : FileReference.File

    sealed class State {
        data class Loading(val progress: Float = 0f) : State()
        data class FeatureDisabled(val printBed: GcodeNativeCanvas.Image) : State()
        data class LargeFileDownloadRequired(val downloadSize: Long) : State()
        data class Error(val exception: Throwable) : State()
        data class PrintingFromSdCard(val file: FileReference) : State()
        data class DataReady(
            val printBed: GcodeNativeCanvas.Image,
            val printerProfile: PrinterProfile? = null,
            val settings: GcodePreviewSettings,
            val exceedsPrintArea: Boolean = false,
            val unsupportedGcode: Boolean = false,
            val isTrackingPrintProgress: Boolean = false,
            val renderContext: LowOverheadMediator<GcodeRenderContext>,
        ) : State()
    }
}