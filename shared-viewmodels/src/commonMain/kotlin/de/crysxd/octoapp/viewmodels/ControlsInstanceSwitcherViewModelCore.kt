package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.di.SharedBaseInjector
import kotlinx.coroutines.flow.combine

class ControlsInstanceSwitcherViewModelCore : BaseViewModelCore(instanceId = "") {

    private val configRepository = SharedBaseInjector.get().printerConfigRepository

    val state = combine(
        BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH),
        configRepository.allInstanceInformationFlow()
    ) { enabled, instances ->
        State(shown = enabled && instances.size > 1)
    }

    data class State(
        val shown: Boolean
    )
}