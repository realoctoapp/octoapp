package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.sharedcommon.url.isNgrokUrl
import io.ktor.http.Url
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

class RemoteAccessNgrokViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    val supportsAutoConnect = SharedBaseInjector.get().printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map { instance ->
        instance?.systemInfo?.interfaceType == SystemInfo.Interface.OctoPrint
    }

    override val tag = "RemoteAccessNgrokViewModelCore"
    override val remoteServiceName = RemoteServiceConnectionBrokenException.REMOTE_SERVICE_NGROK
    override val loadingFlow = flowOf(false)
    override fun Url.isMyUrl() = isNgrokUrl()
    override fun modifyUrl(url: Url) = url.withBasicAuth(user = "xxx", password = "xxx")
}