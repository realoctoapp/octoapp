package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.usecase.RefreshBedMeshUseCase
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.viewmodels.ext.printBed
import de.crysxd.octoapp.viewmodels.helper.bedmesh.BedMeshParser
import de.crysxd.octoapp.viewmodels.helper.bedmesh.ParsedBedMesh
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlin.time.Duration.Companion.seconds

class BedMeshControlsViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "BedMeshControlsViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val refreshBedMeshUseCase = SharedBaseInjector.get().refreshBedMeshUseCase()
    private val parser = BedMeshParser()

    val state = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).distinctUntilChangedBy {
        it?.settings?.bedMesh
    }.map { info ->
        info.toState()
    }.flowOn(Dispatchers.Default)

    private fun PrinterConfigurationV3?.toState(): State {
        val bedLevel = this?.settings?.bedMesh ?: return State.Hidden
        val profile = activeProfile ?: PrinterProfile()

        val notAvailable = State.NotAvailable(
            printBed = printBed,
            printerProfile = profile,
        )

        return try {
            State.Available(
                printBed = notAvailable.printBed,
                printerProfile = profile,
                hasProfiles = bedLevel.profiles.size > 1,
                mesh = parser.parse(
                    mesh = bedLevel.mesh ?: return notAvailable,
                    colorScale = bedLevel.colorScale,
                    meshY = bedLevel.meshY ?: return notAvailable,
                    meshX = bedLevel.meshX ?: return notAvailable,
                    graphZMax = bedLevel.graphZMax,
                    graphZMin = bedLevel.graphZMin,
                    bedMinX = profile.volume.boundingBox.xMin,
                    bedMaxX = profile.volume.boundingBox.xMax,
                    bedMinY = profile.volume.boundingBox.yMin,
                    bedMaxY = profile.volume.boundingBox.yMax,
                )
            )
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to create state (mesh=${bedLevel.mesh} meshX=${bedLevel.meshX} meshY=${bedLevel.meshY})", throwable = e)
            notAvailable
        }
    }

    suspend fun refresh() = try {
        refreshBedMeshUseCase.execute(RefreshBedMeshUseCase.Param(instanceId = instanceId))

        // Gcode executed async, let's keep the loading indicator active a "bit" more
        // as the actual process will take much longer
        delay(30.seconds)
    } catch (e: Exception) {
        reportException(e)
        Napier.e(tag = tag, message = "Failed to refresh mesh", throwable = e)
    }

    sealed class State {

        val settings: GcodePreviewSettings = GcodePreviewSettings()
        abstract val printBed: GcodeNativeCanvas.Image
        abstract val printerProfile: PrinterProfile

        data object Hidden : State() {
            override val printBed = GcodeNativeCanvas.Image.PrintBedGeneric
            override val printerProfile = PrinterProfile()
        }

        data class NotAvailable(
            override val printBed: GcodeNativeCanvas.Image,
            override val printerProfile: PrinterProfile,
        ) : State()

        data class Available(
            override val printBed: GcodeNativeCanvas.Image,
            val mesh: ParsedBedMesh,
            val hasProfiles: Boolean,
            override val printerProfile: PrinterProfile,
        ) : State()
    }
}