package de.crysxd.octoapp.viewmodels

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.OctolapsePluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.sharedcommon.utils.parseHtml
import de.crysxd.octoapp.viewmodels.ext.printBed
import de.crysxd.octoapp.viewmodels.helper.octolapse.OctolapseGcodeRenderPlugin
import io.github.aakira.napier.Napier
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

class OctolapseSupportViewModelCore(instanceId: String) : BaseViewModelCore(instanceId) {

    private val tag = "OctolapseSupportViewModel"
    private val octoPrintProvider = SharedBaseInjector.get().printerEngineProvider
    private val octoPreferences = SharedBaseInjector.get().preferences
    private val octoPrintRepository = SharedBaseInjector.get().printerConfigRepository
    private var activeJobId: String? = null

    private val baseSnapshotPlan = combine(
        octoPrintRepository.instanceInformationFlow(instanceId = instanceId),
        octoPreferences.updatedFlow2.map { it.gcodePreviewSettings }
    ) { instance, settings ->
        State.SnapshotPlanPreview(
            plan = OctolapseSnapshotPlanPreview(),
            printBed = instance.printBed,
            printBedWidthMm = instance?.activeProfile?.volume?.width ?: 200f,
            printBedHeightMm = instance?.activeProfile?.volume?.depth ?: 200f,
            originInCenter = instance?.activeProfile?.volume?.origin == PrinterProfile.Origin.Center,
            renderer = OctolapseGcodeRenderPlugin(OctolapseSnapshotPlanPreview()),
            layerCount = 0,
            layerCountDisplay = settings.layerCountDisplay,
            layerNumberDisplay = settings.layerNumberDisplay,
        )
    }

    private val messages = combine(
        baseSnapshotPlan,
        octoPrintProvider.passiveCachedMessageFlow(instanceId = instanceId, tag = "octolapse-messages", clazz = OctolapsePluginMessage::class).filterNotNull()
    ) { baseSnapshotPlan, message ->
        val error = message.errors?.firstOrNull()
        val preview = message.snapshotPlanPreview

        if (error != null) {
            Napier.i(tag = tag, message = "Received error: ${error.name}")
            State.ShowMessage(
                title = error.name ?: "Unknown error",
                message = error.description?.parseHtml() ?: getString("error_general"),
            )
        } else if (preview != null) {
            Napier.i(tag = tag, message = "Received snapshot plan preview")
            activeJobId = preview.jobId
            baseSnapshotPlan.copyWithPreview(preview)
        } else {
            Napier.i(tag = tag, message = "Received message: ${message.type?.toString() ?: "other"}")
            when (message.type) {
                OctolapsePluginMessage.Type.GcodePreProcessingStart -> State.Loading(getString("octolapse___snapshot_plan___title_analyzing"))
                OctolapsePluginMessage.Type.GcodePreProcessingUpdate -> State.Loading(getString("octolapse___snapshot_plan___title_analyzing"))
                OctolapsePluginMessage.Type.SnapshotPlanComplete -> State.Idle
                OctolapsePluginMessage.Type.TimelapseStart -> State.Idle
                null -> null
            }
        }
    }.filterNotNull()

    private val connection = combine(
        baseSnapshotPlan,
        octoPrintProvider.passiveConnectionEventFlow(instanceId = instanceId, tag = "octolapse-connection")
    ) { baseSnapshotPlan, connection ->
        baseSnapshotPlan to connection
    }.buffer(onBufferOverflow = BufferOverflow.DROP_OLDEST).map { (baseSnapshotPlan, connection) ->
        try {
            val connectionTime = connection?.localTime ?: Instant.DISTANT_PAST
            val timesSinceConnection = Clock.System.now() - connectionTime

            when {
                !octoPreferences.activePluginIntegrations -> {
                    Napier.i(tag = tag, message = "Connected but skipping Octolapse state check, no active plugin integrations allowed")
                    null
                }

                octoPrintRepository.getActiveInstanceSnapshot()?.hasPlugin(OctoPlugins.Octolapse) != true -> {
                    Napier.i(tag = tag, message = "Octolapse not installed")
                    null
                }

                timesSinceConnection < 3.seconds -> {
                    // Delay slightly so other more important requests go first
                    val delay = 3.seconds
                    Napier.i(tag = tag, message = "Connected, checking current Octolapse state after $delay")
                    delay(delay)
                    octoPrintProvider.printer().asOctoPrint().octolapseApi.getSettingsAndState().snapshotPlanPreview?.let {
                        activeJobId = it.jobId
                        Napier.i(tag = tag, message = "Snapshot plan active")
                        baseSnapshotPlan.copyWithPreview(it)
                    } ?: let {
                        Napier.i(tag = tag, message = "No snapshot plan active")
                        State.Idle
                    }
                }

                else -> null
            }
        } catch (e: Exception) {
            // Nice try, we don't care. This is considered optional
            Napier.e(tag = tag, throwable = e, message = "Octolapse  state check failed")
            null
        }
    }.filterNotNull()

    val state = merge(
        messages,
        connection
    ).retry(3) { e ->
        Napier.e(tag = tag, throwable = e, message = "Octolapse flow failed")
        delay(5.seconds)
        true
    }.catch { e ->
        Napier.e(tag = tag, throwable = e, message = "Octolapse flow failed terminally")
    }.onStart {
        emit(State.Idle)
    }.shareIn(AppScope, started = SharingStarted.WhileSubscribedOctoDelay, replay = 1)

    suspend fun acceptSnapshotPlan() = try {
        Napier.i(tag = tag, message = "Accepting snapshot plan")
        val jobId = requireNotNull(activeJobId) { "No render job ID available" }
        octoPrintProvider.printer(instanceId = instanceId).asOctoPrint().octolapseApi.acceptSnapshotPlan(jobId)
        activeJobId = null
        true
    } catch (e: Exception) {
        Napier.e(throwable = e, tag = tag, message = "Failed to accept snapshot plan")
        reportException(e)
        false
    }

    suspend fun declineSnapshotPlan(): Boolean = try {
        Napier.i(tag = tag, message = "Declining snapshot plan")
        val jobId = requireNotNull(activeJobId) { "No render job ID available" }
        octoPrintProvider.printer(instanceId = instanceId).asOctoPrint().octolapseApi.cancelPreprocessing(jobId)
        activeJobId = null
        true
    } catch (e: Exception) {
        Napier.e(throwable = e, tag = tag, message = "Failed to cancel snapshot plan")
        reportException(e)
        false
    }

    private fun State.SnapshotPlanPreview.copyWithPreview(plan: OctolapseSnapshotPlanPreview) = copy(
        plan = plan,
        renderer = OctolapseGcodeRenderPlugin(plan),
        layerCount = plan.snapshotPlans?.snapshotPlans?.size ?: 0,
        printBedWidthMm = plan.snapshotPlans?.volume?.width ?: printBedWidthMm,
        printBedHeightMm = plan.snapshotPlans?.volume?.height ?: printBedHeightMm,
        originInCenter = plan.snapshotPlans?.volume?.origin?.let { it == OctolapseSnapshotPlanPreview.Origin.Center } ?: originInCenter,
    )

    sealed class State {
        // Just a safty feature to not
        var handled: Boolean = false
        abstract val receivedAt: Long

        data object Idle : State() {
            override val receivedAt = 0L
        }

        data class Loading(
            val title: String,
            override val receivedAt: Long = Clock.System.now().toEpochMilliseconds()
        ) : State()

        data class ShowMessage(
            val message: CharSequence,
            val title: String,
            override val receivedAt: Long = Clock.System.now().toEpochMilliseconds()
        ) : State()

        data class SnapshotPlanPreview(
            val plan: OctolapseSnapshotPlanPreview,
            val renderer: OctolapseGcodeRenderPlugin,
            val printBed: GcodeNativeCanvas.Image,
            val printBedWidthMm: Float,
            val printBedHeightMm: Float,
            val originInCenter: Boolean,
            val layerCount: Int,
            val layerCountDisplay: (Int) -> Int,
            val layerNumberDisplay: (Int) -> Int,
            override val receivedAt: Long = Clock.System.now().toEpochMilliseconds()
        ) : State()
    }
}