package de.crysxd.octoapp.viewmodels.ext

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas

private val printBedCache = mutableMapOf<Int, GcodeNativeCanvas.Image>()
val PrinterConfigurationV3?.printBed: GcodeNativeCanvas.Image
    get() {
        // Combine info to one string
        val m115 = this?.m115Response ?: ""
        val profileName = this?.activeProfile?.name ?: ""
        val profileModel = this?.activeProfile?.model ?: ""
        val firmwareName = this?.systemInfo?.printerFirmware ?: ""

        // Info in priority order (first = highest)
        val info = listOf(firmwareName, profileName, profileModel, m115).joinToString()

        // Set background based on machine type (somewhere in info)
        return printBedCache.getOrPut(info.hashCode()) {
            when {
                info.contains("charlotte", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("skr-mini-e3", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("skr_mini_e3", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("ender", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("e3-", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("e3v2-", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("e5-", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedEnder
                info.contains("creality", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedCreality
                info.contains("cr-", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedCreality
                info.contains("prusa", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedPrusa
                info.contains("anycubic", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedAnycubic
                info.contains("artillery", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedArtillery
                info.contains("sidewinder", ignoreCase = true) -> GcodeNativeCanvas.Image.PrintBedArtillery
                else -> GcodeNativeCanvas.Image.PrintBedGeneric
            }
        }
    }