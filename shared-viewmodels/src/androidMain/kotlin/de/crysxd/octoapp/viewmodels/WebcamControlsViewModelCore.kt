package de.crysxd.octoapp.viewmodels

import android.webkit.WebView
import androidx.media3.exoplayer.ExoPlayer
import de.crysxd.octoapp.base.utils.FlowState
import kotlinx.coroutines.flow.Flow

actual typealias VideoPlayer = ExoPlayer

actual data class WebContent(
    val webView: WebView,
    val state: Flow<FlowState<PlaybackData>>,
) {
    data class PlaybackData(
        val width: Int,
        val height: Int
    )
}