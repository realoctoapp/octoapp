package de.crysxd.octoapp.viewmodels.helper.webcam

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.MotionEvent
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebView.setWebContentsDebuggingEnabled
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_HLS_WEBCAM
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.utils.getString
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import de.crysxd.octoapp.viewmodels.BuildConfig
import de.crysxd.octoapp.viewmodels.WebContent
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import io.github.aakira.napier.Napier
import io.ktor.http.URLBuilder
import io.ktor.http.path
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import org.json.JSONObject
import java.io.IOException


@OptIn(ExperimentalCoroutinesApi::class)
internal actual class WebPageWebcamHelper actual constructor(core: BaseViewModelCore) {

    private val tag = "WebRtcWebcamHelper"

    actual suspend fun emit(
        collector: FlowCollector<WebcamControlsViewModelCore.State>,
        settings: ResolvedWebcamSettings.WebPageWebcamSettings,
        activeWebcam: Int,
        webcamCount: Int
    ) {
        val content by lazy { createWebContent() }

        val featureEnabledFlow = BillingManager.isFeatureEnabledFlow(FEATURE_HLS_WEBCAM)
        featureEnabledFlow.flatMapLatest { featureEnabled ->
            flow {
                val state = if (featureEnabled) {
                    Napier.i(tag = tag, message = "WebRTC streams enabled, constructing player")
                    WebcamControlsViewModelCore.State.WebpageReady(
                        webcamCount = webcamCount,
                        flipV = settings.webcam.flipV,
                        flipH = settings.webcam.flipH,
                        rotate90 = settings.webcam.rotate90,
                        displayName = settings.webcam.displayName,
                        webcamUrl = settings.url.toString(),
                        activeWebcam = activeWebcam,
                        warning = null,
                        content = content,
                    )
                } else {
                    WebcamControlsViewModelCore.State.FeatureNotAvailable(
                        webcamCount = webcamCount,
                        activeWebcam = activeWebcam,
                        webcamUrl = settings.url.toString(),
                        featureName = "WebRTC",
                        displayName = settings.webcam.displayName,
                    )
                }

                emit(state)
            }
        }.onEach { state ->
            if (state is WebcamControlsViewModelCore.State.WebpageReady) {
                // Setup player
                Napier.i(tag = tag, message = "Settings up WebView: ${state.content}")
                content.webView.loadDataWithBaseURL(
                    URLBuilder(settings.url).apply { path() }.buildString(),
                    WebPageWebcamHelperCore.constructHtml(settings),
                    "text/html",
                    "UTF-8",
                    null,
                )
            }
        }.onCompletion {
            Napier.i(tag = tag, message = "Stopping WebRTC playback")
            content.webView.destroy()
        }.flowOn(
            context = Dispatchers.Main
        ).collect { state ->
            collector.emit(state)
        }
    }

    @SuppressLint("SetJavaScriptEnabled", "ClickableViewAccessibility")
    private fun createWebContent(): WebContent {
        val context = SharedCommonInjector.get().platform.context
        val stateFlow = MutableStateFlow<FlowState<WebContent.PlaybackData>>(FlowState.Loading())
        val webView = object : WebView(context) {
            @SuppressLint("ClickableViewAccessibility")
            override fun onTouchEvent(event: MotionEvent?): Boolean {
                return false
            }
        }.apply {
            if (BuildConfig.DEBUG) {
                setWebContentsDebuggingEnabled(true)
            }

            settings.run {
                javaScriptEnabled = true
                mediaPlaybackRequiresUserGesture = false
                setSupportZoom(false)
            }

            setBackgroundColor(Color.BLACK)
            addJavascriptInterface(object {
                @JavascriptInterface
                fun log(level: String, message: String): Boolean {
                    when (level.lowercase()) {
                        "i" -> Napier.i(tag = this@WebPageWebcamHelper.tag, message = message)
                        "w" -> Napier.w(tag = this@WebPageWebcamHelper.tag, message = message)
                        "d" -> Napier.d(tag = this@WebPageWebcamHelper.tag, message = message)
                        "v" -> Napier.v(tag = this@WebPageWebcamHelper.tag, message = message)
                        else -> Napier.e(tag = this@WebPageWebcamHelper.tag, message = message)
                    }

                    return true
                }

                @JavascriptInterface
                fun moveToState(stateJson: String): Boolean {
                    val json = JSONObject(stateJson)
                    val state = json.getString("state")
                    val width = if (json.has("width")) json.getInt("width") else 0
                    val height = if (json.has("height")) json.getInt("height") else 0
                    val error = if (json.has("error")) json.getString("error") else getString("error_general")
                    val errorMessage = if (json.has("errorMsg")) json.getString("errorMsg") else getString("error_general")

                    stateFlow.value = when (state) {
                        "loading" -> FlowState.Loading()
                        "playing" -> FlowState.Ready(WebContent.PlaybackData(width = width, height = height))
                        "error" -> FlowState.Error(WebRtcException(message = error, userMessage = errorMessage))
                        else -> FlowState.Error(IllegalStateException("Unsupported state $state"))
                    }

                    return true
                }
            }, "octoapp")
        }

        return WebContent(
            webView = webView,
            state = stateFlow
        )
    }

    private class WebRtcException(
        message: String,
        override val userMessage: String,
    ) : IOException(message), UserMessageException
}