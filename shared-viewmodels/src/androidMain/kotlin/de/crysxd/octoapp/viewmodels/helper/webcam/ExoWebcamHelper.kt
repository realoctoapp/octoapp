package de.crysxd.octoapp.viewmodels.helper.webcam

import android.net.Uri
import androidx.annotation.OptIn
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_HLS_WEBCAM
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach

internal class ExoWebcamHelper {

    private val tag = "ExoWebcamHelper"

    @OptIn(UnstableApi::class)
    @kotlin.OptIn(ExperimentalCoroutinesApi::class)
    suspend fun emit(
        collector: FlowCollector<WebcamControlsViewModelCore.State>,
        webcam: WebcamSettings.Webcam,
        authHeader: String?,
        url: String,
        activeWebcam: Int,
        webcamCount: Int,
        featureName: String,
    ) {
        var player: Player? = null
        val featureEnabledFlow = BillingManager.isFeatureEnabledFlow(FEATURE_HLS_WEBCAM)
        featureEnabledFlow.flatMapLatest { featureEnabled ->
            flow {
                val state = if (featureEnabled) {
                    Napier.i(tag = tag, message = "HLS/RTSP streams enabled, constructing player")
                    val localPlayer = ExoPlayer.Builder(SharedCommonInjector.get().platform.context).build().also {
                        player = it
                    }
                    WebcamControlsViewModelCore.State.VideoReady(
                        uri = url,
                        player = localPlayer,
                        webcamCount = webcamCount,
                        authHeader = authHeader,
                        flipV = webcam.flipV,
                        flipH = webcam.flipH,
                        rotate90 = webcam.rotate90,
                        displayName = webcam.displayName,
                        activeWebcam = activeWebcam,
                        warning = null,
                    )
                } else {
                    WebcamControlsViewModelCore.State.FeatureNotAvailable(
                        webcamCount = webcamCount,
                        activeWebcam = activeWebcam,
                        webcamUrl = url,
                        featureName = featureName,
                        displayName = webcam.displayName,
                    )
                }

                emit(state)
            }
        }.onEach { state ->
            if (state is WebcamControlsViewModelCore.State.VideoReady) {
                // Setup player
                Napier.i(tag = tag, message = "Starting Exo player: ${state.uri}")
                val mediaItem = MediaItem.fromUri(Uri.parse(state.uri))
                val dataSourceFactory = DefaultHttpDataSource.Factory()
                state.authHeader?.let {
                    dataSourceFactory.setDefaultRequestProperties(mapOf("Authorization" to it))
                }
                val mediaSourceFactory = DefaultMediaSourceFactory(dataSourceFactory)
                state.player.apply {
                    setMediaSource(mediaSourceFactory.createMediaSource(mediaItem))
                    prepare()
                    play()
                }
            }
        }.onCompletion {
            Napier.i(tag = tag, message = "Stopping video playback")
            player?.stop()
            player?.release()
            player = null
        }.flowOn(
            context = Dispatchers.Main
        ).collect { state ->
            collector.emit(state)
        }
    }
}