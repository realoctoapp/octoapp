import de.crysxd.octoapp.buildscript.octoAppMultiplatformLibrary

plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.kotlinSerialization)
}

kotlin {
    androidTarget()
    iosX64()
    iosArm64()
    iosSimulatorArm64()

    sourceSets {
        commonMain.dependencies {
            implementation(projects.sharedBase)
        }
        commonTest.dependencies {
            implementation(kotlin("test"))
        }
        androidMain.dependencies {
            api(libs.exoplayer)
            api(libs.exoplayer.ui)
            api(libs.exoplayer.hls)
            api(libs.exoplayer.rtsp)
        }
    }
    //region Opt-in to expect/actual + Kotlin Parcelize
    targets.configureEach {
        compilations.configureEach {
            compileTaskProvider.configure {
                compilerOptions {
                    freeCompilerArgs.addAll(
                        "-opt-in=kotlin.ExperimentalUnsignedTypes,kotlin.RequiresOptIn",
                        "-Xexpect-actual-classes"
                    )
                    freeCompilerArgs.addAll(
                        "-P",
                        "plugin:org.jetbrains.kotlin.parcelize:additionalAnnotation=de.crysxd.octoapp.sharedcommon.CommonParcelize"
                    )
                }
            }
        }
    }
    //endregion
}

octoAppMultiplatformLibrary("viewmodels")