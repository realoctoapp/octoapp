import de.crysxd.octoapp.buildscript.octoAppProject

plugins {
    alias(libs.plugins.androidApplication).apply(false)
    alias(libs.plugins.androidLibrary).apply(false)
    alias(libs.plugins.androidTest).apply(false)
    alias(libs.plugins.kotlinAndroid).apply(false)
    alias(libs.plugins.kotlinMultiplatform).apply(false)
    alias(libs.plugins.kotlinParcelize).apply(false)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinComposeCompiler).apply(false)
    alias(libs.plugins.androidxSafeArgs).apply(false)
    alias(libs.plugins.kotlinKapt).apply(false)
    alias(libs.plugins.googleServices).apply(false)
    alias(libs.plugins.playPublisher).apply(false)
}

octoAppProject {
    minSdk = 27
    compileSdk = 34
    basePackage = "de.crysxd.octoapp"
}
