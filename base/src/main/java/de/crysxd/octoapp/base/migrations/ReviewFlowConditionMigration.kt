package de.crysxd.octoapp.base.migrations

import androidx.core.content.edit
import com.google.gson.Gson
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import kotlinx.datetime.Instant

class ReviewFlowConditionMigration {

    companion object {
        private const val KEY_CONDITIONS = "app_review_trigger_conditions"
    }

    private val tag = "ReviewFlowConditionMigration"

    fun migrate() {
        loadConditions()?.let {
            Napier.i(tag = tag, message = "ReviewFlowConditionMigration running...")
            SharedBaseInjector.get().preferences.reviewFlowConditions = de.crysxd.octoapp.base.models.ReviewFlowConditions(
                appLaunchCounter = it.appLaunchCounter,
                firstStart = Instant.fromEpochMilliseconds(it.firstStart),
                printWasActive = it.printWasActive,
            )
            clearConditions()
        }

        Napier.i(tag = tag, message = "ReviewFlowConditionMigration done")

    }

    private fun storeConditions(conditions: ReviewFlowConditions) {
        BaseInjector.get().sharedPreferences().edit {
            putString(KEY_CONDITIONS, Gson().toJson(conditions))
        }
    }

    private fun loadConditions() = BaseInjector.get().sharedPreferences().getString(KEY_CONDITIONS, null)?.let {
        Gson().fromJson(it, ReviewFlowConditions::class.java)
    }

    private fun clearConditions() = BaseInjector.get().sharedPreferences().edit { remove(KEY_CONDITIONS) }

    private data class ReviewFlowConditions(
        val appLaunchCounter: Int = 0,
        val firstStart: Long = System.currentTimeMillis(),
        val printWasActive: Boolean = false
    )
}