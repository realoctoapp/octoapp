package de.crysxd.octoapp.base.data.models

import android.os.Parcelable
import kotlinx.datetime.Instant
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.math.roundToLong

@Parcelize
@Serializable
data class FcmPrintEvent(
    val fileName: String? = null,
    val progress: Float? = null,
    val type: Type? = null,
    val serverTime: Double? = null,
    val serverTimePrecise: Double? = null,
    val timeLeft: Long? = null,
    val printId: String? = null,
    val message: String? = null,
) : Parcelable {

    val serverTimeFixed: Instant
        get() = when {
            serverTimePrecise != null -> Instant.fromEpochMilliseconds((serverTimePrecise * 1000).roundToLong())
            serverTime != null -> Instant.fromEpochSeconds(serverTime.roundToLong())
            else -> Instant.fromEpochSeconds(0)
        }

    @Serializable
    enum class Type {
        @SerialName("printing")
        Printing,

        @SerialName("paused")
        Paused,

        @SerialName("paused_gcode")
        PausedFromGcode,

        @SerialName("beep")
        Beep,

        @SerialName("first_layer_done")
        FirstLayerDone,

        @SerialName("third_layer_done")
        ThirdLayerDone,

        @SerialName("custom")
        Custom,

        @SerialName("completed")
        Completed,

        @SerialName("filament_required")
        FilamentRequired,

        @SerialName("idle")
        Idle,

        @SerialName("error")
        Error,

        @SerialName("mmu_filament_selection_started")
        Mmu2FilamentSelectionStarted,

        @SerialName("mmu_filament_selection_completed")
        Mmu2FilamentSelectionCompleted,
    }
}

