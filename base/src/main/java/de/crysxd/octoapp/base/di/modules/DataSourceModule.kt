package de.crysxd.octoapp.base.di.modules

import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.data.OctoPrintInstanceInformationSerializer
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector

@Module
class DataSourceModule {

    @Provides
    fun provideOctoPrintInstanceInformationSerialized() = OctoPrintInstanceInformationSerializer(
        json = SharedCommonInjector.get().json
    )

    @Provides
    fun provideLocalGcodeFileDataSource() = SharedBaseInjector.get().localGcodeFileDataSource

}