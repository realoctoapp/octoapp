package de.crysxd.octoapp.base.data.repository

import android.net.Uri
import androidx.core.net.toUri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import de.crysxd.octoapp.base.utils.AppScope
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.request.prepareGet
import io.ktor.client.statement.bodyAsChannel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AndroidMediaFileHelper(
    private val mediaFileRepository: MediaFileRepository,
) {

    private val tag = "AndroidMediaFileHelper"

    // We load on AppScope because we don't want to "waste" a started download.
    fun getMediaUri(url: String, lifecycleOwner: LifecycleOwner, then: (Uri) -> Unit) = AppScope.launch(Dispatchers.IO) {
        try {
            val path = mediaFileRepository.getFilePath(url) ?: let {
                // Store as non-deletable. We don't want to ever load this media again
                HttpClient().prepareGet(url).execute { response ->
                    mediaFileRepository.addFile(key = url, byteReadChannel = response.bodyAsChannel(), eternal = true)
                    mediaFileRepository.getFilePath(url)
                }
            } ?: throw IllegalStateException("Failed to get $url, path is null")

            // Check if caller is still alive
            withContext(Dispatchers.Main) {
                if (lifecycleOwner.lifecycle.currentState >= Lifecycle.State.INITIALIZED) {
                    val uri = path.toFile().toUri()
                    Napier.i(tag = tag, message = "Continuing with $uri")
                    then(uri)
                } else {
                    Napier.w(tag = tag, message = "Lifecycle died")
                }
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }
    }
}