package de.crysxd.octoapp.base.migrations

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import io.github.aakira.napier.Napier

class ObicoMigration {

    private val tag = "ObicoMigration"
    
    fun migrate() = try {
        val repo = SharedBaseInjector.get().printerConfigRepository
        val migrated = repo.getAll().map { instance ->
            val alternativeUrl = instance.alternativeWebUrl
            if (alternativeUrl?.host == "tunnels.app.thespaghettidetective.com") {
                Napier.i(tag = tag, message = "Migrating ${instance.id} from TSD to Obico tunnel")
                instance.copy(alternativeWebUrl = alternativeUrl.withHost("tunnels.app.obico.io"))
            } else {
                instance
            }
        }
        repo.setAll(migrated)
        Napier.i(tag = tag, message = "OctoPrintInstanceInformationMigration is done")
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed OctoPrintInstanceInformationMigration", throwable = e)
    }
}