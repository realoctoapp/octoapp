package de.crysxd.octoapp.base.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseScope