package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import android.os.Parcelable
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import kotlinx.parcelize.Parcelize
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem as NewItem

internal class GcodeHistoryMigration(val context: Context) {

    companion object {
        private const val TAG = "GcodeHistoryMigration"
    }

    fun migrate() = try {
        val ds = LocalGcodeHistoryDataSource(context)
        if (ds.hasAny()) {
            Napier.i(tag = TAG, message = "GcodeHistoryMigration running...")
            val items = ds.get() ?: emptyList()
            val mapped = items.map {
                NewItem(
                    lastUsed = it.lastUsed,
                    isFavorite = it.isFavorite,
                    usageCount = it.usageCount,
                    label = it.label,
                    command = it.command,
                )
            }
            Napier.i(tag = TAG, message = "Mapped ${mapped.size} items")
            SharedBaseInjector.get().gcodeHistoryRepository.import(mapped)
        } else {
            Napier.i(tag = TAG, message = "GcodeHistoryMigration is done")
        }
        ds.delete()
    } catch (e: Exception) {
        Napier.e(tag = TAG, message = "Failed GcodeHistoryMigration", throwable = e)
    }

    private interface DataSource<T> {
        fun store(t: T?)
        fun get(): T?
    }

    private class LocalGcodeHistoryDataSource(
        private val context: Context,
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context),
        private val gson: Gson = Gson()
    ) : DataSource<List<GcodeHistoryItem>> {

        companion object {
            private const val KEY = "gcode_history"
        }

        fun hasAny() = sharedPreferences.contains(KEY)

        fun delete() = sharedPreferences.edit { remove(KEY) }

        override fun store(t: List<GcodeHistoryItem>?) {
            try {
                sharedPreferences.edit {
                    putString(KEY, gson.toJson(t))
                }
            } catch (e: Exception) {
                Napier.e(tag = TAG, throwable = e, message = "Failed")
            }
        }

        override fun get(): List<GcodeHistoryItem>? = if (sharedPreferences.contains(KEY)) {
            gson.fromJson(
                sharedPreferences.getString(KEY, "[]"),
                object : TypeToken<List<GcodeHistoryItem>>() {}.type
            )
        } else {
            null
        }
    }

    @Parcelize
    private data class GcodeHistoryItem(
        val command: String,
        val lastUsed: Long = 0,
        val isFavorite: Boolean = false,
        val usageCount: Int = 0,
        val label: String? = null
    ) : Parcelable
}