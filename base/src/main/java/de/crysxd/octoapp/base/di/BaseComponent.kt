package de.crysxd.octoapp.base.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Component
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.OctoPrintInstanceInformationSerializer
import de.crysxd.octoapp.base.data.repository.AndroidMediaFileHelper
import de.crysxd.octoapp.base.data.repository.ControlsPreferencesRepository
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.data.repository.TutorialsRepository
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.di.modules.AndroidModule
import de.crysxd.octoapp.base.di.modules.DataSourceModule
import de.crysxd.octoapp.base.di.modules.FileModule
import de.crysxd.octoapp.base.di.modules.FirebaseModule
import de.crysxd.octoapp.base.di.modules.LoggingModule
import de.crysxd.octoapp.base.di.modules.OctoPrintModule
import de.crysxd.octoapp.base.di.modules.UseCaseModule
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.network.SslKeyStoreHandler
import de.crysxd.octoapp.base.usecase.ActivateMaterialUseCase
import de.crysxd.octoapp.base.usecase.AddMediaToGalleryUseCase
import de.crysxd.octoapp.base.usecase.ApplyAppThemeUseCase
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.base.usecase.AutoConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.CancelObjectUseCase
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.CreateFolderUseCase
import de.crysxd.octoapp.base.usecase.CreateProgressAppWidgetDataUseCase
import de.crysxd.octoapp.base.usecase.CyclePsuUseCase
import de.crysxd.octoapp.base.usecase.DeleteFileUseCase
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import de.crysxd.octoapp.base.usecase.EmergencyStopUseCase
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.ExecuteSystemCommandUseCase
import de.crysxd.octoapp.base.usecase.ExtrudeFilamentUseCase
import de.crysxd.octoapp.base.usecase.GetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.GetExtrusionShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetGcodeShortcutsUseCase
import de.crysxd.octoapp.base.usecase.GetMaterialsUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetPrinterConnectionUseCase
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSettingsUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.usecase.HandleObicoAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HomePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.JogPrintHeadUseCase
import de.crysxd.octoapp.base.usecase.LoadFileReferencesUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.MoveFileUseCase
import de.crysxd.octoapp.base.usecase.MovePrintHeadUseCase
import de.crysxd.octoapp.base.usecase.OpenOctoprintWebUseCase
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.SelectMmu2FilamentUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.base.usecase.SetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.usecase.ShareFileUseCase
import de.crysxd.octoapp.base.usecase.ShareImageUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.usecase.TunePrintUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import java.io.File
import javax.inject.Named

@BaseScope
@Component(
    modules = [
        AndroidModule::class,
        LoggingModule::class,
        OctoPrintModule::class,
        DataSourceModule::class,
        FirebaseModule::class,
        FileModule::class,
        UseCaseModule::class,
    ]
)
interface BaseComponent {

    // AndroidModule
    @Named(AndroidModule.LOCALIZED)
    fun localizedContext(): Context
    fun context(): Context
    fun app(): Application
    fun sharedPreferences(): SharedPreferences
    fun octoPreferences(): OctoPreferences

    // FileModule
    fun publicFileDirectory(): File

    // SslModule
    fun sslKeyStoreHandler(): SslKeyStoreHandler

    // DataSourceModule
    fun localGcodeFileDataSource(): LocalGcodeFileDataSource
    fun octoPrintInstanceInformationSerializer(): OctoPrintInstanceInformationSerializer

    // OctoprintModule
    fun octorPrintRepository(): PrinterConfigurationRepository
    fun octoPrintProvider(): PrinterEngineProvider
    fun serialCommunicationLogsRepository(): SerialCommunicationLogsRepository
    fun gcodeFileRepository(): GcodeFileRepository
    fun pinnedMenuItemsRepository(): PinnedMenuItemRepository
    fun gcodeHistoryRepository(): GcodeHistoryRepository
    fun widgetPreferencesRepository(): ControlsPreferencesRepository
    fun localDnsResolver(): CachedDns
    fun notificationIdRepository(): NotificationIdRepository
    fun temperatureDataRepository(): TemperatureDataRepository
    fun tutorialsRepository(): TutorialsRepository
    fun androidMediaFileHelper(): AndroidMediaFileHelper
    fun mediaFileRepository(): MediaFileRepository
    fun timelapseRepository(): TimelapseRepository
    fun extrusionHistoryRepository(): ExtrusionHistoryRepository

    // UseCaseModule
    fun setTargetTemperatureUseCase(): SetTargetTemperaturesUseCase
    fun applyTemperaturePresetUseCase(): ApplyTemperaturePresetUseCase
    fun setTemperatureOffsetUseCase(): SetTemperatureOffsetUseCase
    fun homePrintHeadUseCase(): HomePrintHeadUseCase
    fun jogPrintHeadUseCase(): JogPrintHeadUseCase
    fun movePrintHeadUseCase(): MovePrintHeadUseCase
    fun executeGcodeCommandUseCase(): ExecuteGcodeCommandUseCase
    fun extrudeFilamentUseCase(): ExtrudeFilamentUseCase
    fun loadFilesUseCase(): LoadFilesUseCase
    fun loadFileReferenceUseCase(): LoadFileReferencesUseCase
    fun startPrintJobUseCase(): StartPrintJobUseCase
    fun cancelPrintJobUseCase(): CancelPrintJobUseCase
    fun togglePausePrintJobUseCase(): TogglePausePrintJobUseCase
    fun emergencyStopUseCase(): EmergencyStopUseCase
    fun openOctoPrintWebUseCase(): OpenOctoprintWebUseCase
    fun createBugReportUseCase(): CreateBugReportUseCase
    fun updateInstanceCapabilitiesUseCase(): UpdateInstanceCapabilitiesUseCase
    fun getAppLanguageUseCase(): GetAppLanguageUseCase
    fun setAppLanguageUseCase(): SetAppLanguageUseCase
    fun setAlternativeWebUrlUseCase(): SetAlternativeWebUrlUseCase
    fun getPowerDevicesUseCase(): GetPowerDevicesUseCase
    fun applyAppThemeUseCase(): ApplyAppThemeUseCase
    fun executeSystemCommandUseCase(): ExecuteSystemCommandUseCase
    fun getWebcamSettingsUseCase(): GetWebcamSettingsUseCase
    fun createProgressAppWidgetDataUseCase(): CreateProgressAppWidgetDataUseCase
    fun getMaterialsUseCase(): GetMaterialsUseCase
    fun activateMaterialUseCase(): ActivateMaterialUseCase
    fun cyclePsuUseCase(): CyclePsuUseCase
    fun updateNgrokTunnelUseCase(): UpdateNgrokTunnelUseCase
    fun handleOctoEverywhereAppPortalSuccessUseCase(): HandleOctoEverywhereAppPortalSuccessUseCase
    fun handleObicoAppPortalSuccessUseCase(): HandleObicoAppPortalSuccessUseCase
    fun discoverOctoPrintUseCase(): DiscoverOctoPrintUseCase
    fun requestApiAccessUseCase(): RequestApiAccessUseCase
    fun testFullNetworkStackUseCase(): TestFullNetworkStackUseCase
    fun deleteFileUseCase(): DeleteFileUseCase
    fun moveFileUseCase(): MoveFileUseCase
    fun createFolderUseCase(): CreateFolderUseCase
    fun shareImageUseCase(): ShareImageUseCase
    fun publicFileFactory(): FileModule.PublicFileFactory
    fun getConnectOctoEverywhereUrlUseCase(): GetRemoteServiceConnectUrlUseCase
    fun selectMmu2FilamentUseCase(): SelectMmu2FilamentUseCase
    fun getWebcamSnapshotUseCase2(): GetWebcamSnapshotUseCase
    fun handleAutomaticLightEventUseCase(): HandleAutomaticLightEventUseCase
    fun getGcodeShortcutsUseCase(): GetGcodeShortcutsUseCase
    fun getExtrusionShortcutsUseCase(): GetExtrusionShortcutsUseCase
    fun tunePrintUseCase(): TunePrintUseCase
    fun triggerInitialCancelObjectMessageUseCase(): TriggerInitialCancelObjectMessageUseCase
    fun cancelObjectUseCase(): CancelObjectUseCase
    fun autoConnectPrinterUseCase(): AutoConnectPrinterUseCase
    fun getPrinterConnectionUseCase(): GetPrinterConnectionUseCase
    fun addMediaToGalleryUseCase(): AddMediaToGalleryUseCase
    fun shareFileUseCase(): ShareFileUseCase
}