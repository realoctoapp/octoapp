package de.crysxd.octoapp.base.migrations

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Instant
import java.io.File
import java.io.FileNotFoundException
import java.util.Date

class DnsCacheMigration(private val context: Context) {

    private val tag = "DnsCacheMigration"
    fun migrate() = runBlocking(Dispatchers.IO) {
        val cacheFile = File(context.cacheDir, "dns2.json")

        try {
            if (cacheFile.exists()) {
                Napier.i(tag = tag, message = "DnsCacheMigration running...")
                val entries = loadCache(cacheFile).mapValues {
                    CachedDns.Entry(
                        hostname = it.value.hostname,
                        resolvedIpString = it.value.resolvedIpString,
                        validUntil = Instant.fromEpochMilliseconds(it.value.validUntil.time)
                    )
                }
                val dns = SharedBaseInjector.get().dnsResolver as? CachedLocalDnsResolver
                    ?: return@runBlocking Napier.w(tag = tag, message = "Cancelling migration, not a CachedLocalDnsResolver")

                dns.importAll(entries)
                Napier.i(tag = tag, message = "Migrated ${entries.size} DNS entries")
                cacheFile.delete()
            } else {
                Napier.i(tag = tag, message = "DnsCacheMigration is done")
            }
        } catch (e: JsonSyntaxException) {
            try {
                Napier.e(tag = tag, message = "Failed migration permanently", throwable = e)
                cacheFile.delete()
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed migration permanently cleanup", throwable = e)
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed migration", throwable = e)
        }
    }

    private fun loadCache(cacheFile: File) = cacheFile.inputStream().bufferedReader().use {
        val gson = Gson()
        val type = object : TypeToken<Map<String, Entry>>() {}.type
        gson.fromJson<Map<String, Entry>>(it.readText(), type) ?: throw FileNotFoundException()
    }


    private data class Entry(
        val hostname: String,
        val resolvedIpString: List<String>,
        val validUntil: Date,
    )
}