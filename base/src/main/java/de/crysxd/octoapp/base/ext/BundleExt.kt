@file:Suppress("DEPRECATION")

package de.crysxd.octoapp.base.ext

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable

inline fun <reified T : Parcelable> Bundle.getParcelableCompat(key: String): T? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
    getParcelable(key, T::class.java)
} else {
    getParcelable(key)
}

inline fun <reified T : Serializable> Bundle.getSerializableCompat(key: String): T? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
    getSerializable(key, T::class.java)
} else {
    getSerializable(key) as T
}

inline fun <reified T : Parcelable> Intent.getParcelableCompat(key: String): T? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
    getParcelableExtra(key, T::class.java)
} else {
    getParcelableExtra(key)
}