package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import io.github.aakira.napier.Napier

class OctoPreferencesMigration(val context: Context) {

    private val tag = "OctoPreferencesMigration"

    fun migrate() {
        val migrations = listOf(
            "print_notification_enabled" to BooleanMigrator(),
            "app_theme" to StringMigrator(),
            "keep_screen_on" to BooleanMigrator(),
            "app_language" to StringMigrator(),
            "allow_app_rotation" to BooleanMigrator(),
            "allow_notification_battery_saver" to BooleanMigrator(),
            "hide_thumbnail_hin_until" to IntMigrator(),
            "active_instance_id" to StringMigrator(),
            "auto_connect_printer" to BooleanMigrator(),
            "auto_connect_printer_info_shown" to BooleanMigrator(),
            "crash_reporting_enabled" to BooleanMigrator(),
            "analytics_enabled" to BooleanMigrator(),
            "print_notification_was_disconnected" to BooleanMigrator(),
            "print_notification_was_disabled_until_next_launch" to BooleanMigrator(),
            "auto_lights" to StringSetMigrator(),
            "confirm_power_off_devices" to StringSetMigrator(),
            "auto_lights_for_widget_refresh" to BooleanMigrator(),
            "show_webcam_resolution" to BooleanMigrator(),
            "webcam_aspect_ratio_source" to StringMigrator(),
            "suppress_m115_request" to BooleanMigrator(),
            "octoeverywhere_announcemenyt_hidden_at" to IntMigrator(),
            "tutorials_seen_at" to IntMigrator(),
            "allow_terminal_during_print" to BooleanMigrator(),
            "suppress_remote_notification_init" to BooleanMigrator(),
            "debug_network_logging" to BooleanMigrator(),
            "enforce_ip_v4" to BooleanMigrator(),
            "record_webcam_for_debug" to BooleanMigrator(),
            "compact_layout" to BooleanMigrator(),
            "gcode_preview" to StringMigrator(),
            "file_manager" to StringMigrator(),
            "progress_widget" to StringMigrator(),
            "progress_widget_webcam_fullscreen" to StringMigrator(),
            "ask_for_timelapse_before_printing" to BooleanMigrator(),
            "left_hand_mode" to BooleanMigrator(),
            "media_cache_size" to IntMigrator(),
            "gcode_cache_size" to IntMigrator(),
            "http_cache_size" to IntMigrator(),
            "notify_printer_beep" to BooleanMigrator(),
            "data_saver_webcam_interval" to IntMigrator(),
            "ask_notification_permission_not_before_2" to IntMigrator(),
            "notification_permission_request_state_2" to IntMigrator(),
            "show_active_instance_in_status_bar" to BooleanMigrator(),
        )

        val old = PreferenceManager.getDefaultSharedPreferences(context)
        val new = context.getSharedPreferences("settings", Context.MODE_PRIVATE)

        try {
            val oldEditor = old.edit()
            Napier.i(tag = tag, message = "Running OctoPreferencesMigration")
            new.edit(commit = true) {
                migrations.forEach {
                    if (old.contains(it.first)) {
                        Napier.i(tag = tag, message = "Migrating ${it.first}")
                        it.second.migrate(it.first, old, this)
                        oldEditor.remove(it.first)
                    }
                }
            }
            oldEditor.apply()
            Napier.i(tag = tag, message = "OctoPreferencesMigration is done")
        } catch (e: Exception) {
            Napier.i(tag = tag, message = "Failed OctoPreferencesMigration", throwable = e)
        }
    }

    private interface Migrator {
        fun migrate(key: String, from: SharedPreferences, to: SharedPreferences.Editor)
    }

    private class IntMigrator : Migrator {
        override fun migrate(key: String, from: SharedPreferences, to: SharedPreferences.Editor) {
            val mock = -3276232
            try {
                from.getInt(key, mock).takeUnless { it == mock }?.let { to.putInt(key, it) }
            } catch (e: Exception) {
                from.getLong(key, mock.toLong()).takeUnless { it == mock.toLong() }?.let { to.putLong(key, it) }
            }
        }
    }

    private class StringMigrator : Migrator {
        override fun migrate(key: String, from: SharedPreferences, to: SharedPreferences.Editor) {
            from.getString(key, null)?.let { to.putString(key, it) }
        }
    }

    private class StringSetMigrator : Migrator {
        override fun migrate(key: String, from: SharedPreferences, to: SharedPreferences.Editor) {
            from.getStringSet(key, null)?.let { to.putString(key, it.joinToString("%%%barreir%%%")) }
        }
    }

    private class BooleanMigrator : Migrator {
        override fun migrate(key: String, from: SharedPreferences, to: SharedPreferences.Editor) {
            from.getBoolean(key, false).let { to.putBoolean(key, it) }
        }
    }
}