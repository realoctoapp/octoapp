package de.crysxd.octoapp.base.gcode

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.core.graphics.applyCanvas
import de.crysxd.octoapp.base.BuildConfig
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.ext.drawableRes
import de.crysxd.octoapp.base.models.GcodeRenderContext
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.math.roundToInt
import kotlin.system.measureTimeMillis

private const val ASYNC_RENDER_RECOMMENDED_THRESHOLD_MS = 20

class GcodeRenderView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {

    private val tag = "GcodeRenderView"
    private val gestureDetector = GestureDetector(context, GestureListener())
    private val scaleGestureDetector = ScaleGestureDetector(context, ScaleGestureListener())
    private val canvasAdapter = CanvasAdapter()
    val painter = GcodeRenderer(
        canvas = canvasAdapter,
    ).apply {
        plugin = ::drawOverlay
    }

    var useAsyncRender = false
        private set
    private var asyncRenderCache: Bitmap? = null
    private var asyncRenderResult: Bitmap? = null
    var asyncRenderRecommended = false
        private set
    private var asyncRenderBusy = false
    private var pendingAsyncRender = false
    private var renderScope: CoroutineScope? = null

    var isAcceptTouchInput = true
    var renderParams: RenderParams? = null
        set(value) {
            value?.let {
                canvasAdapter.setQuality(value.quality)
                GcodeRenderer.RenderParams(
                    renderContext = value.renderContext,
                    printBed = value.printBed,
                    printBedWidthMm = value.printBedSizeMm.x,
                    printBedHeightMm = value.printBedSizeMm.y,
                    extrusionWidthMm = value.extrusionWidthMm,
                    minPrintHeadDiameterPx = resources.getDimension(R.dimen.gcode_render_view_print_head_size),
                    originInCenter = value.originInCenter,
                    quality = value.quality
                )
            }.let(painter::submitRenderParams)
            field = value
        }

    var overlay: Overlay? = null
        set(value) {
            field = value
            invalidate()
        }

    var nativeOverlay: (GcodeNativeCanvas.() -> Unit)? = null

    init {
        setWillNotDraw(false)
        setLayerType(LAYER_TYPE_HARDWARE, null)
        clipToOutline = false
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (pendingAsyncRender) {
            renderAsync()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (isAcceptTouchInput) {

            // Play nice with control center overlay. This view gets touch priority
            when (event.action) {
                MotionEvent.ACTION_DOWN -> parent.requestDisallowInterceptTouchEvent(true)
                MotionEvent.ACTION_UP -> parent.requestDisallowInterceptTouchEvent(false)
            }

            scaleGestureDetector.onTouchEvent(event)
            if (!scaleGestureDetector.isInProgress) {
                gestureDetector.onTouchEvent(event)
            }
        }
        return true
    }

    fun enableAsyncRender(coroutineScope: CoroutineScope) {
        renderScope = coroutineScope
        useAsyncRender = true
    }

    private fun drawOverlay(adapter: GcodeNativeCanvas) {
        require(adapter is CanvasAdapter) { "Wrong canvas presented" }
        overlay?.onPaint?.invoke(adapter.canvas)
        nativeOverlay?.invoke(adapter)
    }

    private fun animateZoom(focusX: Float, focusY: Float, newZoom: Float) {
        ObjectAnimator.ofFloat(painter.zoom, newZoom).also { animator ->
            animator.addUpdateListener { painter.zoom(focusX = focusX, focusY = focusY, newZoom = it.animatedValue as Float) }
            animator.interpolator = DecelerateInterpolator()
            animator.duration = 150
        }.start()
    }

    override fun invalidate() {
        if (useAsyncRender) {
            renderAsync()
        } else {
            super.invalidate()
        }
    }

    override fun onDraw(canvas: Canvas) = measureTimeMillis {
        super.onDraw(canvas)

        // Check HW acceleration
        if (!canvas.isHardwareAccelerated && BuildConfig.DEBUG) {
            Napier.w(tag = tag, message = "Missing hardware acceleration!")
        }

        if (useAsyncRender) {
            asyncRenderResult?.let {
                canvas.drawBitmap(it, 0f, 0f, null)
            }
        } else {
            painter.draw(canvas = canvas)
        }
    }.let {
        // Do not log in non-debug builds to increase performance
        if (BuildConfig.DEBUG) {
            if (it > 5) {
                Napier.w(tag = tag, message = "Draw took ${it}ms")
            }
        }

        if (it > ASYNC_RENDER_RECOMMENDED_THRESHOLD_MS) {
            asyncRenderRecommended = true
        }
    }

    private fun renderAsync(): Unit = measureTimeMillis {
        if (width == 0 || height == 0 || asyncRenderBusy) {
            // Trigger render again after we are done
            pendingAsyncRender = true
            return
        }
        val w = width
        val h = height

        asyncRenderBusy = true

        renderScope?.launch(Dispatchers.Default) {
            // Create bitmap
            val bitmap = asyncRenderCache?.takeIf { it.height == h && it.width == w }
                ?: Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            asyncRenderCache = bitmap

            // Render. This bitmap may be blank when we need to draw to the view.
            bitmap.eraseColor(Color.TRANSPARENT)
            bitmap.applyCanvas {
                painter.draw(canvas = this)
            }

            // We are done. Paint to view
            post {
                // Flush. This bitmap is always ready to be drawn.
                val result = asyncRenderResult?.takeIf { it.height == h && it.width == w }
                    ?: Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
                asyncRenderResult = result
                result.eraseColor(Color.TRANSPARENT)
                result.applyCanvas {
                    drawBitmap(bitmap, 0f, 0f, null)
                }

                // Invalidate to draw to view
                super.invalidate()

                // New data came in while we were busy? Trigger next round
                asyncRenderBusy = false
                if (pendingAsyncRender) {
                    pendingAsyncRender = false
                    renderAsync()
                }
            }
        } ?: run {
            asyncRenderBusy = false
        }
    }.let {
        if (BuildConfig.DEBUG && it > 10) {
            // Do not log in non-debug builds to increase performance
            Napier.v(tag = tag, message = "Async render took ${it}ms")
        }
    }

    private fun GcodeRenderer.draw(canvas: Canvas) = canvasAdapter.withCanvasActive(canvas) {
        draw()
    }

    inner class ScaleGestureListener : ScaleGestureDetector.OnScaleGestureListener {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            painter.zoomBy(focusX = detector.focusX, focusY = detector.focusY, scaleFactor = detector.scaleFactor)
            return true
        }

        override fun onScaleBegin(detector: ScaleGestureDetector) = true

        override fun onScaleEnd(detector: ScaleGestureDetector) = Unit
    }

    inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent) = true

        override fun onShowPress(e: MotionEvent) = Unit

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            val (xMm, yMm) = painter.getPrintBedCoordinateFromViewPortPosition(x = e.x, y = e.y)
            return overlay?.onClick?.invoke(xMm, yMm) == true
        }

        override fun onScroll(p0: MotionEvent?, e1: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
            painter.moveBy(distanceX, distanceY)
            return true
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            val newZoom = if (painter.zoom > 1) {
                1f
            } else {
                painter.doubleTapZoom
            }
            animateZoom(e.x, e.y, newZoom)

            return true
        }

        override fun onLongPress(e: MotionEvent) = Unit

        override fun onFling(e1: MotionEvent?, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean = false

    }

    data class Overlay(
        val onPaint: Canvas.() -> Unit,
        val onClick: (x: Float, y: Float) -> Boolean,
    )

    data class RenderParams(
        val renderContext: GcodeRenderContext,
        val printBedSizeMm: PointF,
        val printBed: GcodeNativeCanvas.Image,
        val extrusionWidthMm: Float = 0.5f,
        val originInCenter: Boolean,
        val quality: GcodePreviewSettings.Quality,
    )

    inner class CanvasAdapter : GcodeNativeCanvas {

        private var currentCanvas: Canvas? = null
        val canvas get() = requireNotNull(currentCanvas) { "Canvas not available" }

        private var paint = Paint()
        private val colorMoveExtrusion = ContextCompat.getColor(context, R.color.gcode_extrusion)
        private val colorMoveTravel = ContextCompat.getColor(context, R.color.gcode_travel)
        private val colorMovePreviousLayer = ContextCompat.getColor(context, R.color.gcode_previous)
        private val colorMoveRemainingLayer = ContextCompat.getColor(context, R.color.gcode_remaining)
        private val colorMoveUnsupported = ContextCompat.getColor(context, R.color.gcode_unsupported)
        private val colorPrintHead = ContextCompat.getColor(context, R.color.gcode_print_head)
        private val red = ContextCompat.getColor(context, R.color.red)
        private val green = ContextCompat.getColor(context, R.color.green)
        private val colorYellow = ContextCompat.getColor(context, R.color.yellow)
        private val colorLightGrey = ContextCompat.getColor(context, R.color.light_grey)
        private val accent = ContextCompat.getColor(context, R.color.accent)
        private val imageCache = mutableMapOf<GcodeNativeCanvas.Image, Drawable>()

        override val viewPortWidth get() = width.toFloat()
        override val viewPortHeight get() = height.toFloat()
        override val paddingTop get() = this@GcodeRenderView.paddingTop.toFloat()
        override val paddingLeft get() = this@GcodeRenderView.paddingLeft.toFloat()
        override val paddingRight get() = this@GcodeRenderView.paddingRight.toFloat()
        override val paddingBottom get() = this@GcodeRenderView.paddingBottom.toFloat()

        init {
            setQuality(GcodePreviewSettings.Quality.Medium)
        }

        fun withCanvasActive(canvas: Canvas, block: () -> Unit) {
            currentCanvas = canvas
            block()
            currentCanvas = null
        }

        fun setQuality(quality: GcodePreviewSettings.Quality) {
            paint.isAntiAlias = quality >= GcodePreviewSettings.Quality.Ultra
            paint.strokeCap = if (quality >= GcodePreviewSettings.Quality.Medium) Paint.Cap.ROUND else Paint.Cap.BUTT
        }

        private fun GcodeNativeCanvas.Image.load() = imageCache.getOrPut(this) {
            val drawable = ContextCompat.getDrawable(context, drawableRes)

            requireNotNull(drawable) { "Failed to load drawable for $this" }
        }

        private fun GcodeNativeCanvas.Paint.map(): Paint = paint.also {
            it.alpha = 255
            it.strokeWidth = strokeWidth
            it.style = when (mode) {
                GcodeNativeCanvas.Paint.Mode.Fill -> Paint.Style.FILL
                GcodeNativeCanvas.Paint.Mode.Stroke -> Paint.Style.STROKE
            }
            it.color = when (val c = color) {
                GcodeNativeCanvas.Color.MoveExtrusion -> colorMoveExtrusion
                GcodeNativeCanvas.Color.MovePreviousLayer -> colorMovePreviousLayer
                GcodeNativeCanvas.Color.MoveRemainingLayer -> colorMoveRemainingLayer
                GcodeNativeCanvas.Color.MoveTravel -> colorMoveTravel
                GcodeNativeCanvas.Color.MoveUnsupported -> colorMoveUnsupported
                GcodeNativeCanvas.Color.PrintHead -> colorPrintHead
                GcodeNativeCanvas.Color.Yellow -> colorYellow
                GcodeNativeCanvas.Color.LightGrey -> colorLightGrey
                GcodeNativeCanvas.Color.Accent -> accent
                GcodeNativeCanvas.Color.Red -> red
                GcodeNativeCanvas.Color.Green -> green
                is GcodeNativeCanvas.Color.Custom -> Color.argb(255, (c.red * 255).roundToInt(), (c.green * 255).roundToInt(), (c.blue * 255).roundToInt())
            }
        }

        override fun invalidate() = this@GcodeRenderView.invalidate()

        override fun saveState() {
            canvas.save()
        }

        override fun restoreState() = canvas.restore()

        override fun scale(x: Float, y: Float) =
            canvas.scale(x, y)

        override fun translate(x: Float, y: Float) =
            canvas.translate(x, y)

        override fun rotate(degrees: Float) =
            canvas.rotate(degrees)

        override fun drawLine(xStart: Float, yStart: Float, xEnd: Float, yEnd: Float, paint: GcodeNativeCanvas.Paint) =
            canvas.drawLine(xStart, yStart, xEnd, yEnd, paint.map())

        override fun drawLines(points: FloatArray, offset: Int, count: Int, paint: GcodeNativeCanvas.Paint) =
            canvas.drawLines(points, offset, count, paint.map())

        override fun drawRect(left: Float, top: Float, right: Float, bottom: Float, paint: GcodeNativeCanvas.Paint) =
            canvas.drawRect(left, top, right, bottom, paint.map())

        override fun intrinsicWidth(image: GcodeNativeCanvas.Image) =
            image.load().intrinsicWidth.toFloat()

        override fun intrinsicHeight(image: GcodeNativeCanvas.Image) =
            image.load().intrinsicWidth.toFloat()

        override fun drawImage(image: GcodeNativeCanvas.Image, x: Float, y: Float, width: Float, height: Float) = with(image.load()) {
            setBounds(x.toInt(), y.toInt(), width.toInt(), height.toInt())
            draw(canvas)
        }

        override fun drawArc(
            centerX: Float,
            centerY: Float,
            radius: Float,
            startDegrees: Float,
            sweepAngle: Float,
            paint: GcodeNativeCanvas.Paint
        ) {
            canvas.drawArc(centerX - radius, centerY - radius, centerX + radius, centerY + radius, startDegrees, sweepAngle, false, paint.map())
        }
    }
}