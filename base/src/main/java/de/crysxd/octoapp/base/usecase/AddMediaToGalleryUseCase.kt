package de.crysxd.octoapp.base.usecase

import android.content.ContentValues
import android.content.Context
import android.os.Environment
import android.provider.MediaStore
import android.webkit.MimeTypeMap
import de.crysxd.octoapp.base.di.modules.FileModule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.datetime.Instant
import java.io.File
import java.io.IOException
import javax.inject.Inject

class AddMediaToGalleryUseCase @Inject constructor(
    private val publicFileFactory: FileModule.PublicFileFactory,
) : UseCase2<AddMediaToGalleryUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger): Unit = withContext(Dispatchers.IO) {
        // Create values
        val extension = File(param.originalName).extension
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        logger.i("Pushing ${param.file.path} (${param.originalName}) to gallery with mime type $mimeType")
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, param.originalName)
            put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
            put(MediaStore.MediaColumns.DATE_TAKEN, param.date.toEpochMilliseconds())
            put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_MOVIES)
            put(MediaStore.Video.Media.IS_PENDING, 1)
        }

        // Create Uri
        val (videoUri, out) = param.context.contentResolver.let { resolver ->
            val uri = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, contentValues) ?: throw IOException("Unable to create URI")
            val fos = uri.let { resolver.openOutputStream(it) } ?: throw IOException("Unable to open output")
            uri to fos
        }

        // Copy file
        param.file.inputStream().copyTo(out)

        // Push to gallery
        contentValues.clear()
        contentValues.put(MediaStore.Video.Media.IS_PENDING, 0)
        param.context.contentResolver.update(videoUri, contentValues, null, null)
    }

    data class Params(
        val context: Context,
        val file: File,
        val date: Instant,
        val originalName: String,
    )
}