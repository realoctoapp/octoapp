package de.crysxd.octoapp.base.ext

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigFetchThrottledException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun FirebaseRemoteConfig.fetchAndActivateSafe() = withContext(Dispatchers.IO) {
    try {
        fetchAndActivate().blockingAwait()
        true
    } catch (e: FirebaseRemoteConfigFetchThrottledException) {
        Napier.d(tag = "FirebaseRemoteConfig", message = "Remote config is throttled: ${e.message}")
        false
    } catch (e: java.lang.Exception) {
        // Continue with old values
        false
    }
}
