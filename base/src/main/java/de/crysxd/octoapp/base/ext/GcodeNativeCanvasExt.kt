package de.crysxd.octoapp.base.ext

import androidx.annotation.DrawableRes
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas

@get:DrawableRes
val GcodeNativeCanvas.Image.drawableRes
    get() = when (this) {
        GcodeNativeCanvas.Image.PrintBedEnder -> R.drawable.print_bed_ender
        GcodeNativeCanvas.Image.PrintBedPrusa -> R.drawable.print_bed_prusa
        GcodeNativeCanvas.Image.PrintBedGeneric -> R.drawable.print_bed_generic
        GcodeNativeCanvas.Image.PrintBedArtillery -> R.drawable.print_bed_artillery
        GcodeNativeCanvas.Image.PrintBedAnycubic -> R.drawable.print_bed_anycubic
        GcodeNativeCanvas.Image.PrintBedCreality -> R.drawable.print_bed_creality
    }