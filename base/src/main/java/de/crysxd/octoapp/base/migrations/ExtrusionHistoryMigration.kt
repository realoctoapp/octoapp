package de.crysxd.octoapp.base.migrations

import android.content.Context
import android.content.SharedPreferences
import android.os.Parcelable
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.base.di.SharedBaseInjector
import io.github.aakira.napier.Napier
import kotlinx.parcelize.Parcelize
import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem as NewItem

class ExtrusionHistoryMigration(val context: Context) {

    private val tag = "ExtrusionHistoryMigration"

    fun migrate() = try {
        val ds = LocalExtrusionHistoryDataSource(context)
        if (ds.hasAny()) {
            Napier.i(tag = tag, message = "ExtrusionHistoryMigration running...")
            val items = ds.get() ?: emptyList()
            val mapped = items.map {
                NewItem(
                    distanceMm = it.distanceMm,
                    lastUsed = it.lastUsed,
                    isFavorite = it.isFavorite,
                    usageCount = it.usageCount
                )
            }
            Napier.i(tag = tag, message = "Mapped ${mapped.size} items")
            SharedBaseInjector.get().extrusionHistoryRepository.import(mapped)
        } else {
            Napier.i(tag = tag, message = "ExtrusionHistoryMigration is done")
        }
        ds.delete()
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed migration", throwable = e)
    }

    private class LocalExtrusionHistoryDataSource(
        private val context: Context,
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context),
        private val gson: Gson = Gson()
    ) {

        companion object {
            private const val KEY = "extrusion_history"
        }

        fun hasAny() = sharedPreferences.contains(KEY)

        fun delete() = sharedPreferences.edit { remove(KEY) }

        fun get(): List<ExtrusionHistoryItem>? = if (sharedPreferences.contains(KEY)) {
            gson.fromJson(
                sharedPreferences.getString(KEY, "[]"),
                object : TypeToken<List<ExtrusionHistoryItem>>() {}.type
            )
        } else {
            null
        }
    }

    @Parcelize
    private data class ExtrusionHistoryItem(
        val distanceMm: Int,
        val lastUsed: Long = 0,
        val isFavorite: Boolean = false,
        val usageCount: Int = 0,
    ) : Parcelable
}