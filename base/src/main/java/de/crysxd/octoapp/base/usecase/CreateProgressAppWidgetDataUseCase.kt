package de.crysxd.octoapp.base.usecase

import android.os.Parcelable
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.models.event.Message
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize
import java.util.Date
import javax.inject.Inject

class CreateProgressAppWidgetDataUseCase @Inject constructor(
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val printerEngineProvider: PrinterEngineProvider
) : UseCase2<CreateProgressAppWidgetDataUseCase.Params, CreateProgressAppWidgetDataUseCase.Result>() {

    init {
        suppressLogging = true
    }

    override suspend fun doExecute(param: Params, logger: Logger): Result =
        param.currentMessage?.let { fromCurrentMessage(it, param.instanceId) }
            ?: fromNetworkRequest(param.instanceId)

    private suspend fun fromNetworkRequest(instanceId: String): Result = withContext(Dispatchers.IO) {
        val instance = printerConfigurationRepository.get(instanceId) ?: throw IllegalStateException("Unable to locate instance for $instanceId")
        val octoPrint = printerEngineProvider.printer(instance.id)
        val asyncJob = async { octoPrint.jobApi.getJob() }
        val asyncState = async {
            try {
                octoPrint.printerApi.getPrinterState()
            } catch (e: PrinterNotOperationalException) {
                // Printer not connected, null state will indicate this
                null
            }
        }

        val state = asyncState.await()
        val job = asyncJob.await()

        return@withContext Result(
            isPrinting = state?.state?.flags?.printing == true,
            isPausing = state?.state?.flags?.pausing == true,
            isCancelling = state?.state?.flags?.cancelling == true,
            isPaused = state?.state?.flags?.paused == true,
            isPrinterConnected = state != null,
            isLive = false,
            printProgress = job.progress.completion?.let { it / 100f },
            printTimeLeft = job.progress.printTimeLeft,
            printTimeLeftOrigin = job.progress.printTimeLeftOrigin,
            instanceId = instance.id,
            label = instance.label,
        )
    }

    private fun fromCurrentMessage(currentMessage: Message.Current, instanceId: String) = Result(
        isPrinting = currentMessage.state.flags.printing,
        isPausing = currentMessage.state.flags.pausing,
        isCancelling = currentMessage.state.flags.cancelling,
        isPaused = currentMessage.state.flags.paused,
        isLive = true,
        isPrinterConnected = true,
        printProgress = currentMessage.progress?.completion?.let { it / 100f },
        printTimeLeft = currentMessage.progress?.printTimeLeft,
        printTimeLeftOrigin = currentMessage.progress?.printTimeLeftOrigin,
        instanceId = instanceId,
        label = printerConfigurationRepository.get(instanceId)?.label ?: instanceId
    )

    data class Params(
        val currentMessage: Message.Current?,
        val instanceId: String
    )

    @Parcelize
    data class Result(
        val isPrinting: Boolean,
        val isPausing: Boolean,
        val isPaused: Boolean,
        val isCancelling: Boolean,
        val isPrinterConnected: Boolean,
        val isLive: Boolean,
        val printProgress: Float?,
        val printTimeLeft: Long?,
        val printTimeLeftOrigin: String?,
        val instanceId: String,
        val label: String,
        val createdAt: Date = Date(),
    ) : Parcelable
}