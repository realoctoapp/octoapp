package de.crysxd.octoapp.base.usecase

import android.content.Context
import android.webkit.MimeTypeMap
import androidx.core.app.ShareCompat
import de.crysxd.octoapp.base.di.modules.FileModule
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import javax.inject.Inject

class ShareFileUseCase @Inject constructor(
    private val publicFileFactory: FileModule.PublicFileFactory,
) : UseCase2<ShareFileUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) = withContext(Dispatchers.IO) {
        // Create cache dir and make sure we delete all old files
        val (file, uri) = publicFileFactory.createPublicFile(param.name)
        file.delete()
        param.file.copyTo(file)

        try {
            // Share
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(param.name.substringAfterLast(".", ""))

            logger.i("Sharing ${param.file.path} from $uri with mime type $mimeType (${File(param.file.path).length().formatAsFileSize()})")
            ShareCompat.IntentBuilder(param.context)
                .setStream(uri)
                .setChooserTitle(param.file.name)
                .setType(mimeType ?: "file/*")
                .startChooser()
        } catch (e: Exception) {
            file.delete()
            throw e
        }
    }

    data class Params(
        val context: Context,
        val file: File,
        val name: String = file.name,
    )
}