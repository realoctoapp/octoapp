package de.crysxd.octoapp.base.di.modules

import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.di.SharedBaseInjector

@Module
class UseCaseModule {

    @Provides
    fun getWebcamSettingsUseCase() = SharedBaseInjector.get().getWebcamSettingsUseCase()

    @Provides
    fun getActiveHttpUrlUseCase() = SharedBaseInjector.get().getActiveHttpUrlUseCase()

    @Provides
    fun applyWebcamTransformationsUseCase() = SharedBaseInjector.get().applyWebcamTransformationsUseCase()

    @Provides
    fun getPowerDevicesUseCase() = SharedBaseInjector.get().getPowerDevicesUseCase()

    @Provides
    fun handleAutomaticLightEventUseCase() = SharedBaseInjector.get().handleAutomaticLightEventUseCase()

    @Provides
    fun testFullNetworkStackUseCase() = SharedBaseInjector.get().testFullNetworkStackUseCase()

    @Provides
    fun discoverOctoPrintUseCase() = SharedBaseInjector.get().discoverOctoPrintUseCase()

    @Provides
    fun updateInstanceCapabilitiesUseCase() = SharedBaseInjector.get().updateInstanceCapabilitiesUseCase()

    @Provides
    fun getAppLanguageUseCase() = SharedBaseInjector.get().getAppLanguageUseCase()

    @Provides
    fun requestApiAccessUseCase() = SharedBaseInjector.get().requestApiAccessUseCase()

    @Provides
    fun setTargetTemperaturesUseCase() = SharedBaseInjector.get().setTargetTemperaturesUseCase()

    @Provides
    fun setTemperatureOffsetUseCase() = SharedBaseInjector.get().setTemperatureOffsetUseCase()

    @Provides
    fun loadFilesUseCase() = SharedBaseInjector.get().loadFilesUseCase()

    @Provides
    fun loadFileReferencesUseCase() = SharedBaseInjector.get().loadFileReferencesUseCase()

    @Provides
    fun cancelPrintJobUseCase() = SharedBaseInjector.get().cancelPrintJobUseCase()

    @Provides
    fun startPrintJobUseCase() = SharedBaseInjector.get().startPrintJobUseCase()

    @Provides
    fun togglePausePrintJobUseCase() = SharedBaseInjector.get().togglePausePrintJobUseCase()

    @Provides
    fun getCurrentPrinterProfileUseCase() = SharedBaseInjector.get().getCurrentPrinterProfileUseCase()

    @Provides
    fun getWebcamSnapshotUseCase() = SharedBaseInjector.get().getWebcamSnapshotUseCase()

    @Provides
    fun createBugReportUseCase() = SharedBaseInjector.get().createBugReportUseCase()

    @Provides
    fun getRemoteServiceConnectUrlUseCase() = SharedBaseInjector.get().getRemoteServiceConnectUrlUseCase()

    @Provides
    fun handleOctoEverywhereAppPortalSuccessUseCase() = SharedBaseInjector.get().handleOctoEverywhereAppPortalSuccessUseCase()

    @Provides
    fun handleObicoAppPortalSuccessUseCase() = SharedBaseInjector.get().handleObicoAppPortalSuccessUseCase()

    @Provides
    fun setAlternativeWebUrlUseCase() = SharedBaseInjector.get().setAlternativeWebUrlUseCase()

    @Provides
    fun updateNgrokTunnelUseCase() = SharedBaseInjector.get().updateNgrokTunnelUseCase()

    @Provides
    fun connectPrinterUseCase() = SharedBaseInjector.get().connectPrinterUseCase()

    @Provides
    fun autoConnectPrinterUseCase() = SharedBaseInjector.get().autoConnectPrinterUseCase()

    @Provides
    fun getPrinterConnectionUseCase() = SharedBaseInjector.get().getPrinterConnectionUseCase()

    @Provides
    fun setAppLanguageUseCase() = SharedBaseInjector.get().setAppLanguageUseCase()

    @Provides
    fun executeSystemCommandUseCase() = SharedBaseInjector.get().executeSystemCommandUseCase()

    @Provides
    fun emergencyStopUseCase() = SharedBaseInjector.get().emergencyStopUseCase()

    @Provides
    fun activateMaterialUseCase() = SharedBaseInjector.get().activateMaterialUseCase()

    @Provides
    fun getMaterialsUseCase() = SharedBaseInjector.get().getMaterialsUseCase()

    @Provides
    fun cyclePsuUseCase() = SharedBaseInjector.get().cyclePsuUseCase()

    @Provides
    fun jogPrintHeadUseCase() = SharedBaseInjector.get().jogPrintHeadUseCase()

    @Provides
    fun movePrintHeadUseCase() = SharedBaseInjector.get().movePrintHeadUseCase()

    @Provides
    fun homePrintHeadUseCase() = SharedBaseInjector.get().homePrintHeadUseCase()

    @Provides
    fun executeGcodeCommandUseCase() = SharedBaseInjector.get().executeGcodeCommandUseCase()

    @Provides
    fun extrudeFilamentUseCase() = SharedBaseInjector.get().extrudeFilamentUseCase()

    @Provides
    fun getExtrusionShortcutsUseCase() = SharedBaseInjector.get().getExtrusionShortcutsUseCase()

    @Provides
    fun getGcodeShortcutsUseCase() = SharedBaseInjector.get().getGcodeShortcutsUseCase()

    @Provides
    fun tunePrintUseCase() = SharedBaseInjector.get().tunePrintUseCase()

    @Provides
    fun cancelObjectUseCase() = SharedBaseInjector.get().cancelObjectUseCase()

    @Provides
    fun triggerInitialCancelObjectMessageUseCase() = SharedBaseInjector.get().triggerInitialCancelObjectMessageUseCase()

    @Provides
    fun selectMmu2FilamentUseCase() = SharedBaseInjector.get().selectMmu2FilamentUseCase()

    @Provides
    fun generateOctoEverywhereLiveLinkUseCase() = SharedBaseInjector.get().generateOctoEverywhereLiveLinkUseCase()

    @Provides
    fun deleteFileUseCase() = SharedBaseInjector.get().deleteFileUseCase()

    @Provides
    fun moveFileUseCase() = SharedBaseInjector.get().moveFileUseCase()

    @Provides
    fun downloadFileUseCase() = SharedBaseInjector.get().downloadFileUseCase()

    @Provides
    fun createFolderUseCase() = SharedBaseInjector.get().createFolderUseCase()

    @Provides
    fun uploadFileUseCase() = SharedBaseInjector.get().uploadFileUseCase()

    @Provides
    fun applyTemperaturePresetUseCase() = SharedBaseInjector.get().applyTemperaturePresetUseCase()

}