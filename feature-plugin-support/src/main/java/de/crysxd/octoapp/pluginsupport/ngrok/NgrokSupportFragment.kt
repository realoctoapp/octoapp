package de.crysxd.octoapp.pluginsupport.ngrok

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.octoapp.pluginsupport.di.injectViewModel
import kotlinx.coroutines.flow.collect

class NgrokSupportFragment : Fragment() {

    private val viewModel by injectViewModel<NgrokSupportViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreatedFixed {
            viewModel.events.collect()
        }
    }
}