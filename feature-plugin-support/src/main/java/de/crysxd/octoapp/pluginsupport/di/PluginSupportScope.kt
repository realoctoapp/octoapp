package de.crysxd.octoapp.pluginsupport.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PluginSupportScope