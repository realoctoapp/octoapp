package de.crysxd.octoapp.pluginsupport.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.ViewModelFactory
import de.crysxd.octoapp.base.di.ViewModelKey
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.pluginsupport.mmu2filamentselect.Mmu2FilamentSelectSupportViewModel
import de.crysxd.octoapp.pluginsupport.ngrok.NgrokSupportViewModel
import de.crysxd.octoapp.pluginsupport.octolapse.OctolapseBottomSheetViewModel
import de.crysxd.octoapp.pluginsupport.octolapse.OctolapseSupportViewModel
import javax.inject.Provider

@Module
open class ViewModelModule {

    @Provides
    fun bindViewModelFactory(creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>): ViewModelProvider.Factory =
        ViewModelFactory(creators)

    @Provides
    @IntoMap
    @ViewModelKey(OctolapseBottomSheetViewModel::class)
    open fun provideOctolapseBottomSheetViewModel(
        printerEngineProvider: PrinterEngineProvider,
        printerConfigurationRepository: PrinterConfigurationRepository,
    ): ViewModel = OctolapseBottomSheetViewModel(
        printerEngineProvider = printerEngineProvider,
        printerConfigurationRepository = printerConfigurationRepository,
    )

    @Provides
    @IntoMap
    @ViewModelKey(OctolapseSupportViewModel::class)
    open fun provideOctolapseSupportViewModel(
        printerEngineProvider: PrinterEngineProvider,
        printerConfigurationRepository: PrinterConfigurationRepository,
        octoPreferences: OctoPreferences,
    ): ViewModel = OctolapseSupportViewModel(
        printerEngineProvider = printerEngineProvider,
        printerConfigurationRepository = printerConfigurationRepository,
        octoPreferences = octoPreferences,
    )

    @Provides
    @IntoMap
    @ViewModelKey(NgrokSupportViewModel::class)
    open fun provideNgrokSupportViewModel(
        printerEngineProvider: PrinterEngineProvider,
        printerConfigurationRepository: PrinterConfigurationRepository,
        updateNgrokTunnelUseCase: UpdateNgrokTunnelUseCase,
    ): ViewModel = NgrokSupportViewModel(
        printerEngineProvider = printerEngineProvider,
        printerConfigurationRepository = printerConfigurationRepository,
        updateNgrokTunnelUseCase = updateNgrokTunnelUseCase,
    )

    @Provides
    @IntoMap
    @ViewModelKey(Mmu2FilamentSelectSupportViewModel::class)
    open fun provideMmu2FilamentSelectSupportViewModel(
        printerEngineProvider: PrinterEngineProvider,
        printerConfigurationRepository: PrinterConfigurationRepository,
    ): ViewModel = Mmu2FilamentSelectSupportViewModel(
        printerEngineProvider = printerEngineProvider,
        printerConfigurationRepository = printerConfigurationRepository,
    )
}