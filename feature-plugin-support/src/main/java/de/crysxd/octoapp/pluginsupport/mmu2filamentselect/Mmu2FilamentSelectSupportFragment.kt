package de.crysxd.octoapp.pluginsupport.mmu2filamentselect

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.compose.menu.MenuBottomSheetFragment
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.plugins.Mmu2FilamentSelectMenu
import de.crysxd.octoapp.pluginsupport.di.injectViewModel
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch

class Mmu2FilamentSelectSupportFragment : Fragment() {

    private val tag = "Mmu2FilamentSelectSupportFragment"
    private val viewModel by injectViewModel<Mmu2FilamentSelectSupportViewModel>()
    private val bottomSheetTag = "mmu2-filament-select-menu"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            viewModel.events.collect { event ->
                Napier.i(tag = tag, message = "Mmu2FilamentSelect event: $event")
                val currentFragment = childFragmentManager.findFragmentByTag(bottomSheetTag) as? MenuBottomSheetFragment
                val instanceId = SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()?.id ?: return@collect

                when (event) {
                    Mmu2FilamentSelectSupportViewModel.Event.CloseMenu -> currentFragment?.dismiss()

                    is Mmu2FilamentSelectSupportViewModel.Event.ShowMenu -> if (currentFragment == null && lifecycle.currentState > Lifecycle.State.CREATED) {
                        MenuBottomSheetFragment.createForMenu(
                            menu = Mmu2FilamentSelectMenu(
                                instanceId = instanceId,
                                source = event.labelSource
                            ),
                            instanceId = instanceId,
                        ).show(childFragmentManager, bottomSheetTag)
                    }
                }
            }
        }.invokeOnCompletion {
            Napier.i(tag = tag, message = "Mmu2FilamentSelect event completed")
        }
    }
}