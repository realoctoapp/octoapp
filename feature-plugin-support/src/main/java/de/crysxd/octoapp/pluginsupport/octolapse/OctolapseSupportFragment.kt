package de.crysxd.octoapp.pluginsupport.octolapse

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.pluginsupport.di.injectViewModel
import io.github.aakira.napier.Napier

class OctolapseSupportFragment : Fragment() {

    private val tag = "OctolapseSupportFragment"
    private val viewModel by injectViewModel<OctolapseSupportViewModel>()
    private val bottomSheetTag = "octolapse-bottom-sheet"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Napier.i(tag = tag, message = "Starting Octolapse support")

        lifecycleScope.launchWhenCreatedFixed {
            viewModel.events.collect {
                if (it.handled) return@collect
                it.handled = true

                Napier.i(tag = tag, message = "Octolapse event: $it")
                when (it) {
                    is OctolapseSupportViewModel.Event.OpenBottomSheet -> if (childFragmentManager.findFragmentByTag(bottomSheetTag) == null) {
                        try {
                            OctolapseBottomSheetFragment.create(it.plan).show(childFragmentManager, bottomSheetTag)
                        } catch (e: IllegalStateException) {
                            // State loss, ignore
                            Napier.w(tag = tag, message = "Unable to show dialog", throwable = e)
                        }
                    }

                    is OctolapseSupportViewModel.Event.ShowMessage ->
                        requireOctoActivity().showDialog(it.message)
                }
            }
        }
    }
}