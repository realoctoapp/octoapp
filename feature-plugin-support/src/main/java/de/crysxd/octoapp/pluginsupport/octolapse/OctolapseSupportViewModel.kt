package de.crysxd.octoapp.pluginsupport.octolapse

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.OctoActivity
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.OctolapsePluginMessage
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolapse.OctolapseSnapshotPlanPreview
import de.crysxd.octoapp.pluginsupport.R
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class OctolapseSupportViewModel(
    private val printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    private val octoPreferences: OctoPreferences,
) : ViewModel() {

    private val tag = "OctolapseSupportViewModel"
    private val mutableEvents = MutableSharedFlow<Event>(1)
    val events = mutableEvents.map { it }

    init {
        observeConnection()
        observeEvents()
    }

    private fun observeEvents() = viewModelScope.launch {
        printerEngineProvider.passiveCachedMessageFlow("octolapse-messages", OctolapsePluginMessage::class)
            .filterNotNull()
            .onEach { message ->
                message.errors?.firstOrNull()?.let { error ->
                    mutableEvents.tryEmit(
                        Event.ShowMessage(
                            OctoActivity.Message.DialogMessage(
                                title = { getString(R.string.octolapse___error___title) },
                                text = { listOf(error.name?.let { "<b>$it</b>" }, error.description).joinToString("<br><br>").toHtml() }
                            )
                        )
                    )
                }

                if (message.type == OctolapsePluginMessage.Type.GcodePreProcessingStart) {
                    mutableEvents.tryEmit(Event.OpenBottomSheet(null))
                }
            }.saveCollect()
    }

    private fun observeConnection() = viewModelScope.launch {
        printerEngineProvider.passiveConnectionEventFlow("octolapse-conneciton").onEach {
            try {
                when {
                    !octoPreferences.activePluginIntegrations -> Napier.i(
                        tag = tag,
                        message = "Connected but skipping Octolapse state check, no active plugin integrations allowed"
                    )

                    printerConfigurationRepository.getActiveInstanceSnapshot()?.hasPlugin(OctoPlugins.Octolapse) != true -> Napier.i(
                        tag = tag,
                        message = "Octolapse not installed"
                    )

                    else -> {
                        // Delay slightly so other more important requests go first
                        val delay = 5.seconds
                        Napier.i(tag = tag, message = "Connected, checking current Octolapse state after $delay")
                        delay(delay)
                        printerEngineProvider.printer().asOctoPrint().octolapseApi.getSettingsAndState().snapshotPlanPreview?.let {
                            mutableEvents.tryEmit(Event.OpenBottomSheet(it))
                        }
                    }
                }
            } catch (e: Exception) {
                // Nice try, we don't care. This is considered optional
                Napier.e(tag = tag, message = "Octolapse  state check failed", throwable = e)
            }
        }.saveCollect()
    }

    private suspend fun Flow<*>.saveCollect() = retry {
        delay(1000)
        Napier.e(tag = tag, message = "Failed", throwable = it)
        true
    }.collect()

    sealed class Event {
        var handled = false

        data class ShowMessage(val message: OctoActivity.Message.DialogMessage) : Event()
        data class OpenBottomSheet(val plan: OctolapseSnapshotPlanPreview?) : Event()
    }
}