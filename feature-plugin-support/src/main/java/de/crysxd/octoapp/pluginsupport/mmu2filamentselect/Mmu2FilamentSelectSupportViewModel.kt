package de.crysxd.octoapp.pluginsupport.mmu2filamentselect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.engine.models.settings.Settings.Mmu.LabelSource
import de.crysxd.octoapp.engine.models.settings.Settings.Mmu.LabelSource.Manual
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn

class Mmu2FilamentSelectSupportViewModel(
    printerEngineProvider: PrinterEngineProvider,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
) : ViewModel() {

    private val tag = "Mmu2FilamentSelectSupportViewModel"
    val events = printerEngineProvider.passiveCachedMessageFlow(tag = "mmu2-filament-select-support", CompanionPluginMessage::class)
        .map { it?.mmuSelectionActive == true }
        .distinctUntilChanged()
        .map {
            Napier.i(tag = tag, message = "MMU change: $it")
            val source = printerConfigurationRepository.getActiveInstanceSnapshot()?.settings?.plugins?.mmu?.labelSource ?: Manual
            when (it) {
                true -> Event.ShowMenu(source)
                false -> Event.CloseMenu
            }
        }.retry {
            Napier.e(tag = tag, message = "Failed", throwable = it)
            delay(1000)
            true
        }.shareIn(viewModelScope, started = SharingStarted.Lazily, replay = 1)

    sealed class Event {
        data class ShowMenu(val labelSource: LabelSource) : Event()
        data object CloseMenu : Event()
    }
}