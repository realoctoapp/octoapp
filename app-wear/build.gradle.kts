import com.github.triplet.gradle.androidpublisher.ReleaseStatus
import de.crysxd.octoapp.buildscript.OctoAppBuildConfig
import de.crysxd.octoapp.buildscript.getPlatyCredentials
import de.crysxd.octoapp.buildscript.getPlayFraction
import de.crysxd.octoapp.buildscript.getPlayTrack
import de.crysxd.octoapp.buildscript.getPlayUpdatePriority
import de.crysxd.octoapp.buildscript.getVersionCodeWear
import de.crysxd.octoapp.buildscript.getVersionName
import de.crysxd.octoapp.buildscript.octoAppAndroidApp

plugins {
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinComposeCompiler)
    alias(libs.plugins.googleServices)
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.firebaseCrashlytics)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.playPublisher)
}

octoAppAndroidApp(getVersionCodeWear()) {
    defaultConfig {
        minSdk = maxOf(25, OctoAppBuildConfig.minSdk)
    }
}

play {
    serviceAccountCredentials.set(getPlatyCredentials())
    track.set("wear:" + getPlayTrack())
    defaultToAppBundles.set(true)
    releaseStatus.set(if (getPlayFraction() >= 1.0) ReleaseStatus.COMPLETED else ReleaseStatus.IN_PROGRESS)
    updatePriority.set(getPlayUpdatePriority())
    userFraction.set(getPlayFraction())
    releaseName.set(getVersionName())
}

dependencies {
    implementation(projects.base)
    implementation(libs.androidx.core.ktx)

    // Wear specifics
    implementation(libs.wear.core)
    implementation(libs.wear.remoteinteractions)
    implementation(libs.wear.phoneinteractions)

    // Compose - Update later to BOM version
    // Do not include! implementation "androidx.compose.material:material:$composeVersion"
    val composeVersion = "1.2.1"
    implementation("androidx.compose.ui:ui:$composeVersion")
    implementation("androidx.compose.ui:ui:$composeVersion")
    implementation("androidx.compose.ui:ui-tooling:$composeVersion")
    implementation("androidx.compose.foundation:foundation:$composeVersion")
    implementation("androidx.activity:activity-compose:1.5.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.5.1")
    implementation("androidx.compose.ui:ui-tooling:$composeVersion")
    implementation("androidx.compose.animation:animation-graphics:$composeVersion")
    implementation("androidx.compose.ui:ui-text:$composeVersion")
    implementation("androidx.compose.material:material-icons-extended:$composeVersion")

    // Compose for Wear
    implementation(libs.wear.compose.foundation)
    implementation(libs.wear.compose.material)

    // Others
    implementation(libs.accompanist.pager)
    implementation(libs.androidx.splashscreen)
    implementation(libs.androidx.profiles)
}