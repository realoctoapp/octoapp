package de.crysxd.octoapp.wear

import android.content.Context
import android.net.Uri
import androidx.annotation.StringRes
import com.google.android.gms.wearable.CapabilityClient
import com.google.android.gms.wearable.DataEventBuffer
import com.google.android.gms.wearable.Node
import com.google.android.gms.wearable.Wearable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.blockingAwait
import de.crysxd.octoapp.base.ext.import
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.di.WearScope
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@WearScope
class WearDataLayerService @Inject constructor(private val context: Context) {
    private val tag = "WearDataLayerService"
    private val dataClient = Wearable.getDataClient(context)
    private val capabilityClients = Wearable.getCapabilityClient(context)
    val nodeCount = MutableStateFlow<Int?>(null)
    private var pullJob: Job? = null

    init {
        AppScope.launch {
            try {
                // Pull configuration after start up, but delay to not overload the watch
                // Skip delay if we have no configurations
                if (BaseInjector.get().octorPrintRepository().getAll().isNotEmpty()) {
                    delay(15_000)
                }
                pullConfiguration()

                // Keep updated with data
                Napier.i(tag = tag, message = "Waiting for Wear OS requests")
                dataClient.addListener(::onDataReceived)

                // Keep updated with nodes
                capabilityClients.addListener(
                    {
                        AppScope.launch {
                            try {
                                searchNodes()
                            } catch (e: Exception) {
                                Napier.e(tag = tag, message = "Failed to search nodes", throwable = e)
                            }
                        }
                    },
                    context.getString(R.string.rpc_capability___configuration_producer)
                )
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Error in wear data layer", throwable = e)
            }
        }
    }

    fun pullConfiguration() {
        if (pullJob?.isActive != true) {
            Napier.d(tag = tag, message = "Pulling configuration...")
            pullJob = doPullConfiguration()
        } else {
            Napier.d(tag = tag, message = "Skipping configuration pull, already active")
        }
    }

    private suspend fun searchNodes(): List<Node> {
        Napier.i(tag = tag, message = "Searching for nodes...")

        val nodes = Wearable.getCapabilityClient(context)
            .getCapability(context.getString(R.string.rpc_capability___configuration_producer), CapabilityClient.FILTER_REACHABLE)
            .blockingAwait()
            .nodes
            .sortedByDescending { it.isNearby }

        Napier.i(tag = tag, message = "${nodes.size} nodes online")
        nodeCount.emit(nodes.size)
        return nodes
    }

    private fun doPullConfiguration() = AppScope.launch {
        val node = try {
            searchNodes().firstOrNull() ?: return@launch Napier.w(tag = tag, message = "No node online")
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "Failed to search nodes", throwable = e)
            return@launch
        }

        fun uriFromPath(@StringRes path: Int) = Uri.parse("wear:${context.getString(path)}").buildUpon().authority(node.id).build()

        try {
            val uri = uriFromPath(R.string.rpc_asset_path___instance_configuration)
            Napier.i(tag = tag, message = "Requesting data item $uri")
            val data = dataClient.getDataItem(uri).blockingAwait()?.data
                ?: return@launch Napier.i(tag = tag, message = "No config available")
            saveConfiguration(data)
        } catch (e: java.lang.Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }

        try {
            val uri = uriFromPath(R.string.rpc_asset_path___settings)
            Napier.i(tag = tag, message = "Requesting data item $uri")
            val data = dataClient.getDataItem(uri).blockingAwait()?.data
                ?: return@launch Napier.i(tag = tag, message = "No settings available")
            saveSettings(data)
        } catch (e: java.lang.Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }

        try {
            val uri = uriFromPath(R.string.rpc_asset_path___dns_cache)
            Napier.i(tag = tag, message = "Requesting data item $uri")
            val data = dataClient.getDataItem(uri).blockingAwait()?.data
                ?: return@launch Napier.i(tag = tag, message = "No DNS cache available")
            saveDnsCache(data)
        } catch (e: java.lang.Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }

        try {
            val uri = uriFromPath(R.string.rpc_asset_path___gcode_history)
            Napier.i(tag = tag, message = "Requesting data item $uri")
            val data = dataClient.getDataItem(uri).blockingAwait()?.data
                ?: return@launch Napier.i(tag = tag, message = "No Gcode history cache available")
            saveGcodeHistory(data)
        } catch (e: java.lang.Exception) {
            Napier.e(tag = tag, message = "Failed", throwable = e)
        }
    }

    private fun onDataReceived(events: DataEventBuffer) {
        Napier.i(tag = tag, message = "Received data events")

        events.forEach { event ->
            val data = event.dataItem.data ?: return@forEach Napier.i(tag = tag, message = "Skipping ${event.dataItem.uri.path} because data is empty")
            when (event.dataItem.uri.path) {
                context.getString(R.string.rpc_asset_path___bug_report_bundle) -> Unit // Ignore, that's us sending it
                context.getString(R.string.rpc_asset_path___instance_configuration) -> saveConfiguration(data)
                context.getString(R.string.rpc_asset_path___settings) -> saveSettings(data)
                context.getString(R.string.rpc_asset_path___dns_cache) -> saveDnsCache(data)
                context.getString(R.string.rpc_asset_path___gcode_history) -> saveGcodeHistory(data)
                else -> Napier.e(tag = tag, message = "Failed", throwable = SuppressedIllegalStateException("Unknown data event path ${event.dataItem.uri} received"))
            }
        }
    }

    private fun saveSettings(data: ByteArray) = AppScope.launch {
        Napier.i(tag = tag, message = "Saving ${data.size} byte settings")
        BaseInjector.get().octoPreferences().import(data)

        // Ensure always null, we don't have this concept on wear
        BaseInjector.get().octoPreferences().activeInstanceId = null
    }

    private fun saveDnsCache(data: ByteArray) = AppScope.launch(Dispatchers.IO) {
        Napier.i(tag = tag, message = "Saving ${data.size} byte DNS cache")
        val resolver = BaseInjector.get().localDnsResolver() as? CachedLocalDnsResolver ?: return@launch
        resolver.importCache(data)
    }

    private fun saveGcodeHistory(data: ByteArray) = AppScope.launch(Dispatchers.IO) {
        Napier.i(tag = tag, message = "Saving ${data.size} byte Gcode history")
        val repo = BaseInjector.get().gcodeHistoryRepository()
        repo.import(Gson().fromJson(String(data), object : TypeToken<List<GcodeHistoryItem>>() {}.type))
    }

    private fun saveConfiguration(data: ByteArray) = AppScope.launch {
        Napier.i(tag = tag, message = "Saving ${data.size} byte configuration")
        BaseInjector.get().octorPrintRepository().atomicChange {
            val local = getAll()
            val incoming = BaseInjector.get().octoPrintInstanceInformationSerializer().deserialize(data)
            val merged = listOf(incoming.map { it.id }, local.map { it.id }).flatten().distinct().mapNotNull { instanceId ->
                val i = incoming.firstOrNull { it.id == instanceId }
                val l = local.firstOrNull { it.id == instanceId }

                when {
                    i == null -> {
                        Napier.d(tag = tag, message = "Local instance of ${l?.id} has no matching incoming instance, deleting")
                        null
                    }

                    l == null -> {
                        Napier.d(tag = tag, message = "Local instance of null is outdated, importing")
                        i
                    }

                    (l.capabilitiesFetchedAt ?: 0) > (i.capabilitiesFetchedAt ?: 0) -> {
                        Napier.d(tag = tag, message = "Local instance of ${l.id} is newer, skipping import")
                        l.copy(appSettings = i.appSettings)
                    }

                    else -> {
                        Napier.d(tag = tag, message = "Upgrading local instance of ${l.id} with incoming")
                        i
                    }
                }
            }

            setAll(merged)
            Napier.i(tag = tag, message = "Now having ${BaseInjector.get().octorPrintRepository().getAll().size} instances")
        }
    }
}
