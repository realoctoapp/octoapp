package de.crysxd.octoapp.ui.screens.main

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.Crossfade
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.core.content.edit
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.wear.ambient.AmbientModeSupport
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_WEAR_OS_APP
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.ui.framework.AmbientModeSupport
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import java.util.concurrent.TimeUnit
import kotlin.time.Duration.Companion.seconds

class MainActivity : AppCompatActivity(), AmbientModeSupport.AmbientCallbackProvider {

    private val tag = "MainActivity"
    private lateinit var ambientController: AmbientModeSupport.AmbientController
    private val ambientModeStartTime = mutableStateOf(0L)
    private val keyAvailable = "available"

    override fun onCreate(savedInstanceState: Bundle?) {
        // Handle the splash screen transition.
        var starting = savedInstanceState == null
        installSplashScreen().setKeepOnScreenCondition { starting }

        super.onCreate(savedInstanceState)
        ambientController = AmbientModeSupport.attach(this)
        ambientController.setAmbientOffloadEnabled(false)

        val cachedAvailability = getSharedPreferences().getBoolean(keyAvailable, false)
        val featureAvailableFlow = BillingManager.isFeatureEnabledFlow(FEATURE_WEAR_OS_APP)
            .distinctUntilChanged()
            .onEach { getSharedPreferences().edit { putBoolean(keyAvailable, it) } }
        val billingAvailableFlow = BillingManager.state
            .map { it.isBillingAvailable }
            .distinctUntilChanged()

        setContent {
            //region Main UI
            val featureAvailable by featureAvailableFlow.collectAsState(initial = cachedAvailability)
            val state = when {
                !featureAvailable -> State.Disabled
                else -> State.Enabled
            }

            // To be compliant with Google Wear quality guidelines :)
            LaunchedEffect(Unit) {
                delay(0.5.seconds)
                starting = false
            }

            SideEffect {
                window.setBackgroundDrawable(ColorDrawable(Color.BLACK))
            }


            Crossfade(targetState = state) {
                when (it) {
                    State.Disabled -> {
                        Napier.i(tag = tag, message = "BillingManager reports app disabled, moving to disabled state")
                        val billingAvailable by billingAvailableFlow.collectAsState(initial = false)
                        WearAppDisabledUi { billingAvailable }
                    }

                    State.Enabled -> {
                        Napier.i(tag = tag, message = "BillingManager reports app enabled")
                        AmbientModeSupport(ambientStartTime = ambientModeStartTime.value) {
                            MainUi(ambientModeStartTime.value > 0)
                        }
                    }
                }
            }
            //endregion
        }
    }

    private fun getSharedPreferences() = getSharedPreferences("billing-cache", MODE_PRIVATE)

    override fun onResume() {
        super.onResume()
        BillingManager.onResume()
        WearInjector.get().wearDataLayerService().pullConfiguration()
    }

    override fun onPause() {
        super.onPause()
        BillingManager.onPause()
    }

    //region Ambient
    override fun getAmbientCallback(): AmbientModeSupport.AmbientCallback = MyAmbientCallback()

    private inner class MyAmbientCallback : AmbientModeSupport.AmbientCallback() {

        private val minAmbientInterval = TimeUnit.MINUTES.toMillis(5)
        private var lastAmbientUpdate = 0L

        init {
            Napier.i(tag = tag, message = "Ambient prepared!")
        }

        override fun onEnterAmbient(ambientDetails: Bundle) {
            Napier.i(
                tag = tag, message = "Enter ambient: ${
                ambientDetails.keySet().associateWith {
                    @Suppress("DEPRECATION")
                    ambientDetails.get(it)
                }
            }")
            setAmbientMode(true)
            lastAmbientUpdate = 0L
        }

        override fun onExitAmbient() {
            Napier.i(tag = tag, message = "Exit ambient")
            setAmbientMode(false)
            lastAmbientUpdate = 0L
        }

        override fun onUpdateAmbient() {
            if (System.currentTimeMillis() - lastAmbientUpdate > minAmbientInterval) {
                Napier.i(tag = tag, message = "Updated ambient")
                setAmbientMode(true)
                lastAmbientUpdate = System.currentTimeMillis()
            } else {
                Napier.i(tag = tag, message = "Skipping ambient update, too soon")
            }
        }

        fun setAmbientMode(ambientMode: Boolean) {
            ambientModeStartTime.value = if (ambientMode) System.currentTimeMillis() else 0L
        }
    }
    //endregion

    private sealed class State {
        data object Disabled : State()
        data object Enabled : State()
    }
}