package de.crysxd.octoapp.ui.framework

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Battery0Bar
import androidx.compose.material.icons.rounded.Battery1Bar
import androidx.compose.material.icons.rounded.Battery2Bar
import androidx.compose.material.icons.rounded.Battery3Bar
import androidx.compose.material.icons.rounded.Battery4Bar
import androidx.compose.material.icons.rounded.Battery5Bar
import androidx.compose.material.icons.rounded.Battery6Bar
import androidx.compose.material.icons.rounded.BatteryAlert
import androidx.compose.material.icons.rounded.BatteryChargingFull
import androidx.compose.material.icons.rounded.BatteryFull
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.AlarmManagerCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.FcmPrintEvent
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.ext.injectCurrentMessage
import de.crysxd.octoapp.notification.FcmPrintEventReceiver
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsEta
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.utils.GetAmbientModeStateUseCase
import de.crysxd.octoapp.utils.isRound
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Instant
import java.text.DateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

private const val TAG = "AmbientSupport"

@Composable
fun AmbientModeSupport(
    ambientStartTime: Long,
    normalUi: @Composable () -> Unit
) {
    val vm = viewModel(
        modelClass = AmbientModeViewModel::class.java,
        factory = AmbientModeViewModelFactory(),
    )

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
    ) {
        normalUi()

        if (ambientStartTime != 0L) {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.Black)
            ) {
                Spacer(Modifier.weight(0.05f))
                Time(Modifier.weight(0.1f))
                Spacer(Modifier.weight(0.05f))
                AmbientState(ambientStartTime = ambientStartTime, modifier = Modifier.weight(if (isRound) 0.7f else 0.84f), vm = vm)
                Spacer(Modifier.weight(0.01f))
                if (isRound) {
                    Battery(Modifier.weight(0.14f))
                }
            }
        }
    }
}

@Composable
private fun Battery(modifier: Modifier = Modifier) = Row(
    modifier = modifier.fillMaxWidth(),
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.Center,
) {
    val context = LocalContext.current
    var batteryLevel by remember { mutableStateOf(-1) }
    var charging by remember { mutableStateOf(false) }
    val icon = when {
        charging -> Icons.Rounded.BatteryChargingFull
        batteryLevel < 0 -> Icons.Rounded.BatteryAlert
        batteryLevel in 0..10 -> Icons.Rounded.Battery0Bar
        batteryLevel in 10..20 -> Icons.Rounded.Battery1Bar
        batteryLevel in 20..30 -> Icons.Rounded.Battery2Bar
        batteryLevel in 30..40 -> Icons.Rounded.Battery3Bar
        batteryLevel in 40..55 -> Icons.Rounded.Battery4Bar
        batteryLevel in 55..70 -> Icons.Rounded.Battery5Bar
        batteryLevel in 70..85 -> Icons.Rounded.Battery6Bar
        else -> Icons.Rounded.BatteryFull
    }

    DisposableEffect(Unit) {
        val batteryReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
                charging = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) != 0
            }
        }

        context.registerReceiver(batteryReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))

        // Cancel everything
        onDispose {
            context.unregisterReceiver(batteryReceiver)
        }
    }

    if (batteryLevel > 0) {
        Icon(
            imageVector = icon,
            contentDescription = null,
            modifier = Modifier
                .size(12.dp)
                .rotate(90f)
                .offset(x = 1.dp)
        )
        Spacer(modifier = Modifier.width(OctoAppTheme.dimens.margin0))
        Text(text = batteryLevel.formatAsPercent(), style = OctoAppTheme.typography.labelSmall)
    }
}

@Composable
private fun Time(modifier: Modifier = Modifier) = with(LocalDensity.current) {
    var fontSize by remember { mutableStateOf(1.sp) }
    var time by remember { mutableStateOf("") }
    val context = LocalContext.current

    DisposableEffect(Unit) {
        val action = "de.crysxd.octoapp.TIME_UPDATE"
        val ambientUpdateAlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val ambientUpdatePendingIntent = Intent(action).let { ambientUpdateIntent ->
            PendingIntent.getBroadcast(context, 0, ambientUpdateIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
        }
        val ambientUpdateBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                time = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault()).format(Date())
                val calendar = Calendar.getInstance()
                calendar.add(Calendar.MINUTE, 5)
                calendar.set(Calendar.SECOND, 0)
                calendar.set(Calendar.MILLISECOND, 0)
                val nextTickTime = calendar.time.time

                AlarmManagerCompat.setAndAllowWhileIdle(
                    context.getSystemService(AlarmManager::class.java),
                    AlarmManager.RTC_WAKEUP,
                    nextTickTime,
                    ambientUpdatePendingIntent
                )
            }
        }


        // Register for updates + initial update to schedule next
        IntentFilter(action).also { filter ->
            ContextCompat.registerReceiver(context, ambientUpdateBroadcastReceiver, filter, ContextCompat.RECEIVER_NOT_EXPORTED)
            ambientUpdateBroadcastReceiver.onReceive(context, Intent())
        }

        // Cancel everything
        onDispose {
            context.unregisterReceiver(ambientUpdateBroadcastReceiver)
            ambientUpdateAlarmManager.cancel(ambientUpdatePendingIntent)
        }
    }

    Text(
        modifier = modifier
            .fillMaxWidth()
            .onGloballyPositioned {
                fontSize = it.size.height.toSp()
            },
        text = time,
        textAlign = TextAlign.Center,
        style = OctoAppTheme.typography.titleLarge.copy(fontSize = fontSize, fontWeight = FontWeight.ExtraLight)
    )
}

@Composable
private fun AmbientState(
    ambientStartTime: Long,
    vm: AmbientModeViewModel,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
    ) {
        LaunchedEffect(key1 = Unit) { vm.clearOld() }
        LaunchedEffect(key1 = ambientStartTime) { vm.refresh() }
        val states by vm.ambientModeState.collectAsState(initial = emptyList())

        when (states.size) {
            0 -> Unit
            1 -> SingleAmbientState(states.first())
            else -> MultiAmbientState(states)
        }
    }
}

@Composable
private fun MultiAmbientState(states: List<AmbientData>) = with(LocalDensity.current) {
    val lineMaxCount = 2
    val line1 = states.take(lineMaxCount)
    val line2 = states.drop(lineMaxCount)
    val lines = listOf(line1, line2)
    var width by remember { mutableStateOf(0.dp) }

    Napier.i(tag = TAG, message = "States: $states")
    lines.filter { it.isNotEmpty() }.forEachIndexed { i, lineStates ->
        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxHeight(if (i == 0) 0.5f else 1f)
                .fillMaxWidth()
                .padding(horizontal = if (isRound) OctoAppTheme.dimens.margin1 else OctoAppTheme.dimens.margin0, vertical = OctoAppTheme.dimens.margin0)
                .onGloballyPositioned { width = it.size.width.toDp() }
        ) {
            lineStates.forEach { state ->
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .width(width / lineMaxCount)
                        .fillMaxHeight()
                        .padding(horizontal = OctoAppTheme.dimens.margin0)
                        .border(width = 1.dp, color = Color.White.copy(alpha = 0.3f), shape = MaterialTheme.shapes.medium)
                        .padding(OctoAppTheme.dimens.margin0)
                ) {
                    Text(
                        text = state.state,
                        maxLines = 1,
                        textAlign = TextAlign.Center,
                        style = OctoAppTheme.typography.labelLarge.also { it.copy(lineHeight = it.fontSize) },
                    )
                    Text(
                        text = listOfNotNull(state.detail, state.label).joinToString("\n"),
                        maxLines = 2,
                        textAlign = TextAlign.Center,
                        style = OctoAppTheme.typography.labelSmall,
                    )
                }
            }
        }
    }
}

@Composable
private fun SingleAmbientState(state: AmbientData) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxHeight()
            .padding(OctoAppTheme.dimens.margin2)
    ) {
        val largeStyle = OctoAppTheme.typography.titleLarge
        val smallStyle = OctoAppTheme.typography.title
        var reducedTextSizeFor by remember { mutableStateOf("") }
        val textStyle = if (state.state.length > 6 || reducedTextSizeFor == state.state) smallStyle else largeStyle

        Text(
            text = state.state,
            modifier = Modifier.fillMaxWidth(),
            style = textStyle,
            textAlign = TextAlign.Center,
            overflow = TextOverflow.Clip,
            onTextLayout = { textLayoutResult ->
                if (textLayoutResult.didOverflowWidth) {
                    reducedTextSizeFor = state.state
                }
            },
        )

        state.detail?.let {
            Text(
                text = it,
                style = OctoAppTheme.typography.base,
                color = MaterialTheme.colors.onBackground.copy(alpha = 0.8f),
                textAlign = TextAlign.Center
            )
        }
    }
}

@Suppress("UNCHECKED_CAST")
private class AmbientModeViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = AmbientModeViewModel() as T
}

@OptIn(ExperimentalCoroutinesApi::class)
private class AmbientModeViewModel : ViewModel() {

    init {
        Napier.i(tag = TAG, message = "Create ambient VM")
    }

    private val fcmEventReceiver = FcmPrintEventReceiver(viewModelScope)
    private val lastMessages = mutableMapOf<String, Message.Current?>()
    private val refreshTrigger = MutableSharedFlow<Unit>(replay = 1)
    val ambientModeState = BaseInjector.get().octorPrintRepository()
        .allInstanceInformationFlow()
        .distinctUntilChangedBy { it.values.map { it.id + it.label } }
        .combine(refreshTrigger) { it, _ -> it.values.toList().sortedBy { it.label } }
        .map { instances ->
            Napier.i(tag = TAG, message = "Update ${instances.size} instances")
            instances.map { createInstanceFlow(it) }
        }.flatMapLatest { flows ->
            combine(flows) { it.toList() }
        }.onEach {
            Napier.i(tag = TAG, message = "New state: $it")
        }.shareIn(viewModelScope, started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 1000))

    fun refresh() {
        viewModelScope.launch {
            Napier.i(tag = TAG, message = "Refreshing")
            refreshTrigger.emit(Unit)
        }
    }

    fun clearOld() {
        Napier.i(tag = TAG, message = "Clearing old")
        lastMessages.clear()
    }

    private fun createInstanceFlow(instance: PrinterConfigurationV3) = flow {
        emit(null)
        emit(loadInstanceState(instance))
    }.combine(fcmEventReceiver.events.map { it[instance.id] }) { loaded, fcm ->
        val current = loaded ?: lastMessages[instance.id]?.also { Napier.i(tag = TAG, message = "Reused old current message: $it") } ?: peekCurrentMessageFlow(instance)
        current?.let { fcm?.upgradeCurrentMessage(it) } ?: current
    }.onEach { current ->
        lastMessages[instance.id] = current
        current?.let { injectCurrentMessage(instance.id, it) }
    }.map {
        it.toAmbientState(instance)
    }.catch {
        Napier.e(tag = TAG, throwable = it, message = "Failed to load ambient state")
        val context = BaseInjector.get().localizedContext()
        emit(AmbientData(label = instance.label, instanceId = instance.id, offline = true, state = context.getString(R.string.wear___ambient_state___offline)))
    }

    private suspend fun peekCurrentMessageFlow(instance: PrinterConfigurationV3) = withTimeoutOrNull(100) {
        // Do not use wear here as we are the source of additional data in wearPassiveCurrentFlow() :)
        BaseInjector.get().octoPrintProvider().passiveCurrentMessageFlow(tag = "ambient-vm", instanceId = instance.id).firstOrNull()?.also {
            Napier.i(tag = TAG, message = "Took state from current flow: $it")
        }
    }

    suspend fun loadInstanceState(instance: PrinterConfigurationV3) = withTimeoutOrNull(10_000) {
        WearInjector.get().getAmbientModeStateUseCase().execute(GetAmbientModeStateUseCase.Params(instance.id))
    }?.also {
        Napier.i(tag = TAG, message = "Loaded state from server: $it")
    }

    private fun FcmPrintEvent.upgradeCurrentMessage(currentMessage: Message.Current): Message.Current {
        return if (serverTimeFixed > currentMessage.serverTime) {
            currentMessage.copy(
                progress = currentMessage.progress?.copy(
                    completion = progress ?: currentMessage.progress?.completion ?: 0f,
                    printTimeLeft = timeLeft ?: currentMessage.progress?.printTimeLeft ?: 0,
                ) ?: progress?.let { p ->
                    timeLeft?.let { t ->
                        ProgressInformation(
                            completion = p,
                            printTimeLeft = t,
                            printTime = 0,
                            printTimeLeftOrigin = null,
                            filepos = 0,
                        )
                    }
                },
                state = currentMessage.state.copy(
                    flags = PrinterState.Flags(
                        pausing = false,
                        cancelling = false,
                        operational = type == FcmPrintEvent.Type.Idle,
                        printing = type == FcmPrintEvent.Type.Printing,
                        paused = type == FcmPrintEvent.Type.Paused,
                        closedOrError = currentMessage.state.flags.closedOrError || type == FcmPrintEvent.Type.Error,
                        error = currentMessage.state.flags.error,
                        ready = currentMessage.state.flags.ready,
                        sdReady = currentMessage.state.flags.sdReady
                    )
                )
            )
        } else {
            currentMessage
        }
    }

    private fun Message.Current?.toAmbientState(instance: PrinterConfigurationV3): AmbientData {
        val context = BaseInjector.get().localizedContext()

        val (state, detail) = when {
            this == null -> context.getString(R.string.loading) to null
            state.flags.isPrinting() -> when {
                state.flags.paused -> context.getString(R.string.paused) to null
                state.flags.pausing -> context.getString(R.string.pausing) to null
                state.flags.cancelling -> context.getString(R.string.cancelling) to null
                else -> {
                    val state = progress?.completion?.formatAsPercent(maxDecimals = 0) ?: context.getString(R.string.wear___ambient_state___printing)
                    val detail = progress?.printTimeLeft?.formatAsSecondsEta(useCompactFutureDate = true, allowRelative = true, showLabel = true)
                    state to detail
                }
            }

            state.flags.isOperational() -> context.getString(R.string.wear___ambient_state___idle) to null
            state.flags.isError() -> context.getString(R.string.wear___ambient_state___error) to null
            else -> context.getString(R.string.wear___ambient_state___idle) to null
        }

        return AmbientData(
            label = instance.label,
            instanceId = instance.id,
            serverTime = this?.serverTime ?: Instant.DISTANT_PAST,
            detail = detail,
            state = state,
        )
    }
}

data class AmbientData(
    val label: String,
    val instanceId: String,
    val detail: String? = null,
    val state: String,
    val serverTime: Instant = Instant.DISTANT_PAST,
    val offline: Boolean = false,
)
