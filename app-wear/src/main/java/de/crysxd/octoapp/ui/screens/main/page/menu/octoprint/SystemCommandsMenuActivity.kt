package de.crysxd.octoapp.ui.screens.main.page.menu.octoprint

import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.core.os.bundleOf
import androidx.wear.compose.material.items
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import kotlinx.parcelize.Parcelize

class SystemCommandsMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        OctoScalingLazyColumn(state = scalingLazyListState) {
                            item {
                                MenuHeader(text = instance?.label ?: "OctoPrint")
                            }
                            items(instance?.systemCommands ?: emptyList()) {
                                ExecuteSystemCommandMenuItem(cmd = it, instanceId = instance!!.id)
                            }
                        }
                    }
                }
            }
        }
    }

    @Parcelize
    data class Args(
        val instanceId: String,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelableCompat<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}