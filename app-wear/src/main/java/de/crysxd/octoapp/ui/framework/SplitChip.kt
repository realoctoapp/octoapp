package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.ui.OctoAppTheme

@Composable
fun SplitChip(
    text: String,
    icon: ImageVector,
    actionIcon: ImageVector,
    textAction: suspend () -> Unit,
    iconAction: () -> Unit
) {
    Box(
        modifier = Modifier
            .height(52.dp)
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.small),
        contentAlignment = Alignment.CenterEnd
    ) {
        MenuItemChip(
            icon = icon,
            text = text,
            onClick = textAction,
            textModifier = Modifier.padding(end = OctoAppTheme.dimens.margin2)
        )
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(ratio = 1f)
                .background(Color.White.copy(alpha = 0.1f))
                .clickable(role = Role.Button) { iconAction() },
        ) {
            Icon(
                imageVector = actionIcon,
                contentDescription = null,
            )
        }
    }
}


@Preview(widthDp = 200, heightDp = 200, fontScale = 1.3f)
@Composable
private fun PreviewChips() = OctoAppTheme.ForOctoPrint(instance = PrinterConfigurationV3(id = "123", webUrl = "".toUrl(), apiKey = "")) {
    Column(
        verticalArrangement = Arrangement.spacedBy(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        SplitChip(
            text = "Some text isudfhi",
            icon = Icons.Rounded.LocalFireDepartment,
            actionIcon = Icons.Rounded.LocalFireDepartment,
            textAction = {},
            iconAction = {})
        SplitChip(text = "Some text", icon = Icons.Rounded.LocalFireDepartment, actionIcon = Icons.Rounded.LocalFireDepartment, textAction = {}, iconAction = {})
    }
}