package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.ui.OctoAppTheme

@Composable
fun TitleDetails(title: String? = null, detail: String? = null) {
    title?.let {
        Text(
            text = it,
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.title,
            modifier = Modifier.fillMaxWidth()
        )
    }

    detail?.let {
        Text(
            text = it,
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.labelSmall,
            color = MaterialTheme.colors.onBackground.copy(alpha = 0.7f),
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin0to1)
                .fillMaxWidth(),
        )
    }
}