package de.crysxd.octoapp.ui.screens.main.page.menu.temperature

import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.core.os.bundleOf
import androidx.wear.compose.material.ScalingLazyListScope
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import kotlinx.parcelize.Parcelize

class TemperaturePresetSubMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        OctoScalingLazyColumn(state = scalingLazyListState) {
                            content()
                        }
                    }
                }
            }
        }
    }

    private suspend fun applyTemps(
        applyExtruder: Boolean = false,
        applyBed: Boolean = false,
        applyChamber: Boolean = false,
        executeGcode: Boolean = false,
    ) {
        BaseInjector.get().applyTemperaturePresetUseCase().execute(
            ApplyTemperaturePresetUseCase.Params(
                instanceId = args.instanceId,
                profileName = args.profileName,
                executeGcode = executeGcode,
                componentFilter = { it.isChamber && applyChamber || it.isExtruder && applyExtruder || it.isBed && applyBed }
            )
        )

        showSuccess()
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun ScalingLazyListScope.content() {
        item {
            MenuHeader(text = args.profileName)
        }

        if (args.hasExtruder) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_preheat_hotend, args.profileName),
                    onClick = {
                        applyTemps(applyExtruder = true)
                    }
                )
            }
        }

        if (args.hasBed) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_preheat_bed, args.profileName),
                    onClick = {
                        applyTemps(applyBed = true)
                    }
                )
            }
        }

        if (args.hasChamber) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_preheat_chamber, args.profileName),
                    onClick = {
                        applyTemps(applyChamber = true)
                    }
                )
            }
        }

        if (args.hasGcode) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_execute_gcode, args.profileName),
                    onClick = {
                        applyTemps(executeGcode = true)
                    }
                )
            }
        }
    }

    @Parcelize
    data class Args(
        val profileName: String,
        val instanceId: String,
        val hasExtruder: Boolean,
        val hasBed: Boolean,
        val hasChamber: Boolean,
        val hasGcode: Boolean,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelableCompat<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}