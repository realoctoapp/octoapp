package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Check
import androidx.compose.material.icons.rounded.KeyboardBackspace
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.wear.compose.material.Button
import androidx.wear.compose.material.ButtonDefaults
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.dialog.Dialog
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ext.requireActivity
import de.crysxd.octoapp.ext.setScreenStaysOn
import de.crysxd.octoapp.ext.showFailure
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.utils.Constants
import de.crysxd.octoapp.utils.isRound
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import java.text.DecimalFormat

@Composable
fun ValueInputDialog(
    visible: Boolean,
    suffix: String? = null,
    callback: suspend (Float?) -> Unit,
) {
    val scope = rememberCoroutineScope()

    Dialog(
        showDialog = visible,
        onDismissRequest = { scope.launch { callback(null) } },
        content = { Input(suffix, callback) }
    )
}

@Composable
private fun Input(
    suffix: String? = null,
    callback: suspend (Float?) -> Unit,
) {
    val state = remember { mutableStateOf("") }
    var loading by remember { mutableStateOf(false) }
    val value by state
    val format = DecimalFormat("#")

    val bottomPadding = if (isRound) OctoAppTheme.dimens.margin3 else OctoAppTheme.dimens.margin1
    val horizontalPadding = if (isRound) OctoAppTheme.dimens.margin2 else OctoAppTheme.dimens.margin1
    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    if (loading) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Text(text = stringResource(id = R.string.loading))
        }
    } else {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    start = horizontalPadding,
                    end = horizontalPadding,
                    bottom = bottomPadding
                )
        ) {
            val haptic = LocalHapticFeedback.current
            Display(value = value, suffix = suffix)
            Keyboard(state = state) {
                scope.launch {
                    try {
                        loading = true
                        context.requireActivity().setScreenStaysOn(true)
                        val number = try {
                            format.parse(value)?.toFloat()
                        } catch (e: Exception) {
                            null
                        }
                        callback(number ?: 0f)
                    } catch (e: CancellationException) {
                        // Nothing
                    } catch (e: Exception) {
                        Napier.e(tag = "EnterValue", message = "Failed", throwable = e)
                        context.showFailure(e)
                    } finally {
                        context.requireActivity().setScreenStaysOn(false)
                        haptic.performHapticFeedback(HapticFeedbackType.LongPress)
                    }
                }
            }
        }
    }
}

@Composable
private fun Display(
    value: String,
    suffix: String?,
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .padding(horizontal = OctoAppTheme.dimens.margin1, vertical = OctoAppTheme.dimens.margin1to2)
            .fillMaxWidth()
    ) {
        Text(
            text = value.dropWhile { it == '0' }.takeUnless { it.isEmpty() } ?: "0",
            style = OctoAppTheme.typography.input,
        )
        suffix?.let {
            Text(
                text = it,
                style = OctoAppTheme.typography.input,
                modifier = Modifier
                    .alpha(0.7f)
                    .padding(start = OctoAppTheme.dimens.margin0)
            )
        }
    }
}

@Composable
private fun ColumnScope.Keyboard(
    state: MutableState<String>,
    action: () -> Unit
) {
    var value by state
    Row(modifier = Modifier.weight(1f, fill = true)) {
        KeyboardButton(text = "1") { value += "1" }
        KeyboardButton(text = "2") { value += "2" }
        KeyboardButton(text = "3") { value += "3" }
    }
    Row(modifier = Modifier.weight(1f, fill = true)) {
        KeyboardButton(text = "4") { value += "4" }
        KeyboardButton(text = "5") { value += "5" }
        KeyboardButton(text = "6") { value += "6" }
    }
    Row(modifier = Modifier.weight(1f, fill = true)) {
        KeyboardButton(text = "7") { value += "7" }
        KeyboardButton(text = "8") { value += "8" }
        KeyboardButton(text = "9") { value += "9" }
    }
    Row(modifier = Modifier.weight(1f, fill = true)) {
        KeyboardButton(
            icon = Icons.Rounded.KeyboardBackspace,
            background = Color.Transparent,
            foreground = MaterialTheme.colors.onBackground,
            action = { value = value.dropLast(1).takeUnless { it.isEmpty() } ?: "0" }
        )
        KeyboardButton(text = "0") { value += "0" }
        KeyboardButton(
            icon = Icons.Rounded.Check,
            background = MaterialTheme.colors.secondary,
            foreground = MaterialTheme.colors.onSecondary,
            action = action,
        )
    }
}

@Composable
private fun RowScope.KeyboardButton(
    text: String? = null,
    icon: ImageVector? = null,
    background: Color = MaterialTheme.colors.surface,
    foreground: Color = MaterialTheme.colors.onSurface,
    action: () -> Unit
) {
    val haptic = LocalHapticFeedback.current
    Button(
        onClick = {
            haptic.performHapticFeedback(HapticFeedbackType.TextHandleMove)
            action()
        },
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin0)
            .clip(MaterialTheme.shapes.small)
            .weight(1f, fill = true)
            .fillMaxHeight(),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = background,
            contentColor = foreground,
        )
    ) {
        if (text != null) {
            Text(
                text = text,
                style = OctoAppTheme.typography.button,
            )
        }

        if (icon != null) {
            Icon(
                imageVector = icon,
                contentDescription = text,
                modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin0, vertical = OctoAppTheme.dimens.margin0to1)
            )
        }
    }
}

@Composable
@Preview(
    name = "Preview",
    widthDp = Constants.PREVIEW_SIZE_DP,
    heightDp = Constants.PREVIEW_SIZE_DP,
    showBackground = true,
    backgroundColor = 0xFF000000,
    device = "id:wearos_large_round"
)
@Preview(
    name = "Preview",
    widthDp = Constants.PREVIEW_SIZE_DP,
    heightDp = Constants.PREVIEW_SIZE_DP,
    showBackground = true,
    backgroundColor = 0xFF000000,
    device = "id:wearos_small_round"
)
@Preview(
    name = "Preview",
    widthDp = Constants.PREVIEW_SIZE_DP,
    heightDp = Constants.PREVIEW_SIZE_DP,
    showBackground = true,
    backgroundColor = 0xFF000000,
)
fun InputPreview0() = OctoAppTheme.WithTheme {
    Input(suffix = "°C") {}
}