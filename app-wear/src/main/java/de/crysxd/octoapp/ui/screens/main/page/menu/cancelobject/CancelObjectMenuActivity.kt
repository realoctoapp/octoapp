package de.crysxd.octoapp.ui.screens.main.page.menu.cancelobject

import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.East
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.core.os.bundleOf
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.items
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.usecase.CancelObjectUseCase
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ext.wearCancelObjectFlow
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.Confirmation
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.rememberConfirmationState
import de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore
import kotlinx.parcelize.Parcelize

class CancelObjectMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        val objects by rememberCancelObjects()
                        OctoScalingLazyColumn(state = scalingLazyListState) {
                            content(objects = objects)
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun rememberCancelObjects(): State<List<CancelObjectViewModelCore.PrintObject>> {
        return BaseInjector.get().octoPrintProvider().wearCancelObjectFlow(instanceId = args.instanceId, tag = "cancel-object").collectAsState(initial = emptyList())
    }

    private fun ScalingLazyListScope.content(objects: List<CancelObjectViewModelCore.PrintObject>) {
        item {
            MenuHeader(text = stringResource(id = R.string.widget_cancel_object))
        }

        items(objects) { item ->
            val confirmationState by rememberConfirmationState()
            val name = item.label
            Confirmation(
                state = confirmationState,
                text = stringResource(id = R.string.cancel_object___confirmation_message, name),
                action = {
                    BaseInjector.get().cancelObjectUseCase().execute(
                        CancelObjectUseCase.Params(
                            instanceId = instanceId,
                            objectId = item.objectId,
                        )
                    )
                    showSuccess()
                }
            )

            val iconAlpha by animateFloatAsState(targetValue = if (item.active) 1f else 0f)

            MenuItemChip(
                icon = Icons.Rounded.East,
                iconModifier = Modifier.graphicsLayer { alpha = iconAlpha },
                text = remember(name, item.cancelled) {
                    buildAnnotatedString {
                        withStyle(if (item.cancelled) SpanStyle(textDecoration = TextDecoration.LineThrough) else SpanStyle()) {
                            append(name)
                        }
                    }
                },
                enabled = !item.cancelled,
                onClick = { confirmationState.visible = true }
            )
        }
    }

    @Parcelize
    data class Args(
        val instanceId: String,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelableCompat<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}
