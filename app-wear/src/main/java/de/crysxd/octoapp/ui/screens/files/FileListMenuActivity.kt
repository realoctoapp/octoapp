package de.crysxd.octoapp.ui.screens.files

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Folder
import androidx.compose.material.icons.rounded.InsertDriveFile
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.core.os.bundleOf
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.items
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.Confirmation
import de.crysxd.octoapp.ui.framework.ConfirmationState
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import de.crysxd.octoapp.ui.framework.rememberConfirmationState
import de.crysxd.octoapp.ui.screens.files.FileListViewModel.State
import kotlinx.parcelize.Parcelize

class FileListMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    private val selectFileRequestLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            val vm = viewModel(
                modelClass = FileListViewModel::class.java,
                key = "files/${args.instanceId}/${args.folder?.path}",
                factory = FileListViewModel.Factory(args.instanceId, args.folder)
            )

            val state by vm.state.collectAsState()
            val printing by vm.printing.collectAsState()
            if (printing) {
                finish()
            }

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        OctoScalingLazyColumn(state = scalingLazyListState) {
                            when (state) {
                                is State.Error -> error(state as State.Error, vm)
                                is State.Files -> files((state as State.Files).files)
                                State.Loading -> loading()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun ScalingLazyListScope.files(files: List<FileObject>) {
        if (files.isEmpty()) {
            emptyState()
        } else {
            fileList(files)
        }
    }

    private fun ScalingLazyListScope.emptyState() {
        if (args.folder == null) {
            item {
                Text(
                    style = OctoAppTheme.typography.title,
                    text = stringResource(R.string.file_manager___file_list___no_files_on_octoprint_title)
                )
            }
        } else {
            item {
                MenuHeader(text = args.folder?.display ?: "Unknown")
            }
            item {
                Text(
                    text = stringResource(R.string.file_manager___file_list___no_files_in_folder),
                    style = OctoAppTheme.typography.labelSmall,
                    modifier = Modifier.fillMaxSize(),
                    textAlign = TextAlign.Center,
                )
            }
        }
    }

    private fun ScalingLazyListScope.fileList(files: List<FileObject>) {
        item {
            MenuHeader(text = args.folder?.display ?: stringResource(id = R.string.file_manager___file_list___your_files))
        }

        items(files) {
            FileItem(file = it)
        }
    }

    @Composable
    fun FileItem(file: FileObject) {
        val printConfirmationState by rememberConfirmationState()
        val materialConfirmationState by rememberConfirmationState()
        val timelapseConfirmationState by rememberConfirmationState()
        var materialConfirmed by remember { mutableStateOf(false) }
        var timelapseConfirmed by remember { mutableStateOf(false) }
        if (file is FileObject.File) {
            suspend fun startPrint() = startPrintJob(
                fileObject = file,
                materialConfirmed = materialConfirmed,
                timelapseConfirmed = timelapseConfirmed,
                timelapseConfirmationState = timelapseConfirmationState,
                materialConfirmationState = materialConfirmationState,
            )

            Confirmation(
                state = printConfirmationState,
                text = stringResource(id = R.string.wear___file_list___confirm_start_print, file.display.take(64)),
                action = {
                    timelapseConfirmed = false
                    materialConfirmed = false
                    startPrint()
                }
            )
            Confirmation(
                state = timelapseConfirmationState,
                text = stringResource(R.string.wear___file_list___confirm_last_timelapse_config),
                action = {
                    timelapseConfirmed = true
                    startPrint()
                }
            )
            Confirmation(
                state = materialConfirmationState,
                text = stringResource(R.string.wear___file_list___confirm_no_material_selection),
                action = {
                    materialConfirmed = true
                    startPrint()
                }
            )
        }

        val context = LocalContext.current
        MenuItemChip(
            icon = if (file is FileObject.Folder) Icons.Rounded.Folder else Icons.Rounded.InsertDriveFile,
            text = file.display,
            onClick = {
                when (file) {
                    is FileObject.File -> {
                        printConfirmationState.visible = true
                    }

                    is FileObject.Folder -> Intent(context, FileListMenuActivity::class.java).apply {
                        putExtras(Args(args.instanceId, file).toBundle())
                        selectFileRequestLauncher.launch(this)
                    }
                }
            }
        )
    }

    private suspend fun startPrintJob(
        fileObject: FileObject.File,
        materialConfirmed: Boolean,
        timelapseConfirmed: Boolean,
        materialConfirmationState: ConfirmationState,
        timelapseConfirmationState: ConfirmationState,
    ) {
        val result = BaseInjector.get().startPrintJobUseCase().execute(
            StartPrintJobUseCase.Params(
                file = fileObject,
                materialSelectionConfirmed = materialConfirmed,
                timelapseConfigConfirmed = timelapseConfirmed,
                instanceId = args.instanceId
            )
        )

        when (result) {
            StartPrintJobUseCase.Result.PrintStarted -> {
                setResult(Activity.RESULT_OK)
                showSuccess()
                finish()
            }

            StartPrintJobUseCase.Result.MaterialSelectionRequired -> {
                materialConfirmationState.visible = true
            }

            StartPrintJobUseCase.Result.TimelapseConfigRequired -> {
                timelapseConfirmationState.visible = true
            }
        }
    }

    private fun ScalingLazyListScope.loading() {
        item {
            Text(
                text = stringResource(id = R.string.loading),
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxSize()
            )
        }
    }

    private fun ScalingLazyListScope.error(error: State.Error, vm: FileListViewModel) {
        item {
            Text(
                textAlign = TextAlign.Center,
                text = error.exception.composeErrorMessage().toString(),
                modifier = Modifier.fillMaxWidth(),
                style = OctoAppTheme.typography.labelSmall
            )
        }
        item {
            Text(
                text = stringResource(id = R.string.retry_general),
                textAlign = TextAlign.Center,
                style = OctoAppTheme.typography.button,
                modifier = Modifier
                    .padding(top = OctoAppTheme.dimens.margin1)
                    .clip(MaterialTheme.shapes.small)
                    .clickable { vm.loadFiles() }
                    .fillMaxWidth(),
            )
        }
    }


    @Parcelize
    data class Args(
        val instanceId: String,
        val folder: FileObject.Folder? = null,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelableCompat<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}