package de.crysxd.octoapp.ui

import androidx.annotation.ColorRes
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTagsAsResourceId
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.wear.compose.material.Colors
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Shapes
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.models.colors
import de.crysxd.octoapp.ext.toCompose
import de.crysxd.octoapp.ui.framework.LocalOctoPrint
import de.crysxd.octoapp.utils.isRound

data class OctoAppTheme(
    val colors: OctoAppThemeColors = OctoAppThemeColors(),
    val dimens: OctoAppThemeDimens = OctoAppThemeDimens(),
    val typography: OctoAppThemeTypography = OctoAppThemeTypography(),
) {

    companion object {
        private val LocalOctoAppTheme = compositionLocalOf { OctoAppTheme() }

        val dimens
            @Composable get() = LocalOctoAppTheme.current.dimens

        val typography
            @Composable get() = LocalOctoAppTheme.current.typography

        val colors
            @Composable get() = LocalOctoAppTheme.current.colors

        @Composable
        fun WithTheme(content: @Composable () -> Unit) =
            LocalOctoAppTheme.current.WithTheme(content = content)

        @Composable
        fun ForOctoPrint(instance: PrinterConfigurationV3?, content: @Composable () -> Unit) {
            val theme = if (isSystemInDarkTheme()) instance.colors.dark else instance.colors.light

            CompositionLocalProvider(LocalOctoPrint provides instance) {
                LocalOctoAppTheme.current.copy(
                    colors = colors.copy(
                        octoPrintColor = theme.main.toCompose(),
                        octoPrintColorLight = theme.accent.toCompose(),
                    )
                ).WithTheme(content)
            }
        }

        @Composable
        fun ForMenuStyle(menuStyle: MenuStyle, content: @Composable () -> Unit) {
            val themedColors = colors.copy(
                menuBackground = increasedAlphaColorResource(id = menuStyle.backgroundColorRes),
                menuForeground = increasedAlphaColorResource(id = menuStyle.foregroundColorRes),
            )

            LocalOctoAppTheme.current.copy(colors = themedColors).WithTheme(content)
        }

        @Composable
        // Watch screen's suck...increase alpha compared to main app
        private fun increasedAlphaColorResource(@ColorRes id: Int, boostFactor: Float = 1.5f) =
            colorResource(id).let { it.copy(alpha = (it.alpha * boostFactor).coerceAtMost(1f)) }

    }

    @Composable
    @OptIn(ExperimentalComposeUiApi::class)
    fun WithTheme(content: @Composable () -> Unit) {
        MaterialTheme(
            colors = Colors(
                primary = colorResource(id = R.color.primary),
                primaryVariant = colorResource(id = R.color.primary_dark),
                secondary = colorResource(id = R.color.accent),
                secondaryVariant = colorResource(id = R.color.accent),
                background = Color.Black,
                error = colorResource(id = R.color.color_error),
                onBackground = Color.White,
                onError = Color.White,
                onPrimary = Color.White,
                onSecondary = Color.White,
                onSurface = colorResource(id = R.color.normal_text),
                surface = colorResource(id = R.color.input_background),
            ),
            shapes = Shapes(
                small = RoundedCornerShape(percent = 50),
                medium = RoundedCornerShape(8.dp),
                large = RoundedCornerShape(14.dp),
            )
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .semantics {
//                        if(BuildConfig.BUILD_TYPE == "benchmark") {
                        testTagsAsResourceId = true
//                        }
                    }
                    .clip(shape = if (isRound) CircleShape else RectangleShape),
            ) {
                CompositionLocalProvider(LocalOctoAppTheme provides this@OctoAppTheme) {
                    content()
                }
            }
        }
    }

    sealed class MenuStyle(
        val foregroundColorRes: Int,
        val backgroundColorRes: Int,
    ) {
        data object Support : MenuStyle(R.color.menu_style_support_foreground, R.color.menu_style_support_background)
        data object Printer : MenuStyle(R.color.menu_style_printer_foreground, R.color.menu_style_printer_background)
        data object OctoPrint : MenuStyle(R.color.menu_style_octoprint_foreground, R.color.menu_style_octoprint_background)
        data object Neutral : MenuStyle(R.color.menu_style_neutral_foreground, R.color.menu_style_neutral_background)
        data object Settings : MenuStyle(R.color.menu_style_settings_foreground, R.color.menu_style_settings_background)
    }

    @Immutable
    data class OctoAppThemeColors(
        val menuForeground: Color = Color(0xFFEE3F3F),
        val menuBackground: Color = Color(0x26EE3F3F),
        val errorBackground: Color = Color(0xFFEE3F3F),
        val octoPrintColor: Color = Color.Magenta,
        val octoPrintColorLight: Color = Color.Magenta,
    )

    @Immutable
    data class OctoAppThemeDimens(
        val margin0: Dp = 2.dp,
        val margin0to1: Dp = 5.dp,
        val margin1: Dp = 10.dp,
        val margin1to2: Dp = 15.dp,
        val margin2: Dp = 20.dp,
        val margin3: Dp = 40.dp,
        val margin4: Dp = 60.dp,
        val margin5: Dp = 90.dp,
        val margin6: Dp = 130.dp,

        val surfaceCorners: Dp = 20.dp
    )

    @Immutable
    data class OctoAppThemeTypography(
        val base: TextStyle = TextStyle(
            fontSize = 14.sp,
            fontWeight = FontWeight.W400,
            fontFamily = FontFamily(
                fonts = listOf(
                    Font(
                        resId = R.font.roboto_light,
                        weight = FontWeight.Light,
                        style = FontStyle.Normal
                    ),
                    Font(
                        resId = R.font.roboto_regular,
                        weight = FontWeight.Normal,
                        style = FontStyle.Normal
                    ),
                    Font(
                        resId = R.font.roboto_medium,
                        weight = FontWeight.Medium,
                        style = FontStyle.Normal
                    ),
                )
            ),
        ),
        val accent: TextStyle = TextStyle(
            fontWeight = FontWeight.W400,
            fontSize = 14.sp,
            fontFamily = FontFamily(
                fonts = listOf(
                    Font(
                        resId = R.font.ubuntu_light,
                        weight = FontWeight.Light,
                        style = FontStyle.Normal
                    ),
                    Font(
                        resId = R.font.ubuntu_regular,
                        weight = FontWeight.Normal,
                        style = FontStyle.Normal
                    )
                )
            ),
        ),
        val label: TextStyle = base.copy(fontSize = 12.sp, fontWeight = FontWeight.Light),
        val labelSmall: TextStyle = base.copy(fontSize = 10.sp),
        val labelLarge: TextStyle = base.copy(fontSize = 16.sp, fontWeight = FontWeight.Light),
        val input: TextStyle = accent.copy(fontSize = 16.sp),
        val button: TextStyle = accent.copy(fontSize = 14.sp),
        val buttonSmall: TextStyle = base.copy(fontSize = 14.sp, fontWeight = FontWeight.Medium),
        val sectionHeader: TextStyle = base.copy(fontWeight = FontWeight.Medium),
        val title: TextStyle = accent.copy(fontSize = 17.sp, lineHeight = 20.sp),
        val titleLarge: TextStyle = accent.copy(fontSize = 40.sp, fontWeight = FontWeight.Light),
    )
}