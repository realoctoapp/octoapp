package de.crysxd.octoapp.ui.framework

import androidx.compose.runtime.compositionLocalOf
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3

val LocalOctoPrint = compositionLocalOf<PrinterConfigurationV3?> { null }