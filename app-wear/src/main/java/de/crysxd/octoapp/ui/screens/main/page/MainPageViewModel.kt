package de.crysxd.octoapp.ui.screens.main.page

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.ext.wearEventFlow
import de.crysxd.octoapp.ext.wearPassiveCurrentMessageFlow
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

class MainPageViewModel(instanceId: String) : ViewModel() {

    val instanceInformation = BaseInjector.get().octorPrintRepository()
        .instanceInformationFlow(instanceId = instanceId)
    val connection = BaseInjector.get().octoPrintProvider()
        .passiveConnectionEventFlow(tag = "main-page-connection", instanceId = instanceId)
    val current = BaseInjector.get().octoPrintProvider()
        .wearPassiveCurrentMessageFlow(tag = "main-page-event", instanceId = instanceId)
    val flags = current
        .map { it.state.flags }
        .distinctUntilChanged()
    val state = BaseInjector.get().octoPrintProvider()
        .wearEventFlow(tag = "main-page-state", instanceId = instanceId)

    init {
        Napier.i(tag = "MainPageViewModel", message = "New VM for $instanceId")
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(val instanceId: String) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T = MainPageViewModel(instanceId) as T
    }
}