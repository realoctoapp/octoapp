package de.crysxd.octoapp

import android.app.Application
import androidx.wear.phone.interactions.notifications.BridgingConfig
import androidx.wear.phone.interactions.notifications.BridgingManager
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.PlayBillingAdapter
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.NetworkModule
import de.crysxd.octoapp.base.di.SharedBaseComponent
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.logging.AndroidFirebaseAdapter
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.logging.FirebaseAntiLog
import de.crysxd.octoapp.base.logging.LogcatAntiLog
import de.crysxd.octoapp.base.migrations.AllMigrations
import de.crysxd.octoapp.base.migrations.AllSharedMigrations
import de.crysxd.octoapp.base.network.CachedLocalDnsResolver
import de.crysxd.octoapp.base.network.NetworkServiceDiscovery
import de.crysxd.octoapp.base.network.dnssd.DnsSd
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.sharedcommon.di.PlatformModule
import de.crysxd.octoapp.sharedcommon.di.SharedCommonComponent
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.utils.MultiplatformStrings
import io.github.aakira.napier.Napier
import kotlinx.coroutines.runBlocking
import java.util.Locale

class OctoApp : Application() {

    override fun onCreate() {
        super.onCreate()

        // Setup logging
        if (BuildConfig.DEBUG) {
            Napier.base(LogcatAntiLog)
        }
        Napier.base(FirebaseAntiLog)
        Napier.base(CachedAntiLog)
        FirebaseAntiLog.initAdapter(AndroidFirebaseAdapter())
        val wrapped = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            Napier.e(tag = "Uncaught!", throwable = e, message = "Uncaught exeception")
            wrapped?.uncaughtException(t, e)
        }

        // Do not bridge status notifications from the phone
        BridgingManager.fromContext(this).setConfig(
            BridgingConfig.Builder(this, true)
                .addExcludedTag(getString(R.string.print_status_bridge_tag))
                .build()
        )

        // Setup Koin
        SharedCommonInjector.setComponent(
            SharedCommonComponent(
                platformModule = PlatformModule(app = this),
            )
        )
        SharedBaseInjector.setComponent(
            SharedBaseComponent(
                sharedCommonComponent = SharedCommonInjector.get(),
                networkModule = NetworkModule(
                    networkServiceDiscovery = { NetworkServiceDiscovery.Noop },
                    dns = { CachedLocalDnsResolver(this, DnsSd.create(this)) }
                )
            )
        )

        // Setup Dagger
        BaseInjector.init(this)
        WearInjector.init(BaseInjector.get())

        // Start writing to disk
        CachedAntiLog.setUpDiskCache()

        // Setup billing
        BillingManager.adapter = PlayBillingAdapter(this)

        // Migrate
        runBlocking {
            AllMigrations(this@OctoApp).migrate()
            AllSharedMigrations().migrate()
        }

        // Setup language
        MultiplatformStrings.locale = Locale.forLanguageTag(BaseInjector.get().getAppLanguageUseCase().executeBlocking(Unit).appLanguage)

        // Setup RemoteConfig
        val beta = SharedCommonInjector.get().platform.betaBuild.toString()
        Firebase.analytics.setUserProperty("beta_release", beta)
        Firebase.remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        Firebase.remoteConfig.setConfigSettingsAsync(remoteConfigSettings {
            minimumFetchIntervalInSeconds = if (BuildConfig.DEBUG) 10 else 3600
        })

        // Init Wear services
        WearInjector.get().wearDataLayerService()

        // Inject test env for benchmarks
        if (BuildConfig.BENCHMARK) {
            BaseInjector.get().octoPreferences().isAutoConnectPrinter = true
            BaseInjector.get().octorPrintRepository().setActive(
                trigger = "benchmark",
                instance = PrinterConfigurationV3(
                    id = "test-env",
                    webUrl = BuildConfig.BENCHMARK_TEST_ENV.toUrl(),
                    apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                )
            )
        }
    }
}