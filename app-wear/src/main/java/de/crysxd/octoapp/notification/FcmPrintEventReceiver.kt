package de.crysxd.octoapp.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import de.crysxd.octoapp.base.data.models.FcmPrintEvent
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.getParcelableCompat
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class FcmPrintEventReceiver(coroutineScope: CoroutineScope) : BroadcastReceiver() {

    companion object {
        private const val TAG = "FcmPrintEventReceiver"
        private const val EXTRA_EVENT = "event"
        private const val EXTRA_INSTANCE_ID = "instance"
        private const val ACTION = "de.crysxd.octoapp.notification.PRINT_EVENT"

        fun sendToAll(fcmPrintEvent: FcmPrintEvent, instanceId: String) {
            Napier.i(tag = TAG, message = "Sending to all: $instanceId -> $fcmPrintEvent")
            val i = Intent(ACTION)
            i.putExtra(EXTRA_EVENT, fcmPrintEvent)
            i.putExtra(EXTRA_INSTANCE_ID, instanceId)
            LocalBroadcastManager.getInstance(BaseInjector.get().app()).sendBroadcast(i)
        }
    }

    private val mutableEvents = MutableStateFlow(emptyMap<String, FcmPrintEvent>())
    val events = mutableEvents

    init {
        coroutineScope.launch {
            LocalBroadcastManager.getInstance(BaseInjector.get().app()).registerReceiver(this@FcmPrintEventReceiver, IntentFilter(ACTION))
            while (true) delay(10_000)
        }.invokeOnCompletion {
            LocalBroadcastManager.getInstance(BaseInjector.get().app()).unregisterReceiver(this@FcmPrintEventReceiver)

        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        intent.getStringExtra(EXTRA_INSTANCE_ID)?.let { id ->
            intent.getParcelableCompat<FcmPrintEvent>(EXTRA_EVENT)?.let { event ->
                mutableEvents.update { original ->
                    Napier.i(tag = TAG, message = "Updating to all: $id -> $event")
                    val new = original.toMutableMap()
                    new[id] = event
                    new
                }
            } ?: Napier.e(tag = TAG, message = "No event", throwable = IllegalArgumentException("No event"))
        } ?: Napier.e(tag = TAG, message = "No instance ID", throwable = IllegalArgumentException("No instance ID"))
    }
}