package de.crysxd.octoapp.notification

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.FcmPrintEvent.Type.Printing
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.NotificationAESCipher
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class FcmNotificationService : FirebaseMessagingService() {

    companion object {
        private const val tag = "FcmNotificationService"

        // Time in Python format with sec.micros
        private var lastUpdateServerTime = mutableMapOf<String, Double>()
        private var lastEventServerTime = mutableMapOf<String, Double>()
    }

    private val cipher = NotificationAESCipher()
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Napier.e(tag = tag, message = "Exception in scope", throwable = throwable)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Napier.i(tag = tag, message = "New token: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        try {
            message.data["raw"]?.let {
                handleRawDataEvent(
                    instanceId = message.data["instanceId"] ?: throw IllegalArgumentException("Not instance id"),
                    raw = it,
                )
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Error handling FCM push notification")
        }
    }

    private fun handleRawDataEvent(instanceId: String, raw: String) = AppScope.launch(exceptionHandler) {
        Napier.i(tag = tag, message = "Received message with raw data for instance: $instanceId")

        // Check if for active instance or feature enabled
        val isForActive = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.id == instanceId
        if (!isForActive && !BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)) {
            Napier.i(tag = tag, message = "Dropping message for $instanceId as it's not the active instance and quick switch is disabled")
            return@launch
        }

        // Decrypt and decode data
        val data = cipher.parseRawData(instanceId = instanceId, raw = raw) ?: return@launch
        Napier.i(tag = tag, message = "Data: ${data.type} => $data")

        // Ensure we don't post old data
        val serverTime = data.serverTimePrecise ?: data.serverTime ?: 0.0
        val isEvent = data.type != Printing
        fun doTimeCheck(map: MutableMap<String, Double>): Boolean {
            val previousLast = map[instanceId] ?: 0.0
            return if (previousLast > serverTime) {
                Napier.i(tag = tag, message = "Skipping update, last server time was $previousLast which is after $serverTime")
                false
            } else {
                Napier.i(tag = tag, message = "$serverTime after $previousLast")
                map[instanceId] = serverTime
                true
            }
        }

        val useNotification = if (isEvent) doTimeCheck(lastEventServerTime) else doTimeCheck(lastUpdateServerTime)
        if (!useNotification) return@launch

        // Handle event
        FcmPrintEventReceiver.sendToAll(instanceId = instanceId, fcmPrintEvent = data)
    }
}