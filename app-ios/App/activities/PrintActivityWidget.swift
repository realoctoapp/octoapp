//
//  PrintActivityWidget.swift
//  OctoApp
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import WidgetKit
import SwiftUI
import Intents

@main
struct WidgetExtension: Widget {
  let kind: String = "WidgetExtension"
  
  var body: some WidgetConfiguration {
      ActivityConfiguration(attributesType: PrintActivity.Data.self) { context in
      StatusView(attribute: context.attributes, state: context.state)
    }
  }
}

struct WidgetExtension_Previews: PreviewProvider {
  static var previews: some View {
      let testAttribute = PrintActivity.Data(
      )
    let testState = StatusAttribute.ContentState(
      status: .pending,
      estimatedCompletionTime: Date()
    )
    
    StatusView(attribute: testAttribute, state: testState)
      .previewContext(
        WidgetPreviewContext(
          family: .systemLarge
        )
      )
  }
}
