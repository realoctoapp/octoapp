//
//  PurchaseView.swift
//  OctoApp
//
//  Created by Christian Würthner on 25/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import StoreKit
import SwiftUITrackableScrollView
import Combine

struct PurchaseView: View {
    
    private static var texts: PurchaseOffers.Texts {
        ConfigUtilsKt.getPurchaseOffers().activeConfig.textsWithData
    }
    
    private static var sellingPoints: [SellingPointStruct] {
        ConfigUtilsKt.getPurchaseOffers().activeConfig.filteredSellingPoints.map{ SellingPointStruct(sellingPoint: $0) }
    }
    
    var transferId: String?
        
    @State private var banner: String? = PurchaseView.texts.highlightBanner
    @State private var sellingPoints: [SellingPointStruct] = PurchaseView.sellingPoints
    @State private var offers: [PurchaseOfferStruct] = PurchaseView.getOffers()
    @State private var purchases: [BillingPurchaseStruct] = BillingManager.shared.currentState.purchases
    @State private var purchaseCompleted = false
    @State private var showSuccessForProductId: String? = nil
    @State private var showLoading = false

    var body: some View {
        ZStack {
            PurchaseTransferHandler(
                purchase: purchase,
                transferId: transferId,
                showLoading: $showLoading
            )
            PurchaseViewContent(
                title: StringResourcesKt.parseHtml(PurchaseView.texts.purchaseScreenTitle) as! String,
                offersTitle:  StringResourcesKt.parseHtml(PurchaseView.texts.skuListTitle) as! String,
                description: StringResourcesKt.parseHtml(PurchaseView.texts.purchaseScreenDescription) as! String,
                continueCta: PurchaseView.texts.purchaseScreenContinueCta,
                highlightBanner: banner.flatMap { StringResourcesKt.parseHtml($0) as? String },
                sellingPoints: sellingPoints,
                offers: offers,
                purchases: purchases,
                purchaseProduct: purchase
            )
        }
        .macOsCompatibleSheet(item: $showSuccessForProductId) { productId in
            PurchaseSuccessView(isTip: productId == "tip")
        }
        .task {
            while !Task.isCancelled && banner != nil {
                try? await Task.sleep(for: .seconds(1))
                banner = PurchaseView.texts.highlightBanner
            }
        }
        .onReceive(BillingManager.shared.state.asPublisher()) { (state: BillingState) in
            let mapped = BillingStateStruct(state: state)
            offers = PurchaseView.getOffers(products: mapped.products)
            purchases = mapped.purchases
        }
        .onDisappear {
            if purchases.isEmpty {
                OctoAnalytics.shared.logEvent(
                    event: OctoAnalytics.EventPurchaseScreenClosed.shared,
                    params: [:]
                )
            }
        }
        .overlay {
            if showLoading {
                ProgressView()
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(.ultraThinMaterial)
            }
        }
        .animation(.default, value: showLoading)
    }
    
    static func getOffers(
        products: [BillingProductStruct] = BillingManager.shared.currentState.products
    ) -> [PurchaseOfferStruct] {
        let offers = ConfigUtilsKt.getPurchaseOffers().activeConfig.offers ?? [:]
        let sortedOffers = offers.compactMap {
            PurchaseOfferStruct(productId: $0, offer: $1, products: products)
        }.sorted {
            // Sort tip to bottom and rest ascending by price
            if $0.id == "tip" {
                return false
            } else if $1.id == "tip" {
                return true
            } else {
                return $1.product.priceOrder > $0.product.priceOrder
            }
        }
        return sortedOffers
    }
    
    
    func purchase(product: Product, options: Set<Product.PurchaseOption> = []) async throws {
        let result = try await product.purchase(options: options)
        
        switch result {
        case let .success(.verified(transaction)):
            // Successful purhcase
            await transaction.finish()
            showSuccessForProductId = product.id
        case let .success(.unverified(_, error)):
            // Successful purchase but transaction/receipt can't be verified
            // Could be a jailbroken phone
            Napier.e(tag: "PurchaseView", message: "Error in purchase \(error)")
            break
        case .pending:
            // Transaction waiting on SCA (Strong Customer Authentication) or
            // approval from Ask to Buy
            break
        case .userCancelled:
            // ^^^
            break
        @unknown default:
            break
        }
    }
}

private struct PurchaseTransferHandler : View {
    
    var purchase: (Product, Set<Product.PurchaseOption>) async throws -> Void
    @State var transferId: String?
    @Binding var showLoading: Bool
    @State private var showTransferDisclaimer: Bool = false
    @State private var triggerTransferId: String? = nil
    @EnvironmentObject private var orchestrator: OrchestratorViewModel

    var body: some View {
        Color.clear
            .alert(
                LocalizedStringKey("billing_transfer___disclaimer"~),
                isPresented: $showTransferDisclaimer
            ) {
                Button("billing_transfer___continue"~, role: .none) {
                    triggerTransferId = transferId
                    transferId = nil
                }
            }
            .task {
                try? await Task.sleep(for: .seconds(2))
                if transferId != nil, transferId != "", !Task.isCancelled {
                    showTransferDisclaimer = true
                }
            }
            .task(id: triggerTransferId) {
                do {
                    if let id = triggerTransferId, triggerTransferId != "" {
                        showLoading = true
                        defer { showLoading = false }
                        let result = try await BillingManager.shared.redeemTransfer(transferId: id)
                        
                        switch result {
                        case result as BillingProductTransferRedeemResult.Error: orchestrator.postError(
                            error: (result as! BillingProductTransferRedeemResult.Error).exception
                        )
                        case result as BillingProductTransferRedeemResult.Success: do {
                            let billingProduct = (result as! BillingProductTransferRedeemResult.Success).product
                            let product = (billingProduct.nativeProduct as! Product)
                            try await purchase(product, [])
                        }
                        default: Napier.e(tag: "PurchaseView", message: "Unexpected case: \(String(describing: result))")
                        }
                    }
                } catch {
                    Napier.e(tag: "PurchaseView", message: "Unexpected error: \(error)")
                }
            }
    }
}

private struct PurchaseViewContent: View {
    
    var title: String
    var offersTitle: String
    var description: String
    var continueCta: String
    var highlightBanner: String?
    var sellingPoints: [SellingPointStruct]
    var offers: [PurchaseOfferStruct]
    var purchases: [BillingPurchaseStruct]
    var purchaseProduct: (Product, Set<Product.PurchaseOption>) async throws -> Void
    
    @State private var safeAreaTop = getSafeAreaTop()
    @State private var safeAreaBottom = getSafeAreaBottom()
    @State private var contentOffset: CGFloat = -1
    @State private var headerScale: CGFloat = 1
    @State private var activeSellingPoint = 0
    @State private var width: CGFloat = 0
    @State private var height: CGFloat = 0
    @State private var showOffers = false
    @State private var interactionRecorded = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .top) {
                background
                header
                scrollView
                overlay
            }
            .animation(.default, value: showOffers)
            .edgesIgnoringSafeArea(.top)
            .edgesIgnoringSafeArea(.bottom)
            .background(OctoTheme.colors.windowBackground)
            .onChange(of: geometry.safeAreaInsets) { _, safeArea in
                safeAreaTop = safeArea.top
                safeAreaBottom = safeArea.bottom
            }
            .onChange(of: geometry.size) { _, size in
                width = size.width
                height = size.height
            }
        }
    }
    
    var scrollView: some View {
        ScrollViewReader { reader in
            TrackableScrollView(
                .vertical,
                showIndicators: false,
                contentOffset: $contentOffset
            ) {
                VStack(spacing: 0) {
                    scrollableHeader
                    
                    VStack(spacing: 0) {
                        if showOffers {
                            offersContent
                        } else {
                            initialContent
                        }
                        
                        Spacer()
                    }
                    .background(OctoTheme.colors.windowBackground)
                }
                .frame(maxHeight: .infinity)
                .fixedSize(horizontal: false, vertical: false)
            }
            .onChange(of: contentOffset) { _, co in
                headerScale = 1 + ((1 - (min(co, 200) / 200)) * 0.2)
                
                if !interactionRecorded && purchases.isEmpty && co > 0 {
                    interactionRecorded = true
                    OctoAnalytics.shared.logEvent(
                        event: OctoAnalytics.EventPurchaseScreenScroll.shared,
                        params: [:]
                    )
                }
            }
            .onChange(of: showOffers) {
                withAnimation {
                    reader.scrollTo("header", anchor: .bottom)
                }
            }
        }
    }
    
    var overlay: some View {
        VStack {
            statusBarScrim
            Spacer()
            if !showOffers {
                button
            }
        }
    }
    
    var button: some View {
        PurchaseShowOffersButton(
            safeAreaBottom: safeAreaBottom,
            hasPurchases: !purchases.isEmpty,
            continueCta: continueCta,
            clickListener: { showOffers = true }
        )
    }
    
    @ViewBuilder
    var background: some View {
        if showOffers {
            OctoTheme.colors.windowBackground
        } else {
            OctoTheme.colors.inputBackground
        }
    }
    
    var header: some View {
        VStack(spacing: 0) {
            Image("purchaseHeaderScrim")
                .resizable()
                .frame(maxWidth: .infinity)
                .frame(height: safeAreaTop * 4)
                .padding(.top, -safeAreaTop * 3)
            
            Image("purchaseHeader")
                .resizable()
                .frame(maxWidth: .infinity)
                .aspectRatio(contentMode: .fit)
                .scaleEffect(headerScale)
        }
    }
    
    var statusBarScrim: some View {
        ZStack { }
            .frame(maxWidth: .infinity)
            .frame(height: safeAreaTop)
            .background(.ultraThinMaterial)
    }
    
    var scrollableHeader: some View {
        ZStack(alignment: .bottom) {
            header.opacity(0)
                .frame(maxWidth: .infinity)
            
            Image("Launcher")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 96)
                .padding(OctoTheme.dimens.margin0)
                .background(Circle().fill(OctoTheme.colors.windowBackground))
                .offset(x: 0, y: OctoTheme.dimens.margin1)
        }
        .clipped()
        .id("header")
    }
    
    var offersContent: some View {
        PurchaseOfferContentView(
            offers: offers,
            purchases: purchases,
            title: offersTitle,
            height: height + safeAreaBottom,
            purchaseProduct: purchaseProduct
        )
        .toolbar {
            ToolbarItemGroup(placement: .cancellationAction) {
                Button(action: { showOffers = false }) {
                    Label("billing___back"~, systemImage: "chevron.left")
                    Text("billing___back"~)
                }
            }
        }
        .onAppear {
            if purchases.isEmpty {
                OctoAnalytics.shared.logEvent(
                    event: OctoAnalytics.EventPurchaseOptionsShown.shared,
                    params: [
                        "title" : title,
                        "text" : description,
                        "button_text" : continueCta
                    ]
                )
            }
        }
    }
    
    var initialContent: some View {
        PurchaseIntialContentView(
            title: title,
            description: description,
            highlightBanner: highlightBanner,
            sellingPoints: sellingPoints,
            hasPurchases: !purchases.isEmpty,
            width: width,
            height: height,
            safeAreaBottom: safeAreaBottom
        )
        .onAppear {
            if purchases.isEmpty {
                OctoAnalytics.shared.logEvent(
                    event: OctoAnalytics.EventPurchaseIntroShown.shared,
                    params: [:]
                )
            }
        }
    }
}

struct PurchaseView_Previews: PreviewProvider {
    
    static var previews: some View {
        StatefulPreviewWrapper(true) { yes in
            StatefulPreviewWrapper(PreviewState(count: 0, offers: [])) { state in
                ZStack{}.sheet(isPresented: yes) {
                    NavigationStack {
                        PurchaseViewContent(
                            title: StringResourcesKt.parseHtml("Thank you for<br>supporting OctoApp!") as! String,
                            offersTitle: "High Eight! You rock!",
                            description: StringResourcesKt.parseHtml("Hi, I'm Chris! Creating OctoApp and supporting all of you takes a lot of my time. To be able to continue this effort, I decided to make more advanced features exclusive for my supporters.</u><br/><br/><b>I will never start showing ads in this app nor will I take away features that are already available.</b><br/><br/>Following features will be unlocked:") as! String,
                            continueCta: "Support OctoApp",
                            highlightBanner: "This is a banner text \(state.wrappedValue.count)",
                            sellingPoints: [
                                SellingPointStruct(
                                    title: "Selling Point 1",
                                    description: "Lorem",
                                    imageUrl: "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                                ),
                                SellingPointStruct(
                                    title: "Selling Point 2",
                                    description: "Lorem",
                                    imageUrl: "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                                ),
                                SellingPointStruct(
                                    title: "Selling Point 3",
                                    description: "Lorem ipsum dolor sit amet long text lonsd sjdfns dfisdufs dk fjdis fiodsf iosdjoifs",
                                    imageUrl: "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                                ),
                                SellingPointStruct(
                                    title: "Selling Point 4",
                                    description: "Lorem",
                                    imageUrl: "party"
                                )
                            ],
                            offers: state.wrappedValue.offers,
                            purchases: [
                                BillingPurchaseStruct(
                                    productId: "preview_support_infinite",
                                    isSubscription: true
                                )
                            ],
                            purchaseProduct: { _, _ in }
                        )
                        .toolbarDoneButton()
                    }
                    .task {
                        BillingManager.shared.onResume()
                        try? await Task.sleep(for: .seconds(1))
                        let foundProducts = BillingManager.shared.currentState.products
                        print("\(foundProducts.count) products found")
                        let offers = [
                            PurchaseOfferStruct(
                                productId: "preview_support_infinite",
                                offer: PurchaseOffers.Offer(
                                    badge: .bestvalue,
                                    dealFor: nil,
                                    label: "Support for a lifetime",
                                    description: nil
                                ),
                                products: foundProducts
                            ),
                            PurchaseOfferStruct(
                                productId: "preview_support_infinite_sale",
                                offer: PurchaseOffers.Offer(
                                    badge: .sale,
                                    dealFor: "preview_support_infinite",
                                    label: "Support for a lifetime",
                                    description: "Introductory price **for June only!**"
                                ),
                                products: foundProducts
                            ),
                            PurchaseOfferStruct(
                                productId: "preview_support_1_year",
                                offer: PurchaseOffers.Offer(
                                    badge: .popular,
                                    dealFor: nil,
                                    label: "Support for one year",
                                    description: nil
                                ),
                                products: foundProducts
                            ),
                            PurchaseOfferStruct(
                                productId: "preview_support_1_month",
                                offer: PurchaseOffers.Offer(
                                    badge: .none,
                                    dealFor: nil,
                                    label: "Support for one month",
                                    description: nil
                                ),
                                products: foundProducts
                            ),
                            PurchaseOfferStruct(
                                productId: "tip",
                                offer: PurchaseOffers.Offer(
                                    badge: .none,
                                    dealFor: nil,
                                    label: "Just a coffee for the dev",
                                    description: nil
                                ),
                                products: foundProducts
                            )
                        ].compactMap { $0 }
                      
                        while !Task.isCancelled {
                            state.wrappedValue = PreviewState(
                                count: state.wrappedValue.count + 1,
                                offers: offers
                            )
                            
                            try? await Task.sleep(for: .seconds(1))
                        }
                    }
                }
            }
        }
    }
}

private struct PreviewState {
    var count: Int
    var offers: [PurchaseOfferStruct]
}
