//
//  PurchaseSuccessView.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct PurchaseSuccessView: View {
    
    var isTip: Bool
    @Environment(\.dismissToRoot) var dismissToRoot
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            content
        }
        .interactiveDismissDisabled(true)
    }
    
    var content: some View {
        VStack(spacing: 0) {
            header
            
            VStack(spacing: 0) {
                title
                text
                Spacer()
                button
            }
            .padding(.top, OctoTheme.dimens.margin3)
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
            .padding(.bottom, OctoTheme.dimens.margin12)
            .multilineTextAlignment(.center)
        }
        .background { background }
        .ignoresSafeArea(edges: .top)
    }
    
    var background: some View {
        SimpleLottieView(lottieFile: "confetti")
            .scaledToFill()
            .ignoresSafeArea(edges: .all)
            .opacity(0.4)
            .blur(radius: 3)
    }
    
    var header: some View {
        ZStack {
            OctoAvatar(action: .party)
        }
        .padding(OctoTheme.dimens.margin3)
        .background(OctoTheme.colors.inputBackground.opacity(0.5))
    }
    
    var title: some View {
        Text("purchase_dialog_title"~)
            .typographyTitle()
    }
    
    @ViewBuilder
    var text: some View {
        if !isTip {
            Text("purchase_dialog_subtitle"~)
                .foregroundColor(OctoTheme.colors.green)
                .typographySectionHeader()
                .padding(OctoTheme.dimens.margin2)
                .frame(maxWidth: .infinity)
                .surface(color: .special(OctoTheme.colors.greenTranslucent))
                .padding([.top, .bottom], OctoTheme.dimens.margin2)
        } else {
            Spacer()
                .frame(height: OctoTheme.dimens.margin1)
        }
        
        Text(String(format: "purchase_dialog_text"~, "hello@octoapp.eu"))
            .foregroundColor(OctoTheme.colors.lightText)
            .typographyBase()
            .padding(.bottom, OctoTheme.dimens.margin2)
    }
    
    var button: some View {
        OctoButton(
            text: "purchase_dialog_button"~,
            clickListener: {
                if isTip { dismiss() } else { dismissToRoot() }
            }
        )
    }
}

struct PurchaseSuccessView_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(true) { yes in
            ZStack {}.sheet(isPresented: yes) {
                PurchaseSuccessView(isTip: false)
            }
        }
    }
}
