//
//  SellingPointDetails.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import CachedAsyncImage

struct SellingPointDetailsView: View {
    
    var sellingPoints: [SellingPointStruct]
    @Binding var activeSellingPoint: Int
    @Environment(\.dismiss) private var dismiss
    private var filteredSellingPoints: [SellingPointStruct] {
        sellingPoints//.filter { $0.imageUrl != "party" }
    }
    
    var body: some View {
        NavigationStack {
            carousel
                .background(OctoTheme.colors.darkGrey)
                .padding(.bottom, OctoTheme.dimens.margin01)
                .toolbar {
                    ToolbarItemGroup(placement: .confirmationAction) {
                        Button("done"~) { dismiss() }
                    }
                }
                .navigationTitle(sellingPoints[activeSellingPoint].title ?? " ")
                .navigationBarTitleDisplayMode(.inline)
                .ignoresSafeArea(.all, edges: .bottom)
                .safeAreaInset(edge: .bottom) {
                    HStack {
                        ForEach(0..<filteredSellingPoints.count, id: \.self) { index in
                            Circle()
                                .fill(.white)
                                .scaleEffect(index == activeSellingPoint ? 1 : 0.9)
                                .opacity(index == activeSellingPoint ? 1 : 0.5)
                                .animation(.spring(), value: activeSellingPoint)
                                .frame(width: 10, height: 10)
                        }
                    }
                    .frame(maxWidth: .infinity)
                    .padding(.top, OctoTheme.dimens.margin0)
                    .background(.ultraThinMaterial)
                }
        }
        .preferredColorScheme(.dark)
    }
    
    var carousel: some View {
        GeometryReader { geo in
            let aspectRatio: CGFloat = 1179 / 2556
            
            FullscreenCarouselView(
                spacing: OctoTheme.dimens.margin2,
                itemsData: filteredSellingPoints,
                cardWidth: geo.size.width,
                activeCard: $activeSellingPoint
            ) { index, sellingPoint in
                SellingPointCarouselItem(
                    sellingPoint: sellingPoint
                )
                .frame(maxWidth: .infinity,maxHeight: .infinity)
                .aspectRatio(aspectRatio, contentMode: .fit)
                .fixedSize(horizontal: true, vertical: false)
                .surface()
            }
            .padding(.bottom, OctoTheme.dimens.margin12)
            .frame(width: geo.size.width)
        }
    }
}

struct SellingPointCarouselItem: View {
    
    var sellingPoint: SellingPointStruct
    
    @State private var showImage = false
    private let aspectRatio: CGFloat = 1179 / 2556
    
    var body: some View {
        CachedAsyncImage(
            url: sellingPoint.imageUrl.flatMap{ URL(string: $0) },
            urlCache: URLCache.imageCache,
            content: { imageDisplay(image: $0) },
            placeholder: { placeholder }
        )
    }
    
    @ViewBuilder
    func imageDisplay(image: Image) -> some View {
        ZStack {
            if showImage {
                image.resizable()
            }
        }
        .animation(.default, value: showImage)
        .onAppear { showImage = true }
    }
    
    @ViewBuilder
    var placeholder: some View {
        if sellingPoint.imageUrl == "party" {
            ZStack {              
                SimpleLottieView(lottieFile: "octo-party")
                    .scaledToFit()
                    .padding(OctoTheme.dimens.margin3)
            }
            .aspectRatio(aspectRatio, contentMode: .fill)
            .id(UUID().uuidString)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background {
                SimpleLottieView(lottieFile: "confetti", loop: true)
                    .blur(radius: 4)
                    .opacity(0.6)
                    .scaledToFill()
            }
        } else {
            ProgressView()
        }
    }
}

struct SellingPointDetails_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(1) { active in
            SellingPointDetailsView(
                sellingPoints: [
                    SellingPointStruct(
                        title: "Selling Point 1",
                        description: "Lorem",
                        imageUrl: "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                    ),
                    SellingPointStruct(
                        title: "Selling Point 2",
                        description: "Lorem",
                        imageUrl: "https://firebasestorage.googleapis.com/v0/b/octoapp-4e438.appspot.com/o/resources%2Fpurchase%2Ffeature_gcode.webp?alt=media&token=e39d0867-00da-4f41-957a-126c246924a8"
                    ),
                    SellingPointStruct(
                        title: "Selling Point 3",
                        description: "Lorem ipsum dolor sit amet long text lonsd sjdfns dfisdufs dk fjdis fiodsf iosdjoifs",
                        imageUrl: "nil"
                    ),
                    SellingPointStruct(
                        title: "Selling Point 4",
                        description: "Lorem",
                        imageUrl: "party"
                    )
                ],
                activeSellingPoint: active
            )
        }
    }
}
