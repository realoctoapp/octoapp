//
//  Terminal.swift
//  OctoApp
//
//  Created by Christian on 11/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct Terminal: View {
    
    @StateObject var viewModel = TerminalViewModel()
    
    var body: some View {
        TerminalContent(
            state: viewModel.state,
            clear: { viewModel.clear() },
            executeGcode: { await viewModel.executeGcode($0) },
            toggleStyledLogs: { await viewModel.toggleStyledLogs() }
        )
        .usingViewModel(viewModel)
        .navigationTitle("terminal___title"~)
        .navigationBarTitleDisplayMode(.inline)
    }
}

private struct TerminalContent: View {
    
    var state: TerminalViewModel.State
    var clear: () -> Void
    var executeGcode: (String) async -> Void
    var toggleStyledLogs: () async -> Void
    
    @State private var commandInput = ""
    
    var body: some View {
        VStack(spacing: 0) {
            list
            VStack(spacing: 0) {
                hairline
                shortcuts
                input
            }
            .padding(.bottom, getSafeAreaBottom())
            .background(bodyBackground)
            .overlay(printingOverlay)
        }
        .ignoresSafeArea(.container, edges: .bottom)
        .animation(.default, value: state.inputEnabled)
    }
    
    var bodyBackground: some View {
        Rectangle()
            .fill(OctoTheme.colors.windowBackground)
            .shadow(radius: 2)
    }
    
    @ViewBuilder
    var printingOverlay: some View {
        if !state.inputEnabled {
            VStack {
                Text("terminal___pause_the_print_to_send_commands"~)
                    .typographySectionHeader()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(.ultraThinMaterial)
        }
    }
    
    var hairline : some View {
        ControlsHairline()
    }
    
    var list: some View {
        TerminalList(
            logs: state.logs,
            styled: state.useStyledList
        )
        .animation(.default, value: state.logs)
        .animation(.default, value: state.useStyledList)
        .animation(.default, value: state.inputEnabled)
    }
    
    var shortcuts: some View {
        TerminalShortcuts(
            shortcuts: state.shortcuts,
            filterCount: state.filterCount,
            useStyledList: state.useStyledList,
            executeGcode: { await executeGcode($0.command) },
            toggleStyledLogs: toggleStyledLogs,
            clear: clear
        )
    }
    
    var input: some View {
        OctoTextField(
            placeholder: "terminal___input_placeholder"~,
            labelActiv: LocalizedStringKey("terminal___input_placeholder_active"),
            input: $commandInput,
            autocorrectionDisabled: true,
            multiline: true,
            showSubmit: true,
            onSubmitButton: {
                if !commandInput.isEmpty {
                    await executeGcode(commandInput)
                    commandInput = ""
                }
            }
        )
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .padding(.bottom, OctoTheme.dimens.margin1)
    }
}

struct Terminal_Previews: PreviewProvider {
    static var previews: some View {
        TerminalContent(
            state: TerminalViewModel.State(
                logs: previewLogs,
                inputEnabled: true,
                shortcuts: previewShortcuts,
                useStyledList: true,
                filterCount: 1
            ),
            clear: {},
            executeGcode: { _ in },
            toggleStyledLogs: { }
        )
        .previewDisplayName("Styled")
        
        TerminalContent(
            state: TerminalViewModel.State(
                logs: previewLogs,
                inputEnabled: true,
                shortcuts: previewShortcuts,
                useStyledList: false,
                filterCount: 1
            ),
            clear: {},
            executeGcode: { _ in },
            toggleStyledLogs: { }
        )
        .previewDisplayName("Plain")
        
        TerminalContent(
            state: TerminalViewModel.State(
                logs: previewLogs,
                inputEnabled: false,
                shortcuts: previewShortcuts,
                useStyledList: false,
                filterCount: 1
            ),
            clear: {},
            executeGcode: { _ in },
            toggleStyledLogs: { }
        )
        .previewDisplayName("No input")
        
        TerminalContent(
            state: TerminalViewModel.State(
                logs: previewLogs,
                inputEnabled: true,
                shortcuts: previewShortcuts,
                useStyledList: false,
                filterCount: 0
            ),
            clear: {},
            executeGcode: { _ in },
            toggleStyledLogs: { }
        )
        .previewDisplayName("No filters")
        
        TerminalContent(
            state: TerminalViewModel.State(
                logs: previewLogs,
                inputEnabled: true,
                shortcuts: [],
                useStyledList: false,
                filterCount: 1
            ),
            clear: {},
            executeGcode: { _ in },
            toggleStyledLogs: { }
            
        )
        .previewDisplayName("No shortcuts")
    }
}

private var previewShortcuts: [TerminalViewModel.Shortcut] {
    [
        TerminalViewModel.Shortcut(
            command: "G28",
            label: "G28",
            isFavourite: true
        ),
        TerminalViewModel.Shortcut(
            command: "G29",
            label: "G29",
            isFavourite: true
        ),
        TerminalViewModel.Shortcut(
            command: "M500",
            label: "M500",
            isFavourite: true
        ),
        TerminalViewModel.Shortcut(
            command: "M501",
            label: "M501",
            isFavourite: true
        )
    ]
}

private var previewLogs: [TerminalViewModel.TerminalLogLine] {
    var logs: [TerminalViewModel.TerminalLogLine] = []
    
    for i in 0...99 {
        logs.append(
            TerminalViewModel.TerminalLogLine(
                id: i * 100,
                text: "Send: G\(i)",
                textStyled: "G\(i)",
                isStart: true,
                isEnd: false,
                isMiddle: false
            )
        )
        logs.append(
            TerminalViewModel.TerminalLogLine(
                id: i * 200,
                text: "Receive: wait",
                textStyled: "wait",
                isStart: false,
                isEnd: false,
                isMiddle: true
            )
        )
        logs.append(
            TerminalViewModel.TerminalLogLine(
                id: i * 300,
                text: "Receive: wait",
                textStyled: "wait",
                isStart: false,
                isEnd: false,
                isMiddle: true
            )
        )
        logs.append(
            TerminalViewModel.TerminalLogLine(
                id: i * 400,
                text: "Recv: ok",
                textStyled: "ok",
                isStart: false,
                isEnd: true,
                isMiddle: false
            )
        )
    }
    
    return logs
}
