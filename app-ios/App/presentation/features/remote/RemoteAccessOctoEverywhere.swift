//
//  RemoteAccessOctoEverywhere.swift
//  OctoApp
//
//  Created by Christian on 29/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private let surfaceBackground = Color.white.opacity(0.05)
private let surfaceForeground = Color.white

struct RemoteAccessOctoEverywhere: View {
    
    var loading: Bool
        
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            TitleBar()
            Description()
            Connection(loading: loading)
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .background(OctoTheme.colors.externalOctoEverywhere2)
    }
}

private struct TitleBar : View {
    
    var body: some View {
        Image("OctoEverywhereLarge")
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(OctoTheme.dimens.margin12)
            .background(OctoTheme.colors.externalOctoEverywhere)
    }
}

private struct Description : View {
    
    var body: some View {
        Text("configure_remote_acces___octoeverywhere___description_1"~)
            .foregroundColor(.white)
            .multilineTextAlignment(.center)
            .frame(maxWidth: .infinity)
            .typographyBase()
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        }
}

@MainActor
private struct Connection : View {
    
    var loading: Bool
    @State private var loginUrl: String? = nil
    @State private var showSignIn = false
    @State private var showDisclaimer = false
    @StateObject private var viewModel = RemoteAccessOctoEverywhereViewModel()
    @Namespace var animation
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            if viewModel.connected {
                RemoteAccessConnected(text: "configure_remote_acces___octoeverywhere___connected"~)

                OctoAsyncButton(
                    text: "configure_remote_acces___octoeverywhere___disconnect_button"~,
                    type: .special(
                        background: OctoTheme.colors.white,
                        foreground: OctoTheme.colors.externalOctoEverywhere3,
                        stroke: OctoTheme.colors.externalOctoEverywhere3
                    ),
                    loading: loading,
                    clickListener: {
                        try await viewModel.disconnect()
                    }
                )
                .matchedGeometryEffect(id: "button", in: animation)
            } else {
                OctoAsyncButton(
                    text: "configure_remote_acces___octoeverywhere___connect_button"~,
                    type: .special(
                        background: OctoTheme.colors.externalOctoEverywhere3,
                        foreground: OctoTheme.colors.textColoredBackground,
                        stroke: .clear
                    ),
                    loading: loading,
                    clickListener: {
                        if let url = await viewModel.getLoginUrl() {
                            loginUrl = url
                            showDisclaimer = true
                        }
                    }
                )
                .alert(
                    LocalizedStringKey("configure_remote_acces___leaving_app___title"),
                    isPresented: $showDisclaimer,
                    actions: { Button("sign_in___continue"~) { showSignIn = true } },
                    message: { Text(String(format: "configure_remote_acces___leaving_app___text"~, "OctoEverywhere")) }
                )
                .matchedGeometryEffect(id: "button", in: animation)
                .macOsCompatibleSheet(isPresented: $showSignIn) {
                    if let url = loginUrl {
                        SafariView(url: url)
                    } else {
                        ScreenError()
                    }
                }
                
                if viewModel.failure != nil {
                    RemoteAccessFailure(
                        failure: viewModel.failure,
                        background: surfaceBackground,
                        foreground: surfaceForeground,
                        highlight: OctoTheme.colors.externalOctoEverywhere3
                    )
                } else {
                    RemoteAccessUspView(
                        foreground: surfaceForeground,
                        background: surfaceBackground,
                        usps: [
                            RemoteAccessUsp(
                                systemImageName: "checkmark.icloud.fill",
                                description: "configure_remote_acces___octoeverywhere___usp_1"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "video.fill",
                                description: "configure_remote_acces___octoeverywhere___usp_2"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "star.fill",
                                description: "configure_remote_acces___octoeverywhere___usp_3"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "lock.fill",
                                description: "configure_remote_acces___octoeverywhere___usp_4"~
                            )
                        ]
                    )
                }
            }
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .animation(.default, value: viewModel.connected)
        .usingViewModel(viewModel)
        .onChange(of: viewModel.connected) {
            showSignIn = false
        }
    }
}

private class RemoteAccessOctoEverywhereViewModel : BaseViewModel {
    var currentCore: RemoteAccessOctoEverywhereViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    
    @Published var connected: Bool = false
    @Published var failure: RemoteConnectionFailureStruct? = nil
    
    func createCore(instanceId: String) -> RemoteAccessOctoEverywhereViewModelCore {

        return RemoteAccessOctoEverywhereViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: RemoteAccessOctoEverywhereViewModelCore) {
        core.connectedState.asPublisher()
            .sink { (state: RemoteAccessBaseViewModelCore.State) in
                self.connected = state.connected
                self.failure = state.failure?.toStruct()
            }
            .store(in: &bag)
    }
    
    func clearData() {
        connected = false
        failure = nil
    }

    func disconnect() async throws {
        try await currentCore?.disconnect()
    }
    
    func getLoginUrl() async -> String? {
        try? await currentCore?.getLoginUrl()
    }
}

struct RemoteAccessOctoEverywhere_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessOctoEverywhere(loading: false)
    }
}
