//
//  RemoteAccessObico.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private let surfaceBackground = Color.white.opacity(0.9)
private let surfaceForeground = Color.black

struct RemoteAccessObico: View {
    
    var loading: Bool
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            TitleBar()
            Description()
            Connection(loading: loading)
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .background(OctoTheme.colors.externalObico2)
    }
}

private struct TitleBar : View {
    
    var body: some View {
        HStack {
            Image("obicoLarge")
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Spacer()
            
            Text("configure_remote_acces___free_tier_available"~)
                .foregroundColor(.white)
                .padding([.top, .bottom], OctoTheme.dimens.margin0)
                .padding([.leading, .trailing], OctoTheme.dimens.margin1)
                .background(Capsule().fill(OctoTheme.colors.externalObico))
                .typographyBase()
        }
        .padding(OctoTheme.dimens.margin12)
    }
}

private struct Description : View {
    
    var body: some View {
        Text("configure_remote_acces___spaghetti_detective___description"~)
            .foregroundColor(.black)
            .multilineTextAlignment(.center)
            .frame(maxWidth: .infinity)
            .typographyBase()
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
    }
}

@MainActor
private struct Connection : View {
    
    var loading: Bool
    @State private var showSignIn = false
    @State private var loginUrl: String? = nil
    @State private var showDisclaimer = false
    @State private var customInput = ""
    @State private var showInvalidUrl = false
    @State private var showCustomInput = false
    @StateObject private var viewModel = RemoteAccessObicoViewModel()
    @Namespace var animation
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            if viewModel.connected == true {
                RemoteAccessConnected(text: "configure_remote_acces___spaghetti_detective___connected"~)
                
                OctoAsyncButton(
                    text: "configure_remote_acces___spaghetti_detective___disconnect_button"~,
                    type: .special(
                        background: OctoTheme.colors.white,
                        foreground: OctoTheme.colors.externalObico,
                        stroke: OctoTheme.colors.externalObico
                    ),
                    loading: loading,
                    clickListener: {
                        try await viewModel.disconnect()
                    }
                )
                .matchedGeometryEffect(id: "button", in: animation)
                
                DataUsage(
                    dataUsage: viewModel.dataUsage,
                    retry: { viewModel.reloadDataUsage() }
                )
            } else if viewModel.connected == false {
                OctoAsyncButton(
                    text: "configure_remote_acces___spaghetti_detective___connect_button"~,
                    type: .special(
                        background: OctoTheme.colors.externalObico,
                        foreground: OctoTheme.colors.textColoredBackground,
                        stroke: .clear
                    ),
                    loading: loading,
                    clickListener: {
                        if let url = await viewModel.getLoginUrl(baseUrl: nil) {
                            loginUrl = url
                            showDisclaimer = true
                        }
                    }
                )
                .alert(
                    LocalizedStringKey("configure_remote_acces___leaving_app___title"),
                    isPresented: $showDisclaimer,
                    actions: { Button("sign_in___continue"~) { showSignIn = true } },
                    message: { Text(String(format: "configure_remote_acces___leaving_app___text"~, "Obico")) }
                )
                .matchedGeometryEffect(id: "button", in: animation)
                .macOsCompatibleSheet(isPresented: $showSignIn) {
                    if let url = loginUrl {
                        SafariView(url: url)
                    } else {
                        ScreenError()
                    }
                }
                
                OctoAsyncButton(
                    text: "configure_remote_acces___spaghetti_detective___custom_button"~,
                    type: .linkWithCustomColor(textColor: OctoTheme.colors.externalObico),
                    small: true,
                    clickListener: {
                        showCustomInput = true
                    }
                )
                .alert(
                    "configure_remote_acces___spaghetti_detective___custom_title"~,
                    isPresented: $showCustomInput
                ) {
                    TextField(
                        "configure_remote_acces___spaghetti_detective___custom_hint"~,
                        text: $customInput
                    )
                    .keyboardType(.URL)
                    .textContentType(.URL)
                    Button("configure_remote_acces___spaghetti_detective___custom_action"~) {
                        Task {
                            if UrlExtKt.toUrlOrNull(_: customInput) == nil || !(customInput.lowercased().hasPrefix("http://") || customInput.lowercased().hasPrefix("https://")) {
                                showInvalidUrl = true
                            } else if let url = await viewModel.getLoginUrl(baseUrl: customInput) {
                                loginUrl = url
                                showDisclaimer = true
                            }
                        }
                    }
                }
                .padding(.top, -OctoTheme.dimens.margin12)
                .alert(
                    LocalizedStringKey("configure_remote_acces___spaghetti_detective___custom_error"~),
                    isPresented: $showInvalidUrl,
                    actions: { Button("sign_in___continue"~) { showInvalidUrl = true } }
                )
                
                if viewModel.failure != nil {
                    RemoteAccessFailure(
                        failure: viewModel.failure,
                        background: surfaceBackground,
                        foreground: surfaceForeground,
                        highlight: OctoTheme.colors.externalObico
                    )
                } else {
                    RemoteAccessUspView(
                        foreground: surfaceForeground,
                        background: surfaceBackground,
                        usps: [
                            RemoteAccessUsp(
                                systemImageName: "checkmark.icloud.fill",
                                description: String(
                                    format: "configure_remote_acces___spaghetti_detective___usp_1"~,
                                    "300",
                                    "$4"
                                )
                            ),
                            RemoteAccessUsp(
                                systemImageName: "video.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_2"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "eye.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_3"~
                            ),
                            RemoteAccessUsp(
                                systemImageName: "lock.fill",
                                description: "configure_remote_acces___spaghetti_detective___usp_4"~
                            )
                        ]
                    )
                }
            }
        }
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .animation(.default, value: viewModel.connected)
        .usingViewModel(viewModel)
        .onChange(of: viewModel.connected) {
            showSignIn = false
        }
    }
}

private struct DataUsage : View {
    var dataUsage : FlowStateStruct<ObicoDataUsageStruct>
    var retry: () -> Void
    
    private var detailText: String {
        switch dataUsage {
        case .loading: return "loading"~
        case .error(_): return "configure_remote_acces___spaghetti_detective___data_usage_failed"~
        case .ready(let t): do {
            if t.hasDataCap {
                let df = DateFormatter()
                df.dateStyle = .short
                df.timeStyle = .none
                return String(
                    format: "configure_remote_acces___spaghetti_detective___data_usage_limited"~,
                    t.totalBytes.asStyleFileSize(intOnly: true),
                    t.monthlyCapBytes.asStyleFileSize(intOnly: true),
                    df.string(from: Date() + t.resetIn)
                )
            } else {
                return ""
            }
        }
        }
    }
    
    private var shouldShow : Bool {
        if case .ready(let du) = dataUsage {
            return du.hasDataCap
        } else {
            return true
        }
    }
    
    var body: some View {
        HStack() {
            if (shouldShow) {
                VStack(alignment: .leading, spacing: OctoTheme.dimens.margin01) {
                    Text("configure_remote_acces___spaghetti_detective___data_usafe"~)
                        .foregroundColor(surfaceForeground)
                        .typographySectionHeader()
                    
                    if case .ready(let du) = dataUsage {
                        ProgressView(value: Double(du.totalBytes), total: Double(du.monthlyCapBytes))
                            .tint(OctoTheme.colors.externalObico)
                    } else {
                        ProgressView(value: 0, total: 100)
                    }
                    
                    Text(detailText)
                        .foregroundColor(.black.opacity(0.75))
                        .typographyLabelSmall()
                }
                
                ZStack {
                    if dataUsage == .loading {
                        ProgressView()
                            .tint(.black)
                    } else {
                        OctoIconButton(
                            icon: "arrow.counterclockwise",
                            color: OctoTheme.colors.externalObico,
                            clickListener: retry
                        )
                    }
                }
                .frame(width: 50, height: 50)
                
            }
        }
        .padding([.top, .bottom, .leading], OctoTheme.dimens.margin12)
        .padding([.trailing], OctoTheme.dimens.margin01)
        .surface(color: .special(surfaceBackground))
        .animation(.default, value: dataUsage)
    }
}

private class RemoteAccessObicoViewModel : BaseViewModel {
    var currentCore: RemoteAccessObicoViewModelCore? = nil
    var bag:Set<AnyCancellable> = []
    
    @Published var connected: Bool? = nil
    @Published var failure: RemoteConnectionFailureStruct? = nil
    @Published var dataUsage: FlowStateStruct<ObicoDataUsageStruct> = .loading
    
    func createCore(instanceId: String) -> RemoteAccessObicoViewModelCore {
        return RemoteAccessObicoViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: RemoteAccessObicoViewModelCore) {
        core.connectedState.asPublisher()
            .sink { (state: RemoteAccessBaseViewModelCore.State) in
                self.connected = state.connected
                self.failure = state.failure?.toStruct()
            }
            .store(in: &bag)
        
        core.dataUsage.asPublisher()
            .sink { (state: FlowState<ObicoDataUsage>) in
                self.dataUsage = FlowStateStruct(state) { $0.toStruct() }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        connected = nil
        failure = nil
        dataUsage = .loading
    }
    
    func disconnect() async throws {
        try await currentCore?.disconnect()
    }
    
    func reloadDataUsage() {
        Task {
            try? await currentCore?.reloadDataUsage()
        }
    }
    
    func getLoginUrl(baseUrl: String?) async -> String? {
        try? await currentCore?.getLoginUrl(baseUrl: baseUrl)
    }
}

struct RemoteAccessObico_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessObico(loading: false)
            .previewDisplayName("General")
        
        DataUsage(
            dataUsage: .loading,
            retry: {}
        )
        .previewDisplayName("Data Usage / Loading")
        
        
        DataUsage(
            dataUsage: .error(
                KotlinException()
            ),
            retry: {}
        )
        .previewDisplayName("Data Usage / Error")
        
        DataUsage(
            dataUsage: .ready(
                ObicoDataUsageStruct(
                    monthlyCapBytes: 0,
                    resetIn: TimeInterval(123322),
                    totalBytes: 374678,
                    hasDataCap: false
                )
            ),
            retry: {}
        )
        .previewDisplayName("Data Usage / No Cap")
        
        DataUsage(
            dataUsage: .ready(
                ObicoDataUsageStruct(
                    monthlyCapBytes: 82334330,
                    resetIn: TimeInterval(123322),
                    totalBytes: 3746478,
                    hasDataCap: true
                )
            ),
            retry: {}
        )
        .previewDisplayName("Data Usage / Cap")
        
    }
}
