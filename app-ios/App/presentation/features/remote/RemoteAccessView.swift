//
//  RemoteAccessView.swift
//  OctoApp
//
//  Created by Christian on 29/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct RemoteAccessView: View {
    @StateObject private var viewModel = RemoteAccessViewModel()
    @State private var selection : RemoteService = .none
    @Environment(\.colorScheme) private var colorScheme
    
    var body: some View {
        content
            .animation(.spring(), value: selection)
            .animation(.spring(), value: viewModel.state.services)
            .ignoresSafeArea(edges: .bottom)
            .navigationTitle("configure_remote_access___title"~)
            .navigationBarTitleDisplayMode(.inline)
            .background(OctoTheme.colors.inputBackground)
            .withInstanceId { _, id in viewModel.initWithInstanceId(id) }
            .onAppear { OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventRemoteConfigScreenOpened(), params: [:]) }
            .onChange(of: viewModel.state.services) { _, t in
                // Select first tab if none selected
                if selection == .none {
                    selection = t.isEmpty ? .none : t[0]
                }
            }
    }
    
    var content: some View {
        GeometryReader { geo in
            if selection != .none && !viewModel.state.services.isEmpty {
                ScrollViewReader { reader in
                    ScrollView {
                        RemoteAccessLayout(screenHeight: geo.size.height) {
                            header
                            tabs
                            pager
                        }
                    }.onChange(of: selection) { _, x in
                        withAnimation {
                            reader.scrollTo("bottom", anchor: .bottom)
                        }
                    }
                }
            }
        }
    }
    
    var header: some View {
        VStack {
            let lottieFile = colorScheme == .dark ? "octo-vacation-dark" : "octo-vacation"
            SimpleLottieView(lottieFile: lottieFile, loop: true)
                .aspectRatio(16/9, contentMode: .fit)
                .padding(.top, OctoTheme.dimens.margin4)
                .padding(.bottom, -OctoTheme.dimens.margin2)
                .id(lottieFile)
            
            Text(LocalizedStringKey(String(format: "configure_remote_acces___description"~, UriLibrary.shared.getFaqUri(faqId: "use_remotely"))))
                .multilineTextAlignment(.center)
                .typographyLabel()
                .padding([.bottom], OctoTheme.dimens.margin4)
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        }
    }
    
    var tabs: some View {
        OctoTabs(
            selection: $selection,
            tabs: viewModel.state.services,
            label: { service in service.title }
        )
        .background(OctoTheme.colors.windowBackground)
    }
    
    var pager: some View {
        OctoPager(
            selection: $selection,
            tabs: viewModel.state.services,
            pages: { service in
                Page {
                    ZStack {
                        switch(service) {
                        case .octoEverywhere: RemoteAccessOctoEverywhere(loading: viewModel.state.loading.contains(.octoEverywhere))
                        case .obico: RemoteAccessObico(loading: viewModel.state.loading.contains(.obico))
                        case .ngrok: RemoteAccessNgrok()
                        case .manual: RemoteAccessManual()
                        case .none: Color.clear
                        }
                    }
                }
            }
        )
        .id("bottom")
        .background(OctoTheme.colors.windowBackground)
    }
}

private struct Page<Content: View> : View {
    let content: () -> Content
    var body : some View {
        content()
            .surface(shadow: true)
            .padding(OctoTheme.dimens.margin12)
    }
}

private struct RemoteAccessLayout: Layout {
    var screenHeight: CGFloat
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        var p = proposal.replacingUnspecifiedDimensions()
        let headerHeight = subviews[0].sizeThatFits(proposal).height
        let tabHeight = subviews[1].sizeThatFits(proposal).height
        let contentHeight = screenHeight - tabHeight
        p.height = headerHeight + tabHeight + contentHeight
        return p
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        
        // Header
        let headerHeigh = subviews[0].sizeThatFits(proposal).height
        subviews[0].place(
            at: p,
            proposal: .init(width: bounds.size.width, height: headerHeigh)
        )
        
        // Tabs
        p.y += headerHeigh
        let tabHeight = subviews[1].sizeThatFits(proposal).height
        subviews[1].place(
            at: p,
            proposal: .init(width: bounds.size.width, height: tabHeight)
        )
        
        // Pager
        p.y += tabHeight
        let pagerHeight = screenHeight - tabHeight
        subviews[2].place(
            at: p,
            proposal: .init(width: bounds.size.width, height: pagerHeight)
        )
    }
}

enum RemoteService : Identifiable, Equatable {
    var id: String { return title }
    case octoEverywhere, obico, ngrok, manual, none
}

extension RemoteService {
    var title: String {
        switch self {
        case .manual: return "configure_remote_acces___manual___title"~
        case .ngrok: return "configure_remote_acces___ngrok___title"~
        case .octoEverywhere: return "configure_remote_acces___octoeverywhere___title"~
        case .obico: return "configure_remote_acces___spaghetti_detective___title"~
        case .none: return ""
        }
    }
    
    init(service: RemoteAccessViewModelCore.Service) {
        switch service {
        case .octoeverywhere: self = .octoEverywhere
        case .obico: self = .obico
        case .ngrok: self = .ngrok
        case .manual: self = .manual
        default: self = .none
        }
    }
}

class RemoteAccessViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    @Published var state = RemoteAccessViewModel.State(services: [], loading: [])
    
    func initWithInstanceId(_ instanceId: String) {
        let core = RemoteAccessViewModelCore(instanceId: instanceId)
        core.state.asPublisher()
            .sink { (state: RemoteAccessViewModelCore.State) in
                self.state = RemoteAccessViewModel.State(
                    services: state.services.map { s in RemoteService(service: s) },
                    loading:  state.loadingServices.map { s in RemoteService(service: s) }
                )
            }
            .store(in: &bag)
    }
}

extension RemoteAccessViewModel {
    struct State : Equatable {
        var services: [RemoteService]
        var loading: [RemoteService]
    }
}

struct RemoteAccessView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            RemoteAccessView()
        }
        .background(OctoTheme.colors.windowBackground)
        
    }
}
