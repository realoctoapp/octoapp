//
//  RemoteAccessFailure.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct RemoteAccessFailure: View {
    var failure: RemoteConnectionFailureStruct?
    var background: Color
    var foreground: Color
    var highlight: Color
    
    @State private var showError = false
    @State private var showDetails = false
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        if let f = failure {
            HStack(alignment: .center, spacing: OctoTheme.dimens.margin1) {
                Text(failure?.message ?? String(format: "configure_remote_access___failure"~, f.date.formatted()))
                    .foregroundColor(foreground)
                    .typographyLabel()
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                if let url = failure?.learnMoreUrl {
                    OctoButton(text: "learn_more"~, type: .link, small: true) {
                        openUrl(url)
                    }
                    .padding(.trailing, -OctoTheme.dimens.margin1)
                } else {
                    OctoIconButton(
                        icon: "info.circle.fill",
                        color: highlight,
                        clickListener: { showError = true }
                    )
                }
            }
            .padding([.leading, .trailing], OctoTheme.dimens.margin12)
            .padding([.top, .bottom], OctoTheme.dimens.margin1)
            .surface(color: .special(background))
            .alert(failure?.errorMessage ?? "error_general"~, isPresented: $showError) {
                if failure?.errorMessageStack != nil {
                    Button("show_details"~) { showError = false; showDetails = true }
                }
                Button("done"~) { showError = false }
            }
            .alert(failure?.errorMessage ?? "???", isPresented: $showDetails, actions: {
                Button("done"~) { showError = false }
            }, message: {
                Text(failure?.errorMessageStack ?? "???")
            })
        }
    }
}

struct RemoteAccessFailure_Previews: PreviewProvider {
    static var previews: some View {
        RemoteAccessFailure(
            failure: RemoteConnectionFailureStruct(
                errorMessage: "Something went wrong",
                errorMessageStack: "Stack",
                stackTrace: "Trace",
                remoteServiceName: "some",
                date: Date(),
                message: "Test is a message",
                learnMoreUrl: "http://some.com"
            ),
            background: OctoTheme.colors.inputBackground,
            foreground: OctoTheme.colors.darkText,
            highlight: OctoTheme.colors.accent
        )
        .previewDisplayName("With message")
        
        RemoteAccessFailure(
            failure: RemoteConnectionFailureStruct(
                errorMessage: "Something went wrong",
                errorMessageStack: "Stack",
                stackTrace: "Trace",
                remoteServiceName: "some",
                date: Date(),
                message: nil,
                learnMoreUrl: nil
            ),
            background: OctoTheme.colors.inputBackground,
            foreground: OctoTheme.colors.darkText,
            highlight: OctoTheme.colors.accent
        )
        .previewDisplayName("Without message")
        
        RemoteAccessFailure(
            failure: nil,
            background: OctoTheme.colors.inputBackground,
            foreground: OctoTheme.colors.darkText,
            highlight: OctoTheme.colors.accent
        )
        .previewDisplayName("None")
    }
}
