//
//  MultiToolControls.swift
//  OctoApp
//
//  Created by Christian on 24/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct MultiToolControls: View {
    @StateObject var viewModel: MultiToolControlsViewModel
    
    var body: some View {
        if viewModel.state.toolCount > 1 {
            MultiToolControlsContent(state: viewModel.state) { toolIndex in
                viewModel.setActiveTool(toolIndex: toolIndex)
            }
        }
    }
}

private struct MultiToolControlsContent: View {
    var state: MultiToolControlsState
    var onActivate: (Int?) -> Void
    
    var body: some View {
        ControlsScaffold(title: "widget_multi_tool___title"~) {
            VStack {
                group
                explainer
            }
        }
    }
    
    var explainer: some View {
        Text("widget_multi_tool___explainer"~)
            .foregroundColor(OctoTheme.colors.lightText)
            .typographyLabelSmall()
            .multilineTextAlignment(.center)
            .padding(.top, OctoTheme.dimens.margin01)
    }
    
    var group: some View {
        ToolOptionsLayout(activeIndex: state.activeToolIndex) {
            options
            slider
        }
        .frame(height: 48)
        .background(OctoTheme.colors.inputBackground)
        .clipShape(Capsule())
    }
    
    var options: some View {
        ForEach(-1..<state.toolCount, id: \.self) { toolIndex in
            let toolIndexFixed = toolIndex >= 0 ? toolIndex : nil
            ToolOption(
                toolIndex: toolIndexFixed,
                activeIndex: state.activeToolIndex,
                onActivate: { onActivate(toolIndexFixed) }
            )
        }
    }
    
    var slider: some View {
        OctoTheme.colors.accent
            .cornerRadius(OctoTheme.dimens.cornerRadiusSmall)
            .zIndex(-100)
    }
}


private struct ToolOption : View {
    var toolIndex: Int?
    var activeIndex: Int?
    var onActivate: () -> Void
    
    private var label: String {
        if let ti = toolIndex {
            return "\(ti + 1)"
        } else {
            return "target_off"~
        }
    }
    
    var body: some View {
        let isActive = toolIndex == activeIndex
        
        Button(action: onActivate) {
            Text(label)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .foregroundColor(isActive ? OctoTheme.colors.textColoredBackground : OctoTheme.colors.accent)
                .typographyButton(small: true)
        }
    }
}

private struct ToolOptionsLayout: Layout {
    
    let activeIndex: Int?
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        return proposal.replacingUnspecifiedDimensions()
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let optionWidth = bounds.width / CGFloat(subviews.count - 1)
        
        for i in 0..<(subviews.count - 1) {
            subviews[i].place(at: p, proposal: .init(width: optionWidth, height: bounds.height))
            p.x += optionWidth
        }
        
        let activeIndexFixed = activeIndex.flatMap { $0 + 1 } ?? 0
        var activeP =  bounds.origin
        activeP.x += optionWidth * CGFloat(activeIndexFixed)
        subviews.last!.place(at: activeP, proposal: .init(width: optionWidth, height: bounds.height))
    }
}


struct MultiToolControls_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(0) { active in
            MultiToolControlsContent(state: MultiToolControlsState(activeToolIndex: active.wrappedValue, toolCount: 2)) {
                active.wrappedValue = $0
            }
            .animation(.spring(), value: active.wrappedValue)
            .previewDisplayName("Two")
        }
        
        StatefulPreviewWrapper(2) { active in
            MultiToolControlsContent(state: MultiToolControlsState(activeToolIndex: active.wrappedValue, toolCount: 4)) {
                active.wrappedValue = $0
            }
            .animation(.spring(), value: active.wrappedValue)
            .previewDisplayName("Many")
        }
    }
}
