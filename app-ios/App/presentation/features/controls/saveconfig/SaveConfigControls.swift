//
//  SaveConfigControls.swift
//  OctoApp
//
//  Created by Christian on 21/05/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

struct SaveConfigControls: View {
    
   @StateObject var viewModel: SaveConfigControlsViewModel
    
    var body: some View {
        SaveConfigControlsContent(
            saveConfigPending: viewModel.state.saveConfigPending,
            onSave: { await viewModel.saveConfig() }
        )
    }
}

private struct SaveConfigControlsContent: View {
    
    var saveConfigPending: Bool
    var onSave: () async -> Void
    
    var body: some View {
        if saveConfigPending {
            ControlsScaffold(title: nil, supportsEdit: false) {
                SmallAnnouncement(
                    text: "save_config___description"~,
                    hideButton: "save_config___action"~,
                    onHideAnnouncement: {
                        await onSave()
                        try? await Task.sleep(for: .seconds(30.0))
                    }
                )
            }
        }
    }
}

class SaveConfigControlsViewModel : BaseViewModel {
    
    @Published private(set) var state = State(saveConfigPending: false)
    var currentCore: SaveConfigControlsViewModelCore?
    var bag: Set<AnyCancellable> = []
    
    func createCore(instanceId: String) -> SaveConfigControlsViewModelCore {
        SaveConfigControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: SaveConfigControlsViewModelCore) {
        core.state.asPublisher()
            .sink { [weak self] (state: SaveConfigControlsViewModelCore.State) in
                self?.state = State(saveConfigPending: state.saveConfigPending)
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = State(saveConfigPending: false)
    }
    
    func saveConfig() async {
        try? await currentCore?.saveConfig()
    }
}

extension SaveConfigControlsViewModel {
    struct State : Equatable {
        var saveConfigPending: Bool
    }
}

#Preview {
    SaveConfigControlsContent(saveConfigPending: true, onSave: {})
}
