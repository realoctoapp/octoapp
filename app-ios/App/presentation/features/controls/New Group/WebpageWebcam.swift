//
//  HtmlImage.swift
//  OctoApp
//
//  Created by Christian on 10/02/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI
import WebKit

struct WebpageWebcam : View {
    
    let content: WebcamControlsViewModel.WebContent
    let transformation: WebcamTransformation
    let onImageSizeChange: (CGSize) -> Void
    let onSnapshotAvailable: ((() async throws -> UIImage)?) -> Void
    @Binding var isLive: Bool
    let coordinator: WebpageWebcamCoordinator
    @Binding var zoomedIn: Bool
    @State private var imageSize = CGSize.zero
    @State private var nativeAspect: CGFloat? = nil
    @State private var viewRemoved = false
    @State private var detachTask: Task<Void, Error>? = nil
    @Environment(\.scenePhase) private var scenePhase
    private var webViewOpacity : Double {
        switch coordinator.state {
        case .error(_): 0
        case .loading: 0
        case .playing(_, _): 1
        }
    }
    var body: some View {
        ZStack {
            stateView
            
            // Stop webcam when app enters background
            if !viewRemoved {
                webcamView.frame(alignment: .center)
            }
        }
        .animation(.default, value: coordinator.state)
        .animation(.default, value: scenePhase)
        .onChange(of: scenePhase) { old, new in
            if new == .background && !coordinator.pipActive {
                coordinator.killVideo {
                    viewRemoved = true
                }
            } else if new == .active {
                viewRemoved = false
            }
        }
    }
    
    var webcamView: some View {
        WebcamWebViewRepresentable(
            coordinator: coordinator,
            content: content
        )
        .equatable()
        .frame(width: imageSize.width, height: imageSize.height)
        .webcamTransformations(
            transformation: transformation,
            showScale: false,
            canZoom: true,
            imageSize: imageSize,
            zoomedIn: $zoomedIn
        )
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .opacity(webViewOpacity)
        .onChange(of: coordinator.state) { _, newState in
            Napier.i(tag: "WebcamWebView", message: "State is now \(newState)")
            if case .playing(let width, let height) = newState {
                imageSize = CGSize(width: width, height: height)
                onImageSizeChange(imageSize)
                isLive = true
                onSnapshotAvailable { try await coordinator.requestSnapshot() }
            } else {
                onImageSizeChange(CGSize(width: 0, height: 0))
                isLive = false
                onSnapshotAvailable(nil)
            }
        }
        .onChange(of: content) { _, content in
            coordinator.reinitView(content: content)
        }
    }
    
    @ViewBuilder
    var stateView: some View {
        switch coordinator.state {
        case .loading: ProgressView()
                .tint(OctoTheme.colors.textColoredBackground)
            
        case .playing: Color.clear
            
        case .error(errorMessage: let errorMessage): ScreenError(
            title: "webcam___error_webrtc_general"~,
            description: errorMessage,
            error: nil,
            canDismiss: false,
            onRetry: { coordinator.reinitView(content: content) },
            textColor: OctoTheme.colors.textColoredBackground
        )
        .padding(OctoTheme.dimens.margin12)
        }
    }
}

class WebpageWebcamCoordinator: NSObject, ObservableObject, WKScriptMessageHandler {
    
    @Published private(set) var state: WebviewState = .loading
    @Published private(set) var pipActive = false

    let logName = "logHandler"
    let stateName = "stateHandler"
    let snapshotName = "snapshotHandler"
    let tag = "WebpageWebcam"

    var view: WKWebView? = nil
    var lastContent: WebcamControlsViewModel.WebContent? = nil
    
    private typealias SnapshotContinuation = CheckedContinuation<UIImage, Error>
    private var continuation: SnapshotContinuation?
    
    func killVideo(then: @escaping () -> Void = {}) {
        var stoppedRtc = false
        var closedMedia = false
        Napier.i(tag: tag, message: "Killing video")
        
        view?.evaluateJavaScript("stopWebRTC()") { _,_ in
            stoppedRtc = true
            Napier.i(tag: self.tag, message: "WebRTC stopped")
            if stoppedRtc && closedMedia {
                Napier.i(tag: self.tag, message: "Video killed")
                then()
            }
        } ?? {  Napier.i(tag: self.tag, message: "Unable to stop RTC, no video") }()
        
        view?.closeAllMediaPresentations {
            closedMedia = true
            Napier.i(tag: self.tag, message: "All media represntations closed")
            if stoppedRtc && closedMedia {
                Napier.i(tag: self.tag, message: "Video killed")
                then()
            }
        }  ?? {  Napier.i(tag: self.tag, message: "Unable to close media, no video") }()
        
        if view == nil {
            Napier.i(tag: self.tag, message: "Video was not active, skip kill")
            then()
        }
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let args = message.body as? [String: Any] ?? [:]
        
        if message.name == logName {
            let level = args["level"] as? String ?? "i"
            let message = args["message"] as? String ?? "Unknown message"
            
            switch level {
            case "v": Napier.v(tag: tag, message: message)
            case "d": Napier.d(tag: tag, message: message)
            case "w": Napier.w(tag: tag, message: message)
            case "e": Napier.e(tag: tag, message: message)
            default: Napier.i(tag: tag, message: message)
            }
        } else if message.name == stateName {
            let stateString = args["state"] as? String ?? "none"
            let errorMessage = args["errorMsg"] as? String ?? "Unknown error"
            let width = args["width"] as? Int ?? 0
            let height = args["height"] as? Int ?? 0
            pipActive = args["pipActive"] as? Bool == true
            Napier.i(tag: tag, message: "pipActive=\(pipActive)")

            switch stateString {
            case "loading": state = .loading
            case "playing": state = .playing(width: width, height: height)
            case "error": state = .error(errorMessage: errorMessage)
            default: Napier.e(tag: tag, message: "Unknown state \(stateString)")
            }
        } else if message.name == snapshotName {
            Napier.i(tag: tag, message: "Received snapshot")
            guard let dataUri = args["dataUri"] as? String  else {
                continuation?.resume(throwing: WebcamWebViewError(message: "Invalid snapshot"))
                return
            }
            guard let base64 = dataUri.split(separator: ",").last else {
                continuation?.resume(throwing: WebcamWebViewError(message: "Missing , split"))
                return
            }
            guard let data = Data(base64Encoded: String(base64), options: .ignoreUnknownCharacters) else {
                continuation?.resume(throwing: WebcamWebViewError(message: "Base64 decode error"))
                return
            }
            guard let image = UIImage(data: data) else {
                continuation?.resume(throwing: WebcamWebViewError(message: "Image decode error"))
                return
            }
            
            continuation?.resume(returning: image)
        }
    }
    
    @MainActor
    func requestSnapshot() async throws -> UIImage {
        guard let view = view else {
            throw WebcamWebViewError(message: "No Webview available")
        }
        
        
        // Awaiting snapshot
        let snpashotTask = Task {
            let image = try await withCheckedThrowingContinuation { continuation in
                self.continuation = continuation
            }
            try Task.checkCancellation()
            return image
        }
        
        // Awaiting timeout
        let timeoutTask = Task {
            try await Task.sleep(for: .seconds(2))
            try Task.checkCancellation()
            self.continuation?.resume(throwing: WebcamWebViewError(message: "Capturing snapshot timed out"))
        }
        defer {
            self.continuation = nil
            timeoutTask.cancel()
        }
       
        // Requesting snapshot
        try await view.evaluateJavaScript("takeSnapshot()")
        
        // Aawiting comletion
        return try await snpashotTask.value
    }
    
    @MainActor
    func requestPip() async throws -> Void {
        try await view?.evaluateJavaScript("enterPip()")
    }
    
    func reinitView(content: WebcamControlsViewModel.WebContent) -> Void {
        guard let webView = view else { return }
        
        #if DEBUG
            webView.isInspectable = true
        #endif

        webView.isOpaque = false
        webView.backgroundColor = UIColor.black
        webView.scrollView.backgroundColor = UIColor.black
        webView.scrollView.bounces = false
        webView.scrollView.isScrollEnabled = false
        webView.scrollView.pinchGestureRecognizer?.isEnabled = false
        webView.scrollView.subviews.forEach { $0.isUserInteractionEnabled = false }
        webView.loadHTMLString(content.html, baseURL: URL(string: content.baseUrl))
        Napier.i(tag: tag, message: "HTML loaded")
    }
    
    func submitSnapshot(snapshot: UIImage) {
        continuation?.resume(returning: snapshot)
    }
    
    deinit {
        view = nil
    }
}

private struct WebcamWebViewError : Error {
    var message: String
}

private struct WebcamWebViewRepresentable: UIViewRepresentable, Equatable {
    
    static func == (lhs: WebcamWebViewRepresentable, rhs: WebcamWebViewRepresentable) -> Bool {
        true
    }
    
    private static let tag = "WebpageWebcamRepresentable"
    let coordinator: WebpageWebcamCoordinator
    var content: WebcamControlsViewModel.WebContent
    
    func makeUIView(context: Context) -> WKWebView {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true
        webConfiguration.allowsAirPlayForMediaPlayback = true
        webConfiguration.allowsPictureInPictureMediaPlayback = true
        webConfiguration.applicationNameForUserAgent = "OctoApp"
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []
        webConfiguration.defaultWebpagePreferences.allowsContentJavaScript = true
        webConfiguration.userContentController.add(coordinator, name: coordinator.logName)
        webConfiguration.userContentController.add(coordinator, name: coordinator.stateName)
        webConfiguration.userContentController.add(coordinator, name: coordinator.snapshotName)
        
        Napier.i(tag: Self.tag, message: "WebView created")
        return WKWebView(frame: .zero, configuration: webConfiguration)
    }

    
    func updateUIView(_ webView: WKWebView, context: Context) {
        if coordinator.lastContent != content || coordinator.view != webView {
            coordinator.view = webView
            coordinator.reinitView(content: content)
            coordinator.lastContent = content
        }
    }
    
    func makeCoordinator() -> WebpageWebcamCoordinator {
        return coordinator
    }
    
    static func dismantleUIView(_ uiView: WKWebView, coordinator: WebpageWebcamCoordinator) {
        Napier.i(tag: tag, message: "Destroying WebView, view removed")
        coordinator.killVideo()
        coordinator.view = nil
        coordinator.lastContent = nil
    }
}

enum WebviewState : Equatable {
    case loading, playing(width: Int, height: Int), error(errorMessage: String)
}

