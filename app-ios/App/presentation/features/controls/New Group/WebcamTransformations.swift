//
//  WebcamTransformations.swift
//  OctoApp
//
//  Created by Christian on 10/02/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

private struct WebcamTransformationModifier: ViewModifier {

    var flipH: Bool
    var flipV: Bool
    var rotate90: Bool
    var imageSize: CGSize
    var fill = false
    var canZoom = true
    var showScale = false
    @Binding var zoomedIn: Bool
    @State private var zoom: CGFloat = 1
    @State private var zoomAnchor: UnitPoint = .center
    @State private var lastZoom: CGFloat = 1
    @State private var pan: CGSize = .zero
    @State private var lastPan: CGSize = .zero
    private var rotatedImageSize: CGSize { rotate90 ? CGSize(width: imageSize.height, height: imageSize.width) : imageSize }
   
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            let magnificationGesture = MagnificationGesture()
                .onChanged{ gesture in
                    zoomAnchor = .center
                    zoom = lastZoom * gesture
                    zoomedIn = zoom > 1
                }
                .onEnded { _ in
                    fixOffsetAndScale(geometry: geometry)
                }
            
            let dragGesture = DragGesture()
                .onChanged { gesture in
                    if (zoom > 1) {
                        var newOffset = lastPan
                        newOffset.width += gesture.translation.width
                        newOffset.height += gesture.translation.height
                        pan = newOffset
                    }
                }
                .onEnded { _ in
                    fixOffsetAndScale(geometry: geometry)
                }
            
            let doubleTapGesture = TapGesture(count: 2)
                .onEnded { _ in
                    withAnimation {
                        zoom = zoom > 1 ? 1 : 2
                        lastZoom = zoom
                        zoomedIn = zoom > 1
                        fixOffsetAndScale(geometry: geometry)
                    }
                }
            
            ZStack {
                image(size: geometry.size, content: content)
                scale(size: geometry.size)
            }
            .background(.black) // Needed since Xcode 16! Otherwise the gestures are not detected
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .position(
                x: geometry.size.width / 2,
                y: geometry.size.height / 2
            )
            .scaleEffect(zoom, anchor: zoomAnchor)
            .offset(pan)
            .if(canZoom) { $0.gesture(dragGesture) }
            .if(canZoom) { $0.gesture(magnificationGesture).gesture(doubleTapGesture) }
        }
    }
    
    @ViewBuilder
    private func image(size: CGSize, content: Content) -> some View {
        content
            .scaleEffect(adjustSampleSizeScale(size))
            .scaleEffect(aspectFillScale(size))
            .if(flipH) { $0.scaleEffect(x: -1, y: 1) }
            .if(flipV) { $0.scaleEffect(x: 1, y: -1) }
            .if(rotate90) { $0.rotationEffect(.degrees(-90)).scaleEffect(rotationScale(size)) }
            .if(!showScale) { $0.scaleEffect(x: 1.03, y: 1.03) }
    }
    
    @ViewBuilder
    private func scale(size: CGSize) -> some View {
        if showScale {
            VStack {
                Text("\(rotationScale(size))")
                Text("\(rotatedImageSize.width.formatted())x\(rotatedImageSize.height.formatted())")
                Text("\(size.width.formatted())x\(size.height.formatted())")
            }
        }
    }
    
    private func adjustSampleSizeScale(_ contentSize: CGSize) -> CGFloat {
        if imageSize.width == 0 || imageSize.height == 0 { return 1 }
        
        return min(contentSize.width / imageSize.width, contentSize.height / imageSize.height)
    }
        
    private func aspectFillScale(_ contentSize: CGSize) -> CGFloat {
        if !fill { return 1 }
        
        let rotatedSize = rotatedImageSize
        let contentAspect = contentSize.width / contentSize.height
        let rotatedAspect = rotatedSize.width / rotatedSize.height
        
        if rotatedAspect > contentAspect {
            return rotatedAspect / contentAspect
        } else {
            return contentAspect / rotatedAspect
        }
    }
    
    private func rotationScale(_ contentSize: CGSize) -> CGFloat {
        let rotatedSize = rotatedImageSize
        let size = imageSize
        let contentAspect = contentSize.width / contentSize.height
        let imageAspect = size.width / size.height
        let rotatedAspect = rotatedSize.width / rotatedSize.height
        
        var extraScale: CGFloat =  1
        
        if rotatedImageSize != imageSize && imageAspect >= contentAspect {
            extraScale = rotatedAspect / contentAspect
        }
        
        if contentAspect > imageAspect {
            return (size.height / size.width) * extraScale
        } else {
            return (size.width / size.height) * extraScale
        }
    }
    
    private func fixOffsetAndScale(geometry: GeometryProxy) {
        let newScale: CGFloat = .minimum(.maximum(zoom, 1), 4)
        let screenSize = geometry.size
        let rotatedSize = rotatedImageSize
        
        let originalScale = rotatedSize.width / rotatedSize.height >= screenSize.width / screenSize.height ?
        geometry.size.width / rotatedSize.width :
        geometry.size.height / rotatedSize.height
        
        let imageWidth = (rotatedSize.width * originalScale) * newScale
        
        var width: CGFloat = .zero
        if imageWidth > screenSize.width {
            let widthLimit: CGFloat = imageWidth > screenSize.width ?
            (imageWidth - screenSize.width) / 2
            : 0
            
            width = pan.width > 0 ?
                .minimum(widthLimit, pan.width) :
                .maximum(-widthLimit, pan.width)
        }
        
        let imageHeight = (rotatedSize.height * originalScale) * newScale
        var height: CGFloat = .zero
        if imageHeight > screenSize.height {
            let heightLimit: CGFloat = imageHeight > screenSize.height ?
            (imageHeight - screenSize.height) / 2
            : 0
            
            height = pan.height > 0 ?
                .minimum(heightLimit, pan.height) :
                .maximum(-heightLimit, pan.height)
        }
        
        let newOffset = CGSize(width: width, height: height)
        lastZoom = newScale
        lastPan = newOffset
        withAnimation() {
            pan = newOffset
            zoom = newScale
            zoomedIn = zoom > 1
        }
    }
    
}

extension View {
    func webcamTransformations(
        transformation: WebcamTransformation,
        showScale: Bool,
        canZoom: Bool,
        imageSize: CGSize,
        zoomedIn: Binding<Bool>
    ) -> some View {
        self.modifier(
            WebcamTransformationModifier(
                flipH: transformation.flipH,
                flipV: transformation.flipV,
                rotate90: transformation.rotate90,
                imageSize: imageSize,
                canZoom: canZoom,
                showScale: showScale,
                zoomedIn: zoomedIn
            )
        )
    }
}

struct WebcamTransformation : Equatable {
    var rotate90: Bool
    var flipH: Bool
    var flipV: Bool
}

