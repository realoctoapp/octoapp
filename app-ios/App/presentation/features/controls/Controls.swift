//
//  Controls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct Controls: View {
    
    @Environment(\.instanceId) private var instanceId
    @Environment(\.instanceLabel) private var instanceLabel
    @Environment(\.openURL) private var openUrl
    @Environment(\.controlsInEditMode) private var controlsInEditMode
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @StateObject private var viewModel = ControlsViewModel()
    @State private var scrolledToTop = true
    @State private var showMainMenu = false
    @State private var showTemperaturePresets = false
    @State private var lastControls: [ControlsViewModel.Control] = []
    @State private var safeAreaTop = getSafeAreaTop()
    private var printing: Bool {
        switch viewModel.state {
        case .print(_, _): return true
        default: return false
        }
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            emptyState
            scrollView
            decoration
        }
        .toolbar {
            ToolbarItem(placement: .bottomBar) {
                bottomBar
            }
        }
        .toolbarRole(.navigationStack)
        .usingViewModel(viewModel)
        .macOsCompatibleSheet(isPresented: $showMainMenu) {
            MenuHost(menu: MainMenu(instanceId: instanceId), menuId: .mainmenu) { result in
                if (result == SettingsMenu.companion.ResultShowControlCenter) {
                    orchestrator.controlCenterShown = true
                } else if (result == SettingsMenu.companion.ResultCustomizeControls) {
                    orchestrator.controlsInEditMode = true
                }
                
                showMainMenu = false
            }
        }
        .macOsCompatibleSheet(isPresented: $showTemperaturePresets) {
            MenuHost(menu: TemperatureMenu(instanceId: instanceId))
        }
        .animation(.spring(), value: controlsInEditMode)
        .environmentObject(viewModel)
        .screenOnWhilePrinting(printing: printing)
        .connectActionMediator()
    }
    
    @ViewBuilder
    var emptyState: some View {
        if (controlTypes().isEmpty) {
            ZStack {
                ProgressView()
            }
            .frame(maxHeight: .infinity)
        }
    }
    
    var decoration: some View {
        ControlsDecoration(
            scrolledToTop: $scrolledToTop
        )
    }
    
    var scrollView: some View {
        ScrolledToTopScrollView(scrolledToTop: $scrolledToTop) {
            ControlsContent(
                controlTypes: controlTypes(),
                updateOrder: { viewModel.updateOrder(order: $0) },
                togggleVisible: { viewModel.toggleVisible(type: $0) }
            )
            .padding(.top, OctoTheme.dimens.margin1)
            .onChange(of: controlTypes()) { _, controlTypes in
                lastControls = controlTypes
            }
        }
        // We ignore the safe are top and apply the fixed safe are manually.
        // This prevents the scroll view from moving down with the orchestrator banner
        .padding(.top, safeAreaTop)
        .ignoresSafeArea(.container, edges: .top)
    }
    
    var bottomBar: some View {
        ControlsBottomBar(
            state: viewModel.state,
            onTogglePause: { try await viewModel.togglePausePrint() },
            onCancel: { try await  viewModel.cancelPrint() },
            onEmergencyStop: { try await  viewModel.emergencyStop() },
            onOpenMenu: { showMainMenu = true},
            onShowFiles: { openUrl(UriLibrary.shared.getFileManagerUri(instanceId: instanceId, path: nil, label: instanceLabel, folder: nil)) },
            onShowTemperatures: { showTemperaturePresets = true }
        )
        .mask(Rectangle().padding(.bottom, -30))
        .padding([.leading, .trailing], -20)
        .frame(maxWidth: .infinity)
    }
    
    private func controlTypes() -> [ControlsViewModel.Control] {
        let filter: (ControlsViewModel.Control) -> Bool = { controlsInEditMode || $0.visible }
        
        // We go into initial state after we switched an instance, to prevent flicker etc we reuse the last controls until
        // the new VM is providing a state
        switch(viewModel.state) {
        case .initial: return lastControls
        case .connect(controls: let controls): return controls.filter(filter)
        case .prepare(controls: let controls): return controls.filter(filter)
        case .print(controls: let controls, info: _): return controls.filter(filter)
        }
    }
}

struct ControlsDecoration : View {
    
    @Binding var scrolledToTop: Bool
    @EnvironmentObject var viewModel: ControlsViewModel
    @State private var topShadowRadius: CGFloat = 0
    @State private var headerAlpha: CGFloat = 1
    private let shadowRadius : CGFloat = 2

    var body: some View {
        ZStack(alignment: .top) {
            hairline
            header
        }
        .animation(.default, value: headerAlpha)
        .safeAreaInset(edge: .top) { headerBackground }
        .onChange(of: scrolledToTop) { _, value in
            // Don't use "normal" if/else to only trigger the change once and not constantly
            if !scrolledToTop && topShadowRadius == 0 {
                topShadowRadius = shadowRadius
                headerAlpha = 0
            } else if scrolledToTop && topShadowRadius > 0 {
                topShadowRadius = 0
                headerAlpha = 1
            }
        }
    }
    
    var hairline: some View {
        ControlsHairline()
            .opacity(1 - headerAlpha)
    }
    
    var headerBackground: some View {
        ZStack {}
            .frame(maxWidth: .infinity)
            .background(OctoTheme.colors.windowBackground)
            .shadow(radius: topShadowRadius)
    }
    
    var header: some View {
        ZStack {
            ControlsHeader(state: viewModel.state)
                .opacity(headerAlpha)
            
            HStack{
                Spacer()
                ControlsInstanceSwitcher()
                    .shadow(radius: (1 - headerAlpha) * shadowRadius * 2)
            }
        }
        .padding(.top, OctoTheme.dimens.margin1)
    }
}


private struct ControlToggleVisibleKey: EnvironmentKey {
    static let defaultValue: () -> Void = {}
}

private struct ControlIsVisibleKey: EnvironmentKey {
    static let defaultValue = true
}

extension EnvironmentValues {
    var controlToggleVisible: () -> Void {
        get { self[ControlToggleVisibleKey.self] }
        set { self[ControlToggleVisibleKey.self] = newValue }
    }
    
    var controlIsVisible: Bool {
        get { self[ControlIsVisibleKey.self] }
        set { self[ControlIsVisibleKey.self] = newValue }
    }
}

private struct ControlsContent: View, Equatable {
    
    static func == (lhs: ControlsContent, rhs: ControlsContent) -> Bool {
        lhs.controlTypes == rhs.controlTypes
    }
    
    var controlTypes: [ControlsViewModel.Control]
    var updateOrder: ([ControlsViewModel.ControlType]) -> Void
    var togggleVisible: (ControlsViewModel.ControlType) -> Void
    private var idiom : UIUserInterfaceIdiom { UIDevice.current.userInterfaceIdiom }
    
    // ViewModels areattached in the controls so it's only active while visible
    @StateObject private var webcamControlsViewModel = WebcamControlsViewModel()
    @StateObject private var quickPrintControlsViewModel = QuickPrintControlsViewModel()
    @StateObject private var quickAccessControlsViewModel = QuickAccessControlsViewModel()
    @StateObject private var moveControlsViewModel = MoveControlsViewModel()
    @StateObject private var executeGcodeCotnrolsViewModel = ExecuteGcodeCotnrolsViewModel()
    @StateObject private var extrudeControlsViewModel = ExtrudeControlsViewModel()
    @StateObject private var temperatureControlsViewModel = TemperatureControlsViewModel()
    @StateObject private var progressControlsViewModel = ProgressControlsViewModel()
    @StateObject private var tuneControlsViewModel = TuneControlsViewModel()
    @StateObject private var multiToolControlsViewModel = MultiToolControlsViewModel()
    @StateObject private var cancelObjectViewModel = CancelObjectViewModel()
    @StateObject private var printConfidenceViewModel = PrintConfidenceViewModel()
    @StateObject private var gcodePreviewControlsViewModel = GcodePreviewControlsViewModel()
    @StateObject private var connectionControlsViewModel = ConnectionControlsViewModel()
    @StateObject private var bedMeshControlsViewModel = BedMeshControlsViewModel()
    @StateObject private var announcementControlsVieWModel = AnnouncementControlsVieWModel()
    @StateObject private var saveConfigControlsViewModel = SaveConfigControlsViewModel()
    
    @Namespace private var animation
    @Environment(\.controlsInEditMode) private var controlsInEditMode
    @State private var draggedItem: ControlsViewModel.ControlType? = nil
    @State private var webcamNativeImageSize: CGSize = CGSize.zero
    @State private var sortedControlTypes: [ControlsViewModel.Control] = []
    
    var body: some View {
        controls
            .onAppear { sortedControlTypes = controlTypes }
            .onChange(of: controlTypes) { _, new in sortedControlTypes = new }
            .onChange(of: controlsInEditMode) { draggedItem = nil }
            .padding(.top, OctoTheme.dimens.margin5)
            .padding(.bottom, OctoTheme.dimens.margin2)
            .animation(.spring(), value: webcamNativeImageSize)
            .animation(.spring(), value: sortedControlTypes)
            .animation(.spring(), value: gcodePreviewControlsViewModel.hidden)
            .animation(.default, value: webcamControlsViewModel.state.type)
            .animation(.default, value: quickPrintControlsViewModel.state)
            .animation(.default, value: executeGcodeCotnrolsViewModel.visible)
            .animation(.default, value: executeGcodeCotnrolsViewModel.items)
            .animation(.default, value: extrudeControlsViewModel.visible)
            .animation(.default, value: extrudeControlsViewModel.items)
            .animation(.default, value: temperatureControlsViewModel.observable.type)
            .animation(.default, value: temperatureControlsViewModel.controllable.type)
            .animation(.default, value: progressControlsViewModel.state?.settings)
            .animation(.default, value: connectionControlsViewModel.state)
            .animation(.default, value: tuneControlsViewModel.state)
            .animation(.default, value: multiToolControlsViewModel.state)
            .animation(.default, value: cancelObjectViewModel.state)
            .animation(.default, value: printConfidenceViewModel.state)
            .animation(.default, value: moveControlsViewModel.state)
            .animation(.default, value: announcementControlsVieWModel.announcements)
            .animation(.default, value: saveConfigControlsViewModel.state)
            .animation(.default, value: draggedItem)
            .onChange(of: controlTypes) { _, controlTypes in Napier.i(tag: "Controls", message: "\(controlTypes)") }
            .usingViewModel(temperatureControlsViewModel, active: controlTypes.contains { $0.type == .temperature })
            .usingViewModel(moveControlsViewModel, active: controlTypes.contains { $0.type == .move })
            .usingViewModel(connectionControlsViewModel, active: controlTypes.contains { $0.type == .connect })
            .usingViewModel(progressControlsViewModel, active: controlTypes.contains { $0.type == .progress })
            .usingViewModel(extrudeControlsViewModel, active: controlTypes.contains { $0.type == .extrude })
            .usingViewModel(executeGcodeCotnrolsViewModel, active: controlTypes.contains { $0.type == .sendGcode })
            .usingViewModel(gcodePreviewControlsViewModel, active: controlTypes.contains { $0.type == .gcodePreview })
            .usingViewModel(tuneControlsViewModel, active: controlTypes.contains { $0.type == .tune })
            .usingViewModel(quickAccessControlsViewModel, active: controlTypes.contains { $0.type == .quickAccess })
            .usingViewModel(quickPrintControlsViewModel, active: controlTypes.contains { $0.type == .quickPrint })
            .usingViewModel(multiToolControlsViewModel, active: controlTypes.contains { $0.type == .multiTool })
            .usingViewModel(cancelObjectViewModel, active: controlTypes.contains { $0.type == .cancelObject })
            .usingViewModel(webcamControlsViewModel, active: controlTypes.contains { $0.type == .webcam })
            .usingViewModel(printConfidenceViewModel, active: controlTypes.contains { $0.type == .webcam })
            .usingViewModel(bedMeshControlsViewModel, active: controlTypes.contains { $0.type == .bedMesh })
            .usingViewModel(announcementControlsVieWModel, active: controlTypes.contains { $0.type == .announcement })
            .usingViewModel(saveConfigControlsViewModel, active: controlTypes.contains { $0.type == .saveConfig })
    }
    
    @ViewBuilder
    var controls: some View {
        if sortedControlTypes.contains(where: { $0.type == .connect }) {
            ConnectControls(viewModel: connectionControlsViewModel)
        }
        
        if sortedControlTypes.contains(where: { $0.type == .announcement}) {
            AnnouncementControls(viewModel: announcementControlsVieWModel)
        }
        
        if sortedControlTypes.contains(where: { $0.type == .saveConfig}) {
            SaveConfigControls(viewModel: saveConfigControlsViewModel)
        }
        
        ControlsLayout {
            ForEach(sortedControlTypes, id: \.type) { control in
                VStack {
                    switch(control.type) {
                    case .connect: do {}
                    case .announcement: do {}
                    case .saveConfig: do {}
                    case .progress:  ProgressControls(viewModel: progressControlsViewModel, gcodeViewModel: gcodePreviewControlsViewModel)
                    case .temperature: TemperatureControls(viewModel: temperatureControlsViewModel)
                    case .move: MoveControls(viewModel: moveControlsViewModel)
                    case .extrude: ExtrudeControls(viewModel: extrudeControlsViewModel)
                    case .sendGcode: ExecuteGcodeControls(viewModel: executeGcodeCotnrolsViewModel)
                    case .gcodePreview: GcodePreviewControls(viewModel: gcodePreviewControlsViewModel)
                    case .tune: TuneControls(viewModel: tuneControlsViewModel)
                    case .quickAccess: QuickAccessControls(viewModel: quickAccessControlsViewModel)
                    case .quickPrint:  QuickPrintControls(viewModel: quickPrintControlsViewModel)
                    case .multiTool: MultiToolControls(viewModel: multiToolControlsViewModel)
                    case .cancelObject: CancelObjectControls(viewModel: cancelObjectViewModel)
                    case .bedMesh: BedMeshControls(viewModel: bedMeshControlsViewModel)
                    case .webcam: WebcamControls(
                        viewModel: webcamControlsViewModel,
                        confidenceViewModel: printConfidenceViewModel,
                        nativeImageSize: $webcamNativeImageSize
                    )
                    }
                }
                .environment(\.controlIsVisible, control.visible)
                .environment(\.controlToggleVisible, { togggleVisible(control.type) })
                .matchedGeometryEffect(id: control.type, in: animation)
                .onDrag {
                        draggedItem = control.type
                    return NSItemProvider(object: control.type.id as NSString)
                }
                .onDrop(
                    of: [.text],
                    delegate: DropViewDelegate(
                        destinationItem: control.type,
                        controlTypes: $sortedControlTypes,
                        draggedItem: $draggedItem,
                        afterDropped: updateOrder
                    )
                )
            }
        }
    }
}

struct DropViewDelegate: DropDelegate {
    
    let destinationItem: ControlsViewModel.ControlType
    @Binding var controlTypes: [ControlsViewModel.Control]
    @Binding var draggedItem: ControlsViewModel.ControlType?
    let afterDropped: ([ControlsViewModel.ControlType]) -> Void
    
    func dropUpdated(info: DropInfo) -> DropProposal? {
        return DropProposal(operation: .move)
    }
    
    func performDrop(info: DropInfo) -> Bool {
        draggedItem = nil
        return true
    }
    
    
    func dropEntered(info: DropInfo) {
        // Swap Items
        if let draggedItem {
            let fromIndex = controlTypes.firstIndex { $0.type == draggedItem }
            if let fromIndex {
                let toIndex = controlTypes.firstIndex { $0.type == destinationItem }
                if let toIndex, fromIndex != toIndex {
                    controlTypes.move(
                        fromOffsets: IndexSet(integer: fromIndex),
                        toOffset: (toIndex > fromIndex ? (toIndex + 1) : toIndex)
                    )
                    afterDropped(controlTypes.map { $0.type })
                }
            }
        }
    }
}

fileprivate extension [ControlsViewModel.ControlType] {
    func hasControl(_ type: ControlsViewModel.ControlType) -> Bool {
        contains { $0 == type }
    }
}

//struct Controls_Previews: PreviewProvider {
//    static var previews: some View {
//        Controls()
//    }
//}
