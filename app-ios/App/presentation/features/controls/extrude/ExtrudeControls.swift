//
//  Extrude.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Flow
import Combine
import OctoAppBase

struct ExtrudeControls: View {
    
    @StateObject var viewModel: ExtrudeControlsViewModel
    @Environment(\.instanceId) private var instanceId
    @State private var showOtherDistance = false
    @State private var showColdExtrusion = false
    @State private var showConfirmation = false
    @State private var showSettings = false
    @State private var showConfirmationDistance : Int32 = 0
    
    private var extrudeConfirmation: String {
        String(format:"widget_extrude___confirmation_message"~, showConfirmationDistance.formatted())
    }
    
    var body: some View {
        if viewModel.visible {
            control
        }
    }
    
    var control: some View {
        GenericHistoryItemsControl(
            title: "extrude"~,
            iconSystemName: "slider.horizontal.3",
            iconAction: { showSettings = true },
            otherText: "other"~,
            options: [GenericHistoryItemGroup(label: nil, items: viewModel.items)],
            optionSelected: { item in
                try await extrude(distance: item.data)
            },
            optionLongPressed:  { item in
                viewModel.togglePinned(distanceMm: item.data)
            },

            otherSelected: {
                showOtherDistance = true
            }
        )
        .alert(
            "error_cold_extrusion"~,
            isPresented: $showColdExtrusion,
            actions: {}
        )
        .confirmationDialog(
            extrudeConfirmation,
            isPresented: $showConfirmation,
            actions:  {
                Button("cancel"~, role: .cancel) {}
                Button("extrude_retract"~) {
                    Task {
                        try? await extrude(distance: showConfirmationDistance, confirmed: true)
                    }
                }
            },
            message: {
                Text(extrudeConfirmation)
            }
        )
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: ExtrudeControlsSettingsMenu(instanceId: instanceId))
        }
        .macOsCompatibleSheet(isPresented: $showOtherDistance) {
            ExtrudeControlsOther { distanceMm in
                Task {
                    try? await extrude(distance: distanceMm, confirmed: true)
                }
            }
        }
    }
    
    func extrude(distance: Int32, confirmed: Bool = false) async throws {
        switch(try await viewModel.extrude(distanceMm: distance, confirmed: confirmed)) {
        case .coldextrusionprevented: showColdExtrusion = true
        case .needsconfirmation: do {
            showConfirmation = true
            showConfirmationDistance = distance
        }
        default: do {}
        }
    }
}


struct Extrude_Previews: PreviewProvider {
    static var previews: some View {
        ExtrudeControls(viewModel: ExtrudeControlsViewModel())
    }
}
