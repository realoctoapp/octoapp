//
//  ExtrudeControlsOther.swift
//  OctoApp
//
//  Created by Christian Würthner on 22/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ExtrudeControlsOther: View {
    
    var distanceEntered: (Int32) -> Void
    
    var body: some View {
        MenuValueInput(
            title: "extrude_retract"~,
            value: "",
            hint: "distance_in_mm_negative_for_retract"~,
            confirmButton: "extrude_retract"~,
            keyboardType: .numbersnegative,
            validator: { input in
                if (input.allSatisfy { $0.isNumber || $0 == Character("-")}) {
                    return nil
                } else {
                    return "error_please_enter_a_value"~
                }
            },
            action: { input, _, _ in
                if let disanceMm = Int32(input) {
                    distanceEntered(disanceMm)
                }
            }
        )
    }
}

struct ExtrudeControlsOther_Previews: PreviewProvider {
    static var previews: some View {
        ExtrudeControlsOther { distanceMm in
            
        }
    }
}
