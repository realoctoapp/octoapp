//
//  GcodePreviewControlsFullscreen.swift
//  OctoApp
//
//  Created by Christian Würthner on 31/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

extension GcodePreviewControlsViewModel {
    func handleAppear(file: FileObjectStruct?) {
        if file != nil {
            useManual(file: file, layerIndex: 0, layerProgress: 1)
        }
    }
}

struct GcodePreviewControlsFullscreen: View {
    
    @StateObject var viewModel: GcodePreviewControlsViewModel
    var file: FileObjectStruct? = nil
    var canUseLive = true

    var body: some View {
        NavigationStack {
            GcodePreviewControlsFullscreenInternal(
                viewModel: viewModel,
                file: file,
                canUseLive: canUseLive
            )
            .toolbarDoneButton()
            .onAppear {
                viewModel.handleAppear(file: file)
            }
        }
    }
}

struct GcodePreviewControlsFullscreenEmbedded: View {

    var file: FileObjectStruct? = nil
    var canUseLive = false
    @StateObject private var viewModel = GcodePreviewControlsViewModel()

    var body: some View {
        GcodePreviewControlsFullscreenInternal(
            viewModel: viewModel,
            file: file,
            canUseLive: canUseLive
        )
        .usingViewModel(viewModel)
        .onAppear {
            viewModel.handleAppear(file: file)
        }
    }
}

private struct GcodePreviewControlsFullscreenInternal: View {
    
    @StateObject var viewModel: GcodePreviewControlsViewModel
    var file: FileObjectStruct? = nil
    var canUseLive = false
        
    var body: some View {
        GcodePreviewControlsFullscreenContent(
            state: viewModel.state,
            canUseLive: canUseLive,
            file: file,
            onRetry: { viewModel.retry() },
            onAllowLargeDownload: { viewModel.allowLargeDownloads() },
            onSetLive: { viewModel.useLive() },
            onSetManualProgress: { layer, layerProgress in
                viewModel.useManual(file: file, layerIndex: layer, layerProgress: layerProgress)
            }
        )
        .toolbarBackground(.visible, for: .navigationBar)
        .onChange(of: file) { _, newFile in
            viewModel.useManual(file: file, layerIndex: 0, layerProgress: 1)
        }
    }
}

struct GcodePreviewControlsFullscreenContent: View {
    
    var state: GcodePreviewControlsViewModel.State
    var canUseLive: Bool
    var file: FileObjectStruct?
    var onRetry: () -> Void
    var onAllowLargeDownload: () -> Void
    var onSetLive: () -> Void
    var onSetManualProgress: (_ layer: Int, _ layerProgress: Float) -> Void
    @State private var showSettings = false
    @State private var orientation = UIDevice.current.orientation
    @State private var exceedsPrintArea = false
    @State private var currentLayer: Int = 0
    @State private var currentLayerProgress: Float = 0
    
    private var renderContext: LowOverheadMediator<GcodeRenderContext>? {
        switch state {
        case .dataReady(
            printBed: _,
            printerProfile: _,
            settings: _,
            exceedsPrintArea: _,
            unsupportedGcode: _,
            isTrackingPrintProgress: _,
            renderContext: let renderContext
        ): return renderContext
        default: return nil
        }
    }
    
    private var featureDisabled: Bool {
        switch state {
        case .featureDisabled(printBed: _): return true
        default: return false
        }
    }
    
    var body: some View {
        GeometryReader { geo in
            if geo.size.width > geo.size.height {
                HStack(spacing: 0) {
                    canvas(landscape: true)
                    controls(landscape: true).frame(maxWidth: 350)
                }
            } else {
                VStack(spacing: 0) {
                    canvas(landscape: false)
                    controls(landscape: false)
                }
            }
        }
        .onRotate { newOrientation in
            orientation = newOrientation
        }
    }
    
    @ViewBuilder
    func controls(landscape: Bool) -> some View {
        if !featureDisabled {
            PreviewControls(
                renderContext: renderContext,
                onSetManualProgress: onSetManualProgress,
                layerMirror: $currentLayer,
                layerProgressMirror: $currentLayerProgress
            )
            .if(landscape) { $0.frame(maxWidth: .infinity, maxHeight: .infinity) }
            .padding(OctoTheme.dimens.margin2)
            .background(OctoTheme.colors.inputBackground)
            .onRotate { newOrientation in
                orientation = newOrientation
            }
        }
    }
    

    @ViewBuilder
    func canvas(landscape: Bool) -> some View {
        ZStack {
            switch state {
            case .loading(progress: let progress): GcodePreviewLoading(progress: progress)
            case .featureDisabled(printBed: let printBed): GcodePreviewFeatureDisabledFullscreen(printBed: printBed)
            case .error(exception: let exception): GcodePreviewError(exception: exception, onRetry: onRetry)
            case .printingFromSdCard(file: let file): GcodePreviewPrintingFromSdCard(file: file)
            case .largeFileDownloadRequired(
                downloadSize: let downloadSize
            ): GcodePreviewLargeFileDonwloadRequired(
                downloadSize: downloadSize,
                onAllowLargeDownload: onAllowLargeDownload
            )
            case .dataReady(
                printBed: let printBed,
                printerProfile: let printerProfile,
                settings: let settings,
                exceedsPrintArea: let exceedsPrintArea,
                unsupportedGcode: let unsupportedGcode,
                isTrackingPrintProgress: let isTrackingPrintProgress,
                renderContext: let renderContext
            ):  GeometryReader { geo in
                DataReady(
                    canUseLive: canUseLive,
                    printBed: printBed,
                    file: file,
                    printerProfile: printerProfile,
                    settings: settings,
                    exceedsPrintArea: exceedsPrintArea,
                    unsupportedGcode: unsupportedGcode,
                    isTrackingPrintProgress: isTrackingPrintProgress,
                    extraPaddingTop: geo.safeAreaInsets.top,
                    landscape: landscape,
                    renderContext: renderContext,
                    onLayerUp: { onSetManualProgress(currentLayer + 1, 1) },
                    onLayerDown: { onSetManualProgress(currentLayer - 1, 1) },
                    onSetLive: { live in
                        if live {
                            onSetLive()
                        } else {
                            onSetManualProgress(currentLayer, currentLayerProgress)
                        }
                    }
                )
                .ignoresSafeArea(edges: .top)
            }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

private struct PreviewControls : View {
    
    var renderContext: LowOverheadMediator<GcodeRenderContext>?
    var onSetManualProgress: (_ layer: Int, _ layerProgress: Float) -> Void
    @Binding var layerMirror: Int
    @Binding var layerProgressMirror: Float
    
    @State private var sinkId = UUID().uuidString
    @State private var disabled = true
    @State private var layerCount = 1
    @State private var layerHeightLabel: String = ""
    @State private var layerLabel: String = ""
    @State private var layerProgerssLabel: String = ""
    @State private var currentLayer: Float = 0
    @State private var currentLayerProgress: Float = 0
    @State private var receivedLayer: Float = 0
    @State private var receivedLayerProgress: Float = 0
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            PreviewControlSlider(
                title: "gcode_preview___layer"~,
                valueLabel: layerLabel,
                extraLabel: layerHeightLabel,
                maxValue: Float(layerCount + 1),
                step: 1,
                disabled: disabled,
                value: $currentLayer
            )
            
            PreviewControlSlider(
                title: "gcode_preview___layer_progress"~,
                valueLabel: layerProgerssLabel,
                extraLabel: "",
                maxValue: 1,
                step: 0.001,
                disabled: disabled,
                value: $currentLayerProgress
            )
        }
        .onChange(of: currentLayer) { _, new in
            if receivedLayer != new {
                receivedLayer = new
                Task {
                    try? await Task.sleep(for: .milliseconds(50))
                    if !Task.isCancelled {
                        onSetManualProgress(ensureLayerRange(layer: new), 1)
                        currentLayerProgress = 1
                        receivedLayerProgress = 1
                    }
                }
            }
        }
        .onChange(of: currentLayerProgress) { _, new in
            if receivedLayerProgress != new {
                receivedLayerProgress = new
                Task {
                    try? await Task.sleep(for: .milliseconds(50))
                    if !Task.isCancelled {
                        onSetManualProgress(ensureLayerRange(layer: currentLayer), new)
                    }
                }
            }
        }
        .onReceive(renderContext) { renderContext in
            consumeRenderContext(renderContext: renderContext)
        }
        .task(id: "\(receivedLayer);\(receivedLayerProgress)") {
            try? await Task.sleep(for: .milliseconds(50))
            if !Task.isCancelled {
                currentLayer = receivedLayer
                layerMirror = Int(receivedLayer)
                layerProgressMirror = receivedLayerProgress
                currentLayerProgress = receivedLayerProgress
            }
        }
    }
    
    var content: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            PreviewControlSlider(
                title: "gcode_preview___layer"~,
                valueLabel: layerLabel,
                extraLabel: layerHeightLabel,
                maxValue: Float(layerCount + 1),
                step: 1,
                disabled: disabled,
                value: $currentLayer
            )
            
            PreviewControlSlider(
                title: "gcode_preview___layer_progress"~,
                valueLabel: layerProgerssLabel,
                extraLabel: "",
                maxValue: 1,
                step: 0.001,
                disabled: disabled,
                value: $currentLayerProgress
            )
        }
        .tint(OctoTheme.colors.accent)
        .padding(OctoTheme.dimens.margin2)
    }
    
    func ensureLayerRange(layer: Float) -> Int {
        min(Int(layer), Int(layerCount - 2))
    }
    
    func consumeRenderContext(renderContext: GcodeRenderContext?) {
        guard let rc = renderContext else {
            disabled = true
            return
        }
        
        disabled = false
        layerCount = Int(rc.layerCount)
        layerHeightLabel = rc.layerZHeight.formatAsLayerHeight()
        receivedLayer = Float(rc.layerNumber)
        receivedLayerProgress = rc.layerProgress
        layerLabel = String(
            format: "x_of_y"~,
            rc.layerNumberDisplay(KotlinInt(integerLiteral: Int(rc.layerNumber))).intValue.formatted(),
            rc.layerCountDisplay(KotlinInt(integerLiteral: Int(rc.layerCount))).intValue.formatted()
        )
        layerProgerssLabel = (rc.layerProgress * 100).formatAsPercent()
    }
}

private struct PreviewControlSlider : View {
 
    var title: String
    var valueLabel: String
    var extraLabel: String
    var maxValue: Float
    var step: Float
    var disabled: Bool
    @Binding var value: Float
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            HStack {
                Text(title)
                    .typographySubtitle()
                Text(valueLabel)
                    .foregroundColor(OctoTheme.colors.lightText)
                    .typographySubtitle()
                Spacer()
                Text(extraLabel)
                    .foregroundColor(OctoTheme.colors.lightText)
                    .typographySubtitle()
            }
            
            
            Slider(
                value: $value,
                in: 0...max(1, maxValue),
                step: step
            )
            .disabled(disabled)
        }
        .animation(.spring(), value: value)
    }
}

private struct DataReady : View {
    
    var canUseLive: Bool
    var printBed: GcodeNativeCanvasImageStruct
    var file: FileObjectStruct?
    var printerProfile: PrinterProfileStruct?
    var settings: GcodePreviewSettingsStruct
    var exceedsPrintArea: Bool
    var unsupportedGcode: Bool
    var isTrackingPrintProgress: Bool
    var extraPaddingTop: CGFloat
    var landscape: Bool
    var renderContext: LowOverheadMediator<GcodeRenderContext>
    
    var onLayerUp: () -> Void
    var onLayerDown: () -> Void
    var onSetLive: (_ live: Bool) -> Void
    
    @State var canGoLayerDown = false
    @State var canGoLayerUp = false
    @State var lastUpdate: Date = Date.now
    @State var live = true
    @State var showSettings = false
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack {
                gcodePreview
                buttonBar
                badges
            }
            printAreaWarning
        }
        .onReceive(renderContext) { renderContext in
            guard let renderContext = renderContext else {
                return
            }
            
            live = true
            canGoLayerUp = (renderContext.layerCount - 1) > renderContext.layerNumber
            canGoLayerDown = renderContext.layerNumber > 0
            lastUpdate = Date.now
        }
        .task(id: lastUpdate) {
            try? await Task.sleep(for: gcodePreviewNotLiveAfter)
            if !Task.isCancelled {
                live = false
            }
        }
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: GcodeSettingsMenu())
        }
    }
    
    @ViewBuilder
    var printAreaWarning : some View {
        if exceedsPrintArea {
            HStack(spacing: OctoTheme.dimens.margin12) {
                Image(systemName: "exclamationmark.triangle.fill")
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
                    .font(.system(size: 24))
                
                Text(
                    String(
                        format: "gcode_preview___warning_print_area"~,
                        printerProfile?.volume.width.formatted() ?? "0",
                        printerProfile?.volume.depth.formatted() ?? "0",
                        printerProfile?.name ?? "???"
                    )
                )
                .foregroundColor(OctoTheme.colors.textColoredBackground)
                .typographyLabelSmall()
            }
            .frame(maxWidth: .infinity)
            .padding(OctoTheme.dimens.margin12)
            .background(OctoTheme.colors.yellow)
        }
    }
    
    var buttonBar: some View {
        ZStack {
            if landscape {
                VStack(spacing: OctoTheme.dimens.margin01) {
                    buttons
                }
                .padding([.top, .bottom], OctoTheme.dimens.margin01)
            } else {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    buttons
                }
                .padding([.leading, .trailing], OctoTheme.dimens.margin1)
            }
        }
        .clipShape(Capsule())
        .background(Capsule().fill(OctoTheme.colors.accent).shadow(radius: 3))
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: landscape ? .trailing : .bottom)
        .padding(OctoTheme.dimens.margin12)
    }
    
    @ViewBuilder
    var buttons: some View {
        OctoIconButton(
            icon: "arrow.up",
            color: OctoTheme.colors.textColoredBackground,
            clickListener: onLayerUp
        )
        .disabled(!canGoLayerUp)
        
        divider
        
        if canUseLive {
            OctoIconButton(
                icon: isTrackingPrintProgress ? "play.slash.fill" : "play.fill",
                color: OctoTheme.colors.textColoredBackground,
                clickListener: { onSetLive(!isTrackingPrintProgress) }
            )
            
            divider
        }
        
        OctoIconButton(
            icon: "square.3.layers.3d.top.filled",
            color: OctoTheme.colors.textColoredBackground,
            clickListener: { showSettings = true }
        )
        
        divider
        
        OctoIconButton(
            icon: "arrow.down",
            color: OctoTheme.colors.textColoredBackground,
            clickListener: onLayerDown
        )
        .disabled(!canGoLayerDown)
    }
    
    var divider : some View {
        OctoTheme.colors.textColoredBackground
            .opacity(0.3)
            .frame(width: landscape ? 24 : 1, height: landscape ? 1 : 24)
            .frame(maxHeight: .infinity)
            .fixedSize()
    }
    
    var badges: some View {
        HStack {
            if unsupportedGcode {
                TextTag(
                    text: "gcode_preview___unsupported_gcode"~,
                    color: OctoTheme.colors.yellow
                )
            }
            
            Spacer()
            
            WebcamLiveBadge(
                state: WebcamLiveBadge.StateWithDelay(
                    state: WebcamLiveBadge.State(live: live && isTrackingPrintProgress, evenOddFrame: false),
                    nextFrameDelay: 3000
                )
            )
        }
        .padding(OctoTheme.dimens.margin1)
        .padding(.top, extraPaddingTop)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
    }
    
    var gcodePreview: some View {
        ZStack {
            Color.clear.aspectRatio(1, contentMode: .fit)
            
            if let printerProfile = printerProfile {
                GcodeRenderView(
                    mediator: renderContext,
                    printBed: printBed,
                    printBedWidthMm: printerProfile.volume.width,
                    printBedHeightMm: printerProfile.volume.depth,
                    extrusionWidthMm: printerProfile.estimatedNozzleDiameter,
                    originInCenter: printerProfile.volume.origin == .center,
                    quality: settings.quality,
                    paddingHorizontal:  landscape ? OctoTheme.dimens.margin3 : OctoTheme.dimens.margin12,
                    paddingTop: extraPaddingTop + OctoTheme.dimens.margin2,
                    paddingBottom: 50 + OctoTheme.dimens.margin2
                )
            } else {
                OctoTheme.colors.inputBackgroundAlternative
                    .clipShape(RoundedRectangle(cornerRadius: OctoTheme.dimens.cornerRadiusSmall))
                
                Image(systemName: "exclamationmark.triangle.fill")
                    .foregroundColor(OctoTheme.colors.lightText)
                    .font(.system(size: 24))
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct GcodePreviewControlsFullscreen_Previews: PreviewProvider {
    
    static var lines: KotlinFloatArray {
        let a = KotlinFloatArray(size: 12)
        a.set(index: 0, value: 50)
        a.set(index: 1, value: 50)
        a.set(index: 2, value: 120)
        a.set(index: 3, value: 120)
        a.set(index: 4, value: 200)
        a.set(index: 5, value: 200)
        a.set(index: 6, value: 100)
        a.set(index: 7, value: 200)
        a.set(index: 8, value: 20)
        a.set(index: 9, value: 50)
        a.set(index: 10, value: 50)
        a.set(index: 11, value: 50)
        return a
    }
    
    static var previews: some View {
        let dataReady = GcodePreviewControlsViewModel.State.dataReady(
            printBed: .printbedender,
            printerProfile: PrinterProfileStruct(
                id: "some",
                current: true,
                default_: true,
                model: "Ender 3",
                name: "Ender 3",
                color: "sdf",
                volume: PrinterProfileStruct.Volume(
                    depth: 230,
                    width: 230,
                    height: 250,
                    origin: .lowerleft,
                    formFactor: .rectengular
                ),
                axes: PrinterProfileStruct.Axes(
                    e: PrinterProfileStruct.Axis(inverted: false, speed: 0),
                    x: PrinterProfileStruct.Axis(inverted: false, speed: 0),
                    y: PrinterProfileStruct.Axis(inverted: false, speed: 0),
                    z: PrinterProfileStruct.Axis(inverted: false, speed: 0)
                ),
                extruders: [
                    PrinterProfileStruct.Extruder(
                        nozzleDiameter: 0.4,
                        componentName: "extruder",
                        extruderComponents: ["extruder"]
                    ),
                ],
                heatedChamber: false,
                heatedBed: false,
                estimatedNozzleDiameter: 0.4
            ),
            settings: GcodePreviewSettingsStruct(
                showPreviousLayer: true,
                showCurrentLayer: false,
                layerOffset: 1,
                quality: .medium
            ),
            exceedsPrintArea: true,
            unsupportedGcode: false,
            isTrackingPrintProgress: true,
            renderContext: LowOverheadMediator(
                initialData: GcodeRenderContext(
                    previousLayerPaths: nil,
                    completedLayerPaths: [
                        GcodePath(
                            arcs: [],
                            lines: lines,
                            linesOffset: 0,
                            linesCount: 12,
                            type: .travel,
                            moveCount: 3
                        ),
                        GcodePath(
                            arcs: [],
                            lines: lines,
                            linesOffset: 0,
                            linesCount: 12,
                            type: .extrude,
                            moveCount: 3
                        )
                    ],
                    remainingLayerPaths: nil,
                    printHeadPosition: GcodePoint(x: 115, y: 115),
                    gcodeBounds: GcodeRect(top: 0, left: 0, right: 230, bottom: 230),
                    layerCount: 42,
                    layerNumber: 13,
                    layerZHeight: 2.4,
                    layerProgress: 0.45,
                    layerNumberDisplay: { KotlinInt(integerLiteral: $0.intValue + 1) },
                    layerCountDisplay: { KotlinInt(integerLiteral: $0.intValue + 1) }
                )
            )
        )
        
        GcodePreviewControlsFullscreenContent(
            state: dataReady,
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Printing")
        
        GcodePreviewControlsFullscreenContent(
            state: dataReady,
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Printing iPad 11")
        .previewDevice(PreviewDevice(rawValue: "iPad Pro (11-inch) (4th generation)"))
        .previewInterfaceOrientation(.landscapeLeft)
        
        GcodePreviewControlsFullscreenContent(
            state: dataReady,
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Printing iPad 12")
        .previewDevice(PreviewDevice(rawValue: "iPad Pro (12.9-inch) (6th generation)"))
        .previewInterfaceOrientation(.portrait)
        
        GcodePreviewControlsFullscreenContent(
            state: dataReady,
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Printing iPad mƒtoini")
        .previewDevice(PreviewDevice(rawValue: "iPad mini (6th generation)"))
        .previewInterfaceOrientation(.portrait)
        
        GcodePreviewControlsFullscreenContent(
            state: .loading(progress: 0),
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Loading initial")
        
        GcodePreviewControlsFullscreenContent(
            state: .loading(progress: 0.5),
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Loading progress")
        
        GcodePreviewControlsFullscreenContent(
            state: .featureDisabled(printBed: .printbedprusa),
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Feature disabled")
        
        GcodePreviewControlsFullscreenContent(
            state: .error(exception: KotlinException(message: "Something")),
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Error")
        
        GcodePreviewControlsFullscreenContent(
            state: .largeFileDownloadRequired(downloadSize: 2342342),
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Large download")
        
        GcodePreviewControlsFullscreenContent(
            state: .printingFromSdCard(
                file: FileObjectStruct(
                    origin: .other(name: "sdcard"),
                    display: "Some file.gcode",
                    name: "Some file.gcode",
                    path: "/",
                    isFolder: false,
                    isPrintable: true
                )
            ),
            canUseLive: true,
            onRetry: {},
            onAllowLargeDownload: {},
            onSetLive: {},
            onSetManualProgress: { _, _ in }
        )
        .previewDisplayName("Printing from SD")
    }
}
