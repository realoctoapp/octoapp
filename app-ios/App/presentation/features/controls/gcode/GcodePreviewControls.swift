//
//  GcodePreviewControls.swift
//  OctoApp
//
//  Created by Christian Würthner on 30/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

let gcodePreviewNotLiveAfter : Duration = .seconds(5)

struct GcodePreviewControls: View {
    
    @StateObject var viewModel: GcodePreviewControlsViewModel
    @State var showFullscreen = false
    
    var body: some View {
        content
    }
    
    @ViewBuilder
    var content: some View {
        if !viewModel.hidden {
            GcodePreviewControlsContent(
                state: viewModel.state,
                paused: showFullscreen,
                onRetry: { viewModel.retry() },
                onAllowLargeDownload: { viewModel.allowLargeDownloads() },
                onOpenFullscreen: { showFullscreen = true }
            )
            .animation(.default, value: viewModel.state.type)
            .fullScreenCover(isPresented: $showFullscreen) {
                GcodePreviewControlsFullscreen(viewModel: viewModel)
            }
            .onChange(of: showFullscreen) {
                // Always reset back to live when closing the fullscreen
                viewModel.useLive()
            }
        }
    }
}

private struct GcodePreviewControlsContent: View {
    
    var state: GcodePreviewControlsViewModel.State
    var paused: Bool
    var onRetry: () -> Void
    var onAllowLargeDownload: () -> Void
    var onOpenFullscreen: () -> Void
    @State var showSettings = false
    
    var body: some View {
        ControlsScaffold(
            title: "gcode_preview"~,
            iconSystemName: "slider.horizontal.3",
            iconAction: { showSettings = true },
            control: { box }
        )
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: GcodeSettingsMenu())
        }
    }
    
    var box: some View {
        ZStack {
            stateView
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .surface()
    }
    
    @ViewBuilder
    var stateView: some View {
        switch state {
        case .loading(progress: let progress): 
            GcodePreviewLoading(progress: progress)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .aspectRatio(16/10, contentMode: .fit)
            
        case .featureDisabled(printBed: let printBed): 
            GcodePreviewFeatureDisabled(printBed: printBed)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .aspectRatio(16/10, contentMode: .fit)
            
        case .error(exception: let exception):
            GcodePreviewError(exception: exception, onRetry: onRetry)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .aspectRatio(16/10, contentMode: .fit)
            
        case .printingFromSdCard(file: let file): 
            GcodePreviewPrintingFromSdCard(file: file)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .aspectRatio(16/10, contentMode: .fit)
            
        case .largeFileDownloadRequired(downloadSize: let downloadSize):
            GcodePreviewLargeFileDonwloadRequired(
                downloadSize: downloadSize,
                onAllowLargeDownload: onAllowLargeDownload
            )
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .aspectRatio(16/10, contentMode: .fit)
            
        case .dataReady(
            printBed: let printBed,
            printerProfile: let printerProfile,
            settings: let settings,
            exceedsPrintArea: let exceedsPrintArea,
            unsupportedGcode: let unsupportedGcode,
            isTrackingPrintProgress: let isTrackingPrintProgress,
            renderContext: let renderContext
        ): DataReady(
            printBed: printBed,
            printerProfile: printerProfile,
            settings: settings,
            paused: paused,
            exceedsPrintArea: exceedsPrintArea,
            unsupportedGcode: unsupportedGcode,
            isTrackingPrintProgress: isTrackingPrintProgress,
            renderContext: renderContext,
            onOpenFullscreen: onOpenFullscreen
        )
        }
    }
}

private struct DataReady : View {
    
    var printBed: GcodeNativeCanvasImageStruct
    var printerProfile: PrinterProfileStruct?
    var settings: GcodePreviewSettingsStruct
    var paused: Bool
    var exceedsPrintArea: Bool
    var unsupportedGcode: Bool
    var isTrackingPrintProgress: Bool
    var renderContext: LowOverheadMediator<GcodeRenderContext>
    var onOpenFullscreen: () -> Void
    
    @State private var layerProgress: String = " "
    @State private var layerNumber: String = " "
    @State private var lastUpdate: Date = Date.now
    @State private var live = true
    
    var body: some View {
        PrintBedLayout(
            printBed: printBed,
            printerProfile: printerProfile,
            settings: settings,
            paused: paused,
            exceedsPrintArea: exceedsPrintArea,
            unsupportedGcode: unsupportedGcode,
            isTrackingPrintProgress: isTrackingPrintProgress,
            renderContext: renderContext,
            overlay: {
                ZStack(alignment: .bottomTrailing) {
                    liveBadge
                    fullscreenButton
                }
            },
            legend: {
                info
            }
        )
        .onReceive(renderContext) { rc in
            consumeRenderContext(renderContext: rc)
        }
        .task(id: lastUpdate) {
            try? await Task.sleep(for: gcodePreviewNotLiveAfter)
            if !Task.isCancelled {
                live = false
            }
        }
    }
    
    var liveBadge: some View {
        WebcamLiveBadge(
            state: WebcamLiveBadge.StateWithDelay(
                state: WebcamLiveBadge.State(live: live, evenOddFrame: false),
                nextFrameDelay: 1000
            )
        )
        .padding(OctoTheme.dimens.margin1)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
    }
    
    var info: some View {
        VStack(alignment: .leading, spacing: OctoTheme.dimens.margin0) {
            Text("gcode_preview___layer").typographySubtitle().lineLimit(1)
            Text(layerNumber).typographyLabel().padding(.bottom, OctoTheme.dimens.margin1)
            Text("gcode_preview___layer_progress").typographySubtitle().lineLimit(1)
            Text(layerProgress).typographyLabel()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding([.top, .trailing, .bottom], OctoTheme.dimens.margin12)
        
    }
    
    var fullscreenButton: some View {
        OctoIconButton(
            icon: "arrow.up.backward.and.arrow.down.forward",
            clickListener: onOpenFullscreen
        )
        .padding(OctoTheme.dimens.margin01)
        .frame(alignment: .topTrailing)
    }
    
    func consumeRenderContext(renderContext: GcodeRenderContext?) {
        guard let renderContext = renderContext else {
            layerProgress = " "
            layerNumber = " "
            return
        }
        
        let number = KotlinInt(int: renderContext.layerNumber)
        let count = KotlinInt(int: renderContext.layerCount)
        
        live = true
        lastUpdate = Date.now
        layerProgress = (renderContext.layerProgress * 100).formatAsPercent()
        layerNumber = String(
            format: "x_of_y"~,
            renderContext.layerNumberDisplay(number).intValue.formatted(),
            renderContext.layerCountDisplay(count).intValue.formatted()
        )
    }
}


struct GcodePreviewControls_Previews: PreviewProvider {
//
//    static var lines: KotlinFloatArray {
//        let a = KotlinFloatArray(size: 12)
//        a.set(index: 0, value: 50)
//        a.set(index: 1, value: 50)
//        a.set(index: 2, value: 120)
//        a.set(index: 3, value: 120)
//        a.set(index: 4, value: 200)
//        a.set(index: 5, value: 200)
//        a.set(index: 6, value: 100)
//        a.set(index: 7, value: 200)
//        a.set(index: 8, value: 20)
//        a.set(index: 9, value: 50)
//        a.set(index: 10, value: 50)
//        a.set(index: 11, value: 50)
//        return a
//    }
//
    static var previews: some View {
//        GcodePreviewControlsContent(
//            state: .dataReady(
//                printBed: .printbedender,
//                printerProfile: PrinterProfileStruct(
//                    id: "some",
//                    current: true,
//                    default_: true,
//                    model: "Ender 3",
//                    name: "Ender 3",
//                    color: "sdf",
//                    volume: PrinterProfileStruct.Volume(
//                        depth: 230,
//                        width: 230,
//                        height: 250,
//                        origin: .lowerleft,
//                        formFactor: .rectengular
//                    ),
//                    axes: PrinterProfileStruct.Axes(
//                        e: PrinterProfileStruct.Axis(inverted: false, speed: 0),
//                        x: PrinterProfileStruct.Axis(inverted: false, speed: 0),
//                        y: PrinterProfileStruct.Axis(inverted: false, speed: 0),
//                        z: PrinterProfileStruct.Axis(inverted: false, speed: 0)
//                    ),
//                    extruders: [
//                        PrinterProfileStruct.Extruder(
//                            nozzleDiameter: 0.4,
//                            componentName: "extruder",
//                            extruderComponents: ["extruder"]
//                        )
//                    ],
//                    heatedChamber: false,
//                    heatedBed: false,
//                    estimatedNozzleDiameter: 0.4
//                ),
//                settings: GcodePreviewSettingsStruct(
//                    showPreviousLayer: true,
//                    showCurrentLayer: false,
//                    layerOffset: 1,
//                    quality: .medium
//                ),
//                exceedsPrintArea: false,
//                unsupportedGcode: false,
//                isTrackingPrintProgress: true,
//                renderContext: LowOverheadMediator(
//                    initialData: GcodeRenderContext(
//                        previousLayerPaths: nil,
//                        completedLayerPaths: [
//                            GcodePath(
//                                arcs: [],
//                                lines: lines,
//                                linesOffset: 0,
//                                linesCount: 12,
//                                type: .travel,
//                                moveCount: 3
//                            ),
//                            GcodePath(
//                                arcs: [],
//                                lines: lines,
//                                linesOffset: 0,
//                                linesCount: 12,
//                                type: .extrude,
//                                moveCount: 3
//                            )
//                        ],
//                        remainingLayerPaths: nil,
//                        printHeadPosition: GcodePoint(x: 115, y: 115),
//                        gcodeBounds: GcodeRect(top: 0, left: 0, right: 230, bottom: 230),
//                        layerCount: 42,
//                        layerNumber: 13,
//                        layerZHeight: 2.4,
//                        layerProgress: 0.45,
//                        layerNumberDisplay: { KotlinInt(integerLiteral: $0.intValue + 1) },
//                        layerCountDisplay: { KotlinInt(integerLiteral: $0.intValue + 1) }
//                    )
//                )
//            ),
//            paused: false,
//            onRetry: {},
//            onAllowLargeDownload: {},
//            onOpenFullscreen: {}
//        )
//        .previewDisplayName("Printing")
//
        GcodePreviewControlsContent(
            state: .loading(progress: 0),
            paused: false,
            onRetry: {},
            onAllowLargeDownload: {},
            onOpenFullscreen: {}
        )
        .fixedSize(horizontal: false, vertical: true)
        .previewDisplayName("Loading initial")

        GcodePreviewControlsContent(
            state: .loading(progress: 0.5),
            paused: false,
            onRetry: {},
            onAllowLargeDownload: {},
            onOpenFullscreen: {}
        )
        .fixedSize(horizontal: false, vertical: true)
        .previewDisplayName("Loading progress")

        GcodePreviewControlsContent(
            state: .featureDisabled(printBed: .printbedprusa),
            paused: false,
            onRetry: {},
            onAllowLargeDownload: {},
            onOpenFullscreen: {}
        )
        .fixedSize(horizontal: false, vertical: true)
        .previewDisplayName("Feature disabled")

        GcodePreviewControlsContent(
            state: .error(exception: KotlinException(message: "Something")),
            paused: false,
            onRetry: {},
            onAllowLargeDownload: {},
            onOpenFullscreen: {}
        )
        .fixedSize(horizontal: false, vertical: true)
        .previewDisplayName("Error")

        GcodePreviewControlsContent(
            state: .largeFileDownloadRequired(downloadSize: 2342342),
            paused: false,
            onRetry: {},
            onAllowLargeDownload: {},
            onOpenFullscreen: {}
        )
        .fixedSize(horizontal: false, vertical: true)
        .previewDisplayName("Large download")
        
        GcodePreviewControlsContent(
            state: .printingFromSdCard(
                file: FileObjectStruct(
                    origin: .other(name: "sdcard"),
                    display: "Some file.gcode",
                    name: "Some file.gcode",
                    path: "/",
                    isFolder: false,
                    isPrintable: true
                )
            ),
            paused: false,
            onRetry: {},
            onAllowLargeDownload: {},
            onOpenFullscreen: {}
        )
        .fixedSize(horizontal: false, vertical: true)
        .previewDisplayName("Printing from SD")
    }
}
