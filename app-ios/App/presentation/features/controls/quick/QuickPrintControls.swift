//
//  QuickPrintControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct QuickPrintControls: View {
    
    @StateObject var viewModel: QuickPrintControlsViewModel
    
    var body: some View {
        ZStack {
            switch (viewModel.state) {
            case .hidden: ZStack{}
            default: QuickPrintControlsItems(
                state: viewModel.state,
                reload: { viewModel.reload() },
                startPrint: { await viewModel.startPrint($0, materialConfirmed: $1, timelapseConfirmed: $2) }
            )
            }
        }
      
    }
}

private struct QuickPrintControlsItems: View {
    var state: QuickPrintControlsState
    var reload: () -> Void
    var startPrint: (String, Bool, Bool) async -> StartPrintResultStruct
    
    var body: some View {
        ControlsScaffold(title: "widget_quick_print"~) {
            ZStack {
                switch state {
                case .loading: LoadingPlaceholder()
                case .failed(let e): ErrorPlaceholder(throwable: e, reload: reload)
                case .hidden: do {}
                case .loaded(let items): VStack(spacing: OctoTheme.dimens.margin01) {
                    ForEach(items) { item in
                        QuickPrintControlsItem(item: item, startPrint: startPrint)
                    }
                }
                }
            }
        }
    }
}

private struct QuickPrintControlsItem: View {
    var item: QuickPrintFileLinkStruct
    var startPrint: (String, Bool, Bool) async -> StartPrintResultStruct

    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceId) private var instanceId
    @MainActor @State private var startPrintResult: StartPrintResultStruct = .success
    
    var body: some View {
        DataButton(
            iconSystemName: "printer.fill",
            clickListener: {
                startPrintResult = .failed
                let res = await startPrint(item.file.path, false, false)
                startPrintResult = res
            }
        ) {
            Button(
                action: {
                    openUrl(
                        UriLibrary.shared.getFileManagerUri(
                            instanceId: instanceId,
                            path: item.file.path,
                            label: item.file.display,
                            folder: KotlinBoolean(bool: item.file.isFolder)
                        )
                    )
                },
                label: {
                    HStack(spacing: OctoTheme.dimens.margin01) {
                        QuickPrintControlsThumbnail(thumbnailUrl: item.file.smallThumbnail)
                        QuickPrintControlsTexts(file: item.file, selected: item.selected)
                        Spacer()
                        Image(systemName: "chevron.right")
                            .foregroundColor(OctoTheme.colors.accent)
                    }
                    .padding([.top, .bottom], OctoTheme.dimens.margin0)
                    .padding(.trailing, OctoTheme.dimens.margin12)
                    .padding(.leading, OctoTheme.dimens.margin0)
                    .frame(maxWidth: .infinity)
                    .background(Capsule().fill(OctoTheme.colors.inputBackground))
                    .fixedSize(horizontal: false, vertical: true)
                }
            )
        }
        .animation(.default, value: item)
        .startPrintConfirmations(result: startPrintResult, filePath: item.file.path) { materialConfirmed, timelapseConfirmed in
            startPrintResult = await startPrint(
                item.file.path,
                materialConfirmed,
                timelapseConfirmed
            )
        }
    }
}

private struct QuickPrintControlsThumbnail: View {
    var thumbnailUrl: String?
    
    var body: some View {
        OctoPrintNetworkImage(
            url: thumbnailUrl,
            backupSystemName: "doc.fill",
            small: true
        )
        .clipShape(Circle())
        .frame(width: 32, height: 32)
        .padding(OctoTheme.dimens.margin01)
        .frame(maxHeight: .infinity)
    }
}

private struct QuickPrintControlsTexts: View {
    var file: FileObjectStruct
    var selected: Bool
    private var detail: String {
        if selected {
            return "file_manager___file_list___currently_selected"~
        } else if let lastPrintDate = file.prints?.lastPrintDate {
            let success = file.prints?.lastPrintSuccess == true
            let successText = "file_manager___file_details___last_print_at_x_success"
            let failureText = "file_manager___file_details___last_print_at_x_failure"
            return String(format: success ? successText~ : failureText~, lastPrintDate.formatted())
        } else {
            return "file_manager___file_list___just_uploaded"~
        }
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(file.display)
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyBase()
            Text(detail)
                .typographyLabelSmall()
        }
        .lineLimit(1)
    }
}

struct QuickPrintControls_Previews: PreviewProvider {
    static var previews: some View {
        QuickPrintControlsItems(
            state: .loaded(
                [
                    QuickPrintFileLinkStruct(
                        file: PreviewData.printableFile,
                        selected: true
                    ),
                    QuickPrintFileLinkStruct(
                        file: PreviewData.printableFile,
                        selected: false
                    ),
                    QuickPrintFileLinkStruct(
                        file: PreviewData.printableFile,
                        selected: false
                    )
                ]
            ),
            reload: {},
            startPrint: { _, _, _ in .materialConfirmationRequired(timelapseConfirmed: true) }
        )
        .previewDisplayName("Loaded")
        
        QuickPrintControlsItems(
            state: .loading,
            reload: {},
            startPrint: { _, _ ,_ in .materialConfirmationRequired(timelapseConfirmed: true) }
        )
        .previewDisplayName("Loading")
        
        QuickPrintControlsItems(
            state: .failed(KotlinThrowable()),
            reload: {},
            startPrint: { _, _, _ in .materialConfirmationRequired(timelapseConfirmed: true) }
        )
        .previewDisplayName("Failed")
    }
}
