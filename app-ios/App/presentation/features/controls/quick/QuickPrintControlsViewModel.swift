//
//  QuickPrintControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase

class QuickPrintControlsViewModel : BaseViewModel {
    var currentCore: QuickPrintControlsViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var state: QuickPrintControlsState = .loading
    @Published var fileStartPrintPath: String? = nil
    @Published var fileStartPrintState: StartPrintResultStruct = .failed

    func createCore(instanceId: String) -> QuickPrintControlsViewModelCore {
        return QuickPrintControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: QuickPrintControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: FlowState<QuickPrintControlsViewModelCore.State>) in
                if state is FlowStateLoading {
                    self.state = .loading
                } else if let error = state as? FlowStateError {
                    self.state = .failed(error.throwable)
                } else if let ready = state as? FlowStateReady {
                    let items =  ready.data!.files.map { file in
                        QuickPrintFileLinkStruct(
                            file: file.fileObject.toStruct(),
                            selected: file.selected
                        )
                    }
                    
                    self.state = items.isEmpty ? .hidden :.loaded(items)
                }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        Napier.i(tag: "QPC-DEBUG", message: "Clear data")
        state = .loading
    }
    
    func reload() {
        currentCore?.reload()
    }
    
    func startPrint(_ path: String, materialConfirmed: Bool, timelapseConfirmed: Bool) async -> StartPrintResultStruct {
        let result = try? await currentCore?.startPrint(
            path: path,
            materialConfirmed: materialConfirmed,
            timelapseConfirmed: timelapseConfirmed
        )
        return result?.toStruct() ?? .failed
    }
}

enum QuickPrintControlsState: Equatable {
    case loading
    case loaded([QuickPrintFileLinkStruct])
    case failed(KotlinThrowable)
    case hidden
}

struct QuickPrintFileLinkStruct: Equatable, Identifiable {
    var id: String { file.id }
    var file: FileObjectStruct
    var selected: Bool
}

extension QuickPrintControlsState {
    var isHidden: Bool {
        if case .hidden = self {
            return true
        } else {
            return false
        }
    }
}
