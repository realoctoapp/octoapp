//
//  AnnouncementControls.swift
//  OctoApp
//
//  Created by Christian on 21/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct AnnouncementControls: View {
 
    @StateObject var viewModel: AnnouncementControlsVieWModel
    
    var body: some View {
        AnnouncementControlsContent(
            announcements: viewModel.announcements,
            notifyNotificationsChanged: { viewModel.checkNotificationsNow() }
        )
    }
}

@MainActor
private struct AnnouncementControlsContent: View {
    
    var announcements: [AnnouncementStruct]
    var notifyNotificationsChanged: () -> Void
    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceId) private var instanceId
    @State private var playAnimations = false
    @State private var showNotificationSheet = false
    @State private var showCompanionSheet = false
    @State private var showCompanionMenu = false
    
    var body: some View {
        ControlsScaffold(title: nil, supportsEdit: false) {
            VStack(spacing: OctoTheme.dimens.margin1) {
                sandboxAnnouncement
                
                ForEach(announcements) { announcement in
                    AnnouncementController(announcementId: announcement.id) { hide in
                        SmallAnnouncement(
                            hasBackground: false,
                            text: announcement.text,
                            buttonTextColor: announcement.redColor ? OctoTheme.colors.red : OctoTheme.colors.accent,
                            learnMoreButton: announcement.canHide ? announcement.learnMoreText : nil,
                            hideButton: announcement.canHide ? "hide"~ : announcement.learnMoreText,
                            onLearnMore: { hanleLearnMore(announcement) },
                            onHideAnnouncement: { if announcement.canHide { hide() } else { hanleLearnMore(announcement) } }
                        )
                        .background {
                            if announcement.confettiBackground {
                                versionBackground
                            }
                        }
                        .surface(color: announcement.redColor ? .red : .normal)
                    }
                }
                
            }
            .frame(maxWidth: OctoTheme.dimens.maxButtonWidth)
            .macOsCompatibleSheet(isPresented: $showNotificationSheet) {
                AnnouncementNotificationSheet {
                    withAnimation {
                        notifyNotificationsChanged()
                        showNotificationSheet = false
                    }
                }
            }
            .macOsCompatibleSheet(isPresented: $showCompanionMenu) {
                MenuHost(menu: MoonrakerCompanionMenu(instanceId: instanceId))
            }
            .macOsCompatibleSheet(isPresented: $showCompanionSheet) {
                AnnouncementCompanionSheet {
                    withAnimation {
                        notifyNotificationsChanged()
                        showCompanionSheet = false
                    }
                }
            }
        }
    }
    
    private func hanleLearnMore(_ announcement: AnnouncementStruct) {
        if let url = announcement.learnMoreLink {
            switch url {
            case AnnouncementControlsViewModelCore.companion.LinkCompanionInstallSheet: showCompanionSheet = true
            case AnnouncementControlsViewModelCore.companion.LinkCompanionRunningMenu: showCompanionMenu = true
            case AnnouncementControlsViewModelCore.companion.LinkNotificationSheet: showNotificationSheet = true
            default: openUrl(url)
            }
        }
    }
    
    @ViewBuilder
    var sandboxAnnouncement: some View {
        if Bundle.main.appStoreReceiptURL?.path().localizedStandardContains("sandboxReceipt") == true {
            AnnouncementController(announcementId: "sandbox_announcement") { hide in
                SmallAnnouncement(
                    hasBackground: false,
                    text: "billing_testflight_announcement___text"~,
                    buttonTextColor: OctoTheme.colors.red,
                    hideButton: "billing_testflight_announcement___hide"~,
                    onHideAnnouncement: { hide() }
                )
                .surface(color: .red)
            }
        }
    }
    
    var versionBackground: some View {
        ZStack {
            if playAnimations {
                SimpleLottieView(lottieFile: "confetti-small")
                    .scaledToFill()
                OctoTheme.colors.inputBackground.opacity(0.4)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .surface()
        .animation(.default, value: playAnimations)
        .task {
            while !Task.isCancelled {
                try? await Task.sleep(for: .seconds(4))
                playAnimations = true
                try? await Task.sleep(for: .seconds(4))
                playAnimations = false
            }
        }
    }
}

class AnnouncementControlsVieWModel: BaseViewModel {
    var currentCore: AnnouncementControlsViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var announcements: [AnnouncementStruct] = []
    
    func createCore(instanceId: String) -> AnnouncementControlsViewModelCore {
        AnnouncementControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: AnnouncementControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: AnnouncementControlsViewModelCore.State) in
                self.announcements = state.announcements.map { AnnouncementStruct(announcement: $0) }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        announcements = []
    }
    
    func checkNotificationsNow() {
        Task {
            try? await currentCore?.checkNotificationNow()
        }
    }
}

struct AnnouncementStruct: Identifiable, Equatable {
    var id: String
    var text: String
    var learnMoreText: String?
    var learnMoreLink: String?
    var canHide: Bool
    var redColor: Bool
    var confettiBackground: Bool
}

extension AnnouncementStruct {
    init(announcement: AnnouncementControlsViewModelCore.Announcement) {
        self.init(
            id: announcement.id,
            text: announcement.text,
            learnMoreText: announcement.learnMoreText,
            learnMoreLink: announcement.learnMoreLink,
            canHide: announcement.canHide,
            redColor: announcement.redColor,
            confettiBackground: announcement.confettiBackground
        )
    }
}

struct AnnouncementControls_Previews: PreviewProvider {
    static var previews: some View {
        AnnouncementControlsContent(
            announcements: [
                AnnouncementStruct(
                    id: "1",
                    text: "Only learn more",
                    learnMoreText: "Learn more",
                    learnMoreLink: "sdf",
                    canHide: false,
                    redColor: false,
                    confettiBackground: false
                ),
                AnnouncementStruct(
                    id: "2",
                    text: "Learn more and hide",
                    learnMoreText: "Learn more",
                    learnMoreLink: "sdf",
                    canHide: true,
                    redColor: false,
                    confettiBackground: false
                ),
                AnnouncementStruct(
                    id: "3",
                    text: "Red",
                    learnMoreText: "Learn more",
                    learnMoreLink: AnnouncementControlsViewModelCore.companion.LinkNotificationSheet,
                    canHide: true,
                    redColor: true,
                    confettiBackground: false
                ),
                AnnouncementStruct(
                    id: "4",
                    text: "Confetti",
                    learnMoreText: "Learn more",
                    learnMoreLink: "sdf",
                    canHide: true,
                    redColor: false,
                    confettiBackground: true
                )
            ],
            notifyNotificationsChanged: {}
        )
    }
}
