//
//  TuneControlsDetails.swift
//  OctoApp
//
//  Created by Christian on 21/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import UIKit

struct TuneControlsInputs: View {
    
    @StateObject var viewModel: TuneControlsViewModel
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        TuneControlsInputsContent(
            state: viewModel.state,
            babyStepIntervals: viewModel.babyStepIntervals,
            applyValues:  { feedRate, flowRate, fanSpeeds in
                try await viewModel.applyChanges(
                    feedRate: feedRate,
                    flowRate: flowRate,
                    fanSpeeds: fanSpeeds
                )
            },
            changeOffset: { changeMm in
                Task {
                    try? await viewModel.changeOffset(
                        changeMm: changeMm
                    )
                }
            },
            resetOffset: {
                try await viewModel.resetOffset()
            },
            saveOffset: {
                try await viewModel.saveOffsetToConfig()
            }
        )
        .onAppear { viewModel.setActivePolling(active: true) }
        .onDisappear { viewModel.setActivePolling(active: false) }
        .frame(maxWidth: .infinity)
    }
}

private struct TuneControlsInputsContent: View {
    
    var state: TuneControlsState
    var babyStepIntervals: [Float]
    var applyValues: (_ feedRate: Int?, _ flowRate: Int?, _ fanSpeeds: [String:Int?]) async throws -> Void
    var changeOffset: (_ distance: Float) -> Void
    var resetOffset: () async throws -> Void
    var saveOffset: () async throws -> Void
    let feedRateKey = "__feedRate"
    let flowRateKey = "__flowRate"
    
    @State private var inputs: [String:String] = [:]
    @State private var synced = true
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(spacing: OctoTheme.dimens.margin1) {
                    content
                    Spacer()
                }
                .padding(OctoTheme.dimens.margin12)
            }
            .navigationTitle("tune"~)
            .navigationBarTitleDisplayMode(.large)
            .toolbarDoneButton()
        }
    }
    
    @ViewBuilder
    var content: some View {
        if state.isValuesImprecise {
            AnnouncementController(announcementId: "tune_data_disclaimer") { hide in
                LargeAnnouncement(
                    title: "tune_print___disclaimer_title"~,
                    text: "tune_print___disclaimer_detail"~,
                    hideButton: "tune_print___disclaimer_action_hide"~,
                    onHideAnnouncement: hide
                )
                .padding(.bottom, OctoTheme.dimens.margin1)
            }
        }
        
        VStack(spacing: OctoTheme.dimens.margin12) {
            VStack(spacing: OctoTheme.dimens.margin1) {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    TuneControlsInput(
                        value: state.feedRate,
                        labelActive: "tune_print___feed_rate_label_active",
                        placeholder:  "tune_print___feed_rate_label"~,
                        key: feedRateKey,
                        input: $inputs
                    )

                    TuneControlsInput(
                        value: state.flowRate,
                        labelActive: "tune_print___flow_rate_label_active",
                        placeholder:  "tune_print___flow_rate_label"~,
                        key: flowRateKey,
                        input: $inputs
                    )
                }

                ForEach(state.fanSpeeds.chunked(into: 2), id: \.self) { row in
                    HStack(spacing: OctoTheme.dimens.margin1) {
                        ForEach(row) { fan in
                            TuneControlsInput(
                                value: fan.speed,
                                labelActive: fan.label + " (%)",
                                placeholder: fan.label + " (%)",
                                key: fan.component,
                                input: $inputs
                            )
                        }
                    }
                }
                
                if !synced {
                    OctoAsyncButton(
                        text: "tune_print___apply"~,
                        small: true,
                        fillWidth: true
                    ) {
                        await clearFocus()
                        let fans = state.fanSpeeds.reduce(into: [String: Int]()) { map, fan in
                            map[fan.component] =  parseString(inputs[fan.component])
                        }
                        try? await applyValues(
                            parseString(inputs[feedRateKey]),
                            parseString(inputs[flowRateKey]),
                            fans
                        )
                        determineSynced(state: state, inputs: inputs)
                    }
                    .zIndex(-1000)
                    .transition(.move(edge: .top).combined(with: .opacity))
                }
            }
            .padding(OctoTheme.dimens.margin1)
            .surface()
            .onAppear {
                flushValues(state: state)
                determineSynced(state: state, inputs: inputs)
            }
            .onChange(of: state) { _, new in
                flushValues(state: new)
                determineSynced(state: new, inputs: inputs)
            }
            .onChange(of: inputs) { _, new in
                determineSynced(state: state, inputs: new)
            }

            BabyStepInput(
                offsetMm: state.zOffset,
                babyStepIntervals: babyStepIntervals,
                changeOffset: changeOffset,
                resetOffset: resetOffset,
                saveOffset: saveOffset
            )
        }
        .animation(.spring, value: synced)
    }
    
    func determineSynced(state: TuneControlsState, inputs: [String:String]) {
        let feedRateSynced = parseString(inputs[feedRateKey]) == state.feedRate
        let flowRateSynced = parseString(inputs[flowRateKey]) == state.flowRate
        let fansSynced = state.fanSpeeds.allSatisfy { parseString(inputs[$0.component]) == $0.speed }
        synced = feedRateSynced && flowRateSynced && fansSynced
        Napier.i(tag: "Debug", message: "Synced: \(feedRateSynced) \(flowRateSynced) \(fansSynced) -> \(synced)")
    }
    
    func parseString(_ string: String?) -> Int? {
        guard let s = string else { return nil }
        return Int(s)
    }
    
    func flushValues(state: TuneControlsState) {
        inputs[feedRateKey] = state.feedRate?.formatted() ?? ""
        inputs[flowRateKey] = state.flowRate?.formatted() ?? ""
        state.fanSpeeds.forEach { fan in
            inputs[fan.component] = fan.speed?.formatted() ?? ""
        }
    }
    
    @MainActor
    func clearFocus() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
    }
}

private extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

private struct TuneControlsInput: View {
    
    var value: Int?
    var labelActive: String
    var placeholder: String
    var key: String
    @Binding var input: [String:String]
    @State var internalInput: String = ""
    
    var body: some View {
        OctoTextField(
            placeholder: placeholder,
            labelActiv: LocalizedStringKey(labelActive),
            alternativeBackgroung: true, 
            input: $internalInput
        )
        .frame(maxWidth: .infinity)
        .keyboardType(.numberPad)
        .onChange(of: input[key]) { _, new in
            internalInput = new ?? ""
        }
        .onChange(of: internalInput) { _, new in
            input[key] = new
        }
        .onChange(of: value) { _, new in
            internalInput = new?.formatted() ?? ""
        }
        .onAppear {
            internalInput = value?.formatted() ?? ""
        }
    }
}

private struct BabyStepInput: View {
    
    var offsetMm: Float?
    var babyStepIntervals: [Float]
    var changeOffset: (_ distance: Float) -> Void
    var resetOffset: () async throws -> Void
    var saveOffset: () async throws -> Void
    @State private var showSaveExplainer = false
    @State private var saveLoading = false
    @State private var input = ""
    
    private var resetShown: Bool { abs(offsetMm ?? 0) > 0.0001 }
       
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            ZStack(alignment:.trailing) {
                OctoTextField(
                    placeholder: "tune_print___z_baby_step"~,
                    labelActiv: LocalizedStringKey("tune_print___z_baby_step"~),
                    input: $input
                )
                .disabled(true)
                
                if resetShown {
                    OctoAsyncIconButton(
                        icon: "xmark",
                        clickListener: resetOffset
                    )
                }
            }
            .padding([.leading, .trailing, .top], OctoTheme.dimens.margin1)
            .padding(.bottom, -OctoTheme.dimens.margin1)
            .onAppear { flushOffset(offsetMm: offsetMm) }
            .onChange(of: offsetMm) { _, new in flushOffset(offsetMm: new) }
         
            BabyStepDivider()
            
            BabyStepButtons(
                changeOffset: changeOffset,
                babyStepIntervals: babyStepIntervals
            )
            
            if resetShown {
                BabyStepDivider()
           
                OctoAsyncButton(
                    text: "tune_print___save_config"~,
                    small: true,
                    fillWidth: true,
                    loading: saveLoading,
                    clickListener: { showSaveExplainer = true }
                )
                .padding([.leading, .trailing, .bottom], OctoTheme.dimens.margin1)
                .confirmationDialog(
                    "",
                    isPresented: $showSaveExplainer,
                    actions: {
                        Button("tune_print___save_config"~) {
                            Task {
                                saveLoading = true
                                defer{ saveLoading = false }
                                try? await resetOffset()
                            }
                        }
                    },
                    message: {
                        Text(String(format:"tune_print___save_config_explainer"~))
                    }
                )
            } else {
                Spacer(minLength: OctoTheme.dimens.margin1)
            }
        }
        .surface()
        .animation(.default, value: resetShown)
    }
    
    private func flushOffset(offsetMm: Float?) {
        input = offsetMm?.formatAsLength(onlyMilli: true, minDecimals: 3, maxDecimals: 3) ?? ""
    }
}

private struct BabyStepButtons : View {
    
    var changeOffset: (_ distance: Float) -> Void
    var babyStepIntervals: [Float]
    @State private var length: BabyStepLength
    
    init(
        changeOffset: @escaping (_: Float) -> Void,
        babyStepIntervals: [Float]
    ) {
        self.changeOffset = changeOffset
        self.babyStepIntervals = babyStepIntervals
        self.length = BabyStepLength(value: babyStepIntervals[0])
    }
    
    var body: some View {
        VStack(spacing: 0) {
            HStack {
                OctoTabs(
                    scrollable: false,
                    selection: $length,
                    tabs: babyStepIntervals.map { BabyStepLength(value: $0) },
                    label:  { $0.formatted() }
                )
                .padding(OctoTheme.dimens.margin0)
            }
            .frame(maxWidth: .infinity)
            .frame(height: 36)
            .background(Capsule().fill(OctoTheme.colors.inputBackground))
            .padding(OctoTheme.dimens.margin0)
            .onChange(of: babyStepIntervals) { _, new in
                length = BabyStepLength(value: new[0])
            }
           
            HStack {
                Button(
                    action: { changeOffset(length.value * -1)  },
                    label: {
                        Image(systemName: "chevron.down")
                            .padding(OctoTheme.dimens.margin12)
                            .frame(maxWidth: .infinity)
                    }
                )
                
                OctoTheme.colors.textColoredBackground
                    .opacity(0.15)
                    .frame(width: 1)
                
                Button(
                    action: { changeOffset(length.value)  },
                    label: {
                        Image(systemName: "chevron.up")
                            .padding(OctoTheme.dimens.margin12)
                            .frame(maxWidth: .infinity)
                    }
                )
            }
            .foregroundColor(OctoTheme.colors.textColoredBackground)
            
        }
        .surface(color: .special(OctoTheme.colors.accent))
        .fixedSize(horizontal: false, vertical: true)
        .padding([.leading, .trailing], OctoTheme.dimens.margin1)
    }
}

private struct BabyStepLength : Identifiable, Hashable {
    var value: Float
    var id : Float { value }
    
    func formatted() -> String {
        value.formatAsLength(maxDecimals: 4)
            .replacingOccurrences(of: "mm", with: "")
            .trimmingCharacters(in:CharacterSet.whitespaces)
    }
}

private struct BabyStepDivider : View {
    
    var body: some View {
        OctoTheme.colors.accent.opacity(0.15)
            .padding([.leading, .trailing], OctoTheme.dimens.margin1)
            .frame(height: 1)
    }
}

struct TuneControlsInputs_Previews: PreviewProvider {
    static var previews: some View {
        TuneControlsInputsContent(
            state: TuneControlsState(
                flowRate: 100,
                feedRate: 98,
                fanSpeeds: [
                    TuneControlsState.Fan(
                        speed: 90, 
                        label: "Fan",
                        component: "fan",
                        isPartCoolingFan: true
                    )
                ],
                zOffset: 0.0015,
                isValuesImprecise: true
            ),
            babyStepIntervals: [0.005, 0.01, 0.025, 0.05],
            applyValues: { _, _ ,_ in
                try await Task.sleep(for: .seconds(1))
            },
            changeOffset: { _ in },
            resetOffset: { try? await Task.sleep(for: .seconds(1))  },
            saveOffset: { try? await Task.sleep(for: .seconds(1)) }
        )
    }
}
