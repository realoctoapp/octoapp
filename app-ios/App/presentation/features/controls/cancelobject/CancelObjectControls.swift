//
//  CancelObjectControls.swift
//  OctoApp
//
//  Created by Christian on 29/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct CancelObjectControls: View {
    
    @StateObject var viewModel: CancelObjectViewModel
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    
    var body: some View {
        CancelObjectControlsContent(
            state: viewModel.state,
            onRetry:  { viewModel.retry() },
            onRefreshObjects: { await viewModel.refreshObjects() },
            onCancel: {
                do {
                    try await viewModel.cancelObject(objectId: $0)
                } catch {
                    orchestrator.postError(error: KotlinThrowable(message: "Swift error: \(error)"))
                }
            }
        )
    }
}

private struct CancelObjectControlsContent: View {
    
    var state: FlowStateStruct<CancelObjectViewModel.ObjectList>
    var onRetry: () -> Void = {}
    var onRefreshObjects: () async -> Void = {}
    var onCancel: (_ objectId: String) async -> Void

    @State private var showFullscreen: Bool = false
    
    private var isVisible: Bool {
        switch state {
        case .ready(let objects): return !objects.subset.isEmpty
        default: return true
        }
    }
    private var isReady: Bool {
        switch state {
        case .ready(_): return true
        default: return false
        }
    }
    
    var body: some View {
        if isVisible {
            ControlsScaffold(
                title: "widget_cancel_object"~,
                iconSystemName: isReady ? "square.grid.3x3.topright.filled" : nil,
                iconAction: { showFullscreen = true },
                control: { control }
            )
            .macOsCompatibleSheet(
                isPresented: $showFullscreen,
                sheetContent: { fullscreen }
            )
        }
    }
    
    @ViewBuilder
    private func states(fullscreen: Bool) -> some View {
        switch state {
        case .loading: loadingState
        case .error(let error): ScreenError(
            error: error,
            onRetry: onRetry
        )
        .padding(OctoTheme.dimens.margin12)
        .surface()
        case .ready(let objects): if fullscreen {
            CancelObjectFullscreen(
                objects: objects,
                onCancel: onCancel
            )
        } else {
            CancelObjectControlsObjects(
                objects: objects.subset,
                onCancel: onCancel
            )
        }
        }
    }
    
    private var loadingState: some View {
        ProgressView().frame(height: 100)
    }
    
    private var control: some View {
        states(fullscreen: false)
    }
    
    private var fullscreen: some View {
        states(fullscreen: true)
            .asResponsiveSheet()
            .task { await onRefreshObjects() }
    }
}

private struct CancelObjectControlsObjects: View {
    
    var objects: [CancelObjectViewModel.PrintObject]
    var onCancel: (_ objectId: String) async -> Void
    @Namespace private var animation
    
    var body: some View {
        LazyVStack(spacing: OctoTheme.dimens.margin01) {
            ForEach(objects) { object in
                CancelObjectControlsObject(object: object) {
                    await onCancel(object.objectId)
                }
            }
        }
    }
}


@MainActor
private struct CancelObjectControlsObject: View {
    
    var object: CancelObjectViewModel.PrintObject
    var onCancel: () async -> Void
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin0) {
            content
            
            if !object.cancelled {
                button.zIndex(-100)
            }
        }
        .background(Capsule().fill(OctoTheme.colors.inputBackground.opacity(0.5)))
        .clipShape(Capsule())
        .animation(.spring(), value: object)
    }

    var content : some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            icon
            label
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .background(Capsule().fill(OctoTheme.colors.inputBackground))
    }
    
    var button : some View {
        CancelObjectControlsButton(
            object: object,
            onCancel: onCancel
        )
        .id(object.id)
    }
    
    var icon : some View {
        Image(systemName: object.active ? "arrow.forward" : "")
            .frame(width: 24, height: 24)
            .padding(.leading, OctoTheme.dimens.margin12)
    }
    
    var label: some View {
        Text(object.label)
            .lineLimit(1)
            .typographyButton(small: true)
            .padding([.top, .bottom], OctoTheme.dimens.margin1)
            .strikethrough(object.cancelled)
            .opacity(object.cancelled ? 0.8 : 1)
    }
}

@MainActor
private struct CancelObjectControlsButton: View {
    
    var object: CancelObjectViewModel.PrintObject
    var onCancel: () async -> Void
    
    @State private var showConfirmation: Bool = false
    @State private var loading: Bool = false
    
    var body: some View {
        OctoAsyncIconButton(
            icon: "trash.fill",
            loading: loading,
            clickListener: {
                showConfirmation = true
            }
        )
        // I don't know why...but we need this to ensure the confirmation is consistently shown?
        .background(showConfirmation ? .clear.opacity(0.001) : .clear)
        .frame(width: 36, height: 36)
        .padding(.trailing, OctoTheme.dimens.margin1)
        .cancelObjectConfirmation(object: object, shown: $showConfirmation) {
            loading = true
            Task {
                await onCancel()
                loading = false
            }
        }
    }
}



struct CancelObjectControls_Previews: PreviewProvider {
    static var previews: some View {
        CancelObjectControlsContent(state: .loading) { _ in }
            .previewDisplayName("Loading")
        
        CancelObjectControlsContent(state: .error(KotlinThrowable(message: "Some"))) { _ in }
            .previewDisplayName("Error")
        
        
        CancelObjectControlsContent(
            state: .ready(
                CancelObjectViewModel.ObjectList(
                    all: [],
                    subset: [],
                    renderParams: CancelObjectViewModel.RenderParams(
                        printBed: .printbedanycubic,
                        printBedWidthMm: 0,
                        printBedHeightMm: 0,
                        originInCenter: false
                    ),
                    renderPlugin: CancelObjectGcodeRendererPlugin(objects: [])
                )
            )
        ) { _ in }
        .previewDisplayName("Empty")
        
        CancelObjectControlsContent(
            state: .ready(
                CancelObjectViewModel.ObjectList(
                    all: [],
                    subset: [
                        CancelObjectViewModel.PrintObject(
                            objectId: "1",
                            cancelled: false,
                            active: false,
                            label: "Object A",
                            centerX: 20,
                            centerY: 20
                        ),
                        CancelObjectViewModel.PrintObject(
                            objectId: "2",
                            cancelled: false,
                            active: true,
                            label: "Object B with super long name should not break",
                            centerX: 60,
                            centerY: 60
                        ),
                        CancelObjectViewModel.PrintObject(
                            objectId: "3",
                            cancelled: true,
                            active: false,
                            label: "Object C",
                            centerX: 90,
                            centerY: 90
                        ),
                        CancelObjectViewModel.PrintObject(
                            objectId: "4",
                            cancelled: false,
                            active: false,
                            label: "Object D",
                            centerX: 120,
                            centerY: 120
                        ),
                        CancelObjectViewModel.PrintObject(
                            objectId: "5",
                            cancelled: false,
                            active: false,
                            label: "Object E",
                            centerX: 170,
                            centerY: 170
                        )
                    ],
                    renderParams: CancelObjectViewModel.RenderParams(
                        printBed: .printbedanycubic,
                        printBedWidthMm: 200,
                        printBedHeightMm: 200,
                        originInCenter: false
                    ),
                    renderPlugin: CancelObjectGcodeRendererPlugin(objects: [])
                )
            )
        ) { _ in
            try? await Task.sleep(for: .seconds(2))
        }
        .previewDisplayName("Data")
    }
}
