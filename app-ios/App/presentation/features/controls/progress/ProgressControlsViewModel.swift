//
//  ProgressControlsViewModel.swift
//  OctoApp
//
//  Created by Christian on 12/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import Combine
import OctoAppBase
import SwiftUI

class ProgressControlsViewModel : BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: ProgressControlsViewModelCore? = nil
    
    @Published var state: State? = nil

    func createCore(instanceId: String) -> ProgressControlsViewModelCore {
        return ProgressControlsViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: ProgressControlsViewModelCore) {
        core.state.asPublisher()
            .sink { (state: ProgressControlsViewModelCore.State) in
                self.state = State(state: state)
            }
            .store(in: &bag)
    }
    
    func clearData() {
        state = nil
    }
}

extension ProgressControlsViewModel {
    struct State : Equatable {
        let thumbanilUrl: String?
        let settings: ProgressWidgetSettingsStruct
        let fullscreenSettings: ProgressWidgetSettingsStruct
        let fullscreenDataItems: [DataItem]
        let dataItems: [DataItem]
        let completion: Float
        let status: String
        let printing: Bool
    }
    
    struct DataItem: Equatable, Identifiable {
        var id: String { label }
        var label: String
        var value: String
        var largeValue: Bool = false
        var valueLayoutClue: String? = nil
        var maxLines: Int = 1
        var iconSystemName: String? = nil
        var iconColor: Color? = nil
    }
}


extension ProgressControlsViewModel.State {
    init(state: ProgressControlsViewModelCore.State) {
        var thumbnail: String? = nil
        
        if state.activeFile is FlowStateLoading {
            thumbnail = NetworkImageLoadingMarker
        }
        
        if let f = (state.activeFile as? FlowStateReady<FileObject.File>)?.data {
            thumbnail = f.largeThumbnail?.path
        }

        
        self.init(
            thumbanilUrl: thumbnail,
            settings: state.settings.toStruct(),
            fullscreenSettings: state.fullscreenSettings.toStruct(),
            fullscreenDataItems: state.fullscreenDataItems.map { ProgressControlsViewModel.DataItem(item: $0) },
            dataItems: state.dataItems.map { ProgressControlsViewModel.DataItem(item: $0) },
            completion: state.completion,
            status: state.status,
            printing: state.printing
        )
    }
}
extension ProgressControlsViewModel.DataItem {
    init(item: ProgressControlsViewModelCore.StateDataItem)  {
        var iconSystemName: String? = nil
        var iconColor: Color? = nil
        
        switch item.icon {
        case ProgressControlsViewModelCore.StateIcon.star: iconSystemName = "star.fill"
        case ProgressControlsViewModelCore.StateIcon.circle: iconSystemName = "circle.fill"
        default: iconSystemName = nil
        }
        
        switch item.iconColor {
        case ProgressControlsViewModelCore.StateColor.green: iconColor = OctoTheme.colors.green
        case ProgressControlsViewModelCore.StateColor.orange: iconColor = OctoTheme.colors.orange
        case ProgressControlsViewModelCore.StateColor.yellow: iconColor = OctoTheme.colors.yellow
        case ProgressControlsViewModelCore.StateColor.green: iconColor = OctoTheme.colors.green
        default: iconColor = nil
        }
        
        self.init(
            label: item.label,
            value: item.value,
            largeValue: item.largeValue,
            valueLayoutClue: item.valueLayoutClue,
            maxLines: Int(item.maxLines),
            iconSystemName: iconSystemName,
            iconColor: iconColor
        )
    }
}
