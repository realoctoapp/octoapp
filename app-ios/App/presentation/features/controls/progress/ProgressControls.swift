//
//  ProgressControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct ProgressControls: View {
    
    @StateObject var viewModel: ProgressControlsViewModel
    @StateObject var gcodeViewModel: GcodePreviewControlsViewModel
    @State private var showSettings = false
    
    var body: some View {
        ControlsScaffold(
            title: "progress_widget___title"~,
            iconSystemName: "slider.horizontal.3",
            iconAction: { showSettings = true}
        ) {
            VStack(spacing: OctoTheme.dimens.margin12) {
                ProgressControlsBar(
                    progress: Double(viewModel.state?.completion ?? 0.0),
                    status: viewModel.state?.status ?? ""
                )
                
                HStack(alignment: .top, spacing: 0) {
                    ProgressControlsThumbnail(
                        settings: viewModel.state?.settings,
                        thumbnailUrl: viewModel.state?.thumbanilUrl
                    )
                    
                    ProgressControlsInformation(
                        items: viewModel.state?.dataItems ?? [],
                        settings: viewModel.state?.settings,
                        role: .fornormal
                    )
                    .frame(maxWidth: .infinity)
                }
            }
        }
        .animation(.default, value: viewModel.state)
        .macOsCompatibleSheet(isPresented: $showSettings) {
            MenuHost(menu: ProgressControlsSettingsMenu(role: .fornormal))
        }
    }
}

struct ProgressControls_Previews: PreviewProvider {
    static var previews: some View {
        ProgressControls(
            viewModel: ProgressControlsViewModel(),
            gcodeViewModel: GcodePreviewControlsViewModel()
        )
    }
}
