//
//  ProgressControlsBar.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI



struct ProgressControlsBar: View {
    var progress: Double
    var status: String
    private let trailingMin = 0.7

    private var alignment: Alignment {
        return progress >= trailingMin ? .trailing : .leading
    }
    
    @Environment(\.instanceColor) private var progressColor
    
    var body: some View {
        ControlsProgressBarLayout(progress: progress, trailingMin: trailingMin) {
            progressColor.main.clipShape(Capsule())
            Text(status)
                .foregroundColor(alignment == .leading ? OctoTheme.colors.darkText : OctoTheme.colors.textColoredBackground)
                .typographySubtitle()
                .padding([.leading, .trailing], OctoTheme.dimens.margin1)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: alignment)
            
            
        }
        .background(OctoTheme.colors.inputBackground)
        .frame(maxWidth: .infinity)
        .frame(height: 45)
        .clipShape(Capsule())
        .animation(.spring(dampingFraction: 1), value: progress)
        .animation(.spring(dampingFraction: 1), value: status)
    }
    
}

struct ProgressControlsBar_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(1) { progress in
            ProgressControlsBar(
                progress: progress.wrappedValue / 100,
                status: progress.wrappedValue.formatted() + "%"
            )
                .task {
                    while progress.wrappedValue < 100 {
                        try? await Task.sleep(for: .milliseconds(1000))
                        progress.wrappedValue = Double(progress.wrappedValue) + Double(Int.random(in: 1..<10))
                    }
                    progress.wrappedValue = 0
                }
            
        }
        .previewDisplayName("Animated")
        
        ProgressControlsBar(
            progress: 0,
            status: "0%"
        )
        .previewDisplayName("0%")
        
        ProgressControlsBar(
            progress: 99,
            status: "99%"
        )
        .previewDisplayName("99%")
    }
}
