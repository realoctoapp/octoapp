//
//  ProgressControlsInformation.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Flow
import OctoAppBase

struct ProgressControlsInformation: View {
    var items: [ProgressControlsViewModel.DataItem]
    var settings: ProgressWidgetSettingsStruct?
    var role: ProgressControlsRole
    
    var body: some View {
        let fontSize = settings?.fontSize ?? .normal
        let spacing = fontSize == .small ? OctoTheme.dimens.margin0 : OctoTheme.dimens.margin01
        let forcedWhite = role == .forwebcamfullscreen
        
        VStack(spacing: spacing) {
            HStack(spacing: 0) {
                HFlow(
                    alignment: .top,
                    itemSpacing: spacing,
                    rowSpacing: spacing
                ) {
                    ForEach(items.filter { !$0.largeValue }) { item in
                        InfoItem(
                            title: item.label,
                            text: item.value,
                            textLayoutClue: item.valueLayoutClue,
                            fontSize: fontSize,
                            largeField: item.largeValue,
                            maxLines: item.maxLines,
                            forcedWhite: forcedWhite
                        )
                    }
                }
                               
                Spacer()
            }
            
            ForEach(items.filter { $0.largeValue }) { item in
                InfoItem(
                    title: item.label,
                    text: item.value,
                    textLayoutClue: item.valueLayoutClue,
                    fontSize: fontSize,
                    largeField: item.largeValue,
                    maxLines: item.maxLines,
                    forcedWhite: forcedWhite
                )
                .frame(maxWidth: .infinity)
            }
        }
        .animation(.default, value: items)
    }
    
    private struct InfoItem : View {
        var title: String
        var text: String
        var textLayoutClue: String?
        var systemIconName: String? = nil
        var iconColor: Color = .clear
        var fontSize: ProgressWidgetSettings.FontSize
        var largeField: Bool = false
        var maxLines = 1
        var forcedWhite = false
        
        var body: some View {
            VStack(alignment: .leading) {
                HStack(spacing: OctoTheme.dimens.margin0) {
                    Text(title)
                        .foregroundColor(forcedWhite ? OctoTheme.colors.white.opacity(0.85) : OctoTheme.colors.lightText)
                    
                    if let i = systemIconName {
                        Image(systemName: i)
                            .foregroundColor(iconColor)
                            .scaleEffect(x: 0.5, y: 0.5)
                    }
                }
                .if(fontSize == .small) { $0.typographyLabelSmall() }
                .if(fontSize == .normal) { $0.typographyLabel() }
                .typographyBase()
                
                ZStack(alignment: .leading) {
                    text(text: text)
                    
                    if let clue = textLayoutClue {
                        text(text: clue)
                            .opacity(0)
                    }
                }
            }
            .if (largeField) { $0.frame(maxWidth: .infinity, alignment: .leading)}
            .padding([.trailing, .leading], OctoTheme.dimens.margin01)
            .padding([.top, .bottom], OctoTheme.dimens.margin0)
            .background(RoundedRectangle(cornerRadius: OctoTheme.dimens.margin01).fill(OctoTheme.colors.inputBackground.opacity(0.45)))
        }
        
        func text(text: String) -> some View {
            Text(text)
                .if(forcedWhite) { $0.foregroundColor(OctoTheme.colors.textColoredBackground)}
                .if(fontSize == .small) { $0.typographyLabel() }
                .if(fontSize == .normal) { $0.typographyBase() }
                .typographyData()
                .lineLimit(maxLines)
                .multilineTextAlignment(.leading)
                .animation(nil, value: text)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
}


struct ProgressControlsInformation_Previews: PreviewProvider {
    static var previews: some View {
        let items = [
            ProgressControlsViewModel.DataItem(
                label: "Spent",
                value: "3h 45min",
                largeValue: false,
                valueLayoutClue: "",
                maxLines: 1,
                iconSystemName: nil,
                iconColor: nil
            ),
            ProgressControlsViewModel.DataItem(
                label: "ETA",
                value: "12:45",
                largeValue: false,
                valueLayoutClue: "",
                maxLines: 1,
                iconSystemName: "circle.fill",
                iconColor: .green
            ),
            ProgressControlsViewModel.DataItem(
                label: "Speed",
                value: "5 mm/s",
                valueLayoutClue: "000 mm/s"
            ),
            ProgressControlsViewModel.DataItem(
                label: "Print name",
                value: "Some name that is quite long dsfh hdsf iuhsuidhf iuhdsf iuhsdiuf hdiush fisuhif sdhiuhs.gcode",
                largeValue: true,
                maxLines: 3
            )
        ]
        
        ProgressControlsInformation(
            items: items,
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .full,
                printNameStyle: .compact,
                fontSize: .normal
            ),
            role: .fornormal
        )
        .previewDisplayName("Normal")

        ProgressControlsInformation(
            items: items,
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .full,
                printNameStyle: .compact,
                fontSize: .small
            ),
            role: .fornormal
        )
        .previewDisplayName("Small font")

        ProgressControlsInformation(
            items: items,
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .compact,
                printNameStyle: .compact,
                fontSize: .normal
            ),
            role: .forwebcamfullscreen
        )
        .previewDisplayName("Webcam")
        .background(.black)
    }
}
