//
//  PluginLibrary.swift
//  OctoApp
//
//  Created by Christian on 10/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct PluginsLibrary: View {
    
    var category: String?
    @StateObject private var viewModel = PluginsLibraryViewModel()
    
    var body: some View {
        PluginsLibraryContent(
            category: category,
            index: viewModel.index
        )
        .usingViewModel(viewModel)
    }
}

private let defaultCategory = PluginsLibraryViewModel.PluginCategory(name: "", plugins: [])
private struct PluginsLibraryContent: View {
    
    var category: String? = ""
    var index: PluginsLibraryViewModel.PluginIndex
    @State private var activeCategory = defaultCategory
    
    var body: some View {
        pager
            .navigationTitle("plugin_library___title"~)
            .navigationBarTitleDisplayMode(.inline)
            .background(OctoTheme.colors.windowBackground)
            .onChange(of: index.categories) { _, new in flushCategory(category: category, index: new) }
            .onChange(of: category) { _, new in flushCategory(category: new, index: index.categories) }
    }
    
    private var pager: some View {
        OctoPagerWithTabs(
            selection: $activeCategory,
            tabs: index.categories,
            label: { $0.name },
            pages: { PluginCategory(category: $0) }
        )
    }
    
    private func flushCategory(category: String?, index: [PluginsLibraryViewModel.PluginCategory]) {
        Napier.i(tag: "PluginLibrary", message: "Switching to category \(String(describing: category))")
        activeCategory = index.first { $0.id == category } ?? index.first ?? activeCategory
    }
}

private struct PluginCategory : View {
    
    var category: PluginsLibraryViewModel.PluginCategory
    
    var body: some View {
        ZStack {
            scrollView
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .background(OctoTheme.colors.inputBackground)
    }
    
    private var scrollView: some View {
        ScrollView {
            VStack {
                items
            }
            .padding([.top, .bottom], OctoTheme.dimens.margin12)
        }
    }
    
    private var items: some View {
        ForEach(category.plugins) { plugin in
            Plugin(plugin: plugin)
        }
    }
}

private struct Plugin : View {
    
    var plugin: PluginsLibraryViewModel.Plugin
    @Environment(\.openURL) private var openUrl
    private var isOctoApp: Bool { plugin.id == OctoPlugins.shared.OctoApp && plugin.installed != true }
    private var highlightColor: Color { isOctoApp ? OctoTheme.colors.red : OctoTheme.colors.yellow }
    private var buttonType: OctoButtonType { isOctoApp ? .linkColoredBackground : .link }
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin12) {
            title
            description
            actions
        }
        
        .padding(OctoTheme.dimens.margin12)
        .surface(color: isOctoApp ? .special(OctoTheme.colors.redColorSchemeLight) : .alternative)
    }
    
    var title: some View {
        HStack(spacing: 0) {
            Text(plugin.name)
                .lineLimit(2)
                .typographySubtitle()
            
            Spacer(minLength: OctoTheme.dimens.margin2)
            
            HStack(spacing: OctoTheme.dimens.margin01) {
                if plugin.installed == true {
                    installed
                } else if plugin.highlight == true {
                    recommended
                }
            }
        }
    }
    
    var description: some View {
        Text(LocalizedStringKey(plugin.description))
            .frame(maxWidth: .infinity, alignment: .leading)
            .multilineTextAlignment(.leading)
            .typographyBase()
            .lineLimit(8)
    }
    
    var actions : some View {
        HStack(spacing: -OctoTheme.dimens.margin12) {
            Spacer()
            
            if let configUrl = plugin.configureLink {
                OctoButton(text: "plugin_library___configure"~, type: buttonType, small: true) {
                    openUrl(configUrl)
                }
            }
            
            if let tutorialUrl = plugin.octoAppTutorial {
                OctoButton(text: "plugin_library___octoapp_tutorial"~, type: buttonType, small: true) {
                    openUrl(tutorialUrl)
                }
            }
            
            if let pluginUrl = plugin.pluginPage {
                OctoButton(text: "plugin_library___plugin_page"~, type: buttonType, small: true) {
                    openUrl(pluginUrl)
                }
            }
        }
        .padding(.trailing, -OctoTheme.dimens.margin12)
    }
    
    @ViewBuilder
    var installed: some View {
        if plugin.highlight {
            TextTag(
                icon: "star.fill",
                text: "",
                color: highlightColor
            )
        }
        
        TextTag(
            icon: "checkmark.circle.fill",
            text: "plugin_library___installed"~,
            color: OctoTheme.colors.green
        )
    }
    
    var recommended: some View {
        TextTag(
            icon: "star.fill",
            text: "plugin_library___recommended"~,
            color: highlightColor
        )
    }
}

struct PluginLibrary_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack{
            PluginsLibraryContent(
                index: PluginsLibraryViewModel.PluginIndex(
                    index: PluginsLibraryViewModelCore.Companion.shared.baseIndex
                )
            )
        }
    }
}
