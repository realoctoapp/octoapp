//
//  File.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct NgrokSupport : View {
    @StateObject private var viewModel = NgrokSupportViewModel()
    
    var body: some View {
        Color.clear
            .frame(height: 0)
            .usingViewModel(viewModel)
    }
}

private class NgrokSupportViewModel : BaseViewModel {
    fileprivate var currentCore: NgrokSupportViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    
    func createCore(instanceId: String) -> NgrokSupportViewModelCore {
        return NgrokSupportViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: NgrokSupportViewModelCore) {
        Napier.i(tag: "NgrokSupportViewModel", message: "Ngrok support started")
        
        core.events.asPublisher()
            .sink { (_ : KotlinUnit) in }
            .store(in: &bag)
    }
    
    func clearData() {}
}
