//
//  Mmu2FilamentSelect.swift
//  OctoApp
//
//  Created by Christian on 11/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct Mmu2FilamentSelectSupport : View {
    
    @StateObject private var viewModel = Mmu2FilamentSelectViewModel()
    @State private var labelSource: Mmu2FilamentSelectViewModel.LabelSource? = nil
    @Environment(\.instanceId) private var instanceId
    
    var body: some View {
        Color.clear
            .frame(height: 0)
            .usingViewModel(viewModel)
            .onChange(of: viewModel.labelSource) { _, ls in labelSource = ls }
            .macOsCompatibleSheet(item: $labelSource) { ls in
                MenuHost(menu: Mmu2FilamentSelectMenu(source: ls.toClass(), instanceId: instanceId))
            }
    }
}

private class Mmu2FilamentSelectViewModel : BaseViewModel {
    fileprivate var currentCore: Mmu2FilamentSelectViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var labelSource: Mmu2FilamentSelectViewModel.LabelSource? = nil
    
    
    func createCore(instanceId: String) -> Mmu2FilamentSelectViewModelCore {
        return Mmu2FilamentSelectViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: Mmu2FilamentSelectViewModelCore) {
        Napier.i(tag: "MmuFilamentSelectViewModel", message: "MMU2 support started")

        core.events.asPublisher()
            .sink { (event : Mmu2FilamentSelectViewModelCore.Event) in
                self.labelSource = LabelSource((event as? Mmu2FilamentSelectViewModelCore.EventShowMenu)?.labelSource)
            }
            .store(in: &bag)
    }
    
    func clearData() {
        labelSource = nil
    }
}

private extension Mmu2FilamentSelectViewModel {

    enum LabelSource: Equatable, Identifiable {
        case manual, filamentManager, spoolManager
        
        var id: String {
            switch self {
            case .manual: return "manual"
            case .filamentManager: return "filamentManager"
            case .spoolManager: return "spoolManager"
            }
        }
        
        init?(_ source: Settings.MmuLabelSource?) {
            guard let s = source else { return nil }
            
            switch s {
            case .manual: self = .manual
            case .filamentmanager: self = .filamentManager
            case .spoolmanager: self = .spoolManager
            default: return nil
            }
        }
        
        func toClass() -> Settings.MmuLabelSource {
            switch self {
            case .filamentManager: return .filamentmanager
            case .spoolManager: return .spoolmanager
            case .manual: return .manual
            }
        }
    }
}
