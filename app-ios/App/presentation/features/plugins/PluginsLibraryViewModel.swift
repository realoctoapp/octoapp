//
//  PluginLibraryViewModel.swift
//  OctoApp
//
//  Created by Christian on 28/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

class PluginsLibraryViewModel: BaseViewModel {
    var currentCore: PluginsLibraryViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var index: PluginIndex = PluginIndex()
    
    func createCore(instanceId: String) -> PluginsLibraryViewModelCore {
        return PluginsLibraryViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: PluginsLibraryViewModelCore) {
        core.index.asPublisher()
            .sink { (index: PluginsLibraryViewModelCore.PluginsIndex) in
                self.index = PluginIndex(index: index)
            }
            .store(in: &bag)
    }
    
    func clearData() {
        index = PluginIndex()
    }
}

extension PluginsLibraryViewModel {
    struct PluginIndex {
        var categories: [PluginCategory] = []
    }
    
    struct PluginCategory : Identifiable, Hashable, Equatable {
        var id: String?
        var name: String
        var plugins: [Plugin]
    }
    
    struct Plugin : Identifiable, Hashable, Equatable {
        var id: String { key }
        var key: String
        var name: String
        var description: String
        var highlight: Bool = false
        var pluginPage: String? = nil
        var octoAppTutorial: String? = nil
        var installed: Bool? = nil
        var order: Int = 0
        var configureLink: String? = nil
    }
}

extension PluginsLibraryViewModel.PluginIndex {

    init(index: PluginsLibraryViewModelCore.PluginsIndex) {
        self.init(
            categories: index.categories.map { category in
                PluginsLibraryViewModel.PluginCategory(
                    id: category.id,
                    name: category.name,
                    plugins: category.plugins.filter { plugin in
                        plugin.supported == true
                    }.map { plugin in
                        PluginsLibraryViewModel.Plugin(
                            key: plugin.key,
                            name: plugin.name,
                            description: plugin.description_,
                            highlight: plugin.highlight,
                            pluginPage: plugin.pluginPage?.description,
                            octoAppTutorial: plugin.octoAppTutorial?.description,
                            installed: plugin.installed?.boolValue,
                            order: Int(plugin.order),
                            configureLink: plugin.configureLink?.description
                        )
                    }
                )
            }
        )
    }
}
