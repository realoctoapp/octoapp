//
//  WebcamFullscreen.swift
//  OctoApp
//
//  Created by Christian on 11/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct WebcamFullscreen: View {

    @State private var nativeImageSize: CGSize = CGSizeMake(1, 1)
    @State private var originalIdleTimerDisabled: Bool = false
    @Environment(\.dismiss) var dismiss
    @StateObject private var progressViewModel = ProgressControlsViewModel()
    @StateObject private var gcodeViewModel = GcodePreviewControlsViewModel()

    private var nativeAspectRatio: CGFloat? {
        nativeImageSize != CGSize.zero ? (nativeImageSize.width / nativeImageSize.height) : nil
    }
    
    var body: some View {
        ZStack {
            WebcamDisplay(canZoom: true, nativeImageSize: $nativeImageSize) { inputs in
                overlay(inputs: inputs)
                    .padding(.top, getSafeAreaTop())
            }
            .ignoresSafeArea(.all)
        }
        .background(.black)
        .preferredColorScheme(.dark)
        .webcamFullscreenDragToDismiss { dismiss() }
        .unlockedRotation()
        .usingViewModel(progressViewModel)
        .usingViewModel(gcodeViewModel)
        .onAppear {
            originalIdleTimerDisabled = UIApplication.shared.isIdleTimerDisabled
            UIApplication.shared.isIdleTimerDisabled = true
        }
        .onDisappear {
            UIApplication.shared.isIdleTimerDisabled = originalIdleTimerDisabled
        }
    }
    
    func overlay(inputs: WebcamOverlayInputs) -> some View {
        VStack {
            // Padding for rounded screen corners
            topBar(inputs: inputs)
                .safeAreaPadding([.leading, .trailing])
            Spacer()
            bottomBar(inputs: inputs)
        }
    }
    
    @ViewBuilder
    func topBar(inputs: WebcamOverlayInputs) -> some View {
        WebcamTopBar(
            nativeResolution: nativeImageSize,
            liveState: inputs.liveBadgeState,
            displayName: inputs.displayName
        )
    }
    
    @ViewBuilder
    func bottomBar(inputs: WebcamOverlayInputs) -> some View {
        let printing = progressViewModel.state?.printing == true
        WebcamBottomBar(
            isFullscreen: true,
            bottomPadding: getSafeAreaBottom(),
            requestSnapshot: inputs.snapshot,
            pictureInPicture: inputs.pictureInPicture
        ) {
            if printing && !inputs.zoomedIn {
                ProgressControlsInformation(
                    items: progressViewModel.state?.fullscreenDataItems ?? [],
                    settings: progressViewModel.state?.fullscreenSettings,
                    role: .forwebcamfullscreen
                )
                .transition(.move(edge: .bottom).combined(with: .opacity))
            }
        }
        .animation(.spring(), value: printing && inputs.zoomedIn)
    }
}

struct WebcamFullscreen_Previews: PreviewProvider {
    static var previews: some View {
        WebcamFullscreen()
    }
}
