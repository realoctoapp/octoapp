//
//  SignInViewDiscover.swift
//  App
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct SignInViewDiscover: View {
    
    var state: SignInViewModelCore.StateDiscover
    var onContinueWithUrl: (String, BackendType?, Bool) -> Void
    var onContinuePrevious: (PrinterConfigurationV3) -> Void
    var onDeletePrevious: (String) -> Void
    
    var body: some View {
        VStack(spacing: 0) {
            SignInViewDiscoverNetwork(services: state.services) { url, type in
                onContinueWithUrl(url, type, true)
            }
            
            SignInViewDiscoverReuse(
                configs: state.configs,
                hasQuickSwitch: state.hasQuickSwitch,
                onContinueWithConfig: onContinuePrevious,
                onContinueWithUrl: { onContinueWithUrl($0, nil, false) },
                onDeletePrevious: onDeletePrevious
            )
            
            SignInViewDiscoverOthers(optionsCount: state.services.count) {url in
                onContinueWithUrl(url, nil, false)
            }
        }
    }
}

private struct SignInViewDiscoverNetwork: View {
    
    var services: [NetworkService]
    var onContinueWithUrl: (String, BackendType) -> Void
    
    var body: some View {
        SignInOptionSection(text: "sign_in___discovery___discovered_devices") {
            ProgressView()
        }
        
        if (services.isEmpty) {
            ComponentSignInOption(
                title: "xx",
                subtitle: "xx",
                type: .placeholder,
                clickListener: {}
            )
            .redacted(reason: .placeholder)
            
        } else {
            VStack {
                ForEach(services, id: \.id) { service in
                    ComponentSignInOption(
                        title: LocalizedStringKey(service.label),
                        subtitle: LocalizedStringKey(service.detailLabel),
                        type: .discovered
                    ) {
                        switch service.origin {
                        case .dnssd: OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventDnsServiceSelected(), params: [:])
                        case .upnp: OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventUpnpServiceSelected(), params: [:])
                        default: break
                        }
                        
                        onContinueWithUrl(service.webUrl, service.type)
                    }
                }
            }
        }
    }
}

private struct SignInViewDiscoverReuse: View {
    
    var configs: [PrinterConfigurationV3]
    var hasQuickSwitch: Bool
    var onContinueWithConfig: (PrinterConfigurationV3) -> Void
    var onContinueWithUrl: (String) -> Void
    var onDeletePrevious: (String) -> Void
    
    @State private var showingSupporter = false
    @State private var showConnect = false
    @State private var connectUrl: String? = nil
    @State private var showDelete = false
    
    var body: some View {
        if !configs.isEmpty {
            SignInOptionSection(text: "sign_in___discovery___previously_connected_devices") {
                OctoIconButton(icon: showDelete ? "xmark" : "trash", color: OctoTheme.colors.normalText, iconScale: 0.9) {
                    showDelete.toggle()
                }
            }
            
            VStack {
                if !hasQuickSwitch {
                    ComponentSignInOption(
                        title: LocalizedStringKey("sign_in___discovery___quick_switch_disabled_title"~),
                        subtitle: LocalizedStringKey("sign_in___discovery___quick_switch_disabled_subtitle"~),
                        type: .support
                    ) {
                        showingSupporter = true
                    }
                }
                
                ForEach(configs, id: \.id) { config in
                    HStack {
                        ComponentSignInOption(
                            title: LocalizedStringKey(config.label),
                            subtitle: "\(config.webUrl.host):\(config.webUrl.port.description)",
                            type: .reuse
                        ) {
                            if hasQuickSwitch {
                                onContinueWithConfig(config)
                            } else {
                                // Show manual input with prefilled url
                                connectUrl = config.webUrl.description()
                            }
                        }
                        .opacity(hasQuickSwitch ? 1 : 0.4)
                        
                        if(showDelete) {
                            OctoIconButton(icon: "trash") {
                                onDeletePrevious(config.id)
                                showDelete = false
                            }
                            .foregroundColor(OctoTheme.colors.destructive)
                        }
                    }
                }
            }
            .macOsCompatibleSheet(isPresented: $showingSupporter) {
                BecomeSupporterView()
            }
            .animation(.spring(), value: showDelete)
            .macOsCompatibleSheet(item: $connectUrl) { url in
                SignInViewManual(input: url, onContinue: onContinueWithUrl)
            }
        }
    }
}


private struct SignInViewDiscoverOthers: View {
    
    @State private var showingManual = false
    var optionsCount: Int
    var onContinueWithUrl: (String) -> Void
    
    var body: some View {
        SignInOptionSection(text: "sign_in___discovery___other_options") {}
        VStack {
            signInOption(octoPrint: true)
            signInOption(octoPrint: false)
        }
    }
    
    private func signInOption(octoPrint: Bool) -> some View {
        ComponentSignInOption(
            title: LocalizedStringKey(String(format: "sign_in___discovery___connect_x_manually_title"~, octoPrint ? "OctoPrint" : "Klipper")),
            subtitle: octoPrint ? "sign_in___discovery___connect_manually_subtitle_octoprint" : "sign_in___discovery___connect_manually_subtitle_klipper",
            type: .manual
        ) {
            OctoAnalytics.shared.logEvent(event: OctoAnalytics.EventManualUrlSelected(), params: ["options_count" : optionsCount])
            showingManual.toggle()
        }
        .macOsCompatibleSheet(isPresented: $showingManual) {
            SignInViewManual(){ webUrl in
                onContinueWithUrl(webUrl)
            }
        }
    }
}


struct SignInViewDiscover_Previews: PreviewProvider {
    static var previews: some View {
        SignInViewDiscover(
            state: SignInViewModelCore.StateDiscover(services: [], configs: [], hasQuickSwitch: true),
            onContinueWithUrl: { _, _, _ in },
            onContinuePrevious: { _ in },
            onDeletePrevious: { _ in }
        )
    }
}
