//
//  HelpContactViewModel.swift
//  OctoApp
//
//  Created by Christian on 08/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import OctoAppBase
import SwiftUIMailView

class HelpContactViewModel : ObservableObject {
    
    private let tag = "HelpContactViewModel"
    private var core = HelpContactViewModelCore()
    private let platform = SharedCommonInjector.shared.get().platform
    private let fileManager = Foundation.FileManager.default

    deinit {
        Napier.e(tag: tag, message: "Cleaning up files")
        try? fileManager.removeItem(at: directory)
    }
    
    var contactTimeZone: String { core.contactTimeZone }
    
    var contactTime: String {
        guard let tz = (TimeZone(abbreviation: contactTimeZone) ?? TimeZone(identifier: contactTimeZone)) else {
            return "???"
        }
        
        let df = DateFormatter()
        df.timeZone = tz
        df.dateStyle = .none
        df.timeStyle = .short
        
        return df.string(from: Date())
    }
    
    private var directory : URL {
        fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!.appendingPathComponent("data")
    }
    
    func checkForUpdate() async -> Bool {
        do {
            let currentVersion = platform.appVersion
            let beta = platform.betaBuild
            let info = try await getAppInfo()
            if beta {
                return false
            } else if info.version <= currentVersion {
                Napier.e(tag: tag, message: "Already on latest version: \(currentVersion) -> \(info.version)")
                return false
            } else {
                Napier.e(tag: tag, message: "Update available: \(currentVersion) -> \(info.version)")
                return true
            }
        } catch {
            Napier.e(tag: tag, message: "Failed to get app store version: \(error)")
            return false
        }
    }
    
    private func getAppInfo() async throws -> AppInfo {
        let bundleId = platform.appId
        guard let url = URL(string: "http://itunes.apple.com/us/lookup?bundleId=\(bundleId)") else { throw VersionError.invalidBundleInfo }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        let result = try JSONDecoder().decode(LookupResult.self, from: data)
        guard let info = result.results.first else {
            throw VersionError.invalidResponse
        }
        
        return info
    }
    
    func prepareFeedbackMail(
        sendPhoneInfo: Bool,
        sendOctoPrintInfo: Bool,
        sendLogs: Bool,
        message: String
    ) async -> ComposeMailData {
        guard let mail = try? await core.prepareFeedbackMail(
            sendLogs: sendLogs,
            sendOctoPrintInfo: sendOctoPrintInfo,
            sendPhoneInfo: sendPhoneInfo
        ) else {
            return ComposeMailData(
                subject: HelpContactViewModel.fallbackSubject,
                recipients: [HelpContactViewModel.fallbackEmail],
                message: message,
                attachments: nil
            )
        }
        
        var attachments: [AttachmentData] = []
        if let a = mail.attachments {
            do {
                attachments.append(try createAttachment(attachments: a))
            } catch {
                Napier.e(tag: tag, message: "Failed to create ZIP file: \(error)")
            }
        }
        
        return ComposeMailData(
            subject: mail.subject,
            recipients: [mail.receipientAddress],
            message: message,
            attachments: attachments
        )
    }
    
    private func createAttachment(attachments: [HelpContactViewModelCore.Attachment]) throws -> AttachmentData {
        let zipName = "data.zip"
        let zipFile = directory.appendingPathComponent(zipName)
        
        try? fileManager.removeItem(at: directory)
        try? fileManager.removeItem(at: zipFile)
        try fileManager.createDirectory(at: directory, withIntermediateDirectories: true, attributes: nil)
        
        try attachments.forEach { a in
            let path = directory.appendingPathComponent(a.name)
            Napier.i(tag: tag, message: "Writing \(a.name) to \(path)")
            try a.bytes.toNsData().write(to: path)
        }
        
        let coord = NSFileCoordinator()
        var readError: NSError?
        var copyError: NSError?
        coord.coordinate(
            readingItemAt: directory,
            options: NSFileCoordinator.ReadingOptions.forUploading,
            error: &readError
        ) { (zippedURL: URL) -> Void in
            do {
                try fileManager.copyItem(at: zippedURL, to: zipFile)
            } catch let caughtCopyError {
                copyError = caughtCopyError as NSError
            }
        }
        
        if let anyError = readError ?? copyError  {
            Napier.i(tag: tag, message: "Failed to create ZIP file: \(anyError)")
            try? fileManager.removeItem(at: zipFile)
            throw anyError
        } else {
            return AttachmentData(
                data: try Data(contentsOf: zipFile),
                mimeType: "application/zip",
                fileName: zipName
            )
        }
    }
    
    func saveAttachments(mailData: ComposeMailData) -> URL? {
        guard let attachment = mailData.attachments?.first else { return nil }
        let fileManager = Foundation.FileManager.default
        let cachesDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let zipFile = cachesDirectory.appendingPathComponent(attachment.fileName)
       
        do {
            try attachment.data.write(to: zipFile)
            Napier.i(tag: tag, message: "Exported ZIP file: \(zipFile)")
            return zipFile
        } catch let error {
            try? fileManager.removeItem(at: zipFile)
            Napier.i(tag: tag, message: "Failed to export ZIP file: \(error)")
            return nil
        }
    }
}

private enum VersionError: Error {
    case invalidBundleInfo, invalidResponse
}

private class LookupResult: Decodable {
    var results: [AppInfo]
}

private class AppInfo: Decodable {
    var version: String
    var trackViewUrl: String
}

extension HelpContactViewModel {
    static let fallbackEmail = "hello@octoprint.eu"
    static let fallbackSubject = "Feedback OctoApp for iOS"
}
