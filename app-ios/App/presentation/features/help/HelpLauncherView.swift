//
//  HelpHeader.swift
//  OctoApp
//
//  Created by Christian on 25/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct HelpLauncherView: View {
    
    
    @StateObject private var viewModel = HelpLauncherViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                Header(
                    introUrl: viewModel.introUrl,
                    faq: viewModel.faq,
                    knownBugs: viewModel.knownBugs
                )
                Spacer()
            }
            .padding(OctoTheme.dimens.margin12)
        }
        .navigationTitle("help___title"~)
    }
}

private struct Header: View {
    
    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceSystemInfo.interfaceType) private var interfaceType
    let introUrl: String
    let faq: [Option]
    let knownBugs: [Option]
    private let idiom = UIDevice.current.userInterfaceIdiom
    private let options = [
        Option(
            id: "header-0",
            title: "main_menu___explore_support_plugins_short"~,
            iconSystemName: "puzzlepiece.extension.fill",
            url: UriLibrary.shared.getPluginLibraryUri(category: nil).description(),
            style: .neutral,
            descriptionText: nil
        ),
        Option(
            id: "header-1",
            title: "main_menu___item_show_tutorials"~,
            iconSystemName: "graduationcap.fill",
            url: UriLibrary.shared.getTutorialsUri().description(),
            style: .neutral,
            descriptionText: nil
        )
    ]
    private let generalContactOptions = [
        Option(
            id: "contact-a",
            title: "help___3dprinting_discord"~,
            iconSystemName: nil,
            url: "https://discord.com/invite/3dprinters",
            style: .octoprint,
            descriptionText: nil
        ),
        Option(
            id: "contact-b",
            title: "help___3dprinting_reddit"~,
            iconSystemName: nil,
            url: "https://www.reddit.com/r/3Dprinting/",
            style: .octoprint,
            descriptionText: nil
        ),
        Option(
            id: "contact-c",
            title: "help___3dprinting_printables"~,
            iconSystemName: nil,
            url: "https://www.printables.com/group/discover",
            style: .octoprint,
            descriptionText: nil
        ),
        Option(
            id: "contact-d",
            title: "help___3dprinting_thingiverse"~,
            iconSystemName: nil,
            url: "https://www.thingiverse.com/groups/3d-printing-forum",
            style: .octoprint,
            descriptionText: nil
        ),
    ]
    private var octoPrintContactOptions: [Option] {
        return generalContactOptions + [
        Option(
            id: "contact-0",
            title: "help___octoprint_community"~,
            iconSystemName: nil,
            url: "https://octoprint.org/help/?utm_source=octoapp",
            style: .octoprint,
            descriptionText: nil
        ),
        //        Option(
        //            id: "contact-1",
        //            title: "help___octoprint_discord"~,
        //            iconSystemName: nil,
        //            url: "https://discord.octoprint.org/",
        //            style: .octoprint,
        //            descriptionText: "help___octoprint_klipper_help_subtitle"~
        //        ),
    ]
    }
    private var klipperContactOptions : [Option] {
        return generalContactOptions + [
            Option(
                id: "contact-4",
                title: ("help___octoprint_discord"~).replacing("OctoPrint", with: "Klipper"),
                iconSystemName: nil,
                url: "https://discord.klipper.org/",
                style: .octoprint,
                descriptionText: nil
            ),
        ]
    }
    private let octoAppContactOptions = [
        Option(
            id: "contact-2",
            title: "help___report_a_bug"~,
            iconSystemName: nil,
            url: UriLibrary.shared.getContactUri(bugReport: true).description(),
            style: .octoprint,
            descriptionText: nil
        ),
        Option(
            id: "contact-3",
            title: "help___ask_a_question"~,
            iconSystemName: nil,
            url: UriLibrary.shared.getContactUri(bugReport: false).description(),
            style: .octoprint,
            descriptionText: nil
        )
    ]

    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            HStack(alignment: .top, spacing: OctoTheme.dimens.margin1) {
                Image("HelpAvatar")
                    .resizable()
                    .if(idiom == .pad) {$0.frame(width: 96, height: 96) }
                    .if(idiom != .pad) {$0.frame(width: 72, height: 72) }
                
                VStack(spacing: OctoTheme.dimens.margin01) {
                    Text("help___introduction_part_1"~)
                        .foregroundColor(OctoTheme.colors.darkText)
                        .typographyBase()
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(OctoTheme.dimens.margin12)
                        .surface(color: .red)
                    
                    VStack {
                        Text("help___introduction_part_2"~)
                            .foregroundColor(OctoTheme.colors.darkText)
                            .typographyBase()
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(OctoTheme.dimens.margin12)
                            .foregroundColor(OctoTheme.colors.darkText)

                        Button(
                            action: { openUrl(introUrl) },
                            label: {
                                ZStack(alignment: .bottom) {
                                    Image("IntroductionVideo")
                                        .resizable(resizingMode: .stretch)
                                        .aspectRatio(contentMode: .fit)
                                    
                                    Text("help___introduction_video_caption"~)
                                        .foregroundColor(OctoTheme.colors.textColoredBackground)
                                        .padding(OctoTheme.dimens.margin01)
                                        .typographyLabel()
                                }
                            }
                        )
                        .frame(maxWidth: .infinity)
                        .surface(color: .red)
                        .padding([.leading, .trailing, .bottom], OctoTheme.dimens.margin01)
                    }
                    .surface(color: .red)
                    .frame(maxWidth: .infinity)
                    
                    OptionsBlock(options: options)
                        .padding(.top, OctoTheme.dimens.margin1)
                    
                }
            }
        }
        OptionsBlock(title: "help___faq_title"~, options: faq)
        OptionsBlock(title: "help___bugs_title"~, options: knownBugs)
        if interfaceType == .octoPrint {
            OptionsBlock(title: "help___octoprint_help_title"~, options: octoPrintContactOptions)
        } else {
            OptionsBlock(title: "help___klipper_help_title"~, options: klipperContactOptions)
        }
        OptionsBlock(title: "help___app_help_title"~, options: octoAppContactOptions)
        Spacer()
    }
}

private struct OptionsBlock: View {
    
    var title: String?
    var options: [Option]
    @Environment(\.openURL) private var openUrl
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            if !options.isEmpty {
                if let t = title {
                    Text(t)
                        .multilineTextAlignment(.center)
                        .typographyTitle()
                        .padding(.top, OctoTheme.dimens.margin2)
                        .padding(.bottom, OctoTheme.dimens.margin1)
                }
                
                ForEach(options) { option in
                    Button(action: { handleClick(option.url) }) {
                        SimpleMenuItem(
                            title: option.title,
                            iconSystemName: option.iconSystemName,
                            style: option.style,
                            showAsOutline: option.iconSystemName != nil,
                            showAsSubMenu: true,
                            descriptionText: option.descriptionText
                        )
                    }
                }
            }
        }
    }
    
    func handleClick(_ url: String) {
        if let url = URL(string: url) {
            openUrl(url)
        }
    }
}

private class HelpLauncherViewModel : ObservableObject {
    
    private var core = HelpLauncherViewModelCore()
    
    var introUrl: String {
        return core.introUrl
    }
    
    var faq: [Option] {
        return core.faq.filter{
            $0.hidden != true
        }.map {
            Option(
                id: $0.id ?? UUID().uuidString,
                title: $0.title ?? "???",
                iconSystemName: nil,
                url: UriLibrary.shared.getFaqUri(faqId: $0.id ?? "unknown").description(),
                style: .settings,
                descriptionText: nil
            )
        }
    }
    
    var knownBugs: [Option] {
        return core.knownBugs.map {
            Option(
                id: $0.id ?? UUID().uuidString,
                title: $0.title ?? "???",
                iconSystemName: nil,
                url: UriLibrary.shared.getFaqUri(faqId: $0.id ?? "unknown").description(),
                style: .support,
                descriptionText: nil
            )
        }
    }
}

private struct Option: Identifiable {
    let id: String
    let title: String
    let iconSystemName: String?
    let url: String
    let style: MenuItemStyle
    let descriptionText: String?
}

struct HelpHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HelpLauncherView()
    }
}

