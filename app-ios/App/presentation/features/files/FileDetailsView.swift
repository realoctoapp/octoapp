//
//  FileDetailsView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct FileDetailsView: View {
    var file: FileObjectStruct
    var canStartPrint: Bool
    var onStartPrint: () async -> Void
    
    @Environment(\.instanceId) private var instanceId
    @State private var showMenu = false
        
    var body: some View {
        TabView {
            FileDetailsViewInfo(
                file: file,
                canStartPrint: canStartPrint,
                onStartPrint: onStartPrint
            )
            .tabItem {
                Label("file_manager___file_details___tab_info"~, systemImage: "doc")
            }

            GcodePreviewControlsFullscreenEmbedded(
                file: file
            )
            .tabItem{
                Label("file_manager___file_details___tab_preview"~, systemImage: "square.stack.3d.up.fill")
            }
        }
        .macOsCompatibleSheet(isPresented: $showMenu) {
            MenuHost(menu: AddItemMenu(instanceId: instanceId, path: file.path))
        }
    }
}

private struct FileDetailsViewInfo: View {
    var file: FileObjectStruct
    var canStartPrint: Bool
    var onStartPrint: () async -> Void
    var displayPath: String {
        var parts = file.path.split(separator: "/")
        parts.removeLast()
        return "/\(parts.joined(separator: "/"))"
    }
    var filamentUse: String? {
        let format = "%.02f m / %.02f cm³"
        if (file.filamentUseMm?.isEmpty == true) {
            return nil
        } else  if (file.filamentUseMm?.count == 1) {
            let f = file.filamentUseMm![0]
            return  String(format: format, f.name, f.length ?? "?", f.volume ?? "?")
        } else {
            return file.filamentUseMm?.map { i in
                String(format: "%s: \(format)", i.name, i.length ?? "?", i.volume ?? "?")
            }.joined(separator: "\n")
        }
        
    }
    var dimension: String? {
        guard let dimen = file.gcodeDimension else {
            return nil
        }
        return String(format: "%.1f × %.1f × %.1f mm", dimen.width ?? "?", dimen.depth ?? "?", dimen.height ?? "?")
    }
    var printTime: String? {
        guard let pt = file.estimatedPrintTime else {
            return nil
        }
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .brief
        return formatter.string(from: pt)
    }
    var lastPrint: String {
        guard let lastPrint = file.prints?.lastPrintDate else {
            return "file_manager___file_details___never"~
        }
        
        switch(file.prints?.lastPrintSuccess) {
        case false: return String(format: "file_manager___file_details___last_print_at_x_failure"~, lastPrint.formatted())
        case true: return String(format: "file_manager___file_details___last_print_at_x_success"~, lastPrint.formatted())
        default: return lastPrint.formatted()
        }
    }
    var failures: String {
        guard let failure = file.prints?.failure else {
            return "file_manager___file_details___never"~
        }
        
        return String(format: "x_times"~, failure.formatted())
    }
    var successes: String {
        guard let success = file.prints?.success else {
            return "file_manager___file_details___never"~
        }
        
        return String(format: "x_times"~, success.formatted())
    }
    
    var body: some View {
        ZStack(alignment: .bottom) {
            List {
                if (file.largeThumbnail != nil) {
                    Section {
                        ZStack(alignment: .center) {
                            OctoPrintNetworkImage(
                                url: file.largeThumbnail,
                                backupSystemName: "photo",
                                backupColor: OctoTheme.colors.lightText,
                                small: false
                            )
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .aspectRatio(1, contentMode: .fit)
                        }
                        .frame(maxWidth: .infinity, maxHeight: 300, alignment: .center)
                    }
                }
                
                ForEach(file.metadata) { section in
                    Section(section.label) {
                        ForEach(section.items) { item in
                            FileDetailsItem(
                                title: item.label,
                                value: item.value
                            )
                        }
                    }
                }
                
                // Padding for button
                Section {
                    Color.clear
                        .frame(height:  OctoTheme.dimens.margin4)
                        .listRowBackground(Color.clear)
                }
            }
            
            
            OctoAsyncButton(
                text: "start_printing"~,
                enabled: canStartPrint,
                clickListener: onStartPrint
            )
            .padding(OctoTheme.dimens.margin12)
        }
    }
}

struct FileDetailsView_Previews: PreviewProvider {
    static var file = FileObjectStruct(
        origin: .gcode,
        display: "Some file.gcode",
        name: "Some file.gcode",
        path: "/Some file.gcode",
        isFolder: false,
        isPrintable: true
    )
    
    static var previews: some View {
        FileDetailsView(file: file, canStartPrint: false, onStartPrint: { try? await Task.sleep(for: .seconds(1))})
            .previewDisplayName("Already printing")
        
        FileDetailsView(file: file, canStartPrint: false, onStartPrint: { try? await Task.sleep(for: .seconds(1))})
            .previewDisplayName("Can start printing")
    }
}
