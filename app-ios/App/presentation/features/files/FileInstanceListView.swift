//
//  FileInstanceListView.swift
//  OctoApp
//
//  Created by Christian Würthner on 23/04/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct FileInstanceListView: View {
    
    @StateObject private var viewModel = FileInstanceListViewModel()
    @State private var showMenu = false

    var body: some View {
        List {
            ForEach(viewModel.instances) { instance in
                NavigationLink(value: instance.url) {
                    InstanceItem(instance: instance)
                }
            }
        }
        .usingViewModel(viewModel)
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("file_manager___all_printer"~)
        .navigationViewStyle(.columns)
        .toolbar {
            ToolbarItem(placement: .bottomBar) {
                Button("control_center___manage_menu___title"~) {
                    showMenu = true
                }
            }
        }
        .macOsCompatibleSheet(isPresented: $showMenu) {
            MenuHost(menu: ManageOctoPrintInstancesMenu())
        }
    }
}

private struct InstanceItem : View {
    
    var instance: FileInstanceListViewModel.Instance
    @Environment(\.colorScheme) private var colorScheme
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            let main = .dark == colorScheme ? instance.colors.dark.main : instance.colors.light.main
            let accent = .dark == colorScheme ? instance.colors.dark.accent : instance.colors.light.accent
            
            OctoPrintNetworkImage(
                url: nil,
                backupSystemName: "externaldrive.connected.to.line.below.fill",
                backupColor: main
            )
            .frame(width: 48, height: 48)
            .padding(OctoTheme.dimens.margin01)
            .surface(color: .special(accent))

            Text(instance.label)
                .typographySubtitle()
                .lineLimit(1)
                .multilineTextAlignment(.leading)
        }
    }
}

private class FileInstanceListViewModel : BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: FileInstanceListViewModelCore? = nil
    @Published var instances: [Instance] = []
    
    func createCore(instanceId: String) -> FileInstanceListViewModelCore {
        FileInstanceListViewModelCore()
    }
    
    func publish(core: FileInstanceListViewModelCore) {
        core.instances.asPublisher()
            .sink { (list: FileInstanceListViewModelCore.InstanceList) in
                self.instances = list.instances.map {
                    Instance(
                        label: $0.label,
                        colors: $0.colors.toStruct(),
                        type: $0.type,
                        url: $0.url
                    )
                }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        instances = []
    }
}

extension FileInstanceListViewModel {
    struct Instance : Identifiable {
        var id: Int { url.hashValue }
        var label: String
        var colors: ColorsStruct
        var type: String
        var url: String
    }
}

struct FileInstanceListView_Previews: PreviewProvider {
    static var previews: some View {
        FileInstanceListView()
    }
}
