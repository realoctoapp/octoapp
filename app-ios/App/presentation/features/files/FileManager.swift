//
//  FileManager.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct FileManager: View {
    var path: String = "/"
    var label: String? = nil
    @Environment(\.instanceLabel) private var instanceLabel
    @Environment(\.instanceId) private var instanceId
    
    var body: some View {
        FileManagerView(
            path: path,
            title: path == "/" ? instanceLabel : (label ?? "")
        )
    }
}

class FileManagerViewModel : BaseViewModel {
    
    var bag: Set<AnyCancellable> = []
    var currentCore: FileListViewModelCore? = nil
    private var currentPath: String =  "/"
    private var pendingContinuation: CheckedContinuation<Void, Never>? = nil
    
    @Published var printStarted: Bool = false
    @Published var canStartPrint: Bool = false
    @Published var confirmSettingsState: StartPrintResultStruct = .failed
    @Published var state: FileManagerState = .loading
    @Published var informAboutThumbnails = false
    @Published var title: String? = nil
    
    func createCore(instanceId: String) -> FileListViewModelCore {
        return  FileListViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: FileListViewModelCore) {
        core.fileListState
            .asPublisher()
            .sink(receiveValue: { (state: FileListViewModelCore.State) in
                self.canStartPrint = state.canStartPrint
                self.confirmSettingsState = .success
                let nestedState = FlowStateStruct(
                    state.state,
                    mapper: { file in file.toStruct() }
                )
                
                switch nestedState {
                case .loading:
                    switch(self.state) {
                    case .loading: self.state = .loading
                    case .error(_): self.state = .loading
                    case .loaded(_): do { /* Drop loading state as refresh is shown */ }
                    }
                    
                case .error(let e):
                    self.state = .error(e)
                    self.firePendingContinuation()
                
                case .ready(let f):
                    self.state = .loaded(f)
                    self.firePendingContinuation()
                    
                    // Update title, stick with the default for the root folder
                    self.title = f.path == "/" ? self.title : f.display
                }
            })
            .store(in: &bag)
        
        core.informAboutThumbnails
            .asPublisher()
            .sink(receiveValue: { (inform: KotlinBoolean) in
                self.informAboutThumbnails = inform.boolValue
            })
            .store(in: &bag)
    }
    
    func clearData() {
        state = .loading
    }
    
    
    func initWith(path: String, title: String) {
        currentPath = path
        currentCore?.loadFiles(path: path, skipCache: false)
    }
    
    private func firePendingContinuation() {
        self.pendingContinuation?.resume()
        self.pendingContinuation = nil
    }
    
    func reloadAndWait() async {
        await withCheckedContinuation{ continuation in
            self.firePendingContinuation()
            self.pendingContinuation = continuation
            reload()
        }
    }
    
    func reload() {
        currentCore?.loadFiles(path: currentPath, skipCache: true)
    }
    
    func dismissThumbnailInfo() {
        currentCore?.dismissThumbnailInformation()
    }
    
    func startPrint(materialConfirmed: Bool = false, timelapseConfirmed: Bool = false) async -> StartPrintResultStruct {
        let result = try? await currentCore?.startPrint(
            materialConfirmed: materialConfirmed,
            timelapseConfirmed: timelapseConfirmed
        )
        return result?.toStruct() ?? .failed
    }
}

enum FileManagerState : Equatable {
    case loading, error(KotlinThrowable), loaded(FileObjectStruct)
}

struct FileManager_Previews: PreviewProvider {
    static var previews: some View {
        FileManager()
    }
}
