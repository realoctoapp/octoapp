//
//  FileSearchView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct FileSearchView: View {
    
    @StateObject private var viewModel = FileSearchViewModel()
    @State private var enabled = checkEnabled()
    @Environment(\.openURL) private var openUrl
    @State private var showPurchase = false
    
    var searchTerm: String
    
    var body: some View {
        ZStack {
            if enabled {
                enabledContent
            } else {
                disabledContent
            }
        }
        .onReceive(BillingManager.shared.state.asPublisher()) { (_: BillingState) in
            enabled = FileSearchView.checkEnabled()
        }
    }
    
    var enabledContent: some View {
        ZStack {
            switch(viewModel.searchState) {
            case .loading: ProgressView()
            case .error(let e): ScreenError(error: e) { viewModel.search(searchTerm: searchTerm, skipCache: true)}
            case .loaded(let folders, let files): SearchResults(folders: folders, files: files)
            }
        }
        .usingViewModel(viewModel)
        .onAppear { viewModel.search(searchTerm: searchTerm) }
        .onChange(of: searchTerm) { _, new in viewModel.search(searchTerm: new) }
        .refreshable { /* Disable refresh :)*/ }
    }
    
    var disabledContent: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            Text("supporter_perk___title"~)
                .typographyTitle()
                .padding(.bottom, OctoTheme.dimens.margin01)
            
            Text(String(format: "supporter_perk___description"~, "file_manager___file_search___hint"~))
                .typographyLabel()
            
            MenuAnimationView(
                octoAnimationName: "octo-support",
                octoAnimationNameDark: "octo-support-dark"
            )
            .padding([.top, .bottom], OctoTheme.dimens.margin3)
            
            Button(action: { openPurchaseScreen(button: "info") }) {
                SimpleMenuItem(
                    title: "learn_more"~,
                    iconSystemName: "info.circle.fill",
                    style: .neutral
                )
            }
            
            Button(action: { openPurchaseScreen(button: "support") }) {
                SimpleMenuItem(
                    title: "support_octoapp"~,
                    iconSystemName: "heart.fill",
                    style: .support
                )
            }
        }
        .multilineTextAlignment(.center)
        .padding(OctoTheme.dimens.margin12)
        .macOsCompatibleSheet(isPresented: $showPurchase) {
            NavigationStack {
                PurchaseView().toolbarDoneButton()
            }
        }
    }
    
    func openPurchaseScreen(button: String) {
        OctoAnalytics.shared.logEvent(
            event: OctoAnalytics.EventPurchaseScreenOpen.shared,
            params: ["trigger" : "file_search_\(button)"]
        )
        
        showPurchase = true
    }
    
    static func checkEnabled() -> Bool {
        BillingManager.shared.isFeatureEnabled(feature: BillingManagerKt.FEATURE_FILE_MANAGEMENT)
    }
}

private struct SearchResults : View {
    var folders: [FileObjectStruct]
    var files: [FileObjectStruct]
    
    var body : some View {
        List {
            FileListSection(files: folders)
            FileListSection(files: files)
        }
        .animation(.default, value: files)
        .animation(.default, value: folders)
    }
}

private class FileSearchViewModel : BaseViewModel {
    var bag: Set<AnyCancellable> = []
    var currentCore: FileSearchViewModelCore? = nil
    
    @Published var searchState: FileSearchState = .loading
    
    func createCore(instanceId: String) -> FileSearchViewModelCore {
        return FileSearchViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: FileSearchViewModelCore) {
        core.searchState
            .asPublisher()
            .sink(receiveValue: { (state: FlowState<FileSearchViewModelCore.SearchResult>) in
                if state is FlowStateLoading {
                    self.searchState = .loading
                } else if let s = state as? FlowStateError {
                    self.searchState = .error(s.throwable)
                } else if let s = (state as? FlowStateReady)?.data {
                    self.searchState = .loaded(
                        s.folders.map { $0.toStruct() },
                        s.files.map { $0.toStruct() }
                    )
                }
            })
            .store(in: &bag)
    }
    
    func clearData() {
        searchState = .loading
    }
   
    func search(searchTerm: String, skipCache: Bool = false) {
        currentCore?.search(term: searchTerm, skipCache: skipCache)
    }
}


private enum FileSearchState : Equatable {
    case loading, error(KotlinThrowable), loaded([FileObjectStruct], [FileObjectStruct])
}

struct FileSearchView_Previews: PreviewProvider {
    static var previews: some View {
        FileSearchView(searchTerm: "Something")
    }
}
