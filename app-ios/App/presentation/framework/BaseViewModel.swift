//
//  BaseViewModel.swift
//  OctoApp
//
//  Created by Christian on 02/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private struct BaseViewModelHolder: ViewModifier {
    
    @State private var initialized = false
    @State private var referenceCounter = 0
    @Environment(\.instanceId) private var activeInstanceId
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @StateObject private var errorHandler = ErrorHandler()
    private var instanceId: String { fixedInstanceId ?? activeInstanceId }
    
    let viewModel: any BaseViewModel
    let fixedInstanceId: String?
    let tag = "BaseViewModelHolder"
    let active: Bool
    
    func body(content: Content) -> some View {
        content
            .onAppear {
                if !initialized {
                    initialized = true
                    viewModel.clearData()
                    Napier.d(tag: tag, message: "Init \(viewModel)")
                }
                
                if active {
                    attach(instanceId: instanceId, reason: "Appear")
                }
            }
            .onChange(of: instanceId) { oldInstanceId, newInstanceId in
                if active {
                    detact(instanceId: oldInstanceId, reason: "Change of instance")
                    attach(instanceId: newInstanceId, reason: "Change of instance")
                }
            }
            .onDisappear {
                if active {
                    detact(instanceId: instanceId, reason: "Disappear")
                }
            }
            .onChange(of: active) { _, nowActive in
                if nowActive {
                    attach(instanceId: instanceId, reason: "Activating")
                } else {
                    detact(instanceId: instanceId, reason: "Deactivating")
                }
            }
            .onChange(of: errorHandler.error) { _, error in
                if let e = error?.throwable {
                    orchestrator.postError(error: e)
                }
            }
    }
    
    func attach(instanceId: String, reason: String) {
        if referenceCounter == 0 {
            Napier.d(tag: tag, message: "Attaching \(viewModel) @ \(instanceId) (refCounter=\(referenceCounter), reason=\(reason)")
            let core = viewModel.start(instanceId)
            errorHandler.observeErrors(core: core)
        }
        
        referenceCounter += 1
    }
    
    func detact(instanceId: String, reason: String) {
        referenceCounter -= 1
        
        if referenceCounter == 0 {
            Napier.d(tag: tag, message: "Detaching \(viewModel) @ \(instanceId) (refCounter=\(referenceCounter), reason=\(reason))")
            viewModel.stop()
        }
    }
}

private class ErrorHandler : ObservableObject {
        @Published var error: ErrorWrapper? = nil
    var bag : Set<AnyCancellable> = []
    
    func observeErrors(core: BaseViewModelCore) {
        bag.forEach { $0.cancel() }
        bag.removeAll()
        
        core.errors.asPublisher()
            .sink { (e: KotlinThrowable) in
                self.error = ErrorWrapper(throwable: e)
            }
            .store(in: &bag)
    }
}

private struct ErrorWrapper : Equatable, Identifiable {
    var id: String = UUID().uuidString
    var throwable: KotlinThrowable
}

extension View {
    func usingViewModel(_ viewModel: any BaseViewModel, fixedInstanceId: String? = nil, active: Bool = true) -> some View {
        modifier(BaseViewModelHolder(viewModel: viewModel, fixedInstanceId: fixedInstanceId, active: active))
    }
}

@MainActor
protocol BaseViewModel : ObservableObject {
    associatedtype Core: BaseViewModelCore
    
    var currentCore: Core? { get set }
    var bag:Set<AnyCancellable> { get set }
    
    func createCore(instanceId: String) -> Core
    func publish(core: Core)
    func clearData()
}

extension BaseViewModel {
    func start(_ instanceId: String) -> BaseViewModelCore {
        stop()
        let core = createCore(instanceId: instanceId)
        currentCore = core
        publish(core: core)
        
        return core
    }
    
    func stop() {
        currentCore = nil
        bag.forEach { $0.cancel() }
        bag.removeAll()
    }
}
