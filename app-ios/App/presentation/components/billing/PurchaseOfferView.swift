//
//  OfferVire.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import StoreKit
import OctoAppBase

@MainActor
struct PurchaseOfferView: View {
    
    var offer: PurchaseOfferStruct
    var screenTitle: String
    var purchases: [BillingPurchaseStruct]
    var purchaseProduct: (Product, Set<Product.PurchaseOption>) async throws -> Void
    
    @State private var showSubscriptionWarning = false
    @State private var showLifetimeActiveError = false
    @State private var showTipWarning = false
    @State private var showSubscriptions = false
    @State private var quantity = ""
    @State private var loading = false
 
    private var buttonLabel : String { offer.label ?? offer.product.productName }
    private var freeTrialText: String? {
        guard let introductoryOffer = offer.product.storeProduct?.subscription?.introductoryOffer else {
            return nil
        }
        
        switch introductoryOffer.paymentMode {
        case .freeTrial: return "\(introductoryOffer.periodCount.formatted()) \(introductoryOffer.period.unit.localizedDescription)"
        default: return nil
        }
    }
    private var purchased: Bool {
        purchases.contains { $0.productId == offer.product.id }
    }
    private var hasActiveSubscription: Bool {
        let offerIsNotSubscription = offer.product.storeProduct?.subscription == nil
        let anySubscriptionActive = purchases.contains { $0.isSubscription }
        return offerIsNotSubscription && anySubscriptionActive
    }
    private var hasAlreadyLifetime: Bool {
        purchases.contains { !$0.isSubscription }
    }
    private var isTip: Bool {
        offer.id == "tip"
    }
    
    var body: some View {
        ZStack {
            if isTip {
                tipView
            } else {
                ZStack(alignment: .topTrailing) {
                    badges
                    card
                }
                .surface()
            }
        }
        .manageSubscriptionsSheet(isPresented: $showSubscriptions)
        .alert("billing___warning_active_subscription"~, isPresented: $showSubscriptionWarning) {
            Button("Continue"~, role: .destructive) {
                Task {
                    loading = true
                    try? await purchase(bypassChecks: true)
                    loading = false
                }
            }
            Button("billing___manage_subscriptions"~) {
                showSubscriptions = true
            }
        }
        .alert(String(format: "billing___warning_tip"~, offer.product.price), isPresented: $showTipWarning) {
            TextField("billing___tip_quantity"~, text: $quantity)
                .keyboardType(.numberPad)
            
            Button("Continue"~, role: .destructive) {
                Task {
                    loading = true
                    try? await purchase(
                        bypassChecks: true,
                        options: [ .quantity(Int(quantity) ?? 1) ]
                    )
                    loading = false
                }
            }
        }
        .alert("billing___error_lifetime_already_active"~, isPresented: $showLifetimeActiveError) {
            Button("OK"~) {}
        }
    }
    
    var tipView: some View {
        OctoButton(
            text: "Tip the developer",
            small: true,
            clickListener: { showTipWarning = true }
        )
        .padding(.top, OctoTheme.dimens.margin5)
    }
    
    @ViewBuilder
    var badges: some View {
        switch offer.badge {
        case .sale: Image("badgeSale")
        case .bestValue: Image("badgeBestValue")
        case .popular: Image("badgePopular")
        case .none: ZStack {}
        }
    }
    
    var card: some View {
        VStack(spacing: 0) {
            VStack(spacing: 0) {
                saleInfo
                price
                subscriptionInfo
            }
            .padding(OctoTheme.dimens.margin2)
            
            OctoAsyncButton(
                text: buttonLabel,
                type: .surface,
                small: true,
                enabled: !purchased,
                clickListener: { try await purchase() }
            )
        }
    }
    
    @ViewBuilder
    var saleInfo : some View {
        if let deal = offer.dealFor {
            HStack {
                Text(deal.price)
                    .strikethrough()
                    .foregroundColor(OctoTheme.colors.lightText)
                    .typographyLabel()
                
                if
                    let oldPrice = offer.product.storeProduct?.price,
                    let newPrice = deal.storeProduct?.price
                {
                    Text(calcDiscount(oldPrice: oldPrice, newPrice: newPrice))
                        .fixedSize(horizontal: true, vertical: false)
                        .foregroundColor(OctoTheme.colors.textColoredBackground)
                        .typographyLabel()
                        .padding([.top, .bottom], OctoTheme.dimens.margin0)
                        .padding([.leading, .trailing], OctoTheme.dimens.margin01)
                        .background(Capsule().fill(OctoTheme.colors.red))
                }
            }
        }
    }
    
    var price: some View {
        HStack(alignment: .bottom, spacing: 0) {
            Text(offer.product.price)
                .typographyDataLarge()
            
            if
                let period = offer.product.storeProduct?.subscription?.subscriptionPeriod,
                let format = offer.product.storeProduct?.subscriptionPeriodFormatStyle
            {
                Text(" / \(priceSuffix(period: period, format: format))")
            }
        }
        .foregroundColor(OctoTheme.colors.lightText)
        .typographyLabel()
    }
    
    var subscriptionInfo : some View {
        VStack {
            if isTip {
                Text("billing___tip_description"~)
            } else if purchased {
                Text("billing___owned"~)
            } else if offer.product.storeProduct?.subscription != nil {
                if let trial = freeTrialText {
                    Text(String(format: "billing___subscription_free_trial"~, trial))
                } else {
                    Text("billing___subscription"~)
                }
            } else {
                Text("billing___one_time_purchase"~)
            }
            
            if let description = offer.descriptionText {
                Text(LocalizedStringKey(description))
            }
        }
        .foregroundColor(OctoTheme.colors.lightText)
        .typographyLabelSmall()
    }
    
    func calcDiscount(oldPrice: Decimal, newPrice: Decimal) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 0
        return formatter.string(from: ((1 - (oldPrice / newPrice)) * -1) as NSNumber) ?? ""
    }
    
    func priceSuffix(period: Product.SubscriptionPeriod, format: Date.ComponentsFormatStyle) -> String {
        var format2 = format
        format2.style = .abbreviated
        let formatted =  period.formatted(format2)
        
        // Filter out "1" if the value is one so we only get "year"
        return period.value == 1 ? formatted.filter { $0.isLetter } : formatted
    }
    
    func purchase(bypassChecks: Bool = false, options: Set<Product.PurchaseOption> = []) async throws {
        if !bypassChecks {
            guard !isTip else {
                showTipWarning = true
                return
            }
            
            guard !hasAlreadyLifetime else {
                showLifetimeActiveError = true
                return
            }
            
            guard !hasActiveSubscription else {
                showSubscriptionWarning = true
                return
            }
        }
        
        OctoAnalytics.shared.logEvent(
            event: OctoAnalytics.EventPurchaseOptionSelected.shared,
            params: [
                "button_text" : buttonLabel,
                "title" : screenTitle,
                "trial" : offer.product.storeProduct?.subscription?.introductoryOffer.flatMap{
                    "\($0.period)\($0.period.unit)"
                } ?? "",
                "badge" : offer.badge,
                "deal_for" : offer.dealFor?.id ?? "",
                "sku" : offer.product.id
            ]
        )
        
        if let product = offer.product.storeProduct {
            try await purchaseProduct(product, options)
        }
    }
}
