//
//  PurchaseShowOffersButton.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct PurchaseShowOffersButton: View {
    
    var safeAreaBottom: CGFloat
    var hasPurchases: Bool
    var continueCta: String
    var clickListener: () -> Void = {}
    
    var body: some View {
        OctoButton(
            text: hasPurchases ? "billing___supporter_button"~ : continueCta,
            clickListener: clickListener
        )
        .padding(.bottom, safeAreaBottom)
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        .if(safeAreaBottom == 0) { $0.padding(.bottom, OctoTheme.dimens.margin12) }
        .transition(.move(edge: .bottom))
    }
}
