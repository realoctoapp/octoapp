//
//  DataButton.swift
//  OctoApp
//
//  Created by Christian on 26/02/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import SwiftUI

struct DataButton<Content: View>: View {
    var iconSystemName: String?
    var clickListener: () async throws -> Void
    var content: () -> Content
    
    var body: some View {
        HStack(spacing: 0) {
            ZStack(alignment: .leading) {
                content()
                    .frame(maxWidth: .infinity)
            }
            .padding([.top, .bottom], OctoTheme.dimens.margin0)
            .padding(.trailing, OctoTheme.dimens.margin12)
            .padding(.leading, OctoTheme.dimens.margin0)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Capsule().fill(OctoTheme.colors.inputBackground))
            
            if let icn = iconSystemName {
                OctoAsyncIconButton(
                    icon: icn,
                    clickListener: clickListener
                )
                .padding(.leading, -OctoTheme.dimens.margin01)
                .zIndex(-1)
            }
        }
        .background(Capsule().fill(OctoTheme.colors.inputBackground.opacity(0.66)))
        .clipShape(Capsule())
        .fixedSize(horizontal: false, vertical: true)
    }
}

#Preview {
    DataButton(
        iconSystemName: "folder.fill",
        clickListener: {}
    ) {
        Text("Some content")
    }
}
