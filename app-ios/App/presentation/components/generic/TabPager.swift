//
//  TabPager.swift
//  OctoApp
//
//  Created by Christian on 28/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI


struct OctoTabs<TabId: Hashable&Identifiable> : View {
    
    var scrollable = true
    @Binding var selection: TabId
    var tabs: [TabId]
    var label: (TabId) -> String
    
    var body : some View {
        if scrollable {
            ScrollViewReader { reader in
                ScrollView(.horizontal, showsIndicators: false) {
                    tabsRow { tab in
                        reader.scrollTo(tab, anchor: .center)
                    }
                    .fixedSize()
                    .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                    .padding([.top], OctoTheme.dimens.margin12)
                    .onChange(of: selection) { _, s in
                        Task {
                            // For some reason we need to decouple this to make it work...
                            try? await Task.sleep(for: .milliseconds(10))
                            await MainActor.run {
                                withAnimation {
                                    reader.scrollTo(s, anchor: .center)
                                }
                            }
                        }
                    }
                    .onChange(of: tabs) {
                        withAnimation {
                            reader.scrollTo(selection, anchor: .center)
                        }
                    }
                }
            }
        } else {
            tabsRow(scrollTo: { _ in })
        }
    }
    
    private func tabsRow(scrollTo: @escaping (TabId) -> Void) -> some View {
        HStack(spacing: scrollable ? OctoTheme.dimens.margin01 : 0) {
            ForEach(tabs) { tab in
                TabButton(
                    selection: $selection,
                    tabId: tab,
                    label: label(tab),
                    fillHeight: !scrollable,
                    scrollToMe: { scrollTo(tab) }
                )
            }
        }
    }
}

private struct TabButton<TabId: Hashable&Identifiable> : View{
    
    @Binding var selection: TabId
    var tabId: TabId
    var label: String
    var fillHeight: Bool
    var scrollToMe: () -> Void
    
    var body : some View {
        OctoButton(
            text: label,
            type: selection == tabId ? .primary : .primaryUnselected,
            small: true,
            fillWidth: fillHeight,
            fillHeight: fillHeight,
            clickListener: {
                withAnimation {
                    selection = tabId
                    scrollToMe()
                }
            }
        )
        .id(tabId)
    }
}

struct OctoPager<TabId: Hashable&Identifiable, Page: View> : View{
    
    @Binding var selection: TabId
    var tabs: [TabId]
    @ViewBuilder var pages: (TabId) -> Page
    
    var body : some View {
        TabView(selection: $selection) {
            ForEach(tabs) { tab in
                ZStack {
                    pages(tab)
                }
                .tag(tab)
            }
        }
        .tabViewStyle(.page(indexDisplayMode: .never))
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct OctoPagerWithTabs<TabId: Hashable&Identifiable, Page: View> : View{
    
    @Binding var selection: TabId
    var tabs: [TabId]
    var label: (TabId) -> String
    @ViewBuilder var pages: (TabId) -> Page
    
    var body : some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            OctoTabs(
                scrollable: true,
                selection: $selection,
                tabs: tabs,
                label: label
            )
            
            OctoPager(
                selection: $selection,
                tabs: tabs,
                pages: pages
            )
        }
    }
}


struct TabPager_Previews: PreviewProvider {
    static var previews: some View {
        let  tabs = ["0", "1", "2", "3", "4", "5"]
        
        StatefulPreviewWrapper("1") { activeTab in
            VStack {
                OctoPagerWithTabs(
                    selection: activeTab,
                    tabs: tabs,
                    label: { tab in "Tab \(tab)" }
                ) { tab in
                    Text(tab)
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                        .surface()
                        .padding()
                }
            }
        }
    }
}
