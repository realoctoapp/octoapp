//
//  OctoAvatar.swift
//  OctoApp
//
//  Created by Christian on 15/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import LottieUI

struct OctoAvatar: View {
    
    var action: OctoAvatarAction
    @Namespace private var animation
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        LottieView(lottieAnimation)
            .loopMode(loop ? .loop : .playOnce)
            .play(true)
            .matchedGeometryEffect(id: "avatar", in: animation)
            .id(AnimationId(action: action, colorScheme: colorScheme))
            .aspectRatio(7/3, contentMode: .fit)
            .ifPad { $0.frame(maxWidth: 400, maxHeight: 400) }
    }
    
    var lottieAnimation: String {
        var suffix = ""
        if colorScheme == .dark {
            suffix = "-dark"
        }
        
        switch action {
        case .wave: return "octo-wave\(suffix)"
        case .swim: return "octo-swim\(suffix)"
        case .party: return "octo-party\(suffix)"
        case .idle: return "octo-blink\(suffix)"
        }
    }
    
    var loop: Bool {
        switch action {
        case .wave: return false
        case .swim: return true
        case .party: return false
        case .idle: return true
        }
    }
}

private struct AnimationId : Hashable {
    let action: OctoAvatarAction
    let colorScheme: ColorScheme?
}

enum OctoAvatarAction {
    case idle, wave, swim, party
}

struct OctoAvatar_Previews: PreviewProvider {
    static var previews: some View {
        OctoAvatar(action: .wave)
            .previewDisplayName("Wave")
            .background(.red.opacity(0.1))
        
        OctoAvatar(action: .swim)
            .previewDisplayName("Swim")
            .background(.red.opacity(0.1))
        
        OctoAvatar(action: .idle)
            .previewDisplayName("Idle")
            .background(.red.opacity(0.1))
        
        OctoAvatar(action: .idle)
            .previewDisplayName("Blink")
            .background(.red.opacity(0.1))
        
        OctoAvatar(action: .party)
            .previewDisplayName("Party")
            .background(.red.opacity(0.1))
    }
}

