//
//  SwiftUIView.swift
//  OctoApp
//
//  Created by Christian on 03/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct TextTag: View {
    var icon: String? = nil
    var text: String
    var color: Color
    var countdownSecs: Double = 0
    var countDownFilled: Bool = false
    
    var body: some View {
        ZStack(alignment: .leading) {
           countDown
            
            HStack(spacing: OctoTheme.dimens.margin01) {
                iconView
                textView
            }
            .padding([.leading, .trailing], OctoTheme.dimens.margin01)
            .padding([.top, .bottom], OctoTheme.dimens.margin0)
        }
        .background(color)
        .clipShape(Capsule())
        .animation(.linear(duration: countdownSecs), value: countDownFilled)
        .fixedSize(horizontal: true, vertical: true)
    }
    
    @ViewBuilder
    private var countDown: some View {
        if countdownSecs > 0 {
            Color.white.opacity(0.2)
                .frame(maxWidth: countDownFilled ? .infinity : 0, alignment: .leading)
        }
    }
    
    @ViewBuilder
    private var iconView: some View {
        if let i = icon {
            ZStack {
                Image(systemName: i)
                    .font(.system(size: 10))
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
                
                // To enforce height if we only have an icon by no txt
                TextTabLabel(text: " ")
            }
        }
    }
    
    @ViewBuilder
    private var textView: some View {
        // Only add if text is not empty
        if !text.isEmpty {
            TextTabLabel(text: text)
        }
    }
}

private struct TextTabLabel: View {
    
    var text: String
    
    var body: some View {
        Text(text)
            .foregroundColor(OctoTheme.colors.textColoredBackground)
            .typographyLabelSmall()
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        TextTag(icon: "circle.fill", text: "Live", color: OctoTheme.colors.red)
            .previewDisplayName("Live")
        TextTag(text: "720p", color: .black.opacity(0.1))
            .previewDisplayName("Resolution")
        HStack {
            TextTag(icon: "star.fill", text: "", color: .red)
            TextTag(icon: "star.fill", text: "Star", color: .red)
        }
            .previewDisplayName("Icon only")
    }
}

extension String: Identifiable {
    public var id: String { self }
}
