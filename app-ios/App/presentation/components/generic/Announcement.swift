//
//  SmallAnnouncement.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI


struct AnnouncementController<Content: View> : View {
        
    var announcementId: String
    @ViewBuilder var content: (@escaping ()-> Void) -> Content

    private var defaults: UserDefaults { UserDefaults.init(suiteName: "announcements") ?? UserDefaults.standard }
    private var defaultsKey : String { "announcements:\(announcementId)" }
    private var wasDismissed: Bool { defaults.bool(forKey: defaultsKey ) }
    @State private var wasDismissedNow: Bool = false

    
    var body: some View {
        if !wasDismissed && !wasDismissedNow {
            content() {
                // Dismiss with global animation so all layouts properly update
                withAnimation {
                    defaults.set(true, forKey: defaultsKey)
                    wasDismissedNow = true
                }
            }
        }
    }
}

struct LargeAnnouncement: View {
    var partOfList:Bool = false
    var title: String
    var text: String
    var buttonTextColor: Color? = nil
    var learnMoreButton: String? = nil
    var hideButton: String = "hide"~
    var onLearnMore: () -> Void = {}
    var onHideAnnouncement: () -> Void
    
    private var sideMargin: CGFloat { return partOfList ? OctoTheme.dimens.margin12 : OctoTheme.dimens.margin2 }
    private var buttonType: OctoButtonType {
        if let buttonTextColor = buttonTextColor {
            return .linkWithCustomColor(textColor: buttonTextColor)
        } else {
            return .link
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: OctoTheme.dimens.margin0) {
            Text(title)
                .frame(maxWidth: .infinity, alignment: .leading)
                .typographySubtitle()
                .multilineTextAlignment(.leading)
                .padding([.leading, .trailing, .top], sideMargin)

            
            Text(text)
                .foregroundColor(OctoTheme.colors.lightText)
                .frame(maxWidth: .infinity, alignment: .leading)
                .typographyBase()
                .multilineTextAlignment(.leading)
                .padding([.leading, .trailing], sideMargin)
                .padding(.top, OctoTheme.dimens.margin0)
            
            HStack(spacing: -OctoTheme.dimens.margin12) {
                Spacer()
                if let lm = learnMoreButton {
                    OctoButton(
                        text: lm,
                        type: buttonType,
                        small: true,
                        clickListener: onLearnMore
                    )
                }
                OctoButton(
                    text: hideButton,
                    type: buttonType,
                    small: true,
                    clickListener: onHideAnnouncement
                )
            }
            .if(!partOfList) { $0.padding(.trailing, OctoTheme.dimens.margin01) }
            .padding(.bottom, OctoTheme.dimens.margin01)
            .padding(.top, OctoTheme.dimens.margin1)
        }
        .frame(maxWidth: .infinity)
        .if(!partOfList) { $0.surface() }
        .if(partOfList) { $0.listRowInsets(EdgeInsets()) }
        
    }
}
      

struct SmallAnnouncement: View {
    var hasBackground: Bool = true
    var hasPadding: Bool = true
    var text: String
    var buttonTextColor: Color? = nil
    var learnMoreButton: String? = nil
    var hideButton: String? = "hide"~
    var onLearnMore: () async -> Void = {}
    var onHideAnnouncement: () async -> Void
    
    private var buttonType: OctoButtonType {
        if let buttonTextColor = buttonTextColor {
            return .linkWithCustomColor(textColor: buttonTextColor)
        } else {
            return .link
        }
    }
    
    var body: some View {
        HStack(spacing: -OctoTheme.dimens.margin12) {
            Text(text)
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyBase()
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity, alignment: .leading)
                .fixedSize(horizontal: false, vertical: true)
            
            HStack(spacing: -OctoTheme.dimens.margin12) {
                if let lm = learnMoreButton {
                    OctoAsyncButton(
                        text: lm,
                        type: buttonType,
                        small: true,
                        clickListener: onLearnMore
                    )
                }
                if let hide = hideButton {
                    OctoAsyncButton(
                        text: hide,
                        type: buttonType,
                        small: true,
                        clickListener: onHideAnnouncement
                    )
                }
            }
            .padding(.trailing, OctoTheme.dimens.margin01)
            .layoutPriority(1)
        }
        .frame(maxWidth: .infinity)
        .if(hasPadding) { $0.padding([.leading, .bottom, .top], OctoTheme.dimens.margin2) }
        .if(hasBackground) { $0.surface() }
    }
}
        


struct SmallAnnouncement_Previews: PreviewProvider {
    static var previews: some View {
        LargeAnnouncement(
            title: "This is an announcement",
            text: "This is a description",
            hideButton: "Hide",
            onHideAnnouncement: {}
        )
        .previewDisplayName("Large")
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)

        LargeAnnouncement(
            title: "This is an announcement",
            text: "This is a description",
            learnMoreButton: "Learn more",
            hideButton: "Hide",
            onHideAnnouncement: {}
        )
        .previewDisplayName("Large + Learn more")
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)

        SmallAnnouncement(
            text: "This is a description dfdfhuihf uishufsu fhssd fnjf s hdfhu hfuish dsfuhsduif hsdui fudshufi dshiufhdsiuhfuisdhf iushdiu fhdsui fhuisdshdfiusdh ifusdhui sddududu",
            learnMoreButton: "Learn more",
            hideButton: "Hide",
            onHideAnnouncement: {}
        )
        .previewDisplayName("Small + Learn more")
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)

        SmallAnnouncement(
            text: "This is a description dsuhu sdhuifhsuifhsui hfuis",
            hideButton: "Hide",
            onHideAnnouncement: {}
        )
        .previewDisplayName("Small")
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)

        SmallAnnouncement(
            text: "Welcome to OctoApp 1.17! 🍾",
            learnMoreButton: "What's new?",
            hideButton: "Hide",
            onLearnMore: {},
            onHideAnnouncement: {}
        )
        .previewDisplayName("Small 2")
        .padding([.leading, .trailing], OctoTheme.dimens.margin12)
        
        SmallAnnouncement(
            hasBackground: false,
            text: "Some sale is going on!",
            buttonTextColor: OctoTheme.colors.red,
            learnMoreButton: "learn_more"~,
            onLearnMore: {},
            onHideAnnouncement: {}
        )
        .surface(color: .red)
        .previewDisplayName("Special colors")
    }
}
