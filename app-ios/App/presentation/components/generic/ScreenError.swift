//
//  ScreenError.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct ScreenError: View {
    var title: String = "error_general"~
    var description: String? = nil
    var error: KotlinThrowable? = nil
    var onDetails: (() -> Void)? = nil
    var canDismiss: Bool = true
    var onRetry: (() async -> Void)? = nil
    var textColor: Color? = nil
    var onDetailsLabel: String? = nil
    
    @Environment(\.dismiss) private var dismiss
    @EnvironmentObject private var orchestrator: OrchestratorViewModel

    var body: some View {
        VStack(spacing: 0) {
            Text(title)
                .if(textColor != nil) { $0.foregroundColor(textColor!) }
                .multilineTextAlignment(.center)
                .lineLimit(2)
                .typographySubtitle()
            
            if let description = description, !description.isEmpty {
                Text(LocalizedStringKey(description))
                    .if(textColor != nil) { $0.foregroundColor(textColor!) }
                    .multilineTextAlignment(.center)
                    .typographyLabel()
                    .lineLimit(5)
                    .padding(.top, OctoTheme.dimens.margin01)
            }
            
            if error != nil || onRetry != nil || canDismiss || onDetails != nil {
                HStack(spacing: OctoTheme.dimens.margin12) {
                    let firstType = onRetry != nil || canDismiss ? OctoButtonType.secondary : OctoButtonType.primary
                    if let details = onDetails {
                        OctoButton(text: onDetailsLabel ?? "show_details"~, type: firstType, small: true) {
                            details()
                        }
                    } else if let throwable = error {
                        OctoButton(text: "show_details"~, type: firstType, small: true) {
                            orchestrator.postError(error: throwable, showDetailsDirectly: true)
                        }
                    }
                    
                    if let retry = onRetry {
                        OctoAsyncButton(text: "try_again"~, small: true, clickListener: retry)
                    } else if canDismiss {
                        OctoButton(text: "done"~, small: true, clickListener: { dismiss() })
                    }
                }
                .padding(.top, OctoTheme.dimens.margin2)
            }
        }
    }
}

struct ScreenError_Previews: PreviewProvider {
    static var previews: some View {
        ScreenError(description: "Description", error: KotlinThrowable()) {}
        ScreenError(description: "Description", error: KotlinThrowable())
        ScreenError(description: "Description with (here)[http://link] link")
    }
}
