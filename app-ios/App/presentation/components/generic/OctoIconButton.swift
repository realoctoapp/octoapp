//
//  OctoIconButton.swift
//  App
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct OctoIconButton: View {
    
    var icon: String
    var color: Color = OctoTheme.colors.accent
    var iconScale: CGFloat = 1
    var loading: Bool = false
    var enabled: Bool = true
    var clickListener: () -> Void
    @State var pressed = false
    var extraClickArea: CGFloat = 5
    
    var body: some View {
        ZStack {
            Button(
                action: {
                    if !loading { clickListener() }
                }
            ) {
                Image(systemName: icon)
                    .scaleEffect(iconScale)
                    .font(.system(size: 22))
                    .foregroundColor(color)
                    .padding(extraClickArea)
                
                // Background makes it easier to click some icons with transparent areas?
                    .background(OctoTheme.colors.windowBackground.opacity(0.01), in: Circle())
            }
            .padding(-extraClickArea)
            .buttonStyle(IconButtonStyle())
            .opacity(loading ? 0 : 1)
            .disabled(!enabled)

            ProgressView()
                .tint(color)
                .opacity(loading ? 1 : 0)
            
        }
        .opacity(enabled ? 1 : 0.4)
        .animation(.default, value: loading && enabled)
    }
}

struct OctoAsyncIconButton : View {
    var icon: String
    var color: Color = OctoTheme.colors.accent
    var iconScale: CGFloat = 1
    var loading: Bool = false
    var enabled: Bool = true
    var clickListener: () async throws -> Void

    @MainActor @State private var internalLoading: Bool = false
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    var combinedLoading: Bool { loading || internalLoading }

    var body: some View {
        OctoIconButton(
            icon: icon,
            color: color,
            iconScale: iconScale,
            loading: combinedLoading,
            enabled: enabled,
            clickListener: {
                if combinedLoading { return }
                
                internalLoading = true
                Task {
                    do {
                        try await clickListener()
                    } catch {
                        orchestrator.postError(error: KotlinThrowable(message: "Error in Swift: \(error)"))
                    }
                    
                    internalLoading = false
                }
            }
        )
    }
}

private struct IconButtonStyle: ButtonStyle {
    
    @Environment(\.isEnabled) private var isEnabled: Bool
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(OctoTheme.dimens.margin1)
            .scaleEffect(0.75)
            .background(configuration.isPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear)
            .clipShape(Circle())
            .scaleEffect(1.25)
            .opacity(isEnabled ? 1 : 0.5)
    }
}

struct OctoIconButton_Previews: PreviewProvider {
    static var previews: some View {
        OctoIconButton(icon: "eye") {
            
        }
        .background(.green)
    }
}
