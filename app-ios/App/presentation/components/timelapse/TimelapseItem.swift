//
//  TimelapseItem.swift
//  OctoApp
//
//  Created by Christian on 11/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct TimelapseItem: View {
    var file: TimelapseFileStruct
    var action: () -> Void
    
    private var descriptionText: String {
        let size = file.bytes.asStyleFileSize()
        let date = file.date.flatMap {
            let f = RelativeDateTimeFormatter()
            return f.string(for: $0)
        }
        
        guard let d = date else { return size }
        return "\(d)\n\(size)"
    }
    
    var body: some View {
        Button(action: action) {
            HStack(alignment: .center, spacing: OctoTheme.dimens.margin1) {
                thumbnail
                text
            }
        }
    }
    
    var thumbnail: some View {
        OctoPrintNetworkImage(url: file.thumbnail)
            .aspectRatio(contentMode: .fill)
            .ifPad { $0.frame(width: 136, height: 102) }
            .ifNotPad{ $0.frame(width: 91, height: 68) }
            .surface()
    }
    
    var text: some View {
        VStack(alignment: .leading, spacing: OctoTheme.dimens.margin0) {
            title
            description
        }
        .frame(maxWidth: .infinity)
        .multilineTextAlignment(.leading)
    }
    
    var title: some View {
        Text(file.name)
            .foregroundColor(OctoTheme.colors.darkText)
            .typographyBase()
            .lineLimit(2)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    var description: some View {
        Text(descriptionText)
            .foregroundColor(OctoTheme.colors.lightText)
            .typographyLabel()
            .lineLimit(2)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
}


struct TimelapseItem_Previews: PreviewProvider {
    static var previews: some View {
        TimelapseItem(
            file: TimelapseFileStruct(
                name: "CE3PRO_PlantPot_SquareHoles_drainHoles.gcode_20220522102406.mp4",
                bytes: 3263344,
                date: Date(milliseconds: 3267223383),
                downloadPath: nil,
                thumbnail: nil,
                processing: false,
                rendering: false,
                recording: false
            ),
            action: {}
        )
    }
}
