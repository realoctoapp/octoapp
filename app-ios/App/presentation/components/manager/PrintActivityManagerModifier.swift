//
//  PrintActivityManagerModifier.swift
//  OctoApp
//
//  Created by Christian on 08/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase
import Combine
import WidgetKit


private struct ManagingLiveActivityDisabled: ViewModifier {
    func body(content: Content) -> some View {
        content
    }
}

private struct ManagingLiveActivity: ViewModifier {
    
    private var tag = "[LA] ManagingLiveActivity"
    @StateObject private var viewModel = ManagingLiveActivityViewModel()
    @Environment(\.instanceId) private var instanceId
    @Environment(\.instanceLabel) private var instanceLabel
    @Environment(\.instanceColor) private var instanceColor
    @Environment(\.instanceHasCompanion) var instanceHasCompanion
    @Environment(\.scenePhase) var scenePhase
    
    func body(content: Content) -> some View {
        content.if(!ProcessInfo.processInfo.isMacCatalystApp) {
            $0
                .usingViewModel(viewModel)
                .onChange(of: viewModel.data) { _, newData in
                    guard let data = newData else { return }
                    if scenePhase != .active { return }
                    if instanceHasCompanion != true { return }
                    
                    Task {
                        do {
                            try await SharedPrintAcitvyManager.startOrUpdateActivity(
                                instanceId: instanceId,
                                instanceLabel: instanceLabel,
                                instanceColor: data.colors,
                                thumbnailUrl: data.thumbnailUrl,
                                data: data.data
                            )
                        } catch {
                            Napier.e(tag: tag, message: "Failed to update live activity: \(error)")
                        }
                    }
                }
                .onChange(of: "\(viewModel.data?.data.state ?? "")\(instanceId)") { _, _ in
                    WidgetCenter.shared.reloadAllTimelines()
                }
                .onAppear {
                    // User signed in, this is always added
                    Napier.i(tag: tag, message: "Starting live acitivty management")
                }
                .onDisappear {
                    Napier.i(tag: tag, message: "Stopped live acitivty management")
                }
        }
    }
}

private class ManagingLiveActivityViewModel: BaseViewModel {
    var currentCore: ManagingLiveActivityViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published private(set) var data: DataWithThumbnail? = nil
    
    func createCore(instanceId: String) -> ManagingLiveActivityViewModelCore {
        return ManagingLiveActivityViewModelCore(instanceId: instanceId)
    }
    
    func publish(core: ManagingLiveActivityViewModelCore) {
        core.state.asPublisher()
            .sink { (data: ManagingLiveActivityViewModelCore.PrintActivityData?) in
                self.data = data.flatMap {
                    DataWithThumbnail(
                        thumbnailUrl: data?.thumbnailUrl,
                        data: PrintActivityAttributes.Data($0),
                        colors: $0.colors.toStruct()
                   )
                }
            }
            .store(in: &bag)
    }
    
    func clearData() {
        data = nil
    }
}

private struct DataWithThumbnail: Equatable {
    var thumbnailUrl: String?
    var data: PrintActivityAttributes.Data
    var colors: ColorsStruct
}

private extension PrintActivityAttributes.Data {
    init(_ data: ManagingLiveActivityViewModelCore.PrintActivityData) {
        self.init(
            fileName: data.fileName,
            state: PrintActivityAttributes.State(data.state).string,
            progress: Int(data.progress),
            sourceTime: Int(data.sourceTime.toEpochMilliseconds()),
            timeLeft: data.timeLeft?.intValue,
            printTime: data.printTime?.intValue
        )
    }
}

private extension PrintActivityAttributes.State {
    init(_ state: ManagingLiveActivityViewModelCore.State) {
        switch state {
        case .paused: self = .paused
        case .printing: self = .printing
        case .idle: self = .idle
        default: do {
            Napier.e(tag: "[LA] PrintActivity.State", message: "Missing enum value \(state)!")
            self = .printing
        }
        }
    }
}

extension View {
    func manageLiveActivity() -> some View {
        modifier(ManagingLiveActivity())
    }
}

