//
//  MenuAnimation.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct MenuAnimationView: View {
    
    var octoAnimationName: String
    var octoAnimationNameDark: String
    @Environment(\.colorScheme) private var colorScheme
    
    var body: some View {
        let animationName = colorScheme == .dark ? octoAnimationNameDark : octoAnimationName
        SimpleLottieView(lottieFile: animationName)
            .aspectRatio(586/256, contentMode: .fit)
            .padding([.leading, .trailing], -OctoTheme.dimens.margin12)
            .padding(.bottom, OctoTheme.dimens.margin2)
            .id(animationName)
    }
}

struct MenuAnimation_Previews: PreviewProvider {
    static var previews: some View {
        MenuAnimationView(
            octoAnimationName: "octo-support",
            octoAnimationNameDark: "octo-support-dark"
        )
    }
}
