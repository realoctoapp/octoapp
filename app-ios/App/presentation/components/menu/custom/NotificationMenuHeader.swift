//
//  NotificationMenuHeader.swift
//  OctoApp
//
//  Created by Christian on 08/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct NotificationMenuHeader: View {
    
    @Environment(\.instanceCompanionRunning) private var companionRunning
    @State private var hasPermission: Bool = false
    
    var body: some View {
        NotificationMenuHeaderContent(
            hasPlugin: companionRunning == true,
            hasPermission: hasPermission
        )
        .onAppear {
            UNUserNotificationCenter.current().getNotificationSettings { settings in
                hasPermission = settings.authorizationStatus == .authorized
            }
        }
    }
}

private struct NotificationMenuHeaderContent: View {
    
    var hasPlugin: Bool
    var hasPermission: Bool
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            NotificationMenuHeaderItem(
                checked: hasPlugin,
                text: hasPlugin ? "print_notifications_menu___has_plugin"~ : "print_notifications_menu___missing_plugin"~
            )
            NotificationMenuHeaderItem(
                checked: hasPermission,
                text: hasPermission ? "print_notifications_menu___has_permission"~ : "print_notifications_menu___missing_permission"~
            )
        }
        .typographyLabel()
    }
}

private struct NotificationMenuHeaderItem : View {
    
    var checked: Bool
    var text: String
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin01) {
            Image(systemName: checked ? "checkmark.circle.fill" : "x.circle.fill")
                .foregroundColor(checked ? OctoTheme.colors.green : OctoTheme.colors.red)
            Text(text)
        }
    }
}

struct NotificationMenuHeader_Previews: PreviewProvider {
    static var previews: some View {
        NotificationMenuHeaderContent(
            hasPlugin: true,
            hasPermission: false
        )
    }
}
