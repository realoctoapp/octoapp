//
//  FeatureDisabledView.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct SupporterPerkView: View {
    
    var body: some View {
        MenuAnimationView(
            octoAnimationName: "octo-support",
            octoAnimationNameDark: "octo-support-dark"
        )
    }
}

struct SupporterPerkView_Previews: PreviewProvider {
    static var previews: some View {
        SupporterPerkView()
    }
}
