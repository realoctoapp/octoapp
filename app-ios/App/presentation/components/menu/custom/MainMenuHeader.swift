//
//  MainMenuHeader.swift
//  OctoApp
//
//  Created by Christian on 08/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

@MainActor
struct MainMenuHeader: View {
    
    @State private var supportBanner = getBanner()
    @State private var shouldAdvertisePremium = getShouldAdvertisePremium()
        
    var body: some View {
        MainMenuHeaderContent(
            showSupport: shouldAdvertisePremium,
            supportBanner: supportBanner,
            supportButton: MainMenuHeader.getButton()
        )
        .task {
            while !Task.isCancelled && supportBanner != nil {
                try? await Task.sleep(for: .seconds(1))
                supportBanner = MainMenuHeader.getBanner()
            }
        }
        .onReceive(BillingManager.shared.state.asPublisher()) { (_: BillingState) in
            shouldAdvertisePremium = MainMenuHeader.getShouldAdvertisePremium()
        }
    }
    
    static func getShouldAdvertisePremium() -> Bool {
        BillingManager.shared.shouldAdvertisePremium()
    }
    
    static func getBanner() -> String? {
        ConfigUtilsKt.getPurchaseOffers()
            .activeConfig
            .textsWithData
            .launchPurchaseScreenHighlight
            .flatMap { StringResourcesKt.parseHtml($0) as? String }
    }
    
    static func getButton() -> String {
        ConfigUtilsKt.getPurchaseOffers()
            .activeConfig
            .textsWithData
            .launchPurchaseScreenCta
    }
}

private struct MainMenuHeaderContent: View {
        
    var showSupport: Bool
    var supportBanner: String?
    var supportButton: String

    @EnvironmentObject private var menuHost: MenuHostViewModel
    @Environment(\.instanceId) private var instanceId
    @Environment(\.openURL) private var openUrl
    @Environment(\.instanceSystemInfo) private var systemInfo
    
        
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin1) {
            if (showSupport) {
                billingItem
            }
            
            mainItems
        }
    }
    
    var mainItems: some View {
        VStack {
            HStack {
                Button(
                    action: {
                        Task {
                            try await menuHost.pushMenu(subMenu: SettingsMenu())
                        }
                    }
                ) {
                    SimpleMenuItem(
                        title: "main_menu___item_show_settings"~,
                        iconSystemName: MenuIcon.settings.systemIconName,
                        style: .settings,
                        showAsOutline: true,
                        showAsSubMenu: true
                    )
                }
                Button(
                    action: {
                        menuHost.openUrl(url: UriLibrary.shared.getTutorialsUri())
                    }
                ) {
                    SimpleMenuItem(
                        title: "main_menu___item_show_tutorials"~,
                        iconSystemName: MenuIcon.tutorials.systemIconName,
                        style: .neutral,
                        showAsOutline: true,
                        showAsSubMenu: true
                    )
                }
            }
            
            HStack {
                Button(
                    action: {
                        Task {
                            try await menuHost.pushMenu(subMenu: OctoPrintMenu(instanceId: instanceId))
                        }
                    }
                ) {
                    SimpleMenuItem(
                        title: systemInfo.interfaceType.label,
                        iconSystemName: nil,
                        iconName: systemInfo.interfaceType.icon,
                        style: .octoprint,
                        showAsOutline: true,
                        showAsSubMenu: true
                    )
                }
                Button(
                    action: {
                        Task {
                            try await menuHost.pushMenu(subMenu: PrinterMenu(instanceId: instanceId))
                        }
                    }
                ) {
                    SimpleMenuItem(
                        title: "main_menu___item_show_printer"~,
                        iconSystemName: MenuIcon.printer.systemIconName,
                        style: .printer,
                        showAsOutline: true,
                        showAsSubMenu: true
                    )
                }
            }
        }
        .padding(.top, OctoTheme.dimens.margin1)
    }
    
    @ViewBuilder
    var billingItem: some View {
        VStack {
            if let sb = supportBanner {
                Text(sb)
                    .typographyBase()
                    .multilineTextAlignment(.center)
            }
            
            Button(
                action: {
                    OctoAnalytics.shared.logEvent(
                        event: OctoAnalytics.EventPurchaseScreenOpen.shared,
                        params: ["trigger" : "main_menu"]
                    )
                    openUrl(UriLibrary.shared.getPurchaseUri())
                }
            ) {
                SimpleMenuItem(
                    title: supportButton.count > 0 ? supportButton : "main_menu___item_support_octoapp"~,
                    iconSystemName: MenuIcon.support.systemIconName,
                    style: .support,
                    showAsOutline: false,
                    showAsSubMenu: false
                )
            }
        }
        .if(supportBanner != nil) {
            $0.padding([.top, .leading, .trailing], 50)
                .padding([.top, .bottom], OctoTheme.dimens.margin12)
                .background(OctoTheme.colors.menuStyleSupportBackground)
                .padding([.top, .leading, .trailing], -50)
            
        }
    }
}

struct MainMenuHeader_Previews: PreviewProvider {
    static var previews: some View {
        MainMenuHeaderContent(
            showSupport: true,
            supportBanner: "Sale active! Get OctoApp now!!1!!! So much text!!! So long!",
            supportButton: "Get sale now!!"
        )
        .previewDisplayName("Sale")
        .padding()
        .clipped()
        
        MainMenuHeaderContent(
            showSupport: true,
            supportBanner: nil,
            supportButton: ""
        )
        .previewDisplayName("Normal")
        .padding()
        
        MainMenuHeaderContent(
            showSupport: false,
            supportBanner: nil,
            supportButton: ""
        )
        .previewDisplayName("Premium")
        .padding()
    }
}
