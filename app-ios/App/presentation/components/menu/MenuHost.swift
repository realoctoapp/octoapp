//
//  MenuHost.swift
//  OctoApp
//
//  Created by Christian on 02/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

let MenuNoResult = "nothing"

struct MenuHost: View {
    
    var menu: OctoAppBase.Menu
    var menuId: OctoAppBase.MenuId = .other
    var onResult: (String) async -> Void = { e in
        if e != MenuNoResult {
            Napier.i(tag: "MenuHost", message: "Dropped result: \(e)")
        }
    }
    
    @Environment(\.dismiss) private var dismiss
    @StateObject private var viewModel = MenuHostViewModel()
    @State private var path = [MenuReference]()
    private var runningOnMac: Bool { ProcessInfo.processInfo.isiOSAppOnMac }
    
    var body: some View {
        ZStack {
            if let menu = path.last {
                MenuHostScene(menu: menu, staticList: false)
            }
        }
        .environmentObject(viewModel)
        .onChange(of: viewModel.path) { _, p in
            path = p
        }
        .task {
            viewModel.menuId = menuId
            viewModel.closeHandler = { result in
                await onResult(result)
                dismiss()
            }
            if viewModel.path.isEmpty {
                try? await viewModel.pushMenu(subMenu: menu)
            }
        }
        .onDisappear {
            viewModel.destroyAll()
        }
       .errorDialogDisplay()
       .deeplinkNavigation()
       .asResponsiveSheet()
       .observeUiConfiguration()
    }
}

struct EmbeddedMenuHost: View {
    
    var menu: MenuReference
    @Environment(\.dismiss) private var dismiss
    @StateObject private var viewModel = MenuHostViewModel()
    @State private var subMenu: MenuReference? = nil
    @State private var showSubMenu: Bool = false
    
    var body: some View {
        ZStack {
            MenuHostScene(
                menu: menu,
                staticList: true
            )
        }
        .environmentObject(viewModel)
        .onChange(of: viewModel.path) { _, p in
            if !viewModel.path.isEmpty {
                showSubMenu = true
                subMenu = viewModel.path.last
            }
        }
        .onAppear {
            viewModel.menuId = menu.menuId
        }
        .onDisappear {
            viewModel.destroyAll()
        }
        .onChange(of: showSubMenu) { _, show in
            if !show {
                viewModel.path.removeLast()
            }
        }
        .macOsCompatibleSheet(isPresented: $showSubMenu) {
            if let menu = subMenu {
                MenuHost(menu: menu.reference, menuId: menu.menuId)
            }
        }
    }
}

private struct MenuHostScene : View {
    
    var menu: MenuReference
    var staticList: Bool
    
    @Environment(\.openURL) private var openUrl
    @EnvironmentObject private var menuHost: MenuHostViewModel
    @Namespace private var animation

    private var innerOpenUrl: OpenURLAction {
        return OpenURLAction { url in
            if !menu.reference.onUrlOpened(menuHost: menuHost, url: url.absoluteString) {
                openUrl(url)
            }
            
            return OpenURLAction.Result.handled
        }
    }
    
    var body: some View {
        MenuView(
            menu: menu,
            staticList: staticList
        )
        .environment(\.openURL, innerOpenUrl)
        .onChange(of: menuHost.urlEvent) { _, e in
            if let u = e?.url {
                openUrl(u)
            }
        }
    }
}

class MenuHostViewModel : ObservableObject, OctoAppBase.MenuHost {
    
    @MainActor @Published fileprivate var path : [MenuReference] = []
    @MainActor @Published fileprivate var urlEvent: UrlEvent? = nil
    @MainActor  var hasBackStack : Bool { path.count > 1 }
    var reloadAction: ((MenuReference) async throws -> Void)? = nil
    var closeHandler: (String) async -> Void = { _ in }
    fileprivate var menuId: MenuId = .other
    
    func openUrl(url: Ktor_httpUrl) {
        Task {
            await MainActor.run {
                urlEvent = UrlEvent(url: url.description(), date: Date())
            }
        }
    }
    
    func pushMenu(subMenu: OctoAppBase.Menu) async throws {
        await MainActor.run {
            path.append(MenuReference(reference: subMenu, menuId: menuId))
        }
    }
    
    func popMenu() async throws {
        await MainActor.run {
            let removed = path.removeLast()
            try? removed.reference.onDestroy()
        }
    }
    
    func reloadMenu() async throws {
        guard let menu = await path.last else { return }
        try await reloadAction?.self(menu)
    }
    
    func closeMenu(result: String?) async throws {
        await closeHandler(result ?? MenuNoResult)
    }
    
    @MainActor
    fileprivate func destroyAll() {
        for menu in path {
            try? menu.reference.onDestroy()
        }
    }
}


struct MenuHost_Previews: PreviewProvider {
    @State static var yes = true
    
    static var previews: some View {
        Color.clear.macOsCompatibleSheet(isPresented: $yes) {
            MenuHost(menu: AutoConnectPrinterInfoMenu(), menuId: .other)
        }
        .previewDisplayName("Simple")
        
        Color.clear.macOsCompatibleSheet(isPresented: $yes) {
            MenuHost(menu: PreviewMenuItems.shared.MenuWithNavigation, menuId: .other)
        }
        .previewDisplayName("Navigation")
    }
}

fileprivate struct UrlEvent : Equatable {
    var url: String
    var date: Date
}
