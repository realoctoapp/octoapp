//
//  MenuItem.swift
//  OctoApp
//
//  Created by Christian on 25/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import LottieUI

private let strokeWidth: CGFloat = 1

struct SimpleMenuItem : View {
    private let strokeWidth: CGFloat = 1
    
    var title: String
    var iconSystemName: String?
    var iconName: String? = nil
    var style: MenuItemStyle
    var showAsOutline: Bool = false
    var showAsSubMenu: Bool = false
    var slim: Bool = false
    var descriptionText: String? = nil
    
    var body: some View {
        VStack {
            InternalMenuItem(
                title: title,
                iconSystemName: iconSystemName,
                iconName: iconName,
                style: style,
                showAsOutline: showAsOutline,
                slim: slim
            ) {
                ZStack {
                    if showAsSubMenu {
                        SubMenuDecoration(style: style)
                    }
                }
            }
            
            if let description = descriptionText {
                Description(text: description)
            }
        }
    }
}

private struct InternalMenuItem<Decoration: View> : View {
    
    var title: String
    var iconSystemName: String?
    var iconName: String? = nil
    var style: MenuItemStyle
    var showAsOutline: Bool = false
    var loading: Bool = false
    var success: Int = 0
    var badgeCount: Int = 0
    var iconColorOverride: HexColor? = nil
    var pinned: Bool = false
    var slim: Bool = false
    var decoration: () -> Decoration
    
    var body: some View {
        HStack(spacing: OctoTheme.dimens.margin1) {
            MenuItemIcon(
                loading: loading,
                iconSystemName: iconSystemName,
                iconName: iconName,
                style: style,
                success: success,
                iconColorOverride: iconColorOverride,
                pinned: pinned,
                badgeCount: badgeCount,
                slim: slim
            )
            
            Text(title)
                .foregroundColor(OctoTheme.colors.darkText)
                .typographyButton(small: true)
                .lineLimit(1)
                .opacity(loading ? 0.33 : 1)
                .padding([.top, .bottom], slim ? OctoTheme.dimens.margin01 : OctoTheme.dimens.margin1)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            decoration()
        }
        .frame(maxWidth: .infinity)
        .accentColor(style.foreground)
        .if(showAsOutline) { $0.background(Capsule().strokeBorder(style.foreground, lineWidth: strokeWidth)) }
        .if(!showAsOutline) { $0.background(Capsule().fill(style.background)) }
    }
}

@MainActor
struct MenuItem: View {
    
    var item: MenuItemReference
    @State private var loading = false
    @State private var secondaryLoading = false
    @State private var successCounter = 0
    
    @State private var showInput = false
    @State private var inputValue = ""
    
    @State private var showDocumentPicker = false
    
    @State private var showConfirmation = false
    @State private var confirmationMessage = ""
    @State private var confirmationAction = ""
    @State private var confirmationForSecondary = false
    
    @EnvironmentObject private var menuHost: MenuHostViewModel
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    
    var body: some View {
        content
            .confirmationDialog(
                LocalizedStringKey(confirmationMessage),
                isPresented: $showConfirmation,
                actions: {
                    Button(
                        role: .destructive,
                        action: { performClick(secondary: confirmationForSecondary, confirmed: true)},
                        label: { Text(confirmationAction) }
                    )
                },
                message: { Text(LocalizedStringKey(confirmationMessage)) }
            )
            .macOsCompatibleSheet(isPresented: $showInput) {
                if let i = item.reference as? InputMenuItem {
                    MenuValueInput(
                        title: i.inputTitle,
                        value: inputValue,
                        hint: i.inputHint,
                        confirmButton: i.confirmAction,
                        keyboardType: i.keyboardType,
                        validator: { try? await UtilsKt.onValidateInputSave(i, input: $0) },
                        action: { input, _, _ in handleInput(input: input) }
                    )
                }
            }
            .documentPicker(
                isPresented: $showDocumentPicker,
                onCancel: {
                    loading = false
                },
                onDocumentsPicked: { urls in
                    if let url = urls.first {
                        handleFileSelected(url: url)
                    } else {
                        loading = false
                    }
                }
            )
            .animation(.default, value: loading || secondaryLoading)
    }
    
    private var content: some View {
        VStack {
            ZStack(alignment: .trailing) {
                HStack(spacing: OctoTheme.dimens.margin01) {
                    primaryButton
                    secondaryButton
                }
                .opacity(item.enabled  ? 1 : 0.5)
                featureDisabled
            }
            descroptionText
        }
    }
    
    @ViewBuilder
    private var descroptionText: some View {
        if let d = item.reference.description_ {
            Description(text: d)
        }
    }
    
    @ViewBuilder
    private var featureDisabled: some View {
        if item.featureDisabled {
            DisabledFeatureDecoration(style: item.reference.style).onTapGesture {
                Task {
                    try await menuHost.pushMenu(subMenu: SupporterPerkMenu(featureName: item.reference.title))
                }
            }
        }
    }
    
    @ViewBuilder
    private var primaryButton: some View {
        Menu(
            content: { primaryButtonContent },
            label: { label },
            primaryAction: { performClick() }
        )
    }
    
    @ViewBuilder
    private var secondaryButton: some View {
        if let i = item.reference.secondaryButtonIcon {
            Button (
                action: { performClick(secondary: true) }
            ) {
                MenuItemIcon(
                    loading: secondaryLoading,
                    iconSystemName: i.systemIconName,
                    style: item.reference.style,
                    success: 0
                )
            }
        }
    }
    
    @ViewBuilder
    private var primaryButtonContent: some View {
        if item.reference.canBePinned {
            PinnedStateMenu(itemId: item.id) {
                successCounter += 1
            }
        } else {
            Text("menu_controls_cant_pin"~)
        }
    }
    
    private var label: some View {
        InternalMenuItem(
            title: item.reference.title,
            iconSystemName: item.reference.icon.systemIconName,
            style: item.reference.style,
            loading: loading,
            success: successCounter,
            badgeCount: item.badgeCount,
            iconColorOverride: item.reference.iconColorOverwrite,
            pinned: item.pinned
        ) {
            ZStack {
                if item.featureDisabled {
                    // No decoration for disabled
                } else if let i = item.reference as? ToggleMenuItem {
                    ToggleDecoration(item: i)
                } else if item.reference.rightDetail != nil {
                    RightDetailDecoration(item: item)
                } else if item.reference.showAsSubMenu {
                    SubMenuDecoration(style: item.reference.style)
                }
            }
        }
    }
    
    private func performClick(secondary: Bool = false, confirmed: Bool = false) {
        guard !loading && !secondaryLoading else { return }
        guard !item.featureDisabled else {
            Task {
                try await menuHost.pushMenu(subMenu: SupporterPerkMenu(featureName: item.reference.title))
            }
            return
        }
        guard item.enabled else { return }
        
        if let confirmedItem = item.reference as? ConfirmedMenuItem {
            let isConfigured = !secondary && confirmedItem.confirmationMessage != nil && confirmedItem.confirmationPositiveAction != nil
            
            let isConfiguredSecondary = secondary && confirmedItem.secondaryConfirmationMessage != nil && confirmedItem.secondaryConfirmationPositiveAction != nil
            
            let needsConfirmation = secondary && isConfiguredSecondary || !secondary && isConfigured
            
            if needsConfirmation {
                guard (confirmed || confirmedItem.canSkipConfirmation) && (isConfigured || isConfiguredSecondary) else {
                    confirmationAction = (secondary ? confirmedItem.secondaryConfirmationPositiveAction : confirmedItem.confirmationPositiveAction) ?? "???"
                    confirmationMessage = (secondary ? confirmedItem.secondaryConfirmationMessage : confirmedItem.confirmationMessage) ?? "???"
                    confirmationForSecondary = secondary
                    showConfirmation = true
                    return
                }
            }
        }
        
        secondaryLoading = secondary
        loading = !secondary
        
        Task {
            if item.reference is FilePickerMenuItem {
                showDocumentPicker = true
            } else if let inputItem = item.reference as? InputMenuItem, !secondary {
                inputValue = (try? await inputItem.getCurrentValue()) ?? ""
                showInput = true
                loading = false
                secondaryLoading = false
            } else {
                var error: KotlinThrowable? = nil
                
                if secondary {
                    error = try await UtilsKt.onSecondaryClickedSafe(item.reference, menuHost: menuHost)
                } else {
                    error = try await UtilsKt.onClickedSafe(item.reference, menuHost: menuHost)
                }
                
                if let e = error {
                    orchestrator.postError(error: e)
                } else if item.reference.showSuccess {
                    successCounter += 1
                }
                
                loading = false
                secondaryLoading = false
            }
        }
    }
    
    private func handleInput(input: String) {
        secondaryLoading = false
        loading = true
        
        Task {
            if let inputItem = item.reference as? InputMenuItem {
                if let error = try await UtilsKt.onNewInputSafe(
                    inputItem,
                    menuHost: menuHost,
                    input: input
                ) {
                    orchestrator.postError(error: error)
                } else {
                    successCounter += 1
                }
            }
            
            loading = false
            secondaryLoading = false
        }
    }
    
    private func handleFileSelected(url: URL) {
        secondaryLoading = false
        loading = true
        
        Task {
            let secureScope = url.startAccessingSecurityScopedResource()
            defer {
                if secureScope {
                    url.stopAccessingSecurityScopedResource()
                }
            }
            
            if let inputItem = item.reference as? FilePickerMenuItem {
                if let error = try await UtilsKt.onFileSelectedSafe(
                    inputItem,
                    menuHost: menuHost,
                    path: url.path(percentEncoded: false)
                ) {
                    orchestrator.postError(error: error)
                } else {
                    successCounter += 1
                }
            }
            
            loading = false
            secondaryLoading = false
        }
    }
}

private struct Description: View {
    var text: String
    
    var body: some View {
        Text(text)
            .fixedSize(horizontal: false, vertical: true)
            .typographyLabel()
            .multilineTextAlignment(.center)
            .padding(.top, OctoTheme.dimens.margin0)
            .padding(.bottom, OctoTheme.dimens.margin12)
    }
}

private struct PinnedStateMenu: View {
    
    var itemId: String
    var onSuccess: () -> Void
    @StateObject private var viewModel = MenuItemViewModel()
    @EnvironmentObject private var menuHost: MenuHostViewModel
    
    var body: some View {
        let state = viewModel.getPinnedState(itemId: itemId).filter { $0.0 != .widget }
        Text(String(format: "menu_controls___pin_to_x"~, ""))
        ForEach(state, id: \.0.ordinal) { item in
            PinnedStateMenuToggle(menuId: item.0, pinned: item.1) { _, pinned in
                Task {
                    viewModel.setPinned(itemId: itemId, menuId: item.0, pinned: pinned)
                    try await menuHost.reloadMenu()
                    onSuccess()
                }
            }
        }
    }
}

private struct PinnedStateMenuToggle: View {
    
    var menuId: MenuId
    @State var pinned: Bool
    var onChange: (Bool, Bool) -> Void
    
    var body: some View {
        Toggle(menuId.label, isOn: $pinned)
            .onChange(of: pinned, onChange)
    }
}

@MainActor
private struct MenuItemIcon: View {
    
    var loading: Bool
    var iconSystemName: String?
    var iconName: String? = nil
    var style: MenuItemStyle
    var success: Int
    var iconColorOverride: HexColor?
    var pinned: Bool = false
    var badgeCount: Int = 0
    var slim: Bool = false
    
    private var parsedColorOverride: Color? {
        if let co = iconColorOverride?.color {
            return co
        } else {
            return nil
        }
    }
    
    @Environment(\.colorScheme) var colorScheme
    @State private var showSuccess = false
    private var iconSize : CGFloat { slim ? 36 : 40 }
    
    var body: some View {
        if iconName != nil || iconSystemName != nil {
            ZStack {
                ZStack {
                    if let icon = iconName {
                        Image(icon).renderingMode(.template)
                    } else if let icon = iconSystemName {
                        Image(systemName: icon)
                    }
                    
                    if showSuccess {
                        LottieView(colorScheme == .dark ? "success-check-dark" : "success-check")
                        .play(true)
                        .scaleEffect(2)
                        .frame(width: iconSize, height: iconSize)
                        .id(success)
                    }
                    
                    ZStack {
                        if badgeCount > 0 {
                            Image(systemName: "\(min(9, badgeCount)).circle.fill")
                                .font(.system(size: 13))
                                .foregroundColor(OctoTheme.colors.red)
                                .padding(-1)
                                .background(Circle().fill(.white))
                                .padding(1)
                        } else if pinned {
                            Image(systemName: "pin.fill")
                                .font(.system(size: 8))
                                .padding(3)
                                .background(Circle().fill(.black.opacity(0.2)))
                        }
                    }
                    .foregroundColor(OctoTheme.colors.textColoredBackground)
                    .frame(minWidth: iconSize, minHeight: iconSize, alignment: .bottomTrailing)
                    .offset(x: 3)
                }
                .foregroundColor(parsedColorOverride ?? style.foreground)
                .font(.system(size: 20))
                .opacity(loading ? 0 : 1)
                .padding(OctoTheme.dimens.margin01)
                .aspectRatio(1, contentMode: .fit)
                .frame(minWidth: iconSize, minHeight: iconSize)
                .if(iconColorOverride != nil) { $0.background(Circle().fill(OctoTheme.colors.windowBackground.opacity(0.5))) }
                .padding(strokeWidth)
                .background(Circle().fill(style.background))
                
                if loading {
                    ProgressView()
                        .tint(OctoTheme.colors.normalText)
                }
            }
            .frame(width: iconSize, height: iconSize)
            .animation(.default, value: pinned)
            .onChange(of: success) {
                showSuccess = true
            }
        } else {
            Color.clear.frame(width: 0, height: iconSize)
        }
    }
}

private struct ToggleDecoration : View {
    
    var item: ToggleMenuItem
    @State private var isOn: Bool? = nil
    private var backgroundColor : Color { isOn == true ? item.style.foreground : OctoTheme.colors.darkText.opacity(0.15) }
    
    var body: some View {
        HStack {
            if let on = isOn {
                if on { Spacer() }
                Circle()
                    .fill(.white)
                    .padding(OctoTheme.dimens.margin0)
                    .shadow(radius: on ? 0.5 : 0)
                if !on { Spacer() }
            }
        }
        .frame(width: 50, height: 30)
        .background(Capsule().fill(backgroundColor))
        .onReceive(item.stateEnum.asPublisher()) { (state: ToggleMenuItemState) in
            isOn = state == .on
        }
        .padding(.trailing, OctoTheme.dimens.margin1)
        .animation(.default, value: isOn)
    }
}

private struct RightDetailDecoration : View {
    
    var item: MenuItemReference
    @State private var rightDetail: String? = nil
    
    var body: some View {
        Text(rightDetail ?? "")
            .multilineTextAlignment(.trailing)
            .typographySectionHeader()
            .padding(.trailing, OctoTheme.dimens.margin12)
            .animation(.default, value: rightDetail)
            .onReceive(item.reference.rightDetail!.asPublisher()) { (detailText: NSString) in
                rightDetail = detailText as String
            }
    }
}

private struct DisabledFeatureDecoration : View {
    
    var style: MenuItemStyle
    
    var body: some View {
        Image(systemName: "heart.fill")
            .aspectRatio(1, contentMode: .fit)
            .font(.system(size: 14))
            .padding(OctoTheme.dimens.margin01)
            .background(Circle().fill(style.background))
            .padding(.trailing, OctoTheme.dimens.margin1)
            .foregroundColor(style.foreground)
    }
}

struct SubMenuDecoration : View {
    
    var style : MenuItemStyle
    
    var body: some View {
        Image(systemName: "chevron.forward")
            .font(.system(size: 20))
            .aspectRatio(1, contentMode: .fit)
            .foregroundColor(style.foreground)
            .padding([.top, .bottom, .trailing], OctoTheme.dimens.margin1)
    }
}

private class MenuItemViewModel : ObservableObject {
    
    private let core = MenuItemViewModelCore()
    
    func setPinned(itemId: String, menuId: MenuId, pinned: Bool) {
        core.setItemPinned(itemId: itemId, menuId: menuId, pinned: pinned)
    }
    
    func getPinnedState(itemId: String) -> [(MenuId, Bool)] {
        return core.getPinningState(itemId: itemId).map { ($0.key, $0.value.boolValue) }
    }
}

struct MenuItem_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 5) {
            SimpleMenuItem(
                title: "Explore plugins",
                iconSystemName: nil,
                style: .printer
            )
            
            SimpleMenuItem(
                title: "Explore plugins",
                iconSystemName: nil,
                iconName: "mainsail_menu",
                style: .printer
            )
            
            SimpleMenuItem(
                title: "Explore plugins",
                iconSystemName: "puzzlepiece.extension.fill",
                style: .printer,
                showAsOutline: true
            )
            
            SimpleMenuItem(
                title: "Explore plugins",
                iconSystemName: "puzzlepiece.extension.fill",
                style: .printer,
                showAsOutline: false
            )
            
            SimpleMenuItem(
                title: "Explore plugins",
                iconSystemName: "puzzlepiece.extension.fill",
                style: .printer,
                slim: true
            )
            
            NavigationLink(value: "") {
                SimpleMenuItem(
                    title: "Explore plugins",
                    iconSystemName: "puzzlepiece.extension.fill",
                    style: .printer,
                    showAsSubMenu: true
                )
            }
        }
        .previewDisplayName("Simple items")
        
        VStack(spacing: 5) {
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.Normal))
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.Toggle))
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.Secondary))
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.RevolvingOptions))
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.Confirmed))
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.Input, featureDisabled: true))
            MenuItem(item: MenuItemReference(reference: PreviewMenuItems.shared.Input, pinned: true))
        }
        .environmentObject(MenuHostViewModel())
        .previewDisplayName("Complex items")
        
    }
}
