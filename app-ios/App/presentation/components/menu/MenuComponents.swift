//
//  MenuComponents.swift
//  OctoApp
//
//  Created by Christian on 11/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct MenuLoading : View {
    
    var title: String
    
    var body: some View {
        VStack {
            MenuTitle(title: title)
            ProgressView()
                .padding(OctoTheme.dimens.margin5)
        }
    }
}

struct MenuLoaded : View {
    
    var state: MenuLoadedState
    var staticList: Bool
    
    @State private var scrollViewContentSize: CGSize = .zero
    @State private var scrollViewContentHeight: CGFloat = 0
    @State private var headerContentSize: CGSize = .zero
    @State private var scrolledToTop = true
    private let maxHeight : CGFloat = 600
    
    var body: some View {
        ZStack(alignment: .top) {
            if staticList {
                MenuLoadedInner(
                    state: state,
                    headerContentSize: headerContentSize,
                    staticList: true,
                    scrollViewContentSize: $scrollViewContentSize
                )
            } else {
                ScrolledToTopScrollView(scrolledToTop: $scrolledToTop) {
                    MenuLoadedInner(
                        state: state,
                        headerContentSize: headerContentSize,
                        staticList: false,
                        scrollViewContentSize: $scrollViewContentSize
                    )
                }
                .frame(maxHeight: scrollViewContentHeight > maxHeight ? maxHeight : scrollViewContentHeight)
                
                
                VStack(spacing: OctoTheme.dimens.margin01) {
                    MenuTitle(title: state.title, headerViewType: state.headerViewType)
                    MenuSubtitle(text: state.subtitle)
                    
                    if let viewType = state.viewType {
                        MenuCustomView(type: viewType)
                    }
                }
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                .padding(.bottom, OctoTheme.dimens.margin12)
                .readSize { size in
                    headerContentSize = size
                }
                .padding(.top, OctoTheme.dimens.margin3)
                .padding([.leading, .trailing], OctoTheme.dimens.margin3)
                .background(.ultraThinMaterial.opacity(scrolledToTop ? 0 : 1))
                .padding([.leading, .trailing], -OctoTheme.dimens.margin3)
                .offset(y: -OctoTheme.dimens.margin3)
                .animation(.spring(), value: scrolledToTop)
            }
        }
        .onChange(of: scrollViewContentSize.height) { old, new in
            withAnimation {
                scrollViewContentHeight = new
            }
        }
    }
}

private struct MenuLoadedInner : View {
    
    var state: MenuLoadedState
    var headerContentSize: CGSize
    var staticList: Bool
    @Namespace private var animation
    @Binding var scrollViewContentSize: CGSize

    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            ForEach(state.items) { group in
                VStack(spacing: OctoTheme.dimens.margin0) {
                    ForEach(group) { item in
                        MenuItem(item: item)
                            .id(item.reference.itemId)
                            .transition(.opacity)
                    }
                }
            }
            
            MenuBottomText(text: state.bottomText)
        }
        .if(!staticList) {
            $0.padding(.bottom, OctoTheme.dimens.margin12)
                .padding(.top, headerContentSize.height)
                .padding(.top, OctoTheme.dimens.margin1)
                .padding([.leading, .trailing], OctoTheme.dimens.margin12)
                .readSize { size in
                    // We need a global animation here to resize the sheet, use withAnimation and copy state
                    withAnimation {
                        scrollViewContentSize = size
                    }
                }
        }
    }
}

struct MenuTitle: View {
    
    var title: String
    var headerViewType: String? = nil
    @EnvironmentObject private var menuHost: MenuHostViewModel
    @Namespace private var animation
    
    var body: some View {
        if let headerViewType = headerViewType {
            MenuCustomView(type: headerViewType)
                .matchedGeometryEffect(id: "title", in: animation)
            
        } else if !title.isEmpty {
            ZStack(alignment: .leading) {
                OctoAsyncIconButton(icon: "chevron.left") {
                    if menuHost.hasBackStack {
                        try? await menuHost.popMenu()
                    }
                }
                .opacity(menuHost.hasBackStack ? 1 : 0)
                
                Text(title)
                    .fixedSize(horizontal: false, vertical: true)
                    .lineLimit(2)
                    .typographyTitle()
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity)
                    .padding([.leading, .trailing], OctoTheme.dimens.margin3)
                    .matchedGeometryEffect(id: "title", in: animation)
            }
        }
    }
}

struct MenuEmpty : View {
    
    var state: MenuEmptyState
    @EnvironmentObject private var menuHost: MenuHostViewModel
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin2) {
            MenuTitle(title: state.title)
            MenuSubtitle(text: state.subtitle)
            
            if let a = state.animation {
                MenuAnimationView(
                    octoAnimationName: a.animationName,
                    octoAnimationNameDark: a.animationNameDark
                )
            }
            
            if let text = state.actionText {
                OctoButton(text: text) {
                    state.action(menuHost)
                }
            }
        }
        .padding([.leading, .trailing, .bottom], OctoTheme.dimens.margin12)
    }
}

struct MenuSubtitle : View {
    
    var text: String?
    @Namespace private var animation
    
    var body: some View {
        if let s = text {
            Text(LocalizedStringKey(s))
                .typographyBase()
                .multilineTextAlignment(.center)
                .matchedGeometryEffect(id: "subtitle", in: animation)
        }
    }
}

struct MenuBottomText : View {
    
    var text: String?
    @Namespace private var animation
    
    var body: some View {
        if let b = text, text != "" {
            Text(LocalizedStringKey(b))
                .typographyLabel()
                .multilineTextAlignment(.center)
                .padding(.top, OctoTheme.dimens.margin12)
                .matchedGeometryEffect(id: "bottom", in: animation)
            
        } else {
            Spacer(minLength: 1)
                .frame(maxWidth: .infinity)
        }
    }
}

extension [MenuItemReference] : Identifiable {
    public var id: String { map { $0.id }.joined() }
}
