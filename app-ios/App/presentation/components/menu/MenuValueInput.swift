//
//  MenuValueInput.swift
//  OctoApp
//
//  Created by Christian on 10/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct MenuValueInput: View {
    
    var title: String
    var value: String
    var value2: String? = nil
    var value3: String? = nil
    var hint: String
    var hint2: String? = nil
    var hint3: String? = nil
    var confirmButton: String = "set"~
    var keyboardType: InputMenuItemKeyboardType
    var keyboardType2: InputMenuItemKeyboardType? = nil
    var keyboardType3: InputMenuItemKeyboardType? = nil
    var validator: (String) async -> String?
    var validator2: ((String) async -> String?)? = nil
    var validator3: ((String) async -> String?)? = nil
    var action: (String, String, String) async throws -> Void
    var cancelled: () -> Void = {}
    
    @State private var editedValue1 = ""
    @State private var editedValue2 = ""
    @State private var editedValue3 = ""
    @State private var errorText: String = ""
    @State private var showError = false
    @State private var completed = false
    
    @Environment(\.dismiss) private var dismiss
    
    
    var body: some View {
        NavigationStack {
            VStack {
                OctoTextField(
                    placeholder: hint,
                    labelActiv: LocalizedStringKey(hint),
                    input: $editedValue1,
                    autoFocus: true
                )
                .keyboardType(swiftKeyboardType(keyboardType: keyboardType))
                
                if let hint2 = hint2, let keyboardType2 = keyboardType2  {
                    OctoTextField(
                        placeholder: hint2,
                        labelActiv: LocalizedStringKey(hint2),
                        input: $editedValue2,
                        autoFocus: false
                    )
                    .keyboardType(swiftKeyboardType(keyboardType: keyboardType2))
                }
                
                if let hint3 = hint3,  let keyboardType3 = keyboardType3  {
                    OctoTextField(
                        placeholder: hint3,
                        labelActiv: LocalizedStringKey(hint3),
                        input: $editedValue3,
                        autoFocus: false
                    )
                    .keyboardType(swiftKeyboardType(keyboardType: keyboardType3))
                }
                
                OctoAsyncButton(text: confirmButton) {
                    let error1 = await validator(editedValue1)
                    let error2 = await validator2?(editedValue2)
                    let error3 = await validator3?(editedValue3)
                    
                    if let error = error1 ?? error2 ?? error3  {
                        errorText = error
                        showError = true
                    } else {
                        try await action(editedValue1, editedValue2, editedValue3)
                        completed = true
                        dismiss()
                    }
                }
                .padding(.top, OctoTheme.dimens.margin2)
                Spacer()
            }
            .alert(LocalizedStringKey(errorText), isPresented: $showError) {
                Button("OK"~, role: .cancel) { showError = false }
            }
            .padding(OctoTheme.dimens.margin12)
            .onAppear { 
                editedValue1 = value
                editedValue2 = value2 ?? ""
                editedValue3 = value3 ?? ""
            }
            .navigationTitle(title)
            .toolbar {
                ToolbarItemGroup(placement: .cancellationAction) {
                    Button("cancel"~) { dismiss() }
                }
            }
            .onDisappear {
                if !completed {
                    cancelled()
                }
            }
        }
    }
    
    func swiftKeyboardType(keyboardType: InputMenuItemKeyboardType) -> UIKeyboardType {
        switch keyboardType {
        case .normal: return .default
        case .numbers: return .decimalPad
        case .numbersnegative: return .numbersAndPunctuation
        case .url: return .URL
        default: return .default
        }
    }
}

struct MenuValueInput_Previews: PreviewProvider {
    static var previews: some View {
        MenuValueInput(
            title: "Frame rate",
            value: "15",
            hint: "Some value",
            keyboardType: .numbers,
            validator: { $0.isEmpty ? "Enter a value" : nil },
            action: { _, _, _ in }
        )
    }
}
