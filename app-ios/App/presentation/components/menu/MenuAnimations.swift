//
//  MenuAnimations.swift
//  OctoApp
//
//  Created by Christian on 09/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

extension MenuAnimation {
    var animationName: String {
        switch self {
//        case .notificiations:
        case .materials: return "octo-material"
        case .powerdevices: return "octo-power"
        default: return "octo-wave"
        }
    }
    
    var animationNameDark: String {
        switch self {
//        case .notificiations:
        case .materials: return "octo-material-dark"
        case .powerdevices: return "octo-power-dark"
        default: return "octo-wave-dark"
        }
    }
}
