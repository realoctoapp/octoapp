//
//  ResponsiveSheetWrapper.swift
//
//  Created by Bacem Ben Afia on 04/08/2022.
//  ba.bessem@gmail.com
import SwiftUI
import UIKit

//MARK: - ViewModifier
fileprivate struct ResponsiveSheetWrapper: ViewModifier {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    func body(content: Content) -> some View {
        /// check device type (ipad sheet are centred / iPhone sheet pinned to bottom )
        ///  IPAD mode is ignored. Reenable at will, but we want the same behaviour on all devices
        if ProcessInfo.processInfo.isiOSAppOnMac {
            ZStack {
                Color(UIColor.systemBackground.withAlphaComponent(0.01))
                    .onTapGesture {
                        // tap outside the view to dismiss
                        presentationMode.wrappedValue.dismiss()
                    }
                content
                /// redesign the content
                .asResponsiveSheetContent()
            }
            /// remove system background
            .clearSheetSystemBackground()
            .edgesIgnoringSafeArea(.all)
        } else {
            ZStack (alignment: .bottom){
                Color(UIColor.systemBackground.withAlphaComponent(0.01))
                    .onTapGesture {
                        // tap outside the view to dismiss
                        presentationMode.wrappedValue.dismiss()
                    }
                    .edgesIgnoringSafeArea(.all)
                VStack {
                    /// the small thumb for bottom sheet native like
                    thumb
                    content
                }
                /// redesign the content
                .asResponsiveSheetContent()
            }
            /// remove system background
            .clearSheetSystemBackground()
            .edgesIgnoringSafeArea(.bottom)
        }
    }
    
    var thumb: some View {
        HStack{
            Spacer()
            Capsule()
                .frame(width: 40, height: 4)
                .foregroundColor(Color(UIColor.systemFill))
            Spacer()
        }
        .padding(.top, 8)
        .padding(.bottom, 8)
    }
}

fileprivate struct ResponsiveSheetContent: ViewModifier {
    /// ResponsiveSheetContent will create the form of a bottom sheet (apply corners radius for both iPad an iPhone sheet)
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @Environment(\.horizontalSizeClass) var horizontalSizeClass

    func body(content: Content) -> some View {
        if horizontalSizeClass == .compact {
            content
                .padding(.bottom, safeAreaInsets.bottom)
                .background(Color(UIColor.systemBackground))
                .cornerRadius(10, corners: [.topLeft, .topRight])
        } else {
            content
                .padding()
                .background(Color(UIColor.systemBackground))
                .cornerRadius(10, corners: [.allCorners])
        }
    }
}

fileprivate struct ClearBackgroundViewModifier: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            .background(ClearBackgroundView())
    }
}

//MARK: - UIViewRepresentable
fileprivate struct ClearBackgroundView: UIViewRepresentable {
    /// The Key
    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        if !ProcessInfo.processInfo.isiOSAppOnMac {
            Task(priority: .high) {
                /// With 16.4 we need to be more diligent , try 10 times with delay
                for _ in 0...20 {
                    await MainActor.run {
                        /// GOD BLESS UI KIT
                        /// Target sheet system background view
                        /// Apply clear Color
                        view.superview?.superview?.backgroundColor = .clear
                    }
                    try? await Task.sleep(for: .milliseconds(2))
                }
            }
        }
        return view
    }

    func updateUIView(_ uiView: UIView, context: Context) {}
}

//MARK: - View Extension
extension View {
    /// Return a formatted View Based on Device Type
    /// if iPad => Centred Alert-Like View
    /// if iPhone => Bottom Sheet View
    /// That's all folks
    func asResponsiveSheet() -> some View {
        self.modifier(ResponsiveSheetWrapper())
    }
    
    fileprivate func asResponsiveSheetContent() -> some View {
        self.modifier(ResponsiveSheetContent())
    }
    
    fileprivate func clearSheetSystemBackground() -> some View {
        self.modifier(ClearBackgroundViewModifier())
    }
}

//MARK: - EnvironmentKey & Values
fileprivate struct SafeAreaInsetsKey: EnvironmentKey {
    static var defaultValue: EdgeInsets {
        let insets = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow})
            .first?
            .safeAreaInsets
        
        return EdgeInsets(
            top: insets?.top ?? 0,
            leading: insets?.left ?? 0,
            bottom: insets?.bottom ?? 0,
            trailing: insets?.right ?? 0
        )
    }
}

fileprivate extension EnvironmentValues {
    var safeAreaInsets: EdgeInsets {
        self[SafeAreaInsetsKey.self]
    }
}

//MARK: - UIEdgeInsets Extension
fileprivate extension UIEdgeInsets {
    var insets: EdgeInsets {
        EdgeInsets(top: top, leading: left, bottom: bottom, trailing: right)
    }
}
