//
//  GcodePreviewLoading.swift
//  OctoApp
//
//  Created by Christian Würthner on 31/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct GcodePreviewLoading : View {
    
    var progress: Float
    
    var body: some View {
        VStack {
            Text("loading"~).typographySubtitle()
            ProgressView(value: progress)
                .frame(maxWidth: 200)
        }
        .tint(OctoTheme.colors.accent)
        .padding(OctoTheme.dimens.margin12)
    }
}


struct GcodePreviewLoading_Previews: PreviewProvider {
    static var previews: some View {
        GcodePreviewLoading(progress: 0)
        GcodePreviewLoading(progress: 0.5)
    }
}
