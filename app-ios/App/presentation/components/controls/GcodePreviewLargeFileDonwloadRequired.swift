//
//  GcodePreviewLargeFileDonwloadRequired.swift
//  OctoApp
//
//  Created by Christian Würthner on 31/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct GcodePreviewLargeFileDonwloadRequired: View {
    
    var downloadSize: Int64
    var onAllowLargeDownload: () -> Void
    
    var body: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            Text("gcode_preview___download_size_exceeded_title"~)
                .typographySubtitle()
            Text("gcode_preview___download_size_exceeded_description"~)
                .typographyLabelSmall()
            OctoButton(
                text: String(format: "download_x"~, downloadSize.asStyleFileSize()),
                small: true,
                clickListener: onAllowLargeDownload
            )
            .padding(.top, OctoTheme.dimens.margin2)
        }
        .padding(OctoTheme.dimens.margin12)
        .multilineTextAlignment(.center)
        
    }
}


struct GcodePreviewLargeFileDonwloadRequired_Previews: PreviewProvider {
    static var previews: some View {
        GcodePreviewLargeFileDonwloadRequired(
            downloadSize: 342234,
            onAllowLargeDownload: {}
        )
    }
}
