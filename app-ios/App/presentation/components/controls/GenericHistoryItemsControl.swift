//
//  HisotryPillsControl.swift
//  OctoApp
//
//  Created by Christian Würthner on 22/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Flow

struct GenericHistoryItemsControl<T : Equatable>: View {
    
    var title: String
    var iconSystemName: String? = nil
    var iconSystemName2: String? = nil
    var supportsEdit: Bool = true
    var iconAction: () async -> Void = {}
    var iconAction2: () async -> Void = {}
    var actionLoading: Bool = false
    var actionLoading2: Bool = false
    var otherText: String
    var options: [GenericHistoryItemGroup<T>]
    var optionSelected: (GenericHistoryItem<T>) async throws -> Void
    var optionLongPressed: (GenericHistoryItem<T>) -> Void
    var otherSelected: () async throws -> Void
        
    @Namespace private var animation
    
    var body: some View {
        ControlsScaffold(
            title: title,
            iconSystemName: iconSystemName,
            iconSystemName2: iconSystemName2,
            supportsEdit: supportsEdit,
            iconAction: iconAction,
            iconAction2: iconAction2,
            actionLoading: actionLoading,
            actionLoading2: actionLoading2
        ) {
            VStack(spacing: OctoTheme.dimens.margin1) {
                ForEach(options) { group in
                    Options(
                        title: group.label,
                        otherText: otherText,
                        options: group.items,
                        optionSelected: optionSelected,
                        optionLongPressed: optionLongPressed,
                        otherSelected: otherSelected,
                        animation: animation
                    )
                }
            }
        }
        .animation(.spring(), value: options)
    }
}

private struct Options<T: Equatable>: View {
    
    var title: String?
    var otherText: String
    var options: [GenericHistoryItem<T>]
    var optionSelected: (GenericHistoryItem<T>) async throws -> Void
    var optionLongPressed: (GenericHistoryItem<T>) -> Void
    var otherSelected: () async throws -> Void
    var animation: Namespace.ID

    var body: some View {
        HStack {
            
            if let text = title {
                Text(text)
                    .typographyFocus()
                    .rotationEffect(.degrees(-90))
                    .fixedSize()
                    .frame(width: 14)
                    .padding([.trailing], OctoTheme.dimens.margin01)
            }
            
            HFlow(
                itemSpacing: OctoTheme.dimens.margin1,
                rowSpacing: OctoTheme.dimens.margin01
            ) {
                ForEach(options) { option in
                    PinnedWrapper(
                        pinned: option.pinned,
                        alternativeBackground: title != nil
                    ) {
                        OctoAsyncButton(
                            text: option.title,
                            type: .primary,
                            small: true,
                            clickListener: { try await optionSelected(option) },
                            longClickListener: { optionLongPressed(option) }
                        )
                    }
                    .matchedGeometryEffect(id: option.id, in: animation)
                }
                
                if title == nil {
                    OctoAsyncButton(
                        text: otherText,
                        type: .secondary,
                        small: true,
                        clickListener: { try await otherSelected() }
                    )
                    .matchedGeometryEffect(id: "other", in: animation)
                }
            }
            
            Spacer()
        }
        .if(title != nil) {
            $0.padding(OctoTheme.dimens.margin12).surface()
        }
    }
}

struct PinnedWrapper<Content: View> : View {
    
    var pinned: Bool
    var alternativeBackground: Bool = false
    var content: () -> Content
    
    var body: some View {
        HStack(spacing: 0) {
            content()
            
            if pinned {
                Image(systemName: "pin.fill")
                    .font(.system(size: 12))
                    .padding(.trailing, OctoTheme.dimens.margin0)
                    .foregroundColor(OctoTheme.colors.accent)
            }
        }
        .background(Capsule().fill(alternativeBackground ? OctoTheme.colors.inputBackgroundAlternative : OctoTheme.colors.inputBackground))
    }
}

struct GenericHistoryItemGroup<T: Equatable>: Identifiable, Equatable {
    var id: String? { label }
    var label: String?
    var items: [GenericHistoryItem<T>]
}

struct GenericHistoryItem<T: Equatable>: Identifiable, Equatable {
    var title: String
    var id: String
    var pinned: Bool
    var data: T
}

struct PillsControl_Previews: PreviewProvider {
    static var previews: some View {
        GenericHistoryItemsControl(
        title: "Some title",
        otherText: "Other option",
        options: [
            GenericHistoryItemGroup(
                label: nil,
                items: [
                    GenericHistoryItem(title: "Option A", id: "A", pinned: true, data: 10),
                    GenericHistoryItem(title: "Option B", id: "B", pinned: false, data: 100),
                    GenericHistoryItem(title: "Option C", id: "C", pinned: false, data: 120)
                ]
            ),
            GenericHistoryItemGroup(
                label: "Second",
                items: [
                    GenericHistoryItem(title: "Option A", id: "A2", pinned: true, data: 10),
                    GenericHistoryItem(title: "Option B", id: "B2", pinned: false, data: 100),
                    GenericHistoryItem(title: "Option C", id: "C2", pinned: false, data: 120)
                ]
            )
           
        ],
        optionSelected: { _ in try await Task.sleep(for: .seconds(1)) },
        optionLongPressed: { _ in },
        otherSelected: { try await Task.sleep(for: .seconds(1)) }
        )
    }
}
