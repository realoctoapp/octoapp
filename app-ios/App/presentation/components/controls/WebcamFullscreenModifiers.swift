//
//  WebcamFullscreenSwipeToDismiss.swift
//  OctoApp
//
//  Created by Christian on 19/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI



private struct WebcamFullscreenSwipeToDismiss: ViewModifier {
    
    @State private var dragProgress: CGFloat = 0
    private let dragProgressThreshold: CGFloat = 200
    var dismiss: () -> Void
    
    func body(content: Content) -> some View {
        content
            .background(.red.opacity(0.00002))
            .offset(x: 0, y: dragProgress)
            .animation(.spring(), value: dragProgress)
            .gesture(
                DragGesture().onChanged { value  in
                    dragProgress = value.location.y - value.startLocation.y
                    if dragProgress > dragProgressThreshold {
                        dismiss()
                    }
                }.onEnded { _ in
                    dragProgress = 0
                }
            )
            .onAppear {
                dragProgress = 0
            }
    }
}

private struct WebcamFullscreenUnlockedRotation: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            #if os(iOS)
            .onAppear { AppDelegate.orientationLock = .all }
            .onDisappear { AppDelegate.orientationLock = AppDelegate.defaultOrientation }
            #endif
    }
}

extension View {
    func webcamFullscreenDragToDismiss(dismiss: @escaping () -> Void) -> some View {
        modifier(WebcamFullscreenSwipeToDismiss(dismiss: dismiss))
    }
    
    func unlockedRotation() -> some View {
        modifier(WebcamFullscreenUnlockedRotation())
    }
}
