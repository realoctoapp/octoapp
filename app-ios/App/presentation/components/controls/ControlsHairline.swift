//
//  ControlsHairline.swift
//  OctoApp
//
//  Created by Christian on 08/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct ControlsHairline: View {
    
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        // In light mode the elements use elevation,
        // so we only need the hairline in dark mode
        if colorScheme == .dark {
            OctoTheme.colors.white
                .opacity(0.2)
                .frame(height: 2)
        }
    }
}

struct ControlsHairline_Previews: PreviewProvider {
    static var previews: some View {
        ControlsHairline()
    }
}
