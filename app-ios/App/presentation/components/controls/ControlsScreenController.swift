//
//  ControlsScreenController.swift
//  OctoApp
//
//  Created by Christian on 10/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase


private struct ControlsScreenController: ViewModifier {
    
    let printing: Bool
    @StateObject private var viewModel = ControlsScreenControllerViewModel()
    
    func body(content: Content) -> some View {
        content
            .onChange(of: printing)  { _, p in updateScreenState(keepOnDuringPrint: viewModel.isKeepScreenOnDuringPrint, printing: p) }
            .onChange(of: viewModel.isKeepScreenOnDuringPrint) { _, p in updateScreenState(keepOnDuringPrint: p, printing: printing) }
            .onAppear { updateScreenState(keepOnDuringPrint: viewModel.isKeepScreenOnDuringPrint, printing: printing) }
            .onDisappear {
                Napier.i(tag: "ControlsScreenController", message: "Reset screen on timer")
                UIApplication.shared.isIdleTimerDisabled = false
            }
    }
    
    private func updateScreenState(keepOnDuringPrint: Bool, printing: Bool) {
        let keepOn = printing && keepOnDuringPrint
        UIApplication.shared.isIdleTimerDisabled = keepOn
        Napier.i(tag: "ControlsScreenController", message: "Keeping screen on while printing: printing=\(printing) keepOnDuringPrint=\(keepOnDuringPrint) keepOn=\(keepOn)")
    }
}

extension View {
    func screenOnWhilePrinting(printing: Bool) -> some View {
        modifier(ControlsScreenController(printing: printing))
    }
}


private class ControlsScreenControllerViewModel : ObservableObject {
    
    @Published var isKeepScreenOnDuringPrint: Bool = false
    private static var preferences: OctoPreferences? { SharedBaseInjector.shared.getOrNull()?.preferences }
   
    init() {
        ControlsScreenControllerViewModel.preferences?.updatedFlow2.asPublisher()
            .map { (prefs: OctoPreferences) in prefs.isKeepScreenOnDuringPrint }
            .assign(to: &$isKeepScreenOnDuringPrint)
    }
}
