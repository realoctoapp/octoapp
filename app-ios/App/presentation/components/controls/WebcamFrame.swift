//
//  WebcamFrame.swift
//  OctoApp
//
//  Created by Christian on 31/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

private struct WebcamFrame: ViewModifier {
    
    var nativeAspectRatio: CGFloat?
    var initialAspectRatio: CGFloat
    var configAspectRatio: CGFloat
    var storeAspectRatio: (CGFloat) -> Void
    @StateObject private var viewModel = ViewModel()
    private var activeAspectRatio: CGFloat { viewModel.useNativeAspectRatio ? (nativeAspectRatio ?? initialAspectRatio) : configAspectRatio }
    
    func body(content: Content) -> some View {
        ZStack {
            content
        }
        .frame(maxWidth: .infinity)
        .aspectRatio(activeAspectRatio, contentMode: .fill)
        .animation(.default, value: activeAspectRatio)
        .surface(color: .special(.black))
        .onChange(of: activeAspectRatio) { _, new in
            // Only store if we have a native aspect ratio set
            if nativeAspectRatio != nil {
                storeAspectRatio(new)
            }
        }
        .contentShape(Rectangle())
        .clipped()
    }
}

extension View {
    func webcamFrame(
        nativeAspectRatio: CGFloat?,
        initialAspectRatio: CGFloat,
        configAspectRatio: CGFloat,
        storeAspectRatio: @escaping (CGFloat) -> Void
    ) -> some View {
        modifier(
            WebcamFrame(
                nativeAspectRatio: nativeAspectRatio,
                initialAspectRatio: initialAspectRatio,
                configAspectRatio: configAspectRatio,
                storeAspectRatio: storeAspectRatio
            )
        )
    }
}

private class ViewModel : ObservableObject {
    
    private var bag: [AnyCancellable] = []
    @Published var useNativeAspectRatio: Bool = preferences?.useNativeAspectRatio != true
    
    init() {
        preferences?.updatedFlow2.asPublisher()
            .sink{ (preferences: OctoPreferences) in
                self.useNativeAspectRatio = preferences.useNativeAspectRatio == true
            }
            .store(in: &bag)
    }
}

private var preferences: OctoPreferences? { SharedBaseInjector.shared.getOrNull()?.preferences }

private extension OctoPreferences {
    var useNativeAspectRatio: Bool { webcamAspectRatioSource == OctoPreferences.Companion.shared.VALUE_WEBCAM_ASPECT_RATIO_SOURCE_IMAGE }
}

struct WebcamFrame_Previews: PreviewProvider {
    static var previews: some View {
        Color.red.webcamFrame(
            nativeAspectRatio: 2/3,
            initialAspectRatio: 1,
            configAspectRatio: 4/3,
            storeAspectRatio: { _ in }
        )
    }
}
