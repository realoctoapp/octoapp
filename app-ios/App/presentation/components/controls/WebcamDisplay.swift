//
//  WebcamDisplay.swift
//  OctoApp
//
//  Created by Christian on 19/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct WebcamDisplay<Content: View> : View {
    
    var canZoom: Bool
    @Binding var nativeImageSize: CGSize
    @ViewBuilder var overlay: (WebcamOverlayInputs) -> Content
    
    @EnvironmentObject private var viewModel: WebcamControlsViewModel
    @StateObject private var htmlWebcamAdapter = WebpageWebcamCoordinator()
    @State private var isLive = false
    @State private var evenOddFrame = false
    @State private var zoomedIn = false
    @State private var displayName: String? = nil
    @State private var snapshot: (() async throws -> UIImage)? = nil
    @State private var pictureInPicture: (() async throws -> Void)? = nil
    @Namespace private var animation
    @Environment(\.scenePhase) private var scenePhase
    @Environment(\.openURL) private var openURL

    var body: some View {
        ZStack {
            innerBody
            overlay(
                WebcamOverlayInputs(
                    liveBadgeState: WebcamLiveBadge.State(live: isLive, evenOddFrame: evenOddFrame),
                    zoomedIn: zoomedIn,
                    displayName: displayName,
                    snapshot: snapshot,
                    pictureInPicture: pictureInPicture
                )
            )
        }
        .onChange(of: scenePhase) { _, newPhase in
            switch newPhase {
            case .active: viewModel.onUiStarted()
            case .background: viewModel.onUiStopped()
            default: break
            }
        }
        .onChange(of: viewModel.state) {
            nativeImageSize = CGSize.zero
            isLive = false
        }
    }
    
    @ViewBuilder
    var innerBody: some View {
        switch viewModel.state {
        case .hidden: do {}
        case .loading(
            canSwitchWebcam: _,
            displayName: let name
        ): progress().onChange(of: name, initial: true) { _, new in
            displayName = new
        }
            
        case .error(
            canSwitchWebcam: _,
            exception: let exception,
            description: let description,
            hasDetails: let hasDetails,
            canRetry: let canRetry,
            displayName: let name
        ): error(
            exception: exception,
            description: description,
            hasDetails: hasDetails,
            canRetry: canRetry
        ).onChange(of: name, initial: true) { _, new in
            displayName = new
        }
            
        case .frame(
            canSwitchWebcam: _,
            frames: let frames,
            transformation: let transformation,
            displayName: let name
        ): mjpeg(
            frames: frames,
            transformation: transformation
        ).onChange(of: name, initial: true) { _, new in
            displayName = new
        }
            
        case .webpage(
            canSwitchWebcam: _,
            content: let content,
            transformation: let transformation,
            displayName: let name
        ): webpage(
            content: content,
            transformation: transformation
        ).onChange(of: name, initial: true) { _, new in
            displayName = new
        }
            
        case .notAvailable(
            canSwitchWebcam: _,
            featureName: let featureName,
            displayName: let name
        ): notAvailable(
            featureName: featureName
        ).onChange(of: name, initial: true) { _, new in
            displayName = new
        }
        }
    }
    
    private func webpage(content: WebcamControlsViewModel.WebContent, transformation: WebcamTransformation) -> some View {
        WebpageWebcam(
            content: content,
            transformation: transformation,
            onImageSizeChange: {
                nativeImageSize = $0.rotated(rotate90: transformation.rotate90)
            },
            onSnapshotAvailable: {
                snapshot = $0
                pictureInPicture = { try await htmlWebcamAdapter.requestPip() }
            },
            isLive: $isLive,
            coordinator: htmlWebcamAdapter,
            zoomedIn: $zoomedIn
        )
    }
    
    private func mjpeg(frames: MjpegFrameMediator, transformation: WebcamTransformation) -> some View {
        MjpegWebcam(
            frames: frames,
            transformation: transformation,
            collectEvenOdd: frames.frameInterval > .seconds(0),
            canZoom: canZoom,
            onImageSizeChange: { nativeImageSize = $0.rotated(rotate90: transformation.rotate90) },
            onSnapshotAvailable: {
                snapshot = $0
                pictureInPicture = nil
            },
            isLive: $isLive,
            evenOddFrame: $evenOddFrame,
            zoomedIn: $zoomedIn
        )
    }
    
    private func error(exception: KotlinThrowable, description: String?, hasDetails: Bool, canRetry: Bool) -> some View {
        ScreenError(
            title: String(describing: exception.composeErrorMessage()),
            description: description,
            error: hasDetails ? exception : nil,
            canDismiss: false,
            onRetry: canRetry ? { viewModel.retry() } : nil,
            textColor: OctoTheme.colors.textColoredBackground
        )
        .padding(OctoTheme.dimens.margin12)
        .onAppear {
            pictureInPicture = nil
            snapshot = nil
        }
    }
    
    private func notAvailable(featureName: String) -> some View {
        ScreenError(
            title: "supporter_perk___title"~,
            description: String(format: "supporter_perk___description"~, featureName),
            onDetails: { openURL(UriLibrary.shared.getPurchaseUri()) },
            canDismiss: false,
            textColor: OctoTheme.colors.textColoredBackground,
            onDetailsLabel: "support_octoapp"~
        )
        .padding(OctoTheme.dimens.margin12)
        .onAppear {
            pictureInPicture = nil
            snapshot = nil
        }
    }
    
    private func progress() -> some View {
        ProgressView()
            .tint(OctoTheme.colors.textColoredBackground)
    }
}

private extension CGSize {
    func rotated(rotate90: Bool) -> CGSize {
        rotate90 ? CGSize(width: height, height: width) : self
    }
}
