//
//  PrintBedLayout.swift
//  OctoApp
//
//  Created by Christian on 20/06/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct PrintBedLayout<Overlay: View, Legend: View>: View {
    
    var printBed: GcodeNativeCanvasImageStruct
    var file: FileObjectStruct?
    var printerProfile: PrinterProfileStruct?
    var settings: GcodePreviewSettingsStruct
    var paused: Bool
    var exceedsPrintArea: Bool
    var unsupportedGcode: Bool
    var isTrackingPrintProgress: Bool
    var renderContext: LowOverheadMediator<GcodeRenderContext>
    var plugin: (GcodeNativeCanvas) -> Void = { _ in }
    @ViewBuilder var overlay: () -> Overlay
    @ViewBuilder var legend: () -> Legend
    
    @State private var live = true
    @State private var lastUpdate: Date = Date.now

    var body: some View {
        ZStack {
            HStack(spacing: 0) {
                gcodePreview
                legend()
                    .frame(maxWidth: .infinity)
            }
            
            overlay()
        }
    }
    
    var gcodePreview: some View {
        ZStack {
            if paused {
                printBedBackground
            } else if let printerProfile = printerProfile {
                GcodeRenderView(
                    mediator: renderContext,
                    plugin: plugin,
                    printBed: printBed,
                    printBedWidthMm: printerProfile.volume.width,
                    printBedHeightMm: printerProfile.volume.depth,
                    extrusionWidthMm: printerProfile.estimatedNozzleDiameter,
                    originInCenter: printerProfile.volume.origin == .center,
                    quality: settings.quality,
                    paddingHorizontal: OctoTheme.dimens.margin12,
                    paddingTop: OctoTheme.dimens.margin12,
                    paddingBottom: OctoTheme.dimens.margin12
                )
                .aspectRatio(
                    CGFloat(printerProfile.volume.width / printerProfile.volume.depth),
                    contentMode: .fit
                )
            } else {
                printBedBackground
                    .aspectRatio(1, contentMode: .fit)
                Image(systemName: "exclamationmark.triangle.fill")
                    .foregroundColor(OctoTheme.colors.lightText)
                    .font(.system(size: 24))
                
            }
        }
        .frame(maxWidth: .infinity)
        .animation(.default, value: paused)
    }
    
    var printBedBackground: some View {
        Image(printBed.imageName)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .padding(OctoTheme.dimens.margin12)
    }
}

struct PrintBedLayout_Previews: PreviewProvider {
    static var previews: some View {
        PrintBedLayout(
            printBed: .printbedender,
            file: nil,
            printerProfile: PrinterProfileStruct(
                id: "some",
                current: true,
                default_: true,
                model: "Ender 3",
                name: "Ender 3",
                color: "sdf",
                volume: PrinterProfileStruct.Volume(
                    depth: 300,
                    width: 230,
                    height: 250,
                    origin: .lowerleft,
                    formFactor: .rectengular
                ),
                axes: PrinterProfileStruct.Axes(
                    e: PrinterProfileStruct.Axis(inverted: false, speed: 0),
                    x: PrinterProfileStruct.Axis(inverted: false, speed: 0),
                    y: PrinterProfileStruct.Axis(inverted: false, speed: 0),
                    z: PrinterProfileStruct.Axis(inverted: false, speed: 0)
                ),
                extruders: [
                    PrinterProfileStruct.Extruder(
                        nozzleDiameter: 0.4,
                        componentName: "extruder",
                        extruderComponents: ["extruder"]
                    )
                ],
                heatedChamber: false,
                heatedBed: false,
                estimatedNozzleDiameter: 0.4
            ),
            settings: GcodePreviewSettingsStruct(
                showPreviousLayer: true,
                showCurrentLayer: false,
                layerOffset: 1,
                quality: .medium
            ), paused: false,
            exceedsPrintArea: false,
            unsupportedGcode: false,
            isTrackingPrintProgress: false,
            renderContext: LowOverheadMediator(
                initialData: GcodeRenderContext(
                    previousLayerPaths: nil,
                    completedLayerPaths: [],
                    remainingLayerPaths: nil,
                    printHeadPosition: GcodePoint(x: 115, y: 115),
                    gcodeBounds: GcodeRect(top: 0, left: 0, right: 230, bottom: 230),
                    layerCount: 42,
                    layerNumber: 13,
                    layerZHeight: 2.4,
                    layerProgress: 0.45,
                    layerNumberDisplay: { KotlinInt(integerLiteral: $0.intValue + 1) },
                    layerCountDisplay: { KotlinInt(integerLiteral: $0.intValue + 1) }
                )
            ),
            overlay: {
                Color.blue.opacity(0.2)
            },
            legend: {
                VStack {
                    Text("Some")
                    Text("stuff")
                    Text("here")
                }
            }
        )
    }
}
