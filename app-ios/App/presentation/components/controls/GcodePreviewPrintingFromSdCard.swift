//
//  GcodePreviewPrintingFromSdCard.swift
//  OctoApp
//
//  Created by Christian Würthner on 31/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct GcodePreviewPrintingFromSdCard: View {
    
    var file: FileObjectStruct
    
    var body: some View {
        Text(String(format: "gcode_preview___printing_from_sd_card"~, file.display))
            .typographySubtitle()
            .multilineTextAlignment(.center)
            .padding(OctoTheme.dimens.margin12)
    }
}

struct GcodePreviewPrintingFromSdCard_Previews: PreviewProvider {
    static var previews: some View {
        GcodePreviewPrintingFromSdCard(
            file: FileObjectStruct(
                origin: .other(name: "sdcard"),
                display: "Some file.gcode",
                name: "Some file.gcode",
                path: "/",
                isFolder: false,
                isPrintable: true
            )
        )
    }
}
