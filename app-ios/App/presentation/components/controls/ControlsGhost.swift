//
//  ControlsGhost.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct ControlsGhost: View {
    
    var body: some View {
        ZStack {
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(OctoTheme.colors.inputBackground)
        .cornerRadius(OctoTheme.dimens.cornerRadius)
    }
}

struct ControlsGhost_Previews: PreviewProvider {
    static var previews: some View {
        ControlsGhost()
    }
}
