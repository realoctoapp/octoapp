//
//  ControlsInstanceSwitcher.swift
//  OctoApp
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct ControlsInstanceSwitcher: View {
    
    @StateObject private var viewModel = ControlsInstanceSwitcherViewModel()
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    
    var body: some View {
        ZStack {
            ControlsInstanceSwitcherButton(
                shown: viewModel.shown,
                action: { orchestrator.controlCenterShown = true }
            )
        }
        .animation(.default, value: viewModel.shown)
        .usingViewModel(viewModel, fixedInstanceId: "")
    }
}

private struct ControlsInstanceSwitcherButton: View {
    
    var shown: Bool
    var action: () -> Void
    
    @StateObject private var viewModel = ControlsInstanceSwitcherViewModel()
    @EnvironmentObject private var orchestrator: OrchestratorViewModel
    @Environment(\.instanceColor) private var color
    
    var body: some View {
        if shown {
            Button(
                action: action,
                label: { label }
            )
            .transition(.move(edge: .trailing).combined(with: .opacity))
        }
    }
    
    @ViewBuilder
    private var label: some View {
        Image(systemName: "checklist")
            .font(.system(size: 20))
            .padding(OctoTheme.dimens.margin1)
            .padding(.leading, OctoTheme.dimens.margin01)
            .padding(.trailing, OctoTheme.dimens.margin12)
            .tint(OctoTheme.colors.textColoredBackground)
            .foregroundColor(OctoTheme.colors.textColoredBackground)
            .background(color.main)
            .cornerRadius(100, corners: [.topLeft, .bottomLeft])
    }
}

class ControlsInstanceSwitcherViewModel : BaseViewModel {
    var currentCore: ControlsInstanceSwitcherViewModelCore? = nil
    var bag: Set<AnyCancellable> = []
    @Published var shown = false
    
    func createCore(instanceId: String) -> ControlsInstanceSwitcherViewModelCore {
        return ControlsInstanceSwitcherViewModelCore()
    }
    
    func publish(core: ControlsInstanceSwitcherViewModelCore) {
        core.state.asPublisher()
            .sink { (state: ControlsInstanceSwitcherViewModelCore.State) in
                self.shown = state.shown
            }
            .store(in: &bag)
    }
    
    func clearData() {
        shown = false
    }
}

struct ControlsInstanceSwitcher_Previews: PreviewProvider {
    
    static var previews: some View {
        ControlsInstanceSwitcherButton(shown: true, action: {})
    }
}
