//
//  LoadingPlaceholder.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct LoadingPlaceholder: View {
    var body: some View {
        ProgressView()
            .padding(OctoTheme.dimens.margin3)
    }
}

struct LoadingPlaceholder_Previews: PreviewProvider {
    static var previews: some View {
        LoadingPlaceholder()
    }
}
