//
//  ControlsLayout.swift
//  OctoApp
//
//  Created by Christian on 08/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct ControlsLayout: Layout {
    
    var horizontalSpacing = OctoTheme.dimens.margin12
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        var p = proposal.replacingUnspecifiedDimensions()
        let columnCount = columnCount(fullWidth: p.width)
        var columnHeight =  [CGFloat](repeating: 0, count: columnCount)
        let columnWidth = columnWidth(fullWidth: p.width)
        var nextColumn = -1
        
        subviews.forEach { subview in
            let column = (nextColumn + 1) % columnCount
            nextColumn += 1
            columnHeight[column] += subview.sizeThatFits(
                columnProposal(columnWidth: columnWidth)
            ).height
        }
        
        p.height = columnHeight.max() ?? 0
        return p
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        let p = bounds.origin
        let columnCount = columnCount(fullWidth: bounds.width)
        var columnHeight = [CGFloat](repeating: 0, count: columnCount)
        let columnWidth = columnWidth(fullWidth: bounds.width)
        var nextColumn = -1
                
        subviews.forEach { subview in
            let vp = subview.sizeThatFits(columnProposal(columnWidth: columnWidth))
            let column = (nextColumn + 1) % columnCount
            
            // Advance to next column if the height is not "zero" (give some Slack)
            if vp.height > 20 {
                nextColumn += 1
            }
            
            subview.place(
                at: CGPoint(
                    x: p.x + (CGFloat(column) * (columnWidth + horizontalSpacing)),
                    y: p.y + columnHeight[column]
                ),
                proposal: columnProposal(columnWidth: columnWidth)
            )
            
            columnHeight[column] += vp.height
        }
    }

    func columnCount(fullWidth: CGFloat) -> Int {
        let iPhone14ProMax: CGFloat = 1290 / 2
        let iPad12dot9: CGFloat = 2732 / 2
        if iPhone14ProMax >= fullWidth {
            // iPhone, iPad side view
            return  1
        } else if iPad12dot9 > fullWidth {
            // Small iPads, iPad hald width
            return  2
        } else {
            // iPad 12.9 landscape
            return  3
        }
    }
    
    func columnWidth(fullWidth: CGFloat) -> CGFloat {
        let columnCount = columnCount(fullWidth: fullWidth)
        return (fullWidth - (CGFloat(columnCount - 1) * horizontalSpacing)) / CGFloat(columnCount)
    }
    
    func columnProposal(columnWidth: CGFloat) -> ProposedViewSize {
        ProposedViewSize(width: columnWidth, height: nil)
    }
}

private extension [CGFloat] {
    func minIndex() -> Int {
        return indices.min { self[$0] < self[$1] } ?? 0
    }
}

struct ControlsLayout_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            ControlsLayout() {
                Color.red.frame(height: 100)
                Color.blue.frame(height: 300).frame(width: .infinity)
                Color.green.frame(height: 400).frame(width: .infinity)
                Color.brown.frame(height: 0).frame(width: .infinity)
                Color.gray.frame(height: 50)
                Color.yellow.frame(height: 100)
            }
        }
    }
}
