//
//  WebcamLiveBadge.swift
//  OctoApp
//
//  Created by Christian on 19/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct WebcamLiveBadge: View {
    var state: WebcamLiveBadge.StateWithDelay
    
    var body: some View {
        TextTag(
            icon: "circle.fill",
            text: "app_widget___live"~,
            color: OctoTheme.colors.red,
            countdownSecs: state.nextFrameDelay,
            countDownFilled: state.state.evenOddFrame
        )
        .opacity(state.state.live ? 1 : 0)
        .animation(.default, value: state.state.live)
    }
}

extension WebcamLiveBadge {
    struct State : Equatable {
        var live: Bool
        var evenOddFrame: Bool
    }
    
    struct StateWithDelay : Equatable {
        var state: State
        var nextFrameDelay: TimeInterval
    }
}

struct WebcamLiveBadge_Previews: PreviewProvider {
    static var previews: some View {
        WebcamLiveBadge(
            state: WebcamLiveBadge.StateWithDelay(
                state: WebcamLiveBadge.State(live: true, evenOddFrame: false),
                nextFrameDelay: 12
            )
        )
    }
}
