//
//  GcodePreviewFeatureDisabled.swift
//  OctoApp
//
//  Created by Christian Würthner on 31/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct GcodePreviewFeatureDisabled: View {
    
    var printBed: GcodeNativeCanvasImageStruct
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        GeometryReader { geo in
            HStack {
                previewImage.padding(.leading, -geo.size.height / 2)
                content
            }
        }
    }
    
    var previewImage: some View {
        ZStack {
            Image(printBed.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
            
            Image("gcodePreview")
                .resizable()
                .aspectRatio(contentMode: .fit)
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .aspectRatio(1, contentMode: .fit)
        .padding([.top, .bottom], OctoTheme.dimens.margin12)
    }
    
    var content: some View {
        VStack(alignment: .leading, spacing: OctoTheme.dimens.margin01) {
            Text("supporter_perk___title"~)
                .typographySubtitle()
            
            Text(String(format: "supporter_perk___description"~, "gcode_preview"~))
                .foregroundColor(OctoTheme.colors.normalText)
                .typographyLabelSmall()
                .fixedSize(horizontal: false, vertical: true)
            
            Spacer(minLength: 0)
            
            Button(action: { openUrl(UriLibrary.shared.getPurchaseUri()) }) {
                SimpleMenuItem(
                    title: "support_octoapp"~,
                    iconSystemName: "heart.fill",
                    style: .support,
                    slim: true
                )
            }
            
            Button(action: { SharedBaseInjector.shared.get().preferences.gcodeHiddenIos = true }) {
                SimpleMenuItem(
                    title: "hide"~,
                    iconSystemName: "eye.slash.fill",
                    style: .neutral,
                    slim: true
                )
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(OctoTheme.dimens.margin12)
    }
}


struct GcodePreviewFeatureDisabledFullscreen: View {
    
    var printBed: GcodeNativeCanvasImageStruct
    @Environment(\.openURL) var openUrl
    
    var body: some View {
        VStack {
            previewImage
            text
            button
        }
    }
    
    var previewImage: some View {
        ZStack {
            Image(printBed.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
            
            Image("gcodePreview")
                .resizable()
                .aspectRatio(contentMode: .fit)
        }
        .frame(maxWidth: 400)
        .fixedSize(horizontal: false, vertical: false)
        .aspectRatio(1, contentMode: .fill)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(OctoTheme.dimens.margin12)
    }
    
    var text: some View {
        VStack(spacing: OctoTheme.dimens.margin01) {
            Text("supporter_perk___title"~)
                .typographySubtitle()
            
            Text(String(format: "supporter_perk___description"~, "gcode_preview"~))
                .foregroundColor(OctoTheme.colors.normalText)
                .typographyLabelSmall()
                .fixedSize(horizontal: false, vertical: true)
        }
        .multilineTextAlignment(.center)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(OctoTheme.dimens.margin12)
    }
    
    var button: some View {
        OctoButton(
            text: "support_octoapp"~,
            clickListener: { openUrl(UriLibrary.shared.getPurchaseUri()) }
        )
        .padding(OctoTheme.dimens.margin12)
    }
}

private struct FeatureDisabledLayout: Layout {
    
    func sizeThatFits(proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) -> CGSize {
        if subviews.count != 2 {
            assertionFailure("Expected exactly 2 subviews, got \(subviews.count)")
        }
        
        return proposal.replacingUnspecifiedDimensions()
    }
    
    func placeSubviews(in bounds: CGRect, proposal: ProposedViewSize, subviews: Subviews, cache: inout ()) {
        var p = bounds.origin
        let padding = OctoTheme.dimens.margin12
        let previewVisibleWidth =  bounds.height * 0.4
        let previewSurfaceWidth = bounds.height - padding * 2
        
        // Preview
        p.x = previewVisibleWidth - bounds.height + padding * 2
        p.y += padding
        subviews[0].place(
            at: p,
            proposal: .init(width: previewSurfaceWidth, height: previewSurfaceWidth)
        )
        
        // Content
        p.x = previewVisibleWidth
        p.y -= padding
        subviews[1].place(
            at: p,
            proposal: .init(width: bounds.size.width - previewVisibleWidth, height: bounds.height)
        )
    }
}

struct GcodePreviewFeatureDisabled_Previews: PreviewProvider {
    static var previews: some View {
        GcodePreviewFeatureDisabled(printBed: .printbedcreality)
            .aspectRatio(16/9, contentMode: .fit)
            .surface()
            .padding()
            .previewDisplayName("Control")
        
        GcodePreviewFeatureDisabledFullscreen(printBed: .printbedcreality)
            .previewDisplayName("Fullscreen")
    }
}
