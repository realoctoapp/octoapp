//
//  TerminalShortcuts.swift
//  OctoApp
//
//  Created by Christian on 05/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct TerminalShortcuts: View {
    
    var shortcuts: [TerminalViewModel.Shortcut]
    var filterCount: Int
    var useStyledList: Bool
    var executeGcode: (TerminalViewModel.Shortcut) async -> Void
    var toggleStyledLogs: () async -> Void
    var clear: () -> Void
    
    private let endMarker = "end"
    @State private var showCustomizationItem: TerminalViewModel.Shortcut? = nil
    @State private var shown = false
    @State private var showFilters = false
    @Environment(\.instanceId) private var instanceId
    
    var body: some View {
        ScrollViewReader { reader in
            ScrollView(.horizontal, showsIndicators: false) {
                scrollBody
            }
            .onChange(of: shortcuts.count) {
                Task {
                    try? await Task.sleep(for: .seconds(0.5))
                    await MainActor.run {
                        withAnimation {
                            reader.scrollTo(endMarker, anchor: .trailing)
                        }
                    }
                }
            }
            .animation(.default, value: shortcuts)
            .macOsCompatibleSheet(item: $showCustomizationItem) { item in
                MenuHost(
                    menu: CustomizeGcodeShortcutMenu(
                        gcode: item.command,
                        offerInsert: false
                    )
                )
            }
            .macOsCompatibleSheet(isPresented: $showFilters) {
                MenuHost(
                    menu: TerminalFilterMenu(
                        instanceId: instanceId
                    )
                )
            }
            .task {
                // There is a glitchy animation when we appear, mask it by fading in
                // a bit later
                try? await Task.sleep(for: .seconds(1))
                shown = true
            }
            .opacity(shown ? 1 : 0)
            .animation(.default, value: shown)
        }
    }
    
    var scrollBody: some View {
        HStack {
            Spacer(minLength: OctoTheme.dimens.margin12)
            shortcutItems
            specialOptionItems
            Spacer(minLength: OctoTheme.dimens.margin12).id(endMarker)
        }
        .padding([.top, .bottom], OctoTheme.dimens.margin1)
    }
    
    var shortcutItems: some View {
        ForEach(shortcuts.reversed()) { shortcut in
            PinnedWrapper(pinned: shortcut.isFavourite) {
                OctoAsyncButton(
                    text: shortcut.label,
                    type: .secondary,
                    small: true,
                    clickListener: { await executeGcode(shortcut) },
                    longClickListener: { showCustomizationItem = shortcut }
                )
            }
        }
    }
    
    @ViewBuilder
    var specialOptionItems: some View {
        OctoAsyncButton(
            icon: useStyledList ? "paintbrush.fill" : "paintbrush",
            text: "",
            small: true,
            clickListener: { await toggleStyledLogs() }
        )
        .animation(.default, value: useStyledList)
        
        OctoButton(
            icon: "trash.fill",
            text: "",
            small: true,
            clickListener: { clear() }
        )
        
        OctoButton(
            icon: filterCount > 0 ? "line.3.horizontal.decrease.circle.fill" : "line.3.horizontal.decrease.circle",
            text: "\(filterCount.formatted())",
            small: true,
            clickListener: { showFilters = true }
        )
    }
}

struct TerminalShortcuts_Previews: PreviewProvider {
    static var previews: some View {
        TerminalShortcuts(
            shortcuts: previewShortcuts,
            filterCount: 0,
            useStyledList: true,
            executeGcode: { _ in },
            toggleStyledLogs: { },
            clear: {}
        )
    }
}

private var previewShortcuts: [TerminalViewModel.Shortcut] {
    [
        TerminalViewModel.Shortcut(
            command: "G28",
            label: "G28",
            isFavourite: true
        ),
        TerminalViewModel.Shortcut(
            command: "G29",
            label: "G29",
            isFavourite: true
        ),
        TerminalViewModel.Shortcut(
            command: "M500",
            label: "M500",
            isFavourite: true
        ),
        TerminalViewModel.Shortcut(
            command: "M501",
            label: "M501", 
            isFavourite: true
        )
    ]
}
