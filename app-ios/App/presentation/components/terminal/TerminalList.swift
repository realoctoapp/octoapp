//
//  TerminalList.swift
//  OctoApp
//
//  Created by Christian on 05/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct TerminalList: View {
    
    var logs: [TerminalViewModel.TerminalLogLine]
    var styled: Bool
    
    @State private var stickToBottom = true
    @State private var showScrollDownButton = false
    private let bottomId = "bottom"
    
    var body: some View {
        ZStack {
            if logs.isEmpty {
                loader
            } else {
                scroller
            }
        }
        .animation(.default, value: logs.isEmpty)
    }
    
    var loader: some View {
        ZStack {
            ProgressView()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
    
    var scroller: some View {
        ScrollViewReader { reader in
            ZStack(alignment: .bottom) {
                ScrollView(showsIndicators: false) {
                    list
                }
                .onChange(of: logs) { onUpdate(reader) }
                .onAppear() { onUpdate(reader) }
                
               scrollDownButton(reader)
            }
        }
        .animation(.spring(), value: showScrollDownButton)
        .task(id: stickToBottom) {
            try? await Task.sleep(for: .seconds(0.1))
            if !stickToBottom && !Task.isCancelled {
                try? await Task.sleep(for: .seconds(0.6))
                if !Task.isCancelled {
                    showScrollDownButton = true
                }
            }
        }
        .clipped()
    }
    
    @ViewBuilder
    func scrollDownButton(_ reader: ScrollViewProxy) -> some View {
        if showScrollDownButton {
            Image(systemName: "arrow.down.to.line")
                .aspectRatio(1, contentMode: .fit)
                .padding(OctoTheme.dimens.margin1)
                .background(Circle().fill(OctoTheme.colors.accent).shadow(radius: 1))
                .foregroundColor(OctoTheme.colors.white)
                .padding(OctoTheme.dimens.margin2)
                .onTapGesture { onUpdate(reader, forced: true) }
                .transition(.opacity.combined(with: .move(edge: .bottom)))
                .zIndex(10000)
        }
    }
    
    var list: some View {
        LazyVStack(spacing: 0) {
            lines
            bottom
        }
    }
    
    var lines: some View {
        ForEach(logs) { line in
            TerminalLogLine(
                line: line,
                styled: styled
            )
        }
        .textSelection(.enabled)
    }
    
    var bottom: some View {
        Color.clear
            .onAppear {
                stickToBottom = true
                showScrollDownButton = false
            }
            .onDisappear { stickToBottom = false }
            .frame(height: OctoTheme.dimens.margin12)
            .id(bottomId)
    }
    
    private func onUpdate(_ reader: ScrollViewProxy, forced: Bool = false) {
        // Sometimes it doesn't scroll all the way. Double scroll to make sure.
        if stickToBottom || forced {
            showScrollDownButton = false
            Task {
                await MainActor.run {
                    withAnimation {
                        reader.scrollTo(bottomId, anchor: .bottom)
                    }
                }
                try await Task.sleep(for: .seconds(0.5))
                await MainActor.run {
                    withAnimation {
                        reader.scrollTo(bottomId, anchor: .bottom)
                    }
                }
            }
        }
    }
}
