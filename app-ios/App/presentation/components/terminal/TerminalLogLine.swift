//
//  TerminalLogLine.swift
//  OctoApp
//
//  Created by Christian on 04/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct TerminalLogLine: View {
    
    var line: TerminalViewModel.TerminalLogLine
    var styled: Bool
    private let shapeCorner: CGFloat = 20
    private let shapeCornerInner: CGFloat = 15
    private let shapeWidth: CGFloat = 6
    private let shapeColor = OctoTheme.colors.inputBackground
    
    var body: some View {
        ZStack {
            if !styled {
                plain
            } else if line.isStart {
                styledStart
            } else if line.isMiddle {
                styledMiddle
            } else if line.isEnd {
                styledBottom
            } else {
                plain
            }
        }
        .fixedSize(horizontal: false, vertical: true)
        .padding(.leading, OctoTheme.dimens.margin1)
        .typographyTerminal()
    }
    
    @ViewBuilder
    var plain: some View {
        Text(styled ? line.textStyled : line.text)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding([.leading, .trailing], OctoTheme.dimens.margin1)
            .padding([.top, .bottom], OctoTheme.dimens.margin0)
            .padding(.leading, styled ? shapeWidth : 0)
    }
   
    @ViewBuilder
    var styledStart: some View {
        Text(line.textStyled)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading, shapeWidth + OctoTheme.dimens.margin1)
            .padding(.top, OctoTheme.dimens.margin1)
            .padding(.bottom, shapeCornerInner + OctoTheme.dimens.margin1)
            .padding(.trailing, OctoTheme.dimens.margin1)
            .background(styledTopBackground)
            .padding(.top, OctoTheme.dimens.margin01)
    }
    
    @ViewBuilder
    var styledMiddle: some View {
        Text(line.textStyled)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading, shapeWidth + OctoTheme.dimens.margin1)
            .padding([.top, .bottom], OctoTheme.dimens.margin0)
            .padding(.trailing, OctoTheme.dimens.margin1)
            .background(styledMiddleBackground)
    }
   
    @ViewBuilder
    var styledBottom: some View {
        Text(line.textStyled)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading, shapeWidth + OctoTheme.dimens.margin1)
            .padding(.bottom, shapeCornerInner + shapeWidth)
            .padding(.trailing, OctoTheme.dimens.margin1)
            .background(styledBottomBackground.scaleEffect(x: 1, y: -1))
            .padding(.bottom, OctoTheme.dimens.margin01)
    }
    
    var styledMiddleBackground: some View {
        Canvas { context, size in
            var p = Path()
            p.addLine(to: CGPoint(x: 0, y: size.height))
            p.addLine(to: CGPoint(x: shapeWidth, y: size.height))
            p.addLine(to: CGPoint(x: shapeWidth, y: shapeWidth))
            p.addLine(to: CGPoint(x: shapeWidth, y: 0))
            p.closeSubpath()
            
            context.fill(p, with: .color(shapeColor))
        }
    }
  
    var styledTopBackground: some View {
        Canvas { context, size in
            var p = Path()
            p.move(to: CGPoint(x: size.width, y: 0))
            p.addLine(to: CGPoint(x: shapeCorner, y: 0))
            p.addArc(center: CGPoint(x: shapeCorner, y: shapeCorner), radius: shapeCorner, startAngle: .degrees(270), endAngle: .degrees(180), clockwise: true)
            p.addLine(to: CGPoint(x: 0, y: size.height))
            p.addLine(to: CGPoint(x: shapeWidth, y: size.height))
            p.addArc(center: CGPoint(x: shapeWidth + shapeCornerInner, y: size.height), radius: shapeCornerInner, startAngle: .degrees(180), endAngle: .degrees(270), clockwise: false)
            p.addLine(to: CGPoint(x: size.width, y: size.height - shapeCorner + shapeWidth))
            p.closeSubpath()
            
            context.fill(p, with: .color(shapeColor))
        }
    }
    
    var styledBottomBackground: some View {
        Canvas { context, size in
            var p = Path()
            p.move(to: CGPoint(x: size.width, y: 0))
            p.addLine(to: CGPoint(x: shapeCorner, y: 0))
            p.addArc(center: CGPoint(x: shapeCorner, y: shapeCorner), radius: shapeCorner, startAngle: .degrees(270), endAngle: .degrees(180), clockwise: true)
            p.addLine(to: CGPoint(x: 0, y: size.height))
            p.addLine(to: CGPoint(x: shapeWidth, y: size.height))
            p.addLine(to: CGPoint(x: shapeWidth, y: shapeWidth + shapeCornerInner))
            p.addArc(center: CGPoint(x: shapeWidth + shapeCornerInner, y: shapeWidth + shapeCornerInner), radius: shapeCornerInner, startAngle: .degrees(180), endAngle: .degrees(270), clockwise: false)
            p.addLine(to: CGPoint(x: size.width, y: shapeWidth))
            p.closeSubpath()
            
            context.fill(p, with: .color(shapeColor))
        }
    }
}

struct TerminalLogLine_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 0) {
            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Send: G280",
                    textStyled: "G280",
                    isStart: true,
                    isEnd: false,
                    isMiddle: false
                ),
                styled: true
            )

            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Recv: wait",
                    textStyled: "wait",
                    isStart: false,
                    isEnd: false,
                    isMiddle: true
                ),
                styled: true
            )
        
            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Recv: Ok",
                    textStyled: "Ok",
                    isStart: false,
                    isEnd: true,
                    isMiddle: false
                ),
                styled: true
            )
            
            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Recv: wait",
                    textStyled: "wait",
                    isStart: false,
                    isEnd: false,
                    isMiddle: false
                ),
                styled: true
            )
        }
        .previewDisplayName("Styled")
        
        VStack(spacing: 0) {
            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Send: G28",
                    textStyled: "G28",
                    isStart: true,
                    isEnd: false,
                    isMiddle: false
                ),
                styled: false
            )
            
            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Recv: wait",
                    textStyled: "wait",
                    isStart: false,
                    isEnd: false,
                    isMiddle: true
                ),
                styled: false
            )
            
            TerminalLogLine(
                line: TerminalViewModel.TerminalLogLine(
                    id: 0,
                    text: "Recv: Ok",
                    textStyled: "Ok",
                    isStart: false,
                    isEnd: true,
                    isMiddle: false
                ),
                styled: false
            )
        }
        .previewDisplayName("Plain")
    }
}
