//
//  OrchestratorBanner.swift
//  OctoApp
//
//  Created by Christian on 20/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct OrchestratorBanner<Content: View>: View {
    
    @Binding var bannerHidden: Bool
    @Binding var bannerShrunken: Bool
    var colorScheme: ColorSchemeStruct
    let content: () -> Content

    @State private var wasLabelShown: Bool = false
    @EnvironmentObject private var  viewModel: OrchestratorViewModel
    
    var body: some View {
        ZStack{
            content()
        }
        .onChange(of: viewModel.banner) { _, newData in
            runAnimation(data: newData)
        }
        .onAppear {
            runAnimation(data: viewModel.banner)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .animation(.easeInOut, value: viewModel.banner)
        .safeAreaInset(edge: .top, spacing: 0) {
            ZStack {
                let label = viewModel.banner.instanceLabel
                
                VStack(spacing: 0) {
                    if (!bannerHidden) {
                        HStack(spacing: OctoTheme.dimens.margin1) {
                            if (viewModel.banner.connectionType == nil) {
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle(tint: OctoTheme.colors.textColoredBackground))
                            }
                            
                            if (!bannerShrunken) {
                                if let icon = getConnectionIcon() {
                                    Image(icon)
                                        .padding([.trailing], OctoTheme.dimens.margin1)
                                        .frame(width: 16, height: 16)
                                }
                                Text(getConnectionTitle())
                                    .foregroundColor(getConnectionTextColor())
                                    .typographyBase()
                            }
                        }
                        .frame(maxWidth: .infinity)
                        .transition(.opacity.combined(with: .move(edge: .top)))
                        .padding([.top, .bottom], bannerShrunken ? 0 : OctoTheme.dimens.margin12)
                        .transition(.opacity.combined(with: .move(edge: .top)))
                        .background(getConnectionColor())
                        .frame(maxWidth: .infinity)
                    }
                    
                    
                    if let l = label {
                        Text(l)
                            .foregroundColor(OctoTheme.colors.textColoredBackground)
                            .typographyBase()
                            .padding([.bottom], OctoTheme.dimens.margin01)
                            .padding([.top], bannerHidden ? 0 : OctoTheme.dimens.margin01)
                            .frame(maxWidth: .infinity)
                    }
                }
                .frame(maxWidth: .infinity)
                .background(label != nil || !bannerHidden ? colorScheme.main : Color.clear)
                .frame(maxWidth: .infinity)
            }
        }
    }
    
    private func runAnimation(data: OrchestratorBannerData) {
        // If the label shown changed and we are connected, do not go through the full animation
        let labelShown = viewModel.banner.instanceLabel != nil
        let onlyLabelChange = labelShown != wasLabelShown && data.connectionType != nil
        wasLabelShown = labelShown
        bannerHidden = onlyLabelChange
        bannerShrunken = onlyLabelChange
        
        Task {
            try await Task.sleep(for: .seconds(3))
            if data.connectionType == .default_ {
                bannerHidden = true
            } else if data.connectionType != nil {
                bannerShrunken = true
            }
        }
    }
    
    private func getConnectionTitle() -> String {
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .octoeverywhere: return "main___banner_connected_via_octoeverywhere"~
            case .obico: return "main___banner_connected_via_spaghetti_detective"~
            case .ngrok: return "main___banner_connected_via_ngrok"~
            case .tailscale: return "main___banner_connected_via_tailscale"~
            case .defaultcloud: return "main___banner_connected_via_alternative"~
            default: return "main___banner_connected_via_local"~
            }
        } else {
            return "main___banner_connection_lost_reconnecting"~
        }
    }
    
    private func getConnectionIcon() -> String? {
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .octoeverywhere: return "octoEverywhereSmall"
            case .obico: return "obicoSmall"
            case .ngrok: return "ngrokSmall"
            case .tailscale: return "tailscaleSmall"
            case .defaultcloud: return nil
            default: return nil
            }
        } else {
            return nil
        }
    }
    
    private func getConnectionColor() -> Color {
        // In case we use the default color scheme and we do show a banner, use a darker shade of green
        // for the local connection banner to not have a super similar green
        let instanceLabel = viewModel.banner.instanceLabel
        let instanceColor = colorScheme.main
        let showInstanceWithGreen = instanceLabel != nil && instanceColor == OctoTheme.colors.defaultColorScheme
        
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .octoeverywhere: return OctoTheme.colors.externalOctoEverywhere4
            case .obico: return OctoTheme.colors.externalObico
            case .ngrok: return OctoTheme.colors.externalNgrok
            case .tailscale: return OctoTheme.colors.externalTailscale
            case .defaultcloud: return OctoTheme.colors.blue
            default: return showInstanceWithGreen ? OctoTheme.colors.darkGreen : OctoTheme.colors.green
            }
        } else {
            return OctoTheme.colors.yellow
        }
    }
    
    private func getConnectionTextColor() -> Color {
        if let connectionType = viewModel.banner.connectionType {
            switch(connectionType) {
            case .tailscale: return OctoTheme.colors.darkText
            default: return OctoTheme.colors.textColoredBackground
            }
        } else {
            return OctoTheme.colors.textColoredBackground
        }
    }
}

struct OrchestratorBannerData : Equatable {
    let connectionType: ConnectionType?
    let instanceLabel: String?
    let instanceColors: ColorsStruct?
    let eventTime: Date
}
