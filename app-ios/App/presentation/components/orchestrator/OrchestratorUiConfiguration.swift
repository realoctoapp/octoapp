//
//  OrchestratorUiConfiguration.swift
//  OctoApp
//
//  Created by Christian on 10/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

private struct UiConfiguration: ViewModifier {
    
    @StateObject private var viewModel = UiConfigurationViewModel()
    
    func body(content: Content) -> some View {
        content
            .id(viewModel.appLanguage)
            .preferredColorScheme(viewModel.appTheme.asColorScheme())
            .animation(.default, value: viewModel.appLanguage)
            
    }
    
    private func determineAppLanguage() {
        
    }
}

private extension AppTheme {
    func asColorScheme() -> ColorScheme? {
        switch self {
        case .dark: return .dark
        case .light: return .light
        default: return nil
        }
    }
}

private class UiConfigurationViewModel : ObservableObject {
    
    @Published var appTheme: AppTheme
    @Published var appLanguage: String
    private static var preferences = SharedBaseInjector.shared.get().preferences
    
    init() {
        // Directly init to prevent any delays
        self.appTheme = UiConfigurationViewModel.determineAppTheme()
        self.appLanguage = UiConfigurationViewModel.determineAppLanguage()
        
        // Observe changes to app theme or language
        UiConfigurationViewModel.preferences.updatedFlow2.asPublisher()
            .map { (_: OctoPreferences) in UiConfigurationViewModel.determineAppTheme() }
            .assign(to: &$appTheme)
        
        UiConfigurationViewModel.preferences.updatedFlow2.asPublisher()
            .map { (_: OctoPreferences) in UiConfigurationViewModel.determineAppLanguage() }
            .assign(to: &$appLanguage)
    }
    
    static func determineAppLanguage() -> String {
        return (try? SharedBaseInjector.shared.get().getAppLanguageUseCase().executeBlocking(param: nil)?.appLanguage) ?? "en"
    }
    
    static func determineAppTheme() -> AppTheme {
        return SharedBaseInjector.shared.get().preferences.appTheme
    }
}

extension View {
    func observeUiConfiguration() -> some View {
        modifier(UiConfiguration())
    }
}
