//
//  File.swift
//  OctoApp
//
//  Created by Christian on 31/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

postfix operator ~
postfix func ~ (string: String) -> String {
    return StringResourcesKt.getString(id: string, formatArgs: KotlinArray(size: 0) { _ in nil })
}

private func trimCdata(_ string: String) -> String {
    return string.deletePrefix().deleteSuffix()
}

private func replaceFromat(_ string: String) -> String {
    do {
        StringResourcesKt.getString(id: string, formatArgs: KotlinArray(size: 0) {  _ in nil })
        let r = try NSRegularExpression(pattern: "%((?:\\.|\\d|\\$)*)[abcdefs]")
        return r.stringByReplacingMatches(in: string, range: NSMakeRange(0, string.count), withTemplate: "%$1@")
    } catch {
        return string
    }
}

private extension String {
    func deletePrefix() -> String {
        let pre = "<![CDATA["
        guard self.hasPrefix(pre) else { return self }
        return String(self.dropFirst(pre.count))
    }
    
    func deleteSuffix() -> String {
        let suf = "]]>"
        guard self.hasSuffix(suf) else { return self }
        return String(self.dropLast(suf.count))
    }
}
