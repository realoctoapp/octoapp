//
//  DateExt.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

extension Date {
    var millisecondsSince1970: Int64 {
        Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    func toInstant() -> Kotlinx_datetimeInstant {
        Kotlinx_datetimeInstant.Companion().fromEpochMilliseconds(epochMilliseconds: Int64(timeIntervalSince1970) * 1000)
    }
}


extension Kotlinx_datetimeInstant {
    func toDate() -> Date {
        return Date(timeIntervalSince1970: Double(epochSeconds))
    }
}

extension Int64 {
    func toDate() -> Date {
        return Date(milliseconds: self)
    }
}
