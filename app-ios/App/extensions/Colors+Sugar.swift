//
//  Colors+Sugar.swift
//  OctoApp
//
//  Created by Christian on 28/10/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

extension Settings.AppearanceColors {
    func toStruct() -> ColorsStruct {
        ColorsStruct(
            light: light.toStruct(),
            dark: dark.toStruct()
        )
    }
}

extension Settings.AppearanceColorScheme {
    func toStruct() -> ColorSchemeStruct {
        ColorSchemeStruct(
            main: main.color,
            accent: accent.color
        )
    }
}
