//
//  Calendar+Sugar.swift
//  OctoApp
//
//  Created by Christian Würthner on 24/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation

extension Calendar {
    func numberOfDaysBetween(_ from: Date, and to: Date) -> Int {
        let fromDate = startOfDay(for: from)
        let toDate = startOfDay(for: to)
        let numberOfDays = dateComponents([.day], from: fromDate, to: toDate)
        
        return numberOfDays.day!
    }
}
