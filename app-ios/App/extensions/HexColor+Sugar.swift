//
//  HexColor+Sugar.swift
//  OctoApp
//
//  Created by Christian on 30/07/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

extension HexColor {
    var color : Color {
        Color(
            uiColor: UIColor(
                red: CGFloat(self.red),
                green: CGFloat(self.green),
                blue: CGFloat(self.blue),
                alpha: 1
            )
        )
    }
}
