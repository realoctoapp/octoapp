//
//  ContentViewModel.swift
//  App
//
//  Created by Christian on 29/09/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import OctoAppBase

public class TimerViewModelObservableObject : ObservableObject {
    
    private var wrapped: TimerViewModel
    @Published private(set) var state: TimerUiState
    
    let octoPrint = OctoPrintEngineBuilderKt.OctoPrintEngineBuilder {
        let scope = $0 as OctoPrintEngineBuilderScope
        scope.baseUrls = ["http://doggy-daycare.local:5001"]
        scope.apiKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    }
    
    
    init(wrapped: TimerViewModel) {
        self.wrapped = wrapped
        state = wrapped.stateFlow.value as! TimerUiState
        (wrapped.stateFlow.asPublisher() as AnyPublisher<TimerUiState, Never>)
            .receive(on: RunLoop.main)
            .assign(to: &$state)
    }
        
    deinit {
        wrapped.onCleared()
    }
  
    func settingTime() {
        wrapped.settingTime()
    }
    
    func setTime(seconds: Int32) {
        wrapped.setTime(seconds: seconds)
    }
    
    //ramaining public functions...
}
