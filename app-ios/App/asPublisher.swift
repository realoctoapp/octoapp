//
//  asPublisher.swift
//  App
//
//  Created by Christian on 29/09/2022.
//  Copyright © 2022 orgName. All rights reserved.
//
// From https://proandroiddev.com/kotlin-multiplatform-mobile-sharing-the-ui-state-management-a67bd9a49882

import Combine
import OctoAppBase

public extension Kotlinx_coroutines_coreFlow {
    func asPublisher<T: AnyObject>() -> AnyPublisher<T, Never> {
        (FlowPublisher(flow: self) as FlowPublisher<T>).eraseToAnyPublisher()
    }
}
