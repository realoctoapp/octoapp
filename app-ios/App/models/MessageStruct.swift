//
//  MessageStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import SwiftUI

struct MessageCurrentStruct: Equatable {
    let logs: [String]
    let temps: [HistoricTemperatureDataStruct]
    let state: PrinterStateStruct
    let progress: ProgressInformationStruct?
    let job: JobInformationStruct?
    let offsets: [String: Float]?
    let serverTime: Date
    let isHistoryMessage: Bool
}

struct HistoricTemperatureDataStruct : Equatable {
    let time: Date
    let components: [String: ComponentTemperatureStruct]
}

struct ComponentTemperatureStruct : Equatable {
    let actual: Float?
    let target: Float?
}

struct PrinterStateStruct : Equatable {
    let text: String?
    let flags: PrinterStateFlagsStruct
}

struct PrinterStateFlagsStruct: Equatable {
    let isPrinting: Bool
    let isOperational: Bool
    let isError: Bool
    let cancelling: Bool
    let pausing: Bool
    let paused: Bool
}

struct ProgressInformationStruct : Equatable {
    let completion: Float?
    let filepos: Int64?
    let printTime: TimeInterval?
    let printTimeLeft: TimeInterval?
    let printTimeLeftOrigin: String?
}

struct JobInformationStruct : Equatable {
    let file: FileObjectStruct?
}

extension MessageCurrent {
    func toStruct() -> MessageCurrentStruct {
        return MessageCurrentStruct(
            logs: logs,
            temps: temps.map { $0.toStruct() },
            state: state.toStruct(),
            progress: progress?.toStruct(),
            job: job?.toStruct(),
            offsets: offsets?.mapValues{ $0.floatValue },
            serverTime: serverTime.toDate(),
            isHistoryMessage: isHistoryMessage
        )
    }
}

extension HistoricTemperatureData {
    func toStruct() -> HistoricTemperatureDataStruct {
        return HistoricTemperatureDataStruct(
            time: time.toDate(),
            components: components.mapValues { $0.toStruct() }
        )
    }
}

extension ComponentTemperature {
    func toStruct() -> ComponentTemperatureStruct {
        return ComponentTemperatureStruct(
            actual: actual?.floatValue,
            target: target?.floatValue
        )
    }
}


extension PrinterState.State {
    func toStruct() -> PrinterStateStruct {
        return PrinterStateStruct(text: text, flags: flags.toStruct())
    }
}

extension ProgressInformation {
    func toStruct() -> ProgressInformationStruct {
        return ProgressInformationStruct(
            completion: completion?.floatValue,
            filepos: filepos?.int64Value,
            printTime: TimeInterval(printTime?.intValue ?? 0),
            printTimeLeft: TimeInterval(printTimeLeft?.intValue ?? 0),
            printTimeLeftOrigin: printTimeLeftOrigin
        )
    }
}

extension PrinterState.Flags {
    func toStruct() -> PrinterStateFlagsStruct {
        return PrinterStateFlagsStruct(
            isPrinting: isPrinting(),
            isOperational: isOperational(),
            isError: isError(),
            cancelling: cancelling,
            pausing: pausing,
            paused: paused
        )
    }
}

extension JobInformation {
    func toStruct() -> JobInformationStruct {
        return JobInformationStruct(
            file: file?.toStruct()
        )
    }
}
