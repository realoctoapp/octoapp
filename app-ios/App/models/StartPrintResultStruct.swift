//
//  StartPrintResultStruct.swift
//  OctoApp
//
//  Created by Christian on 29/12/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

enum StartPrintResultStruct : Equatable {
    case success
    case failed
    case materialConfirmationRequired(timelapseConfirmed: Bool)
    case timelapseConfirmationRequired(materialConfirmed: Bool)
}

extension StartPrintHelper.Result {
    
    func toStruct() -> StartPrintResultStruct {
         if self is StartPrintHelper.ResultStarted {
            return .success
        } else if let materialConfirmationRequired = self as? StartPrintHelper.ResultMaterialConfirmationRequired {
            return .materialConfirmationRequired(timelapseConfirmed: materialConfirmationRequired.timelapseConfirmed)
        } else if let timelapseConfirmationRequired = self as? StartPrintHelper.ResultTimelapseConfirmationRequired {
            return .timelapseConfirmationRequired(materialConfirmed: timelapseConfirmationRequired.materialConfirmed)
        } else {
            return .failed
        }
    }
}
