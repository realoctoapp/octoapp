//
//  FlowStateStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

enum FlowStateStruct<T: Equatable>: Equatable {
    case loading, error(KotlinThrowable), ready(T)
    
    init<R>(_ other: FlowState<R>, mapper: (R) -> T) {
        if other is FlowStateLoading {
            self = .loading
        } else if let error = other as? FlowStateError {
            self = .error(error.throwable)
        } else if let data = (other as? FlowStateReady)?.data {
            let r = mapper(data)
            self = .ready(r)
        } else {
            self = .error(KotlinThrowable(message: "Failed to map flow state"))
        }
    }
}
