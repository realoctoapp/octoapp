//
//  SellingPoint.swift
//  OctoApp
//
//  Created by Christian Würthner on 25/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct SellingPointStruct: Equatable, Identifiable {
    var id: String { "\(title ?? "")/\(description ?? "")/\(imageUrl ?? "")" }
    let title: String?
    let description: String?
    let imageUrl: String?
}

extension SellingPointStruct {
    init(sellingPoint: OctoAppBase.PurchaseOffers.SellingPoint) {
        self.init(
            title: sellingPoint.title,
            description: sellingPoint.description_,
            imageUrl: sellingPoint.imageUrl
        )
    }
}
