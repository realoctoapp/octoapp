//
//  GcodeNativeCanvasImage.swift
//  OctoApp
//
//  Created by Christian Würthner on 30/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

enum GcodeNativeCanvasImageStruct: Equatable {
    case printbedanycubic
    case printbedartillery
    case printbedcreality
    case printbedender
    case printbedprusa
    case printbedgeneric
}

extension GcodeNativeCanvasImageStruct {
    var imageName: String {
        switch self {
        case .printbedanycubic: return "printBedAnycubic"
        case .printbedartillery: return "printBedArtillery"
        case .printbedcreality: return "printBedCreality"
        case .printbedender: return "printBedEnder"
        case .printbedprusa: return "printBedPrusa"
        case .printbedgeneric:  return "printBedGeneric"
        }
    }
    
    func toClass() -> GcodeNativeCanvasImage {
        switch self {
        case .printbedanycubic: return .printbedanycubic
        case .printbedartillery: return .printbedartillery
        case .printbedcreality: return .printbedcreality
        case .printbedender: return .printbedender
        case .printbedprusa: return .printbedprusa
        default: return .printbedgeneric
        }
    }
}

extension GcodeNativeCanvasImage {
    func toStruct() -> GcodeNativeCanvasImageStruct {
        switch self {
        case .printbedanycubic: return .printbedanycubic
        case .printbedartillery: return .printbedartillery
        case .printbedcreality: return .printbedcreality
        case .printbedender: return .printbedender
        case .printbedprusa: return .printbedprusa
        default: return .printbedgeneric
        }
    }
}
