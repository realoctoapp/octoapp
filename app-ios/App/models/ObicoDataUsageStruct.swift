//
//  ObicoDataUsage.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct ObicoDataUsageStruct : Equatable {
    var monthlyCapBytes: Int
    var resetIn: TimeInterval
    var totalBytes: Int
    var hasDataCap: Bool
}

extension ObicoDataUsage {
    func toStruct() -> ObicoDataUsageStruct {
        return ObicoDataUsageStruct(
            monthlyCapBytes: Int(monthlyCapBytes),
            resetIn: TimeInterval(resetInSeconds),
            totalBytes: Int(totalBytes),
            hasDataCap: hasDataCap
        )
    }
}
