//
//  RemoteAccessFailure.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct RemoteConnectionFailureStruct {
    let errorMessage: String
    let errorMessageStack: String
    let stackTrace: String
    let remoteServiceName: String
    let date: Date
    let message: String?
    let learnMoreUrl: String?
}

extension RemoteConnectionFailure {
    func toStruct() -> RemoteConnectionFailureStruct {
        return RemoteConnectionFailureStruct(
            errorMessage: errorMessage,
            errorMessageStack: errorMessageStack,
            stackTrace: stackTrace,
            remoteServiceName: remoteServiceName,
            date: Date(milliseconds: dateMillis),
            message: message,
            learnMoreUrl: learnMoreUrl
        )
    }
}
