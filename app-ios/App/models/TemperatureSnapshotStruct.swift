//
//  TemperatureSnapshotStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct TemperatureSnapshotStruct : Equatable {
    let component: String
    let componentLabel: String
    let current: ComponentTemperatureStruct
    let history: [TemperatureHistoryPointStruct]
    let canControl: Bool
    let offset: Float?
    let isHidden: Bool
    let maxTemp: Int
}

extension TemperatureSnapshotStruct : Identifiable {
    public var id: String {
        return self.component
    }
}

//extension [TemperatureSnapshotStruct?] : Identifiable {
//    public var id: String {
//        return self.map { $0.id }.joined(separator: "+")
//    }
//}


struct TemperatureHistoryPointStruct: Equatable, Identifiable {
    var id : Date { return time }
    let temperature: Float
    let time: Date
}

extension TemperatureDataRepository.TemperatureSnapshot {
    func toStruct() -> TemperatureSnapshotStruct {
        return TemperatureSnapshotStruct(
            component: component,
            componentLabel: componentLabel,
            current: current.toStruct(),
            history: history.map {
                TemperatureHistoryPointStruct(
                    temperature: $0.temperature,
                    time: $0.time.toDate()
                )
            },
            canControl: canControl,
            offset: offset?.floatValue,
            isHidden: isHidden,
            maxTemp: Int(maxTemp)
        )
    }
}
