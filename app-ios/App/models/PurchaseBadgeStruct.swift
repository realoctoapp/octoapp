//
//  PurchaseBadgeStruct.swift
//  OctoApp
//
//  Created by Christian Würthner on 26/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

enum PurchaseBadgeStruct: Equatable {
    case popular, bestValue, sale, none
}

extension PurchaseBadgeStruct {
    init(badge: PurchaseOffers.Badge?) {
        guard let badge = badge else {
            self = .none
            return
        }
        
        switch badge {
        case .bestvalue: self = .bestValue
        case .popular: self = .popular
        case .sale: self = .sale
        default: self = .none
        }
    }
}
