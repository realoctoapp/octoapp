//
//  BillingPurchaseStruct.swift
//  OctoApp
//
//  Created by Christian Würthner on 27/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct BillingPurchaseStruct {
    var productId: String
    var isSubscription: Bool
}


extension BillingPurchaseStruct {
    init(purchase: BillingPurchase) {
        self.init(
            productId: purchase.productId,
            isSubscription: purchase.isSubscription
        )
    }
}
