//
//  DarwinRemoteConfig.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import FirebaseRemoteConfig

class DarwinNoopRemoteConfigImpl : DarwinRemoteConfig {
    func fetchAndActivate() async throws -> KotlinBoolean {
        false
    }
    
    func getLong(key: String) -> KotlinLong? {
        nil
    }
    
    func getString(key: String) -> String? {
        nil
    }
}

class DarwinRemoteConfigImpl : DarwinRemoteConfig {
    
    private let remoteConfig: RemoteConfig
    
    init(remoteConfig: RemoteConfig) {
        self.remoteConfig = remoteConfig
    }
    
    func getLong(key: String) -> KotlinLong? {
        if remoteConfig.keys(withPrefix: nil).contains(key) {
            let int = remoteConfig.configValue(forKey: key).numberValue.int64Value
            return KotlinLong(longLong: int)
        } else {
            return nil
        }
    }
    
    func getString(key: String) -> String? {
        if remoteConfig.keys(withPrefix: nil).contains(key) {
            return remoteConfig.configValue(forKey: key).stringValue
        } else {
            return nil
        }
    }
    
    func fetchAndActivate() async throws -> KotlinBoolean {
        do {
            Napier.w(tag: "RemoteConfig", message: "Fetching")
            switch try await remoteConfig.fetchAndActivate() {
            case .error: return false
            case .successFetchedFromRemote: return true
            case .successUsingPreFetchedData: return true
            @unknown default: return false
            }
        } catch {
            Napier.w(tag: "RemoteConfig", message: "Failed to fetch: \(error)")
            return false
        }
    }
}
