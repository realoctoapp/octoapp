//
//  DarwinDns.swift
//  App
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

class DarwinDns : CachedDns {
    
    func addCacheEntry(entry: CachedDnsEntry) {
        
    }
    
    func lookup(hostname: String) -> [IpAddress] {
        var ipList: [IpAddress] = []

        if let url = URL(string: "http://\(hostname)/") {
            guard let hostname = url.host else {
                return ipList
            }
            
            guard let host = hostname.withCString({gethostbyname($0)}) else {
                return ipList
            }
            
            guard host.pointee.h_length > 0 else {
                return ipList
            }
            
            var index = 0
            
            while host.pointee.h_addr_list[index] != nil {
                var addr: in_addr = in_addr()
                memcpy(&addr.s_addr, host.pointee.h_addr_list[index], Int(host.pointee.h_length))
                guard let remoteIPAsC = inet_ntoa(addr) else {
                    return ipList
                }
                ipList.append(IpAddress(ip: String.init(cString: remoteIPAsC)))
                index += 1
            }
        }
        
        return ipList
    }
}
