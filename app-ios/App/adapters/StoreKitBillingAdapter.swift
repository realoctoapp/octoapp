//
//  StoreKitBillingAdapter.swift
//  OctoApp
//
//  Created by Christian Würthner on 25/03/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import StoreKit
import WidgetKit

class StoreKitBillingAdapter : BillingAdapter {

    private var purchases : [BillingPurchase] = []
    private var observationTask: Task<Void, Never>? = nil
    private let tag = "StoreKitBillingAdapter"

    func destroyBilling() async throws {
        Napier.d(tag: tag, message: "Disconnecting from StoreKit")
        observationTask?.cancel()
        observationTask = nil
    }
    
    func isAvailable() -> Bool {
        true
    }
    
    func doInitBilling(purchasesUpdated: @escaping ([BillingPurchase]) -> Void, notifyPurchaseCompleted: @escaping () -> Void) async throws {
        Napier.d(tag: tag, message: "Connecting to StoreKit")
        observationTask?.cancel()
        observationTask = Task(priority: .background) {
            for await verificationResult in Transaction.updates {
                // Using verificationResult directly would be better
                // but this way works for this tutorial
                Napier.d(tag: tag, message: "New purchase update received")
                await handle(updatedTransaction: verificationResult)
                purchasesUpdated(purchases)
                Task {
                    try? await Task.sleep(for: .seconds(5))
                    WidgetCenter.shared.reloadAllTimelines()
                }
            }
        }
    }
    
    func loadProducts(productIds: [String]) async throws -> [BillingProduct] {
        Napier.d(tag: tag, message: "Loading products...")
        return try await Product.products(for: productIds).map { product in
            BillingProduct(
                productId: product.id,
                productName: product.displayName,
                price: product.displayPrice,
                priceOrder: (product.price as NSDecimalNumber).int32Value,
                nativeProduct: product
            )
        }
    }
    
    func loadPurchases() async throws -> [BillingPurchase] {
        for await result in Transaction.currentEntitlements {
            Napier.d(tag: tag, message: "Handling entitlement")
            await handle(updatedTransaction: result)
        }
        
        return purchases
    }
    
    // From https://developer.apple.com/documentation/storekit/transaction/3851206-updates
    @MainActor
    private func handle(updatedTransaction verificationResult: VerificationResult<Transaction>) {
        guard case .verified(let transaction) = verificationResult else {
            // Ignore unverified transactions.
            return
        }
        
        // Get the receipt if it's available.

        var receipt: String? = nil
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
           Foundation.FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                receipt = receiptData.base64EncodedString(options: [])
            } catch {
                Napier.e(tag: tag, message: "Failed to get receipt: \(error)")
            }
        } else {
            Napier.e(tag: tag, message: "No receipt available: \(String(describing: Bundle.main.appStoreReceiptURL))")
        }

        if transaction.revocationDate != nil {
            // Remove access to the product identified by transaction.productID.
            // Transaction.revocationReason provides details about
            // the revoked transaction.
            purchases.removeAll(where: { $0.productId == transaction.productID })
        } else if let expirationDate = transaction.expirationDate,
                  expirationDate < Date() {
            // Do nothing, this subscription is expired.
            purchases.removeAll(where: { $0.productId == transaction.productID })
            return
        } else if transaction.isUpgraded {
            // Do nothing, there is an active transaction
            // for a higher level of service.
            return
        } else {
            // Provide access to the product identified by
            // transaction.productID.
            purchases.removeAll(where: { $0.productId == transaction.productID })
            purchases.append(
                BillingPurchase(
                    productId: transaction.productID,
                    isSubscription: transaction.expirationDate != nil,
                    token: receipt,
                    transactionId: transaction.id.formatted(),
                    nativePurchase: transaction
                )
            )
        }
    }
}
