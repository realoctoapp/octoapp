//
//  SmallProgressBar.swift
//  WidgetExtension
//
//  Created by Christian on 02/02/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import SwiftUI

struct SmallProgressBar : View {
    
    @Environment(\.colorScheme) var colorScheme
    var state: PrintActivityAttributes.Data
    var attributes: PrintActivityAttributes
    
    var body: some View {
        let color = .dark == colorScheme ? attributes.instanceColor.dark : attributes.instanceColor.light
        ControlsProgressBarLayout(progress: Double(state.progress) / 100, trailingMin: 0) {
            color.main.clipShape(Capsule())
            Color.clear
        }
        .frame(maxWidth: .infinity)
        .frame(height: 8)
        .background(color.transparent)
        .clipShape(Capsule())
    }
}

//struct SmallProgressBar_Previews: PreviewProvider {
//    static var previews: some View {
//        SmallProgressBar()
//    }
//}
