//
//  ReloadButton.swift
//  WidgetsExtension
//
//  Created by Christian on 04/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents
import SwiftUI
import OctoAppBase

@available(iOSApplicationExtension 17.0, *)
struct ReloadButton : View {
    
    var tint: Color = OctoTheme.colors.darkText
    
    var body: some View {
        SimpleButton(
            intent: ReloadWidgetIntent(),
            iconSystemName: "arrow.counterclockwise",
            tint: tint
        )
    }
}
