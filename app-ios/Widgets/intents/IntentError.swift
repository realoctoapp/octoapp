//
//  IntentError.swift
//  OctoApp
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation

struct IntentError: LocalizedError, CustomLocalizedStringResourceConvertible {
    public var errorDescription: String
    var localizedStringResource: LocalizedStringResource { LocalizedStringResource(stringLiteral: errorDescription) }
}
