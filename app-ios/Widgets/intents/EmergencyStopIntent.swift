//
//  EmergencyStopIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct EmergencyStopIntent: AppIntent {
    static var title: LocalizedStringResource = "Emergency stop"
    static var description = IntentDescription("Triggers an emergency stop")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        try await SharedBaseInjector.shared.get().emergencyStopUseCase().execute(
            param: EmergencyStopUseCase.Params(instanceId: try printer.resolveId())
        )
        return .result()
    }
}
