//
//  StartPrintIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct StartPrintIntent: AppIntent {
    static var title: LocalizedStringResource = "Start a print"
    static var description = IntentDescription("Starts a print for the file with the given path")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    @Parameter(title: "File path")
    var path: String
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        try await SharedBaseInjector.shared.get().startPrintJobUseCase().execute(
            param: StartPrintJobUseCase.Params(
                file: FileObject.File(
                    path: path
                ),
                materialSelectionConfirmed: true,
                timelapseConfigConfirmed: true,
                instanceId: try printer.resolveId()
            )
        )
        return .result()
    }
}
