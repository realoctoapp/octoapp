//
//  TogglePausePrintIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct TogglePausePrintIntent: AppIntent {
    static var title: LocalizedStringResource = "Toggle pause print"
    static var description = IntentDescription("If the printer is printing, pauses the print. If the printer is paused, resumes the print. If the printer is not printing does nothing.")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        try await SharedBaseInjector.shared.get().togglePausePrintJobUseCase().execute(
            param: TogglePausePrintJobUseCase.Params(instanceId: try printer.resolveId())
        )
        return .result()
    }
}
