//
//  ConfigureStatusWidgetIntent.swift
//  OctoApp
//
//  Created by Christian on 06/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import AppIntents

@available(iOS 17.0, *)
@available(iOSApplicationExtension 17.0, *)
struct ConfigureStatusWidgetIntent: WidgetConfigurationIntent {
    static var title: LocalizedStringResource = "Configure"
    static var description = IntentDescription("Configure")

    @Parameter(title: LocalizedStringResource("app_widget___settings___printer"))
    var printer: PrinterEntity?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_name"), default: false)
    var showName: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_print_name"), default: true)
    var showPrintName: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_eta"), default: false)
    var showPrintEta: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_thumbnail"), default: true)
    var showThumbnail: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_snapshot"), default: false)
    var showSnapshot: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_color"), default: true)
    var showColor: Bool?
    
    @Parameter(title: LocalizedStringResource("app_widget___settings___show_reload"), default: false)
    var showReload: Bool?

    init() {}
    
    init(
        printer: PrinterEntity?,
        showName: Bool,
        showPrintEta: Bool,
        showThumbnail: Bool,
        showColor: Bool,
        showReload: Bool
    ) {
        self.printer = printer
        self.showPrintEta = showPrintEta
        self.showName = showName
        self.showColor = showColor
        self.showReload = showReload
    }
}

