//
//  GetPrinterStatusIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct GetStatusIntent: AppIntent {
    static var title: LocalizedStringResource = "Get status"
    static var description = IntentDescription("Fetches the current status")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some ReturnsValue<PrinterStatusEntity> {
        try await BillingManager.shared.throwIfDisabled()
        
        guard let status = try await SharedBaseInjector.shared.get().getWidgetDataUseCase().execute(
            param: GetWidgetDataUseCase.Params(
                instanceId: try printer.resolveId(),
                loadData: true,
                loadSnapshot: false,
                loadThumbnail: false,
                skipCache: true
            )
        ) else {
            throw IntentError(errorDescription: "Status was nil")
        }
        
        
        return .result(value: PrinterStatusEntity(result: status))
    }
}
