//
//  PausePrintIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents

struct PausePrintIntent: AppIntent {
    static var title: LocalizedStringResource = "Pause print"
    static var description = IntentDescription("If the printer is printing, pauses the print. If the printer is not printing does nothing.")
    
    @Parameter(title: "Printer")
    var printer: PrinterEntity
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some IntentResult {
        try await BillingManager.shared.throwIfDisabled()
        
        let printing = try await SharedBaseInjector.shared.get().getWidgetDataUseCase().execute(
            param: GetWidgetDataUseCase.Params(
                instanceId: try printer.resolveId(),
                loadData: true,
                loadSnapshot: false,
                loadThumbnail: false,
                skipCache: true
            )
        )?.state?.flags.printing == true
        
        if printing {
            try await SharedBaseInjector.shared.get().togglePausePrintJobUseCase().execute(
                param: TogglePausePrintJobUseCase.Params(instanceId: try printer.resolveId())
            )
        }
        
        return .result()
    }
}

