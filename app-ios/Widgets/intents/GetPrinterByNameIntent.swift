//
//  GetPrinterByNameIntent.swift
//  WidgetsExtension
//
//  Created by Christian on 05/06/2024.
//  Copyright © 2024 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import AppIntents


struct GetPrinterIntent: AppIntent {
    static var title: LocalizedStringResource = "Get printer by name"
    static var description = IntentDescription("Get printer by name, as shown in the app. Configure the name in OctoPrint's, Mainsail's or Fluidd's appearance settings.")
    
    @Parameter(title: "Name")
    var name: String
    
    init() {
        ensureInit()
    }
    
    func perform() async throws -> some ReturnsValue<PrinterEntity> {
        try await BillingManager.shared.throwIfDisabled()
        
        guard let selected = (SharedBaseInjector.shared.get().printerConfigRepository.getAll().first { $0.label.lowercased() == name.lowercased() }) else {
            throw IntentError(errorDescription: "No printer with name \(name)")
        }
        let entity = PrinterEntity(
            id: selected.id,
            label: selected.label,
            webUrl: selected.webUrl.description()
        )
        return .result(value: entity)
    }
}
