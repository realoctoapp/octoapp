//
//  PrintActivityBundle.swift
//  PrintActivity
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import WidgetKit
import SwiftUI
import OctoAppBase

func ensureInit() {
    guard SharedBaseInjector.shared.getOrNull() == nil else {
        return
    }
    
    ensureInitBase(subsystemSuffix: ".widgets")
}


@main
struct PrintActivityBundle: WidgetBundle {
    
    init() {
        ensureInit()
    }
    
    var body: some Widget {
        PrintActivityLiveActivity()

        if #available(iOSApplicationExtension 17.0, *) {
            WebcamWidget()
            StatusWidget()
        }
    }
}

