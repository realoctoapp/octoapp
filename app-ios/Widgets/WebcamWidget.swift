//
//  PrintActivity.swift
//  PrintActivity
//
//  Created by Christian on 30/01/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import WidgetKit
import SwiftUI
import Intents
import OctoAppBase
import AppIntents

@available(iOSApplicationExtension 17.0, *)
struct WebcamWidgetEntry: TimelineEntry {
    var date: Date = Date()
    var snapshotPath: String? = nil
    var snapshotSuccess: Bool = false
    var usePlaceholderSnapshot: Bool = false
    var status: String? = nil
    var printing: Bool = false
    var completion: Float? = nil
    var printEta: Date? = nil
    var configuration: ConfigureWebcamWidgetIntent
    var label: String? = nil
    var colors: ColorsStruct? = nil
    var errors: String? = nil
    var featureEnabled: Bool = true
    
    static func preview(configuration: ConfigureWebcamWidgetIntent) -> WebcamWidgetEntry {
        WebcamWidgetEntry(
            snapshotSuccess: true,
            usePlaceholderSnapshot: false,
            status: "70%",
            printing: true,
            completion: 0.7,
            configuration: configuration,
            label: configuration.printer?.label
        )
    }
}

@available(iOSApplicationExtension 17.0, *)
private struct WebcamWidgetTimelineProvider: AppIntentTimelineProvider {
    typealias Entry = WebcamWidgetEntry
    typealias Intent = ConfigureWebcamWidgetIntent
    
    func placeholder(in context: Context) -> WebcamWidgetEntry {
        return  WebcamWidgetEntry.preview(configuration: ConfigureWebcamWidgetIntent())
    }
    
    func snapshot(for configuration: ConfigureWebcamWidgetIntent, in context: Context) async -> WebcamWidgetEntry {
        return await loadImage(configuration: configuration, context: context)
    }
    
    func timeline(for configuration: ConfigureWebcamWidgetIntent, in context: Context) async -> Timeline<WebcamWidgetEntry> {
        let entry = await loadImage(configuration: configuration, context: context)
        
        // Generate the next refresh point, 20 mins if printing, 60 if idleBi
        let refresh = Calendar.current.date(byAdding: .minute, value: entry.printing ? 20 : 60, to: Date())!
        return  Timeline(entries: [entry], policy: .after(refresh))
    }
  
    private func loadImage(configuration: ConfigureWebcamWidgetIntent, context: Context) async -> WebcamWidgetEntry {
        guard await BillingManager.shared.isFeatureEnabledAfterStateCheck() || context.isPreview else {
            Napier.i(tag: "WebcamWidget", message: "Feature not enabled, skipping update")
            return WebcamWidgetEntry(configuration: configuration, featureEnabled: false)
        }
       
        var selectedId: String
        guard let printer = configuration.printer else {
            return WebcamWidgetEntry(configuration: configuration)
        }
        
        do {
            selectedId = try printer.resolveId()
        } catch {
            return WebcamWidgetEntry(configuration: configuration)
        }
        
        Napier.i(tag: "WebcamWidget", message: "Loading for \(selectedId)")
        
        let params = GetWidgetDataUseCase.Params(
            instanceId: selectedId,
            loadData: true,
            loadSnapshot: true,
            loadThumbnail: false,
            skipCache: false
        )
        
        do { 
            guard let result = try await SharedBaseInjector.shared.get().getWidgetDataUseCase().execute(param: params) else {
                return WebcamWidgetEntry(configuration: configuration)
            }
            
            return  WebcamWidgetEntry(
                snapshotPath: result.snapshotPath,
                snapshotSuccess: result.snapshotSuccess,
                status: result.statusText,
                printing: result.state?.flags.isPrinting() == true,
                completion: result.progress?.completion?.floatValue,
                printEta: result.progress?.printTimeLeft.flatMap {
                    Calendar.current.date(byAdding: .second, value: $0.intValue, to: Date())
                },
                configuration: configuration,
                label: result.label,
                colors: result.colors?.toStruct(),
                errors: result.errors
            )
        } catch {
            Napier.i(tag: "WebcamWidget", message: "Failed to get data: \(error)")
            return WebcamWidgetEntry(configuration: configuration)
        }
    }
}

@available(iOSApplicationExtension 17.0, *)
private struct WebcamWidgetEntryView : View {
    
    var entry: WebcamWidgetTimelineProvider.Entry
    private var foregroundColor: Color { OctoTheme.colors.white }
    private var fitSnapshot: Bool { entry.configuration.fitSnapshot == true }
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 4) {
            Pill {
                if let name = entry.label,
                   entry.configuration.showName == true {
                    Text(name)
                    Text(" - ")
                }
                
                Text(entry.date, style: .time)
            }
            
            if let status = entry.status,
               entry.configuration.showStatus == true {
                Pill {
                    Text(status)
                }
            }
            
            if let printEta = entry.printEta,
               entry.configuration.showPrintEta == true {
                Pill {
                    HStack(spacing: 0) {
                        Text("progress_widget___eta"~.appending(": "))
                        Text(printEta, style: .time)
                    }
                }
            }
            
            Spacer()
                .frame(maxWidth: .infinity)
            
            ButtonsBar(
                errors: entry.errors,
                showReload: entry.configuration.showReload
            )
        }
        .padding(widgetPadding)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background {
            HStack {
                if entry.configuration.showColor == true {
                    PrinterColorStrip(colors: entry.colors)
                }
                
                Spacer()
            }
        }
        .containerBackground(for: .widget) {
            image
        }
    }
    
    var image: some View {
        ZStack {
            if entry.usePlaceholderSnapshot,
               let image = UIImage(named: "widget_preview_webcam") {
                snapshot(image: image)
            } else if entry.snapshotSuccess,
                      let snapshotPath = entry.snapshotPath,
                      let image = UIImage(contentsOfFile: snapshotPath) {
                snapshot(image: image)
            } else {
                if let snapshotPath = entry.snapshotPath,
                   let image = UIImage(contentsOfFile: snapshotPath) {
                    snapshot(image: image).blur(radius: 10)
                } else if let image = UIImage(named: "widget_preview_webcam") {
                    snapshot(image: image).blur(radius: 10)
                }
                
                Color.clear
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(.ultraThinMaterial)
                
                Image(systemName: "video.slash.fill")
                    .padding(widgetPadding)
                    .foregroundColor(foregroundColor)
            }
        }
    }
    
    @ViewBuilder
    func snapshot(image: UIImage) -> some View {
        if fitSnapshot {
            Image(uiImage: image)
                .resizable()
                .scaledToFit()
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(.ultraThinMaterial)
                .background {
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFill()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
        } else {
            Image(uiImage: image)
                .resizable()
                .scaledToFill()
                .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

@available(iOSApplicationExtension 17.0, *)
struct WebcamWidget: Widget {
    let kind: String = "WebcamWidget"
    
    var body: some WidgetConfiguration {
        AppIntentConfiguration(
            kind: kind,
            intent: ConfigureWebcamWidgetIntent.self,
            provider: WebcamWidgetTimelineProvider()
        ) { entry in
            if entry.featureEnabled {
                WebcamWidgetEntryView(entry: entry)
                    .widgetURL(printer: entry.configuration.printer)
                    .environment(\.colorScheme, .dark)
            } else {
                WidgetDisabled()
            }
        }
        .contentMarginsDisabled()
        .configurationDisplayName("Webcam")
    }
}

@available(iOSApplicationExtension 17.0, *)
struct WebcamWidget_Previews: PreviewProvider {
    static var previews: some View {
        WebcamWidgetEntryView(entry: WebcamWidgetEntry.preview(configuration: ConfigureWebcamWidgetIntent()))
            .padding(-widgetPadding)
            .previewContext(WidgetPreviewContext(family: .systemSmall))
            .previewDisplayName("Small")
        
        WebcamWidgetEntryView(entry: WebcamWidgetEntry.preview(configuration: ConfigureWebcamWidgetIntent()))
            .padding(-widgetPadding)
            .previewContext(WidgetPreviewContext(family: .systemMedium))
            .previewDisplayName("Medium")
        
        WebcamWidgetEntryView(entry: WebcamWidgetEntry.preview(configuration: ConfigureWebcamWidgetIntent()))
            .padding(-widgetPadding)
            .previewContext(WidgetPreviewContext(family: .systemLarge))
            .previewDisplayName("Large")
    }
}
