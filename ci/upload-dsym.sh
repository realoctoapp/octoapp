#!/bin/bash
set -e

# Find dSYMS
DSYMS=`find . -name "*.dSYM"`

# Find plist
GOOGLE_SERVICES_PLIST="app-ios/App/GoogleService-Info.plist"
echo "Google Services: $GOOGLE_SERVICES_PLIST"

# Find script
SCRIPT=`find ~/Library/Developer/Xcode/DerivedData -name "upload-symbols" -print -quit`
if [ -z "$SCRIPT" ]; then
    print "Failed to locate upload-symbols script"
    exit 1
fi

# Upload
echo "  "
echo "----"
for DSYM in $DSYMS; do
    DESC=`dwarfdump -u "$DSYM"`
    echo "Uploading dSYM: $DESC"
    "$SCRIPT" -gsp "$GOOGLE_SERVICES_PLIST" -p ios "$DSYM"
    echo "----"
done