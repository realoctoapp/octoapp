import express from 'express'
import fs from 'fs'
import path from 'path'
import { spawn } from 'child_process'

const app = express()
const port = 9900
app.use(express.json({ limit: '10Mb' }));

var activeClass = null
var activeMethod = null
var recordingProcess = null

const __dirname = path.resolve();
const recordingDir = path.resolve(__dirname, '../test-documentation/')
if (fs.existsSync(recordingDir)) {
    fs.rmSync(recordingDir, { recursive: true, force: true });
}

const tmpRecordingPath =path.resolve(__dirname, '../build/recording.mp4')

app.post('/', (req, res) => {
    const command = req.body.command
    if (!command) {
        res.statusCode = 400
        res.send()
        return
    }

    switch (command) {
        case "start": start(req.body); break;
        case "failure": failed(req.body); break;
        case "success": success(req.body); break;
    }

    res.statusCode = 200
    res.send('OK')
});

function start(req) {
    if (activeClass != req.testClass) {
        activeClass = req.testClass
        activeMethod = null
        console.log(`${req.testClass}`)
    }

    if (activeMethod != req.testMethod) {
        activeMethod = req.testMethod
        console.log(`  📲 ${req.testMethod}`)
        startRecording()
    }
}

function failed(req) {
    if(req.details) {
        console.log(`    🔴 Failure: ${req.details}`)
    } else {
        console.log(`    🔴 Failure`)
    }

    fs.mkdirSync(recordingDir, { recursive: true });

    if (req.logs) {
        let p = path.resolve(recordingDir, `${req.testId}.log`)
        fs.writeFileSync(p, req.logs)
        console.log(`      🗃 Logs saved`)
    }

    if (req.screenshot) {
        let buff = Buffer.from(req.screenshot, 'base64');
        let p = path.resolve(recordingDir, `${req.testId}.webp`)
        fs.writeFileSync(p, buff)
        console.log(`      📸 Screenshot saved`)
    }

    stopRecording(req.testId)
    startRecording()
}

function success(req) {
     if(req.details) {
        console.log(`    🟢 Success: ${req.details}`)
    } else {
        console.log(`    🟢 Success`)
    }
}

function startRecording() {
//    try {
//        stopRecording()
//
//        fs.mkdirSync(path.resolve(tmpRecordingPath, "../"), { recursive: true });
//
//        recordingProcess = spawn("scrcpy", ["-r", tmpRecordingPath, "--video-bit-rate=500000", "--max-size=768", "--max-fps=15", "--no-playback", "--no-audio"])
//        recordingProcess.stderr.on('data', function(data) {
//            if (data.toString().indexOf("WARN: [FFmpeg]") == -1) {
//                data.toString().split("\n").forEach((line) =>  console.log('scrcpy: ' + line))
//            }
//        });
//    } catch(e) {
//        console.log("FAILED TO START RECORDING", e)
//    }
}

function stopRecording(saveForTestId) {
//    try {
//        if (recordingProcess) {
//            recordingProcess.kill('SIGINT')
//            if (saveForTestId) {
//                fs.renameSync(tmpRecordingPath, path.resolve(recordingDir, `${saveForTestId}.mp4`))
//                console.log(`      🎥 Recording saved`)
//            }
//        }
//    } catch(e) {
//        console.log("FAILED TO STOP RECORDING", e)
//    }
}

app.listen(port, () => {
    console.log(`Test documentation listening on port ${port}`)
});