package de.crysxd.octoapp.files

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.By.text
import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManagerTest
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import kotlin.time.Duration.Companion.seconds

class ShowFilesTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())

    @Before
    fun setUp() {
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
    }

    @After
    fun tearDown() {
        BillingManagerTest.enabledForTest = null
    }

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 10)
    fun WHEN_files_are_opened_THEN_files_are_listed() = with(uiDevice) {
        // GIVEN
        BillingManagerTest.enabledForTest = true
        baristaRule.launchActivity()

        // Open files
        waitForPass("Expected start print to be shown") {
            WorkspaceRobot.waitForPrepareWorkspace()
            findObject(By.res(TestTags.BottomBar.Start)).click()
        }
        waitForPass("Expected files title to be shown") {
            val title = requireNotNull(findObject(By.res(TestTags.FileList.Title))) { "Expected title" }
            require(title.text == SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()?.label) { "Expected folder name to be title" }
        }

        waitForPass("Expected thumbnail info") {
            requireNotNull(findObject(text(getString(R.string.file_manager___thumbnail_info___title)))) { "Expected thumbnail info" }
            requireNotNull(findObject(text(getString(R.string.file_manager___thumbnail_info___details)))) { "Expected thumbnail info details" }
        }

        requireNotNull(findObject(text(getString(R.string.hide)))) { "Expected hide button" }.click()

        waitForPass("Expected thumbnail info to be hidden") {
            require(findObject(text(getString(R.string.file_manager___thumbnail_info___title))) == null) { "Expected thumbnail info to be gone" }
            require(findObject(text(getString(R.string.file_manager___thumbnail_info___details))) == null) { "Expected thumbnail info details to be gone" }
        }

        waitForPass("Expected files to be shown") {
            requireNotNull(findObject(By.res(TestTags.FileList.Column))) { "Expected file list" }.swipe(Direction.UP, 0.5f)
            val files = listOf(
                "layers.gcode",
                "test-ü-ä-ö.gcode",
                "test-ñ.gcode",
                "test-#-ü-ñ--% \uD83E\uDD72",
                "test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D.gcode",
                "test-ñ.gcode",
                "test-#-?-%.gcode"
            )
            files.forEach {
                requireNotNull(findObject(text(it))) { "Expected file '$it'" }
            }
        }

        // Check file details
        findObject(text("test-#-?-%.gcode"))!!.click()

        waitForPass("Expected file details") {
            val title = requireNotNull(findObject(By.res(TestTags.FileList.Title))) { "Expected title" }
            require(title.text == "test-#-?-%.gcode") { "Expected folder name to be title, was '${title.text}'" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___tab_info)))) { "Expected info tab" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___tab_preview)))) { "Expected preview tab" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___print_time)))) { "Expected print time" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___model_size)))) { "Expected model size" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___filament_use)))) { "Expected filament use" }
            requireNotNull(findObject(text(getString(R.string.location)))) { "Expected location" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___path)))) { "Expected path" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___file_size)))) { "Expected size" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___uploaded)))) { "Expected uploaded" }
            requireNotNull(findObject(text(getString(R.string.file_manager___file_details___last_print)))) { "Expected last print" }
        }

        // Check Gcode
        requireNotNull(findObject(text(getString(R.string.file_manager___file_details___tab_preview)))) { "Expected preview tab" }.click()
        waitForPass("Expected file details", timeout = 60.seconds) {
            requireNotNull(findObject(text("1 of 198"))) { "Expected layers" }
        }

        pressBack()

        // Open folder and check special file
        waitForPass("Expected folder to be opened") {
            requireNotNull(findObject(text("test-#-ü-ñ--% \uD83E\uDD72"))) { "Expected folder" }.click()
        }

        waitForPass("Expected folder content") {
            requireNotNull(findObject(text("test-#-ü-ñ--% \uD83E\uDD72"))) { "Expected folder" }
            requireNotNull(findObject(text("test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D-ü-ñ--#-%-@.gcode"))) { "Expected file 1" }.click()
        }

        waitForPass("Expected file content") {
            val title = requireNotNull(findObject(By.res(TestTags.FileList.Title))) { "Expected title" }
            require(title.text == "test-\uD83D\uDE0E \uD83E\uDD2F \uD83D\uDE0D-ü-ñ--#-%-@.gcode") { "Expected file name to be title" }
        }
    }
}
