package de.crysxd.octoapp.files

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.By.text
import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.BottomToolbarRobot
import de.crysxd.octoapp.framework.robots.MenuRobot
import de.crysxd.octoapp.framework.robots.MenuRobot.assertMenuItem
import de.crysxd.octoapp.framework.robots.MenuRobot.assertMenuTitle
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsDuration
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import kotlin.time.Duration.Companion.seconds

class StartPrintTest {

    private val testEnvVanilla = TestEnvironmentLibrary.Terrier
    private val testEnvSpoolManager = TestEnvironmentLibrary.Frenchie
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnvSpoolManager, testEnvVanilla))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AutoConnectPrinterRule())

    @Test(timeout = 300_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_THEN_the_app_shows_printing() = with(uiDevice) {
        //region Setup
        BaseInjector.get().octorPrintRepository().setActive(testEnvVanilla)
        baristaRule.launchActivity()
        //endregion
        //region Open file and start print
        "layers.gcode".let { file ->
            triggerPrint(file)
            verifyPrinting(file)
        }
        //endregion
        //region Wait for printing and Pause
        waitForPass("1% print progress", timeout = 60.seconds) {
            assert(findObject(res(TestTags.BottomBar.Status))?.text == 1.formatAsPercent()) { "Expected 1% progress to be shown" }
        }
        assert(hasObject(res(TestTags.BottomBar.Pause))) { "Expected pause to be shown" }
        assert(hasObject(res(TestTags.BottomBar.Cancel))) { "Expected cancel to be shown" }
        assert(!hasObject(res(TestTags.BottomBar.Resume))) { "Expected resume to be hidden" }
        BottomToolbarRobot.confirmButtonWithSwipe(TestTags.BottomBar.Pause)
        //endregion
        //region Wait for paused and resume
        waitForPass("Print paused", timeout = 60.seconds) {
            assert(findObject(res(TestTags.BottomBar.Status))?.text == getString(R.string.paused)) { "Expected paused to be shown" }
        }
        assert(!hasObject(res(TestTags.BottomBar.Pause))) { "Expected pause to be hidden" }
        assert(hasObject(res(TestTags.BottomBar.Cancel))) { "Expected cancel to be shown" }
        assert(hasObject(res(TestTags.BottomBar.Resume))) { "Expected resume to be shown" }
        BottomToolbarRobot.confirmButtonWithSwipe(TestTags.BottomBar.Resume)
        //endregion
        //region Wait for resume and cancel
        waitForPass("2% print progress", timeout = 60.seconds) {
            assert(findObject(res(TestTags.BottomBar.Status))?.text == 2.formatAsPercent()) { "Expected 2% progress to be shown" }
        }
        assert(hasObject(res(TestTags.BottomBar.Pause))) { "Expected pause to be shown" }
        assert(hasObject(res(TestTags.BottomBar.Cancel))) { "Expected cancel to be shown" }
        assert(!hasObject(res(TestTags.BottomBar.Resume))) { "Expected resume to be hidden" }
        BottomToolbarRobot.confirmButtonWithSwipe(TestTags.BottomBar.Cancel)
        WorkspaceRobot.waitForPrepareWorkspace()
        //endregion
    }

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_and_a_spool_is_selected_with_SpoolManager_THEN_the_app_shows_printing() =
        runMaterialTest("SM Spätzle", "PLA\n99g")

//    FilamentManager is not working well since the testenv migration and returns 500. As it's on it's way out we disable the test
//    @Test(timeout = 120_000)
//    @AllowFlaky(attempts = 5)
//    fun WHEN_a_print_is_started_and_a_spool_is_selected_with_Filament_Manager_THEN_the_app_shows_printing() =
//        runMaterialTest("FM Fusili", "ABS\n821g")

    @Test(timeout = 120_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_print_is_started_and_no_spool_is_selected_THEN_the_app_shows_printing() =
        runMaterialTest(InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.material_menu___print_without_selection), "")

    private fun runMaterialTest(selection: String, selectionRightDetail: String) = with(uiDevice) {
        //region Setup
        BaseInjector.get().octorPrintRepository().setActive(testEnvSpoolManager)
        baristaRule.launchActivity()
        //endregion
        //region Open file and start print
        // Use waits.gcode to NOT use material, changing the spool weights
        triggerPrint("waits.gcode")

        // Check dialog
        verifyMaterialSelection()
        MenuRobot.clickMenuButton(label = selection, rightDetail = selectionRightDetail)
        //endregion
        //region Wait for print workspace to be shown
        MenuRobot.waitForMenuToBeClosed()
        verifyPrinting("waits.gcode")
        //endregion
    }

    private fun triggerPrint(file: String) = with(uiDevice) {
        WorkspaceRobot.waitForPrepareWorkspace()
        findObject(res(TestTags.BottomBar.Start)).click()
        findObject(res(TestTags.FileList.Column)).swipe(Direction.UP, 1f)
        waitForPass("Expected file '$file' to be available and openable") {
            findObject(text(file)).click()
            val title = requireNotNull(findObject(res(TestTags.FileList.Title))) { "Expected title" }
            require(title.text == file) { "Expected title to be '$file'" }
        }
        waitForPass("Expected print to be started") {
            requireNotNull(findObject(res(TestTags.FileList.StartPrint))?.click()) { "Expected print button" }
        }
    }

    private fun verifyPrinting(file: String) = with(uiDevice) {
        // Wait for print workspace
        WorkspaceRobot.waitForPrintWorkspace()

        // Wait for print data to show up
        waitForPass(description = "Less then a minute and file name showing", timeout = 10.seconds) {
            assert(hasObject(text(59.formatAsSecondsDuration()))) { "Expected two views with text" }
            assert(hasObject(text(file))) { "Expected file to be mentioned" }
        }
    }

    private fun verifyMaterialSelection() {
        waitForPass("Wait for menu", timeout = 5.seconds) {
            assertMenuTitle(R.string.material_menu___title_select_material)

            listOf(
//                Pair("FM Fusili", "ABS\n821g"),
                Pair("SM Spätzle", "PLA\n99g"),
//                Pair("FM Fusili", "PLA\n1,000g"),
                Pair("SM Ramen", "PLA, Japan\n170g"),
                Pair("SM Ramen", "PLA, Japan\n1,000g"),
                Pair("SM Spaghetti", "PLA\n90g"),
            ).forEachIndexed { index, (title, detail) ->
                assertMenuItem(label = title, rightDetail = detail)
            }

            listOf(
                "Template",
                "Empty",
                "Not Active",
            ).forEachIndexed { index, title ->
                assertMenuItem(label = title, visible = false)
            }
        }
    }
}
