package de.crysxd.octoapp.remoteaccess

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.ConnectRobot
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.exceptions.ngrok.NgrokTunnelNotFoundException
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import io.ktor.http.Url
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class NgrokTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val testEnvRemote = TestEnvironmentLibrary.CorgiRemote
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AcceptAllAccessRequestRule(testEnv))
        .around(AutoConnectPrinterRule())

    @Test//(timeout = 45_000)
    @AllowFlaky(attempts = 1)
    fun WHEN_ngrok_tunnel_was_deleted_THEN_then_we_disconnect_it() = with(uiDevice) {
        // GIVEN
        BaseInjector.get().octorPrintRepository()
            .setActive(testEnvRemote.copy(alternativeWebUrl = "https://297c-31-21-114-230.ap.ngrok.io".toUrl()))

        // WHEN
        baristaRule.launchActivity()

        // THEN
        waitForDialog(withText(NgrokTunnelNotFoundException(Url("http://notused.com")).userFacingMessage))
        Espresso.onView(withText(R.string.sign_in___continue)).perform(ViewActions.click())

        val info = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
        Truth.assertThat(info).isNotNull()
        Truth.assertThat(info!!.alternativeWebUrl).isNull()
        Truth.assertThat(info.octoEverywhereConnection).isNull()

        waitForPass("Waiting for remote access screen") {
            assert(findObject(By.text(getString(R.string.configure_remote_access___title))) != null) { "Expected configure screen shown" }
        }

        assert(pressBack()) { "Expected back to work" }

        ConnectRobot.waitForConnectionState(R.string.connect_printer___octoprint_not_available_title)
    }
}