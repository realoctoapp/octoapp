package de.crysxd.octoapp.login

import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.MockDiscoveryRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain


class BrokenWebsocketTest {

    private val testEnv = TestEnvironmentLibrary.CorgiNoWebsocket
    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(MockDiscoveryRule())
        .around(AutoConnectPrinterRule())
        .around(AcceptAllAccessRequestRule(testEnv))

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_websocket_is_broken_THEN_http_is_used() {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv, trigger = "test")
        baristaRule.launchActivity()

        // Wait for prepare workspace, if the app doesn't switch from WS to HTTP, we are stuck in connect
        WorkspaceRobot.waitForPrepareWorkspace()
    }
}