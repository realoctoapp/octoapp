package de.crysxd.octoapp.login

import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.closeQuietly
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.isActive
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket

class WrongHostSslServer(
    val port: Int = 8000,
) : TestWatcher() {

    private val tag = "WrongHostSslServer/$port"
    private var serverSocket: ServerSocket? = null
    private var serverJob: Job? = null


    override fun starting(description: Description?) {
        Napier.i(tag = tag, message = "Starting...")
        serverSocket?.closeQuietly()
        serverJob?.cancel("New test")
        serverJob = AppScope.launch(Dispatchers.IO) {
            val ss = ServerSocket(port)
            var counter = 0
            serverSocket = ss
            while (currentCoroutineContext().isActive && !ss.isClosed) {
                try {
                    val incoming = ss.accept()
                    val connectionId = counter++
                    launch {
                        Napier.i(tag = tag, message = "New connection: $connectionId")
                        val outgoing = Socket(InetAddress.getByName("octoapp.eu"), 443)
                        listOf(
                            launch { incoming.getInputStream().copyTo(outgoing.getOutputStream()) },
                            launch { outgoing.getInputStream().copyTo(incoming.getOutputStream()) },
                        ).joinAll()
                        Napier.i(tag = tag, message = "Connection closed: $connectionId")
                    }
                } catch (e: Exception) {
                    Napier.w(tag = tag, message = "Error in socket", throwable = e)
                }
            }
        }
        Napier.i(tag = tag, message = "Started")
    }

    override fun failed(e: Throwable?, description: Description?) {
        succeeded(null)
    }

    override fun succeeded(description: Description?) {
        serverSocket?.closeQuietly()
        serverJob?.cancel("Test over")
    }
}