package de.crysxd.octoapp.login

import io.github.aakira.napier.Napier
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.cio.CIO
import io.ktor.server.engine.embeddedServer
import io.ktor.server.request.uri
import io.ktor.server.response.header
import io.ktor.server.response.respondText
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import okhttp3.Credentials
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class BasicAuthServer(
    val port: Int = 8000,
    val username: String = "test",
    val password: String = "test",
) : TestWatcher() {

    private val server = embeddedServer(CIO, port = port) {
        Napier.i(tag = "BasicAuthServer", message = "Setting up")
        routing {
            get("/{...}") {
                Napier.i(tag = "BasicAuthServer", message = "Got request to ${call.request.uri}")
                val credentials = call.request.headers["authorization"] ?: ""
                val credentialsCorrect = credentials == Credentials.basic(username, password)
                when (credentialsCorrect) {
                    false -> call.also {
                        it.response.header("WWW-Authenticate", "Basic realm=\"OctoApp test server\"")
                    }.respondText(
                        contentType = ContentType.Text.Plain,
                        status = HttpStatusCode.Unauthorized,
                    ) { "Mock server says hi!" }

                    true -> call.respondText(
                        contentType = ContentType.Text.Plain,
                        status = HttpStatusCode.NotFound,
                    ) { "Mock server says hi!" }
                }
            }
        }
    }

    override fun starting(description: Description?) {
        Napier.i(tag = "BasicAuthServer", message = "Starting...")
        server.start(wait = false)
        Napier.i(tag = "BasicAuthServer", message = "Started")
    }

    override fun failed(e: Throwable?, description: Description?) = server.stop(gracePeriodMillis = 0, timeoutMillis = 0)

    override fun succeeded(description: Description?) = server.stop(gracePeriodMillis = 0, timeoutMillis = 0)

}