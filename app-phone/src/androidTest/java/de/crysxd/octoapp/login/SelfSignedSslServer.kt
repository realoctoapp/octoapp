package de.crysxd.octoapp.login

import androidx.test.platform.app.InstrumentationRegistry
import io.github.aakira.napier.Napier
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.network.tls.certificates.buildKeyStore
import io.ktor.network.tls.certificates.saveToFile
import io.ktor.server.application.call
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.embeddedServer
import io.ktor.server.engine.sslConnector
import io.ktor.server.engine.stopServerOnCancellation
import io.ktor.server.netty.Netty
import io.ktor.server.request.uri
import io.ktor.server.response.respondText
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.runBlocking
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.slf4j.LoggerFactory
import java.io.File

class SelfSignedSslServer(
    val port: Int = 8000,
    val hosts: List<String> = listOf("127.0.0.1", "0.0.0.0", "localhost")
) : TestWatcher() {

    private val tag = "SelfSignedSslServer/$port"
    private var serverJob: CompletableJob? = null
    private val keyAlias = "key"
    private val keyPassword = "pass"
    private val storePassword = "pass"
    private val keyStoreFile by lazy {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        File(context.cacheDir, "keystore.jks")
    }
    private val server by lazy {
        embeddedServer(Netty, environment = createEnvironment())
    }

    private fun createEnvironment() = applicationEngineEnvironment {
        log = LoggerFactory.getLogger("ktor.application.$port")

        sslConnector(
            keyStore = createKeystore(),
            keyAlias = keyAlias,
            keyStorePassword = { storePassword.toCharArray() },
            privateKeyPassword = { keyPassword.toCharArray() }) {
            port = this@SelfSignedSslServer.port
            keyStorePath = keyStoreFile
        }

        module {
            routing {
                Napier.i(tag = tag, message = "Listening on $port")
                get("/{...}") {
                    Napier.i(tag = tag, message = "Got request to ${call.request.uri}")
                    call.respondText(
                        contentType = ContentType.Text.Plain,
                        status = HttpStatusCode.NotFound,
                    ) { "Mock server says hi!" }
                }
            }
        }
    }

    private fun createKeystore() = try {
        buildKeyStore {
            certificate(keyAlias) {
                password = keyPassword
                domains = hosts
            }
        }.also {
            keyStoreFile.delete()
            keyStoreFile.parentFile?.mkdirs()
            it.saveToFile(keyStoreFile, storePassword)
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to create keystore", throwable = e)
        throw e
    }

    override fun starting(description: Description?) {
        Napier.i(tag = tag, message = "Starting...")
        server.start(wait = false)
        serverJob = server.stopServerOnCancellation(1000, 1000)
        Napier.i(tag = tag, message = "Started")
    }

    override fun failed(e: Throwable?, description: Description?) {
        succeeded(null)
    }

    override fun succeeded(description: Description?) {
        runBlocking {
            serverJob?.cancelAndJoin()
        }
    }
}