package de.crysxd.octoapp.framework.robots

import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.R
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.condition.waitTime
import kotlin.time.Duration.Companion.seconds

object ConnectRobot {

    private val uiDevice get() = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    fun waitForConnectionState(@StringRes title: Int) {
        WorkspaceRobot.waitForConnectWorkspace()
        waitForPass("Wait for state: ${getString(title)}", timeout = 15.seconds) {
            assert(uiDevice.hasObject(By.text(getString(title)))) { "Expected connect action to be shown" }
        }
        waitTime(2000) // Wait to see if we auto connect
    }

    fun useConnectAction(@StringRes action: Int) {
        waitForPass("Wait for connect button", timeout = 2.seconds) {
            assert(uiDevice.hasObject(By.text(getString(action)))) { "Expected text to be shown" }
            uiDevice.findObject(By.res(TestTags.BottomBar.ConnectAction)).click()
        }
    }

    fun assertPsuCanBeControlled(turnedOn: Boolean, doClick: Boolean = false) {
        if (turnedOn) waitForPass("Wait for PSU turn on shown") {
            assert(!uiDevice.hasObject(By.text(getString(R.string.connect_printer___action_turn_psu_off)))) { "Expected turn off button to be missing" }
            assert(uiDevice.hasObject(By.text(getString(R.string.connect_printer___action_turn_psu_on)))) { "Expected turn on button to be shown" }
        } else waitForPass("Wait for PSU turn off shown") {
            assert(uiDevice.hasObject(By.text(getString(R.string.connect_printer___action_turn_psu_off)))) { "Expected turn off button to be shown" }
            assert(!uiDevice.hasObject(By.text(getString(R.string.connect_printer___action_turn_psu_on)))) { "Expected turn on button to be missing" }
        }

        if (doClick) {
            uiDevice.findObject(By.res(TestTags.BottomBar.ConnectAction)).click()
        }
    }

    fun confirmManualConnectionDialog() {
        waitForDialog(withText(R.string.connect_printer___begin_connection_cofirmation_positive))
        onView(withText(R.string.connect_printer___begin_connection_cofirmation_positive)).inRoot(isDialog()).perform(click())
    }

    fun configureDefaultPowerDevice(label: String) {
        useConnectAction(R.string.connect_printer___action_configure_psu)
        MenuRobot.assertMenuTitle(R.string.power_menu___title_select_device)
        MenuRobot.clickMenuButton(label)
        MenuRobot.waitForMenuToBeClosed()
    }
}