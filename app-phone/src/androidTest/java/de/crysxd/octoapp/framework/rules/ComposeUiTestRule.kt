package de.crysxd.octoapp.framework.rules

import de.crysxd.baseui.compose.theme.OctoAppTheme
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class ComposeUiTestRule : TestWatcher() {

    override fun starting(description: Description?) {
        super.starting(description)
        OctoAppTheme.notifyUiTestActive()
    }
}