package de.crysxd.octoapp.framework

import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector

fun getString(@StringRes res: Int, vararg args: Any) = InstrumentationRegistry.getInstrumentation().targetContext.getString(res, *args)

fun idRes(@IdRes id: Int): BySelector {
    val context = InstrumentationRegistry.getInstrumentation().targetContext
    return By.res(context.resources.getResourceName(id))
}

fun BySelector.text(@StringRes res: Int, vararg args: Any): BySelector = text(getString(res, *args))

fun text(@StringRes res: Int, vararg args: Any): BySelector = By.text(getString(res, *args))