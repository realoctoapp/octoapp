package de.crysxd.octoapp.framework.rules

import android.app.Application
import androidx.test.platform.app.InstrumentationRegistry
import de.crysxd.octoapp.base.di.BaseComponent
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.initializeDependencyInjection
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

abstract class AbstractUseCaseMockRule : TestRule {

    protected abstract fun createBaseComponent(base: BaseComponent): BaseComponent

    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application

            try {
                val b = BaseInjector.get()
                val mockBase = createBaseComponent(b)
                initializeDependencyInjection(app, mockBase)
                base.evaluate()
            } finally {
                initializeDependencyInjection(app)
            }
        }
    }
}