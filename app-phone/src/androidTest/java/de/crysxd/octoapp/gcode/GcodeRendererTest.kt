package de.crysxd.octoapp.gcode

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PointF
import android.os.Looper
import android.util.Log
import androidx.annotation.RawRes
import androidx.compose.runtime.mutableStateListOf
import androidx.core.graphics.applyCanvas
import androidx.core.graphics.get
import androidx.test.platform.app.InstrumentationRegistry
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.gcode.GcodeParser
import de.crysxd.octoapp.base.gcode.GcodeRenderContextFactory
import de.crysxd.octoapp.base.gcode.GcodeRenderView
import de.crysxd.octoapp.base.models.GcodeLayer
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.test.R
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import io.ktor.util.encodeBase64
import io.ktor.utils.io.ByteReadChannel
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import java.io.ByteArrayOutputStream

class GcodeRendererTest {

    @get:Rule
    val rule = TestDocumentationRule()

    @Test
    fun WHEN_arcs_gcode_is_rendered_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_arcs_input,
        result = R.raw.gcode_test_arcs_output,
    )

    @Test
    fun WHEN_arcs_2_gcode_is_rendered_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_arcs_2_input,
        result = R.raw.gcode_test_arcs_2_output,
        layerIndex = 1,
    )

    @Test
    fun WHEN_scraper_gcode_is_rendered_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_scraper_input,
        result = R.raw.gcode_test_scraper_output,
        layerIndex = 0,
    )

    @Test
    fun WHEN_layer_gcode_is_rendered_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_layers_input,
        result = R.raw.gcode_test_layers_output,
        layerIndex = 4,
    )

    @Test
    fun WHEN_cube_cube_is_rendered_on_layer_0_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_cube_input,
        result = R.raw.gcode_test_cube_output_0,
        layerIndex = 0,
    )

    @Test
    fun WHEN_cube_gcode_is_rendered_on_layer_1_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_cube_input,
        result = R.raw.gcode_test_cube_output_1,
        layerIndex = 1,
    )

    @Test
    fun WHEN_cube_gcode_is_rendered_on_layer_2_THEN_result_is_correct() = doTest(
        gcode = R.raw.gcode_test_cube_input,
        result = R.raw.gcode_test_cube_output_2,
        layerIndex = 2,
    )

    private fun doTest(
        @RawRes gcode: Int,
        @RawRes result: Int,
        layerIndex: Int = 1,
    ) {
        //region Params
        val resources = InstrumentationRegistry.getInstrumentation().context.resources
        val bytes = resources.openRawResource(gcode).readBytes()
        val expected = BitmapFactory.decodeStream(resources.openRawResource(result))
        val printBedSize = 220f
        val outSize = 1024
        //endregion
        //region Parse Gcode
        val renderContext = runBlocking {
            val layers = mutableStateListOf<GcodeLayer>()
            GcodeParser(
                content = ByteReadChannel(bytes),
                progressUpdate = { println("Parsing $it") },
                totalSize = bytes.size.toLong(),
                layerSink = {
                    layers += it
                    it
                },
            ).parseFile()

            GcodeRenderContext(
                previousLayerPaths = null,
                remainingLayerPaths = null,
                completedLayerPaths = GcodeRenderContextFactory.ForLayerProgress(
                    layerIndex = layerIndex,
                    progress = 1f
                ).loadSingleLayer(
                    layer = layers[layerIndex],
                ).second,
                printHeadPosition = null,
                gcodeBounds = null,
                layerCount = layers.size,
                layerNumber = 0,
                layerZHeight = 0f,
                layerProgress = 1f,
                layerNumberDisplay = { it },
                layerCountDisplay = { it }
            )
        }
        //endregion
        //region Render
        Looper.prepare()
        val view = GcodeRenderView(
            context = InstrumentationRegistry.getInstrumentation().targetContext
        )

        view.renderParams = GcodeRenderView.RenderParams(
            renderContext = renderContext,
            extrusionWidthMm = 0.1f,
            originInCenter = false,
            printBed = GcodeNativeCanvas.Image.PrintBedEnder,
            printBedSizeMm = PointF(printBedSize, printBedSize),
            quality = GcodePreviewSettings.Quality.Ultra,
        )

        val actual = Bitmap.createBitmap(outSize, outSize, Bitmap.Config.ARGB_8888).applyCanvas {
            view.layout(0, 0, outSize, outSize)
            view.draw(this)
        }
        //endregion
        //region Compare
        assert(actual.width == expected.width) { "Expected bitmap width to match result" }
        assert(actual.height == expected.width) { "Expected bitmap height to match result" }
        var diffCount = 0
        for (x in 0..<actual.width) {
            for (y in 0..<actual.height) {
                if (actual[x, y] != expected[x, y]) {
                    diffCount++
                }
            }
        }
        val diffPercent = diffCount / (actual.width * actual.height).toFloat()
        assert(diffPercent < 0.01) {
            val b64 = ByteArrayOutputStream()
                .also { actual.compress(Bitmap.CompressFormat.PNG, 100, it) }
                .toByteArray()
                .encodeBase64()
                .chunked(256)
                .forEach {
                    Log.w("PNG", it)
                }

            "Expected pixel diff to be less than 1%, was ${diffPercent * 100}%\n\n>>$b64<<"
        }
        //endregion
    }
}