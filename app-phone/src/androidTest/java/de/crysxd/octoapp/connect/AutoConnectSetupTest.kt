package de.crysxd.octoapp.connect

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.framework.getString
import de.crysxd.octoapp.framework.robots.MenuRobot.assertMenuTitle
import de.crysxd.octoapp.framework.robots.MenuRobot.clickMenuButton
import de.crysxd.octoapp.framework.robots.MenuRobot.waitForMenuToBeClosed
import de.crysxd.octoapp.framework.robots.WorkspaceRobot.waitForConnectWorkspace
import de.crysxd.octoapp.framework.robots.WorkspaceRobot.waitForPrepareWorkspace
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.condition.waitForPass
import de.crysxd.octoapp.tests.condition.waitTime
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import kotlin.time.Duration.Companion.seconds

class AutoConnectSetupTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val baristaRule = BaristaRule.create(MainActivity::class.java)
    private val uiDevice by lazy { UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()) }

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AcceptAllAccessRequestRule(testEnv))

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_connect_is_opened_for_the_first_time_THEN_user_can_opt_for_manual_connect() = with(uiDevice) {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        waitForReadyToConnect()
        assertSetupMenu(optionToSelect = R.string.connect_printer___auto_menu___manual_option)
        //endregion
        //region THEN
        waitForPrepareWorkspace()
        assert(!SharedBaseInjector.get().preferences.isAutoConnectPrinter) { "Expect auto connection to be disabled" }
        assert(SharedBaseInjector.get().preferences.wasAutoConnectPrinterInfoShown) { "Expect auto tutorial to be marked as shown" }
        //endregion
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_manual_connection_is_started_THEN_confirmation_is_shown() = with(uiDevice) {
        //region GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        SharedBaseInjector.get().preferences.apply {
            isAutoConnectPrinter = false
            wasAutoConnectPrinterInfoShown = true
        }
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        waitForReadyToConnect()
        confirmManualConnection()
        //endregion
        //region THEN
        waitForPrepareWorkspace()
        //endregion
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_connect_is_opened_for_the_first_time_THEN_user_can_opt_for_auto_connect() = with(uiDevice) {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        baristaRule.launchActivity()
        //endregion
        //region WHEN
        waitForReadyToConnect()
        assertSetupMenu(optionToSelect = R.string.connect_printer___auto_menu___auto_option)
        //endregion
        //region THEN
        waitForPrepareWorkspace()
        assert(SharedBaseInjector.get().preferences.isAutoConnectPrinter) { "Expect auto connection to be enabled" }
        assert(SharedBaseInjector.get().preferences.wasAutoConnectPrinterInfoShown) { "Expect auto tutorial to be marked as shown" }
        //endregion
    }

    private fun UiDevice.waitForReadyToConnect() {
        waitForConnectWorkspace()
        waitForPass("Wait for user state", timeout = 15.seconds) {
            assert(hasObject(By.text(getString(R.string.connect_printer___waiting_for_user_title)))) { "Expected connect action to be shown" }
        }
        waitTime(2000) // Wait to see if we auto connect
        waitForPass("Wait for connect button", timeout = 2.seconds) {
            findObject(By.res(TestTags.BottomBar.ConnectAction)).click()
        }
    }

    private fun assertSetupMenu(optionToSelect: Int) {
        assertMenuTitle(R.string.connect_printer___auto_menu___title)
        clickMenuButton(optionToSelect)
        waitForMenuToBeClosed()
    }

    private fun confirmManualConnection() {
        waitForDialog(withText(R.string.connect_printer___begin_connection_cofirmation_positive))
        onView(withText(R.string.connect_printer___begin_connection_cofirmation_positive)).inRoot(isDialog()).perform(click())
    }
}
