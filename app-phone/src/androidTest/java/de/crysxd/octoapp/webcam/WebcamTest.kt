package de.crysxd.octoapp.webcam

import android.graphics.Point
import androidx.test.platform.app.InstrumentationRegistry
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth.assertThat
import de.crysxd.baseui.compose.controls.webcam.frame1Callback
import de.crysxd.baseui.compose.controls.webcam.frame200Callback
import de.crysxd.baseui.compose.controls.webcam.frameCounter
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.base.billing.BillingManagerTest
import de.crysxd.octoapp.base.data.models.ControlType
import de.crysxd.octoapp.base.data.models.ControlsPreferences
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.rules.ComposeUiTestRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.ConditionWatcher
import de.crysxd.octoapp.tests.condition.Instruction
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.takeWhile
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class WebcamTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(ComposeUiTestRule())
        .around(IdleTestEnvironmentRule(testEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())

    @Test(timeout = 60_000)
    @AllowFlaky(attempts = 3)
    fun WHEN_a_webcam_connection_is_made_THEN_frames_are_loaded() = runBlocking {
        val context = InstrumentationRegistry.getInstrumentation().context
        val server = MjpegTestServer()
        BillingManagerTest.enabledForTest = true
        val serverJob = server.start(context)
        val frameTimes = mutableListOf<Long>()
        var lastFrame: Long? = null
        var start = System.currentTimeMillis()
        val expectedFrames = 50
        val lastFrameDimensions = Point()
        try {
            val connection = MjpegConnection3(
                streamUrl = "http://127.0.0.1:${server.port}".toUrl(),
                throwExceptions = true,
                httpSettings = HttpClientSettings(logTag = "Test"),
                maxSize = null,
                name = "Test"
            )
            connection.load().takeWhile {
                // We start 10 frames short to let the connection end gracefully on our (receivers) terms
                frameTimes.size < expectedFrames
            }.collect { state ->
                when (state) {
                    MjpegConnection3.MjpegSnapshot.Loading -> start = System.currentTimeMillis()
                    is MjpegConnection3.MjpegSnapshot.Frame -> {
                        Napier.i("Mjpeg FRAME")
                        lastFrame?.let {
                            frameTimes.add(System.currentTimeMillis() - it)
                        }
                        lastFrameDimensions.x = state.frame.width
                        lastFrameDimensions.y = state.frame.height
                        lastFrame = System.currentTimeMillis()
                    }
                }
            }
        } finally {
            BillingManagerTest.enabledForTest = null
            serverJob.cancel()
            server.stop()

            val end = System.currentTimeMillis()
            val fps = expectedFrames / ((end - start) / 1000f)
            val stats = "avg=${frameTimes.average()}ms, total=${end - start}ms, rate=${fps}fps"
            Napier.i("Webcam stats: $stats")
            TestDocumentationRule.successMetric = stats
            assertThat(frameTimes.size).isEqualTo(expectedFrames)
            assertThat(fps).isAtLeast(15)
            assertThat(lastFrameDimensions.x).isEqualTo(1920)
            assertThat(lastFrameDimensions.y).isEqualTo(1080)
        }
    }

    @Test(timeout = 40_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_a_webcam_connection_is_made_THEN_frames_are_shown() = runBlocking {
        val server = MjpegTestServer()
        val context = InstrumentationRegistry.getInstrumentation().context
        val serverJob = server.start(context)
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = true
        BaseInjector.get().octoPreferences().wasAutoConnectPrinterInfoShown = true
        BillingManagerTest.enabledForTest = true

        // Put webcam on top
        BaseInjector.get().widgetPreferencesRepository().setWidgetOrder(
            "preprint",
            ControlsPreferences(
                "preprint",
                hidden = emptyList(),
                items = listOf(ControlType.WebcamWidget)
            )
        )

        val idle = WebcamIdleInstruction()

        try {
            var start: Long = 0
            var end: Long = 0

            baristaRule.launchActivity()

            // Setup measurement
            frameCounter.set(0)
            frame1Callback = {
                start = System.currentTimeMillis()
            }
            frame200Callback = {
                end = System.currentTimeMillis()

                idle.testCompleted = true
            }

            // This is only to wait until the idle resource is idle (aka enough frames received)
            ConditionWatcher.waitForCondition(idle, 300_000, 500)

            // Compute stats
            assertThat(start).isNotEqualTo(0)
            assertThat(end).isNotEqualTo(0)

            val fps = 1_000 / ((end - start) / 200f)
            val stats = "total=${end - start}ms, rate=${fps}fps"
            Napier.i("Webcam stats: $stats")
            TestDocumentationRule.successMetric = stats

            assertThat(fps).isAtLeast(5)
        } finally {
            BillingManagerTest.enabledForTest = null
            serverJob.cancel()
            server.stop()
        }
    }

    private class WebcamIdleInstruction : Instruction() {
        var testCompleted = false
        override fun getDescription() = "Wait for webcam test"
        override fun checkCondition() = testCompleted
    }
}
