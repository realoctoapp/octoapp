package de.crysxd.octoapp

import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashFragment : Fragment(R.layout.splash_fragment) {

    private var job: Job? = null

    override fun onResume() {
        super.onResume()
        job?.cancel()
        job = lifecycleScope.launch {
            delay(3000)
            throw IllegalStateException("Splash screen active for 3s, app is stuck!!")
        }
    }

    override fun onPause() {
        super.onPause()
        job?.cancel()
    }
}