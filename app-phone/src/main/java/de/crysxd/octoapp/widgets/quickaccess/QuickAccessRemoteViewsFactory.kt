package de.crysxd.octoapp.widgets.quickaccess

import android.content.Context
import android.widget.RemoteViews
import android.widget.RemoteViewsService.RemoteViewsFactory
import androidx.core.content.ContextCompat
import de.crysxd.baseui.ext.drawableRes
import de.crysxd.baseui.ext.foregroundColorRes
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemLibrary
import de.crysxd.octoapp.widgets.ExecuteWidgetActionActivity
import io.github.aakira.napier.Napier
import kotlinx.coroutines.runBlocking

class QuickAccessRemoteViewsFactory(private val context: Context) : RemoteViewsFactory {

    private val tag = "QuickAccessRemoteViewsFactory"
    private val repository = BaseInjector.get().pinnedMenuItemsRepository()
    private val library = MenuItemLibrary()
    private val items
        get() = runBlocking {
            try {
                loadItems()
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to load menu items", throwable = e)
                emptyList()
            }
        }

    override fun onCreate() = Unit
    override fun onDataSetChanged() = Unit
    override fun onDestroy() = Unit
    override fun getCount() = items.size
    override fun getLoadingView() = null
    override fun getViewTypeCount() = 1
    override fun getItemId(position: Int) = items[position].hashCode().toLong()
    override fun hasStableIds() = true

    private suspend fun loadItems(): List<MenuItem> {
        val (instanceId, systemInfo) = SharedBaseInjector.get().printerConfigRepository.getActiveInstanceSnapshot()
            ?.run { id to (systemInfo ?: SystemInfo()) }
            ?: return emptyList()
        val items = repository.getPinnedMenuItems(MenuId.Widget).toList()
        return items.mapNotNull { itemId ->
            library[itemId, instanceId]
        }.sortedBy { item ->
            item.order
        }.filter { item ->
            // Visible anywhere? Visible here
            MenuContext.entries.any { item.isEnabled(it, systemInfo) && item.isVisible(it, systemInfo) }
        }
    }

    override fun getViewAt(position: Int): RemoteViews? = runBlocking {
        val item = items.getOrNull(position) ?: return@runBlocking null

        RemoteViews(context.packageName, R.layout.app_widget_quick_access_item).apply {
            setTextViewText(R.id.title, item.title)
            setInt(R.id.container, "setBackgroundResource", getBackground(item.style))
            setInt(R.id.icon, "setColorFilter", getForeground(item.style))
            setImageViewResource(R.id.icon, item.icon.drawableRes)
            setOnClickFillInIntent(R.id.container, ExecuteWidgetActionActivity.createClickMenuItemFillIntent(item.itemId))
        }
    }

    private fun getBackground(style: de.crysxd.octoapp.menu.MenuItemStyle) = when (style) {
        de.crysxd.octoapp.menu.MenuItemStyle.Support -> R.drawable.quick_access_item_background_support
        de.crysxd.octoapp.menu.MenuItemStyle.Settings -> R.drawable.quick_access_item_background_settings
        de.crysxd.octoapp.menu.MenuItemStyle.OctoPrint -> R.drawable.quick_access_item_background_octoprint
        de.crysxd.octoapp.menu.MenuItemStyle.Printer -> R.drawable.quick_access_item_background_printer
        de.crysxd.octoapp.menu.MenuItemStyle.Neutral -> R.drawable.quick_access_item_background_neutral
    }

    private fun getForeground(style: de.crysxd.octoapp.menu.MenuItemStyle) = ContextCompat.getColor(
        context,
        style.foregroundColorRes
    )
}