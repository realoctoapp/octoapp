package de.crysxd.octoapp

import android.content.Context
import android.os.Looper
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Clock
import okio.sink
import java.io.File
import kotlin.time.Duration.Companion.seconds

class AnrDetector {

    private val logTag = "AnrDetector"

    fun startDetecting(context: Context, scope: CoroutineScope) {
        val lastAnrFile = File(context.cacheDir, "last-anr.txt")
        CreateBugReportUseCase.registerSource("last-anr") {
            if (!lastAnrFile.exists()) return@registerSource null
            CreateBugReportUseCase.BugReport.File(
                bytes = lastAnrFile.readBytes(),
                name = lastAnrFile.name,
            )
        }

        scope.launch(Dispatchers.Default) {
            Napier.d(tag = logTag, message = "ANR detector started")

            // Initial delay, app starts up.
            delay(3.seconds)

            var anrDetected = false
            val checkResult = MutableStateFlow(false)

            while (currentCoroutineContext().isActive) {
                delay(10.seconds)

                val start = Clock.System.now()
                if (BuildConfig.DEBUG) Napier.v(tag = logTag, message = "ANR ping [${Thread.currentThread().name}]")
                checkResult.value = false

                val pongJob = launch(Dispatchers.Main) {
                    if (BuildConfig.DEBUG) Napier.v(tag = logTag, message = "ANR pong: ${Clock.System.now() - start} [${Thread.currentThread().name}]")
                    checkResult.value = true
                }

                val passed = withTimeoutOrNull(3.seconds) {
                    checkResult.first { it }
                } ?: false

                pongJob.cancel("Timed out")

                when {
                    !anrDetected && !passed -> {
                        val stacktrace = Looper.getMainLooper().thread.stackTrace
                        Napier.e(tag = logTag, message = "--------------- ANR detected ---------------")
                        val throwable = SuppressedIllegalStateException("ANR detected!")
                        throwable.stackTrace = stacktrace
                        Napier.e(tag = logTag, message = "ANR detected!", throwable = throwable)
                        anrDetected = true

                        lastAnrFile.outputStream().use { out ->
                            out.writer().write("ANR at: ${Clock.System.now()}\n\n-----\n\n${throwable.stackTraceToString()}\n\n----\n\n")
                            CachedAntiLog.writeCache(out.sink())
                        }
                    }

                    anrDetected && !passed -> Napier.e(tag = logTag, message = "ANR still active!")

                    anrDetected && passed -> {
                        Napier.e(tag = logTag, message = "----------------- ANR over -----------------")
                        anrDetected = false
                    }

                    else -> Unit
                }
            }
        }.invokeOnCompletion {
            Napier.d(tag = logTag, message = "ANR detector stopped")
        }
    }
}