package de.crysxd.octoapp

import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.widgets.quickaccess.QuickAccessAppWidget
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class SettingsActions {

    private val tag = "SettingsActions"

    init {
        AppScope.launch {
            SharedBaseInjector.get().preferences.updatedFlow2
                .map { it.isCrashReportingEnabled }
                .distinctUntilChanged()
                .collectLatest {
                    FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(it)
                }
        }

        AppScope.launch {
            SharedBaseInjector.get().preferences.updatedFlow2
                .map { it.isAnalyticsEnabled }
                .distinctUntilChanged()
                .collectLatest {
                    Firebase.analytics.setAnalyticsCollectionEnabled(it)
                }
        }

        AppScope.launch {
            SharedBaseInjector.get().preferences.updatedFlow2
                .map { it.appTheme }
                .distinctUntilChanged()
                .collectLatest {
                    BaseInjector.get().applyAppThemeUseCase().executeBlocking(Unit)
                }
        }

        AppScope.launch {
            SharedBaseInjector.get().pinnedMenuItemRepository
                .observePinnedMenuItems(MenuId.Widget)
                .collect {
                    Napier.i(tag = tag, message = "Menu items changed, update QuickAccessAppWidget")
                    QuickAccessAppWidget.notifyWidgetDataChanged()
                }
        }

        AppScope.launch {
            SharedBaseInjector.get().printerConfigRepository
                .instanceInformationFlow(instanceId = null)
                .collect {
                    Napier.i(tag = tag, message = "Active instance changed, update QuickAccessAppWidget")
                    QuickAccessAppWidget.notifyWidgetDataChanged()
                }
        }
    }
}