package de.crysxd.octoapp.notification

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import androidx.core.content.edit
import com.google.gson.Gson
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import io.github.aakira.napier.Napier
import io.ktor.serialization.kotlinx.json.DefaultJson
import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlinx.serialization.encodeToString
import kotlin.time.Duration.Companion.seconds

class PrintNotificationController(
    private val notificationFactory: PrintNotificationFactory,
    private val printNotificationIdRepository: NotificationIdRepository,
    private val octoPreferences: OctoPreferences,
    private val printerConfigurationRepository: PrinterConfigurationRepository,
    context: Context
) : ContextWrapper(context) {

    companion object {
        private const val tag = "PrintNotificationController"
        private const val KEY_LAST_PRINT_PREFIX = "last2-"
        internal val instance by lazy {
            val context = BaseInjector.get().localizedContext()
            val repository = BaseInjector.get().octorPrintRepository()
            val octoPreferences = BaseInjector.get().octoPreferences()
            PrintNotificationController(
                context = context,
                notificationFactory = PrintNotificationFactory(context, repository, octoPreferences),
                octoPreferences = BaseInjector.get().octoPreferences(),
                printNotificationIdRepository = BaseInjector.get().notificationIdRepository(),
                printerConfigurationRepository = BaseInjector.get().octorPrintRepository()
            )
        }
    }

    private val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val sharedPreferences = getSharedPreferences("print_notification_cache", Context.MODE_PRIVATE)
    private val gson = Gson()

    init {
        notificationFactory.createNotificationChannels()
    }

    fun ensureNotificationChannelCreated() {
        notificationFactory.createNotificationChannels()
    }

    suspend fun createServiceNotification(instance: PrinterConfigurationV3?, statusText: String): Pair<Notification, Int> {
        val notification = instance?.id?.let { getLast(it) }
            ?.let { notificationFactory.createStatusNotification(instance.id, it, getString(R.string.print_notification___connecting), doLog = true) }
            ?: notificationFactory.createServiceNotification(instance, statusText)
        val id = printNotificationIdRepository.getPrintStatusNotificationId(instance?.id)

        return notification to id
    }

    fun createInitialServiceNotification(instance: PrinterConfigurationV3?): Pair<Notification, Int> {
        val notification = instance?.id?.let { getLast(it) }
            ?.let { notificationFactory.createStatusNotificationWithoutSnapshot(instance.id, it, getString(R.string.print_notification___connecting), doLog = true) }
            ?: notificationFactory.createServiceNotification(instance, getString(R.string.print_notification___connecting))
        val id = printNotificationIdRepository.getPrintStatusNotificationId(instance?.id)

        return notification to id
    }

    suspend fun update(instanceId: String, printState: PrintState?, stateText: String? = null, autoCancelOtherNotification: Boolean = false) {
        val last = getLast(instanceId)
        val proceed = when {
            // Notifications disabled? Drop
            octoPreferences.wasPrintNotificationDisabledUntilNextLaunch -> {
                Napier.v(tag = tag, message = "Dropping update, notifications disabled")
                false
            }

            // No last or posting last? Proceed
            last == null -> {
                Napier.v(tag = tag, message = "Proceeding with update, no last found")
                true
            }

            // New live events always proceed
            printState?.source == PrintState.Source.Live -> {
                Napier.v(tag = tag, message = "Proceeding with update, is live")
                true
            }

            // If the last was from remote and this is from remote (implied) and the source time progressed, then we proceed
            last.source == PrintState.Source.CachedRemote && printState != null && printState.sourceTime > last.sourceTime -> {
                Napier.v(tag = tag, message = "Proceeding with update, new and last is remote and time progressed")
                true
            }

            // If the last was live but is outdated, then we proceed
            last.source == PrintState.Source.CachedLive && liveThreshold > last.sourceTime -> {
                Napier.v(tag = tag, message = "Proceeding with update, last is outdated live event")
                true
            }

            // Reposting? Proceed
            last == printState || printState == null -> {
                Napier.v(tag = tag, message = "Proceeding with update, repost")
                true
            }

            // Else: we drop
            else -> {
                Napier.v(tag = tag, message = "Dropping update, default")
                false
            }
        }

        if (proceed) (printState ?: last)?.let { ps ->
            setLast(instanceId, ps)
            val notificationId = printNotificationIdRepository.getPrintStatusNotificationId(instanceId)
            notificationFactory.createStatusNotification(instanceId, ps, stateText)?.let {
                Napier.v(tag = tag, message = "Showing print notification: instanceId=$instanceId notificationId=$notificationId")
                notificationManager.notify(notificationId, it)
            } ?: Napier.e(
                tag = tag,
                throwable = SuppressedIllegalStateException("Received update event for instance $instanceId but instance was not found"),
                message = "Failed"
            )
        }

        if (printState?.state == PrintState.State.Printing && autoCancelOtherNotification) {
            cancelNotifications(instanceId, temporaryEvent = true, completed = true)
        }
    }

    suspend fun notifyCompleted(instanceId: String, printState: PrintState) = notificationFactory.createPrintCompletedNotification(
        instanceId = instanceId,
        printState = printState
    )?.let {
        notifyIdle(instanceId)

        val notificationId = printNotificationIdRepository.getPrintCompletedNotificationId(instanceId)
        Napier.i(tag = tag, message = "Showing completed notification: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received completed event for instance $instanceId but instance was not found"), message = "Failed")

    suspend fun notifyFilamentRequired(instanceId: String, printState: PrintState) = notificationFactory.createFilamentChangeNotification(
        instanceId = instanceId,
    )?.let {
        update(instanceId, printState, autoCancelOtherNotification = true)

        val notificationId = printNotificationIdRepository.getTemporaryEventNotificationId(instanceId)
        Napier.i(tag = tag, message = "Showing filament runout notification: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received filament event for instance $instanceId but instance was not found"), message = "Failed")

    suspend fun notifyPausedFromGcode(instanceId: String, printState: PrintState) = notificationFactory.createPausedFromGcodeNotification(
        instanceId = instanceId,
    )?.let {
        update(instanceId, printState, autoCancelOtherNotification = true)

        val notificationId = printNotificationIdRepository.getTemporaryEventNotificationId(instanceId)
        Napier.i(tag = tag, message = "Showing paused from Gcode: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received pause from gcode for instance $instanceId but instance was not found"), message = "Failed")

    fun notifyBeep(instanceId: String) = notificationFactory.createBeepNotification(instanceId = instanceId)?.let {
        val notificationId = printNotificationIdRepository.getPermanentEventNotificationId(instanceId)
        if (BaseInjector.get().octoPreferences().notifyPrinterBeep) {
            Napier.i(tag = tag, message = "Showing beep: instanceId=$instanceId notificationId=$notificationId")
            notificationManager.notify(notificationId, it)
        } else {
            Napier.i(tag = tag, message = "Skipping beep notification, disabled")
        }
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received beep for instance $instanceId but instance was not found"), message = "Failed")

    fun notifyLayerCompletion(instanceId: String, layer: Int) = notificationFactory.createLayerCompletionNotification(instanceId = instanceId, layer = layer)?.let {
        val notificationId = printNotificationIdRepository.getPermanentEventNotificationId(instanceId)
        if (layer in BaseInjector.get().octoPreferences().notifyForLayers) {
            Napier.i(tag = tag, message = "Showing layer completion: layer=$layer instanceId=$instanceId notificationId=$notificationId")
            notificationManager.notify(notificationId, it)
        } else {
            Napier.i(tag = tag, message = "Skipping layer notification, disabled for $layer")
        }
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received beep for instance $instanceId but instance was not found"), message = "Failed")

    fun notifyCustom(instanceId: String, message: String) = notificationFactory.createCustomNotification(instanceId = instanceId, text = message)?.let {
        val notificationId = printNotificationIdRepository.getPermanentEventNotificationId(instanceId)
        notificationManager.notify(notificationId, it)
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received beep for instance $instanceId but instance was not found"), message = "Failed")

    suspend fun notifyFilamentSelectionRequired(instanceId: String, printState: PrintState) = notificationFactory.createFilamentSelectionNotification(
        instanceId = instanceId,
    )?.let {
        update(instanceId, printState, autoCancelOtherNotification = true)

        val notificationId = printNotificationIdRepository.getTemporaryEventNotificationId(instanceId)
        Napier.i(tag = tag, message = "Showing filament selection notification: instanceId=$instanceId notificationId=$notificationId")
        notificationManager.notify(notificationId, it)
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received filament event for instance $instanceId but instance was not found"), message = "Failed")

    suspend fun notifyFilamentSelectionCompleted(instanceId: String, printState: PrintState) = notificationFactory.createFilamentSelectionNotification(
        instanceId = instanceId,
    )?.let {
        // Update will clear events
        Napier.i(tag = tag, message = "Clearing filament selection notification: instanceId=$instanceId")
        update(instanceId, printState, autoCancelOtherNotification = true)
    } ?: Napier.e(tag = tag, throwable = IllegalStateException("Received filament event for instance $instanceId but instance was not found"), message = "Failed")

    suspend fun notifyIdle(instanceId: String) {
        clearLast(instanceId)
        cancelNotifications(instanceId, temporaryEvent = true, permanentEvent = true, status = true)
    }

    private suspend fun cancelNotifications(
        instanceId: String?,
        temporaryEvent: Boolean = false,
        permanentEvent: Boolean = false,
        completed: Boolean = false,
        status: Boolean = false
    ) {
        if (status) {
            val printNotificationId = printNotificationIdRepository.getPrintStatusNotificationId(instanceId)
            Napier.v(tag = tag, message = "Cancelling status notification: instanceId=$instanceId notificationId=$printNotificationId")
            LiveNotificationManager.stop(BaseInjector.get().localizedContext())
            notificationManager.cancel(printNotificationId)
        }

        if (temporaryEvent) {
            val eventNotificationId = printNotificationIdRepository.getTemporaryEventNotificationId(instanceId)
            Napier.v(tag = tag, message = "Cancelling temporary event notification: instanceId=$instanceId notificationId=$eventNotificationId")
            notificationManager.cancel(eventNotificationId)
        }

        if (permanentEvent) {
            val eventNotificationId = printNotificationIdRepository.getPermanentEventNotificationId(instanceId)
            Napier.v(tag = tag, message = "Cancelling permanent event notification: instanceId=$instanceId notificationId=$eventNotificationId")
            notificationManager.cancel(eventNotificationId)
        }

        if (completed) {
            val completedNotificationId = printNotificationIdRepository.getPrintCompletedNotificationId(instanceId)
            Napier.v(tag = tag, message = "Cancelling completed notification: instanceId=$instanceId notificationId=$completedNotificationId")
            notificationManager.cancel(completedNotificationId)
        }

        // Give notification system a bit to settle after cancelling...some phones seem to need this (Xiaomi)
        delay(100)
    }

    fun clearLast(instanceId: String) = sharedPreferences.edit { remove("$KEY_LAST_PRINT_PREFIX$instanceId") }

    fun getLast(instanceId: String) = try {
        sharedPreferences.getString("$KEY_LAST_PRINT_PREFIX$instanceId", null)?.let { DefaultJson.decodeFromString<PrintState>(it) }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to get last print state", throwable = e)
        null
    }

    private fun setLast(instanceId: String, printState: PrintState) = sharedPreferences.edit {
        putString("$KEY_LAST_PRINT_PREFIX$instanceId", DefaultJson.encodeToString(printState.copy(source = printState.source.asCached)))
    }

    fun cancelUpdateNotifications() {
        printerConfigurationRepository.getAll().forEach {
            notificationManager.cancel(printNotificationIdRepository.getPrintStatusNotificationId(it.id))
        }
    }

    private val liveThreshold get() = Clock.System.now() - 30.seconds

}
