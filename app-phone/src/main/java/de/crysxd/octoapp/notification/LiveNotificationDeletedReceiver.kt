package de.crysxd.octoapp.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import io.github.aakira.napier.Napier

class LiveNotificationDeletedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        Napier.i(tag = "LiveNotificationDismissedReceiver", message = "Dismissed")
        LiveNotificationManager.pauseNotificationsUntilNextLaunch(context)
    }
}