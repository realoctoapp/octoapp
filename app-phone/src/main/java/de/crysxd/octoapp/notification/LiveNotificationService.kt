package de.crysxd.octoapp.notification

import android.app.ForegroundServiceStartNotAllowedException
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.os.SystemClock
import android.widget.Toast
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.notification.PrintState.Companion.DEFAULT_FILE_NAME
import de.crysxd.octoapp.notification.PrintState.Companion.DEFAULT_FILE_TIME
import de.crysxd.octoapp.notification.PrintState.Companion.DEFAULT_PROGRESS
import de.crysxd.octoapp.sharedcommon.url.isObicoUrl
import de.crysxd.octoapp.widgets.progress.ProgressAppWidget
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.seconds

class LiveNotificationService : Service() {

    companion object {
        private const val tag = "LiveNotificationService"
        const val ACTION_HIBERNATE = "de.crysxd.octoapp.notification.PrintNotificationService.HIBERNATE"
        const val ACTION_WAKE_UP = "de.crysxd.octoapp.notification.PrintNotificationService.WAKE_UP"
        const val DISCONNECT_IF_NO_MESSAGE_FOR_MS = 90_000L // Moonraker updates every 60s
        const val RETRY_DELAY = 2_000L
        const val CHECK_PRINT_ENDED_DELAY = 1_000L
        const val RETRY_COUNT = 3L
    }

    private val tag = "$this"
    private val notificationController by lazy { PrintNotificationController.instance }
    private val octoPreferences by lazy { BaseInjector.get().octoPreferences() }
    private val instance by lazy {
        // Instance is fixed for this service. When the active instance is changed the service restarts due to settings change
        BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()
    }
    private val requireInstance by lazy {
        requireNotNull(instance) { "Instance was null" }
    }
    private val printerProvider = BaseInjector.get().octoPrintProvider()
    private val eventFlow by lazy {
        printerProvider.eventFlow(instanceId = requireInstance.id, tag = "notification-service", config = EventSource.Config(throttle = 10))
    }
    private val passiveEventFlow by lazy {
        printerProvider.passiveCurrentMessageFlow(instanceId = requireInstance.id, tag = "notification-service-passive")
    }

    private val coroutineJob = SupervisorJob()
    private val coroutineScope = CoroutineScope(coroutineJob + Dispatchers.Main.immediate)
    private var markDisconnectedJob: Job? = null
    private var lastMessageReceivedAt: Long? = null
    private var eventFlowJob: Job? = null
    private var checkPrintEndedJob: Job? = null
    private var printing = false
    private var reconnectionAttempts = 0

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        BaseInjector.get().octoPreferences().wasPrintNotificationDisconnected = false

        // Start notification. We ALWAYS need to do this to prevent a crash if we stop
        // before showing the notification
        notificationController.ensureNotificationChannelCreated()
        val (notification, notificationId) = notificationController.createInitialServiceNotification(instance)
        Napier.i(tag = tag, message = "Starting foreground with: $notification")
        try {
            startForeground(notificationId, notification)
        } catch (e: Exception) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && e is ForegroundServiceStartNotAllowedException) {
                // Android 12 issue...we can't start a foreground service from an Activity sometimes?
                Napier.w(tag = tag, message = "Unable to start foreground service at the moment", throwable = e)
            } else {
                Napier.e(tag = tag, message = "Failed to start", throwable = e)
            }
            return stop()
        }

        coroutineJob.invokeOnCompletion {
            Napier.i(tag = tag, message = "Job stopped")
        }

        // Ensure instance is set from here on
        instance ?: return let {
            Napier.w(tag = tag, message = "Active instance is null, stopping")
            stop()
        }

        if (LiveNotificationManager.isNotificationEnabled) {
            Napier.i(tag = tag, message = "Creating notification service")

            coroutineScope.launch {
                // Check preconditions
                if (!checkPreconditions()) {
                    Napier.i(tag = tag, message = "Preconditions not met, stopping self")
                    notificationController.clearLast(requireInstance.id)
                    stop()
                } else {
                    Napier.i(tag = tag, message = "Preconditions, allowing connection")
                }

                listenOnEventFlow()
            }

            observePreferences()
            observeConnectionChanges()
        } else {
            Napier.i(tag = tag, message = "Notification service disabled, skipping creation")
            stop()
        }
    }

    private suspend fun checkPreconditions(): Boolean = try {
        if (!LiveNotificationManager.isNotificationEnabled) {
            false
        } else {
            val flags = BaseInjector.get().octoPrintProvider().printer(requireInstance.id).printerApi.getPrinterState().state.flags
            val isPrinting = flags.isPrinting()

            // Not printing? Make sure to clear the notification
            if (!isPrinting) {
                Napier.e(tag = tag, message = "Not printing: $flags")

                instance?.id?.let {
                    notificationController.notifyIdle(it)
                }
            }

            isPrinting
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to check preconditions", throwable = e)
        false
    }

    private fun observePreferences() = coroutineScope.launch {
        octoPreferences.updatedFlow.collectLatest {
            if (!octoPreferences.isLivePrintNotificationsEnabled || octoPreferences.activeInstanceId != requireInstance.id) {
                Napier.i(tag = tag, message = "Settings changed, restarting")
                LiveNotificationManager.restart(this@LiveNotificationService)
            }
        }
    }

    private fun observeConnectionChanges() = coroutineScope.launch {
        val octoPrint = BaseInjector.get().octoPrintProvider().printer(instanceId = requireInstance.id)
        val context = this@LiveNotificationService
        octoPrint.baseUrl.collect {
            try {
                if (it.isObicoUrl() && octoPrint.asOctoPrint().obicoApi.getDataUsage().hasDataCap) {
                    // Limited tunnel, we can't use cap
                    Napier.i(tag = tag, message = "Live notification paused, Obico tunnel has limited data")
                    Toast.makeText(context, getString(R.string.configure_remote_acces___spaghetti_detective___live_notification_paused), Toast.LENGTH_SHORT).show()
                    LiveNotificationManager.pauseNotificationsUntilNextLaunch(context)
                }
            } catch (e: java.lang.Exception) {
                Napier.e(tag = tag, message = "Failed", throwable = e)
            }
        }
    }

    private fun listenOnEventFlow() {
        // Hook into event flow to receive updates
        eventFlowJob?.cancel()
        reconnectionAttempts = 0
        lastMessageReceivedAt = null
        val job = Job(coroutineJob)
        eventFlowJob = job

        val notificationLayoutFlow = octoPreferences.updatedFlow2.map { it.notificationsLayout }.distinctUntilChanged()

        coroutineScope.launch(Dispatchers.Default + job) {
            eventFlow.onStart {
                Napier.i(tag = tag, message = "Event flow connected")
            }.onCompletion {
                Napier.i(tag = tag, message = "Event flow disconnected")
            }.onEach {
                onEventReceived(it)
            }.collectWithRetry()
        }

        coroutineScope.launch(Dispatchers.Default + job) {
            passiveEventFlow.onStart {
                Napier.i(tag = tag, message = "Current flow connected")
            }.onCompletion {
                Napier.i(tag = tag, message = "Current flow disconnected")
            }.combine(notificationLayoutFlow) { current, layout ->
                current to layout
            }.distinctUntilChangedBy {
                // Schedule stop if we don't receive the next message soon
                instance?.id?.let { instanceId ->
                    Napier.v(tag = tag, message = "Got current message")
                    markDisconnectedJob?.cancel()
                    markDisconnectedJob = coroutineScope.launch {
                        delay(DISCONNECT_IF_NO_MESSAGE_FOR_MS)
                        Napier.i(tag = tag, message = "No updates for ${DISCONNECT_IF_NO_MESSAGE_FOR_MS}ms, moving notification to disconnected state")
                        notificationController.update(instanceId = instanceId, printState = null, stateText = getString(R.string.print_notification___disconnected))
                    }
                }

                // Only let relevant changes pass: Print time change > 1 min, progress change, job change, state change
                it.relevantHash()
            }.onEach { (current, layout) ->
                handleCurrentMessage(current)
            }.collectWithRetry()
        }
    }

    private fun Pair<Message.Current, String>.relevantHash() = 234 +
            first.state.flags.hashCode() +
            first.job.hashCode() +
            second.hashCode() +
            first.progress?.copy(
                completion = first.progress?.completion?.roundToInt()?.toFloat(),
                printTimeLeft = (first.progress?.printTimeLeft ?: 0) / 60,
                printTime = (first.progress?.printTime ?: 0) / 60,
                filepos = 0,
            ).hashCode()


    private suspend fun Flow<*>.collectWithRetry() = retry(RETRY_COUNT) {
        Napier.e(tag = tag, message = "Fault in event flow. Retrying after ${RETRY_DELAY}ms", throwable = it)
        delay(RETRY_DELAY)
        true
    }.catch {
        Napier.e(tag = tag, message = "Event flow died. Moving into hibernation", throwable = it)
        octoPreferences.wasPrintNotificationDisconnected = true
        hibernate()
    }.collect()

    override fun onDestroy() {
        Napier.i(tag = tag, message = "Service is being destroyed")
        coroutineScope.launch {
            super.onDestroy()
            val keepNotification = instance?.hasCompanionPlugin() == true && printing
            if (keepNotification) {
                notificationController.update(instanceId = requireInstance.id, printState = null)
                stopForeground(STOP_FOREGROUND_DETACH)
            } else {
                stopForeground(STOP_FOREGROUND_REMOVE)
            }

            LiveNotificationManager.startTime = 0
            ProgressAppWidget.notifyWidgetDataChanged()
        }.invokeOnCompletion {
            coroutineJob.cancel("Service stopped")
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_HIBERNATE -> {
                eventFlowJob?.cancel()
                Napier.i(tag = tag, message = "Hibernating now")
            }

            ACTION_WAKE_UP -> {
                listenOnEventFlow()
                Napier.i(tag = tag, message = "Woken up")
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun stop() {
        Napier.i(tag = tag, message = "Stopping self")
        LiveNotificationManager.stop(this)
    }

    private fun hibernate() {
        Napier.i(tag = tag, message = "Triggering hibernation")
        LiveNotificationManager.hibernate(this)
    }

    private fun onEventReceived(event: Event) = when (event) {
        is Event.Disconnected -> handleDisconnectedEvent(event)
        is Event.Connected -> handleConnectedEvent()
        is Event.MessageReceived -> Unit // Done via separate flow
    }

    private fun handleDisconnectedEvent(event: Event.Disconnected) {
        ProgressAppWidget.notifyWidgetOffline()
        lastMessageReceivedAt = lastMessageReceivedAt ?: SystemClock.uptimeMillis()
        val secsSinceLastMessage = TimeUnit.MILLISECONDS.toSeconds(SystemClock.uptimeMillis() - (lastMessageReceivedAt ?: 0))
        when {
            lastMessageReceivedAt == null && secsSinceLastMessage >= 10 && reconnectionAttempts >= 3 -> {
                Napier.w(
                    tag = tag,
                    throwable = event.exception,
                    message = "Unable to connect within ${secsSinceLastMessage}s and after $reconnectionAttempts attempts, going into hibernation"
                )
                BaseInjector.get().octoPreferences().wasPrintNotificationDisconnected = true
                hibernate()
            }

            secsSinceLastMessage >= 60 && reconnectionAttempts >= 3 -> {
                Napier.i(tag = tag, message = "No connection since ${secsSinceLastMessage}s and after $reconnectionAttempts attempts, going into hibernation")
                BaseInjector.get().octoPreferences().wasPrintNotificationDisconnected = true
                hibernate()
            }

            else -> {
                Napier.i(tag = tag, message = "No connection since ${secsSinceLastMessage}s, attempting to reconnect")
                reconnectionAttempts++
            }
        }
    }

    private fun handleConnectedEvent() {
        Napier.i(tag = tag, message = "Connected")
        reconnectionAttempts = 0
    }

    private suspend fun handleCurrentMessage(message: Message.Current) = instance?.id?.let { instanceId ->
        lastMessageReceivedAt = SystemClock.uptimeMillis()
        ProgressAppWidget.notifyWidgetDataChanged(message)

        Napier.v(tag = tag, message = "Updating")

        // Update notification
        if (message.state.flags.isPrinting()) {
            checkPrintEndedJob?.cancel()
            checkPrintEndedJob = null
            // We are printing, update notification
            val print = message.toPrint()
            printing = true
            notificationController.update(instanceId, print, autoCancelOtherNotification = true)
        } else if (checkPrintEndedJob == null) {
            // We are no longer printing.
            checkPrintEndedJob = coroutineScope.launch {
                try {
                    delay(CHECK_PRINT_ENDED_DELAY)
                    if (checkPreconditions()) {
                        Napier.i(tag = tag, message = "Print still active, resuming")
                    } else {
                        Napier.i(tag = tag, message = "Print ended, winding down")

                        // If the print is done and we saw the print printing in the last state, notify
                        // We don't check for print done events if OctoApp plugin is installed
                        if (instance?.hasPlugin(OctoPlugins.OctoApp) == true) {
                            Napier.i(tag = tag, message = "Skipping print completed check, companion configured")
                        } else {
                            notificationController.getLast(instanceId)?.let { last ->
                                val current = message.toPrint()
                                if (last.objectId == current.objectId && current.progress >= 100) {
                                    notificationController.notifyCompleted(instanceId, current)
                                    Napier.i(tag = tag, message = "Print completed")
                                } else {
                                    Napier.i(
                                        tag = tag,
                                        message = "Print not active but did not complete: lastId=${last.objectId} ${current.objectId} progress=${current.progress}"
                                    )
                                }
                            } ?: Napier.w(tag = tag, message = "No last notification, skipping print done")
                        }

                        notificationController.clearLast(instanceId)
                        printing = false
                        stop()
                    }
                } finally {
                    checkPrintEndedJob = null
                }
            }
        }
    }

    private fun Message.Current.toPrint() = PrintState(
        fileDate = job?.file?.date?.toEpochMilliseconds() ?: DEFAULT_FILE_TIME,
        fileName = job?.file?.name ?: DEFAULT_FILE_NAME,
        source = PrintState.Source.Live,
        state = when {
            state.flags.cancelling -> PrintState.State.Cancelling
            state.flags.pausing -> PrintState.State.Pausing
            state.flags.paused -> PrintState.State.Paused
            else -> PrintState.State.Printing
        },
        sourceTime = Clock.System.now(),
        appTime = Clock.System.now(),
        eta = progress?.printTimeLeft?.let { Clock.System.now() + it.seconds },
        progress = progress?.completion ?: DEFAULT_PROGRESS,
    )
}
