package de.crysxd.octoapp

import android.content.Context
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import io.github.aakira.napier.Napier
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import okio.sink
import java.io.File

class CrashDetector {

    fun startDetecting(context: Context) {
        val lastCrashFile = File(context.cacheDir, "last-crash.txt")
        CreateBugReportUseCase.registerSource("last-crash") {
            if (!lastCrashFile.exists()) return@registerSource null
            CreateBugReportUseCase.BugReport.File(
                bytes = lastCrashFile.readBytes(),
                name = lastCrashFile.name,
            )
        }
        val wrapped = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            runBlocking {
                Napier.e(tag = "Uncaught!", throwable = e, message = "Uncaught exception")
                lastCrashFile.outputStream().use { out ->
                    out.writer().write("Crashed at: ${Clock.System.now()}\n\n-----\n\n${e.stackTraceToString()}\n\n----\n\n")
                    CachedAntiLog.writeCache(out.sink())
                }
            }

            wrapped?.uncaughtException(t, e)
        }
    }
}