package de.crysxd.octoapp

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import androidx.core.content.res.ResourcesCompat
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.PlayBillingAdapter
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.fetchAndActivateSafe
import de.crysxd.octoapp.base.logging.AndroidFirebaseAdapter
import de.crysxd.octoapp.base.logging.CachedAntiLog
import de.crysxd.octoapp.base.logging.FirebaseAntiLog
import de.crysxd.octoapp.base.logging.LogcatAntiLog
import de.crysxd.octoapp.base.migrations.AllMigrations
import de.crysxd.octoapp.base.migrations.AllSharedMigrations
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.notification.PrintNotificationSupportBroadcastReceiver
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.utils.MultiplatformStrings
import de.crysxd.octoapp.wear.WearDataLayerService
import de.crysxd.octoapp.widgets.AppWidgetSupportBroadcastReceiver
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Locale

class OctoApp : Application() {

    private val tag = "OctoApp"

    override fun onCreate() {
        super.onCreate()
        // Setup logging
        if (BuildConfig.BUILD_TYPE != "release") {
            Napier.base(LogcatAntiLog)
        }
        Napier.base(CachedAntiLog)
        Napier.base(FirebaseAntiLog)
        FirebaseAntiLog.initAdapter(AndroidFirebaseAdapter())

        // Setup Firebase crash fix
        setupFirebaseCrashFix()

        // Detect ANR and crashes
        CrashDetector().startDetecting(this)

        // Setup DI
        initializeDependencyInjection(this)

        // Setup billing
        BillingManager.adapter = PlayBillingAdapter(this)

        // Start writing to disk
        CachedAntiLog.setUpDiskCache()

        // Migrate
        runBlocking {
            AllMigrations(this@OctoApp).migrate()
            AllSharedMigrations().migrate()
        }

        // Setup language
        MultiplatformStrings.locale = Locale.forLanguageTag(BaseInjector.get().getAppLanguageUseCase().executeBlocking(Unit).appLanguage)

        // Dark mode, must be done sync
        BaseInjector.get().applyAppThemeUseCase().executeBlocking(Unit)

        // Setup SerialCommunicationLogsRepository (jsut create the instance)
        BaseInjector.get().serialCommunicationLogsRepository()

        AppScope.launch {
            // Setup SerialCommunicationLogsRepository (jsut create the instance)
            BaseInjector.get().serialCommunicationLogsRepository()

            // BroadcastReceiver to support widgets and print notification (will register itself)
            AppWidgetSupportBroadcastReceiver(this@OctoApp)
            PrintNotificationSupportBroadcastReceiver().install(this@OctoApp)

            // Setup RemoteConfig
            Firebase.remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
            Firebase.remoteConfig.setConfigSettingsAsync(remoteConfigSettings {
                minimumFetchIntervalInSeconds = if (BuildConfig.DEBUG) 10 else 3600
            })

            AppScope.launch {
                val success = Firebase.remoteConfig.fetchAndActivateSafe()
                Napier.i(tag = tag, message = "Complete remote config fetch. Success: $success")
            }

            // Register default notification channel
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.deleteNotificationChannel("alerts") // Legacy channel, to be deleted
            manager.createNotificationChannel(
                NotificationChannel(
                    getString(R.string.updates_notification_channel),
                    getString(R.string.updates_notification_channel___updates),
                    NotificationManager.IMPORTANCE_HIGH
                )
            )

            // Setup FCM
            Firebase.messaging.token.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Napier.i(tag = "FCM", message = "Token: ${task.result}")
                } else {
                    Napier.w(tag = "FCM", message = "Unable to get token")
                }
            }

            // Migrate user id
            val installId = BaseInjector.get().octoPreferences().installId
            Napier.i(tag = tag, message = "Signed in as $installId")
            Firebase.crashlytics.setUserId(installId)
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.AndroidBuild, Build.FINGERPRINT)

            // Setup analytics
            // Do not enable if we are in a TestLab environment
            val debugBuild = BuildConfig.BUILD_TYPE.lowercase() != "release"
            val crashReportingEnabled = BaseInjector.get().octoPreferences().isCrashReportingEnabled && !debugBuild
            Firebase.crashlytics.setCrashlyticsCollectionEnabled(crashReportingEnabled)
            val analyticsSuppressed = Settings.System.getString(contentResolver, "firebase.test.lab") == "true" || debugBuild
            val analyticsEnabled = BaseInjector.get().octoPreferences().isAnalyticsEnabled && !analyticsSuppressed
            val beta = SharedCommonInjector.get().platform.betaBuild.toString()
            Firebase.analytics.setUserProperty("beta_release", beta)
            Firebase.analytics.setAnalyticsCollectionEnabled(analyticsEnabled)
            if (BuildConfig.DEBUG) {
                Firebase.analytics.setUserProperty("debug", "true")
            }
            Napier.i(
                tag = tag,
                message = "Crash reporting: $crashReportingEnabled Analytics: $analyticsEnabled (debugBuild=$debugBuild buildType=${BuildConfig.BUILD_TYPE})"
            )
            // Connect to test notifications
            if (BuildConfig.DEBUG) {
                Napier.i(tag = tag, message = "Subscribed to debug notifications")
                Firebase.messaging.subscribeToTopic("debug_notifications")
            }
        }

        // Pre-load fonts in background. This will allow us later to asyn inflate views as loading fonts will need a Handler
        // After being loaded once, they are in cache
        val handler = Handler(Looper.getMainLooper())
        val callback = object : ResourcesCompat.FontCallback() {
            override fun onFontRetrievalFailed(reason: Int) = Unit
            override fun onFontRetrieved(typeface: Typeface) = Unit
        }
        ResourcesCompat.getFont(this@OctoApp, R.font.roboto_medium, callback, handler)
        ResourcesCompat.getFont(this@OctoApp, R.font.roboto_light, callback, handler)
        ResourcesCompat.getFont(this@OctoApp, R.font.roboto_regular, callback, handler)


        // Prefetch tutorials
        AppScope.launch {
            try {
                BaseInjector.get().tutorialsRepository().getTutorials(skipCache = false)
            } catch (e: Exception) {
                Napier.w(tag = tag, message = "Unable to fetch tutorials", throwable = e)
            }
        }

        // Setup Wear support
        AppScope.launch {
            WearDataLayerService(this@OctoApp)
        }

        // Update app language property
        AppScope.launch {
            val appLanguage = BaseInjector.get().getAppLanguageUseCase().execute(Unit).appLanguage
            OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.AppLanguage, appLanguage)
        }

        // Delete all cache files
        AppScope.launch {
            BaseInjector.get().publicFileDirectory().deleteRecursively()
        }

        // Apply settings live
        SettingsActions()
    }
}