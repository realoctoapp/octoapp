import com.android.build.api.dsl.ManagedVirtualDevice
import com.github.triplet.gradle.androidpublisher.ReleaseStatus
import de.crysxd.octoapp.buildscript.getPlatyCredentials
import de.crysxd.octoapp.buildscript.getPlayFraction
import de.crysxd.octoapp.buildscript.getPlayTrack
import de.crysxd.octoapp.buildscript.getPlayUpdatePriority
import de.crysxd.octoapp.buildscript.getVersionCodePhone
import de.crysxd.octoapp.buildscript.getVersionName
import de.crysxd.octoapp.buildscript.localProperties
import de.crysxd.octoapp.buildscript.octoAppAndroidApp

plugins {
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.kotlinKapt)
    alias(libs.plugins.kotlinSerialization)
    alias(libs.plugins.kotlinComposeCompiler)
    alias(libs.plugins.googleServices)
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.firebaseCrashlytics)
    alias(libs.plugins.kotlinParcelize)
    alias(libs.plugins.playPublisher)
}

octoAppAndroidApp(getVersionCodePhone()) {
    @Suppress("UnstableApiUsage")
    testOptions {
        managedDevices {
            devices {
                create<ManagedVirtualDevice>("pixel4aApi31") {
                    device = "Pixel 4a"
                    apiLevel = 31
                    systemImageSource = localProperties.getProperty("integrationTests.managedDevideSysteImageSource", "aosp")
                }
            }
        }
    }
}

play {
    serviceAccountCredentials.set(getPlatyCredentials())
    track.set(getPlayTrack())
    defaultToAppBundles.set(true)
    releaseStatus.set(if (getPlayFraction() >= 1.0) ReleaseStatus.COMPLETED else ReleaseStatus.IN_PROGRESS)
    updatePriority.set(getPlayUpdatePriority())
    userFraction.set(getPlayFraction())
    releaseName.set(getVersionName())
}

dependencies {
    implementation(projects.baseUi)
    implementation(projects.featureSignIn)
    implementation(projects.featureHelp)
    implementation(projects.featurePluginSupport)

    // Mock servers
    androidTestImplementation(libs.ktor.server.core)
    androidTestImplementation(libs.ktor.server.netty)
    androidTestImplementation(libs.ktor.server.cio)
    androidTestImplementation(libs.ktor.network.tls)

    // Baseline profiles
    implementation(libs.androidx.profiles)
}
