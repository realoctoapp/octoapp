package de.crysxd.octoapp.sharedcommon.utils

import de.crysxd.octoapp.sharedcommon.ext.format
import platform.Foundation.NSBundle

private object Cache {
    val htmlLinkRegex by lazy { Regex("<a.*?href=[\"']([^\"']*)[\"'][^>]*>([^<]*)</a>") }
    val fontRegex by lazy { Regex("<font[^>]*>([^<]*)</font>") }
    val cache = mutableMapOf<String, String>()
}

actual fun getString(id: String, vararg formatArgs: Any): String {
    return try {
        val raw = Cache.cache.getOrPut(id) {
            fun String.takeIfSuccess() = takeUnless { it == id || it.isEmpty() }
            val format = NSBundle.mainBundle.localizedStringForKey(id, id, null).takeIfSuccess()
                ?: NSBundle.mainBundle.localizedStringForKey(id, id, "EnglishOnly").takeIfSuccess()
                ?: NSBundle.mainBundle.localizedStringForKey(id, id, "Uris").takeIfSuccess()

            format
                ?.takeIfSuccess()
                ?.parseHtml()
                ?.toString()
                ?.trim()
                ?: id
        }

        raw.format(*formatArgs)
    } catch (e: Exception) {
        io.github.aakira.napier.Napier.e(tag = "getString", message = "Exception in generating string", throwable = e)
        id
    }
}

actual fun CharSequence.parseHtml(): CharSequence = this.toString()
    .replace("<b>", "**")
    .replace("</b>", "**")
    .replace("</u>", "")
    .replace("<u>", "")
    .replace("<strong>", "**")
    .replace("</strong>", "**")
    .replace("<p>", "")
    .replace("</p>", "\n\n")
    .replace("\\\"", "\"")
    .replace("<small>", "")
    .replace("</small>", "")
    .replace("<br>", "\n")
    .replace("<br/>", "\n")
    .replace("<![CDATA[", "")
    .replace("]]>", "")
    .replace("&amp;", "&")
    .replace("\\n", "\n")
    .let {
        if (it.contains("</a>")) {
            it.replace(Cache.htmlLinkRegex, "[$2]($1)")
        } else {
            it
        }
    }
    .let {
        if (it.contains("</font>")) {
            it.replace(Cache.fontRegex, "$1")
        } else {
            it
        }
    }
