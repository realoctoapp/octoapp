package de.crysxd.octoapp.sharedcommon.utils

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import platform.UserNotifications.UNAuthorizationStatusAuthorized
import platform.UserNotifications.UNUserNotificationCenter

actual object NotificationUtils {
    actual suspend fun canUseNotifications(): Boolean {
        val c = UNUserNotificationCenter.currentNotificationCenter()
        val result = MutableStateFlow<Boolean?>(null)
        c.getNotificationSettingsWithCompletionHandler {
//            Napier.v(
//                tag = "NotificationUtils",
//                message = """
//                    Got notification state:
//                    UNAuthorizationStatusAuthorized: ${it?.authorizationStatus == UNAuthorizationStatusAuthorized}
//                    UNAuthorizationStatusDenied: ${it?.authorizationStatus == UNAuthorizationStatusDenied}
//                    UNAuthorizationStatusEphemeral: ${it?.authorizationStatus == UNAuthorizationStatusEphemeral}
//                    UNAuthorizationStatusProvisional: ${it?.authorizationStatus == UNAuthorizationStatusProvisional}
//                    UNAuthorizationStatusNotDetermined: ${it?.authorizationStatus == UNAuthorizationStatusNotDetermined}
//                 """.trimIndent()
//            )
            result.value = when (it?.authorizationStatus) {
                UNAuthorizationStatusAuthorized -> true
                else -> false
            }
        }
        return result.filterNotNull().first()
    }
}