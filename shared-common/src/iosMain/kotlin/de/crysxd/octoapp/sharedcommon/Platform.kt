package de.crysxd.octoapp.sharedcommon

import platform.Foundation.NSBundle
import platform.Foundation.NSCachesDirectory
import platform.Foundation.NSFileManager
import platform.Foundation.NSLocale
import platform.Foundation.NSString
import platform.Foundation.NSUserDomainMask
import platform.Foundation.preferredLanguages
import platform.UIKit.UIDevice
import platform.UIKit.UIUserInterfaceIdiomMac
import platform.UIKit.UIUserInterfaceIdiomPad
import kotlin.experimental.ExperimentalNativeApi
import kotlin.native.runtime.GC
import kotlin.native.runtime.NativeRuntimeApi

@OptIn(kotlinx.cinterop.ExperimentalForeignApi::class)
actual class Platform {

    actual companion object {
        val isExtension get() = NSBundle.mainBundle.bundlePath.endsWith(".appex")

        init {
            if (isExtension) {
                setupExtensionProcess()
            }
        }

        @OptIn(NativeRuntimeApi::class)
        actual fun gc() = GC.collect()

        @OptIn(NativeRuntimeApi::class)
        private fun setupExtensionProcess() {
            GC.autotune = false
            GC.targetHeapBytes = 5 * 1024 * 1024
            GC.pauseOnTargetHeapOverflow = true
            println("Extension: Limiting widget memory: GC.targetHeapBytes=${GC.targetHeapBytes}")
        }
    }

    actual val id: String = when (UIDevice.currentDevice.userInterfaceIdiom) {
        UIUserInterfaceIdiomPad -> PlatformIPadOs
        UIUserInterfaceIdiomMac -> PlatformIPadOs
        else -> PlatformIos
    }

    actual val appVersion by lazy {
        val version = (NSBundle.mainBundle.infoDictionary!!["CFBundleShortVersionString"] as NSString).toString()
        if (betaBuild) "$version-beta" else version
    }

    actual val appBuild by lazy {
        (NSBundle.mainBundle.infoDictionary!!["CFBundleVersion"] as String).filter { it.isDigit() }.toInt()
    }

    actual val betaBuild = false //NSBundle.mainBundle.appStoreReceiptURL?.path?.contains("sandboxReceipt") == true

    actual val appId by lazy {
        requireNotNull(NSBundle.mainBundle.bundleIdentifier) { "bundleIdentifier is null" }
    }

    actual val platformVersion by lazy {
        UIDevice.currentDevice.systemVersion
    }

    actual val platformName by lazy {
        UIDevice.currentDevice.systemName
    }

    actual val cacheDirectory by lazy {
        requireNotNull(
            NSFileManager.defaultManager.URLForDirectory(
                directory = NSCachesDirectory,
                inDomain = NSUserDomainMask,
                appropriateForURL = null,
                error = null,
                create = false
            )?.absoluteURL?.path
        ) {
            "Cache directory was null"
        }
    }

    @OptIn(ExperimentalNativeApi::class)
    actual val debugBuild = kotlin.native.Platform.isDebugBinary

    actual val deviceName: String = UIDevice.currentDevice.name

    actual val deviceModel: String = UIDevice.currentDevice.localizedModel

    actual val deviceLanguage: String
        get() = NSLocale.preferredLanguages.first().toString()

    actual val slaveApp = isExtension
}
