package de.crysxd.octoapp.sharedcommon.http

import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.SoftTimeouts
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.darwin.Darwin
import io.ktor.client.engine.darwin.KtorNSURLSessionDelegate
import io.ktor.client.statement.HttpReceivePipeline
import io.ktor.client.statement.request
import io.ktor.util.InternalAPI
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.withTimeoutOrNull
import platform.Foundation.NSData
import platform.Foundation.NSError
import platform.Foundation.NSHTTPURLResponse
import platform.Foundation.NSOperationQueue
import platform.Foundation.NSURLAuthenticationChallenge
import platform.Foundation.NSURLCredential
import platform.Foundation.NSURLRequest
import platform.Foundation.NSURLSession
import platform.Foundation.NSURLSessionAuthChallengeDisposition
import platform.Foundation.NSURLSessionConfiguration
import platform.Foundation.NSURLSessionDataDelegateProtocol
import platform.Foundation.NSURLSessionDataTask
import platform.Foundation.NSURLSessionTask
import platform.Foundation.NSURLSessionTaskDelegateProtocol
import platform.Foundation.NSURLSessionTaskMetrics
import platform.Foundation.NSURLSessionTaskTransactionMetrics
import platform.Foundation.NSURLSessionWebSocketCloseCode
import platform.Foundation.NSURLSessionWebSocketDelegateProtocol
import platform.Foundation.NSURLSessionWebSocketTask
import platform.darwin.NSObject
import kotlin.time.Duration.Companion.seconds

@InternalAPI
internal actual fun SpecificDefaultHttpClient(
    settings: HttpClientSettings,
    config: HttpClientConfig<*>.() -> Unit
) = HttpClient(Darwin) {
    val baseDelegate = KtorNSURLSessionDelegate()
    val dataDelegate = DataDelegate(baseDelegate)
    install(SoftTimeouts.Plugin) {
        timeouts = settings.timeouts
    }

    engine {
        usePreconfiguredSession(
            session = NSURLSession.sessionWithConfiguration(
                NSURLSessionConfiguration.defaultSessionConfiguration,
                dataDelegate,
                delegateQueue = NSOperationQueue()
            ),
            delegate = baseDelegate,
        )
    }

    install("OctoRemoteIp") {
        receivePipeline.intercept(HttpReceivePipeline.State) { response ->
            val host = response.request.url.host
            withTimeoutOrNull(1.seconds) {
                dataDelegate.remoteAddressCache.first { it.containsKey(host) }[host]
            }?.let { remoteIp ->
                response.call.attributes.put(OctoRemoteIpAttributeKey, remoteIp)
            }

            proceedWith(response)
        }
    }

    config()
}


private class DataDelegate(val base: KtorNSURLSessionDelegate) : NSObject(),
    NSURLSessionDataDelegateProtocol,
    NSURLSessionWebSocketDelegateProtocol,
    NSURLSessionTaskDelegateProtocol {

    private val mutableRemoteAddressCache = MutableStateFlow<Map<String, String>>(emptyMap())
    val remoteAddressCache = mutableRemoteAddressCache.asStateFlow()

    override fun URLSession(
        session: NSURLSession,
        dataTask: NSURLSessionDataTask,
        didReceiveData: NSData
    ) = base.URLSession(
        session = session,
        dataTask = dataTask,
        didReceiveData = didReceiveData,
    )

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun URLSession(
        session: NSURLSession,
        taskIsWaitingForConnectivity: NSURLSessionTask
    ) = base.URLSession(
        session = session,
        taskIsWaitingForConnectivity = taskIsWaitingForConnectivity,
    )

    override fun URLSession(
        session: NSURLSession,
        task: NSURLSessionTask,
        didCompleteWithError: NSError?
    ) = base.URLSession(
        session = session,
        task = task,
        didCompleteWithError = didCompleteWithError,
    )

    override fun URLSession(
        session: NSURLSession,
        webSocketTask: NSURLSessionWebSocketTask,
        didOpenWithProtocol: String?
    ) = base.URLSession(
        session = session,
        webSocketTask = webSocketTask,
        didOpenWithProtocol = didOpenWithProtocol,
    )

    override fun URLSession(
        session: NSURLSession,
        webSocketTask: NSURLSessionWebSocketTask,
        didCloseWithCode: NSURLSessionWebSocketCloseCode,
        reason: NSData?
    ) = base.URLSession(
        session = session,
        webSocketTask = webSocketTask,
        didCloseWithCode = didCloseWithCode,
        reason = reason,
    )

    override fun URLSession(
        session: NSURLSession,
        task: NSURLSessionTask,
        willPerformHTTPRedirection: NSHTTPURLResponse,
        newRequest: NSURLRequest,
        completionHandler: (NSURLRequest?) -> Unit
    ) = base.URLSession(
        session = session,
        task = task,
        willPerformHTTPRedirection = willPerformHTTPRedirection,
        newRequest = newRequest,
        completionHandler = completionHandler
    )

    override fun URLSession(
        session: NSURLSession,
        task: NSURLSessionTask,
        didReceiveChallenge: NSURLAuthenticationChallenge,
        completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Unit
    ) = base.URLSession(
        session = session,
        task = task,
        didReceiveChallenge = didReceiveChallenge,
        completionHandler = completionHandler,
    )

    override fun URLSession(
        session: NSURLSession,
        task: NSURLSessionTask,
        didFinishCollectingMetrics: NSURLSessionTaskMetrics
    ) {
        val remoteAddress = didFinishCollectingMetrics.transactionMetrics.firstNotNullOfOrNull {
            (it as? NSURLSessionTaskTransactionMetrics)?.remoteAddress
        } ?: return let {
            Napier.w(tag = "DataDelegate", message = "Unable to find remote IP in metrics")
        }
        val host = task.response?.URL?.host ?: return let {
            Napier.w(tag = "DataDelegate", message = "Unable to find host in metrics")
        }

        Napier.v(tag = "DataDelegate", message = "Received metrics for ${task.response?.URL}")

        mutableRemoteAddressCache.update { old ->
            old.toMutableMap().also {
                it[host] = remoteAddress
            }
        }
    }
}
