package de.crysxd.octoapp.sharedcommon

import platform.Foundation.NSError

class NSErrorException(val error: NSError, message: String): Exception(message)
