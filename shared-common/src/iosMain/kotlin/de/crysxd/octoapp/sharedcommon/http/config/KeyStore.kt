package de.crysxd.octoapp.sharedcommon.http.config

actual data class KeyStore(val certs: List<X509CertificateWrapper>)