package de.crysxd.octoapp.sharedcommon.io

import com.russhwolf.settings.NSUserDefaultsSettings
import com.russhwolf.settings.Settings
import com.russhwolf.settings.set
import de.crysxd.octoapp.sharedcommon.Platform
import io.github.aakira.napier.Napier
import platform.Foundation.NSUserDefaults

actual class SettingsStore actual constructor(private val platform: Platform) {

    actual companion object {
        const val TAG = "SettingsStore"
    }

    private val isMainApp = !Platform.isExtension
    private val groupDefaults = NSUserDefaultsSettings(NSUserDefaults(suiteName = "group.de.crysxd.octoapp"))

    init {
        // We are the main app? Sync to group settings
        if (isMainApp) {
            println("SettingsStore: Operating as main app, triggering initial sync")
            Napier.i(tag = TAG, message = "Operating as main app, triggering initial sync")
            groupDefaults.clear()
            forNameSpace(printerConfigsNamespace)
            forNameSpace(preferencesNamespace)
        }
    }

    actual fun forNameSpace(nameSpace: String): Settings = if (isMainApp) {
        // We are the main app? Normal operation but sync selected
        val master = NSUserDefaultsSettings(NSUserDefaults(suiteName = "${platform.appId}.$nameSpace"))
        if (nameSpace in listOf(printerConfigsNamespace, preferencesNamespace)) {
            MasterSlaveSettings(master = master, slave = groupDefaults)
        } else {
            master
        }
    } else {
        // We are the widget extension? Only access shared defaults
        Napier.i(tag = TAG, message = "Operating as slave app, redirecting namespace $nameSpace to group")
        println("SettingsStore: Operating as slave app, redirecting namespace $nameSpace to group")
        groupDefaults
    }

    private class MasterSlaveSettings(
        private val master: Settings,
        private val slave: Settings,
    ) : Settings by master {
        override val keys get() = master.keys
        override val size get() = master.size

        init {
            Napier.i(tag = TAG, message = "Performing initial sync")
            print("SyncSettings: Performing initial sync")
            master.keys.forEach { key ->
                Napier.v(tag = TAG, message = "Performing initial sync -> $key")
                print("SyncSettings: Performing initial sync -> $key")
                getStringOrNull(key)?.let { slave[key] = it }
                    ?: getBooleanOrNull(key)?.let { slave[key] = it }
                    ?: getIntOrNull(key)?.let { slave[key] = it }
                    ?: getLongOrNull(key)?.let { slave[key] = it }
                    ?: getDoubleOrNull(key)?.let { slave[key] = it }
                    ?: getFloatOrNull(key)?.let { slave[key] = it }
            }
        }

        override fun clear() {
            master.clear()
            slave.clear()
        }

        override fun putBoolean(key: String, value: Boolean) {
            master.putBoolean(key, value)
            slave.putBoolean(key, value)
        }

        override fun putDouble(key: String, value: Double) {
            master.putDouble(key, value)
            slave.putDouble(key, value)
        }

        override fun putFloat(key: String, value: Float) {
            master.putFloat(key, value)
            slave.putFloat(key, value)
        }

        override fun putInt(key: String, value: Int) {
            master.putInt(key, value)
            slave.putInt(key, value)
        }

        override fun putLong(key: String, value: Long) {
            master.putLong(key, value)
            slave.putLong(key, value)
        }

        override fun putString(key: String, value: String) {
            master.putString(key, value)
            slave.putString(key, value)
        }

        override fun remove(key: String) {
            master.remove(key)
            slave.remove(key)
        }
    }
}