package de.crysxd.octoapp.sharedcommon.utils

import de.crysxd.octoapp.sharedcommon.ext.daysFromNow
import de.crysxd.octoapp.sharedcommon.ext.isToday
import de.crysxd.octoapp.sharedcommon.ext.toSuperscriptString
import kotlinx.datetime.Instant
import kotlinx.datetime.toNSDate
import platform.Foundation.NSCalendarUnitHour
import platform.Foundation.NSCalendarUnitMinute
import platform.Foundation.NSDateComponentsFormatter
import platform.Foundation.NSDateComponentsFormatterUnitsStyleAbbreviated
import platform.Foundation.NSDateFormatter
import platform.Foundation.NSLocale
import platform.Foundation.NSNumber
import platform.Foundation.NSNumberFormatter
import platform.Foundation.autoupdatingCurrentLocale

actual object DefaultDataFormatter : DataFormatter {
    private var testingMode = false
    private var localeOverride: String? = null

    override fun enableTestingMode(locale: String) {
        testingMode = true
        localeOverride = locale
    }

    private val numberFormatter by lazy {
        NSNumberFormatter()
    }

    private val dateFormatter by lazy {
        NSDateFormatter()
    }

    private val dateComponentsFormatter by lazy {
        NSDateComponentsFormatter()
    }

    override fun formatNumber(number: Number, minDecimals: Int, maxDecimals: Int): String {
        numberFormatter.maximumFractionDigits = maxDecimals.toULong()
        numberFormatter.minimumFractionDigits = minDecimals.toULong()
        localeOverride?.let {
            numberFormatter.locale = NSLocale(it)
        }
        return numberFormatter.stringFromNumber(NSNumber(number.toDouble())) ?: number.toString()
    }

    override fun formatDate(instant: Instant, showTime: Boolean, showDate: Boolean, showDateAlsoIfToday: Boolean, useCompactFutureDate: Boolean): String {
        if (testingMode) {
            return instant.toString()
        }

        var components = ""
        if (showTime) components += "jjmm"
        if (showDate && !useCompactFutureDate && (!instant.isToday() || showDateAlsoIfToday)) components += "dMMMyy"
        dateFormatter.dateFormat = requireNotNull(NSDateFormatter.dateFormatFromTemplate(components, 0u, NSLocale.autoupdatingCurrentLocale)) {
            "Failed to build format from $components"
        }
        val result = dateFormatter.stringFromDate(instant.toNSDate())
        return if (useCompactFutureDate && !instant.isToday()) {
            "$result${instant.daysFromNow().toSuperscriptString()}"
        } else {
            result
        }
    }

    override fun formatDuration(seconds: Number): String = seconds.toDouble().let {
        if (it < 60) {
            getString("less_than_a_minute")
        } else {
            dateComponentsFormatter.allowedUnits = NSCalendarUnitHour or NSCalendarUnitMinute
            dateComponentsFormatter.unitsStyle = NSDateComponentsFormatterUnitsStyleAbbreviated
            dateComponentsFormatter.stringFromTimeInterval(it) ?: ""
        }
    }
}