package de.crysxd.octoapp.sharedcommon.http.config

import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

data class Timeouts(
    val connectionTimeout: Duration = 15.seconds,
    val requestTimeout: Duration = 20.seconds,
    val webSocketPingPongTimeout: Duration = 5.seconds,
)