package de.crysxd.octoapp.sharedcommon.io

import com.russhwolf.settings.Settings
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.encodeToString


expect class SettingsStore(platform: Platform) {
    companion object
    fun forNameSpace(nameSpace: String): Settings
}

val SettingsStore.Companion.printerConfigsNamespace get() = "printer-configs"
val SettingsStore.Companion.printerConfigsKey get() = "configs"
val SettingsStore.Companion.preferencesNamespace get() = "settings"
val SettingsStore.Companion.activeInstanceIdKey get() = "active_instance_id"

val SettingsJson by lazy { SharedCommonInjector.get().json }

inline fun <reified T> Settings.putSerializable(key: String, value: T) = putString(key, SettingsJson.encodeToString(value))

inline fun <reified T> Settings.putSerializable(key: String, serializer: SerializationStrategy<T>, value: T) =
    putString(key, SettingsJson.encodeToString(serializer, value))


inline fun <reified T> Settings.getSerializableOrNull(key: String): T? = getStringOrNull(key)?.let {
    try {
        SettingsJson.decodeFromString<T>(it)
    } catch (e: Exception) {
        Napier.e(tag = "Settings", message = "Failed to deserialize ${T::class.qualifiedName} from $it", throwable = e)
        null
    }
}

inline fun <reified T> Settings.getSerializableOrNull(key: String, deserializer: DeserializationStrategy<T>): T? = getStringOrNull(key)?.let {
    try {
        SettingsJson.decodeFromString(deserializer, it)
    } catch (e: Exception) {
        Napier.e(tag = "Settings", message = "Failed to deserialize ${T::class.qualifiedName} from $it", throwable = e)
        null
    }
}

inline fun <reified T> Settings.getSerializable(key: String, default: T): T =
    getSerializableOrNull(key) ?: default

inline fun <reified T> Settings.getSerializable(key: String, deserializer: DeserializationStrategy<T>, default: T): T =
    getSerializableOrNull(key, deserializer) ?: default