package de.crysxd.octoapp.sharedcommon.http.config

fun interface Dns {
    fun lookup(hostname: String): List<IpAddress>

    companion object {
        val Noop = Dns { throw IllegalStateException("This DNS can't be used") }
    }
}