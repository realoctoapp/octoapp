package de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_OCTO_EVERYWHERE
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.ktor.http.Url

class OctoEverywhereSubscriptionMissingException(webUrl: Url) : NetworkException(
    userFacingMessage = "<b>OctoEverywhere disabled</b><br><br>OctoEverywhere can't be used anymore as your OctoEverywhere supporter status expired. OctoEverywhere is disconnected for now, you can reconnect it at any time.<br><br><small><b>This is not linked to purchases in OctoApp. OctoEverywhere is a trusted third-party service!</b></small>",
    technicalMessage = "Missing OctoEverywhere supporter status",
    webUrl = webUrl,
    learnMoreLink = "http://" + getString("uri___configure_remote_access")
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_OCTO_EVERYWHERE
}