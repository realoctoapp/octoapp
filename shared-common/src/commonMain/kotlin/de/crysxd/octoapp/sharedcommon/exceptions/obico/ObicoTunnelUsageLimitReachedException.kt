package de.crysxd.octoapp.sharedcommon.exceptions.obico

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import io.ktor.http.Url

class ObicoTunnelUsageLimitReachedException(webUrl: Url) : NetworkException(
    userFacingMessage = "You used up the data volume of your Obico tunnel.\n\nObico is disconnected for now, you can reconnect it again it in the „Configure Remote Access“ menu.",
    technicalMessage = "Received error code 481 from Obico",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = "Obico"
}