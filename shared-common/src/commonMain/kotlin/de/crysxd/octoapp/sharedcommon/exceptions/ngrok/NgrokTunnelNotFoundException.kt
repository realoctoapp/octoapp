package de.crysxd.octoapp.sharedcommon.exceptions.ngrok

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_NGROK
import io.ktor.http.Url

class NgrokTunnelNotFoundException(
    webUrl: Url
) : NetworkException(
    userFacingMessage = "The ngrok tunnel is deleted or the ngrok agent is not running.\n\nThis happens when the ngrok service is stopped or you use a free domain which changes when ngrok is restarted.\nIf you use the OctoPrint ngrok plugin, OctoApp will get the latest tunnel information automatically once you reconnect it.",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_NGROK
}