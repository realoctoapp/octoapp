package de.crysxd.octoapp.sharedcommon.exceptions

import io.ktor.http.Url

class PrinterUnavailableException(e: Throwable? = null, webUrl: Url) : NetworkException(
    originalCause = e,
    webUrl = webUrl,
    userFacingMessage = "Server is not available, check your network connection.",
    technicalMessage = "${e?.message} $webUrl",
)