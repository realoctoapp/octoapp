package de.crysxd.octoapp.sharedcommon.utils

data class ComparableVersion(val text: String) : Comparable<ComparableVersion> {

    override fun toString() = text

    override fun compareTo(other: ComparableVersion): Int {
        val segmentsA = text.split(".").flatMap { it.split("-") }.toMutableList()
        val segmentsB = other.text.split(".").flatMap { it.split("-") }.toMutableList()

        while (segmentsA.isNotEmpty() && segmentsB.isNotEmpty()) {
            val a = segmentsA.removeAt(0)
            val b = segmentsB.removeAt(0)
            val aInt = a.toIntOrNull()
            val bInt = b.toIntOrNull()

            return if (aInt != null && bInt != null) {
                when {
                    aInt > bInt -> 1
                    bInt > aInt -> -1
                    else -> continue
                }
            } else {
                when {
                    aInt != null -> return -1
                    bInt != null -> return 1
                    else -> a.compareTo(b)
                }
            }
        }

        return when {
            segmentsA.size > segmentsB.size -> 1
            segmentsB.size > segmentsA.size -> -1
            else -> 0
        }
    }
}

fun String.asVersion() = ComparableVersion(this)