package de.crysxd.octoapp.sharedcommon.utils

expect fun getString(id: String, vararg formatArgs: Any): String

expect fun CharSequence.parseHtml(): CharSequence
