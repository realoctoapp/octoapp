package de.crysxd.octoapp.sharedcommon.http

enum class ConnectionType {
    OctoEverywhere,
    Obico,
    Ngrok,
    Tailscale,
    Default,
    DefaultCloud,
}