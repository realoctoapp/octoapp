package de.crysxd.octoapp.sharedcommon.http

import de.crysxd.octoapp.sharedcommon.Constants
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import de.crysxd.octoapp.sharedcommon.http.framework.installBasicAuthInterceptorPlugin
import de.crysxd.octoapp.sharedcommon.http.logging.StyledLogger
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.UserAgent
import io.ktor.client.plugins.cache.HttpCache
import io.ktor.client.plugins.cache.storage.CacheStorage
import io.ktor.client.plugins.compression.ContentEncoding
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.util.AttributeKey

val OctoRemoteIpAttributeKey = AttributeKey<String>("octo-remote-ip")

@Suppress("FunctionName")
fun DefaultHttpClient(settings: HttpClientSettings, engine: HttpClientEngine? = null, config: HttpClientConfig<*>.() -> Unit = {}): HttpClient {
    val sharedConfig: HttpClientConfig<*>.() -> Unit = {
        expectSuccess = false
        followRedirects = true

        installUserAgent()
        installContentEncoding()
        installLogging(settings.logLevel, settings.logTag)
        installCaching(settings.cache)
        installTimeouts(settings.timeouts)
        config()

        defaultRequest {
            settings.extraHeaders.forEach { (key, value) ->
                header(key, value)
            }
        }
    }

    return if (engine != null) {
        HttpClient(engine, sharedConfig)
    } else {
        SpecificDefaultHttpClient(settings, sharedConfig)
    }.apply {
        installBasicAuthInterceptorPlugin()
    }
}


inline fun <reified T> HttpRequestBuilder.setJsonBody(body: T) {
    setBody(body)
    contentType(ContentType.Application.Json)
}

internal fun HttpClientConfig<*>.installCaching(cacheStorage: CacheStorage?) {
    cacheStorage?.let { storage ->
        install(HttpCache) {
            publicStorage(storage)
            privateStorage(storage)
        }
    }
}

internal fun HttpClientConfig<*>.installTimeouts(timeouts: Timeouts) {
    install(HttpTimeout) {
        connectTimeoutMillis = timeouts.connectionTimeout.inWholeMilliseconds
        socketTimeoutMillis = timeouts.connectionTimeout.inWholeMilliseconds
    }
}

internal fun HttpClientConfig<*>.installContentEncoding() {
    install(ContentEncoding) {
        deflate(1.0f)
        gzip(0.9f)
    }
}

internal fun HttpClientConfig<*>.installLogging(logLevel: LogLevel, logTag: String) {
    install(Logging) {
        level = logLevel.ktorLevel
        logger = StyledLogger(logTag)
        filter { it.attributes.getOrNull(Constants.SuppressLogging) != true }
    }
}

internal fun HttpClientConfig<*>.installUserAgent() {
    SharedCommonInjector.getOrNull()?.platform?.let { platform ->
        install(UserAgent) {
            agent = "OctoApp ${platform.appVersion} (${platform.platformName} ${platform.platformVersion})"
        }
    }
}

internal expect fun SpecificDefaultHttpClient(settings: HttpClientSettings, config: HttpClientConfig<*>.() -> Unit): HttpClient
