package de.crysxd.octoapp.sharedcommon.exceptions

import io.ktor.http.Url

class PrinterNotFoundException(httpUrl: Url, body: String) : PrinterApiException(httpUrl = httpUrl, responseCode = 404, body = body)