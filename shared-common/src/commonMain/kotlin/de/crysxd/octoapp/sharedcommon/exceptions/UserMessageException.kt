package de.crysxd.octoapp.sharedcommon.exceptions

interface UserMessageException {
    val userMessage: CharSequence
}