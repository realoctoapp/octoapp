package de.crysxd.octoapp.sharedcommon.di

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import org.koin.dsl.module

class SerializationModule : BaseModule {

    @OptIn(ExperimentalSerializationApi::class)
    fun provideDefaultJson(): Json = Json {
        ignoreUnknownKeys = true
        encodeDefaults = true
        explicitNulls = true
        prettyPrint = false
        allowSpecialFloatingPointValues = true
    }

    override val koinModule = module {
        single { provideDefaultJson() }
    }
}


