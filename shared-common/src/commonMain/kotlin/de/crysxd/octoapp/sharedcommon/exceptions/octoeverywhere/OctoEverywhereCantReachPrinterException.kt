package de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class OctoEverywhereCantReachPrinterException(webUrl: Url) : NetworkException(
    userFacingMessage = "OctoApp is connected to OctoEverywhere, but OctoEverywhere can't reach your OctoPrint at the moment",
    technicalMessage = "Received error code 601 from OctoEverywhere",
    webUrl = webUrl,
)