package de.crysxd.octoapp.sharedcommon.exceptions.obico

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_OBICO
import io.ktor.http.Url

class ObicoTunnelNotFoundException(webUrl: Url) : NetworkException(
    userFacingMessage = "The connection with Obico was revoked, please connect Obico again.",
    technicalMessage = "Obico reported tunnel as deleted",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_OBICO
}