package de.crysxd.octoapp.sharedcommon.exceptions.octoeverywhere

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_OCTO_EVERYWHERE
import io.ktor.http.Url

class OctoEverywhereConnectionNotFoundException(webUrl: Url, code: Int) : NetworkException(
    userFacingMessage = "The connection with OctoEverywhere was revoked, please connect OctoEverywhere again.",
    technicalMessage = "OctoEverywhere reported connection as broken ($code)",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_OCTO_EVERYWHERE
}