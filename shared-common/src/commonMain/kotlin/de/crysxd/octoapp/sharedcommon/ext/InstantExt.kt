package de.crysxd.octoapp.sharedcommon.ext

import de.crysxd.octoapp.sharedcommon.utils.DefaultDataFormatter
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

fun Instant.isToday(): Boolean {
    val today = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())
    val candidate = toLocalDateTime(TimeZone.currentSystemDefault())
    return today.date == candidate.date
}

fun Instant.isThisYear(): Boolean {
    val today = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())
    val self = toLocalDateTime(TimeZone.currentSystemDefault())
    return today.year == self.year
}

fun Instant.isPast(): Boolean = Clock.System.now() > this

fun Instant.daysFromNow(): Int {
    val now = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())
    val self = toLocalDateTime(TimeZone.currentSystemDefault())
    return (self.dayOfYear - now.dayOfYear) + (self.year - now.year) * 365
}

fun Instant.isFuture(): Boolean = this > Clock.System.now()

fun Instant.format(
    showDate: Boolean = true,
    showTime: Boolean = true,
    showDateAlsoIfToday: Boolean = false,
    useCompactFutureDate: Boolean = false,
) = DefaultDataFormatter.formatDate(
    instant = this,
    showDate = showDate,
    showTime = showTime,
    showDateAlsoIfToday = showDateAlsoIfToday,
    useCompactFutureDate = useCompactFutureDate,
)