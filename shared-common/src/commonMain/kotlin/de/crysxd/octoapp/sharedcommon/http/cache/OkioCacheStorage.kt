package de.crysxd.octoapp.sharedcommon.http.cache

import com.russhwolf.settings.contains
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.io.FileManager
import de.crysxd.octoapp.sharedcommon.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.io.getSerializableOrNull
import de.crysxd.octoapp.sharedcommon.io.putSerializable
import io.github.aakira.napier.Napier
import io.ktor.client.plugins.cache.storage.CachedResponseData
import io.ktor.http.ContentType
import io.ktor.http.HeadersBuilder
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpProtocolVersion
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.util.date.GMTDate
import io.ktor.util.flattenEntries
import io.ktor.utils.io.core.toByteArray
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Instant
import okio.Buffer
import okio.ByteString.Companion.toByteString
import okio.use

class OkioCacheStorage(
    private val maxSize: () -> Long,
    private val allowedContentTypes: List<ContentType>,
    private val ignoreCacheHeadersAndStoreForEternity: Boolean = false,
    settingsStore: SettingsStore,
    fileManager: FileManager,
) : HttpDiskCache {

    companion object {
        val MediaTypes = listOf(
            ContentType.Image.Any,
            ContentType.Image.PNG,
            ContentType.Image.JPEG,
            ContentType.Image.GIF,
            ContentType.Image.SVG,
        )
    }

    private val tag = "OkioCacheStorage"
    private val settings = settingsStore.forNameSpace("http-cache")
    private val storage = fileManager.forNameSpace("http-cache")
    private val itemPrefix = "item:"
    private val indexPrefix = "index:"
    private val totalSizeKey = "totalSize"
    private val totalCountKey = "totalCount"
    private val mutex = Mutex()

    override val currentSize: Long
        get() = storage.totalSize()

    private fun Url.cacheKeyIndex() =
        indexPrefix + toString().encodeToByteArray().toByteString().md5().hex()

    private fun Url.cacheKeyWithVary(varyKeys: Map<String, String>) =
        itemPrefix + (toString() + varyKeys.toString()).encodeToByteArray().toByteString().md5().hex()

    override suspend fun find(url: Url, varyKeys: Map<String, String>): CachedResponseData? = mutex.withLock {
        val cacheKeyWithVary = url.cacheKeyWithVary(varyKeys)
        val cacheKeyIndex = url.cacheKeyIndex()

        return load(cacheKeyIndex = cacheKeyIndex, cacheKeyWithVary = cacheKeyWithVary).also {
            if (it != null) {
                Napier.i(tag = tag, message = "🎯  Cache hit: ${url.pathSegments.last()} ($cacheKeyWithVary)")
            }
        }
    }

    override suspend fun findAll(url: Url): Set<CachedResponseData> = mutex.withLock {
        val cacheKeyIndex = url.cacheKeyIndex()
        return settings.getString(url.cacheKeyIndex(), "").split(",").mapNotNull { cacheKeyWithVary ->
            load(cacheKeyIndex = cacheKeyIndex, cacheKeyWithVary = cacheKeyWithVary)
        }.toSet().also {
            if (it.isNotEmpty()) {
                Napier.v(tag = tag, message = "🎯  Cache hit: ${url.pathSegments.last()} (all vary) (${it.size})")
            }
        }
    }

    override suspend fun store(url: Url, data: CachedResponseData) = mutex.withLock {
        val cacheKeyIndex = url.cacheKeyIndex()
        val cacheKeyWithVary = url.cacheKeyWithVary(data.varyKeys)

        val contentType = data.headers[HttpHeaders.ContentType]?.let { ContentType.parse(it) }
        if (contentType == null || contentType !in allowedContentTypes) {
            return
        }

        try {
            storage.deleteCacheFile(name = cacheKeyWithVary)
            storage.writeCacheFile(name = cacheKeyWithVary).use {
                // Update cache entry
                settings.putSerializable(
                    key = cacheKeyWithVary,
                    value = CachedResponseDataDto(
                        data = data,
                        indexId = cacheKeyIndex,
                        ignoreCacheHeadersAndStoreForEternity = ignoreCacheHeadersAndStoreForEternity,
                    ),
                )

                // Update index
                val index = settings.getString(key = cacheKeyIndex, "").split(",").toSet()
                settings.putString(cacheKeyIndex, (index + cacheKeyWithVary).joinToString(","))

                // Write to disk
                it.write(fileOffset = 0, array = data.body, arrayOffset = 0, byteCount = data.body.size)
            }

            Napier.v(
                tag = tag,
                message = "💾  Stored ${url.pathSegments.last()} ($cacheKeyWithVary, ${data.body.size} bytes, total size now ${storage.totalSize()} bytes"
            )
            cleanUp()
        } catch (e: Exception) {
            // Cleanup
            Napier.e(tag = tag, message = "Failed to store $url ($cacheKeyWithVary, ${data.body.size} bytes)", throwable = e)
            delete(cacheKeyIndex = cacheKeyIndex, cacheKeyWithVary = cacheKeyWithVary)
        }
    }

    override fun cleanUp() {
        // Delete until size is below max
        val all = settings.keys.filter { it.startsWith(itemPrefix) }.mapNotNull { cacheKeyWithVary ->
            val item = settings.getSerializableOrNull<CachedResponseDataDto>(cacheKeyWithVary) ?: return@mapNotNull null
            cacheKeyWithVary to item
        }.sortedBy { (_, item) ->
            item.responseTime
        }.toMutableList()

        var totalSize = storage.totalSize()
        var totalCount = storage.totalCount()
        val maxSize = maxSize()
        while (totalSize > maxSize && totalCount > 1) {
            val (cacheKeyWithVary, item) = all.removeAt(0)
            totalSize -= item.bodySize
            totalCount -= 1
            Napier.v(tag = tag, message = "Cache exceeds size, deleting ${item.url.toUrl().pathSegments.last()} ($cacheKeyWithVary)")
            delete(cacheKeyIndex = item.cacheKeyIndex, cacheKeyWithVary = cacheKeyWithVary)
        }

        Napier.v(tag = tag, message = "🧹  Cache cleaned up, total size is now $totalSize bytes with $totalCount items")
    }

    private fun load(cacheKeyIndex: String, cacheKeyWithVary: String): CachedResponseData? {
        if (settings.contains(cacheKeyWithVary)) {
            return try {
                val data = settings.getSerializableOrNull<CachedResponseDataDto>(cacheKeyWithVary) ?: throw IllegalStateException("Missing data for $cacheKeyWithVary")
                val bytes = storage.readCacheFile(name = cacheKeyWithVary).use {
                    val bytes = ByteArray(it.size().toInt())
                    it.read(fileOffset = 0, array = bytes, arrayOffset = 0, byteCount = bytes.size)
                    bytes
                }
                data.toCachedResponseData(bytes)
            } catch (e: Exception) {
                Napier.e(tag = tag, message = "Failed to load cacheKeyWithVary=$cacheKeyWithVary")
                delete(cacheKeyIndex = cacheKeyIndex, cacheKeyWithVary = cacheKeyWithVary)
                null
            }
        } else {
            return null
        }
    }

    private fun delete(cacheKeyIndex: String, cacheKeyWithVary: String) {
        storage.deleteCacheFile(cacheKeyWithVary)
        settings.remove(cacheKeyWithVary)
        val index = settings.getString(key = cacheKeyIndex, "").split(",").toSet()
        settings.putString(cacheKeyIndex, (index - cacheKeyWithVary).joinToString(","))
    }

    override fun clear() {
        storage.clear()
        settings.clear()
        Napier.i(tag = tag, message = "Cache cleared")
    }

    private fun Buffer.writeString(string: String, newLine: Boolean = true) {
        write(string.toByteArray())
        if (newLine) {
            write("\n".toByteArray())
        }
    }

    @kotlinx.serialization.Serializable
    private data class UrlIndexDto(
        val linkedKeys: Set<String> = emptySet()
    )

    @kotlinx.serialization.Serializable
    private data class CachedResponseDataDto(
        val cacheKeyIndex: String,
        val url: String,
        val statusCode: Int,
        val statusCodeDescription: String,
        val requestTime: Instant,
        val responseTime: Instant,
        val version: String,
        val versionMajor: Int,
        val versionMinor: Int,
        val expires: Instant,
        val headers: Map<String, String>,
        val varyKeys: Map<String, String>,
        val bodySize: Int,
    ) {

        constructor(
            data: CachedResponseData,
            indexId: String,
            ignoreCacheHeadersAndStoreForEternity: Boolean,
        ) : this(
            cacheKeyIndex = indexId,
            url = data.url.toString(),
            statusCode = data.statusCode.value,
            statusCodeDescription = data.statusCode.description,
            requestTime = Instant.fromEpochMilliseconds(data.requestTime.timestamp),
            responseTime = Instant.fromEpochMilliseconds(data.responseTime.timestamp),
            expires = if (ignoreCacheHeadersAndStoreForEternity) Instant.DISTANT_FUTURE else Instant.fromEpochMilliseconds(data.expires.timestamp),
            headers = data.headers.flattenEntries().toMap().toMutableMap().also {
                if (ignoreCacheHeadersAndStoreForEternity) {
                    it["Cache-Control"] = "public, max-age=31556952, immutable"
                    it.remove("Expires")
                }
            },
            varyKeys = data.varyKeys,
            version = data.version.name,
            versionMajor = data.version.major,
            versionMinor = data.version.minor,
            bodySize = data.body.size
        )

        fun toCachedResponseData(body: ByteArray) = CachedResponseData(
            url = url.toUrl(),
            statusCode = HttpStatusCode(statusCode, statusCodeDescription),
            expires = GMTDate(expires.toEpochMilliseconds()),
            body = body,
            responseTime = GMTDate(responseTime.toEpochMilliseconds()),
            requestTime = GMTDate(requestTime.toEpochMilliseconds()),
            varyKeys = varyKeys,
            version = HttpProtocolVersion.fromValue(
                name = version,
                minor = versionMinor,
                major = versionMajor
            ),
            headers = HeadersBuilder().apply {
                headers.forEach { (name, value) -> append(name, value) }
            }.build()
        )
    }
}