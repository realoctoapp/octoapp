package de.crysxd.octoapp.sharedcommon.utils

expect object NotificationUtils {
    suspend fun canUseNotifications(): Boolean
}