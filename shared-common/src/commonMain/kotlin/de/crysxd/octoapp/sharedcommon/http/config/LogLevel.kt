package de.crysxd.octoapp.sharedcommon.http.config

import io.ktor.client.plugins.logging.LogLevel as KtorLevel

sealed class LogLevel(val ktorLevel: KtorLevel) {
    data object Production : LogLevel(ktorLevel = KtorLevel.INFO)
    data object Verbose : LogLevel(ktorLevel = KtorLevel.HEADERS)
    data object Debug : LogLevel(ktorLevel = KtorLevel.ALL)
}