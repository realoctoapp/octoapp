package de.crysxd.octoapp.sharedcommon.ext

import de.crysxd.octoapp.sharedcommon.utils.DefaultDataFormatter

fun Number.format(
    minDecimals: Int = 0,
    maxDecimals: Int = 2,
    unit: String = "",
) = DefaultDataFormatter.formatNumber(
    number = this,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
    unit = unit,
)

fun Number.formatAsPercent(
    minDecimals: Int = 0,
    maxDecimals: Int = 0
) = DefaultDataFormatter.formatPercent(
    percent = this,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsTemperature(
    minDecimals: Int = 0,
    maxDecimals: Int = 1
) = DefaultDataFormatter.formatTemperature(
    celcius = this,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsWeight(
    onlyGrams: Boolean = false,
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatWeight(
    grams = this,
    onlyGrams = onlyGrams,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsLength(
    onlyMilli: Boolean = true,
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatLength(
    mm = this,
    onlyMilli = onlyMilli,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsSpeed(
    onlyMilli: Boolean = true,
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatLength(
    mm = this,
    onlyMilli = onlyMilli,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
) + "/s"

fun Number.formatAsVolumeOverTime(
    onlyMilli: Boolean = true,
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatVolume(
    mm3 = this,
    onlyMilli = onlyMilli,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
) + "/s"

fun Number.formatAsLayerHeight() = DefaultDataFormatter.formatLength(
    mm = this,
    onlyMilli = true,
    minDecimals = 2,
    maxDecimals = 2,
)

fun Number.formatAsArea(
    onlyMilli: Boolean = false,
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatArea(
    mm2 = this,
    onlyMilli = onlyMilli,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsVolume(
    onlyMilli: Boolean = false,
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatVolume(
    mm3 = this,
    onlyMilli = onlyMilli,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsFileSize(
    minDecimals: Int = 0,
    maxDecimals: Int = 2
) = DefaultDataFormatter.formatFileSize(
    size = this,
    minDecimals = minDecimals,
    maxDecimals = maxDecimals,
)

fun Number.formatAsSecondsDuration() = DefaultDataFormatter.formatDuration(
    seconds = this
)

fun Number.formatAsSecondsEta(
    useCompactFutureDate: Boolean = false,
    showLabel: Boolean = true,
    allowRelative: Boolean = true,
) = DefaultDataFormatter.formatEta(
    seconds = this,
    showLabel = showLabel,
    useCompactFutureDate = useCompactFutureDate,
    allowRelative = allowRelative,
)

fun Number.toSuperscriptString() = format().map {
    when (it) {
        ',' -> '⋅'
        '.' -> '⋅'
        '-' -> '⁻'
        '+' -> '⁺'
        '0' -> '⁰'
        '1' -> '¹'
        '2' -> '²'
        '3' -> '³'
        '4' -> '⁴'
        '5' -> '⁵'
        '6' -> '⁶'
        '7' -> '⁷'
        '8' -> '⁸'
        '9' -> '⁹'
        else -> it
    }
}.joinToString("")