package de.crysxd.octoapp.sharedcommon.exceptions

interface RemoteServiceConnectionBrokenException : SuppressedException {
    val remoteServiceName: String

    companion object {
        const val REMOTE_SERVICE_OBICO = "Obico"
        const val REMOTE_SERVICE_OCTO_EVERYWHERE = "OctoEverywhere"
        const val REMOTE_SERVICE_NGROK = "ngrok"
        const val REMOTE_SERVICE_OTHER = "other"
    }
}
