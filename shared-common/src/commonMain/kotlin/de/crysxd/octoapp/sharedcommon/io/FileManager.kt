package de.crysxd.octoapp.sharedcommon.io

import de.crysxd.octoapp.sharedcommon.Platform
import okio.FileNotFoundException
import okio.FileSystem
import okio.Path.Companion.toPath

expect val DefaultFileSystem: FileSystem

class FileManager(
    private val platform: Platform,
    private val fileSystem: FileSystem = DefaultFileSystem,
) {

    fun forNameSpace(nameSpace: String) = Delegate(nameSpace)

    inner class Delegate(private val nameSpace: String) {
        private fun getRoot() = platform.cacheDirectory.toPath()
            .resolve(platform.appId)
            .resolve(nameSpace)

        fun getPath(name: String) = getRoot().resolve(name)

        fun writeCacheFile(name: String) = try {
            getPath(name = name).let { path ->
                path.parent?.let { fileSystem.createDirectories(it) }
                fileSystem.openReadWrite(path)
            }
        } catch (e: FileNotFoundException) {
            throw FileNotFoundException("File not found: ${getPath(name = name)}")
        }

        fun readCacheFile(name: String) = try {
            getPath(name = name).let { path ->
                fileSystem.openReadOnly(path)
            }
        } catch (e: FileNotFoundException) {
            throw FileNotFoundException("File not found: ${getPath(name = name)}")
        }

        fun deleteCacheFile(name: String) = getPath(name = name).let { path ->
            fileSystem.delete(path, mustExist = false)
        }

        fun clear() = fileSystem.deleteRecursively(getRoot(), mustExist = false)

        fun totalSize(): Long = getRoot().let { path ->
            if (fileSystem.exists(path)) {
                fileSystem.list(path).sumOf {
                    fileSystem.metadata(it).size ?: 0
                }
            } else {
                0
            }
        }

        fun totalCount() = getRoot().let { path ->
            if (fileSystem.exists(path)) {
                fileSystem.list(path).size
            } else {
                0
            }
        }

        fun list() = getRoot().let { path ->
            if (fileSystem.exists(path)) {
                fileSystem.list(path).map { it.name }
            } else {
                emptyList()
            }
        }

        fun exists(name: String) = fileSystem.exists(getPath(name = name))

        fun rename(name: String, newName: String) = fileSystem.atomicMove(getPath(name), getPath(newName))

        fun metadataOrNull(name: String) = fileSystem.metadataOrNull(getPath(name = name))
    }
}