package de.crysxd.octoapp.sharedcommon.exceptions

class ImageByteCacheOverflowException(val cache: ByteArray) : IllegalStateException("Byte cached overflow: ${cache.size} bytes")
