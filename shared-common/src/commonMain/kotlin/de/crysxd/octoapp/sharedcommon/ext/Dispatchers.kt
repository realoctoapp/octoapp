package de.crysxd.octoapp.sharedcommon.ext

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

expect val Dispatchers.SharedIO: CoroutineDispatcher