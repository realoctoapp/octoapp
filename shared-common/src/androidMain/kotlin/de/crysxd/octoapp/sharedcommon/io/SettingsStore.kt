package de.crysxd.octoapp.sharedcommon.io

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.russhwolf.settings.Settings
import com.russhwolf.settings.SharedPreferencesSettings
import de.crysxd.octoapp.sharedcommon.Platform
import io.github.aakira.napier.Napier
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

actual class SettingsStore actual constructor(private val platform: Platform) {

    actual fun forNameSpace(nameSpace: String): Settings = OctoSharedPreferencesSettings(
        platform.context.getSharedPreferences(nameSpace, Context.MODE_PRIVATE)
    )

    actual companion object
}

@Suppress("UNCHECKED_CAST")
class OctoSharedPreferencesSettings(private val sharedPreferences: SharedPreferences) : Settings by SharedPreferencesSettings(sharedPreferences) {

    fun export(): ByteArray {
        val bos = ByteArrayOutputStream()
        bos.use { bytesOut ->
            GZIPOutputStream(bytesOut).use { zip ->
                ObjectOutputStream(zip).use { oo ->
                    oo.writeObject(sharedPreferences.all)
                }
            }
            bytesOut.toByteArray()
        }
        return bos.toByteArray()
    }

    fun import(bytes: ByteArray) {
        val all = ByteArrayInputStream(bytes).use { bytesIn ->
            GZIPInputStream(bytesIn).use { zip ->
                ObjectInputStream(zip).use { oo ->
                    oo.readObject() as Map<String, *>
                }
            }
        }

        sharedPreferences.edit {
            clear()
            all.forEach {
                when (it.value) {
                    is String -> putString(it.key, it.value as String)
                    is Set<*> -> Napier.e(
                        tag = "OctoSharedPreferencesSettings",
                        message = "Unable to import",
                        throwable = IllegalStateException("Unable to import Set<String>")
                    )
                    is Float -> putFloat(it.key, it.value as Float)
                    is Boolean -> putBoolean(it.key, it.value as Boolean)
                    is Int -> putInt(it.key, it.value as Int)
                    is Long -> putLong(it.key, it.value as Long)
                }
            }
        }
    }
}