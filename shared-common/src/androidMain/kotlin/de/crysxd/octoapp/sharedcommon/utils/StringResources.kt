package de.crysxd.octoapp.sharedcommon.utils

import android.annotation.SuppressLint
import android.text.Html
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.ext.withLocale
import de.crysxd.octoapp.sharedcommon.utils.MultiplatformStrings.context
import io.github.aakira.napier.Napier
import java.util.Locale


private val stringCache = mutableMapOf<Int, String>()

object MultiplatformStrings {
    var locale: Locale = Locale.ENGLISH
    internal val context by lazy { SharedCommonInjector.get().platform.context.withLocale(locale) }
}

@SuppressLint("DiscouragedApi")
actual fun getString(id: String, vararg formatArgs: Any): String = try {
    stringCache.getOrPut(key = id.hashCode()) {
        context.run {
            val res = resources.getIdentifier(id, "string", packageName).takeUnless { it == 0 }
            res?.let { getString(it) } ?: id
        }
    }.format(*formatArgs)
} catch (e: Exception) {
    Napier.e(tag = "StringResources", message = "Failed to load $id", throwable = e)
    id
}

actual fun CharSequence.parseHtml(): CharSequence = Html.fromHtml(this.toString(), Html.FROM_HTML_MODE_COMPACT)
