package de.crysxd.octoapp.sharedcommon.utils

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector

actual object NotificationUtils {
    actual suspend fun canUseNotifications() = Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU ||
            ContextCompat.checkSelfPermission(SharedCommonInjector.get().platform.context, Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED
}