package de.crysxd.octoapp.sharedcommon

import android.os.Parcel
import android.os.Parcelable
import kotlinx.datetime.Instant
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parceler
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.TypeParceler

actual typealias CommonParcelize = Parcelize
actual typealias CommonIgnoreOnParcel = IgnoredOnParcel
actual typealias CommonParcelable = Parcelable
actual typealias CommonTypeParceler<T, P> = TypeParceler<T, P>
actual typealias CommonParceler<T> = Parceler<T>

actual object InstantParceler : CommonParceler<Instant?> {
    override fun create(parcel: Parcel) = parcel.readLong().takeIf { it >= 0 }?.let { Instant.fromEpochMilliseconds(it) }
    override fun Instant?.write(parcel: Parcel, flags: Int) = if (this == null) parcel.writeLong(-1) else parcel.writeLong(toEpochMilliseconds())
}