import com.android.build.api.dsl.ManagedVirtualDevice
import de.crysxd.octoapp.buildscript.localProperties
import de.crysxd.octoapp.buildscript.octoAppAndroidBenchmark

plugins {
    alias(libs.plugins.androidTest)
    alias(libs.plugins.kotlinAndroid)
}

octoAppAndroidBenchmark(
    name = "benchmarkphone",
    targetProject = ":app-phone"
) {
    @Suppress("UnstableApiUsage")
    testOptions {
        managedDevices {
            devices {
                create<ManagedVirtualDevice>("pixel4aApi31") {
                    device = "Pixel 4a"
                    apiLevel = 31
                    systemImageSource = localProperties.getProperty("integrationTests.managedDevideSysteImageSource", "aosp")
                }
            }
        }
    }
}