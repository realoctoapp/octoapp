package de.crysxd.octoapp.signin.success

import android.graphics.Rect
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.signin.R
import de.crysxd.octoapp.signin.databinding.SignInSuccessFragmentBinding
import de.crysxd.octoapp.signin.databinding.SignInSuccessFragmentContentBinding
import de.crysxd.octoapp.signin.ext.goBackToDiscover
import io.github.aakira.napier.Napier
import io.ktor.http.hostWithPort
import java.util.UUID

class SignInSuccessFragment : Fragment(), InsetAwareScreen {

    companion object {
        private const val START_DELAY = 2000L
        private const val DURATION = 600L
    }

    private val tag = "SignInSuccessFragment"
    private lateinit var binding: SignInSuccessFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        SignInSuccessFragmentBinding.inflate(layoutInflater, container, false).also {
            it.base.content.removeAllViews()
            SignInSuccessFragmentContentBinding.inflate(inflater, it.base.content, true)
            binding = it
        }.root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(R.transition.sign_in_shard_element)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(R.transition.sign_in_shard_element)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonContinue.animate().setStartDelay(START_DELAY).setDuration(DURATION).alpha(1f).start()
        binding.base.octoView.scheduleAnimation(1000) {
            party()
        }

        binding.buttonContinue.setOnClickListener {
            val args = navArgs<SignInSuccessFragmentArgs>().value
            OctoAnalytics.logEvent(OctoAnalytics.Event.Login)
            OctoAnalytics.logEvent(OctoAnalytics.Event.SignInSuccess)

            val new = PrinterConfigurationV3(
                id = UUID.randomUUID().toString(),
                webUrl = UriLibrary.secureDecode(args.webUrl).toUrlOrNull().let {
                    it ?: throw IllegalStateException("Not an HTTP url: ${args.webUrl} -> ${UriLibrary.secureDecode(args.webUrl)} -> null")
                },
                apiKey = args.apiKey,
                type = args.type,
            )

            // Clearing and setting the active will enforce the navigation to be reset
            // This is important in case we got here after a API key was invalid
            BaseInjector.get().octorPrintRepository().clearActive()
            BaseInjector.get().octorPrintRepository().setActive(new, trigger = "SignIn")

            // Remove all duplicate instances to prevent issues
            BaseInjector.get().octorPrintRepository().getAll().filter {
                it.id != new.id && it.webUrl.hostWithPort == new.webUrl.hostWithPort && it.webUrl.pathSegments == new.webUrl.pathSegments
            }.forEach {
                Napier.i(tag = tag, message = "Duplicate instance to new one will be removed: ${new.webUrl} <--> ${it.webUrl} (${it.id}")
                BaseInjector.get().octorPrintRepository().remove(it.id)
            }
        }

        // Disable back button, we can't go back here
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() = goBackToDiscover()
        })

        prepareVideo()
    }

    override fun onPause() {
        super.onPause()
        binding.video.pauseAnimation()
    }

    override fun onResume() {
        super.onResume()
        binding.video.progress = 0f
        binding.video.playAnimation()
    }

    private fun prepareVideo() {
        binding.video.setAnimation(R.raw.confetti)
    }

    override fun handleInsets(insets: Rect) {
        binding.base.root.updatePadding(
            left = insets.left,
            right = insets.right,
            top = insets.top,
            bottom = insets.bottom
        )
        binding.buttonContainer.updatePadding(
            left = insets.left,
            right = insets.right,
            top = insets.top,
            bottom = insets.bottom
        )
    }
}
