package de.crysxd.octoapp.sharedexternalapis.tutorials

import de.crysxd.octoapp.sharedexternalapis.http.ExternalApiHttpClient
import de.crysxd.octoapp.sharedexternalapis.tutorials.model.Tutorial
import io.github.aakira.napier.Antilog
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.Napier
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Instant
import kotlin.test.AfterTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TutorialApiTest {

    @AfterTest
    fun tearDown() {
        Napier.takeLogarithm()
    }

    @Test
    fun WHEN_tutorials_are_loaded_without_cache_THEN_response_is_parsed() = runBlocking {
        //region GIVEN
        val httpClient = ExternalApiHttpClient(
            MockEngine {
                assertEquals(
                    expected = HttpMethod.Get,
                    actual = it.method,
                    message = "Expected GET"
                )
                respond(
                    //region content = ...
                    content = """
                        {
                          "kind": "youtube#playlistItemListResponse",
                          "etag": "-1lECKFs3kIuOJCvgUBdnlz7JpA",
                          "items": [
                            {
                              "kind": "youtube#playlistItem",
                              "etag": "EDHnZaBTkvJxiKdPaCtZAiDZJB0",
                              "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41NkI0NEY2RDEwNTU3Q0M2",
                              "snippet": {
                                "publishedAt": "2021-01-10T10:33:56Z",
                                "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                                "title": "OctoApp Tutorials: Sign in",
                                "description": "Learn how to sing in to OctoApp!\n\nOctoApp for OctoPrint utilizes a modern approach to the OctoPrint UI and is build with mobile platforms in mind. The app is divided into \"workspaces\" which only offer the tools needed right now. This allows OctoApp to have a very simple and clean UI while not sacrificing on features. The three workspaces are: Connect, Prepare and Print.\n\nhttps://play.google.com/store/apps/details?id=de.crysxd.octoapp",
                                "thumbnails": {
                                  "default": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/default.jpg",
                                    "width": 120,
                                    "height": 90
                                  },
                                  "medium": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/mqdefault.jpg",
                                    "width": 320,
                                    "height": 180
                                  },
                                  "high": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/hqdefault.jpg",
                                    "width": 480,
                                    "height": 360
                                  },
                                  "standard": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/sddefault.jpg",
                                    "width": 640,
                                    "height": 480
                                  }
                                },
                                "channelTitle": "Livingroom Workbench",
                                "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                                "position": 0,
                                "resourceId": {
                                  "kind": "youtube#video",
                                  "videoId": "71nX2FNGK6Q"
                                },
                                "videoOwnerChannelTitle": "Livingroom Workbench",
                                "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                              },
                              "contentDetails": {
                                "videoId": "71nX2FNGK6Q",
                                "videoPublishedAt": "2021-01-10T10:35:08Z"
                              }
                            },
                            {
                              "kind": "youtube#playlistItem",
                              "etag": "36tEKk0zaGEjghqbhQG6E68MEeM",
                              "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4yODlGNEE0NkRGMEEzMEQy",
                              "snippet": {
                                "publishedAt": "2021-01-17T10:21:55Z",
                                "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                                "title": "OctoApp Tutorials: Getting started",
                                "description": "",
                                "thumbnails": {
                                  "default": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/default.jpg",
                                    "width": 120,
                                    "height": 90
                                  },
                                  "medium": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/mqdefault.jpg",
                                    "width": 320,
                                    "height": 180
                                  },
                                  "high": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/hqdefault.jpg",
                                    "width": 480,
                                    "height": 360
                                  },
                                  "standard": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/sddefault.jpg",
                                    "width": 640,
                                    "height": 480
                                  }
                                },
                                "channelTitle": "Livingroom Workbench",
                                "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                                "position": 1,
                                "resourceId": {
                                  "kind": "youtube#video",
                                  "videoId": "lKJhWnLUrHA"
                                },
                                "videoOwnerChannelTitle": "Livingroom Workbench",
                                "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                              },
                              "contentDetails": {
                                "videoId": "lKJhWnLUrHA",
                                "videoPublishedAt": "2021-01-17T10:22:45Z"
                              }
                            },
                            {
                              "kind": "youtube#playlistItem",
                              "etag": "sRhDXEVFbIGiNZ5pLd_NrcfjwS4",
                              "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4wMTcyMDhGQUE4NTIzM0Y5",
                              "snippet": {
                                "publishedAt": "2021-01-23T15:23:01Z",
                                "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                                "title": "OctoApp Tutorials: What's new in 1.5?",
                                "description": "",
                                "thumbnails": {
                                  "default": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/default.jpg",
                                    "width": 120,
                                    "height": 90
                                  },
                                  "medium": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/mqdefault.jpg",
                                    "width": 320,
                                    "height": 180
                                  },
                                  "high": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/hqdefault.jpg",
                                    "width": 480,
                                    "height": 360
                                  },
                                  "standard": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/sddefault.jpg",
                                    "width": 640,
                                    "height": 480
                                  }
                                },
                                "channelTitle": "Livingroom Workbench",
                                "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                                "position": 2,
                                "resourceId": {
                                  "kind": "youtube#video",
                                  "videoId": "4o3L0mPpXG8"
                                },
                                "videoOwnerChannelTitle": "Livingroom Workbench",
                                "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                              },
                              "contentDetails": {
                                "videoId": "4o3L0mPpXG8",
                                "videoPublishedAt": "2021-01-23T15:26:19Z"
                              }
                            }
                            ]
                         }
                    """.trimIndent(),
                    //endregion
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode.OK
                )
            }
        )
        val target = TutorialApi(httpClient)
        //endregion
        //region WHEN
        val tutorials = target.getTutorials(skipCache = true, currentVersion = 10)
        //endregion
        //region THEN
        assertEquals(
            actual = tutorials,
            //region expected =...
            expected = listOf(
                Tutorial(
                    title = "What's new in 1.5?",
                    description = "",
                    thumbnails = listOf(
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/default.jpg", width = 120, height = 90),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/mqdefault.jpg", width = 320, height = 180),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/hqdefault.jpg", width = 480, height = 360),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/sddefault.jpg", width = 640, height = 480)
                    ),
                    publishedAt = Instant.parse("2021-01-23T15:26:19Z"),
                    url = "https://www.youtube.com/watch?v=4o3L0mPpXG8&list=PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ"
                ),
                Tutorial(
                    title = "Getting started",
                    description = "",
                    thumbnails = listOf(
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/default.jpg", width = 120, height = 90),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/mqdefault.jpg", width = 320, height = 180),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/hqdefault.jpg", width = 480, height = 360),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/sddefault.jpg", width = 640, height = 480)
                    ),
                    publishedAt = Instant.parse("2021-01-17T10:22:45Z"),
                    url = "https://www.youtube.com/watch?v=lKJhWnLUrHA&list=PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ"
                ),
                Tutorial(
                    title = "Sign in",
                    description = "Learn how to sing in to OctoApp!\n\nOctoApp for OctoPrint utilizes a modern approach to the OctoPrint UI and is build with mobile platforms in mind. The app is divided into \"workspaces\" which only offer the tools needed right now. This allows OctoApp to have a very simple and clean UI while not sacrificing on features. The three workspaces are: Connect, Prepare and Print.\n\nhttps://play.google.com/store/apps/details?id=de.crysxd.octoapp",
                    thumbnails = listOf(
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/71nX2FNGK6Q/default.jpg", width = 120, height = 90),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/71nX2FNGK6Q/mqdefault.jpg", width = 320, height = 180),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/71nX2FNGK6Q/hqdefault.jpg", width = 480, height = 360),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/71nX2FNGK6Q/sddefault.jpg", width = 640, height = 480)
                    ),
                    publishedAt = Instant.parse("2021-01-10T10:35:08Z"),
                    url = "https://www.youtube.com/watch?v=71nX2FNGK6Q&list=PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ"
                )
            ),
            //endregion
            message = "Expected tutorials to match"
        )
        //endregion
    }

    @Test
    fun WHEN_tutorials_are_loaded_THEN_new_tutorials_are_removed() = runBlocking {
        //region GIVEN
        val httpClient = ExternalApiHttpClient(
            MockEngine {
                assertEquals(
                    expected = HttpMethod.Get,
                    actual = it.method,
                    message = "Expected GET"
                )
                respond(
                    //region content = ...
                    content = """
                        {
                          "kind": "youtube#playlistItemListResponse",
                          "etag": "-1lECKFs3kIuOJCvgUBdnlz7JpA",
                          "items": [
                            {
                              "kind": "youtube#playlistItem",
                              "etag": "EDHnZaBTkvJxiKdPaCtZAiDZJB0",
                              "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41NkI0NEY2RDEwNTU3Q0M2",
                              "snippet": {
                                "publishedAt": "2021-01-10T10:33:56Z",
                                "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                                "title": "OctoApp Tutorials: Sign in",
                                "description": "Learn how to sing in to OctoApp!\n\nOctoApp for OctoPrint utilizes a modern approach to the OctoPrint UI and is build with mobile platforms in mind. The app is divided into \"workspaces\" which only offer the tools needed right now. This allows OctoApp to have a very simple and clean UI while not sacrificing on features. The three workspaces are: Connect, Prepare and Print.\n\nhttps://play.google.com/store/apps/details?id=de.crysxd.octoapp\n\n#octoapp:min:11",
                                "thumbnails": {
                                  "default": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/default.jpg",
                                    "width": 120,
                                    "height": 90
                                  },
                                  "medium": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/mqdefault.jpg",
                                    "width": 320,
                                    "height": 180
                                  },
                                  "high": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/hqdefault.jpg",
                                    "width": 480,
                                    "height": 360
                                  },
                                  "standard": {
                                    "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/sddefault.jpg",
                                    "width": 640,
                                    "height": 480
                                  }
                                },
                                "channelTitle": "Livingroom Workbench",
                                "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                                "position": 0,
                                "resourceId": {
                                  "kind": "youtube#video",
                                  "videoId": "71nX2FNGK6Q"
                                },
                                "videoOwnerChannelTitle": "Livingroom Workbench",
                                "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                              },
                              "contentDetails": {
                                "videoId": "71nX2FNGK6Q",
                                "videoPublishedAt": "2021-01-10T10:35:08Z"
                              }
                            },
                            {
                              "kind": "youtube#playlistItem",
                              "etag": "36tEKk0zaGEjghqbhQG6E68MEeM",
                              "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4yODlGNEE0NkRGMEEzMEQy",
                              "snippet": {
                                "publishedAt": "2021-01-17T10:21:55Z",
                                "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                                "title": "OctoApp Tutorials: Getting started",
                                "description": "#octoapp:min:10",
                                "thumbnails": {
                                  "default": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/default.jpg",
                                    "width": 120,
                                    "height": 90
                                  },
                                  "medium": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/mqdefault.jpg",
                                    "width": 320,
                                    "height": 180
                                  },
                                  "high": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/hqdefault.jpg",
                                    "width": 480,
                                    "height": 360
                                  },
                                  "standard": {
                                    "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/sddefault.jpg",
                                    "width": 640,
                                    "height": 480
                                  }
                                },
                                "channelTitle": "Livingroom Workbench",
                                "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                                "position": 1,
                                "resourceId": {
                                  "kind": "youtube#video",
                                  "videoId": "lKJhWnLUrHA"
                                },
                                "videoOwnerChannelTitle": "Livingroom Workbench",
                                "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                              },
                              "contentDetails": {
                                "videoId": "lKJhWnLUrHA",
                                "videoPublishedAt": "2021-01-17T10:22:45Z"
                              }
                            },
                            {
                              "kind": "youtube#playlistItem",
                              "etag": "sRhDXEVFbIGiNZ5pLd_NrcfjwS4",
                              "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4wMTcyMDhGQUE4NTIzM0Y5",
                              "snippet": {
                                "publishedAt": "2021-01-23T15:23:01Z",
                                "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                                "title": "OctoApp Tutorials: What's new in 1.5?",
                                "description": "#octoapp:min:9",
                                "thumbnails": {
                                  "default": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/default.jpg",
                                    "width": 120,
                                    "height": 90
                                  },
                                  "medium": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/mqdefault.jpg",
                                    "width": 320,
                                    "height": 180
                                  },
                                  "high": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/hqdefault.jpg",
                                    "width": 480,
                                    "height": 360
                                  },
                                  "standard": {
                                    "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/sddefault.jpg",
                                    "width": 640,
                                    "height": 480
                                  }
                                },
                                "channelTitle": "Livingroom Workbench",
                                "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                                "position": 2,
                                "resourceId": {
                                  "kind": "youtube#video",
                                  "videoId": "4o3L0mPpXG8"
                                },
                                "videoOwnerChannelTitle": "Livingroom Workbench",
                                "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                              },
                              "contentDetails": {
                                "videoId": "4o3L0mPpXG8",
                                "videoPublishedAt": "2021-01-23T15:26:19Z"
                              }
                            }
                            ]
                         }
                    """.trimIndent(),
                    //endregion
                    headers = headersOf(HttpHeaders.ContentType, ContentType.Application.Json.toString()),
                    status = HttpStatusCode.OK
                )
            }
        )
        val target = TutorialApi(httpClient)
        //endregion
        //region WHEN
        val tutorials = target.getTutorials(skipCache = false, currentVersion = 10)
        //endregion
        //region THEN
        assertEquals(
            actual = tutorials,
            //region expected =...
            expected = listOf(
                Tutorial(
                    title = "What's new in 1.5?",
                    description = "#octoapp:min:9",
                    thumbnails = listOf(
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/default.jpg", width = 120, height = 90),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/mqdefault.jpg", width = 320, height = 180),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/hqdefault.jpg", width = 480, height = 360),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/4o3L0mPpXG8/sddefault.jpg", width = 640, height = 480)
                    ),
                    publishedAt = Instant.parse("2021-01-23T15:26:19Z"),
                    url = "https://www.youtube.com/watch?v=4o3L0mPpXG8&list=PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ"
                ),
                Tutorial(
                    title = "Getting started",
                    description = "#octoapp:min:10",
                    thumbnails = listOf(
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/default.jpg", width = 120, height = 90),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/mqdefault.jpg", width = 320, height = 180),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/hqdefault.jpg", width = 480, height = 360),
                        Tutorial.Thumbnail(url = "https://i.ytimg.com/vi/lKJhWnLUrHA/sddefault.jpg", width = 640, height = 480)
                    ),
                    publishedAt = Instant.parse("2021-01-17T10:22:45Z"),
                    url = "https://www.youtube.com/watch?v=lKJhWnLUrHA&list=PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ"
                ),
            ),
            //endregion
            message = "Expected tutorials to match"
        )
        //endregion
    }

    @Test
    fun WHEN_tutorials_are_loaded_THEN_cache_is_used() = runBlocking {
        //region GIVEN
        var requestCount = 0
        val httpClient = ExternalApiHttpClient(MockEngine {
            respond(
                status = HttpStatusCode.OK,
                headers = headersOf("Cache-Control" to listOf("max-age=86400"), "Content-Type" to listOf("application/json")),
                //region content = ...
                content = """
                    {
                      "kind": "youtube#playlistItemListResponse",
                      "etag": "OqtHKn-7Z5S73BeaDwsWa97KU7A",
                      "items": [
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "EDHnZaBTkvJxiKdPaCtZAiDZJB0",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41NkI0NEY2RDEwNTU3Q0M2",
                          "snippet": {
                            "publishedAt": "2021-01-10T10:33:56Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Sign in",
                            "description": "Learn how to sing in to OctoApp!\n\nOctoApp for OctoPrint utilizes a modern approach to the OctoPrint UI and is build with mobile platforms in mind. The app is divided into \"workspaces\" which only offer the tools needed right now. This allows OctoApp to have a very simple and clean UI while not sacrificing on features. The three workspaces are: Connect, Prepare and Print.\n\nhttps://play.google.com/store/apps/details?id=de.crysxd.octoapp",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/71nX2FNGK6Q/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 0,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "71nX2FNGK6Q"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "71nX2FNGK6Q",
                            "videoPublishedAt": "2021-01-10T10:35:08Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "36tEKk0zaGEjghqbhQG6E68MEeM",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4yODlGNEE0NkRGMEEzMEQy",
                          "snippet": {
                            "publishedAt": "2021-01-17T10:21:55Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Getting started",
                            "description": "",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/lKJhWnLUrHA/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 1,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "lKJhWnLUrHA"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "lKJhWnLUrHA",
                            "videoPublishedAt": "2021-01-17T10:22:45Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "sRhDXEVFbIGiNZ5pLd_NrcfjwS4",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4wMTcyMDhGQUE4NTIzM0Y5",
                          "snippet": {
                            "publishedAt": "2021-01-23T15:23:01Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.5?",
                            "description": "",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/4o3L0mPpXG8/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 2,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "4o3L0mPpXG8"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "4o3L0mPpXG8",
                            "videoPublishedAt": "2021-01-23T15:26:19Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "EsunzqmWcjahfbb88-P_Kty4kMs",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41MjE1MkI0OTQ2QzJGNzNG",
                          "snippet": {
                            "publishedAt": "2021-02-14T11:06:55Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.6?",
                            "description": "",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/pOQs6Eqm7O4/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/pOQs6Eqm7O4/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/pOQs6Eqm7O4/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/pOQs6Eqm7O4/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 3,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "pOQs6Eqm7O4"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "pOQs6Eqm7O4",
                            "videoPublishedAt": "2021-02-14T11:09:26Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "mdcAWX0WrdxRob-WDL9CIv3ygFs",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4xMkVGQjNCMUM1N0RFNEUx",
                          "snippet": {
                            "publishedAt": "2021-03-20T01:26:31Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.7?",
                            "description": "",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/CyUVaBmSwSU/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/CyUVaBmSwSU/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/CyUVaBmSwSU/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/CyUVaBmSwSU/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 4,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "CyUVaBmSwSU"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "CyUVaBmSwSU",
                            "videoPublishedAt": "2021-03-20T01:26:40Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "RpkHpkjPY-i90AA7mFI2ICOoOmY",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41MzJCQjBCNDIyRkJDN0VD",
                          "snippet": {
                            "publishedAt": "2021-04-23T07:52:30Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.8?",
                            "description": "Version 1.8 allows you to configure a remote access connection and offers a full OctoEverywhere integration.",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/rz3aZSboMnA/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/rz3aZSboMnA/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/rz3aZSboMnA/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/rz3aZSboMnA/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 5,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "rz3aZSboMnA"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "rz3aZSboMnA",
                            "videoPublishedAt": "2021-04-23T08:11:37Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "QWNCwXIX0IEGz3xQCoqfj516ah4",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si5GNjNDRDREMDQxOThCMDQ2",
                          "snippet": {
                            "publishedAt": "2021-06-16T13:50:38Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.9?",
                            "description": "Get OctoApp now! https://play.google.com/store/apps/details?id=de.crysxd.octoapp",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/TeuOlUE25gQ/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/TeuOlUE25gQ/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/TeuOlUE25gQ/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/TeuOlUE25gQ/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 6,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "TeuOlUE25gQ"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "TeuOlUE25gQ",
                            "videoPublishedAt": "2021-06-17T17:29:25Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "6fiP_0tI8-fzvgkLzQUrRQ4ZRlc",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si40NzZCMERDMjVEN0RFRThB",
                          "snippet": {
                            "publishedAt": "2021-07-18T10:05:04Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Sign in",
                            "description": "If you have trouble with your OctoPrint, ask for help here:\n\nOctoPrint discord: https://discord.octoprint.org/\nOctoPrint community forum: https://community.octoprint.org\n\nFor more information and help about the sign in process open this link on your phone:\nhttps://link.octoapp.eu/sign-in-help\n\nIf you have questions or issues with the app, check out the app's help section (click link on your phone): https://link.octoapp.eu/help\n\nEnjoy the app & happy printing!",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/mlWfaCuvDL8/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/mlWfaCuvDL8/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/mlWfaCuvDL8/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/mlWfaCuvDL8/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 7,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "mlWfaCuvDL8"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "mlWfaCuvDL8",
                            "videoPublishedAt": "2021-09-19T13:34:47Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "9ULvv6r9QXA_SIdRrUVft_lG3QM",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si5EMEEwRUY5M0RDRTU3NDJC",
                          "snippet": {
                            "publishedAt": "2021-07-25T14:18:10Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.10?",
                            "description": "Version 1.10 is OctoApp's first anniversary release. It packs a lot of improvements around connecting OctoPrint and customizations for your workflow.\n\n0:00 Sign in\n0:42 Local hostnames\n1:05 Quick access\n1:42 Quick access widget\n1:06 New settings\n1:10 Outro",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/gtxshuB4Vp4/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/gtxshuB4Vp4/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/gtxshuB4Vp4/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/gtxshuB4Vp4/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 8,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "gtxshuB4Vp4"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "gtxshuB4Vp4",
                            "videoPublishedAt": "2021-07-28T15:40:47Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "aW3Ns4VjK9RJRnJwHEmnDKxI-tQ",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41Mzk2QTAxMTkzNDk4MDhF",
                          "snippet": {
                            "publishedAt": "2021-08-01T17:01:38Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: How to customize OctoApp?",
                            "description": "In this short tutorial I will show you how to customize OctoApp for your needs and your workflow!\n\nGet OctoApp now! https://play.google.com/store/apps/details?id=de.crysxd.octoapp\n\n0:00 Intro\n0:22 Customize the controls\n1:05 Customize the menu\n2:00 Customize the appearance",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/-gXtKqAfNRM/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/-gXtKqAfNRM/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/-gXtKqAfNRM/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/-gXtKqAfNRM/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 9,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "-gXtKqAfNRM"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "-gXtKqAfNRM",
                            "videoPublishedAt": "2021-08-01T18:30:40Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "f2aXXBvH5MJGLzYsOqycW2vcFYw",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si5EQUE1NTFDRjcwMDg0NEMz",
                          "snippet": {
                            "publishedAt": "2021-09-19T11:38:58Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Remote access with OctoEverywhere",
                            "description": "This is a simple step-by-step guide to use OctoPrint over the internet, either in the browser or with OctoApp!",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/kvSLAsBHL00/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/kvSLAsBHL00/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/kvSLAsBHL00/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/kvSLAsBHL00/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              },
                              "maxres": {
                                "url": "https://i.ytimg.com/vi/kvSLAsBHL00/maxresdefault.jpg",
                                "width": 1280,
                                "height": 720
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 10,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "kvSLAsBHL00"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "kvSLAsBHL00",
                            "videoPublishedAt": "2021-09-19T13:58:30Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "GJMRq_Ugf9ia6S3XAGqMkV1Ii1Y",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si41QTY1Q0UxMTVCODczNThE",
                          "snippet": {
                            "publishedAt": "2021-09-19T11:50:20Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Remote access with ngrok",
                            "description": "This is a simple step-by-step guide to use OctoPrint over the internet, either in the browser or with OctoApp!",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/Rskcyzujhps/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/Rskcyzujhps/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/Rskcyzujhps/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/Rskcyzujhps/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              },
                              "maxres": {
                                "url": "https://i.ytimg.com/vi/Rskcyzujhps/maxresdefault.jpg",
                                "width": 1280,
                                "height": 720
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 11,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "Rskcyzujhps"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "Rskcyzujhps",
                            "videoPublishedAt": "2021-09-19T13:58:26Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "uvnjn6_BybU9B9pKBZgglN99OJE",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si45RTgxNDRBMzUwRjQ0MDhC",
                          "snippet": {
                            "publishedAt": "2021-09-19T13:54:53Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Remote access with Tailscale",
                            "description": "This is a simple step-by-step guide to use OctoPrint over the internet, either in the browser or with OctoApp!",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/2Ox1JJEEYoU/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/2Ox1JJEEYoU/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/2Ox1JJEEYoU/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/2Ox1JJEEYoU/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              },
                              "maxres": {
                                "url": "https://i.ytimg.com/vi/2Ox1JJEEYoU/maxresdefault.jpg",
                                "width": 1280,
                                "height": 720
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 12,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "2Ox1JJEEYoU"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "2Ox1JJEEYoU",
                            "videoPublishedAt": "2021-09-19T13:55:05Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "H9qOYrP2_rRolRzjy3gWb7Myg8o",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si5ENDU4Q0M4RDExNzM1Mjcy",
                          "snippet": {
                            "publishedAt": "2021-10-15T18:22:34Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.11?",
                            "description": "Version 1.11 makes OctoApp ready for Android 12, introduced a tutorial section and better notifications.\n\nGet the OctoPrint plugin: https://github.com/crysxd/OctoApp-Plugin\n\n0:00 Tutorial section\n0:30 Android 12 and Matrial You\n0:59 Companion plugin and better notifications",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/Zhij_4IRF0k/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/Zhij_4IRF0k/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/Zhij_4IRF0k/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/Zhij_4IRF0k/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              },
                              "maxres": {
                                "url": "https://i.ytimg.com/vi/Zhij_4IRF0k/maxresdefault.jpg",
                                "width": 1280,
                                "height": 720
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 13,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "Zhij_4IRF0k"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "Zhij_4IRF0k",
                            "videoPublishedAt": "2021-10-16T20:00:33Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "XgLrXB98Q6FaxAX0J-5Sxltfmdo",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4zRjM0MkVCRTg0MkYyQTM0",
                          "snippet": {
                            "publishedAt": "2021-12-08T11:03:45Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Remote access with The Spaghetti Detective",
                            "description": "This is a simple step-by-step guide to use OctoPrint over the internet, either in the browser or with OctoApp!",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/kOfhlZgye10/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/kOfhlZgye10/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/kOfhlZgye10/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/kOfhlZgye10/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              },
                              "maxres": {
                                "url": "https://i.ytimg.com/vi/kOfhlZgye10/maxresdefault.jpg",
                                "width": 1280,
                                "height": 720
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 14,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "kOfhlZgye10"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "kOfhlZgye10",
                            "videoPublishedAt": "2021-12-10T07:25:03Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "aWmqafvyNoyWS28lH9PzmlPo3a0",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si45NzUwQkI1M0UxNThBMkU0",
                          "snippet": {
                            "publishedAt": "2021-12-10T07:24:50Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.12?",
                            "description": "Version 1.12 is a huge feature drop! The Spaghetti Detective can now be used with OctoApp and many other improvements!\n\n0:00 Gcode Viewer improvements\n1:03 Camera\n1:55 File management\n3:00 The Spaghetti Detective\n3:42 Plugin library\n4:02 Customise progress\n\n#octoapp:min:11200",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/brLrETRYYV8/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/brLrETRYYV8/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/brLrETRYYV8/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/brLrETRYYV8/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 15,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "brLrETRYYV8"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "brLrETRYYV8",
                            "videoPublishedAt": "2021-12-10T07:24:58Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "57BLrZMklAlT0cL702-omC3qkQk",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si5DNzE1RjZEMUZCMjA0RDBB",
                          "snippet": {
                            "publishedAt": "2022-01-29T09:43:26Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.13?",
                            "description": "Version 1.13 brings some big UI changes! The pause button is dead and multi-printer support just got that much better ðŸš€\n\n0:11 The pause button is dead\n1:10 Timelapse is here!\n1:39 Better multi-printer support",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/4yNwwvXpOUg/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/4yNwwvXpOUg/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/4yNwwvXpOUg/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/4yNwwvXpOUg/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 16,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "4yNwwvXpOUg"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "4yNwwvXpOUg",
                            "videoPublishedAt": "2022-01-29T09:43:26Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "3FISXCVyjsEspHa2agH3Bdkb5fA",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si43MTI1NDIwOTMwQjIxMzNG",
                          "snippet": {
                            "publishedAt": "2022-03-19T06:17:33Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.14?",
                            "description": "Sorry for the background noise! Version 1.14 focuses on extending plugin support and finally adds Octolapse!\n\n0:10 Lefty mode\n0:35 ngrok\n0:46 Octolapse\n1:09 Mmu2filamentselect\n1:36 UsbRelayControl\n1:54 Cache control",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/X_Dfb1aAMZQ/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/X_Dfb1aAMZQ/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/X_Dfb1aAMZQ/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/X_Dfb1aAMZQ/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 17,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "X_Dfb1aAMZQ"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "X_Dfb1aAMZQ",
                            "videoPublishedAt": "2022-03-19T06:19:31Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "3eSCgcWv8MRQX62t1aCBmWaWo2c",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si4yQUE2Q0JEMTk4NTM3RTZC",
                          "snippet": {
                            "publishedAt": "2022-06-06T08:31:33Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.15?",
                            "description": "Version 1.15 introduces a data-saver webcam and *drumrolls* a Wear OS version of OctoApp!\n\nThe watch shown in the video is a Faster 3 with a Wear 2100 chip, the one I mentioned is a bit slow, so this is the performance you can expect! The app is quite slow when you open it for the first time but then speeds up quickly! ðŸ¤“\n\n0:10 Looking back\n0:58 What's new on phones?\n1:46 WearOS version\n3:50 What's next?",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/33vrCW1dNJo/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/33vrCW1dNJo/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/33vrCW1dNJo/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/33vrCW1dNJo/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 18,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "33vrCW1dNJo"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "33vrCW1dNJo",
                            "videoPublishedAt": "2022-06-07T15:01:10Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "aupEqcPRU_BFIHuqwDDevP0lBYI",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si5DMkU4NTY1QUFGQTYwMDE3",
                          "snippet": {
                            "publishedAt": "2022-07-03T16:59:43Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: Getting started on Wear OS",
                            "description": "This is a quick overview on the new Wear OS version of OctoApp!\n\n0:00 Beta & bug reports\n3:16 Connection modes\n7:05 Exploring the app\n14:40 Always-on display\n16:40 Battery & performance",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/uv-ZZxMu-v8/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/uv-ZZxMu-v8/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/uv-ZZxMu-v8/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/uv-ZZxMu-v8/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              },
                              "maxres": {
                                "url": "https://i.ytimg.com/vi/uv-ZZxMu-v8/maxresdefault.jpg",
                                "width": 1280,
                                "height": 720
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 19,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "uv-ZZxMu-v8"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "uv-ZZxMu-v8",
                            "videoPublishedAt": "2022-07-03T17:21:49Z"
                          }
                        },
                        {
                          "kind": "youtube#playlistItem",
                          "etag": "v3ZAAXWdFSA8T-VeUyc4Zx5fiqA",
                          "id": "UEwxZmpsTnFsVUtuVXVXd0IwSmIzd2Y3MHdCY0YzdS13Si44Mjc5REFBRUE2MTdFRDU0",
                          "snippet": {
                            "publishedAt": "2022-09-29T09:34:02Z",
                            "channelId": "UCUFZW6bxLNYxl8uFp37IdPg",
                            "title": "OctoApp Tutorials: What's new in 1.16?",
                            "description": "Version 1.16 adds support for the Cancel Object plugin, improves the webcam fullscreen and much more!\n\n0:11 New controls setup\n0:40 CancelObject plugin\n1:36 Quick Print controls\n2:00 Printer name in status bar\n2:18 Webcam fullscreen improvements\n2:43 WearOS improvements\n2:50 General improvements",
                            "thumbnails": {
                              "default": {
                                "url": "https://i.ytimg.com/vi/IG5ZbL4-Dfo/default.jpg",
                                "width": 120,
                                "height": 90
                              },
                              "medium": {
                                "url": "https://i.ytimg.com/vi/IG5ZbL4-Dfo/mqdefault.jpg",
                                "width": 320,
                                "height": 180
                              },
                              "high": {
                                "url": "https://i.ytimg.com/vi/IG5ZbL4-Dfo/hqdefault.jpg",
                                "width": 480,
                                "height": 360
                              },
                              "standard": {
                                "url": "https://i.ytimg.com/vi/IG5ZbL4-Dfo/sddefault.jpg",
                                "width": 640,
                                "height": 480
                              }
                            },
                            "channelTitle": "Livingroom Workbench",
                            "playlistId": "PL1fjlNqlUKnUuWwB0Jb3wf70wBcF3u-wJ",
                            "position": 20,
                            "resourceId": {
                              "kind": "youtube#video",
                              "videoId": "IG5ZbL4-Dfo"
                            },
                            "videoOwnerChannelTitle": "Livingroom Workbench",
                            "videoOwnerChannelId": "UCUFZW6bxLNYxl8uFp37IdPg"
                          },
                          "contentDetails": {
                            "videoId": "IG5ZbL4-Dfo",
                            "videoPublishedAt": "2022-09-29T09:36:08Z"
                          }
                        }
                      ],
                      "pageInfo": {
                        "totalResults": 21,
                        "resultsPerPage": 50
                      }
                    }
                """.trimIndent()
            //endregion
            )
        })
        val target = TutorialApi(httpClient)
        //endregion
        //region WHEN
        Napier.base(object : Antilog() {
            override fun performLog(priority: LogLevel, tag: String?, throwable: Throwable?, message: String?) {
                if (message?.contains("==>") == true) {
                    requestCount++
                }
            }
        })
        target.getTutorials(skipCache = false, currentVersion = 10)
        assertEquals(expected = 1, actual = requestCount)
        val results = target.getTutorials(skipCache = false, currentVersion = 10)
        //endregion
        //region THEN
        assertEquals(
            expected = 1,
            actual = requestCount,
            message = "Expected one request to be made"
        )
        assertTrue(
            message = "Expected some tutorials to be loaded",
            actual = results.size > 10,
        )
        assertTrue(
            message = "Expected 1.15 tutorial to be loaded with 'OctoApp Tutorials: ' prefix removed",
            actual = results.any { it.title == "What's new in 1.15?" },
        )
        //endregion
    }
}