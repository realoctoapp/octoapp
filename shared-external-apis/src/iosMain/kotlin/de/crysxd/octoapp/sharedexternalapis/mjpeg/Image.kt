@file:OptIn(kotlinx.cinterop.ExperimentalForeignApi::class)

package de.crysxd.octoapp.sharedexternalapis.mjpeg

import kotlinx.cinterop.useContents
import platform.UIKit.UIImage

actual typealias Image = UIImage

actual val Image.widthPx get() = size.useContents { width.toInt() }
actual val Image.heightPx get() = size.useContents { height.toInt() }
