package de.crysxd.octoapp.sharedexternalapis.mjpeg

import android.graphics.Bitmap

actual typealias Image = Bitmap

actual val Image.widthPx get() = width
actual val Image.heightPx get() = height