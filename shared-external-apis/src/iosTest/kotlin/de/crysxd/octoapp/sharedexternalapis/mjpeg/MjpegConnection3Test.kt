package de.crysxd.octoapp.sharedexternalapis.mjpeg

import de.crysxd.octoapp.sharedcommon.http.config.Dns
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector
import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import kotlin.time.Duration.Companion.seconds


class MjpegConnection3Test : MjpegConnection3TestCore() {

    override val settings = HttpClientSettings(
        logLevel = LogLevel.Verbose,
        timeouts = Timeouts(connectionTimeout = 60.seconds),
        dns = Dns.Noop,
        proxySelector = ProxySelector.Noop,
        keyStore = null,
        logTag = "Test"
    )
}