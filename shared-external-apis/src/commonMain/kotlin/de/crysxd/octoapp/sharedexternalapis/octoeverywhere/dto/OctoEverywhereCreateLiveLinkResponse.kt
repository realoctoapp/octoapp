package de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEverywhereCreateLiveLinkResponse(
    @SerialName("Result") val result: Result = Result()
) {
    @Serializable
    data class Result(
        @SerialName("ShortUrl") val shortUrl: String? = null,
        @SerialName("Url") val url: String? = null,
    )
}