package de.crysxd.octoapp.sharedexternalapis.mjpeg

expect class Image

expect val Image.widthPx: Int
expect val Image.heightPx: Int