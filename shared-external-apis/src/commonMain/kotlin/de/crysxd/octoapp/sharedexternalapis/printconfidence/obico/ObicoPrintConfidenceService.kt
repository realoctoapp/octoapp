package de.crysxd.octoapp.sharedexternalapis.printconfidence.obico

import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidenceService
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.retryWhen
import kotlin.time.Duration.Companion.seconds

class ObicoPrintConfidenceService(
    private val confidence: suspend () -> Float,
    private var shouldRetry: (Throwable) -> Boolean
) : PrintConfidenceService {

    override suspend fun checkPrintQuality() = flow {
        while (true) {
            val currentConfidence = confidence()
            Napier.v("Checking print confidence from Obico: $currentConfidence")
            val level = when (currentConfidence) {
                in 0f..0.33f -> PrintConfidence.Level.High
                in 0.33f..0.66f -> PrintConfidence.Level.Medium
                in 0.66f..1f -> PrintConfidence.Level.Low
                else -> PrintConfidence.Level.Unknown
            }
            emit(
                PrintConfidence(
                    description = when (level) {
                        PrintConfidence.Level.High -> "Looking good!"
                        PrintConfidence.Level.Medium -> "Looking fishy"
                        PrintConfidence.Level.Low -> "Probably failing"
                        PrintConfidence.Level.Unknown -> "Not sure"
                    },
                    confidence = currentConfidence,
                    origin = PrintConfidence.Origin.Obico,
                    level = level
                )
            )
            delay(15.seconds)
        }
    }.retryWhen { e, attempt ->
        if (shouldRetry(e)) {
            val delay = (5.seconds * attempt.toInt()).coerceAtMost(30.seconds)
            Napier.e(message = "Obico error, retrying after $delay", throwable = e)
            delay(delay)
            true
        } else {
            Napier.e(message = "Obico error but not allowed to retry, stopping")
            false
        }
    }
}