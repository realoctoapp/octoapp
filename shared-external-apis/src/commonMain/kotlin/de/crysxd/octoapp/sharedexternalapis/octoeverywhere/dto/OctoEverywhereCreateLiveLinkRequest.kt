package de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEverywhereCreateLiveLinkRequest(
    @SerialName("AppToken") val appToken: String,
    @SerialName("ExpirationHours") val expirationHours: Int,
)