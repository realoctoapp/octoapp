package de.crysxd.octoapp.sharedexternalapis.octoeverywhere.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEverywhereConnectionInfoResponse(
    @SerialName("Result") val result: Result = Result()
) {
    @Serializable
    data class Result(
        @SerialName("UserFacingPrinterUrl") val userFacingPrinterUrl: String? = null
    )
}