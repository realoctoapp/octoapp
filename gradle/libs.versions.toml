[versions]
kotlin = "2.0.0"
agp = "8.5.0"
ktor = "2.3.11"
kotlinxSerialization = "1.5.1"
kotlinxCoroutines = "1.7.3"
kotlinxDatetime = "0.4.0"
napier = "2.6.1"
playPublisher = "3.7.0"
uuid = "0.5.0"
koin = "3.2.2"
okio = "3.7.0"
colormath = "3.3.3"
multiplatformSettings = "1.0.0-RC"
androidCoreKtx = "1.13.1"
okhttpLoggingInterceptor = "4.9.2"
junit = "4.13.2"
robolectric = "4.8.1"
exoplayer = "1.3.1"
kotlinMath = "1.5.3"
firebase = "33.1.1"
billing = "7.0.0"
playServices = "18.2.0"
googleServices = "4.4.2"
gson = "2.10.1"
retrofit = "2.9.0"
dagger = "2.47"
sharedPreferencesKtx = "1.2.1"
androidxLifecycle = "2.8.3"
truth = "1.1.3"
mockito = "2.2.0"
navigation = "2.7.7"
browser = "1.8.0"
lottie = "6.1.0"
compose = "2024.06.00"
accompanist = "0.25.1"
zxing = "1.9"
androidxAppcompat = "1.7.0"
androidxConstraintlayout = "2.1.4"
androidxConstraintlayoutCompose = "1.0.1"
androidxNavigation = "2.7.7"
androidxMaterial = "1.12.0"
androidxActivity = "1.9.0"
androidxSwiperefreshlayout = "1.1.0"
markwon = "4.6.2"
picasso = "2.71828"
desugar = "2.0.4"
transitionseverywhere = "2.1.0"
reorderable = "0.9.6"
play-review = "2.0.1"
play-app-update = "2.1.0"
wearCore = "1.3.0"
wearRemoteInteractions = "1.0.0"
wearPhoneInteractions = "1.0.1"
androidxSplashscreen = "1.0.1"
profileinstaller = "1.3.1"
wearCompose = "1.0.1"
androidxJunit = "1.2.1"
espressoCore = "3.6.1"
dexmakerMockitoInline = "2.28.1"
testRunner = "1.6.1"
androidXTest = "1.6.1"
uiautomator = "2.3.0"
barista = "4.2.0"
benchmarkMacroJunit = "1.2.4"
crashlyticsPlugin = "3.0.2"
testOrchestrator = "1.5.0"
composeNavigation = "2.7.7"

[libraries]
build-agp = { module = "com.android.tools.build:gradle-api", version.ref = "agp" }
build-kotlin-api = { module = "org.jetbrains.kotlin:kotlin-gradle-plugin-api", version.ref = "kotlin" }
build-kotlin = { module = "org.jetbrains.kotlin:kotlin-gradle-plugin", version.ref = "kotlin" }

ktor-client-core = { group = "io.ktor", name = "ktor-client-core", version.ref = "ktor" }
ktor-serialization-json = { group = "io.ktor", name = "ktor-serialization-kotlinx-json", version.ref = "ktor" }
ktor-client-content-negotiation = { group = "io.ktor", name = "ktor-client-content-negotiation", version.ref = "ktor" }
ktor-client-logging = { group = "io.ktor", name = "ktor-client-logging", version.ref = "ktor" }
ktor-client-auth = { group = "io.ktor", name = "ktor-client-auth", version.ref = "ktor" }
ktor-client-encoding = { group = "io.ktor", name = "ktor-client-encoding", version.ref = "ktor" }
ktor-client-websockets = { group = "io.ktor", name = "ktor-client-websockets", version.ref = "ktor" }
ktor-client-darwin = { group = "io.ktor", name = "ktor-client-darwin", version.ref = "ktor" }
ktor-client-okhttp = { group = "io.ktor", name = "ktor-client-okhttp", version.ref = "ktor" }

ktor-server-core = { group = "io.ktor", name = "ktor-server-core", version.ref = "ktor" }
ktor-server-cio = { group = "io.ktor", name = "ktor-server-cio", version.ref = "ktor" }
ktor-server-netty = { group = "io.ktor", name = "ktor-server-netty", version.ref = "ktor" }
ktor-network-tls = { group = "io.ktor", name = "ktor-network-tls-certificates", version.ref = "ktor" }

exoplayer = { group = "androidx.media3", name = "media3-exoplayer", version.ref = "exoplayer" }
exoplayer-hls = { group = "androidx.media3", name = "media3-exoplayer-hls", version.ref = "exoplayer" }
exoplayer-rtsp = { group = "androidx.media3", name = "media3-exoplayer-rtsp", version.ref = "exoplayer" }
exoplayer-ui = { group = "androidx.media3", name = "media3-ui", version.ref = "exoplayer" }

kotlinx-serialization-json = { group = "org.jetbrains.kotlinx", name = "kotlinx-serialization-json", version.ref = "kotlinxSerialization" }
kotlinx-serialization-protobuf = { group = "org.jetbrains.kotlinx", name = "kotlinx-serialization-protobuf", version.ref = "kotlinxSerialization" }
kotlinx-coroutines-core = { group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-core", version.ref = "kotlinxCoroutines" }
kotlinx-datetime = { group = "org.jetbrains.kotlinx", name = "kotlinx-datetime", version.ref = "kotlinxDatetime" }

firebase-bom = { module = "com.google.firebase:firebase-bom", version.ref = "firebase" }
firebase-analytics-ktx = { group = "com.google.firebase", name = "firebase-analytics-ktx" }
firebase-config-ktx = { group = "com.google.firebase", name = "firebase-config-ktx" }
firebase-crashlytics = { group = "com.google.firebase", name = "firebase-crashlytics" }
firebase-messaging-ktx = { group = "com.google.firebase", name = "firebase-messaging-ktx" }

compose-bom = { module = "androidx.compose:compose-bom", version.ref = "compose" }
compose-ui = { group = "androidx.compose.ui", name = "ui" }
compose-ui-tooling = { group = "androidx.compose.ui", name = "ui-tooling" }
compose-ui-tooling-preview = { group = "androidx.compose.ui", name = "ui-tooling-preview" }
compose-ui-test = { group = "androidx.compose.ui", name = "ui-test-manifest" }
compose-runtime = { group = "androidx.compose.runtime", name = "runtime" }
compose-foundation = { group = "androidx.compose.foundation", name = "foundation" }
compose-material = { group = "androidx.compose.material", name = "material" }
compose-animation = { group = "androidx.compose.animation", name = "animation-graphics" }
compose-navigation = { group = "androidx.navigation", name = "navigation-compose", version.ref = "composeNavigation" }
compose-constraintlayout = { group = "androidx.constraintlayout", name = "constraintlayout-compose", version.ref = "androidxConstraintlayoutCompose" }

androidx-preferences-ktx = { group = "androidx.preference", name = "preference-ktx", version.ref = "sharedPreferencesKtx" }
androidx-lifecycle-livedata = { group = "androidx.lifecycle", name = "lifecycle-livedata-ktx", version.ref = "androidxLifecycle" }
androidx-lifecycle-viewmodel = { group = "androidx.lifecycle", name = "lifecycle-viewmodel-ktx", version.ref = "androidxLifecycle" }
androidx-lifecycle-viewmodel-compose = { group = "androidx.lifecycle", name = "lifecycle-viewmodel-compose" }
androidx-browser = { group = "androidx.browser", name = "browser", version.ref = "browser" }
androidx-activity-compose = { group = "androidx.activity", name = "activity-compose", version.ref = "androidxActivity" }
androidx-appcompat = { group = "androidx.appcompat", name = "appcompat", version.ref = "androidxAppcompat" }
androidx-constraintlayout = { group = "androidx.constraintlayout", name = "constraintlayout", version.ref = "androidxConstraintlayout" }
androidx-navigation-fragment-ktx = { group = "androidx.navigation", name = "navigation-fragment-ktx", version.ref = "androidxNavigation" }
androidx-navigation-ui-ktx = { group = "androidx.navigation", name = "navigation-ui-ktx", version.ref = "androidxNavigation" }
androidx-material = { group = "com.google.android.material", name = "material", version.ref = "androidxMaterial" }
androidx-swiperefreshlayout = { group = "androidx.swiperefreshlayout", name = "swiperefreshlayout", version.ref = "androidxSwiperefreshlayout" }
androidx-splashscreen = { group = "androidx.core", name = "core-splashscreen", version.ref = "androidxSplashscreen" }
androidx-profiles = { group = "androidx.profileinstaller", name = "profileinstaller", version.ref = "profileinstaller" }

billing = { group = "com.android.billingclient", name = "billing", version.ref = "billing" }
billing-ktx = { group = "com.android.billingclient", name = "billing-ktx", version.ref = "billing" }
playservices-wearable = { group = "com.google.android.gms", name = "play-services-wearable", version.ref = "playServices" }

lottie = { group = "com.airbnb.android", name = "lottie", version.ref = "lottie" }
lottie-compose = { group = "com.airbnb.android", name = "lottie-compose", version.ref = "lottie" }

markwon-core = { group = "io.noties.markwon", name = "core", version.ref = "markwon" }
markwon-image = { group = "io.noties.markwon", name = "image", version.ref = "markwon" }
markwon-linkify = { group = "io.noties.markwon", name = "linkify", version.ref = "markwon" }

wear-core = { group = "androidx.wear", name = "wear", version.ref = "wearCore" }
wear-remoteinteractions = { group = "androidx.wear", name = "wear-remote-interactions", version.ref = "wearRemoteInteractions" }
wear-phoneinteractions = { group = "androidx.wear", name = "wear-phone-interactions", version.ref = "wearPhoneInteractions" }
wear-compose-foundation = { group = "androidx.wear.compose", name = "compose-foundation", version.ref = "wearCompose" }
wear-compose-material = { group = "androidx.wear.compose", name = "compose-material", version.ref = "wearCompose" }

accompanist-flowlayout = { group = "com.google.accompanist", name = "accompanist-flowlayout", version.ref = "accompanist" }
accompanist-pager = { group = "com.google.accompanist", name = "accompanist-pager", version.ref = "accompanist" }

napier = { group = "io.github.aakira", name = "napier", version.ref = "napier" }
uuid = { group = "com.benasher44", name = "uuid", version.ref = "uuid" }
koin-core = { group = "io.insert-koin", name = "koin-core", version.ref = "koin" }
okio = { group = "com.squareup.okio", name = "okio", version.ref = "okio" }
colormath = { group = "com.github.ajalt.colormath", name = "colormath", version.ref = "colormath" }
multiplatform-settings = { group = "com.russhwolf", name = "multiplatform-settings", version.ref = "multiplatformSettings" }
androidx-core-ktx = { group = "androidx.core", name = "core-ktx", version.ref = "androidCoreKtx" }
okhttp-logging-interceptor = { group = "com.squareup.okhttp3", name = "logging-interceptor", version.ref = "okhttpLoggingInterceptor" }
kotlin-math = { group = "dev.romainguy", name = "kotlin-math", version.ref = "kotlinMath" }
happydns = { group = "com.qiniu", name = "happy-dns", version = "0.2.18" }
dnssd = { group = "com.github.andriydruk", name = "dnssd", version = "0.9.16" }
processphoenix = { group = "com.jakewharton", name = "process-phoenix", version = "2.1.2" }
zxing = { group = "me.dm7.barcodescanner", name = "zxing", version.ref = "zxing" }
picasso = { group = "com.squareup.picasso", name = "picasso", version.ref = "picasso" }
reorderable = { group = "org.burnoutcrew.composereorderable", name = "reorderable", version.ref = "reorderable" }
play-inAppReview = { group = "com.google.android.play", name = "review-ktx", version.ref = "play-review" }
play-inAppUpdate = { group = "com.google.android.play", name = "app-update-ktx", version.ref = "play-app-update" }

removesoon-gson = { group = "com.google.code.gson", name = "gson", version.ref = "gson" }
removesoon-retrofit = { group = "com.squareup.retrofit2", name = "converter-gson", version.ref = "retrofit" }
removesoon-retrofit-converter = { group = "com.squareup.retrofit2", name = "retrofit", version.ref = "retrofit" }
removesoon-transitionseverywhere = { group = "com.andkulikov", name = "transitionseverywhere", version.ref = "transitionseverywhere" }

test-junit = { group = "junit", name = "junit", version.ref = "junit" }
test-truth = { group = "com.google.truth", name = "truth", version.ref = "truth" }
test-mockito = { group = "com.nhaarman.mockitokotlin2", name = "mockito-kotlin", version.ref = "mockito" }
test-robolectric = { group = "org.robolectric", name = "robolectric", version.ref = "robolectric" }
test-ktor-client-mock = { group = "io.ktor", name = "ktor-client-mock", version.ref = "ktor" }
test-androidxJunit = { group = "androidx.test.ext", name = "junit", version.ref = "androidxJunit" }
test-espressoCore = { group = "androidx.test.espresso", name = "espresso-core", version.ref = "espressoCore" }
test-core = { group = "androidx.test", name = "core", version.ref = "androidXTest" }
test-dexmakerMockitoInline = { group = "com.linkedin.dexmaker", name = "dexmaker-mockito-inline", version.ref = "dexmakerMockitoInline" }
test-runner = { group = "androidx.test", name = "runner", version.ref = "testRunner" }
test-rules = { group = "androidx.test", name = "rules", version.ref = "androidXTest" }
test-uiautomator = { group = "androidx.test.uiautomator", name = "uiautomator", version.ref = "uiautomator" }
test-barista = { group = "com.adevinta.android", name = "barista", version.ref = "barista" }
test-orchestrator = { group = "androidx.test", name = "orchestrator", version.ref = "testOrchestrator" }

# Used in buildSrc code!
desugar = { group = "com.android.tools", name = "desugar_jdk_libs", version.ref = "desugar" }
test-benchmarkMacroJunit4 = { group = "androidx.benchmark", name = "benchmark-macro-junit4", version.ref = "benchmarkMacroJunit" }
dagger = { group = "com.google.dagger", name = "dagger", version.ref = "dagger" }
dagger-compiler = { group = "com.google.dagger", name = "dagger-compiler", version.ref = "dagger" }

[plugins]
androidApplication = { id = "com.android.application", version.ref = "agp" }
androidTest = { id = "com.android.test", version.ref = "agp" }
androidLibrary = { id = "com.android.library", version.ref = "agp" }
kotlinAndroid = { id = "org.jetbrains.kotlin.android", version.ref = "kotlin" }
kotlinMultiplatform = { id = "org.jetbrains.kotlin.multiplatform", version.ref = "kotlin" }
kotlinParcelize = { id = "org.jetbrains.kotlin.plugin.parcelize", version.ref = "kotlin" }
kotlinKapt = { id = "org.jetbrains.kotlin.kapt", version.ref = "kotlin" }
kotlinSerialization = { id = "org.jetbrains.kotlin.plugin.serialization", version.ref = "kotlin" }
kotlinComposeCompiler = { id = "org.jetbrains.kotlin.plugin.compose", version.ref = "kotlin" }
androidxSafeArgs = { id = "androidx.navigation.safeargs.kotlin", version.ref = "navigation" }
googleServices = { id = "com.google.gms.google-services", version.ref = "googleServices" }
firebaseCrashlytics = { id = "com.google.firebase.crashlytics", version.ref = "crashlyticsPlugin" }
playPublisher = { id = "com.github.triplet.play", version.ref = "playPublisher" }
