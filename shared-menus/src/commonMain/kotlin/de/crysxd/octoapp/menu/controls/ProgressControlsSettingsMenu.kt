package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.billing.FEATURE_GCODE_PREVIEW
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.FontSize.Large
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.FontSize.Normal
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings.FontSize.Small
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.models.ProgressControlsRole
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.SystemInfo.Capability.RealTimeStats
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem.Option
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

@CommonParcelize
class ProgressControlsSettingsMenu(private val role: ProgressControlsRole) : Menu {

    private val octoPreferences get() = SharedBaseInjector.get().preferences
    override val id get() = "ProgressControlsSettingsMenu"
    override val title get() = getString("progress_widget___title")

    private fun load() = when (role) {
        ProgressControlsRole.ForNormal -> octoPreferences.updatedFlow2.map { it.progressWidgetSettings }
        ProgressControlsRole.ForWebcamFullscreen -> octoPreferences.updatedFlow2.map { it.webcamFullscreenProgressWidgetSettings }
    }

    private fun save(progressWidgetSettings: ProgressWidgetSettings) = when (role) {
        ProgressControlsRole.ForNormal -> {
            octoPreferences.progressWidgetSettings = progressWidgetSettings
        }

        ProgressControlsRole.ForWebcamFullscreen -> {
            octoPreferences.webcamFullscreenProgressWidgetSettings = progressWidgetSettings
        }
    }

    override suspend fun getMenuItems(): List<MenuItem> = listOfNotNull(
        ShowThumbnailMenuItem(load = ::load, save = ::save).takeIf { role != ProgressControlsRole.ForWebcamFullscreen },
        ShowTimeUsedMenuItem(load = ::load, save = ::save),
        ShowTimeLeftMenuItem(load = ::load, save = ::save),
        ShowPrinterMessage(load = ::load, save = ::save),
        EtaStyleMenuItem(load = ::load, save = ::save),
        PrintNameStyleMenuItem(load = ::load, save = ::save),
        FontSizeMenuItem(load = ::load, save = ::save),
        ShowLayerInfoMenuItem(load = ::load, save = ::save),
        ShowZHeightMenuItem(load = ::load, save = ::save),
        ShowFlowMenuItem(load = ::load, save = ::save),
        ShowMaxFlowMenuItem(load = ::load, save = ::save),
        ShowSpeedMenuItem(load = ::load, save = ::save),
        ShowMaxSpeedMenuItem(load = ::load, save = ::save),
        ShowFilamentUsedMenuItem(load = ::load, save = ::save),
    )

    private class FontSizeMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit,
    ) : RevolvingOptionsMenuItem {
        override val itemId = "fontSize"
        override var groupId = ""
        override val order = 0
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.FontSize
        override val activeValue get() = load().map { it.fontSize.toString() }
        override val title get() = getString("progress_widget___settings___font_size")
        override val options = ProgressWidgetSettings.FontSize.entries.map {
            Option(
                value = it.toString(),
                label = when (it) {
                    Small -> getString("progress_widget___settings___font_size_small")
                    Normal -> getString("progress_widget___settings___font_size_normal")
                    Large -> getString("progress_widget___settings___font_size_large")
                }
            )
        }

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            save(load().first().copy(fontSize = ProgressWidgetSettings.FontSize.valueOf(option.value)))
        }
    }

    private class PrintNameStyleMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit,
    ) : RevolvingOptionsMenuItem {
        override val itemId = "printNameStyle"
        override var groupId = ""
        override val order = 1
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Label
        override val activeValue get() = load().map { it.printNameStyle.toString() }
        override val title get() = getString("progress_widget___settings___show_print_name")
        override val options = ProgressWidgetSettings.PrintNameStyle.entries.map {
            Option(
                value = it.toString(),
                label = when (it) {
                    ProgressWidgetSettings.PrintNameStyle.None -> getString("progress_widget___settings___show_print_name_none")
                    ProgressWidgetSettings.PrintNameStyle.Compact -> getString("progress_widget___settings___show_print_name_compact")
                    ProgressWidgetSettings.PrintNameStyle.Full -> getString("progress_widget___settings___show_print_name_full")
                }
            )
        }

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            save(load().first().copy(printNameStyle = ProgressWidgetSettings.PrintNameStyle.valueOf(option.value)))
        }
    }

    private class EtaStyleMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit,
    ) : RevolvingOptionsMenuItem {
        override val itemId = "etaStyle"
        override var groupId = "time"
        override val order = 3
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Calendar
        override val activeValue get() = load().map { it.etaStyle.toString() }
        override val title get() = getString("progress_widget___settings___show_eta")
        override val options = ProgressWidgetSettings.EtaStyle.entries.map {
            Option(
                value = it.toString(),
                label = when (it) {
                    ProgressWidgetSettings.EtaStyle.None -> getString("progress_widget___settings___show_eta_none")
                    ProgressWidgetSettings.EtaStyle.Compact -> getString("progress_widget___settings___show_eta_compact")
                    ProgressWidgetSettings.EtaStyle.Full -> getString("progress_widget___settings___show_eta_full")
                }
            )
        }

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            save(load().first().copy(etaStyle = ProgressWidgetSettings.EtaStyle.valueOf(option.value)))
        }
    }

    private class ShowTimeUsedMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "timeUsed"
        override var groupId = "time"
        override val order = 4
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.TimeSpent
        override val state get() = load().map { it.showUsedTime }
        override val title = getString("progress_widget___settings___show_time_used")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showUsedTime = enabled))
        }
    }

    private class ShowTimeLeftMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "timeLeft"
        override var groupId = "time"
        override val order = 5
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.TimeLeft
        override val state get() = load().map { it.showLeftTime }
        override val title = getString("progress_widget___settings___show_time_left")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showLeftTime = enabled))
        }
    }

    private class ShowLayerInfoMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "layerInfo"
        override var groupId = "gcode"
        override val order = 6
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Layers
        override val state get() = load().map { it.showLayer }
        override val billingManagerFeature = FEATURE_GCODE_PREVIEW
        override val title = getString("progress_widget___settings___show_layer")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showLayer = enabled))
        }
    }

    private class ShowZHeightMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "zHeight"
        override var groupId = "gcode"
        override val order = 7
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Height
        override val state get() = load().map { it.showZHeight }
        override val billingManagerFeature = FEATURE_GCODE_PREVIEW
        override val title = getString("progress_widget___settings___show_z_height")
        override val description = getString("progress_widget___settings___gcode_description")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showZHeight = enabled))
        }
    }

    private class ShowFilamentUsedMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "filament_used"
        override var groupId = "realtime"
        override val order = 8
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Length
        override val state get() = load().map { it.showFilamentUsed }
        override val title = getString("progress_widget___settings___show_filament_used")
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = RealTimeStats in systemInfo.capabilities

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showFilamentUsed = enabled))
        }
    }


    private class ShowSpeedMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "speed"
        override var groupId = "realtime"
        override val order = 9
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Speed
        override val state get() = load().map { it.showSpeed }
        override val title = getString("progress_widget___settings___show_speed")
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = RealTimeStats in systemInfo.capabilities

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showSpeed = enabled))
        }
    }

    private class ShowMaxSpeedMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "maxspeed"
        override var groupId = "realtime"
        override val order = 10
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Speed
        override val state get() = load().map { it.showMaxSpeed }
        override val title = getString("progress_widget___settings___show_max_speed")
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = RealTimeStats in systemInfo.capabilities

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showMaxSpeed = enabled))
        }
    }

    private class ShowFlowMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "flow"
        override var groupId = "realtime"
        override val order = 11
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Flow
        override val state get() = load().map { it.showFlow }
        override val title = getString("progress_widget___settings___show_flow")
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = RealTimeStats in systemInfo.capabilities

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showFlow = enabled))
        }
    }

    private class ShowMaxFlowMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "maxflow"
        override var groupId = "realtime"
        override val order = 12
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Flow
        override val state get() = load().map { it.showMaxFlow }
        override val title = getString("progress_widget___settings___show_max_flow")
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = RealTimeStats in systemInfo.capabilities
        override val description: String = getString("progress_widget___settings___show_max_flow_description")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showMaxFlow = enabled))
        }
    }

    class ShowThumbnailMenuItem(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "thumbnail"
        override var groupId = "thumb"
        override val order = 20
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Image
        override val state = load().map { it.showThumbnail }
        override val title = getString("progress_widget___settings___show_thumbnail")
        override val description = getString("progress_widget___settings___thumbnail_description")
        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showThumbnail = enabled))
        }
    }

    private class ShowPrinterMessage(
        private val load: () -> Flow<ProgressWidgetSettings>,
        private val save: (ProgressWidgetSettings) -> Unit
    ) : ToggleMenuItem {
        override val itemId = "message"
        override var groupId = "message"
        override val order = 21
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Message
        override val title = getString("progress_widget___settings___show_printer_message")
        override val description = "Requires Companion plugin"
        override val state = load().map { it.showPrinterMessage }

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            save(load().first().copy(showPrinterMessage = enabled))
        }
    }
}