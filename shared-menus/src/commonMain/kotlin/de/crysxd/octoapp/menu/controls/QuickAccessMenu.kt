package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemLibrary
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize

@CommonParcelize
class QuickAccessMenu(private val instanceId: String, private val itemIds: Set<String>) : SimpleMenu {

    override val id get() = "quick-access/${itemIds.joinToString("+").hashCode()}"
    override val title get() = ""
    override val suppressGrouping get() = true

    override suspend fun getMenuItems(): List<MenuItem> {
        val library = MenuItemLibrary()
        return itemIds.mapNotNull { itemId ->
            library.get(itemId = itemId, instanceId = instanceId)
        }
    }
}