package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class ExtrudeControlsSettingsMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "extrude_settings"
    override val title get() = getString("extrude_settings___title")
    override val subtitle get() = getString("extrude_settings___description")

    companion object {
        private const val DEFAULT_FEED_RATE = 8000
    }

    override suspend fun getMenuItems() = listOf(
        FeedrateMenuItem(instanceId = instanceId),
    )

    class FeedrateMenuItem(private val instanceId: String) : InputMenuItem {
        private val repository get() = SharedBaseInjector.get().printerConfigRepository
        override val currentValue = repository.instanceInformationFlow(instanceId).map { it?.appSettings?.extrudeFeedRate?.toString() ?: "" }
        override val title = getString("extrude_settings___feedrate")
        override val inputHint get() = title
        override val itemId = "extrude_feedrate"
        override var groupId = "feedrate"
        override val canBePinned = false
        override val order = 2
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Speed
        override val keyboardType = InputMenuItem.KeyboardType.Numbers

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (inputAsNumber == null) {
            getString("error_please_enter_a_value")
        } else {
            null
        }

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            repository.updateAppSettings(instanceId) { appSettings ->
                appSettings.copy(extrudeFeedRate = requireNotNull(inputAsNumber?.toInt()) { "Missing float" })
            }
        }
    }
}