package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class GcodeSettingsMenu : SimpleMenu {

    override val id get() = "gcode_settings"
    override val title get() = getString("gcode_preview")

    override suspend fun getMenuItems() = listOf(
        ShowPreviousLayer(),
        ShowCurrentLayer(),
        Quality(),
    )

    class ShowPreviousLayer : ToggleMenuItem {
        override val state = preferences.updatedFlow2.map { it.gcodePreviewSettings.showPreviousLayer }
        override val itemId = "previous"
        override var groupId = "misc"
        override val canBePinned = false
        override val order = 0
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.LayersBelow
        override val title = getString("gcode_preview_settings___show_previous_layer")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.gcodePreviewSettings = preferences.gcodePreviewSettings.copy(showPreviousLayer = enabled)
        }
    }

    class ShowCurrentLayer : ToggleMenuItem {
        override val state = preferences.updatedFlow2.map { it.gcodePreviewSettings.showCurrentLayer }
        override val itemId = "current"
        override var groupId = "misc"
        override val canBePinned = false
        override val order = 1
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Layers
        override val title = getString("gcode_preview_settings___show_current_layer")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.gcodePreviewSettings = preferences.gcodePreviewSettings.copy(showCurrentLayer = enabled)
        }
    }

    class Quality : RevolvingOptionsMenuItem {
        override val activeValue get() = preferences.updatedFlow2.map { it.gcodePreviewSettings.quality.name }
        override val options = listOf(
            RevolvingOptionsMenuItem.Option(
                label = getString("gcode_preview_settings___quality_low"),
                value = GcodePreviewSettings.Quality.Low.name
            ),
            RevolvingOptionsMenuItem.Option(
                label = getString("gcode_preview_settings___quiality_medium"),
                value = GcodePreviewSettings.Quality.Medium.name
            ),
            RevolvingOptionsMenuItem.Option(
                label = getString("gcode_preview_settings___quality_ultra"),
                value = GcodePreviewSettings.Quality.Ultra.name
            )
        )
        override val itemId = "quality"
        override var groupId = "misc"
        override val order = 3
        override val canBePinned = false
        override val enforceSingleLine = false
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.PaintBrush
        override val title = getString("gcode_preview_settings___quality")
        override val description: String get() = when (preferences.gcodePreviewSettings.quality) {
            GcodePreviewSettings.Quality.Low -> getString("gcode_preview_settings___quality_low_description")
            GcodePreviewSettings.Quality.Medium -> getString("gcode_preview_settings___quality_medium_description")
            GcodePreviewSettings.Quality.Ultra -> getString("gcode_preview_settings___quality_ultra_description")
        }

        override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
            preferences.gcodePreviewSettings = preferences.gcodePreviewSettings.copy(quality = GcodePreviewSettings.Quality.valueOf(option.value))
            host?.reloadMenu()
        }
    }
}