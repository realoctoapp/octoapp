package de.crysxd.octoapp.menu.main.octoprint

import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import de.crysxd.octoapp.sharedcommon.url.isUpnPUrl
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.withContext

@CommonParcelize
class OpenOctoPrintMenu(val instanceId: String) : Menu {

    override val id get() = "open_octoprint"
    override val title get() = getString("main_menu___item_open_octoprint")

    override suspend fun getMenuItems(): List<MenuItem> = withContext(Dispatchers.IO) {
        val config = SharedBaseInjector.get().printerConfigRepository.get(instanceId)
        listOfNotNull(
            listOfNotNull(config?.webUrl?.takeUnless { it.isUpnPUrl() }, config?.alternativeWebUrl),
            config?.webUrl?.resolveOptions(config),
        ).flatten().map {
            OpenOctoPrintMenuItem(
                instanceId = instanceId,
                webUrl = it
            )
        }
    }
}

private fun Url.resolveOptions(config: PrinterConfigurationV3): List<Url> = try {
    SharedBaseInjector.get().dnsResolver.lookup(host)
        .map { config.webUrl.withHost(it.hostNameOrIp()) }
} catch (e: Exception) {
    Napier.w(tag = "OpenOctoPrintMenu", message = "Failed to resolve ${config.webUrl}", throwable = e)
    emptyList()
}