package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.OctoPreferences.Companion.VALUE_WEBCAM_ASPECT_RATIO_SOURCE_IMAGE
import de.crysxd.octoapp.base.OctoPreferences.Companion.VALUE_WEBCAM_ASPECT_RATIO_SOURCE_OCTOPRINT
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_WEBCAM_NAME
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_SHOW_WEBCAM_RESOLUTION
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_WEBCAM_ASPECT_RATIO_SOURCE
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_WEBCAM_DATA_SAVER_INTERVAL
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.MenuTargetPlatform
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem.Option
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class WebcamSettingsMenu(
    private val instanceId: String
) : SimpleMenu {

    private val printerConfigRepository get() = SharedBaseInjector.get().printerConfigRepository
    override val id get() = "webcam-menu"
    override val title get() = getString("webcam_settings___title")
    override val subtitle
        get() = getString(
            id = "webcam_settings___subtitle",
            printerConfigRepository.getActiveInstanceSnapshot()?.systemInfo?.interfaceType.label
        )

    override suspend fun getMenuItems() = listOf(
        ShowResolutionMenuItem(),
        ShowNameMenuItem(),
        DataSaverIntervalMenuItem(),
        DataSaverConditionsMenuItem(),
        AspectRatioMenuItem(instanceId = instanceId),
        OverrideRotationLockMenuItem()
    )

    class ShowResolutionMenuItem : ToggleMenuItem {
        override val state = preferences.updatedFlow2.map { it.isShowWebcamResolution }
        override val canBePinned = true
        override val itemId = MENU_ITEM_SHOW_WEBCAM_RESOLUTION
        override var groupId = ""
        override val order = 161
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Ruler
        override val title = getString("webcam_settings___show_resolution")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.isShowWebcamResolution = enabled
        }
    }

    class ShowNameMenuItem : ToggleMenuItem {
        override val state = preferences.updatedFlow2.map { it.isShowWebcamName }
        override val canBePinned = true
        override val itemId = MENU_ITEM_SHOW_WEBCAM_NAME
        override var groupId = ""
        override val order = 162
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Label
        override val title = getString("webcam_settings___show_name")

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.isShowWebcamName = enabled
        }
    }

    class AspectRatioMenuItem(
        instanceId: String
    ) : RevolvingOptionsMenuItem {
        private val interfaceLabel = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.systemInfo?.interfaceType.label
        override val options = listOf(
            Option(label = getString("webcam_settings___aspect_ratio_source_octoprint", interfaceLabel), value = VALUE_WEBCAM_ASPECT_RATIO_SOURCE_OCTOPRINT),
            Option(label = getString("webcam_settings___aspect_ratio_source_image"), value = VALUE_WEBCAM_ASPECT_RATIO_SOURCE_IMAGE),
        )
        override val activeValue = preferences.updatedFlow2.map { it.webcamAspectRatioSource }
        override val itemId = MENU_ITEM_WEBCAM_ASPECT_RATIO_SOURCE
        override var groupId = ""
        override val order = 160
        override val style = MenuItemStyle.Settings
        override val canBePinned = true
        override val icon = MenuIcon.AspectRatio
        override val title = getString("webcam_settings___aspect_ratio_source")

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            preferences.webcamAspectRatioSource = option.value
        }
    }

    class DataSaverIntervalMenuItem : RevolvingOptionsMenuItem {
        override val options = listOf(
            Option(label = getString("target_off"), value = "0"),
            Option(label = getString("x_secs", 0), value = "500"),
            Option(label = getString("x_secs", 1), value = "1000"),
            Option(label = getString("x_secs", 3), value = "3000"),
            Option(label = getString("x_secs", 5), value = "5000"),
            Option(label = getString("x_secs", 10), value = "10000"),
            Option(label = getString("x_secs", 15), value = "15000"),
        )
        override val activeValue = preferences.updatedFlow2.map { it.dataSaverWebcamInterval.toString() }
        override val itemId = MENU_ITEM_WEBCAM_DATA_SAVER_INTERVAL
        override var groupId = ""
        override val order = 163
        override val canBePinned = true
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Antenna
        override val title = getString("webcam_settings___data_saver_webcam")

        // Description is moved to DataSaverConditionsMenuItem when enabled
        override val description get() = if (preferences.dataSaverWebcamInterval == 0L) getString("webcam_settings___data_saver_webcam_description") else null

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            val wasOff = preferences.dataSaverWebcamInterval == 0L
            val newValue = option.value.toLong()
            val isOff = newValue == 0L
            preferences.dataSaverWebcamInterval = newValue

            if (wasOff != isOff) {
                host?.reloadMenu()
            }
        }
    }

    class DataSaverConditionsMenuItem : RevolvingOptionsMenuItem {
        override val options = listOf(
            Option(label = getString("webcam_settings___data_saver_condition_always_on"), value = false.toString()),
            Option(label = getString("webcam_settings___data_saver_condition_mobile_network", 0), value = true.toString()),
        )
        override val activeValue = preferences.updatedFlow2.map { it.dataSaverUseOnMobileNetworkOnly.toString() }
        override val itemId = MENU_ITEM_WEBCAM_DATA_SAVER_INTERVAL
        override var groupId = ""
        override val order = 164
        override val canBePinned = true
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = preferences.dataSaverWebcamInterval > 0L
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Antenna
        override val title = getString("webcam_settings___data_saver_conditions")
        override val description = getString("webcam_settings___data_saver_webcam_description")

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            preferences.dataSaverUseOnMobileNetworkOnly = option.value.toBoolean()
        }
    }

    class OverrideRotationLockMenuItem : ToggleMenuItem {
        override val state = preferences.updatedFlow2.map { it.isOverrideRotationLockForWebcam }


        override val itemId = MENU_ITEM_WEBCAM_DATA_SAVER_INTERVAL
        override var groupId = ""
        override val order = 165
        override val canBePinned = true
        override val style = MenuItemStyle.Settings
        override val icon = MenuIcon.Rotation
        override val supportedTargets: List<MenuTargetPlatform> = listOf(MenuTargetPlatform.Android)
        override val title = getString("webcam_settings___override_rotation_lock")
        override val description = getString("webcam_settings___override_rotation_lock_description")


        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.isOverrideRotationLockForWebcam = enabled
        }
    }
}