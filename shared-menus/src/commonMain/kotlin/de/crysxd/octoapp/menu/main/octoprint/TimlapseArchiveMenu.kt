package de.crysxd.octoapp.menu.main.octoprint

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.DeleteTimelapseUseCase
import de.crysxd.octoapp.base.usecase.DownloadTimelapseUseCase
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.isAndroid
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.MutableStateFlow


@CommonParcelize
class TimelapseArchiveMenu(
    private val instanceId: String,
    private val timelapseFile: TimelapseFile
) : SimpleMenu {

    companion object {
        private const val SharePrefix = "share:"
        private const val PlayPrefix = "play:"
        private const val ExportPrefix = "export:"

        fun getPlayPath(result: String) = if (result.startsWith(PlayPrefix)) {
            result.removePrefix(PlayPrefix)
        } else {
            null
        }

        fun getSharePath(result: String) = if (result.startsWith(SharePrefix)) {
            result.removePrefix(SharePrefix)
        } else {
            null
        }

        fun getExportPath(result: String) = if (result.startsWith(ExportPrefix)) {
            result.removePrefix(ExportPrefix)
        } else {
            null
        }

        private suspend fun MenuHost.downloadAndClose(
            instanceId: String,
            prefix: String,
            file: TimelapseFile,
            progressUpdate: (String) -> Unit = {}
        ) {
            val path = SharedBaseInjector.get().downloadTimelapseUseCase().execute(
                param = DownloadTimelapseUseCase.Param(
                    instanceId = instanceId,
                    timelapseFile = file,
                    progressUpdate = { progressUpdate("$it %") }
                )
            ).filePath

            closeMenu(result = "$prefix$path")
        }
    }

    override val id: String get() = "timalpse/${timelapseFile.name}"
    override val title: String get() = timelapseFile.name
    override val subtitle get() = getString("timelapse_archive___menu___subtitle", timelapseFile.bytes.formatAsFileSize())

    override suspend fun getMenuItems() = listOf(
        DeleteTimelapse(timelapseFile, instanceId),
        PlayTimelapseMenuItem(timelapseFile, instanceId),
        ShareTimelapseMenuItem(timelapseFile, instanceId),
        AddToGalleryMenuItem(timelapseFile, instanceId),
    )

    private class DeleteTimelapse(
        private val file: TimelapseFile,
        private val instanceId: String,
    ) : ConfirmedMenuItem {
        override val itemId = "delete_timelapse"
        override var groupId = ""
        override val order = 1
        override val canBePinned = false
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Delete

        override val title = getString("timelapse_archive___menu___delete")
        override val confirmationMessage = getString("file_manager___file_menu___delete_confirmation_message", file.name)
        override val confirmationPositiveAction = title

        override suspend fun onClicked(host: MenuHost?) {
            SharedBaseInjector.get().deleteTimelapseUseCase().execute(
                param = DeleteTimelapseUseCase.Params(
                    instanceId = instanceId,
                    timelapseFile = file
                )
            )

            host?.closeMenu()
        }
    }

    private abstract class DownloadMenuItem(
        private val file: TimelapseFile,
        private val instanceId: String,
    ) : MenuItem {
        protected abstract val prefix: String
        override val rightDetail = MutableStateFlow("")
        override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = file.downloadPath != null

        override suspend fun onClicked(host: MenuHost?) {
            try {
                host?.downloadAndClose(
                    instanceId = instanceId,
                    prefix = prefix,
                    file = file,
                    progressUpdate = { rightDetail.value = it }
                )
            } finally {
                rightDetail.value = ""
            }
        }
    }

    private class PlayTimelapseMenuItem(
        file: TimelapseFile,
        instanceId: String,
    ) : DownloadMenuItem(
        file = file,
        instanceId = instanceId
    ) {
        override val itemId = "play_timelapse"
        override var groupId = ""
        override val order = 4
        override val canBePinned = false
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Play
        override val title = getString("timelapse_archive___menu___play")
        override val prefix = PlayPrefix
    }

    private class ShareTimelapseMenuItem(
        file: TimelapseFile,
        instanceId: String,
    ) : DownloadMenuItem(
        file = file,
        instanceId = instanceId
    ) {
        override val itemId = "share_timelapse"
        override var groupId = ""
        override val order = 3
        override val canBePinned = false
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Share
        override val title = getString("timelapse_archive___menu___share")
        override val rightDetail = MutableStateFlow("")
        override val prefix: String = SharePrefix
    }

    private class AddToGalleryMenuItem(
        file: TimelapseFile,
        instanceId: String,
    ) : DownloadMenuItem(
        file = file,
        instanceId = instanceId
    ) {
        override val itemId = "add_to_gallery"
        override var groupId = ""
        override val order = 2
        override val canBePinned = false
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.ImageStack
        override val title = if (SharedCommonInjector.get().platform.isAndroid()) {
            getString("timelapse_archive___menu___gallery")
        } else {
            getString("timelapse_archive___menu___camera_roll")
        }
        override val prefix = ExportPrefix
    }
}
