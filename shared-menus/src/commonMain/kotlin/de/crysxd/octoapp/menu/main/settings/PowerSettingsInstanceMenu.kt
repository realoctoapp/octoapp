package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.billing.FEATURE_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CONFIRM_POWER_OFF
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
internal class PowerSettingsInstanceMenu(private val instanceId: String, private val instanceLabel: String) : SimpleMenu {

    override val id get() = "PowerSettingsInstanceMenu"
    override val title get() = instanceLabel

    override suspend fun getMenuItems() = listOf(
        AutomaticLightsSettingsMenuItem(instanceId = instanceId),
        ConfirmPowerOffSettingsMenuItem(instanceId = instanceId),
    )
}

internal class AutomaticLightsSettingsMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MENU_ITEM_AUTOMATIC_LIGHTS
    override var groupId = ""
    override val order = 118
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Lights
    override val canRunWithAppInBackground = false
    override val canBePinned = false
    override val billingManagerFeature = FEATURE_AUTOMATIC_LIGHTS
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_automatic_lights")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(AutomaticLightsSettingsMenu(instanceId = instanceId))
    }
}

internal class ConfirmPowerOffSettingsMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MENU_ITEM_CONFIRM_POWER_OFF
    override var groupId = ""
    override val order = 119
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val canBePinned = false
    override val canRunWithAppInBackground = false
    override val showAsSubMenu = true
    override val icon = MenuIcon.Secure
    override val title = getString("main_menu___item_confirm_power_off")

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(ConfirmPowerOffSettingsMenu(instanceId = instanceId))
    }
}
