package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
internal class AutoConnectPrinterMenu : SimpleMenu {

    override val id get() = "AutoConnectPrinterMenu"
    override val title get() = getString("main_menu___item_auto_connect_printer")
    override val subtitle get() = getString("connect_printer___auto_menu___subtitle")

    override suspend fun getMenuItems() = listOf(AutoConnectPrinterMenuItem())
}

internal class AutoConnectPrinterMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.isAutoConnectPrinter }
    override val itemId = MenuItems.MENU_ITEM_AUTO_CONNECT_PRINTER
    override var groupId = ""
    override val order = 107
    override val canBePinned = true
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Automatic
    override val title = getString("main_menu___item_auto_connect_printer")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.isAutoConnectPrinter = enabled
    }
}
