package de.crysxd.octoapp.menu.terminal

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class TerminalFilterMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "terminal-filters"
    override val title get() = getString("terminal_filters___title")
    override val subtitle get() = getString("terminal_filters___subtitle")

    override suspend fun getMenuItems(): List<MenuItem> = SharedBaseInjector.get().printerConfigRepository
        .get(instanceId)
        ?.settings
        ?.terminalFilters
        ?.mapIndexed { index, filter ->
            FilterOption(
                instanceId = instanceId,
                filter = filter,
                order = index
            )
        } ?: emptyList()

    private class FilterOption(
        private val instanceId: String,
        private val filter: Settings.TerminalFilter,
        override val order: Int,
    ) : ToggleMenuItem {
        private val repo = SharedBaseInjector.get().printerConfigRepository
        override val state = repo.instanceInformationFlow(instanceId).map { config ->
            config?.appSettings?.selectedTerminalFilters?.any { it.name == title } == true
        }
        override val itemId = filter.name
        override var groupId = "none"
        override val title: String = filter.name
        override val style = MenuItemStyle.Settings
        override val canBePinned = false
        override val icon = MenuIcon.Terminal
        override val enforceSingleLine = false

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            repo.updateAppSettings(id = instanceId) { settings ->
                val base = settings.selectedTerminalFilters ?: emptyList()
                settings.copy(selectedTerminalFilters = if (enabled) base + filter else base.filter { it.name != filter.name })
            }
        }
    }
}