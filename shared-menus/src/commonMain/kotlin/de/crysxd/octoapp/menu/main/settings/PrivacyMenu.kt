package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
class PrivacyMenu : SimpleMenu {

    override val id: String get() = "privacy"
    override val title: String get() = getString("privacy_menu___title")
    override val subtitle: String get() = getString("privacy_menu___subtitle")
    override val bottomText: String get() = getString("privacy_menu___bottom_text")

    override suspend fun getMenuItems(): List<MenuItem> = listOf(
        CrashReportingMenuItem(),
        AnalyticsMenuItem(),
    )

    override fun onUrlOpened(menuHost: MenuHost?, url: String) = if (url == "mailto" && menuHost != null) {
        menuHost.openUrl(UriLibrary.getContactUri(bugReport = false))
        true
    } else {
        false
    }

    class CrashReportingMenuItem : ToggleMenuItem {
        override val itemId = MenuItems.MENU_ITEM_CRASH_REPORTING
        override var groupId = ""
        override val order = 10000
        override val style = MenuItemStyle.Neutral
        override val icon = MenuIcon.Bug
        override val title = getString("privacy_menu___crash_reporting_title")
        override val description = getString("privacy_menu___crash_reporting_description")
        override val canBePinned = false
        override val state = preferences.updatedFlow2.map { it.isCrashReportingEnabled }

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.isCrashReportingEnabled = enabled
        }
    }

    class AnalyticsMenuItem : ToggleMenuItem {
        override val itemId = MenuItems.MENU_ITEM_ANALYTICS
        override var groupId = ""
        override val order = 10001
        override val style = MenuItemStyle.Neutral
        override val icon = MenuIcon.Bug
        override val title = getString("privacy_menu___analytics_title")
        override val description = getString("privacy_menu___analytics_description")
        override val canBePinned = false
        override val state = preferences.updatedFlow2.map { it.isAnalyticsEnabled }

        override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
            preferences.isAnalyticsEnabled = enabled
        }
    }
}