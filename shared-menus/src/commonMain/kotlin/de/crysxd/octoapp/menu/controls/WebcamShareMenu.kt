package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GenerateOctoEverywhereLiveLinkUseCase
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

@CommonParcelize
class WebcamShareMenu(val instanceId: String) : SimpleMenu {

    companion object {
        const val ResultShareImage = "share-image"
        const val ResultShareLink = "share-link:"

        fun isShareImage(result: String) = result == ResultShareImage

        fun getSharedLink(result: String): String? = if (result.startsWith(ResultShareLink)) {
            result.removePrefix(ResultShareLink)
        } else {
            null
        }

        fun predetermineCloseMenuResult(instanceId: String) = if (SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.octoEverywhereConnection == null) {
            // No OE, we can only share a image
            ResultShareImage
        } else {
            // OE is available, we can share a live link or image, user must decide
            null
        }
    }

    override val id get() = "share-webcam"
    override val title get() = getString("webcam_share___title")
    override suspend fun getMenuItems(): List<MenuItem> {
        val liveLinkValidity = MutableStateFlow(24.hours)
        return listOf(
            ShareImage(),
            ShareLiveLink(instanceId, liveLinkValidity),
            LiveLinkValidity(liveLinkValidity)
        )
    }

    private class ShareImage : MenuItem {
        override val itemId = "share-image"
        override var groupId = "local"
        override val order = 0
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Webcam
        override val title = getString("webcam_share__snapshot")

        override suspend fun onClicked(host: MenuHost?) {
            host?.closeMenu(ResultShareImage)
        }
    }

    private class ShareLiveLink(
        val instanceId: String,
        val validity: MutableStateFlow<Duration>
    ) : MenuItem {
        override val itemId = "share-live-link"
        override var groupId = "oe"
        override val order = 2
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Link
        override val title = getString("webcam_share___live_link")

        override suspend fun onClicked(host: MenuHost?) {
            val link = SharedBaseInjector.get().generateOctoEverywhereLiveLinkUseCase()
                .execute(GenerateOctoEverywhereLiveLinkUseCase.Params(validity = validity.value, instanceId = instanceId))
            host?.closeMenu("$ResultShareLink$link")
        }
    }

    private class LiveLinkValidity(
        val validity: MutableStateFlow<Duration>,
    ) : RevolvingOptionsMenuItem {
        override val activeValue = validity.map { "${it.inWholeHours}" }
        override val itemId = "live-link-validity"
        override var groupId = "oe"
        override val order = 3
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.TimeLeft
        override val title = getString("webcam_share___live_link_validity")
        override val description = getString("webcam_share___disclaimer")
        override val options = listOf(1.hours, 3.hours, 6.hours, 12.hours, 1.days, 2.days, 3.days, 4.days, 5.days).map {
            RevolvingOptionsMenuItem.Option(label = "${it.inWholeHours}h", value = it.inWholeHours.toString())
        }

        override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
            validity.value = option.value.toInt().hours
        }
    }
}