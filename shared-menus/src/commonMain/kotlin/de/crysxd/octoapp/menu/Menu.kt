package de.crysxd.octoapp.menu

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.ktor.http.Url
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

interface MenuHost {
    suspend fun pushMenu(subMenu: Menu)
    suspend fun popMenu()
    suspend fun closeMenu(result: String? = null)
    suspend fun reloadMenu()
    fun openUrl(url: Url)
}

interface Menu : CommonParcelable {
    val id: String

    val title: String
    val subtitleFlow: Flow<String>? get() = null
    val bottomTextFlow: Flow<String>? get() = null
    val customHeaderViewType: String? get() = null
    val customViewType: String? get() = null
    val emptyStateActionText: String? get() = null
    val emptyStateAnimation: MenuAnimation? get() = null
    val emptyStateAction: (menuHost: MenuHost?) -> Unit? get() = { }
    val emptyStateSubtitle: String? get() = null
    val suppressGrouping: Boolean get() = false

    fun onUrlOpened(menuHost: MenuHost?, url: String): Boolean = false

    @Throws(Throwable::class)
    suspend fun getMenuItems(): List<MenuItem>

    @Throws(Throwable::class)
    fun onDestroy() = Unit
}


interface SimpleMenu : Menu {
    override val subtitleFlow: Flow<String>? get() = subtitle?.let { MutableStateFlow(it).asStateFlow() }
    override val bottomTextFlow: Flow<String>? get() = bottomText?.let { MutableStateFlow(it).asStateFlow() }
    val subtitle: String? get() = null
    val bottomText: String? get() = null
}

interface MenuItem {
    val itemId: String
    var groupId: String
    val order: Int

    val style: MenuItemStyle
    val showAsSubMenu: Boolean get() = false
    val showSuccess: Boolean get() = !showAsSubMenu
    val supportedTargets get() = listOf(MenuTargetPlatform.Darwin, MenuTargetPlatform.Android)
    val canBePinned: Boolean
    val enforceSingleLine: Boolean get() = true
    val secondaryButtonIcon: MenuIcon? get() = null
    val canRunWithAppInBackground: Boolean get() = true
    val billingManagerFeature: String? get() = null
    val icon: MenuIcon
    val title: String
    val iconColorOverwrite: HexColor? get() = null
    val description: String? get() = null
    val badgeCount: Int get() = 0
    val rightDetail: Flow<String>? get() = null

    @Throws(Throwable::class)
    suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = true

    @Throws(Throwable::class)
    suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = true

    @Throws(Throwable::class)
    suspend fun onClicked(host: MenuHost?)

    @Throws(Throwable::class)
    suspend fun onSecondaryClicked(host: MenuHost?) = Unit
}

interface ToggleMenuItem : MenuItem {
    val preferences: OctoPreferences get() = SharedBaseInjector.get().preferences
    val configRepository: PrinterConfigurationRepository get() = SharedBaseInjector.get().printerConfigRepository

    val state: Flow<Boolean>
    val stateEnum: Flow<State> get() = state.map { if (it) State.On else State.Off }
    override val canRunWithAppInBackground get() = false
    override val showSuccess: Boolean get() = false

    suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean)
    override suspend fun onClicked(host: MenuHost?) = handleToggleFlipped(host, !state.first())

    enum class State {
        On, Off
    }
}

interface ConfirmedMenuItem : MenuItem {
    val confirmationPositiveAction: String? get() = null
    val confirmationMessage: String? get() = null
    val secondaryConfirmationPositiveAction: String? get() = null
    val secondaryConfirmationMessage: String? get() = null
    val canSkipConfirmation: Boolean get() = false
}

interface InputMenuItem : MenuItem {
    val currentValue: Flow<String>
    val inputHint: String
    val inputTitle: String get() = title
    val confirmAction: String get() = getString("set")
    val keyboardType: KeyboardType get() = KeyboardType.Normal
    override val canRunWithAppInBackground get() = false
    override val showSuccess get() = false
    override val rightDetail get() = currentValue

    suspend fun onValidateInput(input: String, inputAsNumber: Float?): String?
    suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?)
    suspend fun getCurrentValue() = currentValue.first()

    override suspend fun onClicked(host: MenuHost?) = throw IllegalStateException("Must not be called for InputMenuItem")

    enum class KeyboardType {
        Normal, Numbers, NumbersNegative, Url
    }
}

interface FilePickerMenuItem : MenuItem {
    override val canRunWithAppInBackground get() = false

    override suspend fun onClicked(host: MenuHost?) = throw IllegalStateException("Must not be called for InputMenuItem")
    suspend fun onFileSelected(host: MenuHost?, localPath: String)
}

interface RevolvingOptionsMenuItem : MenuItem {
    val preferences: OctoPreferences get() = SharedBaseInjector.get().preferences
    val configRepository: PrinterConfigurationRepository get() = SharedBaseInjector.get().printerConfigRepository

    val activeValue: Flow<String>
    val options: List<Option>
    override val canRunWithAppInBackground get() = false
    override val rightDetail: Flow<String>?
        get() = activeValue.map { active -> (options.firstOrNull { it.value == active } ?: options.first()).label }

    override val showSuccess: Boolean get() = false

    override suspend fun onClicked(host: MenuHost?) {
        val current = options.indexOfFirst { it.value == activeValue.first() }.coerceAtLeast(0)
        val next = (current + 1) % options.size
        handleOptionActivated(host, options[next])
    }

    suspend fun handleOptionActivated(host: MenuHost?, option: Option)

    data class Option(
        val label: String,
        val value: String
    )
}

enum class MenuItemStyle {
    Support,
    Settings,
    OctoPrint,
    Printer,
    Neutral
}

enum class MenuTargetPlatform {
    Darwin,
    Android
}

enum class MenuAnimation {
    Notificiations,
    PowerDevices,
    Materials
}


