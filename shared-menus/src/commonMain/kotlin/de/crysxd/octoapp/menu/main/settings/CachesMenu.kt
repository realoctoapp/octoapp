package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.milliseconds

@CommonParcelize
class CachesMenu : SimpleMenu {

    override val id get() = "caches"
    override val title get() = getString("caches_menu___title")
    override val subtitle get() = getString("caches_menu___subtitle")

    override suspend fun getMenuItems() = listOf(
        HttpCacheMenuItem(),
        ClearCachesMenuItem(),
        GcodeCacheMenuItem(),
        MediaCacheMenuItem()
    )
}

private class HttpCacheMenuItem : CacheMenuItem(
    get = SharedBaseInjector.get().preferences.updatedFlow2.map { it.httpCacheSize },
    set = { SharedBaseInjector.get().preferences.httpCacheSize = it },
    currentSize = { SharedBaseInjector.get().httpCache.currentSize },
    checkCache = { }
) {
    override val itemId = "httpCache"
    override var groupId = "cache"
    override val order = 1
    override val title = getString("caches_menu___http___title")
    override val description = getString("caches_menu___http___description")
}

private class MediaCacheMenuItem : CacheMenuItem(
    get = SharedBaseInjector.get().preferences.updatedFlow2.map { it.mediaCacheSize },
    set = { SharedBaseInjector.get().preferences.mediaCacheSize = it },
    currentSize = { SharedBaseInjector.get().mediaFileRepository.totalSize },
    checkCache = { SharedBaseInjector.get().mediaFileRepository.checkCacheSize() }
) {
    override val itemId = "mediaCache"
    override var groupId = "cache"
    override val order = 2
    override val title = getString("caches_menu___media___title")
    override val description = getString("caches_menu___media___description")
}

private class GcodeCacheMenuItem : CacheMenuItem(
    get = SharedBaseInjector.get().preferences.updatedFlow2.map { it.gcodeCacheSize },
    set = { SharedBaseInjector.get().preferences.gcodeCacheSize = it },
    currentSize = { SharedBaseInjector.get().localGcodeFileDataSource.totalSize() },
    checkCache = { SharedBaseInjector.get().localGcodeFileDataSource.checkCacheSize() }
) {
    override val itemId = "gcodeCache"
    override var groupId = "cache"
    override val order = 3
    override val title = getString("caches_menu___gcode___title")
    override val description = getString("caches_menu___gcode___description")
}

private class ClearCachesMenuItem : MenuItem {
    override val itemId = "clearCache"
    override var groupId = "clear"
    override val order = 10
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Delete
    override val canBePinned = false
    override val title: String = getString("caches_menu___clear_caches")

    override suspend fun onClicked(host: MenuHost?) {
        withContext(Dispatchers.SharedIO) {
            SharedBaseInjector.get().httpCache.clear()
            SharedBaseInjector.get().localGcodeFileDataSource.clear()
            SharedBaseInjector.get().mediaFileRepository.clear()
        }
        host?.reloadMenu()
    }
}

private abstract class CacheMenuItem(
    get: Flow<Long>,
    private val set: (Long) -> Unit,
    private val currentSize: () -> Long,
    private val checkCache: suspend () -> Unit,
) : RevolvingOptionsMenuItem {
    override val activeValue = get.map { it.toString() }
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Storage
    override val canBePinned = false
    override val canRunWithAppInBackground = false
    override val options
        get() = listOf(32.MiB, 64.MiB, 128.MiB, 256.MiB, 512.MiB, 1024.MiB, 2048.MiB, 4096.MiB, 8192.MiB).map {
            RevolvingOptionsMenuItem.Option(label = "${getCachedSize().formatAsFileSize()} / ${it.formatAsFileSize()}", value = it.toString())
        }

    @Suppress("PrivatePropertyName")
    private val Int.MiB
        get() = this * 1024 * 1024L

    private var lastSize = 0L
    private var lastSizeTime: Instant = Instant.DISTANT_PAST

    fun getCachedSize(): Long {
        // We cache the size here to not access files over and over while inflating the item
        if (Clock.System.now() - lastSizeTime > 500.milliseconds) {
            lastSize = currentSize()
            lastSizeTime = Clock.System.now()
        }
        return lastSize
    }


    override suspend fun handleOptionActivated(host: MenuHost?, option: RevolvingOptionsMenuItem.Option) {
        lastSizeTime = Instant.DISTANT_PAST
        set(option.value.toLong())
        withContext(Dispatchers.SharedIO) { checkCache() }
    }
}