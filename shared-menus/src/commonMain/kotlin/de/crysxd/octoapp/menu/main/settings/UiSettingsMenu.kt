package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.data.models.AppTheme
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.SetAppLanguageUseCase
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.MenuTargetPlatform
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem
import de.crysxd.octoapp.menu.RevolvingOptionsMenuItem.Option
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.menu.main.settings.SettingsMenuHelper.getSupportedAppThemes
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
internal class UiSettingsMenu : SimpleMenu {

    override val id get() = "UiSettingsMenu"
    override val title get() = getString("main_menu___menu_ui_settings_title")
    override val subtitle get() = getString("main_menu___submenu_subtitle")

    override suspend fun getMenuItems() = listOf(
        ChangeLanguageMenuItem(),
        AppThemeMenuItem(),
        KeepScreenOnDuringPrintMenuItem(),
        CustomizeWidgetsMenuItem(),
        LeftHandModeMenuItem(),
        ShowInstanceInStatusBarMenuItem(),
        AllowMoveWhilePrinting(),
        ShowEmergencyStopInBottomBar(),
    )
}

internal class ChangeLanguageMenuItem : MenuItem {
    private val currentLanguage
        get() = SharedBaseInjector.get().getAppLanguageUseCase().executeBlocking(Unit)

    override val itemId = MenuItems.MENU_ITEM_CHANGE_LANGUAGE
    override var groupId = ""
    override val order = 102
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val canBePinned = true
    override val canRunWithAppInBackground = false
    override val icon = MenuIcon.Language
    override val title = currentLanguage.switchLanguageText ?: "???"
    override val supportedTargets = listOf(MenuTargetPlatform.Android)

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = currentLanguage.canSwitchLocale
    override suspend fun onClicked(host: MenuHost?) {
        val newLanguage = currentLanguage.switchLanguage
        SharedBaseInjector.get().setAppLanguageUseCase().execute(SetAppLanguageUseCase.Param(newLanguage))
        SettingsMenuHelper.restartApp()
    }
}

internal class CustomizeWidgetsMenuItem : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_CUSTOMIZE_WIDGETS
    override var groupId = ""
    override val order = 103
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val canBePinned = true
    override val icon = MenuIcon.Customize
    override val canRunWithAppInBackground = false
    override val title = getString("main_menu___item_customize_widgets")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context in listOf(
        MenuContext.Connect,
        MenuContext.Prepare,
        MenuContext.Print,
        MenuContext.PrintPaused,
    )

    override suspend fun onClicked(host: MenuHost?) {
        SettingsMenuHelper.customizeControls(host)
        host?.closeMenu()
    }
}

internal class AppThemeMenuItem : RevolvingOptionsMenuItem {
    override val activeValue get() = preferences.updatedFlow2.map { it.appTheme.toString() }

    override val options = getSupportedAppThemes().map {
        when (it) {
            AppTheme.AUTO -> Option(getString("main_menu___item_app_theme_auto"), AppTheme.AUTO.toString())
            AppTheme.DARK -> Option(getString("main_menu___item_app_theme_dark"), AppTheme.DARK.toString())
            AppTheme.LIGHT -> Option(getString("main_menu___item_app_theme_light"), AppTheme.LIGHT.toString())
        }
    }
    override val itemId = MenuItems.MENU_ITEM_NIGHT_THEME
    override var groupId = ""
    override val order = 105
    override val canBePinned = true
    override val enforceSingleLine = false
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.AppTheme
    override val title = getString("main_menu___item_app_theme")

    override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
        preferences.appTheme = AppTheme.valueOf(option.value)
    }
}

internal class KeepScreenOnDuringPrintMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.isKeepScreenOnDuringPrint }
    override val itemId = MenuItems.MENU_ITEM_SCREEN_ON_DURING_PRINT
    override var groupId = ""
    override val canBePinned = true
    override val order = 108
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.ScreenOn
    override val title = getString("main_menu___item_keep_screen_on_during_pinrt_on")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.isKeepScreenOnDuringPrint = enabled
    }
}

internal class LeftHandModeMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.leftHandMode }
    override val itemId = MenuItems.MENU_ITEM_LEFT_HAND_MODE
    override var groupId = ""
    override val canBePinned = true
    override val order = 107
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.LeftHand
    override val title = getString("main_menu___item_left_hand_mode")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.leftHandMode = enabled
    }
}

internal class AllowMoveWhilePrinting : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.moveDuringPrint }
    override val itemId = MenuItems.MENU_ITEM_MOVE_DURING_PRINT
    override var groupId = ""
    override val canBePinned = true
    override val order = 110
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Move
    override val title = getString("main_menu___move_controls_when_paused")
    override val description = getString("main_menu___move_controls_when_paused_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.moveDuringPrint = enabled
    }
}

internal class ShowEmergencyStopInBottomBar : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.emergencyStopInBottomBar }
    override val itemId = MenuItems.MENU_ITEM_EMERGENCY_STOP_IN_BOTTOM_BAR
    override var groupId = ""
    override val canBePinned = true
    override val order = 109
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.EmergencyStop
    override val title = getString("main_menu___show_emergency_stop_in_bottom_bar")
    override val description = getString("main_menu___show_emergency_stop_in_bottom_bar_description")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.emergencyStopInBottomBar = enabled
    }
}

internal class ShowInstanceInStatusBarMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.showActiveInstanceInStatusBar }
    override val itemId = MenuItems.MENU_ITEM_SHOW_INSTNACE_IN_STATUS_BAR
    override var groupId = ""
    override val canBePinned = true
    override val order = 106
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = MenuIcon.Label
    override val title = getString("main_menu___item_show_instance_in_status_bar")

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.showActiveInstanceInStatusBar = enabled
    }
}