package de.crysxd.octoapp.menu.controls

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.math.roundToInt

@CommonParcelize
class ExecuteMacroMenu(private val instanceId: String, private val macroId: String) : SimpleMenu {

    companion object {
        private fun wrapResult(inputs: Map<String, String>) = Json.encodeToString(inputs)
        fun unwrapResult(inputs: String?) = inputs?.let { Json.decodeFromString<Map<String, String>>(inputs) }
    }

    private val allMacros get() = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.macros ?: emptyList()
    private val macro get() = allMacros.firstOrNull { it.id == macroId }
    val values get() = (macro?.inputs?.associate { it.name to (it.defaultValue ?: "") } ?: emptyMap()).toMutableMap()

    override val id get() = "execute-macro/$macroId"
    override val title get() = macro?.name ?: "???"

    override suspend fun getMenuItems(): List<MenuItem> = (macro?.inputs ?: emptyList()).mapIndexed { index, input ->
        MacroInputMenuItem(
            macroInput = input,
            order = index,
            values = values
        )
    } + ExecuteInputMenuItem(
        order = macro?.inputs?.size ?: 0,
        instanceId = instanceId,
        values = values,
    )

    private class ExecuteInputMenuItem(
        override val order: Int,
        private val instanceId: String,
        private val values: Map<String, String>,
    ) : MenuItem {
        override val itemId = "execute"
        override var groupId = "execute"
        override val style: MenuItemStyle = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Send
        override val title = getString("widget_gcode_send___confirmation_action")

        override suspend fun onClicked(host: MenuHost?) {
            host?.closeMenu(result = wrapResult(values))
        }
    }

    private class MacroInputMenuItem(
        private val macroInput: Macro.Input,
        override val order: Int,
        private val values: MutableMap<String, String>
    ) : InputMenuItem {
        override val currentValue = MutableStateFlow(macroInput.defaultValue ?: "")
        override val inputHint = macroInput.label + rangeLabel
        override val itemId = "macro-input/${macroInput.name}"
        override var groupId = "inputs"
        override val style = MenuItemStyle.Printer
        override val canBePinned = false
        override val icon = MenuIcon.Edit
        override val title = macroInput.label
        override val showSuccess = false
        private val rangeLabel: String
            get() {
                val minValue = macroInput.minValue
                val maxValue = macroInput.maxValue

                return when {
                    minValue != null && maxValue != null -> " ($minValue - $maxValue)"
                    minValue != null -> "(> $minValue)"
                    maxValue != null -> "(< $maxValue)"
                    else -> ""
                }
            }

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?): String? {
            val minValue = macroInput.minValue
            val maxValue = macroInput.maxValue
            return when {
                macroInput.type == Macro.Input.Type.Any -> null
                inputAsNumber == null -> getString("error_please_enter_a_value")
                minValue != null && minValue > inputAsNumber -> getString("error_please_enter_a_value")
                maxValue != null && inputAsNumber > maxValue -> getString("error_please_enter_a_value")
                else -> null
            }
        }

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            val value = when (macroInput.type) {
                Macro.Input.Type.Float -> inputAsNumber?.toString() ?: ""
                Macro.Input.Type.Int -> inputAsNumber?.roundToInt()?.toString() ?: ""
                Macro.Input.Type.Any -> input
            }

            currentValue.value = value
            values[macroInput.name] = value
        }
    }
}