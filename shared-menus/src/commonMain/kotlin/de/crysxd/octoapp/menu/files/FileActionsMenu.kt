package de.crysxd.octoapp.menu.files

import de.crysxd.octoapp.base.billing.FEATURE_FILE_MANAGEMENT
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.DeleteFileUseCase
import de.crysxd.octoapp.base.usecase.DownloadFileUseCase
import de.crysxd.octoapp.base.usecase.MoveFileUseCase
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flowOf

@CommonParcelize
class FileActionsMenu(
    val instanceId: String,
    val display: String,
    val size: Long?,
    val path: String,
    val isDirectory: Boolean
) : Menu {

    companion object {
        private const val actionCopy = "copy"
        private const val actionCut = "cut"
        private const val actionDelete = "delete"
        private const val actionShare = "share:"
        private const val actionMove = "move:"

        fun isCopy(result: String) = result == actionCopy
        fun isCut(result: String) = result == actionCut
        fun isDelete(result: String) = result == actionDelete
        fun getSharePath(result: String) = if (result.startsWith(actionShare)) {
            result.removePrefix(actionShare)
        } else {
            null
        }

        fun getNewPathAfterMove(result: String) = if (result.startsWith(actionMove)) {
            result.removePrefix(actionMove)
        } else {
            null
        }
    }

    override val id get() = "files-actions-${path.hashCode()}"
    override val title get() = display

    override suspend fun getMenuItems() = listOfNotNull(
        DeleteFileMenuItem(display = display, path = path, instanceId = instanceId, isDirectory = isDirectory),
        RenameFileMenuItem(path = path, instanceId = instanceId, display = display),
        CopyFileMenuItem(path = path),
        MoveFileMenuItem(path = path),
        if (isDirectory) null else DownloadAndShareMenuItem(path = path, size = size, instanceId = instanceId),
    )

    class DeleteFileMenuItem(
        val display: String,
        val path: String,
        val isDirectory: Boolean,
        val instanceId: String,
    ) : ConfirmedMenuItem {
        override val itemId = "delete"
        override var groupId = ""
        override val order = 1
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Delete
        override val canBePinned = false
        override val title = getString("file_manager___file_menu___delete")
        override val confirmationMessage = getString("file_manager___file_menu___delete_confirmation_message", display)
        override val confirmationPositiveAction = getString("file_manager___file_menu___delete")

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = path != "/"

        override suspend fun onClicked(host: MenuHost?) {
            SharedBaseInjector.get().deleteFileUseCase().execute(
                DeleteFileUseCase.Params(
                    file = if (isDirectory) FileObject.Folder(path = path) else FileObject.File(path = path),
                    instanceId = instanceId
                )
            )

            host?.closeMenu(actionDelete)
        }
    }

    class RenameFileMenuItem(
        display: String,
        val path: String,
        val instanceId: String,
    ) : InputMenuItem {
        private val extension = FileObject.File(path).extension ?: ""
        private val name = FileObject.File(path).name.removeSuffix(".$extension")

        override val currentValue = flowOf(name)
        override val rightDetail = flowOf("")
        override val itemId = "rename"
        override var groupId = ""
        override val order = 102
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Edit
        override val canBePinned = false
        override val billingManagerFeature = FEATURE_FILE_MANAGEMENT
        override val title = getString("file_manager___file_menu___rename")
        override val inputTitle = getString("file_manager___file_menu___rename_input_title", display)
        override val inputHint = getString("file_manager___file_menu___rename_input_hint")

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = path != "/"

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (input.isBlank() || input.contains("/")) {
            getString("error_please_enter_a_value")
        } else {
            null
        }

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            val newName = input.trim()
            val oldFullName = if (extension.isBlank()) name else "$name.$extension"
            val newFullName = if (extension.isBlank()) newName else "$newName.$extension"
            val newPath = path.removeSuffix(oldFullName) + newFullName

            SharedBaseInjector.get().moveFileUseCase().execute(
                param = MoveFileUseCase.Params(
                    file = FileObject.File(path = path),
                    destinationPath = path.removeSuffix(oldFullName) + newFullName,
                    copyFile = false,
                    destinationOrigin = FileOrigin.Gcode,
                    sourceInstanceId = instanceId,
                    destinationInstanceId = instanceId,
                )
            )

            menuHost?.closeMenu("$actionMove$newPath")
        }
    }

    class CopyFileMenuItem(
        private val path: String
    ) : MenuItem {
        override val itemId = "copy"
        override var groupId = ""
        override val order = 103
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Copy
        override val canBePinned = false
        override val billingManagerFeature = FEATURE_FILE_MANAGEMENT
        override val title = getString("file_manager___file_menu___copy")

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = path != "/"

        override suspend fun onClicked(host: MenuHost?) {
            host?.closeMenu(actionCopy)
        }
    }

    class MoveFileMenuItem(
        private val path: String
    ) : MenuItem {
        override val itemId = "move"
        override var groupId = ""
        override val order = 104
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Cut
        override val canBePinned = false
        override val billingManagerFeature = FEATURE_FILE_MANAGEMENT
        override val title = getString("file_manager___file_menu___move")

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = path != "/"

        override suspend fun onClicked(host: MenuHost?) {
            host?.closeMenu(actionCut)
        }
    }

    class DownloadAndShareMenuItem(
        size: Long?,
        private val path: String,
        private val instanceId: String,
    ) : MenuItem {
        private val mutableRightDetail = MutableStateFlow("")
        override val rightDetail = mutableRightDetail.asStateFlow()

        override val itemId = "download"
        override var groupId = ""
        override val order = 105
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.Share
        override val canBePinned = false
        override val billingManagerFeature = FEATURE_FILE_MANAGEMENT
        override val title = getString("file_manager___file_menu___download_and_share")
        override val description = size?.let { getString("file_manager___file_menu___download_and_share_description", size.formatAsFileSize()) }

        override suspend fun isEnabled(context: MenuContext, systemInfo: SystemInfo) = path != "/"

        override suspend fun onClicked(host: MenuHost?) {
            val sharePath = SharedBaseInjector.get().downloadFileUseCase().execute(
                param = DownloadFileUseCase.Params(
                    file = FileObject.File(path = path),
                    instanceId = instanceId,
                    progressUpdate = { mutableRightDetail.value = "${(it * 100).toInt()}%" },
                )
            ).localFilePath

            host?.closeMenu("$actionShare$sharePath")
        }
    }
}
