package de.crysxd.octoapp.menu.files

import de.crysxd.octoapp.base.billing.FEATURE_FILE_MANAGEMENT
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.CreateFolderUseCase
import de.crysxd.octoapp.base.usecase.UploadFileUseCase
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.menu.FilePickerMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flowOf

@CommonParcelize
class AddItemMenu(
    private val instanceId: String,
    private val path: String,
) : SimpleMenu {

    override val title get() = getString("file_manager___add_menu___title")
    override val id get() = "add-file"

    override suspend fun getMenuItems() = listOf(
        AddFolderMenuItem(instanceId = instanceId, path = path),
        UploadFileMenuItem(instanceId = instanceId, path = path),
    )

    class AddFolderMenuItem(
        private val instanceId: String,
        private val path: String
    ) : InputMenuItem {
        override val currentValue = flowOf("")
        override val itemId = "create_folder"
        override var groupId = ""
        override val order = 101
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.CreateFolder
        override val canBePinned = false
        override val showSuccess = true
        override val billingManagerFeature = FEATURE_FILE_MANAGEMENT
        override val inputHint = getString("file_manager___add_menu___create_folder_input_hint")
        override val title = getString("file_manager___add_menu___create_folder_title")
        override val confirmAction = getString("file_manager___add_menu___create_folder_action")

        override suspend fun onValidateInput(input: String, inputAsNumber: Float?) = if (input.isEmpty()) {
            getString("error_please_enter_a_value")
        } else {
            null
        }

        override suspend fun onNewInput(menuHost: MenuHost?, input: String, inputAsNumber: Float?) {
            SharedBaseInjector.get().createFolderUseCase().execute(
                CreateFolderUseCase.Params(
                    parent = FileObject.Folder(name = "", path = path, origin = FileOrigin.Gcode),
                    name = input,
                    instanceId = instanceId,
                )
            )

            menuHost?.closeMenu()
        }
    }

    class UploadFileMenuItem(
        val instanceId: String,
        val path: String,
    ) : FilePickerMenuItem {
        private val mutableRightDetail = MutableStateFlow("")
        override val rightDetail = mutableRightDetail.asStateFlow()
        override val itemId = "add_file"
        override var groupId = ""
        override val order = 102
        override val style = MenuItemStyle.OctoPrint
        override val icon = MenuIcon.UploadFile
        override val canBePinned = false
        override val billingManagerFeature = FEATURE_FILE_MANAGEMENT
        override val title = getString("file_manager___add_menu___upload_file_title")

        override suspend fun onFileSelected(host: MenuHost?, localPath: String) {
            SharedBaseInjector.get().uploadFileUseCase().execute(
                param = UploadFileUseCase.Params(
                    localPath = localPath,
                    instanceId = instanceId,
                    parent = FileObject.Folder(
                        name = "",
                        path = path,
                        origin = FileOrigin.Gcode,
                    ),
                    progressUpdate =  { progress ->
                        mutableRightDetail.value = if (progress in 0f..<1f) {
                            "${(progress * 100).toInt()}%"
                        } else {
                            ""
                        }
                    }
                ),
            )

            host?.closeMenu()
        }
    }
}