package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.MenuTargetPlatform
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.menu.ToggleMenuItem
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlinx.coroutines.flow.map

@CommonParcelize
internal class AutomaticLightsSettingsMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "AutomaticLightsSettingsMenu/$instanceId"
    override val title get() = getString("main_menu___title_automatic_lights")
    override val subtitle get() = getString("main_menu___subtitle_automatic_lights")
    override val bottomText get() = getString("main_menu___warning_automatic_lights")
    override val emptyStateAction get() = { host: MenuHost? -> host?.openUrl(UriLibrary.getPluginLibraryUri("power")) }
    override val emptyStateActionText get() = getString("power_menu___empty_state_action")
    override val emptyStateSubtitle get() = getString("main_menu___empty_state_subtitle_automatic_lights")
    override val emptyStateAnimation get() = MenuAnimation.PowerDevices

    override suspend fun getMenuItems(): List<MenuItem> {
        val lights = SharedBaseInjector.get().getPowerDevicesUseCase().execute(
            GetPowerDevicesUseCase.Params(
                instanceId = instanceId,
                requiredCapabilities = listOf(PowerDevice.Capability.Illuminate)
            )
        ).results.filter { result ->
            result.device.controlMethods.contains(PowerDevice.ControlMethod.TurnOnOff)
        }.map { result ->
            LightSettingMenuItem(result.device)
        }

        return listOf(
            listOfNotNull(AutoLightsForWidgetMenuItem().takeIf { lights.isNotEmpty() }),
            lights
        ).flatten()
    }
}

internal class AutoLightsForWidgetMenuItem : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.automaticLightsForWidgetRefresh }
    override val itemId = "light_for_widget_refresh"
    override var groupId = "0"
    override val order = 200
    override val canBePinned = false
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Widget
    override val title = getString("main_menu___item_lights_for_widget")
    override val description = getString("main_menu___item_lights_for_widget_description")
    override val supportedTargets = listOf(MenuTargetPlatform.Android)

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        preferences.automaticLightsForWidgetRefresh = enabled
    }
}

internal class LightSettingMenuItem(private val device: PowerDevice) : ToggleMenuItem {
    override val state = preferences.updatedFlow2.map { it.automaticLights.contains(device.id) }
    override val itemId = "light_settings/${device.id}"
    override var groupId = "lights"
    override val order = 100
    override val canBePinned = false
    override val style = MenuItemStyle.Settings
    override val icon = MenuIcon.Light
    override val title = device.displayName

    override suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean) {
        val base = preferences.automaticLights.toMutableSet()
        if (enabled) base.add(device.id) else base.remove(device.id)
        preferences.automaticLights = base
    }
}