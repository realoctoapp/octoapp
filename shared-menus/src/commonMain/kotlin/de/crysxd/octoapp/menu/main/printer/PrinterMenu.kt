package de.crysxd.octoapp.menu.main.printer

import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.DisconnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.EmergencyStopUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.MenuTargetPlatform
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier

@CommonParcelize
class PrinterMenu(private val instanceId: String) : SimpleMenu {

    override val id get() = "printer"
    override val title get() = getString("main_menu___item_show_printer")
    override val subtitle get() = getString("main_menu___submenu_subtitle")

    override suspend fun getMenuItems() = listOf(
        CancelPrintMenuItem(instanceId),
        CancelPrintKeepTemperaturesMenuItem(instanceId),
        EmergencyStopMenuItem(instanceId),
        ShowTemperatureMenuItem(instanceId),
        ShowWebcamMenuItem(instanceId),
        TurnPsuOffMenuItem(instanceId),
        OpenPowerControlsMenuItem(instanceId),
        ShowMaterialPluginMenuItem(instanceId),
        OpenTerminalMenuItem(instanceId),
        CoolDownMenuItem(instanceId),
        DisconnectMenuItem(instanceId),
    )
}

internal class ShowTemperatureMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_TEMPERATURE_MENU
    override var groupId = "links"
    override val order = 310
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val canRunWithAppInBackground = false
    override val icon = MenuIcon.Fire
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_temperature_presets")
    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context != MenuContext.Connect

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(TemperatureMenu(instanceId))
    }
}

internal class ShowMaterialPluginMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_MATERIAL_MENU
    override var groupId = "links"
    override val order = 320
    override val canBePinned = true
    override val canRunWithAppInBackground = false
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Materials
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_materials")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) =
        context != MenuContext.Connect

    override suspend fun onClicked(host: MenuHost?) {
        host?.pushMenu(MaterialMenu(instanceId = instanceId, startPrintAfterSelectionForPath = null))
    }
}

internal class ShowWebcamMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_SHOW_WEBCAM
    override var groupId = "links"
    override val order = 340
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Webcam
    override val showAsSubMenu = true
    override val title = getString("main_menu___show_webcam")
    override val supportedTargets = listOf(MenuTargetPlatform.Android)

    override suspend fun onClicked(host: MenuHost?) {
        host?.openUrl(UriLibrary.getWebcamUri())
    }
}

internal class OpenPowerControlsMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_POWER_CONTROLS
    override var groupId = "links"
    override val order = 330
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerStrip
    override val canRunWithAppInBackground = false
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_open_power_controls")

    override suspend fun onClicked(host: MenuHost?) {
        val devices = SharedBaseInjector.get().getPowerDevicesUseCase().execute(
            GetPowerDevicesUseCase.Params(instanceId = instanceId)
        ).results

        host?.pushMenu(
            if (devices.count() == 1) {
                PowerControlsDeviceMenu(
                    instanceId = instanceId,
                    uniqueDeviceId = devices[0].device.uniqueId,
                    displayName = devices[0].device.displayName,
                    pluginName = devices[0].device.pluginDisplayName,
                    hasRgbw = PowerDevice.ControlMethod.RgbwColor in devices[0].device.controlMethods
                )
            } else {
                PowerControlsMenu(instanceId = instanceId)
            }
        )
    }
}

internal class TurnPsuOffMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_TURN_PSU_OFF
    override var groupId = "actions"
    override val order = 331
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.PowerOff
    override val canBePinned = true
    override val showAsSubMenu = true
    override val title = getString("main_menu___item_turn_psu_off")

    private suspend fun getPowerDevice(): PowerDevice? {
        try {
            val config = SharedBaseInjector.get().printerConfigRepository.get(instanceId) ?: return null
            val devices = SharedBaseInjector.get().getPowerDevicesUseCase().execute(
                GetPowerDevicesUseCase.Params(
                    requiredCapabilities = listOf(PowerDevice.Capability.ControlPrinterPower),
                    allowNetwork = false
                )
            )

            return devices.results.firstOrNull { it.device.id == config.appSettings?.defaultPowerDevices?.get(AppSettings.DEFAULT_POWER_DEVICE_PSU) }?.device
        } catch (e: Exception) {
            Napier.e(tag = "TurnPsuOffMenuItem", message = "Failed to get devices", throwable = e)
            return null
        }
    }

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = if (context in listOf(MenuContext.PrintPaused, MenuContext.Print)) {
        false
    } else {
        getPowerDevice() != null
    }

    override suspend fun onClicked(host: MenuHost?) {
        getPowerDevice()?.turnOff()
    }
}

internal class OpenTerminalMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_OPEN_TERMINAL
    override var groupId = "links"
    override val order = 332
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Terminal
    override val showAsSubMenu = true
    override val canBePinned = true
    override val title = getString("main_menu___item_open_terminal")

    override suspend fun onClicked(host: MenuHost?) {
        host?.openUrl(UriLibrary.getTerminalUri())
    }
}

internal class EmergencyStopMenuItem(private val instanceId: String) : ConfirmedMenuItem {
    override val itemId = MenuItems.MENU_ITEM_EMERGENCY_STOP
    override var groupId = "actions"
    override val order = 350
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.EmergencyStop
    override val canBePinned = true
    override val title = getString("main_menu___item_emergency_stop")
    override val confirmationMessage = getString("emergency_stop_confirmation_message")
    override val confirmationPositiveAction = getString("emergency_stop_confirmation_action")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context in listOf(MenuContext.PrintPaused, MenuContext.Print)

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().emergencyStopUseCase().execute(EmergencyStopUseCase.Params(instanceId))
        host?.closeMenu()
    }
}

internal class CancelPrintKeepTemperaturesMenuItem(private val instanceId: String) : ConfirmedMenuItem {
    override val itemId = MenuItems.MENU_ITEM_CANCEL_PRINT_KEEP_TEMPS
    override var groupId = "actions"
    override val order = 351
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Stop
    override val canBePinned = true
    override val title = getString("main_menu___item_cancel_print_keep_temp")
    override val confirmationMessage = getString("cancel_print_confirmation_message")
    override val confirmationPositiveAction = getString("cancel_print_confirmation_action")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context in listOf(MenuContext.PrintPaused, MenuContext.Print)

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().cancelPrintJobUseCase().execute(
            CancelPrintJobUseCase.Params(instanceId = instanceId, restoreTemperatures = true)
        )
        host?.closeMenu()
    }
}

internal class CancelPrintMenuItem(private val instanceId: String) : ConfirmedMenuItem {
    override val itemId = MenuItems.MENU_ITEM_CANCEL_PRINT
    override var groupId = "actions"
    override val order = 352
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.Stop
    override val canBePinned = true
    override val title = getString("main_menu___item_cancel_print")
    override val confirmationMessage = getString("cancel_print_confirmation_message")
    override val confirmationPositiveAction = getString("cancel_print_confirmation_action")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context in listOf(MenuContext.PrintPaused, MenuContext.Print)

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().cancelPrintJobUseCase().execute(
            CancelPrintJobUseCase.Params(instanceId = instanceId, restoreTemperatures = false)
        )
        host?.closeMenu()
    }
}

internal class CoolDownMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_COOL_DOWN
    override var groupId = "actions"
    override val order = 353
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.CoolDown
    override val title = getString("main_menu___item_cool_down")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context in listOf(MenuContext.Prepare)

    override suspend fun onClicked(host: MenuHost?) {
        SharedBaseInjector.get().applyTemperaturePresetUseCase().execute(
            param = ApplyTemperaturePresetUseCase.Params(
                instanceId = instanceId,
                profileName = ApplyTemperaturePresetUseCase.Params.CoolDownName,
                executeGcode = true,
                componentFilter = { true }
            )
        )
    }
}

internal class DisconnectMenuItem(private val instanceId: String) : MenuItem {
    override val itemId = MenuItems.MENU_ITEM_DISCONNECT
    override var groupId = "actions"
    override val order = 354
    override val canBePinned = true
    override val style = MenuItemStyle.Printer
    override val icon = MenuIcon.UsbOff
    override val title = getString("main_menu___disconnect_printer")

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) =
        context in listOf(MenuContext.Prepare) && systemInfo.printerFirmwareFamily == SystemInfo.FirmwareFamily.Generic

    override suspend fun onClicked(host: MenuHost?) {
        // If a user uses this menu item, they implicitly opt in to manual connection handling. Not ideal, but won't be a common case.
        Napier.w(tag = "DisconnectMenuItem", message = "Settings isAutoConnectPrinter to false to prevent loop")
        SharedBaseInjector.get().preferences.isAutoConnectPrinter = false

        SharedBaseInjector.get().disconnectPrinterUseCase().execute(
            param = DisconnectPrinterUseCase.Params(
                instanceId = instanceId,
            )
        )

        // Close menu, we are now back at connect state
        host?.closeMenu()
    }
}