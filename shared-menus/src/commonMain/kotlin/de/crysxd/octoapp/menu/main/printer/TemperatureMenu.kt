package de.crysxd.octoapp.menu.main.printer

import de.crysxd.octoapp.base.data.models.MenuItems
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.usecase.ApplyTemperaturePresetUseCase
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier


private val printerConfigs by lazy { SharedBaseInjector.get().printerConfigRepository }

private fun findPreset(presetName: String, instanceId: String) =
    printerConfigs.get(instanceId)?.settings?.temperaturePresets?.firstOrNull { it.name == presetName }

@CommonParcelize
class TemperatureMenu(private val instanceId: String) : SimpleMenu {

    override val id: String get() = "TemperatureMenu/$instanceId"
    override val title get() = getString("temperature_menu___title")
    override val subtitle
        get() = getString(
            id = "temperature_menu___subtitle",
            printerConfigs.get(instanceId)?.systemInfo?.interfaceType.label
        )

    override suspend fun getMenuItems(): List<MenuItem> = listOfNotNull(
        listOf(
            CoolDownMenuItem(instanceId),
        ),
        printerConfigs.get(instanceId)?.settings?.temperaturePresets?.map {
            ApplyTemperaturePresetMenuItem(presetName = it.name, instanceId = instanceId)
        }
    ).flatten()
}

@CommonParcelize
class TemperatureSubMenu(private val presetName: String, private val instanceId: String) : SimpleMenu {
    override val id: String get() = "TemperatureSubMenu/$presetName/$instanceId"
    override val title get() = presetName
    override val subtitle get() = getString("main_menu___submenu_subtitle")

    override suspend fun getMenuItems(): List<BaseApplyTemperaturePresetMenuItem> {
        // If we have more than one extruder list each individually
        val extruders = findPreset(instanceId = instanceId, presetName = presetName)
            ?.components
            ?.filter { it.isExtruder }
            ?.takeIf { it.size > 1 }
            ?: emptyList()

        return listOf(
            ApplyTemperaturePresetForAllMenuItem(presetName = presetName, instanceId = instanceId),
            ApplyTemperaturePresetForHotendMenuItem(presetName = presetName, instanceId = instanceId, componentKey = null)
        ) + extruders.map {
            ApplyTemperaturePresetForHotendMenuItem(presetName = presetName, instanceId = instanceId, componentKey = it.key)
        } + listOf(
            ApplyTemperaturePresetForBedMenuItem(presetName = presetName, instanceId = instanceId),
            ApplyTemperaturePresetForChamberMenuItem(presetName = presetName, instanceId = instanceId),
            ExecuteGcodeForTemperaturePresetMenuItem(presetName = presetName, instanceId = instanceId),
        )
    }
}

abstract class BaseApplyTemperaturePresetMenuItem(
    val presetName: String,
    val instanceId: String
) : MenuItem {

    override val style = MenuItemStyle.Printer
    override var groupId = ""
    override val icon = MenuIcon.Fire
    override val canBePinned = true
    private val profile get() = printerConfigs.get(instanceId)?.settings?.temperaturePresets?.firstOrNull { it.name == presetName }
    abstract val executeGcode: Boolean

    abstract fun filterComponents(component: Settings.TemperaturePreset.Component): Boolean

    override suspend fun isVisible(context: MenuContext, systemInfo: SystemInfo) = context != MenuContext.Connect && isVisible(profile)

    override suspend fun onClicked(host: MenuHost?) {
        Napier.i(tag = "BaseApplyTemperaturePresetMenuItem", message = "Applying temperature preset $presetName")
        SharedBaseInjector.get().applyTemperaturePresetUseCase().execute(
            param = ApplyTemperaturePresetUseCase.Params(
                instanceId = instanceId,
                profileName = presetName,
                executeGcode = executeGcode,
                componentFilter = ::filterComponents
            )
        )
    }

    open fun isVisible(profile: Settings.TemperaturePreset?): Boolean = profile?.components?.any(::filterComponents) == true
}


class ApplyTemperaturePresetMenuItem(
    presetName: String,
    instanceId: String
) : BaseApplyTemperaturePresetMenuItem(
    presetName = presetName,
    instanceId = instanceId
) {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = ApplyTemperaturePresetMenuItem(
            presetName = itemId.replace(MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET, ""),
            instanceId = instanceId
        )
    }

    override val itemId = MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET + presetName
    override val order = 311
    override val title: String = getString("temperature_menu___item_preheat", presetName)
    override val secondaryButtonIcon = MenuIcon.More
    override val executeGcode = true

    override suspend fun onSecondaryClicked(host: MenuHost?) {
        host?.pushMenu(TemperatureSubMenu(presetName = presetName, instanceId = instanceId))
    }

    override fun filterComponents(component: Settings.TemperaturePreset.Component) = true

    override fun isVisible(profile: Settings.TemperaturePreset?) = profile != null
}

class ApplyTemperaturePresetForAllMenuItem(
    presetName: String,
    instanceId: String,
) : BaseApplyTemperaturePresetMenuItem(
    presetName = presetName,
    instanceId = instanceId
) {
    companion object {
        fun forItemId(itemId: String, instanceId: String) = ApplyTemperaturePresetForAllMenuItem(
            presetName = itemId.replace(MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_ALL, ""),
            instanceId = instanceId
        )
    }

    override val itemId = MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_ALL + presetName
    override val order = 312
    override val title: String = getString("temperature_menu___item_preheat", presetName)
    override val executeGcode = true

    override fun filterComponents(component: Settings.TemperaturePreset.Component) = true


    override fun isVisible(profile: Settings.TemperaturePreset?) = profile != null
}


class ApplyTemperaturePresetForHotendMenuItem(
    presetName: String,
    instanceId: String,
    private val componentKey: String?,
) : BaseApplyTemperaturePresetMenuItem(
    presetName = presetName,
    instanceId = instanceId
) {

    companion object {
        fun forItemId(itemId: String, instanceId: String): ApplyTemperaturePresetForHotendMenuItem {
            val parts = itemId.replace(MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_HOTEND, "").split("/")
            return ApplyTemperaturePresetForHotendMenuItem(
                presetName = parts[0],
                componentKey = parts.getOrNull(1),
                instanceId = instanceId
            )
        }
    }

    private val displayName = findPreset(presetName = presetName, instanceId = instanceId)?.components?.firstOrNull(::filterComponents)?.displayName
    private val itemIdSuffix = if (componentKey != null) "/$componentKey" else ""
    private val titleSuffix = if (componentKey != null) displayName?.let { " ($it)" } ?: "" else ""

    override val itemId = MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_HOTEND + presetName + itemIdSuffix
    override val title = getString("temperature_menu___item_preheat_hotend", presetName) + titleSuffix
    override val order = 312
    override val executeGcode = false

    override fun filterComponents(component: Settings.TemperaturePreset.Component) =
        (component.isExtruder && componentKey == null) || (component.isExtruder && component.key == componentKey)
}

class ApplyTemperaturePresetForBedMenuItem(
    presetName: String,
    instanceId: String,
) : BaseApplyTemperaturePresetMenuItem(
    presetName = presetName,
    instanceId = instanceId
) {

    companion object {
        fun forItemId(itemId: String, instanceId: String) = ApplyTemperaturePresetForBedMenuItem(
            presetName = itemId.replace(MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_BED, ""),
            instanceId = instanceId
        )
    }

    override val itemId = MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_BED + presetName
    override val order = 313
    override val title = getString("temperature_menu___item_preheat_bed", presetName)
    override val executeGcode = false

    override fun filterComponents(component: Settings.TemperaturePreset.Component) = component.isBed

    override fun isVisible(profile: Settings.TemperaturePreset?) =
        super.isVisible(profile) && printerConfigs.get(instanceId)?.activeProfile?.heatedBed != false
}

class ApplyTemperaturePresetForChamberMenuItem(
    presetName: String,
    instanceId: String,
) : BaseApplyTemperaturePresetMenuItem(
    presetName = presetName,
    instanceId = instanceId
) {

    companion object {
        fun forItemId(itemId: String, instanceId: String) = ApplyTemperaturePresetForChamberMenuItem(
            presetName = itemId.replace(MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_CHAMBER, ""),
            instanceId = instanceId
        )
    }

    override val itemId = MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_CHAMBER + presetName
    override val order = 314
    override val title = getString("temperature_menu___item_preheat_chamber", presetName)
    override val executeGcode = false

    override fun filterComponents(component: Settings.TemperaturePreset.Component) = component.isChamber

    override fun isVisible(profile: Settings.TemperaturePreset?) =
        super.isVisible(profile) && printerConfigs.get(instanceId)?.activeProfile?.heatedChamber != false
}

class ExecuteGcodeForTemperaturePresetMenuItem(
    presetName: String,
    instanceId: String,
) : BaseApplyTemperaturePresetMenuItem(
    presetName = presetName,
    instanceId = instanceId
) {

    companion object {
        fun forItemId(itemId: String, instanceId: String) = ExecuteGcodeForTemperaturePresetMenuItem(
            presetName = itemId.replace(MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_GCODE, ""),
            instanceId = instanceId
        )
    }

    override val itemId = MenuItems.MENU_ITEM_APPLY_TEMPERATURE_PRESET_GCODE + presetName
    override val order = 315
    override val title = getString("temperature_menu___item_execute_gcode", presetName)
    override val executeGcode = true

    override fun isVisible(profile: Settings.TemperaturePreset?) =
        profile?.gcode != null

    override fun filterComponents(component: Settings.TemperaturePreset.Component) = false
}