package de.crysxd.octoapp.menu.main.settings

import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuIcon
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.SimpleMenu
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.utils.getString

@CommonParcelize
class AndroidBillingMenu : SimpleMenu {

    override val id: String get() = "androidBilling"
    override val title: String get() = getString("billing_menu___title")
    override val subtitle: String get() = getString("billing_menu___description")


    override suspend fun getMenuItems() = listOf<MenuItem>(
        TransferPurchaseMenuItem()
    )

    private class TransferPurchaseMenuItem : MenuItem {
        override val itemId = "supporter_status"
        override var groupId = "default"
        override val order = 133
        override val canBePinned = false
        override val style = MenuItemStyle.Settings
        override val enforceSingleLine = false
        override val icon = MenuIcon.Support
        override val title = getString("billing_menu___transfer_title")
        override val description = getString("billing_menu___transfer_description")

        override suspend fun onClicked(host: MenuHost?) {
            SettingsMenuHelper.transferPurchase(host)
        }
    }
}