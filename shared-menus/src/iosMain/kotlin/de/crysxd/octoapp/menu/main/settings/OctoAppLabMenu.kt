package de.crysxd.octoapp.menu.main.settings

internal actual fun createMenuItems() = listOf(
    SuppressM115Request(),
    AllowTerminalDuringPrint(),
    DebugNetworkLogging(),
    WebsocketSubscription(),
    ActivePluginIntegrations(),
    LongTimeouts(),
    ShowAllMaterialPropertiesMenuItem(),
    LayersFromSlicer(),
    StartArZero(),
    TestCrash(),
    TestError(),
)