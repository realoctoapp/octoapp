package de.crysxd.octoapp.menu.main.settings

interface AndroidMenuHost {
    fun startLiveNotificationService()
    fun requestNotificationPermission()
    fun customizeControls()
    suspend fun transferPurchase()
}