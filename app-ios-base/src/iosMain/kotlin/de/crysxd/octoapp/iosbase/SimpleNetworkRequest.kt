package de.crysxd.octoapp.iosbase

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GenericDownloadUseCase
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.cinterop.allocArrayOf
import kotlinx.cinterop.memScoped
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import kotlinx.coroutines.withContext
import platform.Foundation.NSData
import platform.Foundation.create
import platform.UIKit.UIImage

// We limit concurrent requests here bec
private val RequestSemaphore = Semaphore(permits = 5)

@Throws(Throwable::class)
suspend fun simpleNetworkRequest(
    instanceId: String,
    path: String,
    pathEncoded: Boolean
): DeferredResponse = withContext(Dispatchers.SharedIO) {
    Napier.i(tag = "SimpleNetworkRequest", message = "Starting request to $path on $instanceId")
    async {
        RequestSemaphore.withPermit {
            doSimpleNetworkRequest(
                instanceId = instanceId,
                path = path,
                pathEncoded = pathEncoded
            )
        }
    }.let {
        DeferredResponse(it)
    }
}

private suspend fun doSimpleNetworkRequest(
    instanceId: String,
    path: String,
    pathEncoded: Boolean
) = SharedBaseInjector.get().genericDownloadUseCase().execute(
    param = GenericDownloadUseCase.Params.FromPrinter(
        instanceId = instanceId,
        path = path,
        pathEncoded = pathEncoded
    )
).toNsData()

class DeferredResponse(
    private val deferred: Deferred<NSData>
) {
    @Throws(Throwable::class)
    suspend fun awaitResponse(): NSData = withContext(Dispatchers.Default) {
        deferred.await()
    }

    @Throws(Throwable::class)
    suspend fun awaitImageResponse(): UIImage? = withContext(Dispatchers.Default) {
        UIImage.imageWithData(deferred.await())
    }

    fun cancel() = deferred.cancel()
}

@OptIn(kotlinx.cinterop.ExperimentalForeignApi::class, kotlinx.cinterop.BetaInteropApi::class)
fun ByteArray.toNsData(): NSData = memScoped {
    NSData.create(bytes = allocArrayOf(this@toNsData), length = this@toNsData.size.toULong())
}
