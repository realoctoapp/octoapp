package de.crysxd.octoapp.iosbase

import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import de.crysxd.octoapp.base.data.models.PurchaseOffers
import de.crysxd.octoapp.base.get

@Suppress("Unused")
fun getPurchaseOffers() : PurchaseOffers = OctoConfig.get(OctoConfigField.PurchaseOffers)