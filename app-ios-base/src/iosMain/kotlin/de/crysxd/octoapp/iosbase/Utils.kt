package de.crysxd.octoapp.iosbase

import de.crysxd.octoapp.menu.FilePickerMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.sharedcommon.utils.getString
import platform.Foundation.NSNumberFormatter


suspend fun MenuItem.onClickedSafe(menuHost: MenuHost?) = try {
    onClicked(menuHost)
    null
} catch (e: Throwable) {
    io.github.aakira.napier.Napier.e(tag = "Utils", message = "Caught exception in onClickedSafe block", throwable = e)
    e
}

suspend fun FilePickerMenuItem.onFileSelectedSafe(menuHost: MenuHost?, path: String) = try {
    onFileSelected(menuHost, path)
    null
} catch (e: Throwable) {
    io.github.aakira.napier.Napier.e(tag = "Utils", message = "Caught exception in onClickedSafe block", throwable = e)
    e
}


suspend fun MenuItem.onSecondaryClickedSafe(menuHost: MenuHost?) = try {
    onSecondaryClicked(menuHost)
    null
} catch (e: Throwable) {
    io.github.aakira.napier.Napier.e(tag = "Utils", message = "Caught exception in onSecondaryClickedSafe block", throwable = e)
    e
}

suspend fun InputMenuItem.onNewInputSafe(menuHost: MenuHost?, input: String) = try {
    onNewInput(menuHost, input, input.parseFloat())
    null
} catch (e: Throwable) {
    io.github.aakira.napier.Napier.e(tag = "Utils", message = "Caught exception in onNewInputSafe block", throwable = e)
    e
}

suspend fun InputMenuItem.onValidateInputSave(input: String) = try {
    onValidateInput(input, input.parseFloat())
} catch (e: Throwable) {
    io.github.aakira.napier.Napier.e(tag = "Utils", message = "Caught exception in onValidateInputSave block", throwable = e)
    getString("error_general")
}

private fun String.parseFloat(): Float? = try {
    NSNumberFormatter().numberFromString(this)?.floatValue
} catch (e: Exception) {
    io.github.aakira.napier.Napier.e(tag = "Utils", message = "Failed to parse number from '$this'", throwable = e)
    null
}