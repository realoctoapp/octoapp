package de.crysxd.octoapp.iosbase

import kotlinx.datetime.Instant

fun newInstant(millisSince1970: Long) = Instant.fromEpochMilliseconds(millisSince1970)