plugins {
    alias(libs.plugins.kotlinMultiplatform)
}

kotlin {
    val wrappedProjects = listOf(
        projects.sharedCommon,
        projects.sharedEngines,
        projects.sharedExternalApis,
        projects.sharedMenus,
        projects.sharedViewmodels,
        projects.sharedBase,
    )

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "OctoAppBase"
            isStatic = true
            wrappedProjects.forEach { p ->
                export(p)
            }
        }
    }

    sourceSets {
        commonMain.dependencies {
            wrappedProjects.forEach { p ->
                api(p)
            }
        }
        commonTest.dependencies {
            implementation(kotlin("test"))
        }
    }
}