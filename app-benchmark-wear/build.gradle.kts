import de.crysxd.octoapp.buildscript.octoAppAndroidBenchmark

plugins {
    alias(libs.plugins.androidTest)
    alias(libs.plugins.kotlinAndroid)
}

octoAppAndroidBenchmark(
    name = "benchmarkwear",
    targetProject = ":app-wear"
)