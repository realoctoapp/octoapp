package de.crysxd.baseui.common.gcode

import android.graphics.PointF
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.lifecycle.lifecycleScope
import androidx.transition.TransitionManager
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.menu.MenuBottomSheetFragment
import de.crysxd.baseui.databinding.GcodePreviewFragmentBinding
import de.crysxd.baseui.di.injectActivityViewModel
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.drawableRes
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.gcode.GcodeRenderView
import de.crysxd.octoapp.base.models.GcodeMove
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.menu.controls.GcodeSettingsMenu
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.ext.formatAsLayerHeight
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.serialization.json.Json
import kotlin.math.roundToInt

class GcodePreviewFragment : BaseFragment() {

    companion object {
        private const val TAG = "GcodePreviewFragment"
        private const val ARG_FILE = "file"
        private const val ARG_USE_LIVE = "useLive"
        private const val ARG_STANDALONE_SCREEN = "standaloneScreen"
        private const val LAYER_PROGRESS_STEPS = 1000
        private const val NOT_LIVE_IF_NO_UPDATE_FOR_MS = 5000L

        fun createForFile(file: FileObject.File, useLive: Boolean) = GcodePreviewFragment().apply {
            arguments = bundleOf(ARG_FILE to file, ARG_USE_LIVE to useLive)
        }
    }

    private lateinit var binding: GcodePreviewFragmentBinding
    private var forceUpdateSlidersOnNext = false
    private var hideLiveJob: Job? = null
    private val file get() = requireNotNull(requireArguments().getParcelableCompat<FileObject.File>(ARG_FILE)) { "Missing file" }
    private val useLive get() = requireArguments().getBoolean(ARG_USE_LIVE, true)
    private val isStandaloneScreen get() = requireArguments().getBoolean(ARG_STANDALONE_SCREEN)
    override val viewModel: GcodePreviewViewModel by injectActivityViewModel()
    private val seekBarListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
        override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                pushSeekBarValuesToViewModel(seekBar)
                Json
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        GcodePreviewFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.downloadGcode(file, false)

        if (useLive) {
            viewModel.useLiveProgress()
        } else {
            viewModel.useManualProgress(0, 1f)
            forceUpdateSlidersOnNext = true
        }

        binding.downloadLargeFile.setOnClickListener { viewModel.downloadGcode(file, true) }
        binding.retryButton.setOnClickListener { viewModel.downloadGcode(file, true) }
        binding.downloadLargeFile.text = getString(R.string.download_x, file.size?.formatAsFileSize() ?: "")

        binding.layerSeekBar.setOnSeekBarChangeListener(seekBarListener)
        binding.layerProgressSeekBar.setOnSeekBarChangeListener(seekBarListener)

        binding.nextLayerButton.setOnClickListener {
            binding.layerSeekBar.progress += 1
            pushSeekBarValuesToViewModel(binding.layerSeekBar)
        }

        binding.previousLayerButton.setOnClickListener {
            binding.layerSeekBar.progress -= 1
            pushSeekBarValuesToViewModel(binding.layerSeekBar)
        }

        binding.settingsButton.setOnClickListener {
            MenuBottomSheetFragment.createForMenu(
                menu = GcodeSettingsMenu(),
                instanceId = null,
            ).show(childFragmentManager)
        }

        if (isStandaloneScreen) {
            binding.renderView.updatePadding(top = requireContext().resources.getDimensionPixelSize(R.dimen.common_view_top_padding))
        }

        binding.textViewDisbaledDescription.text = getString(R.string.supporter_perk___description, getString(R.string.gcode_preview))
        binding.syncButton.isVisible = isStandaloneScreen
        binding.syncButtonSeparator.isVisible = isStandaloneScreen
        binding.syncButton.setOnClickListener {
            (viewModel.viewState.value as? GcodePreviewViewModel.ViewState.DataReady)?.let { currentState ->
                if (currentState.isLive) {
                    pushSeekBarValuesToViewModel()
                } else {
                    viewModel.useLiveProgress()
                }
            }
        }

        binding.buttonEnableFeature.setOnClickListener {
            OctoAnalytics.logEvent(OctoAnalytics.Event.PurchaseScreenOpen, mapOf("trigger" to "gcode_preview"))
            UriLibrary.getPurchaseUri().open(requireOctoActivity())
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreatedFixed {
            viewModel.viewState.observe(viewLifecycleOwner, ::updateViewState)
        }
    }

    private fun pushSeekBarValuesToViewModel(seekBar: SeekBar? = null) {
        // Show entire layer if layer is changed
        if (seekBar == binding.layerSeekBar) {
            binding.layerProgressSeekBar.progress = LAYER_PROGRESS_STEPS
        }

        viewModel.useManualProgress(
            layer = binding.layerSeekBar.progress,
            progress = binding.layerProgressSeekBar.progress / LAYER_PROGRESS_STEPS.toFloat()
        )
    }

    override fun onStart() {
        super.onStart()

        if (isStandaloneScreen) {
            updateKeepScreenOn()
            Napier.i(tag = tag, message = "Start")
        }
    }


    private fun updateKeepScreenOn() {
        if (BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint) {
            Napier.i(tag = tag, message = "[GCD] Keeping screen on")
            requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            Napier.i(tag = tag, message = "[GCD] Not keeping screen on")
            requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    override fun onStop() {
        super.onStop()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        Napier.i(tag = tag, message = "Stop")
    }

    private fun updateViewState(state: GcodePreviewViewModel.ViewState) {
        TransitionManager.beginDelayedTransition(view as ViewGroup)

        binding.renderGroup.isVisible = state is GcodePreviewViewModel.ViewState.DataReady
        binding.largeFileGroup.isVisible = state is GcodePreviewViewModel.ViewState.LargeFileDownloadRequired
        binding.errorGroup.isVisible = state is GcodePreviewViewModel.ViewState.Error
        binding.featureDisabledGroup.isVisible = state is GcodePreviewViewModel.ViewState.FeatureDisabled

        when (state) {
            is GcodePreviewViewModel.ViewState.Loading -> {
                binding.loadingGroup.isVisible = true
                binding.progressBar.progress = (state.progress * 100).roundToInt()
                binding.progressBar.isIndeterminate = state.progress == 0f
                Napier.v(tag = tag, message = "Progress: ${state.progress}")
            }

            is GcodePreviewViewModel.ViewState.DataReady -> {
                binding.loadingGroup.isVisible = false
                render(state)
            }

            is GcodePreviewViewModel.ViewState.Error -> {
                binding.loadingGroup.isVisible = false
//                requireOctoActivity().showDialog(state.exception)
                Napier.i(tag = tag, message = "[GCD] Error :(")
            }

            is GcodePreviewViewModel.ViewState.LargeFileDownloadRequired -> {
                binding.loadingGroup.isVisible = false
                Napier.i(tag = tag, message = "[GCD] Large download required")
            }

            is GcodePreviewViewModel.ViewState.FeatureDisabled -> {
                binding.loadingGroup.isVisible = false
                binding.preview.setImageResource(state.printBed.drawableRes)
                binding.preview.foreground = ContextCompat.getDrawable(requireContext(), R.drawable.gcode_preview)
                Napier.i(tag = tag, message = "[GCD] Feature disabled")
            }
        }
    }

    private fun render(state: GcodePreviewViewModel.ViewState.DataReady) {
        if (state.renderContext == null || state.printerProfile == null) {
            Napier.e(
                tag = tag,
                message = "Failed",
                throwable = IllegalStateException("Incomplete data ready state exposed ${state.renderContext == null} ${state.printerProfile == null}")
            )
            return
        }

        binding.layerSeekBar.max = state.renderContext.layerCount - 1
        binding.layerProgressSeekBar.max = LAYER_PROGRESS_STEPS
        if (state.isLive || forceUpdateSlidersOnNext) {
            forceUpdateSlidersOnNext = false
            binding.layerSeekBar.progress = state.renderContext.layerNumber
            binding.layerProgressSeekBar.progress = (state.renderContext.layerProgress * LAYER_PROGRESS_STEPS).roundToInt()
        }

        binding.syncButton.setImageResource(
            if (state.isLive) {
                R.drawable.ic_round_sync_disabled_24
            } else {
                R.drawable.ic_round_sync_24
            }
        )

        binding.live.isVisible = state.isLive
        hideLiveJob?.cancel()
        hideLiveJob = viewLifecycleOwner.lifecycleScope.launchWhenCreatedFixed {
            delay(NOT_LIVE_IF_NO_UPDATE_FOR_MS)
            binding.live.isVisible = false
        }

        val layerProgressPercent = binding.layerProgressSeekBar.progress / LAYER_PROGRESS_STEPS.toFloat()
        binding.layerNumber.text = getString(
            R.string.x_of_y,
            state.renderContext.layerNumberDisplay(state.renderContext.layerNumber),
            state.renderContext.layerCountDisplay(state.renderContext.layerCount)
        )
        binding.layerHeight.text = state.renderContext.layerZHeight.formatAsLayerHeight()
        binding.layerProgress.text = layerProgressPercent.times(100).formatAsPercent()
        binding.unsupportedGcode.isVisible = state.renderContext.completedLayerPaths.any { path -> path.type == GcodeMove.Type.Unsupported && path.moveCount > 0 }

        // Only switch to async render if the view recommends it.
        // This way we have smooth scrolling as long as possible but never block the UI thread
        val renderView = binding.renderView as GcodeRenderView
        if (renderView.asyncRenderRecommended && !renderView.useAsyncRender) {
            renderView.enableAsyncRender(viewLifecycleOwner.lifecycleScope)
        }

        // Show warning if print area is exceeded
        binding.printAreaWarning.isVisible = state.exceedsPrintArea == true
        if (binding.printAreaWarning.isVisible) {
            val volume = state.printerProfile.volume
            binding.printAreaWarning.text =
                getString(R.string.gcode_preview___warning_print_area, volume.width.toInt(), volume.depth.toInt(), state.printerProfile.name)
        }

        renderView.renderParams = GcodeRenderView.RenderParams(
            renderContext = state.renderContext,
            printBed = state.printBed,
            originInCenter = state.printerProfile.volume.origin == PrinterProfile.Origin.Center,
            printBedSizeMm = PointF(state.printerProfile.volume.width, state.printerProfile.volume.depth),
            extrusionWidthMm = state.printerProfile.estimatedNozzleDiameter,
            quality = state.settings.quality,
        )
    }

    private val GcodePreviewViewModel.ViewState.DataReady?.isLive get() = this?.fromUser != null && !fromUser
}