package de.crysxd.baseui.common.controls

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.awaitEachGesture
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.horizontalDrag
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInRoot
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoDialog
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.layout.LeftyModeRow
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsPadding
import de.crysxd.baseui.compose.theme.LocalLeftyMode
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.menu.main.MainMenu
import de.crysxd.octoapp.menu.main.printer.TemperatureMenu
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

@Composable
fun ControlsBottomBarColumn(
    content: @Composable ColumnScope.() -> Unit,
) = ControlsBottomBarSwipeConfirmation {
    Column {
        val (state, scope) = rememberControlsBottomBarState()
        CompositionLocalProvider(LocalControlsBottomBarScope provides scope) {
            Column(Modifier.weight(1f)) {
                content()
            }
        }

        ControlsBottomBar(state = state)
    }
}

//region BottomBar
private val LocalOriginalLayoutDirection = compositionLocalOf { LayoutDirection.Ltr }
private val LocalSwipeConfirmation = compositionLocalOf<SwipeConfirmationScope> { throw IllegalStateException("Not available") }
val LocalControlsBottomBarScope = compositionLocalOf<ControlsBottomBarScope?> { null }

@Composable
private fun rememberControlsBottomBarState(): Pair<ControlsBottomBarState, ControlsBottomBarScope> {
    val vmFactory = ControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(factory = vmFactory, modelClass = ControlsViewModel::class.java, key = vmFactory.id)
    val coroutineScope = rememberCoroutineScope()
    val currentMessage = vm.currentMessage.collectAsStateWhileActive(initial = null, key = "controls-current")
    val emergencyStop = vm.emergencyStopInBottomBar.collectAsStateWhileActive(initial = false, key = "controls-emergency")
    val swipeConfirmations = vm.swipeConfirmations.collectAsStateWhileActive(initial = true, key = "swipe-confirmations")
    val actionOverride = remember { mutableStateOf<ControlsBottomBarAction?>(null) }
    val controlCenter = LocalControlCenter.current
    val instanceId = LocalOctoPrint.current.id

    var showMainMenu by remember { mutableStateOf(false) }
    var showTemperaturePresets by remember { mutableStateOf(false) }
    var showConfirmPause by remember { mutableStateOf(false) }
    var showConfirmResume by remember { mutableStateOf(false) }
    var showConfirmCancel by remember { mutableStateOf(false) }
    var showConfirmEmergencyStop by remember { mutableStateOf(false) }

    if (showMainMenu) {
        MenuSheet(
            menu = MainMenu(instanceId),
            onResult = {
                coroutineScope.launch {
                    // Let menu close, then handle result
                    if (it == MainMenu.ResultOpenControlCenter) {
                        delay(0.5.seconds)
                        controlCenter?.show()
                    }
                }
            },
            onDismiss = { showMainMenu = false }
        )
    }

    if (showTemperaturePresets) {
        MenuSheet(
            menu = TemperatureMenu(instanceId),
            onDismiss = { showTemperaturePresets = false }
        )
    }

    if (showConfirmPause) {
        OctoDialog(
            title = null,
            message = stringResource(R.string.pause_print_confirmation_message),
            positiveButton = stringResource(R.string.pause_print_confirmation_action),
            onPositive = { vm.togglePausePrint() },
            negativeButton = stringResource(R.string.cancel),
            onDismissRequest = { showConfirmPause = false }
        )
    }

    if (showConfirmResume) {
        OctoDialog(
            title = null,
            message = stringResource(R.string.resume_print_confirmation_message),
            positiveButton = stringResource(R.string.resume_print_confirmation_action),
            onPositive = { vm.togglePausePrint() },
            negativeButton = stringResource(R.string.cancel),
            onDismissRequest = { showConfirmResume = false }
        )
    }

    if (showConfirmCancel) {
        OctoDialog(
            title = null,
            message = stringResource(R.string.cancel_print_confirmation_message),
            positiveButton = stringResource(R.string.cancel_print_confirmation_action),
            onPositive = { vm.cancelPrint() },
            negativeButton = stringResource(R.string.cancel),
            onDismissRequest = { showConfirmCancel = false }
        )
    }

    if (showConfirmEmergencyStop) {
        OctoDialog(
            title = null,
            message = stringResource(R.string.emergency_stop_confirmation_message),
            positiveButton = stringResource(R.string.emergency_stop_confirmation_action),
            onPositive = { vm.emergencyStop() },
            negativeButton = stringResource(R.string.cancel),
            onDismissRequest = { showConfirmEmergencyStop = false }
        )
    }

    val scope = object : ControlsBottomBarScope {
        override var customAction by actionOverride
    }

    val state = object : ControlsBottomBarState {
        override val currentMessage by currentMessage
        override val swipeConfirmations by swipeConfirmations
        override val emergencyStopShown by emergencyStop
        override val customAction by actionOverride

        override fun startPrinting() {
            UriLibrary.getFileManagerUri().open()
        }

        override fun togglePausePrint() {
            if (this.swipeConfirmations) {
                // Already confirmed by swipe, direct execute
                vm.togglePausePrint()
            } else {
                // Confirm by dialog
                val isResume = this.currentMessage?.state?.flags?.paused ?: return
                if (isResume) {
                    showConfirmResume = true
                } else {
                    showConfirmPause = true
                }
            }
        }

        override fun cancelPrint() {
            if (this.swipeConfirmations) {
                // Already confirmed by swipe, direct execute
                vm.cancelPrint()
            } else {
                // Confirm by dialog
                showConfirmCancel = true
            }
        }

        override fun emergencyStop() {
            if (this.swipeConfirmations) {
                // Already confirmed by swipe, direct execute
                vm.emergencyStop()
            } else {
                // Confirm by dialog
                showConfirmEmergencyStop = true
            }
        }

        override fun showMainMenu() {
            showMainMenu = true
        }

        override fun showTemperaturePresets() {
            showTemperaturePresets = true
        }
    }

    return state to scope
}

interface ControlsBottomBarState {
    val currentMessage: Message.Current?
    val swipeConfirmations: Boolean
    val emergencyStopShown: Boolean
    val customAction: ControlsBottomBarAction?

    fun startPrinting()
    fun togglePausePrint()
    fun cancelPrint()
    fun emergencyStop()
    fun showMainMenu()
    fun showTemperaturePresets()
}

interface ControlsBottomBarScope {
    var customAction: ControlsBottomBarAction?
}

data class ControlsBottomBarAction(
    val text: String,
    val onClick: suspend () -> Unit,
    val buttonType: OctoButtonType,
)

@Composable
private fun SwipeConfirmationScope.ControlsBottomBar(state: ControlsBottomBarState) {
    CompositionLocalProvider(
        LocalOriginalLayoutDirection provides LocalLayoutDirection.current,
        LocalSwipeConfirmation provides this,
    ) {
        val flags = { state.currentMessage?.state?.flags }
        val current = { state.currentMessage }

        LeftyModeRow(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .background(OctoAppTheme.colors.windowBackground)
                .padding(start = OctoAppTheme.dimens.margin1)
                .octoAppInsetsPadding(bottom = true)
                .height(56.dp)
        ) {
            CustomAction(flags = flags, action = state.customAction)
            EmergencyStop(flags = flags, useSwipeConfirmation = state.swipeConfirmations, onEmergencyStop = state::emergencyStop, visible = state.emergencyStopShown)
            StartPrint(flags = flags, onStartPrinting = state::startPrinting, iconOnly = state.emergencyStopShown)
            ShowTemperaturePresets(flags = flags, onShowTemperaturePresets = state::showTemperaturePresets)
            StopPrinting(flags = flags, onStopPrinting = state::cancelPrint, useSwipeConfirmation = state.swipeConfirmations)
            StopPrintingDivider(flags = flags)
            PausePrinting(flags = flags, onPausePrinting = state::togglePausePrint, useSwipeConfirmation = state.swipeConfirmations)
            ResumePrinting(flags = flags, onResumePrinting = state::togglePausePrint, useSwipeConfirmation = state.swipeConfirmations)
            StateText(current = current)
            MenuButton(onClick = state::showMainMenu)
        }
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun RowScope.CustomAction(
    flags: () -> PrinterState.Flags?,
    action: ControlsBottomBarAction?,
) = AnimatedVisibility(visible = flags()?.isOperational() == false) {
    AnimatedContent(
        targetState = action,
    ) { target ->
        target ?: return@AnimatedContent
        ButtonAction(
            testTag = TestTags.BottomBar.ConnectAction,
            action = target
        )
    }
}

@Composable
private fun RowScope.StartPrint(
    flags: () -> PrinterState.Flags?,
    onStartPrinting: () -> Unit,
    iconOnly: Boolean,
) = AnimatedVisibility(visible = flags()?.isPrinting() == false && flags()?.isOperational() == true) {
    if (iconOnly) {
        BottomBarAction(
            icon = R.drawable.ic_round_folder_24,
            description = R.string.start_printing,
            onClick = onStartPrinting,
            onSwipe = null,
            testTag = TestTags.BottomBar.Start,
        )
    } else {
        ButtonAction(
            testTag = TestTags.BottomBar.Start,
            action = ControlsBottomBarAction(
                text = stringResource(id = R.string.start_printing),
                onClick = onStartPrinting,
                buttonType = OctoButtonType.Primary
            ),
        )
    }
}

@Composable
private fun ButtonAction(
    action: ControlsBottomBarAction,
    testTag: String,
) = OctoButton(
    small = true,
    text = action.text,
    onClick = action.onClick,
    type = action.buttonType,
    modifier = Modifier
        .padding(end = OctoAppTheme.dimens.margin1)
        .testTag(testTag),
)

@Composable
private fun RowScope.ShowTemperaturePresets(
    flags: () -> PrinterState.Flags?,
    onShowTemperaturePresets: () -> Unit,
) = AnimatedVisibility(visible = flags()?.isPrinting() == false && flags()?.isOperational() == true) {
    BottomBarAction(
        icon = R.drawable.ic_round_local_fire_department_24,
        onClick = onShowTemperaturePresets,
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
        description = R.string.temperature_menu___title,
        testTag = TestTags.BottomBar.Temperatures
    )
}


@Composable
private fun RowScope.StopPrinting(
    flags: () -> PrinterState.Flags?,
    useSwipeConfirmation: Boolean,
    onStopPrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !cancelling } ?: false) {
    BottomBarAction(
        icon = R.drawable.ic_round_stop_24,
        onSwipe = onStopPrinting.takeIf { useSwipeConfirmation },
        onClick = onStopPrinting.takeUnless { useSwipeConfirmation },
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin01),
        description = R.string.cancel_print,
        testTag = TestTags.BottomBar.Cancel
    )
}

@Composable
private fun RowScope.EmergencyStop(
    flags: () -> PrinterState.Flags?,
    useSwipeConfirmation: Boolean,
    visible: Boolean,
    onEmergencyStop: () -> Unit,
) = AnimatedVisibility(visible = visible && (flags()?.isOperational() ?: false)) {
    BottomBarAction(
        icon = R.drawable.ic_round_report_24px,
        onSwipe = onEmergencyStop.takeIf { useSwipeConfirmation },
        onClick = onEmergencyStop.takeUnless { useSwipeConfirmation },
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin01),
        description = R.string.emergency_stop,
        testTag = TestTags.BottomBar.EmergencyStop,
        color = OctoAppTheme.colors.red
    )
}

@Composable
private fun RowScope.StopPrintingDivider(
    flags: () -> PrinterState.Flags?,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !cancelling && !resuming && !pausing } ?: false) {
    Box(
        modifier = Modifier
            .padding(end = OctoAppTheme.dimens.margin01)
            .width(1.dp)
            .height(30.dp)
            .zIndex(100f)
            .background(OctoAppTheme.colors.inputBackground)
    )
}

@Composable
private fun RowScope.PausePrinting(
    flags: () -> PrinterState.Flags?,
    useSwipeConfirmation: Boolean,
    onPausePrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !paused && !pausing && !cancelling && !resuming } ?: false) {
    BottomBarAction(
        icon = R.drawable.ic_round_pause_24,
        onSwipe = onPausePrinting.takeIf { useSwipeConfirmation },
        onClick = onPausePrinting.takeUnless { useSwipeConfirmation },
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
        description = R.string.pause,
        testTag = TestTags.BottomBar.Pause
    )
}

@Composable
private fun RowScope.ResumePrinting(
    flags: () -> PrinterState.Flags?,
    useSwipeConfirmation: Boolean,
    onResumePrinting: () -> Unit,
) = AnimatedVisibility(visible = flags()?.run { isPrinting() && !cancelling && !resuming && paused } ?: false) {
    BottomBarAction(
        icon = R.drawable.ic_round_play_arrow_24,
        onSwipe = onResumePrinting.takeIf { useSwipeConfirmation },
        onClick = onResumePrinting.takeUnless { useSwipeConfirmation },
        modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1),
        description = R.string.resume,
        testTag = TestTags.BottomBar.Resume
    )
}

@Composable
private fun MenuButton(onClick: () -> Unit) {
    val borderWidth = 1.dp
    val shape = RoundedCornerShape(
        topStartPercent = 50,
        bottomStartPercent = 50
    )

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .scale(scaleX = if (LocalLeftyMode.current) -1f else 1f, scaleY = 1f)
            .offset(x = borderWidth)
            .padding(vertical = OctoAppTheme.dimens.margin01)
            .border(width = borderWidth, color = OctoAppTheme.colors.secondaryButtonText, shape = shape)
            .clip(shape)
            .background(OctoAppTheme.colors.inputBackground)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = OctoAppTheme.colors.secondaryButtonText),
                onClick = onClick
            )
            .fillMaxHeight()
            .testTag(TestTags.BottomBar.MainMenu)
            .padding(start = OctoAppTheme.dimens.margin1)
            .padding(horizontal = OctoAppTheme.dimens.margin1),
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_round_menu_24),
            contentDescription = stringResource(id = R.string.cd_main_menu),
            tint = OctoAppTheme.colors.secondaryButtonText,
        )
    }
}

@Composable
private fun RowScope.StateText(
    current: () -> Message.Current?,
) = Crossfade(
    targetState = current()?.statusText ?: "",
    modifier = Modifier
        .padding(end = OctoAppTheme.dimens.margin1)
        .weight(1f),
) {
    Box(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = it,
            style = OctoAppTheme.typography.subtitle,
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .testTag(TestTags.BottomBar.Status)
        )
    }
}

@Composable
private fun BottomBarAction(
    @DrawableRes icon: Int,
    modifier: Modifier = Modifier,
    @StringRes description: Int,
    onClick: (() -> Unit)? = null,
    onSwipe: (() -> Unit)? = null,
    color: Color = OctoAppTheme.colors.accent,
    testTag: String,
) = Box(
    modifier = if (onSwipe != null) {
        modifier.swipeConfirmed(icon = icon, description = description, onSwipe = onSwipe, color = color)
    } else {
        modifier.clickConfirmed(onClick = onClick ?: {})
    }.testTag(testTag)
) {
    Icon(
        painter = painterResource(id = icon),
        contentDescription = stringResource(id = description),
        tint = color,
        modifier = Modifier.padding(OctoAppTheme.dimens.margin1)
    )
}

private fun Modifier.clickConfirmed(onClick: () -> Unit) = composed {
    this.clickable(
        onClick = onClick,
        indication = rememberRipple(color = OctoAppTheme.colors.accent, bounded = false),
        interactionSource = remember { MutableInteractionSource() }
    )
}

private fun Modifier.swipeConfirmed(
    @DrawableRes icon: Int,
    @StringRes description: Int,
    color: Color,
    onSwipe: () -> Unit
) = composed {
    val swipeScope = LocalSwipeConfirmation.current
    var centerInWindow by remember { mutableStateOf(Offset.Zero) }
    val usedLayoutDirection = if (LocalLeftyMode.current) LayoutDirection.Rtl else LocalLayoutDirection.current
    val windowWidth = with(LocalDensity.current) { LocalConfiguration.current.screenWidthDp.dp.toPx() }

    onGloballyPositioned {
        // We assume that parent.x=0 and parent.y=windowWidth. If we use positionInRoot(), Ltr Rtl is not honored
        val centerOffset = Offset(x = it.size.width / 2f, y = it.size.height / 2f)
        val piw = it.positionInRoot()
        centerInWindow = if (usedLayoutDirection == LayoutDirection.Ltr) {
            piw + centerOffset
        } else {
            Offset(x = windowWidth - piw.x, y = piw.y) - centerOffset
        }
    }.pointerInput(onSwipe) {
        awaitEachGesture {
            while (true) {
                val pointerId = awaitFirstDown().id.also {
                    swipeScope.confirm(
                        centerInWindow = centerInWindow,
                        icon = icon,
                        then = onSwipe,
                        description = description,
                        layoutDirection = usedLayoutDirection,
                        color = color
                    )
                }

                swipeScope.dragEventManager.onDragStart()
                horizontalDrag(pointerId) { change -> swipeScope.dragEventManager.onDrag(change) }
                swipeScope.dragEventManager.onDragEnd()
            }
        }
    }
}
//endregion

//region Preview
@Composable
private fun BottomBarPreview(
    current: Message.Current?,
    leftyMode: Boolean,
    emergencyStop: Boolean = false
) = OctoAppThemeForPreview(leftyMode) {
    val state = object : ControlsBottomBarState {
        override val currentMessage = current
        override val swipeConfirmations = true
        override var emergencyStopShown = emergencyStop
        override val customAction = null
        override fun startPrinting() = Unit
        override fun togglePausePrint() = Unit
        override fun cancelPrint() = Unit
        override fun emergencyStop() = Unit
        override fun showMainMenu() = Unit
        override fun showTemperaturePresets() = Unit
    }

    ControlsBottomBarSwipeConfirmation {
        Column {
            Spacer(modifier = Modifier.weight(1f))
            ControlsBottomBar(state = state)
        }
    }
}

@Composable
private fun BottomBarPreviewIdle(leftyMode: Boolean, emergencyStop: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    emergencyStop = emergencyStop,
    current = Message.Current(
        serverTime = Instant.DISTANT_PAST,
    )
)

@Composable
private fun BottomBarPreviewPrinting(leftyMode: Boolean, emergencyStop: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    emergencyStop = emergencyStop,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = Instant.DISTANT_PAST,
    )
)

@Composable
private fun BottomBarPreviewPausing(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, pausing = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = Instant.DISTANT_PAST,
    )
)

@Composable
private fun BottomBarPreviewPaused(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, paused = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = Instant.DISTANT_PAST,
    )
)

@Composable
private fun BottomBarPreviewCancelling(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, cancelling = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = Instant.DISTANT_PAST,
    )
)

@Composable
private fun BottomBarPreviewResuming(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = Message.Current(
        state = PrinterState.State(flags = PrinterState.Flags(printing = true, resuming = true)),
        progress = ProgressInformation(completion = 42.1f),
        serverTime = Instant.DISTANT_PAST,
    )
)

@Composable
private fun BottomBarInitial(leftyMode: Boolean) = BottomBarPreview(
    leftyMode = leftyMode,
    current = null
)

@Preview
@Composable
private fun BottomBarPreviewIdleLeft() = BottomBarPreviewIdle(leftyMode = true, emergencyStop = false)

@Preview
@Composable
private fun BottomBarPreviewIdleRight() = BottomBarPreviewIdle(leftyMode = false, emergencyStop = false)

@Preview
@Composable
private fun BottomBarPreviewIdleRightEmergency() = BottomBarPreviewIdle(leftyMode = false, emergencyStop = true)

@Preview
@Composable
private fun BottomBarPreviewPrintingLeft() = BottomBarPreviewPrinting(leftyMode = true, emergencyStop = false)

@Preview
@Composable
private fun BottomBarPreviewPrintingRight() = BottomBarPreviewPrinting(leftyMode = false, emergencyStop = false)

@Preview
@Composable
private fun BottomBarPreviewPrintingRightEmergency() = BottomBarPreviewPrinting(leftyMode = false, emergencyStop = true)

@Preview
@Composable
private fun BottomBarPreviewPausingLeft() = BottomBarPreviewPausing(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewPausingRight() = BottomBarPreviewPausing(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewPausedLeft() = BottomBarPreviewPaused(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewPausedRight() = BottomBarPreviewPaused(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewCancellingLeft() = BottomBarPreviewCancelling(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewCancellingRight() = BottomBarPreviewCancelling(leftyMode = false)

@Preview
@Composable
private fun BottomBarPreviewResumingLeft() = BottomBarPreviewResuming(leftyMode = true)

@Preview
@Composable
private fun BottomBarPreviewResumingRight() = BottomBarPreviewResuming(leftyMode = false)

@Preview
@Composable
private fun BottomBarInitialLeft() = BottomBarInitial(leftyMode = true)

@Preview
@Composable
private fun BottomBarInitialRight() = BottomBarInitial(leftyMode = false)
//endregion
