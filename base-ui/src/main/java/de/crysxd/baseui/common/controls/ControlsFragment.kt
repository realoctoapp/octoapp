package de.crysxd.baseui.common.controls

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.framework.locals.LocalOctoAppInsets
import de.crysxd.octoapp.base.di.BaseInjector
import io.github.aakira.napier.Napier

class ControlsFragment : Fragment(), InsetAwareScreen {

    private val tag = "ControlsFragment"
    private val isKeepScreenOn get() = BaseInjector.get().octoPreferences().isKeepScreenOnDuringPrint
    private var doStartEdit: () -> Unit = {}
    private var applyInsets: (Rect) -> Unit = {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = ComposeContent(instanceId = null) {
        val viewState = rememberControlsState()
        val screenState = rememberControlsScreenState()
        var insets by remember { mutableStateOf(Rect()) }

        LaunchedEffect(viewState) {
            doStartEdit = { viewState.editing = true }
            applyInsets = { insets = it }
        }

        CompositionLocalProvider(LocalOctoAppInsets provides insets) {
            ControlsScreen(state = screenState) {
                ControlsView(state = viewState)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        BaseInjector.get().octoPreferences().updatedFlow.asLiveData().observe(viewLifecycleOwner) {
            updateKeepScreenOn()
        }
    }

    private fun updateKeepScreenOn() {
        if (isKeepScreenOn) {
            Napier.i(tag = tag, message = "Keeping screen on")
            requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            Napier.i(tag = tag, message = "Not keeping screen on")
            requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }

    override fun onStart() {
        super.onStart()
        updateKeepScreenOn()
    }

    override fun onStop() {
        super.onStop()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun startEdit() {
        doStartEdit()
    }

    override fun handleInsets(insets: Rect) {
        applyInsets(insets)
    }
}