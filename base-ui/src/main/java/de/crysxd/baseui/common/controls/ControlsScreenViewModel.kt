package de.crysxd.baseui.common.controls

import androidx.lifecycle.ViewModel
import de.crysxd.octoapp.viewmodels.OrchestratorViewModelCore

class ControlsScreenViewModel : ViewModel() {

    private val core = OrchestratorViewModelCore(triggerCapabilityUpdates = false)

    val connectionState = core.connectionState
    val c = core.uiSettings
}