package de.crysxd.baseui.common.controls

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.layout.ScrollEdge
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsPadding
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.UUID

@Composable
fun ControlsBottomBarSwipeConfirmation(
    content: @Composable SwipeConfirmationScope.() -> Unit
) = Box(
    modifier = Modifier
        .heightIn(min = 200.dp)
        .clip(RectangleShape)
) {
    val isTesting = OctoAppTheme.isUiTest
    val confirmation = remember { mutableStateOf<SwipeConfirmation?>(null) }
    var cancelJob by remember { mutableStateOf<Job?>(null) }
    val coroutine = rememberCoroutineScope()
    val dragEventManager = remember {
        DragEventManager(
            coroutineScope = coroutine,
            onConfirmedEnd = {
                confirmation.value?.then?.invoke()
                confirmation.value = null
            },
            onInteraction = {
                cancelJob?.cancel()
                cancelJob = coroutine.launch {
                    delay(if (isTesting) 5000 else 1500)
                    confirmation.value = null
                }
            }
        )
    }

    val scope = remember {
        object : SwipeConfirmationScope {
            override val dragEventManager = dragEventManager
            override fun confirm(
                centerInWindow: Offset,
                @DrawableRes icon: Int,
                @StringRes description: Int,
                layoutDirection: LayoutDirection,
                color: Color,
                then: () -> Unit
            ) {
                confirmation.value = SwipeConfirmation(
                    centerInWindow = centerInWindow,
                    icon = icon,
                    description = description,
                    then = then,
                    color = color,
                    layoutDirection = layoutDirection
                )
            }
        }
    }

    scope.content()

    SwipeTrackBox(
        confirmation = confirmation,
        dragEventManager = dragEventManager,
    )
}

private val TrackThumbWidth = 56.dp
private val MaxSlideDistance = 800.dp
private val IconSize = 24.dp

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun BoxScope.SwipeTrackBox(
    confirmation: State<SwipeConfirmation?>,
    dragEventManager: DragEventManager,
) {
    AnimatedContent(
        targetState = confirmation.value,
        modifier = Modifier
            .align(Alignment.BottomCenter)
            .fillMaxWidth(),
        transitionSpec = {
            (fadeIn() + slideInVertically { it }) togetherWith (fadeOut() + slideOutVertically { it })
        },
        contentAlignment = Alignment.Center,
    ) { target ->
        Column {
            Box {
                ScrollEdge(visible = { target != null }, alignment = Alignment.BottomCenter)
            }

            target?.let {
                SwipeTrack(
                    confirmation = it,
                    dragEventManager = dragEventManager,
                )
            }
        }
    }
}

@Composable
private fun SwipeTrack(
    confirmation: SwipeConfirmation,
    dragEventManager: DragEventManager = DragEventManager(rememberCoroutineScope()),
) = CompositionLocalProvider(LocalLayoutDirection provides confirmation.layoutDirection) {
    SwipeTrackDirected(confirmation = confirmation, dragEventManager = dragEventManager)
}

@Composable
private fun SwipeTrackDirected(
    confirmation: SwipeConfirmation,
    dragEventManager: DragEventManager,
) = with(LocalDensity.current) {
    // region States
    val padding = OctoAppTheme.dimens.margin1
    val haptic = LocalHapticFeedback.current
    val thumbWidth = TrackThumbWidth.toPx()
    val scope = rememberCoroutineScope()


    val labelAlpha by remember {
        derivedStateOf {
            (1 - dragEventManager.progress * 2).coerceIn(0f..1f)
        }
    }
    val ghostOffset = remember { Animatable(dragEventManager.min) }
    val ghostAlpha = remember { Animatable(0.5f) }
    LaunchedEffect(dragEventManager.max) {
        if (dragEventManager.max > dragEventManager.min) {
            delay(600)
            ghostAlpha.snapTo(0.2f)
            ghostOffset.snapTo(dragEventManager.min)
            ghostOffset.animateTo(dragEventManager.max)
        }
    }
    LaunchedEffect(dragEventManager.confirmed) {
        haptic.performHapticFeedback(HapticFeedbackType.LongPress)
    }
    //endregion

    Box(
        modifier = Modifier
            .background(OctoAppTheme.colors.windowBackground)
            .fillMaxWidth()
            .octoAppInsetsPadding(bottom = true)
            .height(150.dp)
            .testTag(TestTags.BottomBar.SwipeTrack)
            .pointerInput(dragEventManager) {
                detectDragGestures(
                    onDrag = { change, _ -> scope.launch { dragEventManager.onDrag(change) } },
                    onDragCancel = { scope.launch { dragEventManager.onDragEnd() } },
                    onDragEnd = { scope.launch { dragEventManager.onDragEnd() } },
                )
            }
            .padding(vertical = padding)
            .onGloballyPositioned {
                scope.launch {
                    dragEventManager.setRange(
                        min = (confirmation.centerInWindow.x - TrackThumbWidth.toPx() / 2).coerceAtLeast(padding.toPx()),
                        max = (it.size.width - thumbWidth).coerceAtMost(MaxSlideDistance.toPx()) - padding.toPx(),
                        layoutDirection = confirmation.layoutDirection,
                    )
                }
            },
    ) {
        val sliderWidth = (dragEventManager.max - dragEventManager.min).toDp() + TrackThumbWidth
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .padding(start = dragEventManager.min.toDp(), end = padding)
                .width(sliderWidth)
                .clip(RoundedCornerShape(OctoAppTheme.dimens.cornerRadius))
                .background(OctoAppTheme.colors.inputBackground)
        )

        Text(
            text = stringResource(id = confirmation.description),
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.lightText,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(start = dragEventManager.min.toDp(), end = padding)
                .width(sliderWidth)
                .align(Alignment.CenterStart)
                .graphicsLayer { alpha = labelAlpha }
        )

        SwipeThumb(
            icon = confirmation.icon,
            offset = { ghostOffset.value.coerceAtLeast(dragEventManager.offset).toInt() },
            alpha = { ghostAlpha.value - (dragEventManager.progress - 0.5f).coerceIn(0f..1f) },
            color = confirmation.color,
        )

        SwipeThumb(
            icon = confirmation.icon,
            offset = { dragEventManager.offset.toInt() },
            confirmed = dragEventManager.confirmed,
            color = confirmation.color,
        )
    }
}

@Composable
private fun SwipeThumb(
    @DrawableRes icon: Int,
    offset: () -> Int,
    color: Color,
    modifier: Modifier = Modifier,
    confirmed: Boolean = false,
    alpha: () -> Float = { 1f },
) {
    val padding = OctoAppTheme.dimens.margin01
    val cornerRadius = OctoAppTheme.dimens.cornerRadius
    val scaledCornerRadius = cornerRadius - padding
    val confirmColor = OctoAppTheme.colors.snackbarPositive.copy(alpha = 0.5f)
    val confirmProgress by animateFloatAsState(targetValue = if (confirmed) 1f else 0f)

    Box(
        modifier = modifier
            .offset { IntOffset(x = offset(), y = 0) }
            .width(TrackThumbWidth)
            .drawBehind {
                if (confirmProgress > 0) {
                    val pxPadding = padding.toPx() * (1 - confirmProgress)

                    drawRoundRect(
                        color = confirmColor,
                        topLeft = Offset(x = pxPadding, y = pxPadding),
                        size = Size(width = size.width - pxPadding * 2, height = size.height - pxPadding * 2),
                        cornerRadius = CornerRadius(x = cornerRadius.toPx(), y = cornerRadius.toPx())
                    )
                }
            }
            .padding(padding)
            .graphicsLayer { this.alpha = alpha() }
            .clip(RoundedCornerShape(scaledCornerRadius))
            .background(color = color)
            .fillMaxHeight()
    ) {
        Crossfade(targetState = confirmed) {
            Icon(
                painter = painterResource(id = if (it) R.drawable.ic_round_check_24 else icon),
                contentDescription = null,
                tint = OctoAppTheme.colors.white,
                modifier = Modifier
                    .padding(all = (TrackThumbWidth - IconSize - padding * 2) / 2)
            )
        }
    }
}

data class SwipeConfirmation(
    val centerInWindow: Offset,
    @DrawableRes val icon: Int,
    @StringRes val description: Int,
    val then: () -> Unit,
    val color: Color,
    val layoutDirection: LayoutDirection,
    val id: String = UUID.randomUUID().toString(),
)

interface SwipeConfirmationScope {
    val dragEventManager: DragEventManager

    fun confirm(
        centerInWindow: Offset,
        @DrawableRes icon: Int,
        @StringRes description: Int,
        layoutDirection: LayoutDirection,
        color: Color,
        then: () -> Unit,
    )
}

class DragEventManager(
    private val coroutineScope: CoroutineScope,
    private val onConfirmedEnd: () -> Unit = {},
    private val onInteraction: () -> Unit = {}
) {

    private val thumbOffset = Animatable(0f)
    private var lastPosition: Offset? = null
    private var multiplier = 1f
    var min by mutableStateOf(0f)
        private set
    var max by mutableStateOf(0f)
        private set
    val progress by derivedStateOf {
        (offset - min) / (max - min)
    }
    val confirmed by derivedStateOf {
        progress > 0.8f
    }

    val offset get() = thumbOffset.value

    fun setRange(min: Float, max: Float, layoutDirection: LayoutDirection) {
        this.min = min
        this.max = max
        multiplier = if (layoutDirection == LayoutDirection.Ltr) 1f else -1f
        if (offset !in min..max && max > min) {
            coroutineScope.launch {
                thumbOffset.snapTo(offset.coerceIn(min..max))
            }
        }
    }

    fun onDrag(change: PointerInputChange) {
        onInteraction()
        change.consume()
        coroutineScope.launch {
            lastPosition?.let {
                val pc = (change.position - it) * multiplier
                val v = (thumbOffset.value + pc.x).coerceIn(min..max)
                thumbOffset.snapTo(v)
            }
            lastPosition = change.position
        }
    }

    fun onDragEnd() {
        if (confirmed) {
            onConfirmedEnd()
        }

        coroutineScope.launch {
            thumbOffset.animateTo(min)
        }
    }

    fun onDragStart() {
        lastPosition = null
        onInteraction()
        coroutineScope.launch {
            thumbOffset.stop()
        }
    }
}

@Preview
@Composable
private fun PreviewMiddle() = OctoAppThemeForPreview {
    SwipeTrack(
        confirmation = SwipeConfirmation(
            centerInWindow = Offset(400f, 10f),
            description = R.string.cancel,
            icon = R.drawable.ic_round_stop_24,
            then = {},
            layoutDirection = LayoutDirection.Ltr,
            color = OctoAppTheme.colors.primaryButtonBackground,
        ),
    )
}

@Preview
@Composable
private fun PreviewStart() = OctoAppThemeForPreview {
    SwipeTrack(
        confirmation = SwipeConfirmation(
            centerInWindow = Offset(0f, 10f),
            description = R.string.cancel,
            icon = R.drawable.ic_round_stop_24,
            then = {},
            layoutDirection = LayoutDirection.Ltr,
            color = OctoAppTheme.colors.primaryButtonBackground,
        ),
    )
}

@Preview
@Composable
private fun PreviewEnd() = OctoAppThemeForPreview {
    SwipeTrack(
        confirmation = SwipeConfirmation(
            centerInWindow = Offset(100f, 10f),
            description = R.string.cancel,
            icon = R.drawable.ic_round_stop_24,
            then = {},
            layoutDirection = LayoutDirection.Rtl,
            color = OctoAppTheme.colors.primaryButtonBackground,
        ),
    )
}