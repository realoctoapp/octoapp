package de.crysxd.baseui.common

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.res.use
import androidx.core.view.children
import androidx.lifecycle.Lifecycle
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import de.crysxd.baseui.R
import de.crysxd.octoapp.base.utils.AnimationTestUtils
import de.crysxd.octoapp.engine.models.settings.Settings
import java.lang.Math.random

class OctoView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, style: Int = 0) : AppCompatImageView(context, attrs, style) {

    private var swimDrawable: Drawable? = null
    private var idleDrawable: Drawable? = null
    private var partyDrawable: Drawable? = null
    private var swimming = false
    private val animationHandler = Handler(Looper.getMainLooper())
    private var doAfterAnimation: () -> Boolean = { true }
    private var scheduleRunnable: Runnable? = null
    var animateVisibility = false

    private val loopCallback = object : Animatable2Compat.AnimationCallback() {
        override fun onAnimationEnd(drawable: Drawable) {
            super.onAnimationEnd(drawable)
            if (doAfterAnimation()) {
                if (queuedDrawable == null) {
                    getLoopDelay(currentDrawable)?.let {
                        animationHandler.postDelayed(startRunnable, it)
                    }
                } else {
                    val q = queuedDrawable
                    queuedDrawable = null
                    setImageDrawable(q)
                }
            }
        }
    }

    private var currentDrawable: Drawable? = null
    private var queuedDrawable: Drawable? = null

    private val startRunnable = Runnable {
        (drawable as? AnimatedVectorDrawableCompat)?.start()
        triggerBackgroundAction(currentDrawable)
    }

    init {
        loadForColor(Settings.Appearance.Colors())

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.OctoView,
            0,
            0
        ).use {
            when (it.getInt(R.styleable.OctoView_octoActivity, 0)) {
                1 -> swim()
                else -> idle()
            }
        }
    }

    fun loadForColor(colors: Settings.Appearance.Colors) {
        (swimDrawable as? AnimatedVectorDrawableCompat)?.clearAnimationCallbacks()
        (idleDrawable as? AnimatedVectorDrawableCompat)?.clearAnimationCallbacks()
        (partyDrawable as? AnimatedVectorDrawableCompat)?.clearAnimationCallbacks()

        swimDrawable = loadAnimatedDrawable(
            when (colors.themeName) {
                "blue" -> R.drawable.octo_swim_blue
                "yellow" -> R.drawable.octo_swim_yellow
                "red" -> R.drawable.octo_swim_red
                "green" -> R.drawable.octo_swim_green
                "purple" -> R.drawable.octo_swim_violet
                "orange" -> R.drawable.octo_swim_orange
                "white" -> R.drawable.octo_swim_white
                "black" -> R.drawable.octo_swim_black
                else -> R.drawable.octo_swim
            }
        )

        idleDrawable = loadAnimatedDrawable(
            when (colors.themeName) {
                "blue" -> R.drawable.octo_blink_blue
                "yellow" -> R.drawable.octo_blink_yellow
                "red" -> R.drawable.octo_blink_red
                "green" -> R.drawable.octo_blink_green
                "purple" -> R.drawable.octo_blink_violet
                "orange" -> R.drawable.octo_blink_orange
                "white" -> R.drawable.octo_blink_white
                "black" -> R.drawable.octo_blink_black
                else -> R.drawable.octo_blink
            }
        )
        partyDrawable = loadAnimatedDrawable(R.drawable.octo_party)

        (swimDrawable as? AnimatedVectorDrawableCompat)?.registerAnimationCallback(loopCallback)
        (idleDrawable as? AnimatedVectorDrawableCompat)?.registerAnimationCallback(loopCallback)
        (partyDrawable as? AnimatedVectorDrawableCompat)?.registerAnimationCallback(loopCallback)

        currentDrawable = null
        if (swimming) {
            setImageDrawable(swimDrawable)
        } else {
            setImageDrawable(idleDrawable)
        }
    }

    fun doAfterAnimation(lifecycle: Lifecycle, block: () -> Unit) {
        if (currentDrawable == idleDrawable) return block()
        if ((currentDrawable as? AnimatedVectorDrawableCompat)?.isRunning == false) return block()
        doAfterAnimation = {
            doAfterAnimation = { true }
            if (lifecycle.currentState >= Lifecycle.State.CREATED) {
                block()
            }
            false
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun loadAnimatedDrawable(@DrawableRes res: Int) = AnimatedVectorDrawableCompat.create(context, res)
        ?: resources.getDrawable(res, context.theme)

    fun scheduleAnimation(delay: Long, block: OctoView.() -> Unit) {
        scheduleRunnable?.let(animationHandler::removeCallbacks)
        scheduleRunnable = Runnable {
            block(this)
        }
        animationHandler.postDelayed(scheduleRunnable!!, delay)
    }

    fun swim() {
        setImageDrawable(swimDrawable)
    }

    fun party() {
        setImageDrawable(partyDrawable)
    }

    fun idle() {
        setImageDrawable(idleDrawable)
    }

    override fun setImageDrawable(drawable: Drawable?) {
        scheduleRunnable?.let(animationHandler::removeCallbacks)
        // Currently animating? Let animation finish and then swap
        if ((currentDrawable as? AnimatedVectorDrawableCompat)?.isRunning == true) {
            queuedDrawable = drawable
            return
        }

        super.setImageDrawable(drawable)
        animationHandler.removeCallbacks(startRunnable)
        (swimDrawable as? AnimatedVectorDrawableCompat)?.stop()
        (idleDrawable as? AnimatedVectorDrawableCompat)?.stop()
        swimming = drawable == swimDrawable
        currentDrawable = drawable

        // Only start if we did not disbale animations
        val animationsDisabled = try {
            AnimationTestUtils.animationsDisabled
        } catch (e: Exception) {
            false
        }
        if (!animationsDisabled) {
            (drawable as? AnimatedVectorDrawableCompat)?.start()
        }
    }

    private fun getLoopDelay(d: Drawable?) = when (d) {
        idleDrawable -> (2000 + 2000 * random()).toLong()
        partyDrawable -> null
        else -> 0L
    }

    private fun triggerBackgroundAction(d: Drawable?) = when (d) {
        swimDrawable -> findBackgroundView()?.triggerSwimBubbles()
        else -> Unit
    }

    private fun findBackgroundView() = (parent as? ViewGroup)?.children?.firstOrNull { it is OctoBackgroundView } as OctoBackgroundView?

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) = if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.AT_MOST) {
        super.onMeasure(
            MeasureSpec.makeMeasureSpec(
                resources.getDimension(R.dimen.octo_view_width).toInt(),
                MeasureSpec.EXACTLY
            ), MeasureSpec.makeMeasureSpec(
                resources.getDimension(R.dimen.octo_view_height).toInt(),
                MeasureSpec.EXACTLY
            )
        )
    } else {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        animationHandler.removeCallbacks(startRunnable)
        (currentDrawable as? AnimatedVectorDrawableCompat)?.stop()
        doAfterAnimation = { true }
    }

    override fun setVisibility(visibility: Int) = if (animateVisibility) {
        animate().alpha(if (visibility == View.VISIBLE) 1f else 0f).start()
    } else {
        super.setVisibility(visibility)
    }
}