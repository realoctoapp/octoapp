package de.crysxd.baseui.common.feedback

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.SendFeedbackDialogBinding
import de.crysxd.baseui.di.injectViewModel
import de.crysxd.baseui.ext.launchWhenResumedFixed
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import io.github.aakira.napier.Napier
import kotlinx.parcelize.Parcelize
import java.text.DateFormat
import java.util.Date
import java.util.TimeZone


class SendFeedbackDialog : DialogFragment() {

    private lateinit var binding: SendFeedbackDialogBinding
    private val viewModel: SendFeedbackViewModel by injectViewModel()
    private val extraFile by lazy { arguments?.getParcelableCompat<ExtraFile>(ARG_EXTRA_FILE) }

    companion object {
        private const val ARG_FOR_BUG_REPORT = "forBug"
        private const val ARG_EXTRA_FILE = "extraFile"

        fun create(isForBugReport: Boolean = false, extraFile: ExtraFile? = null) = SendFeedbackDialog().also {
            it.arguments = bundleOf(ARG_FOR_BUG_REPORT to isForBugReport, ARG_EXTRA_FILE to extraFile)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        SendFeedbackDialogBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val isForBugReport = arguments?.getBoolean(ARG_FOR_BUG_REPORT, false) == true
        if (isForBugReport) {
            binding.checkboxPhoneInformation.isChecked = true
            binding.checkboxPhoneInformation.isEnabled = false
            binding.checkboxOctoprintInformation.isChecked = true
            binding.checkboxOctoprintInformation.isEnabled = false
        }

        binding.checkboxAdditionalFile.isVisible = extraFile != null
        binding.checkboxAdditionalFile.text = extraFile?.name ?: ""

        try {
            val tz = Firebase.remoteConfig.getString("contact_timezone")
            val formattedTime = DateFormat.getTimeInstance(DateFormat.SHORT).also { it.timeZone = TimeZone.getTimeZone(tz) }.format(Date())
            binding.contactTime.text = getString(R.string.help___contact_detail_information, formattedTime, tz)
        } catch (e: java.lang.Exception) {
            Napier.e(tag = "SendFeedbackDialog", message = "Failed", throwable = e)
            binding.contactTime.isVisible = false
        }

        viewModel.viewState.observe(viewLifecycleOwner) {
            when (it) {
                SendFeedbackViewModel.ViewState.Idle -> Unit
                SendFeedbackViewModel.ViewState.Loading -> {
                    binding.buttonOpenEmail.isEnabled = false
                    binding.buttonOpenEmail.setText(R.string.loading)
                }

                SendFeedbackViewModel.ViewState.Done -> dismissAllowingStateLoss()
            }
        }

        binding.buttonOpenEmail.setOnClickListener {
            handleClick(longPress = false)
        }

        binding.buttonOpenEmail.setOnLongClickListener {
            handleClick(longPress = true)
            true
        }

        // Fix sizing of dialog
        lifecycleScope.launchWhenResumedFixed {
            dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    private fun handleClick(longPress: Boolean) {
        binding.messageInput.error = if (binding.messageInput.editText!!.text.isEmpty()) {
            getString(R.string.contact_form___enter_a_message)
        } else {
            viewModel.sendFeedback(
                context = requireContext(),
                message = binding.messageInput.editText!!.text.toString(),
                sendPhoneInfo = binding.checkboxPhoneInformation.isChecked,
                sendOctoPrintInfo = binding.checkboxOctoprintInformation.isChecked,
                sendLogs = binding.checkboxLogs.isChecked,
                shareSheet = longPress,
                extraFile = extraFile?.let {
                    CreateBugReportUseCase.BugReport.File(name = it.name, bytes = it.bytes)
                }
            )
            null
        }
    }

    @Parcelize
    class ExtraFile(val name: String, val bytes: ByteArray) : Parcelable
}