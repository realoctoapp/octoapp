package de.crysxd.baseui.common.controls

import android.graphics.Bitmap
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.indication
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.material.ripple.RippleAlpha
import androidx.compose.material.ripple.RippleTheme
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.boundsInWindow
import androidx.compose.ui.layout.onPlaced
import androidx.compose.ui.layout.positionInWindow
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoErrorDisplay
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.ScreenPreview
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsPadding
import de.crysxd.baseui.compose.theme.LocalOctoAppColors
import de.crysxd.baseui.compose.theme.OctoAppColors
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.toCompose
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.menu.controlcenter.ManageOctoPrintInstancesMenu
import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsEta
import de.crysxd.octoapp.viewmodels.ControlCenterViewModelCore.ControlCenterInstance
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

@Composable
fun ControlCenter(
    modifier: Modifier = Modifier,
    state: ControlCenterState = rememberControlCenterState()
) = if (state.enabled) {
    ContentEnabled(modifier, state)
} else {
    ContentDisabled(modifier)
}


//region State
interface ControlCenterState {
    val instances: List<ControlCenterInstance>
    val enabled: Boolean
    val activeInstanceId: String?

    fun activate(instanceId: String)
}

@Composable
fun rememberControlCenterState(): ControlCenterState {
    val vm = viewModel(ControlCenterViewModel::class)
    val state = vm.state.collectAsState(vm.stateSnapshot)

    return remember(vm, state) {
        object : ControlCenterState {
            override val instances get() = state.value.instances
            override val enabled: Boolean get() = state.value.enabled
            override val activeInstanceId get() = state.value.activeInstanceId
            override fun activate(instanceId: String) {
                vm.activate(instanceId)
            }
        }
    }
}

// endregion
//region Disabled
@Composable
private fun ContentDisabled(modifier: Modifier = Modifier) = Box(
    modifier = modifier
        .fillMaxSize()
        .testTag(TestTags.ControlCenter.ContentDisabled),
    contentAlignment = Alignment.Center
) {
    OctoErrorDisplay(
        title = stringResource(R.string.supporter_perk___title),
        description = stringResource(R.string.supporter_perk___description, stringResource(R.string.control_center___title)),
        actionLabel = stringResource(R.string.support_octoapp),
        onAction = { UriLibrary.getPurchaseUri().open() }
    )
}

//endregion
//region Enabled
@Composable
private fun ContentEnabled(
    modifier: Modifier = Modifier,
    state: ControlCenterState
) {
    val interactionSource = remember { MutableInteractionSource() }
    var rootOffset by remember { mutableStateOf(Offset.Zero) }
    var rippleColor by remember { mutableStateOf(Color.Unspecified) }
    val colorsByInstance = state.instances.associate { it.instanceId to it.color }
    val scope = rememberCoroutineScope()
    val controlCenter = LocalControlCenter.current

    Box(modifier.testTag(TestTags.ControlCenter.ContentEnabled)) {
        CompositionLocalProvider(LocalRippleTheme provides ControlCenterRippleTheme) {
            Box(
                Modifier
                    .fillMaxSize()
                    .indication(
                        indication = rememberRipple(color = rippleColor),
                        interactionSource = interactionSource,
                    )
            )
        }

        Column(
            Modifier
                .fillMaxSize()
                .onPlaced { rootOffset = it.positionInWindow() }
                .octoAppInsetsPadding(top = true, bottom = true)
        ) {
            var showMenu by remember { mutableStateOf(false) }
            if (showMenu) {
                MenuSheet(ManageOctoPrintInstancesMenu()) {
                    showMenu = false
                }
            }

            Header()
            InstanceList(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth(),
                instances = state.instances,
                activeId = state.activeInstanceId,
                onActivate = { instanceId, offset ->
                    rippleColor = colorsByInstance[instanceId] ?: Color.Unspecified
                    interactionSource.emit(PressInteraction.Press(offset - rootOffset))
                    state.activate(instanceId)
                    delay(1.seconds)
                    scope.launch {
                        delay(0.5.seconds)
                        controlCenter?.dismiss()
                    }
                }
            )

            OctoButton(
                text = stringResource(R.string.control_center___manage_menu___title),
                small = true,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(OctoAppTheme.dimens.margin2),
            ) {
                showMenu = true
            }
        }
    }
}

@Composable
private fun Header(
    modifier: Modifier = Modifier
) = Column(modifier = modifier
    .padding(OctoAppTheme.dimens.margin2)
    .clickable {
        UriLibrary
            .getFaqUri("multiprinter")
            .open()
    }
) {
    Text(
        text = stringResource(R.string.control_center___title),
        style = OctoAppTheme.typography.title,
        color = OctoAppTheme.colors.darkText,
    )

    Text(
        text = stringResource(R.string.control_center___subtitle).toHtml().asAnnotatedString(),
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.normalText,
    )
}

@Composable
@OptIn(ExperimentalFoundationApi::class)
private fun InstanceList(
    modifier: Modifier = Modifier,
    instances: List<ControlCenterInstance>,
    onActivate: suspend (String, Offset) -> Unit,
    activeId: String?
) = LazyVerticalGrid(
    modifier = modifier,
    contentPadding = PaddingValues(OctoAppTheme.dimens.margin2),
    columns = GridCells.Adaptive(400.dp),
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12, alignment = Alignment.CenterVertically),
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin3, alignment = Alignment.CenterHorizontally),
) {
    items(
        items = instances,
        key = { it.instanceId }
    ) { instance ->
        var offset by remember { mutableStateOf(Offset.Zero) }
        Instance(
            instance = instance,
            activeId = activeId,
            onActivate = { onActivate(instance.instanceId, offset) },
            modifier = Modifier
                .animateItemPlacement(spring())
                .onPlaced { offset = it.boundsInWindow().center }
        )
    }
}

@Composable
private fun Instance(
    modifier: Modifier = Modifier,
    instance: ControlCenterInstance,
    activeId: String?,
    onActivate: suspend () -> Unit,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    modifier = modifier
        .widthIn(max = 500.dp)
        .fillMaxWidth()
) {
    val scope = rememberCoroutineScope()
    var activating by remember { mutableStateOf(false) }
    fun doActivate() = scope.launch {
        activating = true
        onActivate()
    }.invokeOnCompletion {
        activating = false
    }

    val colors = object : OctoAppColors by OctoAppTheme.colors {
        override val activeColorScheme: Color
            @Composable get() = instance.color
        override val activeColorSchemeLight: Color
            @Composable get() = instance.colorLight
    }

    CompositionLocalProvider(LocalOctoAppColors provides colors) {
        InstanceCard(instance = instance, modifier = Modifier.weight(1f), onActivate = ::doActivate)
        InstanceActivator(instance = instance, activeId = activeId, activating = activating, onActivate = ::doActivate)
    }
}

@Composable
private fun InstanceCard(
    modifier: Modifier = Modifier,
    instance: ControlCenterInstance,
    onActivate: () -> Unit,
) = Row(
    modifier = modifier
        .shadow(elevation = 2.dp, MaterialTheme.shapes.large)
        .heightIn(min = 80.dp)
        .height(intrinsicSize = IntrinsicSize.Min)
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.windowBackground)
        .clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = rememberRipple(color = OctoAppTheme.colors.activeColorScheme),
            onClick = onActivate,
        )
) {
    InstanceWebcamImage(
        modifier = Modifier
            .fillMaxHeight()
            .aspectRatio(1f), image = instance.snapshot
    )

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .width(4.dp)
            .background(OctoAppTheme.colors.activeColorScheme)
    )

    InstanceInformation(
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin12)
            .fillMaxSize(),
        instance = instance
    )
}

@Composable
private fun InstanceWebcamImage(modifier: Modifier = Modifier, image: Bitmap?) = Box(
    modifier = modifier.background(OctoAppTheme.colors.black)
) {
    Crossfade(image) {
        if (it != null) {
            Image(
                bitmap = it.asImageBitmap(),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxSize(),
            )
        }
    }
}

@Composable
fun InstanceInformation(
    modifier: Modifier = Modifier,
    instance: ControlCenterInstance
) = Column(modifier = modifier) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Text(
            text = instance.label,
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.darkText,
            modifier = Modifier.weight(1f),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
        )

        AnimatedContent(instance.progress?.formatAsPercent(maxDecimals = 0)?.takeIf { it.isNotBlank() }) {
            if (it != null) {
                Text(
                    text = it,
                    style = OctoAppTheme.typography.focus,
                    color = OctoAppTheme.colors.darkText,
                    modifier = Modifier.padding(start = OctoAppTheme.dimens.margin12)
                )
            }
        }
    }

    LinearProgressIndicator(
        progress = instance.progress ?: 0f,
        color = OctoAppTheme.colors.activeColorScheme,
        strokeCap = StrokeCap.Round,
        backgroundColor = OctoAppTheme.colors.inputBackground,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = OctoAppTheme.dimens.margin01, bottom = OctoAppTheme.dimens.margin01)
    )

    Text(
        text = instance.status,
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.normalText,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
    )

    Text(
        text = instance.eta?.formatAsSecondsEta() ?: stringResource(R.string.control_center___plugin_missing).takeIf { instance.advertiseCompanion } ?: "",
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.normalText,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
    )
}

@Composable
private fun InstanceActivator(
    modifier: Modifier = Modifier,
    instance: ControlCenterInstance,
    activeId: String?,
    activating: Boolean,
    onActivate: () -> Unit,
) {
    val effectiveActive = activeId == instance.instanceId || activating
    val windowBackground = OctoAppTheme.colors.windowBackground
    val colorBackground by animateColorAsState(if (effectiveActive) OctoAppTheme.colors.activeColorSchemeLight else windowBackground)
    val color by animateColorAsState(if (effectiveActive) OctoAppTheme.colors.activeColorScheme else OctoAppTheme.colors.normalText)
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .size(32.dp)
            .shadow(elevation = 2.dp, MaterialTheme.shapes.small)
            .clip(MaterialTheme.shapes.small)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = rememberRipple(color = OctoAppTheme.colors.activeColorScheme),
                onClick = onActivate,
            )
            .drawBehind {
                drawCircle(windowBackground)
                drawCircle(colorBackground)
            }
    ) {
        Crossfade(activating to activeId) { (activating, activeId) ->
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                if (activating) {
                    CircularProgressIndicator(
                        strokeWidth = 2.dp,
                        modifier = Modifier.size(16.dp),
                        color = color
                    )
                } else {
                    Icon(
                        painter = painterResource(if (activeId == instance.instanceId) R.drawable.ic_round_check_24 else R.drawable.ic_round_swap_horiz_24),
                        contentDescription = null,
                        tint = color
                    )
                }
            }
        }
    }
}

private object ControlCenterRippleTheme : RippleTheme {

    @Composable
    override fun defaultColor(): Color = Color.Unspecified

    @Composable
    override fun rippleAlpha(): RippleAlpha = RippleAlpha(
        focusedAlpha = 0.66f,
        draggedAlpha = 0.66f,
        hoveredAlpha = 0.66f,
        pressedAlpha = 0.66f,
    )
}

private val ControlCenterInstance.color
    @Composable get() = (if (isSystemInDarkTheme()) colors.dark.main else colors.light.main).toCompose()

private val ControlCenterInstance.colorLight
    @Composable get() = (if (isSystemInDarkTheme()) colors.dark.accent else colors.light.accent).toCompose()

//endregion
//region Preview
@Composable
@ScreenPreview
private fun PreviewDisabled() = OctoAppThemeForPreview {
    ControlCenter(
        state = object : ControlCenterState {
            override val instances = emptyList<ControlCenterInstance>()
            override val enabled = false
            override val activeInstanceId: String? = null
            override fun activate(instanceId: String) = Unit
        }
    )
}

@Composable
@ScreenPreview
private fun PreviewEnabled() = OctoAppThemeForPreview {
    ControlCenter(
        modifier = Modifier.background(Color.LightGray),
        state = object : ControlCenterState {
            override val instances = listOf(
                ControlCenterInstance(
                    instanceId = "1",
                    colors = Settings.Appearance.Colors(),
                    label = "Instance 1",
                    progress = null,
                    advertiseCompanion = false,
                    eta = null,
                    snapshot = null,
                    status = "Idle"
                ),
                ControlCenterInstance(
                    instanceId = "2",
                    colors = Settings.Appearance.Colors(),
                    label = "Instance 2",
                    progress = null,
                    advertiseCompanion = true,
                    eta = null,
                    snapshot = null,
                    status = "Idle"
                ),
                ControlCenterInstance(
                    instanceId = "3",
                    colors = Settings.Appearance.Colors(),
                    label = "Instance 3 with long label",
                    progress = 0.5f,
                    advertiseCompanion = false,
                    eta = 123,
                    snapshot = null,
                    status = "Printing"
                )
            )
            override val enabled = true
            override val activeInstanceId: String? = null
            override fun activate(instanceId: String) = Unit
        }
    )
}
//endregion
