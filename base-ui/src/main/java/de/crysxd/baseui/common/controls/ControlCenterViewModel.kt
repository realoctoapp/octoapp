package de.crysxd.baseui.common.controls

import androidx.lifecycle.ViewModel
import de.crysxd.octoapp.viewmodels.ControlCenterViewModelCore

class ControlCenterViewModel : ViewModel() {

    private val core = ControlCenterViewModelCore()
    val state = core.state
    val stateSnapshot get() = core.stateSnapshot

    fun activate(instanceId: String) = core.activate(instanceId)

}