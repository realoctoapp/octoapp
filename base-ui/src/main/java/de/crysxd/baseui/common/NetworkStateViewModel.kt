package de.crysxd.baseui.common

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope

class NetworkStateViewModel(application: Application) : ViewModel() {
    val networkState = NetworkStateProvider(viewModelScope, application)
        .networkState
        .asLiveData()
}