package de.crysxd.baseui.common.controls

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.integerResource
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.helpers.thenIf
import de.crysxd.baseui.compose.framework.layout.HeaderSynchronizedLazyColumn
import de.crysxd.baseui.compose.framework.layout.HeaderSynchronizedLazyVerticalGrid
import de.crysxd.baseui.compose.framework.layout.HeaderSynchronizedScrollBox
import de.crysxd.baseui.compose.framework.layout.ReorderableItem2
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.data.models.ControlType
import org.burnoutcrew.reorderable.ItemPosition
import org.burnoutcrew.reorderable.ReorderableItem
import org.burnoutcrew.reorderable.rememberReorderableLazyGridState
import org.burnoutcrew.reorderable.rememberReorderableLazyListState
import org.burnoutcrew.reorderable.reorderable

@Composable
fun ControlsLayout(
    editing: Boolean,
    headerView: @Composable () -> Unit,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    items: List<Pair<ControlType, Boolean>>,
    toggleHidden: (ControlType) -> Unit,
    content: @Composable (ControlType, EditState) -> Unit
) {
    val columnSpan = integerResource(id = R.integer.widget_list_span_count)

    if (columnSpan == 1) {
        PhoneLayout(
            editing = editing,
            onMove = onMove,
            items = items,
            toggleHidden = toggleHidden,
            headerView = headerView,
            content = content,
        )
    } else {
        TabletLayout(
            editing = editing,
            onMove = onMove,
            items = items,
            headerView = headerView,
            toggleHidden = toggleHidden,
            content = content,
            columCount = columnSpan,
        )
    }
}

@Composable
private fun PhoneLayout(
    editing: Boolean,
    headerView: @Composable () -> Unit,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    items: List<Pair<ControlType, Boolean>>,
    toggleHidden: (ControlType) -> Unit,
    content: @Composable (ControlType, EditState) -> Unit
) {
    val state = rememberReorderableLazyListState(onMove)

    HeaderSynchronizedLazyColumn(
        state = state.listState,
        contentPadding = PaddingValues(),
        modifier = Modifier
            .thenIf(editing) { reorderable(state) }
            .fillMaxSize()
    ) {
        if (!editing) {
            item("header") { headerView() }
        }

        items(
            items = items,
            key = { it.first },
        ) { type ->
            ReorderableItem(
                reorderableState = state,
                key = type.first,
            ) { dragging ->
                val editState = EditState(
                    editing = editing,
                    dragState = state,
                    dragging = dragging,
                    hidden = type.second,
                    toggleHidden = { toggleHidden(type.first) }
                )

                content(type.first, editState)
            }
        }

        if (!editing) {
            item("paddingBottom") { Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin2)) }
        }
    }
}


@Composable
private fun TabletLayout(
    editing: Boolean,
    headerView: @Composable () -> Unit,
    columCount: Int,
    onMove: (ItemPosition, ItemPosition) -> Unit,
    items: List<Pair<ControlType, Boolean>>,
    toggleHidden: (ControlType) -> Unit,
    content: @Composable (ControlType, EditState) -> Unit
) {
    if (editing) {
        val state = rememberReorderableLazyGridState(onMove)

        HeaderSynchronizedLazyVerticalGrid(
            state = state.gridState,
            columnCount = columCount,
            contentPadding = PaddingValues(),
            modifier = Modifier
                .reorderable(state)
                .fillMaxSize(),
        ) {
            items(
                items = items,
                span = { GridItemSpan(1) },
                key = { it.first },
            ) { type ->
                ReorderableItem2(
                    reorderableState = state,
                    key = type.first,
                ) { dragging ->
                    val editState = EditState(
                        editing = true,
                        dragState = state,
                        dragging = dragging,
                        hidden = type.second,
                        toggleHidden = { toggleHidden(type.first) }
                    )

                    content(type.first, editState)
                }
            }
        }
    } else {
        val scrollState = rememberScrollState()
        val columns = List<MutableList<Pair<ControlType, Boolean>>>(columCount) { mutableListOf() }
        var activeColumn = -1
        items.forEach {
            activeColumn = (activeColumn + 1) % columns.size
            columns[activeColumn].add(it)
        }


        HeaderSynchronizedScrollBox(
            firstVisibleItemIndex = { 0 },
            firstVisibleItemScrollOffset = { scrollState.value },
            footerVisible = { (scrollState.maxValue - scrollState.value) < 20 }
        ) {
            Column(
                Modifier
                    .fillMaxSize()
                    .verticalScroll(scrollState)
                    .testTag("controls:scroller")
                    .padding(bottom = OctoAppTheme.dimens.margin2)
            ) {
                headerView()

                Row {
                    columns.forEach { columnItems ->
                        Column(Modifier.weight(1f)) {
                            columnItems.forEach { type ->
                                val editState = EditState(
                                    editing = false,
                                    dragState = rememberReorderableLazyListState(onMove = { _, _ -> }),
                                    dragging = false,
                                    hidden = type.second,
                                    toggleHidden = { toggleHidden(type.first) }
                                )

                                content(type.first, editState)
                            }
                        }
                    }
                }
            }
        }
    }
}