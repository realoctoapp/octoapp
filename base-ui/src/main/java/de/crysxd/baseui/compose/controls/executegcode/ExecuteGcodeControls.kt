package de.crysxd.baseui.compose.controls.executegcode

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControl
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControlState
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.menu.MenuBottomSheetFragment
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.menu.controls.CustomizeGcodeShortcutMenu
import de.crysxd.octoapp.menu.controls.CustomizeMacroMenu
import de.crysxd.octoapp.menu.controls.ExecuteMacroMenu
import de.crysxd.octoapp.viewmodels.ExecuteGcodeControlsViewModelCore
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Composable
fun ExecuteGcodeControls(
    editState: EditState,
    state: ExecuteGcodeControlsState,
) {
    val instance = LocalOctoPrint.current
    val hasMacros by remember { derivedStateOf { !instance.macros.isNullOrEmpty() } }

    GenericHistoryItemsControl(
        editState = editState,
        state = state,
        title = stringResource(id = R.string.send_gcode),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24).takeIf { hasMacros },
        onAction = { fm, _ ->
            MenuBottomSheetFragment.createForMenu(
                menu = CustomizeMacroMenu(instanceId = instance.id, macroId = null),
                instanceId = instance.id
            ).show(fm)
        },
    )
}

@Composable
fun rememberExecuteGcodeControlsState(): ExecuteGcodeControlsState {
    val fragmentManager = OctoAppTheme.fragmentManager
    val octoActivity = OctoAppTheme.octoActivity
    val instanceId = LocalOctoPrint.current.id
    val context = LocalContext.current
    val vmFactory = ExecuteGcodeControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = ExecuteGcodeControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    val items = vm.items.collectAsStateWhileActive(key = "execute", initial = ExecuteGcodeControlsViewModelCore.State())
    val visible = vm.visible.collectAsStateWhileActive(key = "execute2", initial = ExecuteGcodeControlsViewModelCore.Visibility(false))
    val scope = rememberCoroutineScope()

    return object : ExecuteGcodeControlsState {
        override val context = context
        override val visible get() = visible.value.visible
        override val items get() = items.value.groups.map { it.label to it.items }

        override suspend fun onOtherOptionClicked() = withContext(Dispatchers.Main) {
            UriLibrary.getTerminalUri().open()
        }

        override suspend fun onClick(item: ExecuteGcodeControlsViewModelCore.Item) {
            doExecute(item, confirmed = false)
        }

        private suspend fun doExecute(item: ExecuteGcodeControlsViewModelCore.Item, confirmed: Boolean, inputs: Map<String, String> = emptyMap()) {
            when (vm.execute(id = item.id, inputs = inputs, confirmed = confirmed)) {
                ExecuteGcodeControlsViewModelCore.Result.Failed -> throw CancellationException("Failed")
                ExecuteGcodeControlsViewModelCore.Result.Executed -> Unit
                ExecuteGcodeControlsViewModelCore.Result.NeedsConfirmation -> {
                    val confirmationFlow = MutableSharedFlow<Boolean>()

                    octoActivity?.showDialog(
                        OctoActivity.Message.DialogMessage(
                            text = { getString(R.string.widget_gcode_send___confirmation_message, item.label) },
                            positiveAction = { scope.launch { confirmationFlow.emit(true) } },
                            positiveButton = { getString(R.string.widget_gcode_send___confirmation_action) },
                            dismissAction = { scope.launch { confirmationFlow.emit(false) } },
                        )
                    )

                    if (confirmationFlow.first()) {
                        doExecute(item, confirmed = true)
                    } else {
                        throw CancellationException("User cancelled")
                    }
                }

                ExecuteGcodeControlsViewModelCore.Result.NeedsInputs -> {
                    val inputsFlow = MutableSharedFlow<Map<String, String>?>()

                    MenuBottomSheetFragment.createForMenu(
                        menu = ExecuteMacroMenu(
                            instanceId = instanceId,
                            macroId = vm.getMacroId(item.id) ?: return,
                        ),
                        onResult = { inputsFlow.emit(ExecuteMacroMenu.unwrapResult(it)) },
                        onDismiss = { inputsFlow.emit(null) },
                        instanceId = instanceId,
                    ).show(fragmentManager())

                    inputsFlow.first()?.let {
                        doExecute(item, confirmed = true, inputs = it)
                    } ?: throw CancellationException("User cancelled")
                }
            }
        }

        override suspend fun onLongClick(item: ExecuteGcodeControlsViewModelCore.Item) {
            MenuBottomSheetFragment.createForMenu(
                menu = when (val raw = item.raw) {
                    is ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem -> CustomizeGcodeShortcutMenu(gcode = raw.item.command)
                    is ExecuteGcodeControlsViewModelCore.RawItem.MacroItem -> CustomizeMacroMenu(instanceId = instanceId, macroId = raw.item.id)
                },
                instanceId = instanceId,
            ).show(fragmentManager())
        }
    }
}

interface ExecuteGcodeControlsState : GenericHistoryItemsControlState<ExecuteGcodeControlsViewModelCore.Item> {
    val context: Context
    override val otherOptionText get() = context.getString(R.string.open_terminal)
    override fun key(item: ExecuteGcodeControlsViewModelCore.Item) = item.id
    override fun label(item: ExecuteGcodeControlsViewModelCore.Item) = item.label
    override fun isPinned(item: ExecuteGcodeControlsViewModelCore.Item) = item.isFavorite
}

//region Previews
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    val context = LocalContext.current
    Box(
        modifier = Modifier
            .background(OctoAppTheme.colors.windowBackground)
            .padding(10.dp)
    ) {
        ExecuteGcodeControls(
            editState = EditState.ForPreview,
            state = object : ExecuteGcodeControlsState {
                override val visible = true
                override val context = context
                override val items
                    get() = listOf(
                        null to listOf(
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "1",
                                label = "G90",
                                command = "G90",
                                isFavorite = true,
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "G90"))
                            ),
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "2",
                                label = "G28",
                                command = "G28",
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "G28"))
                            ),
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "3",
                                label = "G29",
                                command = "G29",
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "G29"))
                            ),
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "4",
                                label = "M500",
                                command = "M500",
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "M500"))
                            ),
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "5",
                                label = "M501",
                                command = "M501",
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "M501"))
                            ),
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "6",
                                label = "M502",
                                command = "M502",
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "M502"))
                            ),
                        ),
                        "Macros with long title" to listOf(
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "1",
                                label = "G90",
                                command = "G90",
                                isFavorite = true,
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "G90"))
                            ),
                            ExecuteGcodeControlsViewModelCore.Item(
                                id = "2",
                                label = "G28",
                                command = "G28",
                                raw = ExecuteGcodeControlsViewModelCore.RawItem.HistoryItem(GcodeHistoryItem(command = "G28"))
                            ),
                        )
                    )

                override suspend fun onOtherOptionClicked() = Unit
                override suspend fun onLongClick(item: ExecuteGcodeControlsViewModelCore.Item) = Unit
                override suspend fun onClick(item: ExecuteGcodeControlsViewModelCore.Item) = Unit
            }
        )
    }
}
//endregion