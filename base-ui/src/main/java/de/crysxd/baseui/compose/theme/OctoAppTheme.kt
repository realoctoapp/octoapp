package de.crysxd.baseui.compose.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.material.ripple.LocalRippleTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.booleanResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTagsAsResourceId
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.navigation.NavController
import de.crysxd.baseui.BuildConfig
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.LocalEnterValueProvider
import de.crysxd.baseui.compose.framework.components.rememberEnterValueDialogProvider
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.map

val LocalOctoAppColors = compositionLocalOf<OctoAppColors> { object : OctoAppColors {} }
val LocalOctoAppDimensions = compositionLocalOf<OctoAppDimensions> { object : OctoAppDimensions {} }
val LocalOctoAppTypography = compositionLocalOf<OctoAppTypography> { object : OctoAppTypography {} }
val LocalNavController = compositionLocalOf<() -> NavController> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalOctoActivity = compositionLocalOf<OctoActivity?> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalFragmentManager = compositionLocalOf<() -> FragmentManager> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalCompactLayout = compositionLocalOf<Boolean> { throw IllegalAccessException("Wrap in OctoAppTheme!") }
val LocalPreviewProvider = compositionLocalOf { false }
val LocalLeftyMode = compositionLocalOf<Boolean> { throw IllegalAccessException("Wrap in OctoAppTheme!") }

object OctoAppTheme {
    private var uiTest = mutableStateOf(false)

    val colors @Composable get() = LocalOctoAppColors.current
    val dimens @Composable get() = LocalOctoAppDimensions.current
    val typography @Composable get() = LocalOctoAppTypography.current
    val octoActivity @Composable get() = LocalOctoActivity.current
    val compactLayout @Composable get() = LocalCompactLayout.current
    val isPreview @Composable get() = LocalPreviewProvider.current
    val isDarkMode @Composable get() = booleanResource(id = R.bool.night_mode)
    val isUiTest get() = uiTest.value

    fun notifyUiTestActive() {
        uiTest.value = true
    }

    @Deprecated("Do not use", ReplaceWith(""))
    val navController
        @Composable get() = LocalNavController.current

    @Deprecated("Do not use", ReplaceWith(""))
    val fragmentManager
        @Composable get() = LocalFragmentManager.current
}

@Composable
fun OctoAppThemeForPreview(leftyMode: Boolean = false, content: @Composable () -> Unit) = OctoAppTheme(
    viewModelStoreOwner = requireNotNull(LocalViewModelStoreOwner.current),
    navController = { throw IllegalAccessException("Not available in preview") },
    fragmentManager = { throw IllegalAccessException("Not available in preview") },
    octoActivity = null,
    compactLayout = remember { mutableStateOf(false) },
    leftyMode = remember(leftyMode) { mutableStateOf(leftyMode) },
) {
    require(BuildConfig.DEBUG) { "Preview requires debug build, currently ${BuildConfig.BUILD_TYPE}" }

    LaunchedEffect(Unit) {
        Napier.base(DebugAntilog())
    }

    CompositionLocalProvider(
        LocalPreviewProvider provides true,
        LocalOctoPrint provides PrinterConfigurationV3(id = "preview", webUrl = "http://localhost".toUrl(), apiKey = "api"),
    ) {
        content()
    }
}

@Composable
@OptIn(ExperimentalComposeUiApi::class)
fun OctoAppTheme(
    colors: OctoAppColors = object : OctoAppColors {},
    dimens: OctoAppDimensions = object : OctoAppDimensions {},
    typography: OctoAppTypography = object : OctoAppTypography {},
    compactLayout: State<Boolean> = remember { BaseInjector.get().octoPreferences().updatedFlow2.map { it.compactLayout } }.collectAsState(initial = false),
    leftyMode: State<Boolean> = remember { BaseInjector.get().octoPreferences().updatedFlow2.map { it.leftHandMode } }.collectAsState(initial = false),
    viewModelStoreOwner: ViewModelStoreOwner,
    octoActivity: OctoActivity?,
    navController: () -> NavController = { requireNotNull(octoActivity).navController },
    fragmentManager: () -> FragmentManager,
    content: @Composable () -> Unit,
) = MaterialTheme(
    colors = Colors(
        primary = colors.accent,
        primaryVariant = colors.primaryDark,
        secondary = colors.primary,
        secondaryVariant = colors.accent,
        background = colors.windowBackground,
        error = colors.colorError,
        onBackground = colors.normalText,
        onError = colors.textColoredBackground,
        onPrimary = colors.textColoredBackground,
        onSecondary = colors.textColoredBackground,
        onSurface = colors.normalText,
        surface = colors.inputBackground,
        isLight = !isSystemInDarkTheme()
    ),
    shapes = Shapes(
        small = RoundedCornerShape(percent = 50),
        medium = RoundedCornerShape(dimens.cornerRadiusSmall),
        large = RoundedCornerShape(dimens.cornerRadius),
    )
) {
    CompositionLocalProvider(
        LocalOctoAppTypography provides typography,
        LocalOctoAppDimensions provides dimens,
        LocalOctoAppColors provides colors,
        LocalRippleTheme provides OctoAppRippleTheme(colors),
        LocalViewModelStoreOwner provides viewModelStoreOwner,
        LocalFragmentManager provides fragmentManager,
        LocalNavController provides navController,
        LocalCompactLayout provides compactLayout.value,
        LocalOctoActivity provides octoActivity,
        LocalLeftyMode provides leftyMode.value
    ) {
        // Needs outer providers
        CompositionLocalProvider(
            LocalEnterValueProvider provides rememberEnterValueDialogProvider(),
        ) {
            Box(
                modifier = Modifier.semantics {
                    testTagsAsResourceId = true
                }
            ) {
                content()
            }
        }
    }
}