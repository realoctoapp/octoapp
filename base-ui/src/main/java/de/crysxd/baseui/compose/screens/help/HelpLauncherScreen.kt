package de.crysxd.baseui.compose.screens.help

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.common.feedback.SendFeedbackDialog
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.menu.SimpleMenuItem
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerFluidd
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerMainsail
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerMixed
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.OctoPrint
import de.crysxd.octoapp.menu.MenuItemStyle
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

private const val TAG = "HelpLauncherView"

@Composable
fun HelpLauncherView(
    viewModel: HelpLauncherViewModel = viewModel(HelpLauncherViewModel::class.java)
) = CollapsibleHeaderScreenScaffold(
    header = {
        Title(text = stringResource(R.string.help___title), big = true)
    }
) {
    CompositionLocalProvider(LocalHelpLauncherViewModel provides viewModel) {
        Column(
            modifier = Modifier
                .systemBarsPadding()
                .nestedScroll(LocalNestedScrollConnection.current)
                .verticalScroll(rememberScrollState())
                .padding(OctoAppTheme.dimens.margin2)
        ) {
            Introduction()
            KnownBugs()
            Faq()
            Contact()
        }
    }
}

@Composable
private fun Introduction() = Row {
    IntroductionAvatar()

    Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
        IntroductionBubble1()
        IntroductionBubble2()
        IntroductionTutorials()
    }
}

@Composable
private fun KnownBugs() = LocalHelpLauncherViewModel.current.knownBugs.takeIf { it.isNotEmpty() }?.let { knownBugs ->
    Title(stringResource(id = R.string.help___bugs_title))
    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
    ) {
        knownBugs.forEach { bug ->
            SimpleMenuItem(
                title = bug.title ?: "???",
                icon = null,
                style = MenuItemStyle.Support,
                onClick = {
                    UriLibrary.getFaqUri(bug.id ?: "unknown").open()
                }
            )
        }
    }
}

@Composable
private fun Faq() = LocalHelpLauncherViewModel.current.faq.takeIf { it.isNotEmpty() }?.let { faqs ->
    Title(stringResource(id = R.string.help___faq_title))
    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
    ) {
        faqs.filter { faq ->
            faq.hidden != true
        }.forEach { faq ->
            SimpleMenuItem(
                title = faq.title ?: "???",
                icon = null,
                style = MenuItemStyle.Settings,
                onClick = {
                    UriLibrary.getFaqUri(faq.id ?: "unknown").open()
                }
            )
        }
    }
}

@Composable
private fun Contact() {
    val activity = OctoAppTheme.octoActivity

    // We can't use LocalOctoPrint.current because we might not have an OctoPrint active
    val interfaceType by remember(Unit) {
        try {
            SharedBaseInjector.get().printerConfigRepository.instanceInformationFlow().map { it?.systemInfo?.interfaceType }.distinctUntilChanged()
        } catch (e: IllegalStateException) {
            // Preview
            flowOf(null)
        }
    }.collectAsState(initial = null)

    fun showContact(isForBugReport: Boolean) {
        // Check if there is an update
        val updated = try {
            val updater = activity?.supportFragmentManager?.findFragmentById(R.id.update_checked) as? AppUpdater
            updater?.triggerUpdateIfAvailable() == true
        } catch (e: Exception) {
            Napier.e(tag = TAG, message = "Failed", throwable = e)
            false
        }

        // No updates? Open bug report
        if (!updated) activity?.supportFragmentManager?.let {
            SendFeedbackDialog.create(isForBugReport = isForBugReport).show(it, "bug-report")
        }
    }

    Title(stringResource(id = if (interfaceType == OctoPrint) R.string.help___octoprint_help_title else R.string.help___klipper_help_title))
    Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
        SimpleMenuItem(
            title = stringResource(id = R.string.help___3dprinting_discord),
            icon = null,
            style = MenuItemStyle.OctoPrint,
            onClick = { Uri.parse("https://discord.com/invite/3dprinters").open() }
        )

        SimpleMenuItem(
            title = stringResource(id = R.string.help___3dprinting_reddit),
            icon = null,
            style = MenuItemStyle.OctoPrint,
            onClick = { Uri.parse("https://www.reddit.com/r/3Dprinting/").open() }
        )

        SimpleMenuItem(
            title = stringResource(id = R.string.help___3dprinting_printables),
            icon = null,
            style = MenuItemStyle.OctoPrint,
            onClick = { Uri.parse("https://www.printables.com/group/discover").open() }
        )

        SimpleMenuItem(
            title = stringResource(id = R.string.help___3dprinting_thingiverse),
            icon = null,
            style = MenuItemStyle.OctoPrint,
            onClick = { Uri.parse("https://www.thingiverse.com/groups/3d-printing-forum").open() }
        )

        if (interfaceType in listOf(null, SystemInfo.Interface.OctoPrint)) {
            SimpleMenuItem(
                title = stringResource(id = R.string.help___octoprint_community),
                icon = null,
                style = MenuItemStyle.OctoPrint,
                onClick = { Uri.parse("https://community.octoprint.org/").open() }
            )

            SimpleMenuItem(
                title = stringResource(id = R.string.help___octoprint_community),
                icon = null,
                style = MenuItemStyle.OctoPrint,
                onClick = { Uri.parse("https://octoprint.org/help/?utm_source=octoapp").open() },
                description = stringResource(R.string.help___octoprint_klipper_help_subtitle),
            )
        }

//        if (interfaceType in listOf(null, SystemInfo.Interface.OctoPrint)) {
//            SimpleMenuItem(
//                title = stringResource(id = R.string.help___octoprint_discord),
//                icon = null,
//                style = MenuItemStyle.OctoPrint,
//                onClick = { Uri.parse("https://discord.octoprint.org").open() },
//                description = stringResource(R.string.help___octoprint_klipper_help_subtitle),
//            )
//        }

        if (interfaceType in listOf(null, MoonrakerFluidd, MoonrakerMainsail, MoonrakerMixed)) {
            SimpleMenuItem(
                title = stringResource(id = R.string.help___octoprint_discord).replace("OctoPrint", "Klipper"),
                icon = null,
                style = MenuItemStyle.OctoPrint,
                onClick = { Uri.parse("https://discord.klipper3d.org/").open() },
                description = stringResource(R.string.help___octoprint_klipper_help_subtitle),
            )
        }
    }


    Title(stringResource(id = R.string.help___app_help_title))
    Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
        SimpleMenuItem(
            title = stringResource(id = R.string.help___report_a_bug),
            icon = null,
            style = MenuItemStyle.OctoPrint,
            onClick = { showContact(isForBugReport = true) }
        )
        SimpleMenuItem(
            title = stringResource(id = R.string.help___ask_a_question),
            icon = null,
            style = MenuItemStyle.OctoPrint,
            onClick = { showContact(isForBugReport = false) }
        )
    }
}


@Composable
private fun Title(text: String, big: Boolean = false) = Text(
    text = text,
    style = if (big) OctoAppTheme.typography.titleBig else OctoAppTheme.typography.title,
    color = OctoAppTheme.colors.darkText,
    textAlign = TextAlign.Center,
    modifier = Modifier
        .fillMaxWidth()
        .padding(top = if (big) OctoAppTheme.dimens.margin4 else OctoAppTheme.dimens.margin3)
        .padding(bottom = if (big) OctoAppTheme.dimens.margin3 else OctoAppTheme.dimens.margin2)
)

@Composable
private fun IntroductionAvatar() = Image(
    painter = painterResource(R.drawable.help_avatar),
    contentDescription = null,
    modifier = Modifier
        .size(72.dp)
        .padding(end = OctoAppTheme.dimens.margin2)
)

@Composable
private fun IntroductionBubble1() = Text(
    text = stringResource(R.string.help___introduction_part_1),
    style = OctoAppTheme.typography.base,
    color = OctoAppTheme.colors.normalText,
    modifier = Modifier
        .bubble()
        .padding(OctoAppTheme.dimens.margin12)
)

@Composable
private fun IntroductionBubble2() = Column(
    modifier = Modifier.bubble()
) {
    Text(
        text = stringResource(R.string.help___introduction_part_2),
        style = OctoAppTheme.typography.base,
        color = OctoAppTheme.colors.normalText,
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin12)
    )

    val introUrl = LocalHelpLauncherViewModel.current.introUrl
    Box(
        modifier = Modifier
            .padding(horizontal = OctoAppTheme.dimens.margin01)
            .padding(bottom = OctoAppTheme.dimens.margin01)
            .clip(MaterialTheme.shapes.large)
            .clickable { introUrl.open() }
    ) {
        Image(
            painter = painterResource(id = R.drawable.introduction_video_placeholder),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth(),
            contentScale = ContentScale.FillWidth
        )

        Text(
            text = stringResource(R.string.help___introduction_video_caption),
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.textColoredBackground,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = OctoAppTheme.dimens.margin01)
        )
    }
}

@Composable
private fun IntroductionTutorials() {
    SimpleMenuItem(
        title = stringResource(id = R.string.main_menu___explore_support_plugins_short),
        icon = painterResource(id = R.drawable.ic_round_extension_24),
        style = MenuItemStyle.Neutral,
        onClick = { UriLibrary.getPluginLibraryUri().open() }
    )
    SimpleMenuItem(
        title = stringResource(id = R.string.main_menu___item_show_tutorials),
        icon = painterResource(id = R.drawable.ic_round_school_24),
        style = MenuItemStyle.Neutral,
        onClick = { UriLibrary.getTutorialsUri().open() }
    )
}

private fun Modifier.bubble() = composed {
    this
        .fillMaxWidth()
        .background(OctoAppTheme.colors.menuStyleSupportBackground, MaterialTheme.shapes.large)
}

private val LocalHelpLauncherViewModel = compositionLocalOf<HelpLauncherViewModel> { throw IllegalStateException("Not initialized") }

@Composable
@Preview(showSystemUi = true)
private fun PreviewLauncher() = OctoAppThemeForPreview {
    HelpLauncherView(
        viewModel = HelpLauncherViewModel()
    )
}