package de.crysxd.baseui.compose.framework.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
fun OctoDataButton(
    icon: Painter,
    modifier: Modifier = Modifier,
    contentDescription: String?,
    onContentClicked: (() -> Unit)? = null,
    onClick: (suspend () -> Unit)?,
    content: @Composable () -> Unit,
) = Row(
    modifier = modifier
        .height(IntrinsicSize.Min)
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.small)
        .background(OctoAppTheme.colors.inputBackground.copy(alpha = 0.66f))
) {
    //region Main button
    Box(
        modifier = Modifier
            .weight(1f)
            .fillMaxHeight()
            .clip(MaterialTheme.shapes.small)
            .background(OctoAppTheme.colors.inputBackground)
            .clickable(onClick = onContentClicked ?: {}, enabled = onContentClicked != null)
            .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = OctoAppTheme.dimens.margin01),
        contentAlignment = Alignment.CenterStart,
    ) {
        content()
    }
    //endregion
    //region Print button
    onClick?.let {
        OctoIconButton(
            painter = icon,
            contentDescription = contentDescription,
            onClick = onClick,
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(1f)
                .clip(MaterialTheme.shapes.small)
        )
    }
    //endregion
}