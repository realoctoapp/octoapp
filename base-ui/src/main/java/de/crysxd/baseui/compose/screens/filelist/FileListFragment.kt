package de.crysxd.baseui.compose.screens.filelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.animation.AnimatedContentTransitionScope.SlideDirection.Companion.End
import androidx.compose.animation.AnimatedContentTransitionScope.SlideDirection.Companion.Start
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.framework.helpers.rememberPrinterConfig
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.screens.ComposeScreenFragment
import de.crysxd.baseui.compose.theme.LocalNavController
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.engine.models.files.FileObject

class FileListFragment : ComposeScreenFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = null, insets = composeInsets) {
        val initialConfig = LocalOctoPrint.current
        val navController = rememberNavController()
        val copyScope = rememberFileActionsController()
        val initial = FileListFragmentArgs.fromBundle(requireArguments()).file
        val path = UriLibrary.secureEncode(initial?.path ?: "/")
        val isFolder = initial?.let { it is FileObject.Folder } ?: true

        CompositionLocalProvider(
            LocalFileActionsController provides copyScope,
            LocalNavController provides { navController },
        ) {
            Box {
                NavHost(
                    navController = navController,
                    startDestination = UriLibrary.getFileManagerUri(
                        instanceId = initialConfig.id,
                        path = path,
                        label = null,
                        folder = isFolder,
                    ).encodedPathAndQuery,
                    enterTransition = { slideIntoContainer(towards = Start) },
                    exitTransition = { slideOutOfContainer(towards = Start) },
                    popEnterTransition = { slideIntoContainer(towards = End) },
                    popExitTransition = { slideOutOfContainer(towards = End) },
                ) {
                    fileDestination(
                        initialInstanceId = initialConfig.id,
                        defaultIsFolder = isFolder,
                        defaultPath = path,
                    )

                    searchDestination(
                        initialInstanceId = initialConfig.id,
                    )
                }

                FileActionsBar(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.BottomCenter),
                )
            }
        }
    }

    private fun NavGraphBuilder.fileDestination(
        initialInstanceId: String,
        defaultPath: String,
        defaultIsFolder: Boolean,
    ) = composable(
        route = "/files?instanceId={instanceId}&path={path}&folder={folder}",
        arguments = listOf(
            navArgument("instanceId") {
                type = NavType.StringType
                defaultValue = initialInstanceId
            },
            navArgument("path") {
                type = NavType.StringType
                defaultValue = defaultPath
            },
            navArgument("label") {
                type = NavType.StringType
                defaultValue = null
                nullable = true
            },
            navArgument("folder") {
                type = NavType.BoolType
                defaultValue = defaultIsFolder
            },
        ),
    ) { entry ->
        val instanceId = requireNotNull(entry.arguments?.getString("instanceId")) { "No instance Id!" }
        val path = entry.arguments?.getString("path")?.let { UriLibrary.secureDecode(it) }?.takeUnless { it.isBlank() } ?: "/"
        val display = entry.arguments?.getString("label")?.let { UriLibrary.secureDecode(it) }?.takeUnless { it.isBlank() }
        val isFolder = entry.arguments?.getBoolean("folder") ?: true
        val config by rememberPrinterConfig(instanceId)

        CompositionLocalProvider(
            LocalOctoPrint provides config,
        ) {
            FileListScreen(
                controller = rememberFileListController(
                    path = path,
                    isFolder = isFolder,
                    display = display,
                )
            )
        }
    }

    private fun NavGraphBuilder.searchDestination(
        initialInstanceId: String,
    ) = composable(
        route = "files/{instanceId}/search",
        arguments = listOf(
            navArgument("instanceId") {
                type = NavType.StringType
                defaultValue = initialInstanceId
            },
        ),
    ) { entry ->
        val instanceId = requireNotNull(entry.arguments?.getString("instanceId")) { "No instance Id!" }
        val config by rememberPrinterConfig(instanceId)

        CompositionLocalProvider(
            LocalOctoPrint provides config,
        ) {
            FileSearchScreen()
        }
    }
}