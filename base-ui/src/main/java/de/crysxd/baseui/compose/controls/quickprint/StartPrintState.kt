package de.crysxd.baseui.compose.controls.quickprint

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.menu.main.octoprint.TimelapseMenu
import de.crysxd.octoapp.menu.main.printer.MaterialMenu
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first

@Composable
fun rememberStartPrintState(
    file: FileObject.File,
    startPrint: suspend (Boolean, Boolean) -> StartPrintHelper.Result,
): StartPrintState {
    val instanceId = LocalOctoPrint.current.id
    val activity = LocalOctoActivity.current
    val resultChanged = remember { MutableSharedFlow<Unit>() }

    //region Material confirmation
    var materialConfirmed by remember { mutableStateOf(false) }
    var showMaterialConfirmation by remember { mutableStateOf(false) }
    if (showMaterialConfirmation) {
        MenuSheet(
            menu = MaterialMenu(
                instanceId = instanceId,
                startPrintAfterSelectionForPath = file.path,
            ),
            onDismiss = {
                showMaterialConfirmation = false
                resultChanged.emit(Unit)
            },
            onResult = { result ->
                materialConfirmed = result == MaterialMenu.ResultStartPrint
            }
        )
    }
    //endregion
    //region Timelapse confirmation
    var timelapseConfirmed by remember { mutableStateOf(false) }
    var showTimelapseConfirmation by remember { mutableStateOf(false) }
    if (showTimelapseConfirmation) {
        MenuSheet(
            menu = TimelapseMenu(instanceId = instanceId, offerStartPrint = true),
            onDismiss = {
                showTimelapseConfirmation = false
                resultChanged.emit(Unit)
            },
            onResult = { result ->
                timelapseConfirmed = result == TimelapseMenu.ResultStartPrint
            }
        )
    }
    //endregion

    return object : StartPrintState {
        override suspend fun startPrint() {
            timelapseConfirmed = false
            materialConfirmed = false

            for (i in 0..2) {
                val result = startPrint(materialConfirmed, timelapseConfirmed)
                when (result) {
                    is StartPrintHelper.Result.MaterialConfirmationRequired -> {
                        showMaterialConfirmation = true

                        // Wait for selection, cancel if not confirmed
                        resultChanged.first()
                        if (!materialConfirmed) break
                    }

                    is StartPrintHelper.Result.TimelapseConfirmationRequired -> {
                        showTimelapseConfirmation = true

                        // Wait for selection, cancel if not confirmed
                        resultChanged.first()
                        if (!timelapseConfirmed) break
                    }

                    StartPrintHelper.Result.Failed -> break
                    StartPrintHelper.Result.Started -> {
                        SharedBaseInjector.get().printerConfigRepository.setActive(instanceId, trigger = "start-print")
                        activity?.enforceAllowAutomaticNavigationFromCurrentDestination()
                        break
                    }
                }
            }
        }
    }
}

interface StartPrintState {
    suspend fun startPrint()
}