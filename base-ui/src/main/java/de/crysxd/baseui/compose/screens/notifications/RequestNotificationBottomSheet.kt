package de.crysxd.baseui.compose.screens.notifications

import android.Manifest.permission.POST_NOTIFICATIONS
import android.content.DialogInterface
import android.content.pm.PackageManager.PERMISSION_DENIED
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import de.crysxd.baseui.BaseBottomSheetDialogFragment
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.ComposeContentWithoutOctoPrint
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.days

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
class RequestNotificationBottomSheet : BaseBottomSheetDialogFragment() {

    companion object {
        private const val REQUEST_STATE_INITIAL = 0
        private const val REQUEST_STATE_RATIONALE = 1
        private const val REQUEST_STATE_SUPPRESSED = 3
    }

    override val viewModel = ViewModel()
    private val preferences by lazy { BaseInjector.get().octoPreferences() }

    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        if (isGranted) {
            dismiss()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContentWithoutOctoPrint {
        RequestNotificationView(
            onRequest = { requestPermissionLauncher.launch(POST_NOTIFICATIONS) },
            onDismiss = {
                // Move to suppressed state
                preferences.notificationPermissionRequestState = REQUEST_STATE_SUPPRESSED
                dismiss()
            },
            isRationale = shouldShowRequestPermissionRationale(POST_NOTIFICATIONS) || preferences.notificationPermissionRequestState >= REQUEST_STATE_RATIONALE
        )
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        if (ContextCompat.checkSelfPermission(requireContext(), POST_NOTIFICATIONS) == PERMISSION_DENIED) {
            // Move to next state
            preferences.notificationPermissionRequestState++

            // Suppress this dialog from being shown before tomorrow or 3 months from now
            when (preferences.notificationPermissionRequestState) {
                in REQUEST_STATE_INITIAL until REQUEST_STATE_SUPPRESSED -> preferences.askNotificationPermissionNotBefore = Clock.System.now() + 1.days
                else -> preferences.askNotificationPermissionNotBefore = Clock.System.now() + 90.days
            }

            // If we are in the suppressed range, tell user how to enable via settings
            if (preferences.notificationPermissionRequestState >= REQUEST_STATE_SUPPRESSED) {
                requireOctoActivity().showDialog(
                    OctoActivity.Message.DialogMessage(
                        title = { getString(R.string.notifications_permission___title_rationale) },
                        text = { getString(R.string.notifications_permission___alternative) },
                    )
                )
            }
        } else {
            // Permission granted, reset
            preferences.notificationPermissionRequestState = REQUEST_STATE_INITIAL
        }
    }

    class ViewModel : BaseViewModel()
}
