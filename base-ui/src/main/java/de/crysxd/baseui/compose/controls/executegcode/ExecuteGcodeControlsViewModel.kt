package de.crysxd.baseui.compose.controls.executegcode

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.ext.connectCore
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.viewmodels.ExecuteGcodeControlsViewModelCore

class ExecuteGcodeControlsViewModel(
    private val instanceId: String,
) : ViewModel() {
    val core = connectCore(ExecuteGcodeControlsViewModelCore(instanceId))

    val items = core.items
    val visible = core.visible

    suspend fun execute(
        id: String,
        inputs: Map<String, String>,
        confirmed: Boolean
    ) = core.executeGcodeCommand(
        id = id,
        inputs = inputs,
        confirmed = confirmed
    )

    fun getMacroId(id: String) = core.getMacroId(id)

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ExecuteGcodeControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}