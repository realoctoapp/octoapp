package de.crysxd.baseui.compose.controls.webcam

import android.graphics.Bitmap
import android.net.Uri
import android.widget.ImageView
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawOutline
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.media3.common.MediaItem
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DefaultHttpDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.DefaultMediaSourceFactory
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.printconfidence.PrintConfidenceControlsState
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.components.OctoLinkButtonRow
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.menu.MenuBottomSheetFragment
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.ScaleType.CENTER_CROP
import de.crysxd.octoapp.base.data.models.ScaleType.CENTER_INSIDE
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.ShareImageUseCase
import de.crysxd.octoapp.menu.controls.WebcamSettingsMenu
import de.crysxd.octoapp.menu.controls.WebcamShareMenu
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.AspectRatio
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.math.roundToInt
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State as WebcamState

@Composable
fun WebcamActions(
    state: WebcamControlsState,
    confidenceState: PrintConfidenceControlsState?,
    editState: EditState,
    modifier: Modifier = Modifier,
) = AnimatedContent(
    targetState = state,
    contentKey = { target -> target.current is WebcamState.Hidden }
) { target ->
    if (target.current !is WebcamState.Hidden) {
        WebcamControlsBox(
            editState = editState,
            modifier = modifier
        ) {
            WebcamControlsInternal(
                state = target,
                confidenceState = confidenceState
            )
        }
    }
}


//region State
interface WebcamControlsState {
    val current: WebcamState
    var grabImage: (suspend () -> Bitmap)?
    val isFullscreen: Boolean
    val enforcedAspectRatio: AspectRatio?
    val showResolution: Boolean
    val showName: Boolean
    val isFullscreenSupported: Boolean
        get() = isFullscreen || current.let { it is WebcamState.MjpegReady || it is WebcamState.VideoReady || it is WebcamState.WebpageReady }

    fun onRetry()
    fun onSwitchWebcam()
    fun onShareImage(instanceId: String, img: suspend () -> Bitmap)
    fun onFullscreenClicked()
    fun onTroubleshoot()

    fun getInitialAspectRatio(): Float
    fun updateAspectRatio(ratio: Float)

    fun getInitialScaleType(): ImageView.ScaleType
    fun updateScaleToFillType(type: ImageView.ScaleType)
}

@Composable
fun rememberWebcamControlsState(isFullscreen: Boolean): WebcamControlsState {
    val activity = OctoAppTheme.octoActivity
    val instanceId = LocalOctoPrint.current.id
    val vmFactory = WebcamControlsViewModel.Factory(instanceId)
    val vm = viewModel(
        modelClass = WebcamControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = activity as? ViewModelStoreOwner ?: LocalViewModelStoreOwner.current ?: throw java.lang.IllegalStateException("No VM holder")
    )

    var webcamActive by remember { mutableStateOf(true) }
    val grabImage = remember { mutableStateOf<(suspend () -> Bitmap)?>(null) }
    val state = vm.state.collectAsStateWhileActive(key = "webcam", logTag = "WebcamControls", initial = WebcamState.Loading())
    val aspectRatio = vm.configAspectRatio.collectAsState(initial = AspectRatio(vm.getInitialAspectRatio()))
    val showWebcamName = vm.showWebcamName.collectAsState(initial = vm.initialShowWebcamName)
    val showWebcamResolution = vm.showResolution.collectAsState(initial = vm.initialShowWebcamResolution)
    val lifecycleOwner = LocalLifecycleOwner.current
    DisposableEffect(lifecycleOwner) {
        val lifecycle = lifecycleOwner.lifecycle
        val tag = "WebcamLifecycle"
        val observer = object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                super.onStart(owner)
                webcamActive = true
                Napier.i(tag = tag, message = "Starting")
            }

            override fun onStop(owner: LifecycleOwner) {
                super.onStop(owner)
                webcamActive = false
                Napier.i(tag = tag, message = "Stopping")
            }
        }

        lifecycle.addObserver(observer)
        onDispose {
            lifecycle.removeObserver(observer)
        }
    }

    val controller = OctoAppTheme.navController

    return remember(state, vm, grabImage, aspectRatio, showWebcamResolution, showWebcamName) {
        object : WebcamControlsState {
            override val current by state
            override val isFullscreen = isFullscreen
            override var grabImage: (suspend () -> Bitmap)? by grabImage
            override val enforcedAspectRatio by aspectRatio
            override val showResolution by showWebcamResolution
            override val showName by showWebcamName

            override fun onFullscreenClicked() {
                if (isFullscreen) {
                    controller().popBackStack()
                } else {
                    UriLibrary.getWebcamUri().open()
                }
            }

            override fun onTroubleshoot() {
                UriLibrary.getWebcamTroubleshootingUri().open()
            }

            override fun onRetry() {
                vm.retry()
            }

            override fun onSwitchWebcam() {
                vm.nextWebcam()
            }

            override fun onShareImage(instanceId: String, img: suspend () -> Bitmap) {
                activity?.lifecycleScope?.launch {
                    WebcamShareMenu.predetermineCloseMenuResult(instanceId = instanceId)?.let { result ->
                        handleShareMenuResult(result = result, img = img)
                    } ?: MenuBottomSheetFragment.createForMenu(
                        menu = WebcamShareMenu(instanceId = instanceId),
                        instanceId = instanceId,
                        onResult = { result ->
                            result ?: return@createForMenu
                            handleShareMenuResult(result = result, img = img)
                        }
                    ).show(activity.supportFragmentManager)
                }
            }

            private suspend fun handleShareMenuResult(result: String, img: suspend () -> Bitmap) {
                WebcamShareMenu.isShareImage(result).takeIf { it }?.let { _ ->
                    val context = activity ?: return@let
                    val label = SharedBaseInjector.get().printerConfigRepository.get(instanceId)?.label
                    val current = SharedBaseInjector.get().printerEngineProvider.getLastCurrentMessage(instanceId)
                    val isPrinting = current?.state?.flags?.isPrinting() == true
                    val imageName = listOfNotNull(
                        label,
                        current?.job?.file?.name?.takeIf { isPrinting }?.split(".")?.firstOrNull(),
                        current?.progress?.completion?.let { "${it.roundToInt()}percent" }?.takeIf { isPrinting },
                        SimpleDateFormat("yyyy-MM-dd__hh-mm-ss", Locale.ENGLISH).format(Date())
                    ).joinToString("__")

                    BaseInjector.get().shareImageUseCase().execute(
                        ShareImageUseCase.Params(
                            context = context,
                            imageName = imageName,
                            bitmap = img()
                        )
                    )
                }

                WebcamShareMenu.getSharedLink(result)?.let { link ->
                    ShareCompat.IntentBuilder(activity ?: return@let)
                        .setText(link)
                        .setType("text/plain")
                        .startChooser()
                }
            }

            override fun getInitialAspectRatio() =
                vm.getInitialAspectRatio()

            override fun updateAspectRatio(ratio: Float) =
                vm.storeAspectRatio(ratio)

            override fun getInitialScaleType() = when (vm.getScaleType(isFullscreen, CENTER_INSIDE)) {
                CENTER_CROP -> ImageView.ScaleType.CENTER_CROP
                else -> ImageView.ScaleType.CENTER_INSIDE
            }


            override fun updateScaleToFillType(type: ImageView.ScaleType) = vm.storeScaleType(
                isFullscreen = isFullscreen,
                scaleType = when (type) {
                    ImageView.ScaleType.CENTER_CROP -> CENTER_CROP
                    else -> CENTER_INSIDE
                }
            )
        }
    }
}

//endregion
//region Internals
const val LiveDelayThresholdMs = 3_000L

@Composable
fun WebcamControlsInternal(
    state: WebcamControlsState,
    confidenceState: PrintConfidenceControlsState?,
) {
    val boxState = remember { AspectRatioBoxScope(state.getInitialAspectRatio()) }
    val warning by remember { derivedStateOf { state.current.warning } }

    WebcamWarning(warning = warning) {
        PrintConfidence(confidenceState = confidenceState) {
            AspectRatioBox(
                state = boxState,
                updateInitialAspectRatio = state::updateAspectRatio
            ) {
                //region State
                WebcamView(
                    state = state,
                    boxState = boxState,
                    allowTouch = false,
                )
                //endregion
                //region Actions
                Row(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .fillMaxWidth()
                        .background(WebcamOverlayBrush)
                ) {
                    WebcamActions(webcamState = state) {
                        Spacer(Modifier.weight(1f))
                    }
                }
                //endregion
            }
        }
    }
}

@Composable
fun WebcamWarning(
    warning: WebcamState.Warning?,
    content: @Composable () -> Unit,
) = Column(
    modifier = Modifier
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.accent)
) {
    Box(
        modifier = Modifier
            .clip(MaterialTheme.shapes.large)
            .background(Color.Black)
    ) {
        content()
    }

    val warningId by remember(warning) { derivedStateOf { warning?.id } }

    OctoAnnouncementController(announcementId = warningId?.let { AnnouncementId.Default(it) }) {
        CompositionLocalProvider(LocalContentColor provides OctoAppTheme.colors.textColoredBackground) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                WebcamWarningText(warning = { warning })
                WebcamWarningActions(warningSate = { warning }) {
                    hideAnnouncement()
                }
            }
        }
    }
}

@Composable
fun WebcamWarningText(
    warning: () -> WebcamState.Warning?,
) = Box {
    val warningText by remember(warning) { derivedStateOf { warning()?.text ?: "" } }

    Text(
        text = warningText,
        style = OctoAppTheme.typography.base,
        modifier = Modifier.padding(OctoAppTheme.dimens.margin1),
        color = LocalContentColor.current,
        textAlign = TextAlign.Center,
    )
}

@Composable
fun WebcamWarningActions(
    warningSate: () -> WebcamState.Warning?,
    hideAnnouncement: () -> Unit
) {
    val warningActionText by remember(warningSate) { derivedStateOf { warningSate()?.actionText } }
    val warningActionUri by remember(warningSate) { derivedStateOf { warningSate()?.actionUri } }
    val warningDismissible by remember(warningSate) { derivedStateOf { warningSate()?.dismissible } }

    val canDismiss = warningDismissible == true
    val hasAction = warningActionUri != null && warningActionText != null

    OctoLinkButtonRow(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = OctoAppTheme.dimens.margin01),
        alignment = Alignment.CenterHorizontally,
        texts = listOfNotNull(
            stringResource(id = R.string.hide).takeIf { canDismiss },
            warningActionText.takeIf { hasAction },
        ),
        onClicks = listOfNotNull(
            suspend { hideAnnouncement() }.takeIf { canDismiss },
            suspend { warningActionUri?.open() ?: Unit }.takeIf { hasAction },
        ),
        color = LocalContentColor.current
    )
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
fun PrintConfidence(
    confidenceState: PrintConfidenceControlsState?,
    content: @Composable () -> Unit,
) {
    val background by animateColorAsState(confidenceState?.current?.let { confidenceState.backgroundColorFor(it) } ?: Color.Transparent)
    val shape = MaterialTheme.shapes.large
    val density = LocalDensity.current
    val layoutDirection = LocalLayoutDirection.current

    Column(
        modifier = Modifier
            .clickable(
                enabled = confidenceState?.contentClickUrl != null,
                onClick = { confidenceState?.contentClickUrl?.open() }
            )
            .drawBehind {
                drawRect(background)
                val out = shape.createOutline(size = size, layoutDirection = layoutDirection, density = density)
                drawOutline(outline = out, style = Fill, color = background)
                drawOutline(outline = out, style = Stroke(width = with(density) { 2.dp.toPx() }), color = Color.White.copy(alpha = 0.05f))
            }
    ) {
        content()

        AnimatedContent(
            targetState = confidenceState?.current,
        ) { confidence ->
            if (confidence != null) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
                    modifier = Modifier
                        .padding(OctoAppTheme.dimens.margin1)
                        .fillMaxWidth(),
                ) {
                    confidenceState?.iconFor(confidence = confidence)?.let {
                        Icon(
                            painter = it,
                            contentDescription = null,
                            tint = Color.Unspecified
                        )
                    }
                    ChatBubble(
                        text = confidence.description,
                        modifier = Modifier.weight(1f)
                    )
                    Text(
                        text = confidenceState?.disclaimerFor(confidence = confidence) ?: "",
                        style = OctoAppTheme.typography.labelSmall.copy(fontSize = 8.sp),
                        color = OctoAppTheme.colors.whiteTranslucent,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis,
                        textAlign = TextAlign.End,
                    )
                }
            }
        }
    }
}

@Composable
private fun ChatBubble(
    text: String,
    modifier: Modifier = Modifier,
) = ConstraintLayout(modifier) {
    val (bubbleStart, bubbleCenter, bubbleEnd, bubbleText) = createRefs()

    createHorizontalChain(bubbleStart, bubbleText, bubbleEnd, chainStyle = ChainStyle.Packed(0f))

    Image(
        painter = painterResource(id = R.drawable.chat_bubble_start),
        contentDescription = null,
        contentScale = ContentScale.FillBounds,
        modifier = Modifier.constrainAs(bubbleStart) {
            centerVerticallyTo(parent)
            start.linkTo(parent.start)
        }
    )
    Image(
        painter = painterResource(id = R.drawable.chat_bubble_center),
        contentDescription = null,
        contentScale = ContentScale.FillBounds,
        modifier = Modifier.constrainAs(bubbleCenter) {
            centerVerticallyTo(parent)
            start.linkTo(bubbleStart.end)
            end.linkTo(bubbleEnd.start)
            width = Dimension.fillToConstraints
        }

    )
    Text(
        text = text,
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.black,
        overflow = TextOverflow.Ellipsis,
        maxLines = 1,
        modifier = Modifier.constrainAs(bubbleText) {
            centerVerticallyTo(parent)
            width = Dimension.preferredWrapContent
        }
    )

    Image(
        painter = painterResource(id = R.drawable.chat_bubble_end),
        contentDescription = null,
        contentScale = ContentScale.FillBounds,
        modifier = Modifier.constrainAs(bubbleEnd) {
            centerVerticallyTo(parent)
            linkTo(start = bubbleText.end, end = parent.end, bias = 0f)
        }
    )
}

@Composable
private fun AspectRatioBox(
    state: AspectRatioBoxScope,
    updateInitialAspectRatio: (Float) -> Unit,
    content: @Composable BoxScope.() -> Unit,
) {
    LaunchedEffect(state.aspectRatio) {
        updateInitialAspectRatio(state.aspectRatio)
    }

    Box(
        modifier = Modifier
            .run {
                val ratio = state.aspectRatioOverride ?: state.aspectRatio.takeUnless { it.isNaN() }
                if (ratio == null || state.wrapContentIgnoreAspectRatio) {
                    this
                } else {
                    aspectRatio(ratio)
                }
            }
            .onSizeChanged {
                state.measuredAspectRatio = it.width / it.height.toFloat()
            }
            .fillMaxWidth()
            .animateContentSize()
            .background(Color.Black),
        contentAlignment = Alignment.Center,
        content = content,
    )
}

class AspectRatioBoxScope(
    initialAspectRatio: Float,
) {
    var aspectRatio: Float by mutableFloatStateOf(initialAspectRatio)
    var aspectRatioOverride: Float? by mutableStateOf(null)
    var wrapContentIgnoreAspectRatio: Boolean by mutableStateOf(false)
    var measuredAspectRatio: Float by mutableFloatStateOf(initialAspectRatio)
}

class StateHolder {
    var state: WebcamState? = null
}

@Composable
private fun WebcamControlsBox(
    modifier: Modifier,
    editState: EditState? = null,
    content: @Composable BoxScope.() -> Unit
) {
    var showSettings by remember { mutableStateOf(false) }
    val instanceId = LocalOctoPrint.current.id
    if (showSettings) {
        MenuSheet(
            menu = WebcamSettingsMenu(instanceId = instanceId),
            onDismiss = { showSettings = false }
        )
    }

    ControlsScaffold(
        title = stringResource(id = R.string.webcam),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
        actionContentDescription = stringResource(id = R.string.cd_settings),
        editState = editState,
        onAction = { _, _ -> showSettings = true }
    ) {
        Box(
            contentAlignment = Alignment.Center,
            content = content,
            modifier = modifier,
        )
    }
}

//endregion
//region Previews
private fun createMockWebcamControlsState(state: WebcamState, scaleType: ImageView.ScaleType = ImageView.ScaleType.FIT_CENTER) = object : WebcamControlsState {
    override val current = state
    override val isFullscreen = false
    override val enforcedAspectRatio = AspectRatio(16 / 9f)
    override val showResolution = true
    override val showName = false
    override var grabImage: (suspend () -> Bitmap)? = null
    override fun onRetry() = Unit
    override fun onSwitchWebcam() = Unit
    override fun onShareImage(instanceId: String, img: suspend () -> Bitmap) = Unit
    override fun onFullscreenClicked() = Unit
    override fun onTroubleshoot() = Unit
    override fun getInitialAspectRatio(): Float = 1f
    override fun updateAspectRatio(ratio: Float) = Unit
    override fun getInitialScaleType() = scaleType
    override fun updateScaleToFillType(type: ImageView.ScaleType) = Unit
}

private fun createMockConfidenceState(confidence: PrintConfidence? = null) = object : PrintConfidenceControlsState {
    override val current: PrintConfidence? = confidence
    override var contentClickUrl: Uri? = null

    @Composable
    override fun backgroundColorFor(confidence: PrintConfidence) = OctoAppTheme.colors.octoeverywhere

    @Composable
    override fun iconFor(confidence: PrintConfidence): Painter = painterResource(id = R.drawable.ic_gadget_green)

    @Composable
    override fun disclaimerFor(confidence: PrintConfidence) = stringResource(id = R.string.print_confidence_disclaimer_octoeverywhere)
}

@Preview
@Composable
private fun PreviewLoading() = OctoAppThemeForPreview {
    val text = stringResource(
        id = R.string.webcam_widget___data_warning,
        153_438_253L.formatAsFileSize()
    )

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            WebcamState.Loading(
                webcamCount = 2,
                warning = WebcamState.Warning(
                    id = "test",
                    text = text,
                    dismissible = true,
                    actionText = "Learn more",
                    actionUri = "http://google.com".toUrl()
                )
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewErrorNoRetry() = OctoAppThemeForPreview {
    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            WebcamState.Error(
                webcamCount = 2,
                canRetry = false,
                webcamUrl = "http://streamurl:5050/?action=stream",
                exception = Throwable(),
                activeWebcam = 2,
                canShowDetails = false,
                description = null,
                warning = null,
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewErrorRetry() = OctoAppThemeForPreview {
    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            WebcamState.Error(
                webcamCount = 2,
                canRetry = true,
                webcamUrl = "http://streamurl:5050/?action=stream",
                exception = Throwable(),
                activeWebcam = 2,
                canShowDetails = false,
                description = null,
                warning = null,
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewNotAvailable() = OctoAppThemeForPreview {
    val message = LocalContext.current.getString(
        R.string.please_configure_your_webcam_in_octoprint,
        "OctoPrint"
    )

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            WebcamState.Error(
                webcamCount = 0,
                activeWebcam = 2,
                webcamUrl = null,
                canRetry = false,
                warning = null,
                canShowDetails = false,
                exception = object : IllegalStateException(), UserMessageException {
                    override val userMessage = message
                }
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}

@Preview
@Composable
private fun PreviewMjpeg() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("16:9") }
    val scaleToFill by remember { mutableStateOf(ImageView.ScaleType.CENTER_INSIDE) }
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "16:9"
        }
    }

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            scaleType = scaleToFill,
            state = WebcamState.MjpegReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                rotate90 = false,
                warning = null,
                activeWebcam = 1,
                displayName = "Some cam",
                frameHeight = 720,
                frameWidth = 1280,
                frameInterval = 0,
                frames = LowOverheadMediator(
                    WebcamControlsViewModelCore.State.MjpegReady.Frame(
                        image = ContextCompat.getDrawable(context, R.drawable.webcam_demo)!!.toBitmap(),
                        frameInterval = 0,
                        fps = 0f,
                        frameTime = 0,
                    )
                ),
            ),
        ),
        confidenceState = createMockConfidenceState(
            PrintConfidence(
                description = "Print is going well but this text is way too long!",
                origin = PrintConfidence.Origin.OctoEverywhere,
                confidence = 1f,
                level = PrintConfidence.Level.High
            )
        ),
    )
}

@Preview
@Composable
private fun PreviewMjpeg2() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("16:9") }
    val scaleToFill by remember { mutableStateOf(ImageView.ScaleType.CENTER_INSIDE) }
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "16:9"
        }
    }

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            scaleType = scaleToFill,
            state = WebcamState.MjpegReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                rotate90 = false,
                frameInterval = 0,
                frameWidth = 1280,
                frameHeight = 720,
                displayName = null,
                activeWebcam = 1,
                warning = null,
                frames = LowOverheadMediator(
                    WebcamControlsViewModelCore.State.MjpegReady.Frame(
                        image = ContextCompat.getDrawable(context, R.drawable.webcam_demo)!!.toBitmap(),
                        frameInterval = 0,
                        fps = 13.37f,
                        frameTime = System.currentTimeMillis(),
                    )
                ),
            ),
        ),
        confidenceState = createMockConfidenceState(
            PrintConfidence(
                description = "Print is going well!",
                origin = PrintConfidence.Origin.OctoEverywhere,
                confidence = 1f,
                level = PrintConfidence.Level.High
            )
        ),
    )
}

@Preview
@Composable
@androidx.annotation.OptIn(UnstableApi::class)
private fun PreviewRich() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("4:3") }
    val context = LocalContext.current
    val url = "https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8"
    val player = remember { ExoPlayer.Builder(context).build() }

    DisposableEffect(Unit) {
        val mediaItem = MediaItem.fromUri(url)
        val dataSourceFactory = DefaultHttpDataSource.Factory()
        val mediaSourceFactory = DefaultMediaSourceFactory(dataSourceFactory)
        player.setMediaSource(mediaSourceFactory.createMediaSource(mediaItem))
        player.prepare()
        player.play()
        onDispose { player.stop() }
    }

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "4:3"
        }
    }

    WebcamControlsInternal(
        state = createMockWebcamControlsState(
            state = WebcamState.VideoReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                rotate90 = false,
                warning = null,
                activeWebcam = 1,
                displayName = null,
//                enforcedAspectRatio = enforcedAspectRatio,
                authHeader = null,
//                showResolution = true,
                player = player,
                uri = url,
            )
        ),
        confidenceState = createMockConfidenceState(),
    )
}
//endregion