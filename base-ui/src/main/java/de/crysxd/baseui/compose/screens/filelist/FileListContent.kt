package de.crysxd.baseui.compose.screens.filelist

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.LargeAnnouncement
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.rememberMenuSheetState
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalNavController
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.menu.files.AddItemMenu
import de.crysxd.octoapp.menu.files.FileSortOptionsMenu


@Composable
@OptIn(ExperimentalFoundationApi::class)
fun FileListContent(
    modifier: Modifier = Modifier,
    informAboutThumbnails: Boolean,
    onDismissThumbnailInfo: () -> Unit,
    folder: FileObject.Folder,
) = LazyColumn(
    modifier = modifier
        .nestedScroll(LocalNestedScrollConnection.current)
        .testTag(TestTags.FileList.Column)
) {
    item(key = "thumbnail_info") {
        AnimatedVisibility(
            visible = informAboutThumbnails,
            modifier = Modifier.fillMaxWidth(),
            enter = fadeIn() + expandVertically(),
            exit = fadeOut() + shrinkVertically(),
        ) {
            OctoAnnouncementController(
                announcementId = AnnouncementId.Default("thumbnail_info"),
            ) {
                LargeAnnouncement(
                    title = stringResource(R.string.file_manager___thumbnail_info___title),
                    text = stringResource(R.string.file_manager___thumbnail_info___details),
                    learnMoreButton = stringResource(R.string.learn_more),
                    onLearnMore = { UriLibrary.getPluginLibraryUri(category = "files").open() },
                    onHide = { onDismissThumbnailInfo() },
                    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
                )
            }
        }
    }

    items(
        items = folder.children ?: emptyList(),
        key = { it.path }
    ) { file ->
        FileListItem(
            file = file,
            modifier = Modifier.animateItemPlacement()
        )
    }

    item(
        key = "__placeholder"
    ) {
        FileActionsBar(placeholder = true)
    }
}

@Composable
fun FileListOptions(
    modifier: Modifier = Modifier,
    controller: FileListController,
) = Row(
    modifier = modifier.padding(vertical = OctoAppTheme.dimens.margin01),
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin0, Alignment.End)
) {
    val navController = LocalNavController.current
    val instanceId = LocalOctoPrint.current.id
    val addMenu = rememberMenuSheetState { AddItemMenu(instanceId = instanceId, path = controller.path) }
    val sortMenu = rememberMenuSheetState { FileSortOptionsMenu() }

    if (controller.canSwitchInstance) {
        OctoIconButton(
            painter = painterResource(R.drawable.ic_round_swap_horiz_24),
            contentDescription = stringResource(R.string.cd_settings),
            onClick = controller::showInstancePicker
        )
    }

    Spacer(modifier = Modifier.weight(1f))

    OctoIconButton(
        painter = painterResource(R.drawable.ic_round_search_24),
        contentDescription = stringResource(R.string.cd_search),
        onClick = { navController().navigate("files/$instanceId/search") }
    )

    OctoIconButton(
        painter = painterResource(R.drawable.ic_round_sort_24),
        contentDescription = stringResource(R.string.cd_sort),
        onClick = { sortMenu.show() }
    )

    OctoIconButton(
        painter = painterResource(R.drawable.ic_round_add_24),
        contentDescription = stringResource(R.string.cd_add_item),
        onClick = { addMenu.show() }
    )
}
