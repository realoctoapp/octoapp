package de.crysxd.baseui.compose.screens.companion

import android.net.Uri
import androidx.compose.animation.graphics.ExperimentalAnimationGraphicsApi
import androidx.compose.animation.graphics.res.animatedVectorResource
import androidx.compose.animation.graphics.res.rememberAnimatedVectorPainter
import androidx.compose.animation.graphics.vector.AnimatedImageVector
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.label
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.days

@Composable
fun CompanionAnnouncementView(onDismiss: () -> Unit = {}) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Animation()
        Title()
        Buttons(onDismiss)
    }
}

@Composable
@OptIn(ExperimentalAnimationGraphicsApi::class)
private fun Animation() {
    val vector = AnimatedImageVector.animatedVectorResource(R.drawable.octo_blink)
    var atEnd by remember { mutableStateOf(false) }
    LaunchedEffect(Unit) {
        delay(1000)
        atEnd = true
    }

    Icon(
        painter = rememberAnimatedVectorPainter(vector, atEnd = atEnd),
        contentDescription = null,
        tint = Color.Unspecified,
        modifier = Modifier
            .padding(vertical = OctoAppTheme.dimens.margin2)
            .size(128.dp),
    )
}

@Composable
private fun Title() = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin3),
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    Text(
        text = stringResource(R.string.companion_plugin_explainer___notifications_title),
        style = OctoAppTheme.typography.titleBig,
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxWidth()
    )

    Text(
        text = stringResource(
            id = R.string.companion_plugin_explainer___notifications_description_android,
            LocalOctoPrint.current.systemInfo?.interfaceType.label,
        ),
        style = OctoAppTheme.typography.base,
        textAlign = TextAlign.Center,
        color = OctoAppTheme.colors.normalText,
        modifier = Modifier.fillMaxWidth(),
    )
}

@Composable
private fun Buttons(
    onDismiss: () -> Unit,
) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    val isOctoPrint = LocalOctoPrint.current.systemInfo?.interfaceType == SystemInfo.Interface.OctoPrint

    OctoButton(
        text = stringResource(
            if (isOctoPrint) {
                R.string.companion_plugin_explainer___notifications_action
            } else {
                R.string.companion_plugin_explainer___notifications_action_moonraker
            }
        ),
        onClick = {
            if (isOctoPrint) {
                Uri.parse("https://plugins.octoprint.org/plugins/octoapp/").open()
            } else {
                UriLibrary.getFaqUri("octoapp_companion_moonraker").open()
            }
        }
    )

    OctoButton(
        text = stringResource(id = R.string.notifications_permission___action_later),
        onClick = {
            val notBefore = Clock.System.now() + (8 * 7).days
            Napier.i(tag = "CompanionAnnouncementView", message = "Not asking for companion before $notBefore")
            SharedBaseInjector.get().preferences.askCompanionInstallNotBefore = notBefore
            onDismiss()
        },
        type = OctoButtonType.Link
    )
}

//region Preview
@Preview
@Composable
private fun PreviewNormal() = OctoAppThemeForPreview {
    CompanionAnnouncementView()
}

//endregion