package de.crysxd.baseui.compose.screens

import android.graphics.Rect
import androidx.compose.runtime.mutableStateOf
import androidx.fragment.app.Fragment
import de.crysxd.baseui.InsetAwareScreen

abstract class ComposeScreenFragment : Fragment(), InsetAwareScreen {

    protected val composeInsets = mutableStateOf(Rect())

    override fun handleInsets(insets: Rect) {
        composeInsets.value = insets
    }

}