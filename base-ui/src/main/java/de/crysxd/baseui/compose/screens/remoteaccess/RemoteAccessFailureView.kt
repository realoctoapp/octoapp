package de.crysxd.baseui.compose.screens.remoteaccess

import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import java.text.DateFormat

@Composable
fun RemoteAccessFailureView(
    failure: RemoteConnectionFailure,
    backgroundColor: Color,
    foregroundColor: Color,
    modifier: Modifier = Modifier
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = modifier
        .background(backgroundColor, MaterialTheme.shapes.large)
        .padding(start = OctoAppTheme.dimens.margin2)
        .padding(vertical = OctoAppTheme.dimens.margin12),
) {
    val activity = OctoAppTheme.octoActivity
    val date = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(failure.dateMillis)

    Text(
        text = failure.message ?: stringResource(id = R.string.configure_remote_access___failure, date),
        style = OctoAppTheme.typography.focus,
        color = foregroundColor,
        modifier = Modifier
            .weight(1f)
            .padding(end = OctoAppTheme.dimens.margin12),
    )

    if (failure.learnMoreUrl != null) {
        OctoButton(
            text = stringResource(id = R.string.learn_more),
            small = true,
            type = OctoButtonType.Link,
            onClick = { Uri.parse(failure.learnMoreUrl).open() }

        )
    } else {
        OctoIconButton(
            painter = painterResource(R.drawable.ic_round_info_24),
            contentDescription = null,
            modifier = Modifier.padding(end = OctoAppTheme.dimens.margin2),
            onClick = {
                activity?.showDialog(
                    message = OctoActivity.Message.DialogMessage(
                        text = { failure.errorMessage },
                        neutralButton = { getString(R.string.show_details) },
                        neutralAction = {
                            activity.showDialog(
                                message = OctoActivity.Message.DialogMessage(
                                    text = { failure.errorMessageStack },
                                )
                            )
                        }
                    )
                )
            }
        )
    }
}