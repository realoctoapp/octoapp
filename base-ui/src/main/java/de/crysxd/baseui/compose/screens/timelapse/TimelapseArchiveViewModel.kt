package de.crysxd.baseui.compose.screens.timelapse

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.AddMediaToGalleryUseCase
import de.crysxd.octoapp.base.usecase.ShareFileUseCase
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.viewmodels.TimelapseViewModelCore
import java.io.File

class TimelapseArchiveViewModel(instanceId: String) : ViewModelWithCore() {

    override val core by lazy { TimelapseViewModelCore(instanceId) }
    private val shareFileUseCase = BaseInjector.get().shareFileUseCase()
    private val addMediaToGalleryUseCase = BaseInjector.get().addMediaToGalleryUseCase()
    val state get() = core.state

    suspend fun reload() {
        core.reload()
    }

    suspend fun share(context: Context, path: String) = shareFileUseCase.execute(
        ShareFileUseCase.Params(
            context = context,
            file = File(path)
        )
    )

    suspend fun export(context: Context, timelapseFile: TimelapseFile, path: String) = addMediaToGalleryUseCase.execute(
        AddMediaToGalleryUseCase.Params(
            context = context,
            file = File(path),
            date = timelapseFile.date ?: kotlinx.datetime.Clock.System.now(),
            originalName = timelapseFile.name,
        )
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = TimelapseArchiveViewModel(
            instanceId = instanceId,
        ) as T
    }
}