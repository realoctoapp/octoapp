package de.crysxd.baseui.compose.framework.helpers

@Suppress("ConstPropertyName", "FunctionName")
object TestTags {
    object BottomBar {
        const val Status: TestTag = "bottom-bar-status"
        const val Pause: TestTag = "bottom-bar-pause"
        const val Cancel: TestTag = "bottom-bar-cancel"
        const val EmergencyStop: TestTag = "bottom-bar-emergecyStop"
        const val Resume: TestTag = "bottom-bar-resume"
        const val Temperatures: TestTag = "bottom-bar-temperatures"
        const val MainMenu: TestTag = "bottom-bar-main-menu"
        const val Start: TestTag = "bottom-bar-start"
        const val ConnectAction: TestTag = "bottom-bar-connect"
        const val SwipeTrack: TestTag = "bottom-bar-swipe-track"
    }

    object ControlCenter {
        const val ContentEnabled = "control-center:enabled"
        const val ContentDisabled = "control-center:disabled"
    }

    object FileList {
        const val Column = "file-list:column"
        const val Title = "file-list:title"
        const val StartPrint = "file-list:start-print"
    }

    object Temperature {
        fun SetButton(component: String?) = "temperature:$component:set"
        fun Actual(component: String?) = "temperature:$component:current"
        fun Target(component: String?) = "temperature:$component:target"
        fun Label(component: String?) = "temperature:$component:label"
    }

    object Controls {
        const val StepConnect = "controls:step:connect"
        const val StepPrepare = "controls:step:prepare"
        const val StepPrint = "controls:step:print"
    }

    object Menu {
        fun ItemMainButton(text: String) = "menu:$text:main"
        fun ItemRightDetail(text: String) = "menu:$text:rightdetail"
        fun ItemRigthSwitch(text: String) = "menu:$text:switch"
        fun ItemSecondaryButton(text: String) = "menu:$text:secondary"
        const val Item = "menu:item"
        const val MenuSheet = "menu:sheet"
        const val Title = "menu:title"
        const val Subtitle = "menu:subtitle"
        const val Footer = "menu:foor"
    }
}

typealias TestTag = String