package de.crysxd.baseui.compose.framework.components

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.zIndex
import de.crysxd.baseui.compose.framework.layout.ScrollableTabRow
import de.crysxd.baseui.compose.framework.layout.TabPosition
import de.crysxd.baseui.compose.framework.layout.TabRow
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.launch


@Composable
fun OctoScrollableTabRow(
    selectedIndex: Int,
    onSelection: suspend (Int) -> Unit,
    options: List<String>,
    modifier: Modifier = Modifier,
    horizontalPadding: Dp = OctoAppTheme.dimens.margin2,
) = ScrollableTabRow(
    modifier = modifier.fillMaxWidth(),
    backgroundColor = Color.Transparent,
    selectedTabIndex = selectedIndex,
    edgePadding = horizontalPadding,
    indicator = { tabPositions ->
        Indicator(
            tabPositions = tabPositions,
            indicatorColor = OctoAppTheme.colors.primaryButtonBackground,
            selectedIndex = selectedIndex
        )
    },
    divider = {},
    tabs = {
        val scope = rememberCoroutineScope()
        options.forEachIndexed { index, text ->
            Item(
                selected = index == selectedIndex,
                text = text,
                horizontalPadding = true,
                onSelection = { scope.launch { onSelection(index) } },
                textColorSelected = OctoAppTheme.colors.textColoredBackground,
                textColorUnselected = OctoAppTheme.colors.primaryButtonBackground
            )
        }
    }
)

@Composable
fun OctoTabRow(
    selectedIndex: Int,
    onSelection: suspend (Int) -> Unit,
    options: List<String>,
    modifier: Modifier = Modifier,
) = TabRow(
    modifier = modifier.fillMaxWidth(),
    backgroundColor = Color.Transparent,
    selectedTabIndex = selectedIndex,
    indicator = { tabPositions ->
        Indicator(
            tabPositions = tabPositions,
            indicatorColor = OctoAppTheme.colors.primaryButtonBackground,
            selectedIndex = selectedIndex
        )
    },
    divider = {},
    tabs = {
        val scope = rememberCoroutineScope()
        options.forEachIndexed { index, text ->
            Item(
                selected = index == selectedIndex,
                text = text,
                horizontalPadding = false,
                onSelection = { scope.launch { onSelection(index) } },
                textColorSelected = OctoAppTheme.colors.textColoredBackground,
                textColorUnselected = OctoAppTheme.colors.primaryButtonBackground
            )
        }
    }
)

//region Internals
private val shape = RoundedCornerShape(percent = 50)

@Composable
private fun Item(
    selected: Boolean,
    text: String,
    horizontalPadding: Boolean,
    textColorSelected: Color,
    textColorUnselected: Color,
    onSelection: () -> Unit,
) {
    val animatedTextColor by animateColorAsState(
        targetValue = if (selected) textColorSelected else textColorUnselected,
        label = "textColor"
    )
    OctoButton(
        text = text,
        onClick = onSelection,
        small = true,
        horizontalPadding = horizontalPadding,
        modifier = Modifier.zIndex(100f),
        type = OctoButtonType.Special(
            background = { Color.Transparent },
            foreground = { animatedTextColor },
            suppressShadow = false,
            stroke = { Color.Transparent },
            shape = { shape }
        )
    )
}

@Composable
private fun Indicator(
    tabPositions: List<TabPosition>,
    indicatorColor: Color,
    selectedIndex: Int,
) = Box(Modifier.fillMaxSize()) {
    val selectedTab = tabPositions.getOrNull(selectedIndex) ?: return@Box
    val start by animateDpAsState(
        targetValue = selectedTab.left,
        label = "start",
        animationSpec = spring(
            dampingRatio = Spring.DampingRatioNoBouncy,
            stiffness = Spring.StiffnessMediumLow
        ),
    )
    val width by animateDpAsState(
        targetValue = selectedTab.width,
        label = "width",
        animationSpec = spring(
            dampingRatio = Spring.DampingRatioMediumBouncy,
            stiffness = Spring.StiffnessMedium
        ),
    )

    Box(
        modifier = Modifier
            .padding(start = start)
            .width(width = width)
            .fillMaxHeight()
            .clip(shape)
            .background(indicatorColor)
    )
}
//endregion

//region Previews
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    var selected by remember { mutableStateOf(0) }

    OctoScrollableTabRow(
        selectedIndex = selected,
        onSelection = { selected = it },
        options = listOf(
            "Tab 1",
            "Tab 200000",
            "Tab 300",
            "Tab 40",
            "Tab 5",
            "Tab 60",
            "Tab 700",
            "Tab 80",
            "Tab 9",
        )
    )
}
//endregion