package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoText
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.sharedcommon.utils.parseHtml

@Composable
fun RemoteAccessUspView(
    foregroundColor: Color,
    backgroundColor: Color,
    modifier: Modifier = Modifier,
    usps: List<RemoteAccessUsp>,
) = Column(
    modifier = modifier
        .background(backgroundColor, MaterialTheme.shapes.large)
        .padding(OctoAppTheme.dimens.margin12),
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
) {

    Text(
        text = stringResource(id = R.string.configure_remote_acces___external_service_usp_title),
        style = OctoAppTheme.typography.sectionHeader,
        color = foregroundColor,
        textAlign = TextAlign.Center,
        modifier = Modifier
            .padding(vertical = OctoAppTheme.dimens.margin1)
            .fillMaxWidth(),
    )

    usps.forEach { usp ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
        ) {
            Icon(
                painter = painterResource(id = usp.icon),
                contentDescription = null,
                tint = foregroundColor.copy(alpha = 0.33f),
            )
            OctoText(
                text = usp.description.parseHtml(),
                style = OctoAppTheme.typography.base,
                color = foregroundColor.copy(alpha = 0.8f),
            )
        }
    }
}

data class RemoteAccessUsp(
    @DrawableRes val icon: Int,
    val description: String,
)

//region Preview
@Composable
@Preview
private fun Preview() = OctoAppThemeForPreview {
    RemoteAccessUspView(
        foregroundColor = colorResource(id = R.color.white),
        backgroundColor = colorResource(id = R.color.white_translucent_3),
        modifier = Modifier
            .background(colorResource(id = R.color.octoeverywhere))
            .padding(OctoAppTheme.dimens.margin2),
        usps = listOf(
            RemoteAccessUsp(
                icon = R.drawable.ic_round_network_check_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_1)
            ),
            RemoteAccessUsp(
                icon = R.drawable.ic_round_videocam_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_2)
            ),
            RemoteAccessUsp(
                icon = R.drawable.ic_round_star_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_3)
            ),
            RemoteAccessUsp(
                icon = R.drawable.ic_round_lock_24,
                description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_4)
            )
        )
    )
}
//endregion