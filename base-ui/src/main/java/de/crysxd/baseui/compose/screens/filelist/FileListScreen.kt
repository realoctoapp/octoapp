@file:OptIn(ExperimentalFoundationApi::class, ExperimentalFoundationApi::class)

package de.crysxd.baseui.compose.screens.filelist

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoErrorDisplay
import de.crysxd.baseui.compose.framework.components.OctoTabRow
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.helpers.rememberMenuSheetState
import de.crysxd.baseui.compose.framework.helpers.rememberPrinterConfig
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.layout.StateBox
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsPadding
import de.crysxd.baseui.compose.theme.LocalNavController
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.menu.controlcenter.ManageOctoPrintInstancesMenu.Companion.AddNewInstanceResult
import de.crysxd.octoapp.menu.files.AddItemMenu
import de.crysxd.octoapp.menu.files.SelectInstanceForFilesMenu
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

@Composable
fun FileListScreen(
    controller: FileListController = rememberFileListController(),
) = Box(modifier = Modifier.fillMaxSize()) {
    val pagerState = rememberPagerState { 2 }
    var startPrintFile by remember { mutableStateOf<FileObject.File?>(null) }

    CompositionLocalProvider(LocalOctoPrint provides controller.config) {
        CollapsibleHeaderScreenScaffold(
            header = {
                Header(
                    controller = controller
                )
            },
            tabs = {
                if (controller.isFolder) {
                    FileListOptions(
                        controller = controller,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = OctoAppTheme.dimens.margin2)
                    )
                } else {
                    Tabs(
                        pagerState = pagerState,
                    )
                }
            },
            content = {
                LaunchedEffect(pagerState.targetPage) {
                    if (pagerState.targetPage == 1) {
                        collapse(lock = true)
                    } else {
                        expand()
                    }
                }

                StateBox(
                    modifier = Modifier.fillMaxSize(),
                    state = controller.state,
                    isEmpty = { it is FileObject.Folder && it.children.isNullOrEmpty() },
                    onRetry = { controller.reload() },
                    content = {
                        when (it) {
                            is FileObject.File -> {
                                FileInfoContent(pagerState = pagerState, file = it, canStartPrint = controller.canStartPrint)
                                LaunchedEffect(it) { startPrintFile = it }
                            }

                            is FileObject.Folder -> FileListContent(
                                folder = it,
                                informAboutThumbnails = controller.informAboutThumbnails,
                                onDismissThumbnailInfo = controller::dismissThumbnailInformation
                            )
                        }
                    },
                    emptyContent = { EmptyContent(path = controller.path) },
                )
            }
        )

        AnimatedContent(
            targetState = startPrintFile.takeIf { controller.canStartPrint && pagerState.currentPage == 0 },
            modifier = Modifier
                .octoAppInsetsPadding(bottom = true)
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
        ) { file ->
            if (file != null) {
                FileInfoPrintButton(
                    onStartPrint = controller::startPrint,
                    file = file,
                    modifier = Modifier.testTag(TestTags.FileList.StartPrint)
                )
            } else {
                Spacer(modifier = Modifier.fillMaxWidth())
            }
        }
    }
}


//region Controller
@Composable
fun rememberFileListController(
    path: String = "/",
    display: String? = null,
    isFolder: Boolean = true,
): FileListController {
    val activity = LocalOctoActivity.current
    val actionsController = LocalFileActionsController.current
    val instanceId = LocalOctoPrint.current.id
    val navController = LocalNavController.current
    val config = rememberPrinterConfig(instanceId)
    val canSwitchInstance = remember { BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH) }.collectAsState(false)
    val instancePickerMenu = rememberMenuSheetState(
        menu = { SelectInstanceForFilesMenu(instanceId) },
        onResult = {
            when (it) {
                null -> Unit
                AddNewInstanceResult -> activity?.enforceAllowAutomaticNavigationFromCurrentDestination()
                else -> navController().navigate(UriLibrary.getFileManagerUri(instanceId = it).encodedPathAndQuery)
            }
        }
    )

    LaunchedEffect(path, instanceId, isFolder) {
        actionsController.activePath = FileActionsController.Path(
            path = path,
            instanceId = instanceId
        ).takeIf { isFolder }
    }

    val vm = viewModel(
        modelClass = FileListViewModel::class,
        factory = FileListViewModel.Factory(instanceId),
        key = instanceId
    )
    val state = vm.state.collectAsState()
    val informAboutThumbnails = vm.informAboutThumbnails.collectAsState()

    LaunchedEffect(path, vm) {
        vm.loadFiles(path = path, skipCache = false)
    }

    return remember(vm, state, informAboutThumbnails, config) {
        object : FileListController {
            override val path = path
            override val informAboutThumbnails by informAboutThumbnails

            override val config
                get() = requireNotNull(config.value) { "Missing instance for $instanceId" }

            override val isFolder
                get() = (state.value.state as? FlowState.Ready)?.data?.let { it is FileObject.Folder } ?: isFolder

            override val objectName
                get() = ((state.value.state as? FlowState.Ready)?.data?.display ?: display)?.takeIf { it.isNotBlank() && it != "/" }
                    ?: path.split("/").lastOrNull()?.takeIf { it.isNotBlank() && it != "/" } ?: config.value.label

            override val state
                get() = state.value.state

            override val canSwitchInstance
                get() = canSwitchInstance.value && path == "/"

            override val canStartPrint
                get() = state.value.canStartPrint

            override suspend fun reload() {
                vm.loadFiles(path = path, skipCache = true)
                delay(1.seconds)
                vm.state.first { it.state !is FlowState.Loading }
            }

            override fun showInstancePicker() = instancePickerMenu.show()
            override fun dismissThumbnailInformation() = vm.dismissThumbnailInformation()
            override suspend fun startPrint(materialConfirmed: Boolean, timelapseConfirmed: Boolean) =
                vm.startPrint(materialConfirmed, timelapseConfirmed)
        }
    }
}

interface FileListController {
    val config: PrinterConfigurationV3
    val isFolder: Boolean
    val objectName: String
    val path: String
    val state: FlowState<out FileObject>
    val canSwitchInstance: Boolean
    val canStartPrint: Boolean
    val informAboutThumbnails: Boolean
    suspend fun reload()
    fun showInstancePicker()
    fun dismissThumbnailInformation()
    suspend fun startPrint(materialConfirmed: Boolean, timelapseConfirmed: Boolean): StartPrintHelper.Result
}

//endregion
//region Header
@Composable
private fun Tabs(
    modifier: Modifier = Modifier,
    pagerState: PagerState
) = OctoTabRow(
    selectedIndex = pagerState.targetPage,
    onSelection = { pagerState.animateScrollToPage(it) },
    options = listOf(
        stringResource(R.string.file_manager___file_details___tab_info),
        stringResource(R.string.file_manager___file_details___tab_preview)
    ),
    modifier = modifier
        .padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin1)
        .background(OctoAppTheme.colors.inputBackgroundAlternative, RoundedCornerShape(percent = 50))
        .padding(OctoAppTheme.dimens.margin0)
)

@Composable
private fun Header(
    modifier: Modifier = Modifier,
    controller: FileListController,
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = modifier
        .clip(RectangleShape)
        .fillMaxWidth()
        .padding(horizontal = OctoAppTheme.dimens.margin2)
        .padding(top = OctoAppTheme.dimens.margin3, bottom = OctoAppTheme.dimens.margin2)
) {
    Text(
        text = controller.objectName,
        style = OctoAppTheme.typography.titleBig,
        maxLines = 2,
        color = OctoAppTheme.colors.darkText,
        textAlign = TextAlign.Center,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier.testTag(TestTags.FileList.Title)
    )
}


//endregion
//region List
@Composable
private fun EmptyContent(
    modifier: Modifier = Modifier,
    path: String,
) {
    val instanceId = LocalOctoPrint.current.id
    val menuState = rememberMenuSheetState { AddItemMenu(instanceId = instanceId, path = path) }

    OctoErrorDisplay(
        modifier = modifier
            .padding(top = OctoAppTheme.dimens.margin5)
            .padding(horizontal = OctoAppTheme.dimens.margin2),
        title = stringResource(R.string.file_manager___file_list___no_files_in_folder).asAnnotatedString(),
        description = stringResource(R.string.file_manager___file_list___no_files_on_octoprint_subtitle, LocalOctoPrint.current.label).toHtml().asAnnotatedString(),
        actionLabel = stringResource(R.string.file_manager___file_list___upload_file),
        onAction = { menuState.show() }
    )
}

//endregion
//region Footer
//endregion
//region Preview
@Composable
private fun BasePreview(
    objectName: String = "Test",
    isFolder: Boolean = true,
    canStartPrint: Boolean = true,
    state: FlowState<out FileObject>,
) = OctoAppThemeForPreview {
    val config = LocalOctoPrint.current
    FileListScreen(
        controller = object : FileListController {
            override val config = config
            override val isFolder = isFolder
            override val path = "some/path/test"
            override val objectName = objectName
            override val canSwitchInstance = false
            override val informAboutThumbnails = false
            override val canStartPrint = canStartPrint
            override val state = state
            override fun dismissThumbnailInformation() = Unit
            override suspend fun reload() = Unit
            override fun showInstancePicker() = Unit
            override suspend fun startPrint(materialConfirmed: Boolean, timelapseConfirmed: Boolean) = StartPrintHelper.Result.Started
        }
    )
}

@Preview
@Composable
private fun PreviewFolderLoading() = BasePreview(
    state = FlowState.Loading()
)

@Preview
@Composable
private fun PreviewFileLoading() = BasePreview(
    state = FlowState.Loading(),
    isFolder = false
)

@Preview
@Composable
private fun PreviewFolderError() = BasePreview(
    state = FlowState.Error(Exception())
)

@Preview
@Composable
private fun PreviewFileError() = BasePreview(
    state = FlowState.Error(Exception()),
    isFolder = false
)

@Preview
@Composable
private fun PreviewFolderEmpty() = BasePreview(
    state = FlowState.Ready(
        FileObject.Folder(
            size = 0,
            path = "some/path/test",
            origin = FileOrigin.Gcode,
            name = "Test",
            children = emptyList(),
            display = "Test Display",
        )
    )
)

@Preview
@Composable
private fun PreviewFolder() = BasePreview(
    state = FlowState.Ready(
        FileObject.Folder(
            size = 0,
            path = "some/path/test0",
            origin = FileOrigin.Gcode,
            name = "Test",
            children = listOf(
                FileObject.Folder(
                    size = 0,
                    path = "some/path/test1",
                    origin = FileOrigin.Gcode,
                    name = "Test 2",
                    children = listOf(),
                    display = "Test Display",
                ),
                FileObject.File(
                    size = 3434,
                    path = "some/path/test2",
                    origin = FileOrigin.Gcode,
                    name = "Test 2",
                    display = "File to print.gcode",
                    date = Instant.fromEpochMilliseconds(38463264892)
                ),
                FileObject.File(
                    size = 3434,
                    path = "some/path/test3",
                    origin = FileOrigin.Gcode,
                    name = "Test 2",
                    display = "File to print with really long title that should break and be truncated eventually but not yet because we have so much space to show file names.gcode",
                    date = Instant.fromEpochMilliseconds(38463264892)
                )
            ),
            display = "Test Display",
        )
    )
)


@Preview
@Composable
private fun PreviewFile() = BasePreview(
    state = FlowState.Ready(
        FileObject.File(
            size = 3434,
            path = "some/path/test",
            origin = FileOrigin.Gcode,
            name = "Test",
            display = "File to print.gcode",
            metadata = listOf(
                FileObject.File.MetadataGroup(
                    label = "Group A",
                    id = "a",
                    items = listOf(
                        FileObject.File.MetadataItem(
                            label = "Test",
                            id = "1",
                            value = "Value",
                        ),
                        FileObject.File.MetadataItem(
                            label = "Test 2",
                            id = "2",
                            value = "Value",
                        ),
                        FileObject.File.MetadataItem(
                            label = "Test 3",
                            id = "3",
                            value = "Value that is so long it will break into multiple lines but will not be truncated because we are not monsters and want all content to be equally visible",
                        )
                    ),
                ),
                FileObject.File.MetadataGroup(
                    label = "Group B",
                    id = "B",
                    items = listOf(
                        FileObject.File.MetadataItem(
                            label = "Test",
                            id = "b:1",
                            value = "Value",
                        ),
                        FileObject.File.MetadataItem(
                            label = "Test 2",
                            id = "b:2",
                            value = "Value",
                        ),
                        FileObject.File.MetadataItem(
                            label = "Test 3",
                            id = "b:3",
                            value = "Value that is so long it will break into multiple lines but will not be truncated because we are not monsters and want all content to be equally visible",
                        )
                    )
                )
            )
        )
    ),
    isFolder = false
)

//endregion