package de.crysxd.baseui.compose.controls

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import kotlinx.coroutines.launch
import org.burnoutcrew.reorderable.ReorderableState
import org.burnoutcrew.reorderable.detectReorder
import org.burnoutcrew.reorderable.rememberReorderableLazyListState

@Composable
fun ControlsScaffold(
    title: String?,
    modifier: Modifier = Modifier,
    actionIcon: Painter?,
    actionLoading: Boolean = false,
    editState: EditState? = null,
    actionContentDescription: String? = null,
    onAction: suspend (FragmentManager, NavController) -> Unit = { _, _ -> },
    content: @Composable ColumnScope.() -> Unit,
) = ControlsScaffold(
    title = title,
    modifier = modifier,
    editState = editState,
    actionArea = {
        val getFragmentManager = OctoAppTheme.fragmentManager
        val getNavController = OctoAppTheme.navController

        EditIcon(
            painter = actionIcon,
            onClick = { onAction(getFragmentManager(), getNavController()) },
            loading = actionLoading,
            contentDescription = actionContentDescription,
        )
    },
    content = content
)

@Composable
@OptIn(ExperimentalComposeUiApi::class)
fun ControlsScaffold(
    title: String?,
    modifier: Modifier = Modifier,
    editState: EditState? = null,
    actionArea: (@Composable ControlsScaffoldActionAreaScope.() -> Unit)? = null,
    content: @Composable ColumnScope.() -> Unit,
) = EditBox(
    editState = editState,
    actionArea = actionArea ?: {},
    modifier = modifier
        .fillMaxWidth()
        .padding(
            bottom = if (OctoAppTheme.compactLayout) OctoAppTheme.dimens.margin1 else OctoAppTheme.dimens.margin2,
            top = OctoAppTheme.dimens.margin1,
            start = OctoAppTheme.dimens.margin2,
            end = OctoAppTheme.dimens.margin2
        )
        .pointerInteropFilter {
            // Block touch while in edit mode
            editState?.editing == true
        }
) {
    if (!OctoAppTheme.compactLayout && (title != null || actionArea != null)) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(start = OctoAppTheme.dimens.margin2, end = OctoAppTheme.dimens.margin1)
                .heightIn(min = 40.dp)
                .height(IntrinsicSize.Min),
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12)
        ) {
            title?.let {
                Text(
                    text = title,
                    style = OctoAppTheme.typography.sectionHeader,
                    modifier = Modifier.weight(1f),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1
                )
            }

            actionArea?.invoke(object : ControlsScaffoldActionAreaScope {})
        }
    }

    content()
}

data class EditState(
    val editing: Boolean,
    val dragging: Boolean,
    val dragState: ReorderableState<*>,
    val hidden: Boolean,
    val toggleHidden: () -> Unit,
) {
    companion object {
        val ForPreview
            @Composable get() = EditState(
                editing = false,
                dragging = false,
                dragState = rememberReorderableLazyListState(onMove = { _, _ -> }),
                hidden = false,
                toggleHidden = {}
            )
    }
}

private const val EditScale = 0.66f
private const val EditDragScale = 0.75f

@Composable
private fun EditBox(
    editState: EditState?,
    modifier: Modifier = Modifier,
    actionArea: @Composable ControlsScaffoldActionAreaScope.() -> Unit,
    content: @Composable ColumnScope.() -> Unit
) = Box(contentAlignment = Alignment.Center) {
    //region Animations
    val animatedScale by animateFloatAsState(
        targetValue = when {
            editState?.dragging == true -> EditDragScale
            editState?.editing == true -> EditScale
            else -> 1f
        }
    )

    val animatedElevation by animateDpAsState(
        targetValue = when {
            // We must not set shadow to 0 while editing to prevent a animation glitch
            editState?.dragging == true -> OctoAppTheme.dimens.elementShadowLow
            editState?.editing == true -> 0.000001.dp
            else -> 0.dp
        }
    )

    val animatedAlpha by animateFloatAsState(
        targetValue = when (editState?.editing == true && editState.hidden) {
            true -> 0.3f
            else -> 1f
        }
    )
    //endregion
    //region Actions
    EditIcon(
        painter = painterResource(id = R.drawable.ic_round_drag_handle_24).takeIf { editState?.editing == true },
        modifier = Modifier
            .padding(horizontal = OctoAppTheme.dimens.margin23)
            .align(Alignment.CenterEnd)
            .then(editState?.dragState?.let { Modifier.detectReorder(it) } ?: Modifier),
    )

    if (editState?.editing == true && !editState.dragging) {
        Column(
            modifier = Modifier
                .align(Alignment.CenterStart)
                .padding(horizontal = OctoAppTheme.dimens.margin23),
            verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12),
        ) {
            actionArea(object : ControlsScaffoldActionAreaScope {})

            EditIcon(
                painter = painterResource(id = if (editState.hidden) R.drawable.ic_round_visibility_off_24 else R.drawable.ic_round_visibility_24),
                onClick = { editState.toggleHidden?.invoke() },
            )
        }
    }
    //endregion
    //region Content
    Column(
        modifier = Modifier
            .scale(animatedScale)
            .shadow(elevation = animatedElevation, shape = MaterialTheme.shapes.large)
            .clip(MaterialTheme.shapes.large)
            .background(if (animatedElevation > 0.dp) OctoAppTheme.colors.windowBackground else Color.Transparent)
            .then(if (editState?.editing == true) Modifier.clickable { } else Modifier)
            .alpha(animatedAlpha)
            .then(modifier),
        content = content
    )
    //endregion
}

interface ControlsScaffoldActionAreaScope

@Composable
@Suppress("UnusedReceiverParameter")
fun ControlsScaffoldActionAreaScope.ActionButton(
    painter: Painter?,
    modifier: Modifier = Modifier,
    contentDescription: String? = null,
    loading: Boolean = false,
    onClick: (suspend () -> Unit)? = null,
) = EditIcon(
    painter = painter,
    modifier = modifier,
    contentDescription = contentDescription,
    loading = loading,
    onClick = onClick,
)

@Composable
private fun EditIcon(
    painter: Painter?,
    modifier: Modifier = Modifier,
    contentDescription: String? = null,
    loading: Boolean = false,
    onClick: (suspend () -> Unit)? = null,
) {
    val scope = rememberCoroutineScope()
    var internalLoading by remember { mutableStateOf(loading) }

    Crossfade(
        targetState = painter to (internalLoading || loading),
        modifier = modifier,
        label = "icon",
    ) { (p, l) ->
        Box(
            modifier = Modifier.size(24.dp),
            contentAlignment = Alignment.Center,
        ) {
            if (p != null && !l) {
                Icon(
                    painter = p,
                    contentDescription = contentDescription,
                    tint = OctoAppTheme.colors.primaryButtonBackground,
                    modifier = Modifier
                        .clickable(
                            interactionSource = MutableInteractionSource(),
                            indication = rememberRipple(bounded = false),
                            enabled = onClick != null,
                            onClick = {
                                scope.launch {
                                    try {
                                        internalLoading = true
                                        onClick?.invoke()
                                    } finally {
                                        internalLoading = false
                                    }
                                }
                            }
                        )
                )
            } else if (l) {
                CircularProgressIndicator(
                    strokeWidth = 3.dp,
                    modifier = Modifier.padding(2.dp)
                )
            }
        }
    }
}