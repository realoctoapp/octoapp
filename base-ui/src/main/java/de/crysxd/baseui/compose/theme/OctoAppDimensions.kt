package de.crysxd.baseui.compose.theme

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R

interface OctoAppDimensions {

    val margin0: Dp
        @Composable get() = dimensionResource(R.dimen.margin_0)

    val margin01: Dp
        @Composable get() = dimensionResource(R.dimen.margin_0_1)

    val margin1: Dp
        @Composable get() = dimensionResource(R.dimen.margin_1)

    val margin12: Dp
        @Composable get() = dimensionResource(R.dimen.margin_1_2)

    val margin2: Dp
        @Composable get() = dimensionResource(R.dimen.margin_2)

    val margin23: Dp
        @Composable get() = dimensionResource(R.dimen.margin_2_3)

    val margin3: Dp
        @Composable get() = dimensionResource(R.dimen.margin_3)

    val margin4: Dp
        @Composable get() = dimensionResource(R.dimen.margin_4)

    val margin5: Dp
        @Composable get() = dimensionResource(R.dimen.margin_5)

    val margin6: Dp
        @Composable get() = dimensionResource(R.dimen.margin_6)

    val cornerRadius: Dp
        @Composable get() = dimensionResource(R.dimen.widget_corner_radius)

    val cornerRadiusSmall: Dp
        @Composable get() = dimensionResource(R.dimen.widget_corner_radius_small)

    val elementShadow: Dp
        @Composable get() = 5.dp

    val elementShadowLow: Dp
        @Composable get() = 2.dp

}