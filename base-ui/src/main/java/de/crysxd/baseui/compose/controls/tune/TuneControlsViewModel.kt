package de.crysxd.baseui.compose.controls.tune

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.viewmodels.TuneControlsViewModelCore
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch

class TuneControlsViewModel(
    private val instanceId: String,
) : ViewModel() {

    companion object {
        private var counter = 0
    }

    private val tag = "TuneControlsViewModel/${counter++}"
    private val core = TuneControlsViewModelCore(instanceId = instanceId)
    private val automaticLightEventUseCase = SharedBaseInjector.get().handleAutomaticLightEventUseCase()
    val state = core.state
    val fullscreenState = flow {
        emit(Unit)
        delay(Long.MAX_VALUE)
    }.onStart {
        Napier.i(tag = tag, message = "Starting state flow")
        automaticLightEventUseCase.execute(HandleAutomaticLightEventUseCase.Event.WebcamVisible(source = tag, instanceId = instanceId))
    }.onCompletion {
        Napier.i(tag = tag, message = "Stopping state flow")
        AppScope.launch {
            try {
                automaticLightEventUseCase.execute(HandleAutomaticLightEventUseCase.Event.WebcamGone(source = tag, instanceId = instanceId))
            } catch (e: Exception) {
                Napier.i(tag = tag, message = "Failed to turn off lights", throwable = e)
            }
        }
    }.shareIn(scope = viewModelScope, started = SharingStarted.WhileSubscribedOctoDelay)

    suspend fun applyValues(
        feedRate: Float?,
        flowRate: Float?,
        fanSpeeds: Map<String, Float?>
    ) = core.applyChanges(
        feedRate = feedRate,
        flowRate = flowRate,
        fanSpeeds = fanSpeeds,
    )

    suspend fun changeOffset(
        changeMm: Float
    ) = core.changeOffset(
        changeMm = changeMm,
    )

    suspend fun resetOffset() = core.resetOffset()

    suspend fun saveOffsetToConfig() = core.saveOffsetToConfig()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = TuneControlsViewModel(
            instanceId = instanceId,
        ) as T
    }

    fun setActivePoll(activePoll: Boolean) = core.setActivePoll(active = activePoll)
}