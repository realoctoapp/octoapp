package de.crysxd.baseui.compose.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.utils.NavigationResultMediator
import io.github.aakira.napier.Napier
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FilePickerMenuItemHelperFragment : Fragment() {

    private val tag = "FilePickerMenuItemHelperFragment"
    private lateinit var launcher: ActivityResultLauncher<String>
    private var started = false
    private val resultId get() = requireNotNull(arguments?.getInt(ARG_RESULT_ID))

    companion object {
        private const val ARG_RESULT_ID = "result_id"
        fun createForResultId(resultId: Int) = FilePickerMenuItemHelperFragment().also {
            it.arguments = bundleOf(
                ARG_RESULT_ID to resultId
            )
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = View(context)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        launcher = registerForActivityResult(ActivityResultContracts.GetContent()) {
            Napier.i(tag = tag, message = "Got file for upload: $it")
            NavigationResultMediator.postResult(resultId = resultId, result = it)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        NavigationResultMediator.postResult(resultId = resultId, result = null)
    }

    override fun onStart() {
        super.onStart()
        if (!started) {
            Napier.i(tag = tag, message = "Picking file for upload")
            started = true
            launcher.launch("*/*")
        } else {
            lifecycleScope.launch {
                // Delay a bit to launch we got the result first
                delay(50)
                Napier.i(tag = tag, message = "User returned, ending file selection")
                NavigationResultMediator.postResult(resultId, null)
                parentFragmentManager.beginTransaction()
                    .remove(this@FilePickerMenuItemHelperFragment)
                    .commitAllowingStateLoss()
            }
        }
    }
}