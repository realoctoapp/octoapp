package de.crysxd.baseui.compose.screens.terminal

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.viewmodels.TerminalViewModelCore
import de.crysxd.octoapp.viewmodels.TerminalViewModelCore.TerminalLogLine
import kotlinx.datetime.Clock

@Composable
fun TerminalScreen(controller: TerminalController) = CollapsibleHeaderScreenScaffold(
    headerBackground = OctoAppTheme.colors.inputBackground,
    header = { },
    tabs = { },
) {
    Column {
        val logsState = rememberLazyListState()
        val logsAlpha by animateFloatAsState(if (controller.data.logs.isEmpty()) 0f else 1f)

        TerminalLogs(
            isStyled = controller.data.useStyledList,
            logs = controller.data.logs,
            state = logsState,
            onExecuteGcode = controller::executeGcode,
            canExecute = controller.data.inputEnabled,
            modifier = Modifier
                .weight(1f)
                .graphicsLayer { alpha = logsAlpha },
        )

        TerminalInput(
            onClear = controller::clear,
            onExecuteGcode = {
                controller.executeGcode(it)
                logsState.scrollToItem(0)
            },
            onToggleStyled = controller::toggleStyledLogs,
            inputEnabled = controller.data.inputEnabled,
            shortcuts = controller.data.shortcuts,
            isStyled = controller.data.useStyledList,
            filterCount = controller.data.filterCount,
            modifier = Modifier.background(OctoAppTheme.colors.windowBackground)
        )
    }
}

@Composable
fun rememberTerminalController(): TerminalController {
    val factory = TerminalViewModel.Factory(LocalOctoPrint.current.id)
    val viewModel = viewModel(
        modelClass = TerminalViewModel::class.java,
        factory = factory,
        key = factory.id,
    )

    val state = viewModel.state.collectAsState(
        initial = TerminalViewModelCore.State(
            shortcuts = viewModel.getShortcuts(),
            inputEnabled = false,
            useStyledList = false,
            logs = emptyList(),
            supportsStyledList = false,
            filterCount = 0,
        )
    )

    return remember(state, viewModel) {
        object : TerminalController {
            override val data by state
            override suspend fun clear() = viewModel.clear()
            override suspend fun executeGcode(gcode: String) = viewModel.executeGcode(gcode)
            override suspend fun toggleStyledLogs() = viewModel.toggleStyledLogs()
        }
    }
}

interface TerminalController {
    val data: TerminalViewModelCore.State

    suspend fun clear()
    suspend fun executeGcode(gcode: String)
    suspend fun toggleStyledLogs()
}


//region Preview
@Composable
private fun BasePreview(
    inputEnabled: Boolean = true,
    supportsStyled: Boolean = true,
    logs: List<TerminalLogLine> = listOf(
        TerminalLogLine(
            id = -1,
            text = "Random",
            isStart = false,
            isEnd = false,
            date = Clock.System.now(),
            isMiddle = false,
            textStyled = "Random"
        ),
        TerminalLogLine(
            id = 0,
            text = "G28",
            isStart = true,
            isEnd = false,
            date = Clock.System.now(),
            isMiddle = false,
            textStyled = "G28"
        ),
        TerminalLogLine(
            id = 1,
            text = "Wait",
            isStart = false,
            isEnd = false,
            date = Clock.System.now(),
            isMiddle = true,
            textStyled = "Wait"
        ),
        TerminalLogLine(
            id = 2,
            text = "Wait",
            isStart = false,
            isEnd = false,
            date = Clock.System.now(),
            isMiddle = true,
            textStyled = "Wait"
        ),
        TerminalLogLine(
            id = 3,
            text = "Done",
            isStart = false,
            isEnd = true,
            date = Clock.System.now(),
            isMiddle = false,
            textStyled = "Done"
        ),
        TerminalLogLine(
            id = 4,
            text = "Random",
            isStart = false,
            isEnd = false,
            date = Clock.System.now(),
            isMiddle = false,
            textStyled = "Random"
        ),
    ),
    shortcuts: List<GcodeHistoryItem> = listOf(
        GcodeHistoryItem("G28", isFavorite = true),
        GcodeHistoryItem("M500"),
        GcodeHistoryItem("SAVE_STATE")
    )
) = OctoAppThemeForPreview {
    TerminalScreen(
        controller = object : TerminalController {
            override val data = TerminalViewModelCore.State(
                logs = logs,
                inputEnabled = inputEnabled,
                shortcuts = shortcuts,
                useStyledList = supportsStyled,
                filterCount = 0,
                supportsStyledList = supportsStyled,
            )

            override suspend fun clear() = Unit
            override suspend fun executeGcode(gcode: String) = Unit
            override suspend fun toggleStyledLogs() = Unit
        }
    )
}

@Preview
@Composable
fun PreviewDefault() = BasePreview()


@Preview
@Composable
fun PreviewStyled() = BasePreview(
    supportsStyled = true
)


@Preview
@Composable
fun PreviewInputDisabled() = BasePreview(
    inputEnabled = false
)

//endregion