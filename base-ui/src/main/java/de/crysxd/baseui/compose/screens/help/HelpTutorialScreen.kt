package de.crysxd.baseui.compose.screens.help

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.coerceAtMost
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoBadge
import de.crysxd.baseui.compose.framework.components.OctoListItem
import de.crysxd.baseui.compose.framework.components.PicassoImage
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.layout.StateBox
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedexternalapis.tutorials.model.Tutorial
import de.crysxd.octoapp.viewmodels.HelpTutorialViewModelCore.TutorialsList
import kotlinx.datetime.Instant
import kotlinx.datetime.toInstant
import kotlin.math.absoluteValue

@Composable
fun TutorialScreen(
    controller: TutorialController = rememberTutorialController()
) = CollapsibleHeaderScreenScaffold(
    header = { Header() },
    onRefresh = controller::reload
) {
    StateBox(
        state = controller.state,
        isEmpty = { it.tutorials.isEmpty() },
        emptyText = "No tutorials found",
        onRetry = controller::reload
    ) {
        Content(tutorials = it)
    }
}

//region Content
@Composable
fun Header() = Row(
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
    modifier = Modifier.padding(vertical = OctoAppTheme.dimens.margin5)
) {
    Image(
        painter = painterResource(R.drawable.ic_youtube_play_36),
        contentDescription = null,
    )

    Text(
        text = stringResource(R.string.tutorials___header),
        style = OctoAppTheme.typography.titleBig,
        color = OctoAppTheme.colors.darkText
    )
}

@Composable
private fun Content(tutorials: TutorialsList) = LazyColumn(
    modifier = Modifier.nestedScroll(LocalNestedScrollConnection.current),
) {
    items(
        items = tutorials.tutorials,
        key = { it.url }
    ) {
        OctoListItem(
            onClick = { Uri.parse(it.url).open() },
            thumbnail = { it.Thumbnail() },
            title = { it.Title(seenUntil = tutorials.seenUntil) },
            detail = {
                Text(
                    maxLines = 5,
                    text = it.description.split("\n\n").first().takeIf { it.isNotBlank() } ?: it.publishedAt.format(showTime = false),
                )
            },
        )
    }
}

@Composable
private fun Tutorial.Thumbnail() {
    val width = (LocalConfiguration.current.screenWidthDp.dp * 0.33f).coerceAtMost(250.dp)
    val widthPx = with(LocalDensity.current) { width.roundToPx() }

    thumbnails.minByOrNull { (it.width - widthPx).absoluteValue }?.run {
        if (!OctoAppTheme.isPreview) {
            PicassoImage(
                url = url,
                fallback = R.drawable.ic_round_videocam_24,
                contentScale = { ContentScale.Crop },
                modifier = Modifier
                    .width(width)
                    .aspectRatio(16 / 9f)
            )
        }
    }
}

@Composable
private fun Tutorial.Title(seenUntil: Instant) = Row(
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
) {
    if (publishedAt > seenUntil) {
        OctoBadge(
            text = "New",
            background = OctoAppTheme.colors.red
        )
    }

    Text(
        text = title,
        maxLines = 2,
        overflow = TextOverflow.Ellipsis,
    )
}

//endregion
//region Controller
@Composable
fun rememberTutorialController(): TutorialController {
    val vm = viewModel(HelpTutorialViewModel::class.java)
    val state = vm.state.collectAsState(initial = FlowState.Loading())

    return remember(state, vm) {
        object : TutorialController {
            override val state by state
            override suspend fun reload() = vm.reload()
        }
    }
}

interface TutorialController {
    val state: FlowState<TutorialsList>
    suspend fun reload()
}

//endregion
//region Preview
@Composable
fun BasePreview(state: FlowState<TutorialsList>) = OctoAppThemeForPreview {
    TutorialScreen(
        controller = object : TutorialController {
            override val state get() = state
            override suspend fun reload() = Unit
        }
    )
}

@Composable
@Preview(showSystemUi = true)
fun PreviewLoading() = BasePreview(state = FlowState.Loading())

@Composable
@Preview(showSystemUi = true)
fun PreviewEmpty() = BasePreview(state = FlowState.Ready(TutorialsList(emptyList(), Instant.DISTANT_PAST)))


@Composable
@Preview(showSystemUi = true)
fun PreviewError() = BasePreview(state = FlowState.Error(IllegalStateException("Test")))

@Composable
@Preview(showSystemUi = true)
fun PreviewData() {
    val tutorial = Tutorial(
        title = "Tutorial A with long title that should be shortened",
        url = "a",
        thumbnails = listOf(
            Tutorial.Thumbnail(
                url = "https://i.ytimg.com/vi/vzmCS3UPYEE/default.jpg",
                width = 120,
                height = 90
            ),
            Tutorial.Thumbnail(
                url = "https://i.ytimg.com/vi/vzmCS3UPYEE/mqdefault.jpg",
                width = 320,
                height = 180
            ),
            Tutorial.Thumbnail(
                url = "https://i.ytimg.com/vi/vzmCS3UPYEE/hqdefault.jpg",
                width = 480,
                height = 360
            ),
        ),
        description = "Some description test",
        publishedAt = "2024-06-02T07:42:30Z".toInstant(),
    )

    BasePreview(
        state = FlowState.Ready(
            TutorialsList(
                seenUntil = "2024-06-01T07:42:30Z".toInstant(),
                tutorials = listOf(
                    tutorial,
                    tutorial.copy(title = "Tutorial B", url = "b"),
                    tutorial.copy(title = "Tutorial C with long title that should be shortened", url = "c"),
                    tutorial.copy(title = "Tutorial D", url = "d"),
                    tutorial.copy(title = "Tutorial E", url = "e"),
                )
            )
        )
    )
}
//endregion