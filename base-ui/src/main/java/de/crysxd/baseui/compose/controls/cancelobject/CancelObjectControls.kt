package de.crysxd.baseui.compose.controls.cancelobject

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericLoadingControls
import de.crysxd.baseui.compose.controls.GenericLoadingControlsState
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel.ViewState
import de.crysxd.baseui.compose.controls.cancelobject.CancelObjectViewModel.ObjectList
import de.crysxd.baseui.compose.controls.cancelobject.CancelObjectViewModel.PrintObject
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoDialog
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalCompactLayout
import de.crysxd.baseui.compose.theme.LocalNavController
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.viewmodels.CancelObjectViewModelCore

@Composable
fun CancelObjectControls(
    editState: EditState,
    state: CancelObjectControlsState,
) = GenericLoadingControls(
    editState = editState,
    state = state,
    title = stringResource(id = R.string.widget_cancel_object),
    actionIcon = painterResource(id = R.drawable.ic_round_view_comfy_alt_24),
    onAction = { _, nc -> nc.showAllObjects(state) }
) { objects ->
    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        objects.subset.forEach { obj ->
            Object(obj = obj, onCancel = { state.cancelObject(obj) })
        }

        if (LocalCompactLayout.current) {
            val navController = LocalNavController.current
            OctoButton(
                onClick = { navController().showAllObjects(state) },
                text = stringResource(id = R.string.gcode_preview),
                small = true,
                type = OctoButtonType.Link,
            )
        }
    }
}

private fun NavController.showAllObjects(state: CancelObjectControlsState) {
    navigate(
        R.id.action_show_cancel_object_details,
        CancelObjectBottomSheetArgs(state.instanceId).toBundle()
    )
}

//region Internals
@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun Object(obj: PrintObject, onCancel: () -> Unit) = Row(
    modifier = Modifier
        .height(IntrinsicSize.Min)
        .heightIn(min = 40.dp)
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.small)
        .background(OctoAppTheme.colors.inputBackground.copy(alpha = 0.66f))
) {
    //region Main button
    Crossfade(
        targetState = obj.active,
        modifier = Modifier.weight(1f)
    ) { active ->
        Row(
            modifier = Modifier
                .clip(MaterialTheme.shapes.large)
                .fillMaxHeight()
                .background(OctoAppTheme.colors.inputBackground)
                .clickable { }
                .padding(horizontal = OctoAppTheme.dimens.margin01, vertical = OctoAppTheme.dimens.margin01),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)
        ) {
            Icon(
                painter = painterResource(id = if (active) R.drawable.ic_round_arrow_forward_24 else R.drawable.ic_empty_16),
                contentDescription = null,
                tint = OctoAppTheme.colors.darkText,
                modifier = Modifier
                    .size(24.dp)
                    .padding(start = OctoAppTheme.dimens.margin01),
            )

            Text(
                text = obj.label,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = OctoAppTheme.typography.base.copy(
                    textDecoration = TextDecoration.LineThrough.takeIf { obj.cancelled }
                ),
                color = if (active) OctoAppTheme.colors.darkText else OctoAppTheme.colors.normalText,
                modifier = Modifier.weight(1f)
            )
        }
    }
    //endregion
    //region Cancel button
    AnimatedContent(targetState = obj.loading to obj.cancelled) {
        val (loading, cancelled) = it
        if (cancelled) return@AnimatedContent

        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(1f)
                .clip(MaterialTheme.shapes.small)
                .clickable { if (!loading) onCancel() }
        ) {
            Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
                if (loading) {
                    CircularProgressIndicator(modifier = Modifier.scale(0.66f))
                } else {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_round_delete_24),
                        contentDescription = stringResource(id = R.string.cd_delete),
                        tint = OctoAppTheme.colors.accent,
                    )
                }
            }
        }
    }
    //endregion
}
//endregion

//region State
@Composable
fun rememberCancelObjectState(): CancelObjectControlsState {
    val instanceId = LocalOctoPrint.current.id
    val vmFactory = CancelObjectViewModel.Factory(instanceId)
    val vm = viewModel(
        modelClass = CancelObjectViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory,
        viewModelStoreOwner = requireNotNull(OctoAppTheme.octoActivity)
    )
    val wasError = remember { mutableStateOf(false) }
    val state = vm.state.collectAsStateWhileActive(key = "cancelobject", initial = vm.stateCache)

    SideEffect {
        wasError.value = state.value is ViewState.Error || wasError.value
    }

    var cancelDialog by remember { mutableStateOf<PrintObject?>(null) }
    cancelDialog?.let { obj ->
        OctoDialog(
            title = null,
            message = stringResource(id = R.string.cancel_object___confirmation_message, obj.label),
            positiveButton = stringResource(R.string.cancel_object___confirmation_positive),
            negativeButton = stringResource(R.string.cancel_object___confirmation_negative),
            onPositive = { vm.cancelObject(obj.objectId) },
            onDismissRequest = { cancelDialog = null }
        )
    }

    return object : CancelObjectControlsState {
        override val wasError get() = wasError.value
        override val current get() = state.value
        override val instanceId get() = instanceId
        override fun onRetry() = vm.retry()
        override fun cancelObject(obj: PrintObject) {
            cancelDialog = obj
        }
    }
}

interface CancelObjectControlsState : GenericLoadingControlsState<ObjectList> {
    fun cancelObject(obj: PrintObject)
    val instanceId: String
}
//endregion

//region Preview
@Preview
@Composable
private fun PreviewLoading() = OctoAppThemeForPreview {
    CancelObjectControls(
        editState = EditState.ForPreview,
        state = object : CancelObjectControlsState {
            override val wasError = true
            override val instanceId = ""
            override val current = ViewState.Loading
            override fun onRetry() = Unit
            override fun cancelObject(obj: PrintObject) = Unit
        }
    )
}

@Preview
@Composable
private fun PreviewError() = OctoAppThemeForPreview {
    CancelObjectControls(
        editState = EditState.ForPreview,
        state = object : CancelObjectControlsState {
            override val wasError = true
            override val instanceId = ""
            override val current = ViewState.Error(java.lang.IllegalStateException("Somehting"))
            override fun onRetry() = Unit
            override fun cancelObject(obj: PrintObject) = Unit
        }
    )
}

@Preview
@Composable
private fun PreviewData() = OctoAppThemeForPreview {
    val cancelled = remember {
        mutableStateMapOf(
            "1" to false,
            "2" to false,
            "3" to false,
        )
    }

    val loading = remember {
        mutableStateMapOf(
            "1" to true,
            "2" to false,
            "3" to false,
        )
    }

    CancelObjectControls(
        editState = EditState.ForPreview,
        state = object : CancelObjectControlsState {
            override fun onRetry() = Unit
            override val instanceId = ""
            override val wasError = true
            override val current = ViewState.Data(
                contentKey = "key",
                data = ObjectList(
                    subset = listOf(
                        PrintObject(
                            active = false,
                            objectId = "1",
                            label = "Object A",
                            cancelled = cancelled["1"] == true,
                            loading = loading["1"] == true,
                            center = null,
                            core = CancelObjectViewModelCore.PrintObject(
                                active = false,
                                objectId = "1",
                                label = "Object A",
                                cancelled = cancelled["1"] == true,
                                center = null,
                            )
                        ),
                        PrintObject(
                            active = true,
                            objectId = "2",
                            label = "Object B",
                            cancelled = cancelled["2"] == true,
                            loading = loading["2"] == true,
                            center = null,
                            core = CancelObjectViewModelCore.PrintObject(
                                active = true,
                                objectId = "2",
                                label = "Object B",
                                cancelled = cancelled["2"] == true,
                                center = null,
                            )
                        ),
                        PrintObject(
                            active = false,
                            objectId = "3",
                            label = "Object C",
                            cancelled = cancelled["3"] == true,
                            loading = loading["3"] == true,
                            center = null,
                            core = CancelObjectViewModelCore.PrintObject(
                                active = true,
                                objectId = "2",
                                label = "Object B",
                                cancelled = cancelled["2"] == true,
                                center = null,
                            )
                        )
                    ),
                    all = emptyList()
                )
            )

            override fun cancelObject(obj: PrintObject) {
                cancelled[obj.objectId] = true
            }
        }
    )
}
//endregion