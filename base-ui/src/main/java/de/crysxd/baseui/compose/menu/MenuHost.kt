package de.crysxd.baseui.compose.menu

import android.net.Uri
import androidx.activity.compose.BackHandler
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.spring
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.benasher44.uuid.uuid4
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.PreviewMenuItems
import de.crysxd.octoapp.menu.main.settings.AndroidMenuHost
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

private const val TAG = "MenuHost"

@Composable
fun MenuHost(
    modifier: Modifier = Modifier,
    menu: Menu,
    menuId: MenuId,
    onCloseMenu: (String?) -> Unit,
) {
    val scope = rememberCoroutineScope()
    val state = rememberMenuHostState(
        menu = menu,
        menuId = menuId,
        onCloseMenu = onCloseMenu,
    )

    BackHandler(enabled = state.canNavigateUp) {
        scope.launch {
            state.navigateUp()
        }
    }

    MenuHost(
        state = state,
        modifier = modifier
    )
}


@Composable
fun rememberMenuHostState(
    menu: Menu,
    menuId: MenuId,
    onCloseMenu: suspend (String?) -> Unit,
    onOpenLink: (Uri) -> Unit = { it.open() },
): MenuHostState {
    val context = rememberMenuContext()
    val systemInfo = LocalOctoPrint.current.systemInfo ?: SystemInfo()
    val androidMenuHost = LocalAndroidMenuHost.current
    val viewModel = viewModel(
        modelClass = MenuHostViewModel::class.java,
        key = rememberSaveable { uuid4().toString() }
    )

    val host = remember(viewModel) {
        object : MenuHost, AndroidMenuHost by androidMenuHost {
            override suspend fun closeMenu(result: String?) {
                onCloseMenu(result)
            }

            override fun openUrl(url: Url) {
                onOpenLink(Uri.parse(url.toString()))
            }

            override suspend fun reloadMenu() {
                viewModel.reloadMenu(context, this, systemInfo)
            }

            override suspend fun pushMenu(subMenu: Menu) {
                viewModel.menuStack += subMenu
                viewModel.loadMenu(subMenu, context, this, systemInfo)
            }

            override suspend fun popMenu() {
                if (viewModel.menuStack.size > 1) {
                    viewModel.menuStack.removeLast()
                    viewModel.loadMenu(viewModel.menuStack.last(), context, this, systemInfo)
                } else {
                    closeMenu()
                }
            }
        }
    }

    val hostState = object : MenuHostState {
        override val menu @Composable get() = viewModel.currentMenu.collectAsState().value
        override val canNavigateUp get() = viewModel.menuStack.size > 1
        override val menuId get() = menuId
        override val host get() = host

        override suspend fun navigateUp() = if (viewModel.popMenu(context, host, systemInfo)) {
            Unit
        } else try {
            onCloseMenu(null)
        } catch (e: Exception) {
            Napier.e(tag = TAG, throwable = e, message = "Failed to close menu")
        }
    }

    LaunchedEffect(Unit) {
        try {
            if (viewModel.menuStack.isEmpty()) {
                host.pushMenu(menu)
            }
        } catch (e: Exception) {
            Napier.e(tag = "MenuHost", message = "Failed to load menu")
            delay(0.5.seconds)
            hostState.navigateUp()
            ExceptionReceivers.dispatchException(e)
        }
    }

    return hostState
}

class MenuHostViewModel : ViewModel() {
    val menuStack = mutableStateListOf<Menu>()
    private val preparedCache = mutableMapOf<Menu?, PreparedMenu>()
    private val reloadTrigger = MutableStateFlow(0)
    val currentMenu = MutableStateFlow<PreparedMenu?>(null)
    private var loadJob: Job? = null

    init {
        viewModelScope.launch {
            currentMenu.filterNotNull().collectLatest {
                preparedCache[it.raw] = it
            }
        }
    }

    override fun onCleared() {
        loadJob?.cancel("ViewModel cleared")
        menuStack.forEach { it.onDestroy() }
        super.onCleared()
    }

    suspend fun reloadMenu(context: MenuContext, host: MenuHost, systemInfo: SystemInfo) {
        val new = currentMenu.value?.raw?.prepare(host, context, systemInfo)
        currentMenu.value = new
    }

    suspend fun loadMenu(menu: Menu, context: MenuContext, host: MenuHost, systemInfo: SystemInfo) {
        // "Blockingly" collect first, throws any exceptions
        currentMenu.value = menu.prepare(menuHost = host, menuContext = context, systemInfo = systemInfo)
    }

    fun popMenu(context: MenuContext, host: MenuHost, systemInfo: SystemInfo): Boolean = if (menuStack.size > 1) {
        // Remove last
        val removed = menuStack.removeLast()
        preparedCache.remove(removed)
        removed.onDestroy()

        // Restore previous
        val previous = menuStack.last()
        currentMenu.value = preparedCache[previous] ?: let {
            Napier.e(tag = TAG, message = "Failed", throwable = IllegalStateException("Missing prepared cache on popMenu: $menuStack -> ${preparedCache.keys}"))
            return false
        }

        // Trigger reload async
        viewModelScope.launch {
            try {
                reloadMenu(context, host, systemInfo)
            } catch (e: Exception) {
                Napier.e(tag = TAG, throwable = e, message = "Silent reload failed")
            }
        }

        true
    } else {
        false
    }
}

@Composable
fun MenuHost(
    state: MenuHostState,
    modifier: Modifier = Modifier
) = state.menu?.let { menu ->
    MenuList(
        menu = menu,
        menuHost = state.host,
        menuId = state.menuId,
        horizontalMargin = OctoAppTheme.dimens.margin2,
        modifier = modifier
            .animateContentSize(spring())
            .heightIn(min = 100.dp),
    )
}

//region Preview
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    Box(modifier = Modifier.height(800.dp)) {
        MenuHost(
            menu = PreviewMenuItems.MenuWithNavigation,
            menuId = MenuId.Other,
            onCloseMenu = {},
            modifier = Modifier.border(1.dp, Color.Red)
        )
    }
}
//endregion