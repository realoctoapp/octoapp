package de.crysxd.baseui.compose.framework.components

import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GenericDownloadUseCase
import io.github.aakira.napier.Napier
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.sync.withPermit
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import android.graphics.Color as AndroidColor

// Limit to 5 requests at a time
private const val MaxCacheSize = 20 * 1024 * 1024
private val RequestSemaphore = Semaphore(permits = 5)
private val CacheLock = Mutex()
private val Cache = mutableMapOf<String, CacheEntry>()

private data class CacheEntry(
    val lastAccessedAt: Instant,
    val image: ImageBitmap,
)

@Composable
fun PicassoImage(
    modifier: Modifier = Modifier,
    @DrawableRes fallback: Int = R.drawable.ic_round_image_not_supported_24,
    contentScale: (Boolean) -> ContentScale = { hasAlpha -> if (hasAlpha) ContentScale.FillBounds else ContentScale.Crop },
    fallbackContentScale: ContentScale = ContentScale.Inside,
    paddingIfAlpha: Dp = 5.dp,
    url: String?,
) {
    // ⚠️ This code is run without LocalOctoPrint.current being set!
    val (picassoImage, isFallback) = rememberOctoPicassoImage(
        imageSource = ImageSource.Url(url),
        fallback = fallback
    ).value

    fun ImageBitmap.hasAlpha() = asAndroidBitmap().let {
        (0..<width).count { x -> AndroidColor.alpha(it.getPixel(x, 0)) != 255 } > width / 2
    }

    //region Display
    Crossfade(
        targetState = picassoImage,
        modifier = modifier,
        label = "cross",
    ) { image ->
        Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
            val hasAlpha = remember(image) { image?.hasAlpha() ?: false }

            when (image) {
                null -> CircularProgressIndicator(
                    modifier = Modifier.scale(0.6f)
                )

                else -> Image(
                    bitmap = image,
                    contentDescription = null,
                    contentScale = if (isFallback) fallbackContentScale else contentScale(hasAlpha),
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(if (hasAlpha) paddingIfAlpha else 0.dp)
                )
            }
        }
    }
    //endregion
}

@Composable
fun OctoPicassoImage(
    modifier: Modifier = Modifier,
    @DrawableRes fallback: Int = R.drawable.ic_round_image_not_supported_24,
    contentScale: (Boolean) -> ContentScale = { hasAlpha -> if (hasAlpha) ContentScale.FillBounds else ContentScale.Crop },
    fallbackContentScale: ContentScale = ContentScale.Inside,
    paddingIfAlpha: Dp = 5.dp,
    path: String?,
) {
    val (picassoImage, isFallback) = rememberOctoPicassoImage(
        imageSource = ImageSource.Path(instanceId = LocalOctoPrint.current.id, path = path),
        fallback = fallback
    ).value

    fun ImageBitmap.hasAlpha() = asAndroidBitmap().let {
        (0..<width).count { x -> AndroidColor.alpha(it.getPixel(x, 0)) != 255 } > width / 2
    }

    //region Display
    Crossfade(
        targetState = picassoImage,
        modifier = modifier,
        label = "cross",
    ) { image ->
        Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxSize()) {
            val hasAlpha = remember(image) { image?.hasAlpha() ?: false }

            when (image) {
                null -> CircularProgressIndicator(
                    modifier = Modifier.scale(0.6f)
                )

                else -> Image(
                    bitmap = image,
                    contentDescription = null,
                    contentScale = if (isFallback) fallbackContentScale else contentScale(hasAlpha),
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(if (hasAlpha) paddingIfAlpha else 0.dp)
                )
            }
        }
    }
    //endregion
}

private sealed class ImageSource {
    data class Path(val instanceId: String, val path: String?) : ImageSource()
    data class Url(val url: String?) : ImageSource()
}

@Composable
private fun rememberOctoPicassoImage(
    imageSource: ImageSource,
    @DrawableRes fallback: Int = R.drawable.ic_round_image_not_supported_24,
    placeholderTint: Color = OctoAppTheme.colors.accent,
): MutableState<Pair<ImageBitmap?, Boolean>> {
    val image = remember { mutableStateOf<Pair<ImageBitmap?, Boolean>>(null to false) }
    val context = LocalContext.current
    val isPreview = OctoAppTheme.isPreview
    fun Drawable.tintedImageBitmap() = apply { setTint(placeholderTint.toArgb()) }.toBitmap().asImageBitmap()

    //region "Picasso" (not anymore)
    LaunchedEffect(imageSource) {
        val default = { ContextCompat.getDrawable(context, fallback)?.tintedImageBitmap() to true }

        image.value = try {
            val cacheKey = when (imageSource) {
                is ImageSource.Path -> "${imageSource.instanceId}/${imageSource.path}"
                is ImageSource.Url -> imageSource.url ?: "null-url"
            }

            CacheLock.withLock {
                Cache[cacheKey]?.let {
                    Napier.v(tag = "PicassoImage", message = "Cache hit: $cacheKey")
                    image.value = it.image to false
                    Cache[cacheKey] = it.copy(lastAccessedAt = Clock.System.now())
                    return@LaunchedEffect
                }
            }

            Napier.v(tag = "PicassoImage", message = "Cache miss: $cacheKey")

            when {
                isPreview -> default()

                imageSource is ImageSource.Path -> if (imageSource.path == null) {
                    default()
                } else {
                    RequestSemaphore.withPermit {
                        SharedBaseInjector.get().genericDownloadUseCase().execute(
                            param = GenericDownloadUseCase.Params.FromPrinter(
                                path = imageSource.path,
                                instanceId = imageSource.instanceId,
                                pathEncoded = true,
                            )
                        ).let { bytes ->
                            BitmapFactory.decodeByteArray(bytes, 0, bytes.size)?.let { it.asImageBitmap() to false } ?: default()
                        }
                    }
                }

                imageSource is ImageSource.Url -> if (imageSource.url == null) {
                    default()
                } else {
                    RequestSemaphore.withPermit {
                        SharedBaseInjector.get().genericDownloadUseCase().execute(
                            param = GenericDownloadUseCase.Params.FromPublicWeb(
                                url = imageSource.url,
                            )
                        ).let { it ->
                            BitmapFactory.decodeByteArray(it, 0, it.size)?.let { it.asImageBitmap() to false } ?: default()
                        }
                    }
                }

                else -> default()
            }.also { result ->
                val imageToCache = result.first
                if (!result.second && imageToCache != null) {
                    CacheLock.withLock {
                        Cache.entries.sortedBy { it.value.lastAccessedAt }.take((Cache.size - MaxCacheSize).coerceAtLeast(0)).forEach { (key, _) ->
                            Cache.remove(key)
                        }
                        Cache[cacheKey] = CacheEntry(lastAccessedAt = Clock.System.now(), image = imageToCache)
                    }
                }
            }
        } catch (e: Exception) {
            Napier.e(tag = "PicassoImage", message = "Failed to load image: $imageSource", throwable = e)
            default()
        }
    }
    //endregion

    return image
}
