package de.crysxd.baseui.compose.controls.saveconfig

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.viewmodels.SaveConfigControlsViewModelCore
import kotlinx.coroutines.flow.map

class SaveConfigViewModel(
    private val instanceId: String,
) : ViewModelWithCore() {

    override val core by lazy { SaveConfigControlsViewModelCore(instanceId) }
    val saveConfigPending by lazy { core.state.map { it.saveConfigPending } }

    suspend fun saveConfig() = core.saveConfig()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = SaveConfigViewModel(
            instanceId = instanceId,
        ) as T
    }
}