package de.crysxd.baseui.compose.menu

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.models.MenuContext
import kotlinx.coroutines.flow.map


@Composable
fun rememberMenuContext() = if (OctoAppTheme.isPreview) {
    MenuContext.Prepare
} else {
    val id = LocalOctoPrint.current.id
    val initial = remember(id) {
        SharedBaseInjector.get()
            .printerEngineProvider
            .getLastCurrentMessage(instanceId = id)
            ?.let { MenuContext.fromFlags(it.state.flags) }
            ?: MenuContext.Connect
    }
    val context = remember(id) {
        SharedBaseInjector.get()
            .printerEngineProvider
            .passiveCurrentMessageFlow(tag = "MenuContext", instanceId = id)
            .map { MenuContext.fromFlags(it.state.flags) }
    }.collectAsState(initial = initial)
    context.value
}
