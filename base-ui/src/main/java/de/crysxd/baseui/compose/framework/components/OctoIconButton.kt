package de.crysxd.baseui.compose.framework.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.rememberErrorDialog
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun OctoIconButton(
    painter: Painter,
    contentDescription: String?,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    boundedRipple: Boolean = false,
    usePadding: Boolean = true,
    loading: Boolean = false,
    tint: Color = OctoAppTheme.colors.accent,
    onClick: suspend () -> Unit,
) {
    val scope = rememberCoroutineScope()
    val errorDialog = rememberErrorDialog()
    var internalLoading by remember { mutableStateOf(false) }
    val finalLoading = internalLoading || loading
    val iconAlpha by animateFloatAsState(targetValue = if (finalLoading) 0f else 1f, label = "iconAlpha")
    val loadingVisible by remember { derivedStateOf { iconAlpha < 1 } }
    val enabledAlpha by animateFloatAsState(targetValue = if (enabled) 1f else 0.33f, label = "enabledAlpha")

    Box(
        modifier = modifier
            .graphicsLayer { alpha = enabledAlpha }
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = if (boundedRipple) {
                    rememberRipple(bounded = false, color = tint, radius = 30.dp)
                } else {
                    rememberRipple(bounded = false, color = tint)
                },
                enabled = enabled && !finalLoading,
                onClick = {
                    scope.launch {
                        try {
                            internalLoading = true
                            errorDialog.runWithErrorDialog { onClick() }
                        } finally {
                            internalLoading = false
                        }
                    }
                },
            )
            .padding(if (usePadding) OctoAppTheme.dimens.margin1 else 0.dp),
        contentAlignment = Alignment.Center,
    ) {
        Icon(
            painter = painter,
            contentDescription = contentDescription,
            tint = tint,
            modifier = Modifier.graphicsLayer { alpha = iconAlpha }
        )

        AnimatedVisibility(visible = loadingVisible) {
            CircularProgressIndicator(
                strokeWidth = 3.dp,
                color = tint,
                modifier = Modifier
                    .graphicsLayer { alpha = 1 - iconAlpha }
                    .size(22.dp)
                    .scale(0.8f),
            )
        }
    }
}

//region Preview
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    OctoIconButton(
        painter = painterResource(id = R.drawable.ic_round_refresh_24),
        contentDescription = null,
        onClick = { delay(1000) },
    )
}
//endregion