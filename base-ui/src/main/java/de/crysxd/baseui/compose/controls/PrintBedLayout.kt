package de.crysxd.baseui.compose.controls

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import de.crysxd.baseui.compose.controls.gcodepreview.GcodeNotLiveIfNoUpdateForMs
import de.crysxd.baseui.compose.framework.components.OctoLiveBadge
import de.crysxd.baseui.compose.framework.helpers.GcodeRenderCanvas
import de.crysxd.baseui.compose.framework.helpers.rememberValue
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.R
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.gcode.GcodeNativeCanvas
import de.crysxd.octoapp.base.gcode.GcodeRenderer
import de.crysxd.octoapp.base.models.GcodeRenderContext
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.viewmodels.BedMeshControlsViewModelCore.State.Hidden.printerProfile
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun PrintBedLayout(
    showLive: Boolean,
    data: () -> PrintBedData,
    modifier: Modifier = Modifier,
    bottomEndDecoration: (@Composable () -> Unit)? = null,
    legend: @Composable (Modifier) -> Unit
) = Box(
    modifier = modifier.fillMaxHeight(),
    contentAlignment = Alignment.Center
) {
    var isLive by remember { mutableStateOf(false) }

    OctoLiveBadge(
        visible = isLive,
        modifier = Modifier
            .align(Alignment.TopEnd)
            .padding(OctoAppTheme.dimens.margin1)
    )

    bottomEndDecoration?.let {
        Box(modifier = Modifier.align(Alignment.BottomEnd)) {
            bottomEndDecoration()
        }
    }

    Row(verticalAlignment = Alignment.CenterVertically) {
        val previewWeight = 0.55f
        GcodeControlsReadyRender(
            modifier = Modifier.weight(previewWeight),
            data = data
        ) {
            isLive = it && showLive
        }

        legend(Modifier.weight(1 - previewWeight))
    }
}


@Composable
private fun GcodeControlsReadyRender(
    modifier: Modifier = Modifier,
    data: () -> PrintBedData,
    updateLive: (Boolean) -> Unit,
) {
    val scope = rememberCoroutineScope()
    val context = LocalContext.current
    val async = !OctoAppTheme.isPreview
    val padding = OctoAppTheme.dimens.margin2
    val liveJob = rememberValue<Job?>(null)
    val aspectRatio by remember {
        derivedStateOf {
            data().printerProfile?.volume?.run { width / depth } ?: 1f
        }
    }

    GcodeRenderCanvas(
        modifier = modifier
            .aspectRatio(aspectRatio)
            .fillMaxHeight(),
        contentPadding = PaddingValues(padding),
        asyncRender = async,
        acceptTouchInput = false,
        nativeOverlay = { data().plugin?.invoke(this) },
        renderParams = {
            val currentData = data()

            updateLive(currentData.isTrackingLive)
            liveJob.value?.cancel()
            liveJob.value = scope.launch {
                delay(GcodeNotLiveIfNoUpdateForMs)
                updateLive(false)
            }


            currentData.renderContext?.let { value ->
                GcodeRenderer.RenderParams(
                    renderContext = value,
                    printBed = currentData.printBed,
                    printBedWidthMm = currentData.printerProfile?.volume?.width ?: printerProfile.volume.width,
                    printBedHeightMm = currentData.printerProfile?.volume?.depth ?: printerProfile.volume.depth,
                    extrusionWidthMm = currentData.printerProfile?.estimatedNozzleDiameter ?: printerProfile.estimatedNozzleDiameter,
                    minPrintHeadDiameterPx = context.resources.getDimension(R.dimen.gcode_render_view_print_head_size),
                    originInCenter = (currentData.printerProfile?.volume?.origin ?: printerProfile.volume.origin) == PrinterProfile.Origin.Center,
                    quality = currentData.settings.quality
                )
            }
        }
    )
}

data class PrintBedData(
    val renderContext: GcodeRenderContext?,
    val printerProfile: PrinterProfile?,
    val settings: GcodePreviewSettings,
    val isTrackingLive: Boolean,
    val plugin: (GcodeNativeCanvas.() -> Unit)?,
    val printBed: GcodeNativeCanvas.Image,
)