package de.crysxd.baseui.compose.screens.help

import android.net.Uri
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.viewmodels.HelpLauncherViewModelCore

class HelpLauncherViewModel : ViewModelWithCore() {
    override val core by lazy { HelpLauncherViewModelCore() }
    val knownBugs get() = core.knownBugs
    val faq get() = core.faq
    val introUrl get() = Uri.parse(core.introUrl)
}