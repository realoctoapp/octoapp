package de.crysxd.baseui.compose.framework.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.options
import de.crysxd.octoapp.menu.InputMenuItem
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

val LocalEnterValueProvider = compositionLocalOf<EnterValueProvider> { throw IllegalStateException("No EnterValueProvider available") }

interface EnterValueProvider {
    suspend fun requestEnterValues(
        title: String,
        confirmButton: String,
        inputs: List<EnterValueDialogInputConfig>,
    ): List<String>?
}

@Composable
fun rememberEnterValueDialogProvider(): EnterValueProvider {
    var data by remember { mutableStateOf<Triple<String, String, List<EnterValueDialogInputConfig>>?>(null) }
    val resultFlow = remember { MutableSharedFlow<List<String>?>() }
    val scope = rememberCoroutineScope()

    fun sendResult(result: List<String>? = null) = scope.launch {
        resultFlow.emit(result)
    }

    data?.let { (title, confirmButton, inputs) ->
        EnterValueDialog(
            *inputs.toTypedArray(),
            title = title,
            confirmButton = confirmButton,
            onDismissRequest = {
                sendResult(null)
                data = null
            },
            onDone = { result ->
                sendResult(result)
                data = null
            }
        )
    }

    return remember {
        object : EnterValueProvider {
            override suspend fun requestEnterValues(title: String, confirmButton: String, inputs: List<EnterValueDialogInputConfig>): List<String>? {
                data = Triple(title, confirmButton, inputs)
                return resultFlow.first()
            }
        }
    }
}

@Composable
fun EnterValueDialog(
    vararg inputs: EnterValueDialogInputConfig,
    title: String,
    confirmButton: String,
    onDismissRequest: () -> Unit,
    onDone: suspend (List<String>) -> Unit
) = Dialog(
    onDismissRequest = onDismissRequest,
) {
    EnterValueView(
        *inputs,
        title = title,
        confirmButton = confirmButton,
        onDone = onDone,
        modifier = Modifier
            .shadow(elevation = 10.dp, shape = MaterialTheme.shapes.large)
            .background(color = OctoAppTheme.colors.windowBackground, shape = MaterialTheme.shapes.large)
            .padding(OctoAppTheme.dimens.margin2)
    )
}

@Composable
fun EnterValueView(
    vararg inputs: EnterValueDialogInputConfig,
    title: String?,
    confirmButton: String,
    modifier: Modifier = Modifier,
    onDone: suspend (List<String>) -> Unit
) = Column(modifier = modifier) {
    val activity = OctoAppTheme.octoActivity
    val scope = rememberCoroutineScope()
    var loading by remember { mutableStateOf(false) }
    val inputValues = remember(inputs) {
        inputs.map { mutableStateOf(TextFieldValue(it.value, selection = TextRange(it.value.length))) }
    }

    suspend fun performChecks() = try {
        loading = true
        inputs.mapIndexed { index, config ->
            config to inputValues[index].value.text
        }.firstNotNullOfOrNull { (config, text) ->
            config.validator(text)
        }?.let { error ->
            activity?.showDialog(OctoActivity.Message.DialogMessage(text = { error }))
        } ?: onDone(inputs.mapIndexed { index, _ -> inputValues[index].value.text })
    } finally {
        loading = false
    }

    title?.let {
        Text(
            text = it,
            style = OctoAppTheme.typography.title,
            color = OctoAppTheme.colors.darkText,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(bottom = OctoAppTheme.dimens.margin2)
                .fillMaxWidth()
        )
    }

    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12),
        modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin2)
    ) {
        inputs.forEachIndexed { index, config ->
            EnterValueDialogInput(
                config = config,
                value = inputValues[index],
                first = index == 0,
                last = (index + 1) == inputs.size,
                triggerDone = {
                    scope.launch {
                        try {
                            performChecks()
                        } catch (e: Exception) {
                            activity?.showDialog(e)
                            Napier.e(tag = "EnterValueDialog", message = "Failed to perform checks", throwable = e)
                        }
                    }
                }
            )
        }
    }

    OctoButton(
        text = confirmButton,
        onClick = { performChecks() },
        loading = loading,
    )
}

@Composable
private fun EnterValueDialogInput(
    config: EnterValueDialogInputConfig,
    value: MutableState<TextFieldValue>,
    first: Boolean,
    last: Boolean,
    triggerDone: () -> Unit,
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }

    OctoInputField(
        value = value.value,
        onValueChanged = { value.value = it },
        placeholder = config.label,
        label = config.labelActive,
        labelActive = config.labelActive,
        keyboardOptions = config.keyboard.options.copy(
            imeAction = if (last) ImeAction.Done else ImeAction.Next
        ),
        keyboardActions = KeyboardActions(
            onNext = { focusManager.moveFocus(FocusDirection.Next) },
            onDone = { triggerDone() }
        ),
        modifier = Modifier.focusRequester(focusRequester)
    )

    LaunchedEffect(Unit) {
        if (first) {
            focusRequester.requestFocus()
        }
    }
}

data class EnterValueDialogInputConfig(
    val label: String,
    val labelActive: String,
    val validator: suspend (String) -> String?,
    val value: String,
    val keyboard: InputMenuItem.KeyboardType,
)