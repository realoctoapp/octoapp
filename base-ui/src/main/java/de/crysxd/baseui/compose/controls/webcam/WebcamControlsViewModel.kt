package de.crysxd.baseui.compose.controls.webcam

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.ScaleType
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.shareIn

class WebcamControlsViewModel(
    private val instanceId: String,
) : ViewModelWithCore() {


    private val preferences = SharedBaseInjector.get().preferences
    override val core by lazy { WebcamControlsViewModelCore(instanceId) }
    val state by lazy { core.state.shareIn(viewModelScope, SharingStarted.WhileSubscribedOctoDelay) }
    val initialShowWebcamName get() = preferences.isShowWebcamName
    val initialShowWebcamResolution get() = preferences.isShowWebcamName
    val showWebcamName get() = preferences.updatedFlow2.map { it.isShowWebcamName }
    val showResolution get() = preferences.updatedFlow2.map { it.isShowWebcamResolution }
    val configAspectRatio
        get() = core.configAspectRatio.combine(preferences.updatedFlow2) { ratio, prefs ->
            ratio.takeIf { prefs.webcamAspectRatioSource == OctoPreferences.VALUE_WEBCAM_ASPECT_RATIO_SOURCE_OCTOPRINT }
        }

    fun nextWebcam() = core.nextWebcam()

    fun retry() = core.retry()

    fun getInitialAspectRatio() = core.getInitialAspectRatio()

    fun storeAspectRatio(aspectRatio: Float) = core.storeAspectRatio(aspectRatio)

    fun storeScaleType(isFullscreen: Boolean, scaleType: ScaleType) = core.storeScaleType(isFullscreen, scaleType)

    fun getScaleType(isFullscreen: Boolean, default: ScaleType) = core.getScaleType(isFullscreen, default)

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = WebcamControlsViewModel(
            instanceId = instanceId,
        ) as T
    }
}

