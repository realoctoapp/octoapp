package de.crysxd.baseui.compose.screens.terminal

import androidx.annotation.DrawableRes
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedContentTransitionScope.SlideDirection.Companion.End
import androidx.compose.animation.AnimatedContentTransitionScope.SlideDirection.Companion.Start
import androidx.compose.animation.Crossfade
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.components.OctoInputField
import de.crysxd.baseui.compose.framework.components.OctoPinneableButton
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.rememberErrorDialog
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.LocalLeftyMode
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.menu.controls.CustomizeGcodeShortcutMenu
import de.crysxd.octoapp.menu.terminal.TerminalFilterMenu
import kotlinx.coroutines.launch


@Composable
internal fun TerminalInput(
    modifier: Modifier = Modifier,
    inputEnabled: Boolean,
    shortcuts: List<GcodeHistoryItem>,
    onExecuteGcode: suspend (String) -> Unit,
    onToggleStyled: suspend () -> Unit,
    onClear: suspend () -> Unit,
    isStyled: Boolean,
    filterCount: Int,
) = Crossfade(
    targetState = inputEnabled,
    label = "inputEnabled",
    modifier = modifier,
) { enabled ->
    Box(
        contentAlignment = Alignment.Center,
    ) {
        val overlayColor = OctoAppTheme.colors.windowBackground.copy(alpha = 0.4f)
        val errorDialog = rememberErrorDialog()
        Column(
            modifier = Modifier
                .blur(if (enabled) 0.dp else 15.dp)
                .drawWithContent {
                    drawContent()
                    if (!enabled) {
                        drawRect(overlayColor)
                    }
                }
        ) {
            var input by remember { mutableStateOf("") }
            val scope = rememberCoroutineScope()

            Shortcuts(
                shortcuts = shortcuts,
                modifier = Modifier.fillMaxWidth(),
                onExecuteGcode = onExecuteGcode,
                onToggleStyled = onToggleStyled,
                onClear = onClear,
                isStyled = isStyled,
                filterCount = filterCount,
                onInsert = { input = it }
            )

            InputField(
                modifier = Modifier.fillMaxWidth(),
                value = { input },
                onValueChanged = { input = it },
                onExecuteGcode = {
                    scope.launch {
                        errorDialog.runWithErrorDialog {
                            onExecuteGcode(it)
                            input = ""
                        }
                    }
                },
                executing = { false }
            )
        }

        if (!enabled) {
            Text(
                text = stringResource(R.string.terminal___pause_the_print_to_send_commands),
                style = OctoAppTheme.typography.subtitle,
                color = OctoAppTheme.colors.darkText,
                modifier = Modifier.clickable { /* Capture clicks */ }
            )
        }
    }
}

@Composable
private fun InputField(
    modifier: Modifier = Modifier,
    value: () -> String,
    onValueChanged: (String) -> Unit,
    onExecuteGcode: (String) -> Unit,
    executing: () -> Boolean,
) = OctoInputField(
    modifier = modifier.padding(OctoAppTheme.dimens.margin2),
    value = value(),
    onValueChanged = onValueChanged,
    placeholder = stringResource(R.string.terminal___input_placeholder_active),
    label = stringResource(R.string.terminal___input_placeholder),
    labelActive = stringResource(R.string.terminal___input_placeholder),
    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Send, keyboardType = KeyboardType.Ascii),
    keyboardActions = KeyboardActions(onSend = { onExecuteGcode(value()) }),
) {
    AnimatedContent(
        targetState = value().isNotEmpty() to executing(),
        contentAlignment = Alignment.Center,
        label = "executeButton",
        transitionSpec = { (fadeIn() + slideIntoContainer(towards = Start)) togetherWith (fadeOut() + slideOutOfContainer(towards = End)) },
        modifier = Modifier.size(48.dp, 48.dp)
    ) { (ready, executing) ->
        if (ready || executing) {
            OctoIconButton(
                painter = painterResource(R.drawable.ic_round_send_24),
                contentDescription = stringResource(R.string.cd_execute),
                onClick = { onExecuteGcode(value()) },
                loading = executing
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun Shortcuts(
    modifier: Modifier = Modifier,
    shortcuts: List<GcodeHistoryItem>,
    onExecuteGcode: suspend (String) -> Unit,
    onToggleStyled: suspend () -> Unit,
    onClear: suspend () -> Unit,
    onInsert: (String) -> Unit,
    isStyled: Boolean,
    filterCount: Int,
) {
    val originalDirection = LocalLayoutDirection.current
    val alteredDirection = if (LocalLeftyMode.current) LayoutDirection.Ltr else LayoutDirection.Rtl
    CompositionLocalProvider(LocalLayoutDirection provides alteredDirection) {
        LazyRow(
            modifier = modifier,
            contentPadding = PaddingValues(start = OctoAppTheme.dimens.margin2, top = OctoAppTheme.dimens.margin2, end = OctoAppTheme.dimens.margin2),
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
        ) {
            //region Filter
            item(key = "filter") {
                CompositionLocalProvider(LocalLayoutDirection provides originalDirection) {
                    var showFilters by remember { mutableStateOf(false) }
                    if (showFilters) {
                        MenuSheet(menu = TerminalFilterMenu(instanceId = LocalOctoPrint.current.id)) {
                            showFilters = false
                        }
                    }

                    SpecialShortcut(
                        icon = R.drawable.ic_round_filter_alt_24,
                        onClick = { showFilters = true },
                        text = filterCount.toString()
                    )
                }
            }
            //endregion
            //region Style
            item(key = "style") {
                CompositionLocalProvider(LocalLayoutDirection provides originalDirection) {
                    SpecialShortcut(
                        icon = if (isStyled) R.drawable.ic_round_code_24 else R.drawable.ic_round_brush_24,
                        onClick = onToggleStyled,
                    )
                }
            }
            //endregion
            //region Clear
            item(key = "clear") {
                CompositionLocalProvider(LocalLayoutDirection provides originalDirection) {
                    SpecialShortcut(
                        icon = R.drawable.ic_round_clear_all_24,
                        onClick = onClear,
                    )
                }
            }
            //endregion
            //region History items
            items(
                count = shortcuts.size,
                key = { shortcuts[it].command.hashCode() }
            ) {
                CompositionLocalProvider(LocalLayoutDirection provides originalDirection) {
                    Shortcut(
                        shortcut = shortcuts[it],
                        onExecuted = { onExecuteGcode(shortcuts[it].command) },
                        modifier = Modifier.animateItemPlacement(),
                        onInsert = onInsert,
                    )
                }
            }
            //endregion
        }
    }
}

@Composable
private fun SpecialShortcut(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int,
    onClick: suspend () -> Unit,
    text: String? = null,
) = OctoButton(
    modifier = modifier,
    onClick = onClick,
    type = OctoButtonType.Primary,
    small = true,
    content = {
        Row(
            modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin12),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = icon),
                contentDescription = null,
            )

            text?.let {
                Text(it)
            }
        }
    }
)

@Composable
private fun Shortcut(
    modifier: Modifier = Modifier,
    shortcut: GcodeHistoryItem,
    onExecuted: suspend () -> Unit,
    onInsert: (String) -> Unit,
) {
    var showMenu by remember { mutableStateOf(false) }
    if (showMenu) {
        MenuSheet(
            menu = CustomizeGcodeShortcutMenu(shortcut.command, offerInsert = true),
            onResult = { it?.let(onInsert) },
        ) {
            showMenu = false
        }
    }

    OctoPinneableButton(
        modifier = modifier,
        text = shortcut.name.take(48),
        onClick = onExecuted,
        onLongClick = { showMenu = true },
        type = OctoButtonType.Secondary,
        pinned = shortcut.isFavorite,
    )
}
