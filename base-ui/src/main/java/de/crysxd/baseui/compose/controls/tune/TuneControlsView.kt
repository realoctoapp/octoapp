package de.crysxd.baseui.compose.controls.tune


import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.LargeAnnouncement
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoDialog
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.components.OctoInputField
import de.crysxd.baseui.compose.framework.components.OctoTabRow
import de.crysxd.baseui.compose.framework.layout.HeaderSynchronizedLazyColumn
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.tune.TuneState
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.absoluteValue
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@Composable
fun TuneControlsView(
    tuneState: TuneState,
    onChangeOffset: suspend (Float) -> Unit,
    onApplyValues: suspend (Float?, Float?, Map<String, Float?>) -> Unit,
    onResetOffset: suspend () -> Unit,
    onSaveOffsetToConfig: suspend () -> Unit,
) {
    val scope = rememberCoroutineScope()

    HeaderSynchronizedLazyColumn(
        verticalArrangement = arrangement,
    ) {


        if (tuneState.isValuesImprecise) {
            disclaimer()
        }

        values(
            tuneState = tuneState,
            onApplyValues = onApplyValues,
        )

        zOffset(
            offsetMm = tuneState.offsets?.z,
            changeOffset = {
                // Detach from button so multiple quick clicks all register
                scope.launch {
                    onChangeOffset(it)
                }
            },
            resetOffset = onResetOffset,
            saveOffsetToConfig = onSaveOffsetToConfig,
        )
    }
}

//region Internals
private val arrangement @Composable get() = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)

private fun LazyListScope.disclaimer() = item("disclaimer") {
    OctoAnnouncementController(announcementId = AnnouncementId.Default("tune_data_disclaimer")) {
        LargeAnnouncement(
            title = stringResource(id = R.string.tune_print___disclaimer_title),
            text = stringResource(id = R.string.tune_print___disclaimer_detail),
            hideButton = stringResource(id = R.string.tune_print___disclaimer_action_hide),
        )
    }
}

private fun LazyListScope.values(
    tuneState: TuneState,
    onApplyValues: suspend (Float?, Float?, Map<String, Float?>) -> Unit,
) = item("rates") {
    //region State
    val currentFlowRate = tuneState.print?.flowRate?.roundToInt()
    val currentFeedRate = tuneState.print?.feedRate?.roundToInt()
    val currentFanSpeeds = tuneState.fans.associate { it.component to it.speed?.roundToInt() }
    var feedRateInput by remember { mutableStateOf(TextFieldValue()) }
    var flowRateInput by remember { mutableStateOf(TextFieldValue()) }
    val fanSpeedInputs = remember { mutableStateMapOf<String, TextFieldValue>() }
    var loading by remember { mutableStateOf(false) }
    val allSynced = (listOf(
        flowRateInput.text == (currentFlowRate?.toString() ?: ""),
        feedRateInput.text == (currentFeedRate?.toString() ?: "")
    ) + currentFanSpeeds.map { (fan, value) ->
        fanSpeedInputs[fan]?.text == (value?.toString() ?: "")
    }).all { it }
    var showApplyButton by remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val apply = suspend {
        try {
            loading = true
            onApplyValues(
                feedRateInput.text.toFloatOrNull(),
                flowRateInput.text.toFloatOrNull(),
                fanSpeedInputs.mapValues { (_, value) -> value.text.toFloatOrNull() },
            )
        } finally {
            loading = false
        }
    }
    val applyTrigger: () -> Unit = {
        scope.launch {
            apply()
        }
    }
    LaunchedEffect(allSynced) {
        // Debounce in case values change
        showApplyButton = if (allSynced) {
            false
        } else {
            delay(0.5.seconds)
            true
        }
    }
    //endregion

    Column(
        verticalArrangement = arrangement,
        modifier = Modifier
            .background(OctoAppTheme.colors.inputBackground, shape = MaterialTheme.shapes.large)
            .padding(OctoAppTheme.dimens.margin1)
    ) {
        //region Speed + Flow
        Row(horizontalArrangement = arrangement) {
            TextInput(
                currentValue = currentFeedRate,
                label = stringResource(id = R.string.tune_print___feed_rate_label),
                labelActive = stringResource(id = R.string.tune_print___feed_rate_label_active),
                onApplyValue = applyTrigger,
                onValueChanged = { feedRateInput = it },
                value = feedRateInput,
                modifier = Modifier.weight(1f),
            )

            TextInput(
                currentValue = currentFlowRate,
                label = stringResource(id = R.string.tune_print___flow_rate_label),
                labelActive = stringResource(id = R.string.tune_print___flow_rate_label_active),
                onApplyValue = applyTrigger,
                onValueChanged = { flowRateInput = it },
                value = flowRateInput,
                modifier = Modifier.weight(1f),
            )
        }
        //endregion
        //region Fans
        tuneState.fans.chunked(2).forEach { fans ->
            Row(horizontalArrangement = arrangement) {
                fans.forEach { fan ->
                    val label = fan.label + " (%)"
                    TextInput(
                        currentValue = currentFanSpeeds[fan.component],
                        label = label,
                        labelActive = label,
                        onApplyValue = applyTrigger,
                        onValueChanged = { fanSpeedInputs[fan.component] = it },
                        value = fanSpeedInputs[fan.component] ?: TextFieldValue(),
                        modifier = Modifier.weight(1f),
                    )
                }
            }
        }
        //endregion
        //region Button
        AnimatedVisibility(visible = showApplyButton) {
            val focusManager = LocalFocusManager.current
            OctoButton(
                text = stringResource(id = R.string.tune_print___apply),
                onClick = {
                    applyTrigger()
                    focusManager.clearFocus()
                },
                loading = loading,
                successAnimation = true,
                small = true,
                modifier = Modifier.fillMaxWidth(),
            )
        }
        //endregion
    }
}

private fun LazyListScope.zOffset(
    offsetMm: Float?,
    changeOffset: suspend (Float) -> Unit,
    resetOffset: suspend () -> Unit,
    saveOffsetToConfig: suspend () -> Unit,
) = item("babyStep") {
    Column {
        BabyStepControls(
            offsetMm = offsetMm,
            changeOffset = changeOffset,
            resetOffset = resetOffset,
            saveToConfig = saveOffsetToConfig,
        )

        Text(
            text = stringResource(id = R.string.tune_print___baby_step_explainer),
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.lightText,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin01)
                .padding(horizontal = OctoAppTheme.dimens.margin2)
        )
    }
}

@Composable
private fun BabyStepControls(
    offsetMm: Float?,
    changeOffset: suspend (Float) -> Unit,
    resetOffset: suspend () -> Unit,
    saveToConfig: suspend () -> Unit,
) = Column(
    modifier = Modifier
        .background(OctoAppTheme.colors.inputBackground, MaterialTheme.shapes.large)
) {
    val isKlipper = LocalOctoPrint.current.systemInfo?.printerFirmwareFamily == SystemInfo.FirmwareFamily.Klipper

    BabyStepDisplay(
        offsetMm = offsetMm,
        resetOffset = resetOffset.takeIf { isKlipper },
    )
    BabyStepControlsDivider()
    BabyStepButtons(
        changeOffset = changeOffset,
    )
    AnimatedVisibility(visible = offsetMm != null && offsetMm != 0f && isKlipper) {
        Column {
            BabyStepControlsDivider()
            BabyStepSaveToConfigButton(saveToConfig = saveToConfig)
        }
    }
}

@Composable
fun BabyStepDisplay(
    offsetMm: Float?,
    resetOffset: (suspend () -> Unit)?,
) = Box {
    val resetAlpha by animateFloatAsState(targetValue = if (offsetMm == 0f || offsetMm == null) 0f else 1f, label = "resetAlpha")
    OctoInputField(
        value = offsetMm?.formatAsLength(onlyMilli = true, minDecimals = 3, maxDecimals = 3) ?: "",
        onValueChanged = {},
        placeholder = "",
        readOnly = true,
        label = stringResource(id = R.string.tune_print___z_baby_step),
        labelActive = stringResource(id = R.string.tune_print___z_baby_step),
    )
    if (resetOffset != null) {
        OctoIconButton(
            painter = painterResource(id = R.drawable.round_cleaning_services_24),
            contentDescription = null,
            onClick = resetOffset,
            enabled = resetAlpha > 0,
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .alpha(resetAlpha)
        )
    }
}

@Composable
private fun BabyStepControlsDivider() = Box(
    modifier = Modifier
        .padding(horizontal = OctoAppTheme.dimens.margin1)
        .background(OctoAppTheme.colors.accent.copy(alpha = 0.15f))
        .height(1.dp)
        .fillMaxWidth()
)

@Composable
private fun BabyStepSaveToConfigButton(
    saveToConfig: suspend () -> Unit,
) {
    var showConfirmation by remember { mutableStateOf(false) }
    if (showConfirmation) {
        OctoDialog(
            title = stringResource(id = R.string.tune_print___save_config),
            message = stringResource(id = R.string.tune_print___save_config_explainer),
            positiveButton = stringResource(id = R.string.tune_print___save_config),
            negativeButton = stringResource(id = R.string.cancel),
            onPositive = saveToConfig,
            onDismissRequest = { showConfirmation = false }
        )
    }
    OctoButton(
        text = stringResource(id = R.string.tune_print___save_config),
        onClick = { showConfirmation = true },
        loading = false,
        small = true,
        modifier = Modifier
            .fillMaxWidth()
            .padding(OctoAppTheme.dimens.margin1),
    )
}

@Composable
private fun BabyStepButtons(
    changeOffset: suspend (Float) -> Unit,
) = Column(
    modifier = Modifier
        .padding(OctoAppTheme.dimens.margin1)
        .background(OctoAppTheme.colors.primaryButtonBackground, MaterialTheme.shapes.large)
        .border(width = OctoAppTheme.dimens.margin0, color = OctoAppTheme.colors.primaryButtonBackground, shape = MaterialTheme.shapes.large)
) {

    var activeStepSizeIndex by remember { mutableStateOf(0) }
    val stepSizes = LocalOctoPrint.current.settings?.babyStepIntervals ?: Settings().babyStepIntervals
    Box(
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin0)
            .height(38.dp)
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.large)
            .background(OctoAppTheme.colors.inputBackground, shape = MaterialTheme.shapes.large)
            .zIndex(1000f)
    ) {
        OctoTabRow(
            selectedIndex = activeStepSizeIndex,
            onSelection = { activeStepSizeIndex = it },
            options = stepSizes.map { it.formatAsBabyStep() },
            modifier = Modifier.padding(OctoAppTheme.dimens.margin0),
        )
    }

    Row(
        modifier = Modifier.drawBehind {
            val width = 1.dp.toPx()
            drawRect(
                color = Color.White.copy(alpha = 0.15f),
                topLeft = center - Offset(x = width / 2, y = size.height / 2),
                size = size.copy(width = width)
            )
        }
    ) {
        OctoIconButton(
            painter = painterResource(id = R.drawable.ic_round_keyboard_arrow_up_24),
            onClick = { changeOffset(stepSizes[activeStepSizeIndex].absoluteValue) },
            modifier = Modifier.weight(1f),
            contentDescription = null,
            boundedRipple = false,
            tint = OctoAppTheme.colors.textColoredBackground,
        )

        OctoIconButton(
            painter = painterResource(id = R.drawable.ic_round_keyboard_arrow_down_24),
            onClick = { changeOffset(stepSizes[activeStepSizeIndex].absoluteValue.times(-1f)) },
            modifier = Modifier.weight(1f),
            contentDescription = null,
            boundedRipple = false,
            tint = OctoAppTheme.colors.textColoredBackground,
        )
    }
}

private fun Number.formatAsBabyStep() = formatAsLength(onlyMilli = true, maxDecimals = 5)
    .removeSuffix("mm")
    .trim()

@Composable
private fun TextInput(
    currentValue: Int?,
    label: String,
    labelActive: String,
    onApplyValue: () -> Unit,
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    modifier: Modifier = Modifier,
) {
    var isFocused by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current
    var changeByTyping by remember { mutableStateOf(false) }
    LaunchedEffect(currentValue) {
        if (!isFocused) {
            onValueChanged(TextFieldValue(currentValue?.toString() ?: ""))
        }
    }

    Box(
        contentAlignment = Alignment.CenterEnd,
        modifier = modifier,
    ) {
        OctoInputField(
            value = value,
            onValueChanged = {
                onValueChanged(it)
                changeByTyping = true
            },
            placeholder = "",
            label = label,
            alternativeBackground = true,
            labelActive = labelActive,
            modifier = Modifier.onFocusChanged { isFocused = it.isFocused },
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done, keyboardType = KeyboardType.Decimal),
            keyboardActions = KeyboardActions(
                onDone = {
                    onApplyValue()
                    focusManager.clearFocus()
                }
            ),
        )
    }
}
//endregion

//region Preview
@Composable
@Preview(device = Devices.PIXEL_4, showSystemUi = true)
private fun Preview() = OctoAppThemeForPreview {
    val fan = TuneState.Fan(
        component = "fan",
        label = "Fan",
        speed = 100f,
        isPartCoolingFan = true,
    )

    var state by remember {
        mutableStateOf(
            TuneState(
                print = TuneState.Print(
                    flowRate = 100f,
                    feedRate = 100f
                ),
                fans = listOf(
                    fan.copy(component = "fan1", label = "Fan 1", speed = 100f),
                    fan.copy(component = "fan2", label = "Fan 2", speed = 100f)
                ),
                offsets = TuneState.Offsets(
                    z = -0.015f
                ),
                isValuesImprecise = true,
            )
        )
    }

    LaunchedEffect(Unit) {
        repeat(10) {
            delay(1.seconds)
            state = state.copy(
                fans = listOf(
                    fan.copy(component = "fan1", label = "Fan 1", speed = it * 10f),
                    fan.copy(component = "fan2", label = "Fan 2", speed = 100f)
                ),
            )
        }
    }

    TuneControlsView(
        tuneState = state,
        onChangeOffset = { z ->
            delay(500.milliseconds)
            state = state.copy(
                offsets = state.offsets?.let {
                    it.copy(z = (it.z ?: 0f) + z)
                }
            )
        },
        onApplyValues = { feed, flow, fans ->
            delay(500.milliseconds)
            state = state.copy(
                print = state.print?.copy(flowRate = flow, feedRate = feed),
                fans = fans.mapValues { (key, value) -> fan.copy(component = key, label = key, speed = value) }.values.toList()
            )
        },
        onResetOffset = { delay(1.seconds) },
        onSaveOffsetToConfig = { delay(1.seconds) }
    )
}
//endregion