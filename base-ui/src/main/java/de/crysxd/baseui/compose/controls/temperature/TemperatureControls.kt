package de.crysxd.baseui.compose.controls.temperature

import android.content.Context
import android.graphics.Bitmap
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Shader
import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.ContextCompat
import androidx.core.graphics.applyCanvas
import androidx.core.graphics.get
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import de.crysxd.octoapp.menu.controls.TemperatureControlsSettingsMenu
import de.crysxd.octoapp.sharedcommon.ext.formatAsTemperature
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext

@Composable
fun TemperatureControls(
    state: TemperatureControlsState,
    editState: EditState,
    modifier: Modifier = Modifier
) {
    var showSettings by remember { mutableStateOf(false) }
    if (showSettings) {
        MenuSheet(
            menu = TemperatureControlsSettingsMenu(instanceId = LocalOctoPrint.current.id),
            onDismiss = { showSettings = false }
        )
    }

    ControlsScaffold(
        editState = editState,
        title = stringResource(id = R.string.widget_temperature),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
        actionContentDescription = stringResource(id = R.string.cd_settings),
        onAction = { _, _ -> showSettings = true },
        modifier = modifier,
    ) {
        TemperatureControlsInternals(
            temperatures = { state.current },
            count = { state.count },
            onSetTemperature = state::onSetTemperature
        )
    }
}

interface TemperatureControlsState {
    val current: List<TemperatureDataRepository.TemperatureSnapshot?>
    val count: Int
    fun onSetTemperature(component: TemperatureDataRepository.TemperatureSnapshot)
}

@Composable
fun rememberTemperatureControlsState(): TemperatureControlsState {
    val vmFactory = TemperatureControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = TemperatureControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    val initial = vm.getInitialComponentCount().let { (0 until it).map { null } }
    val state = vm.temperature.collectAsStateWhileActive(key = "temperature", initial = initial)
    val navController = OctoAppTheme.navController
    val context = LocalContext.current

    return object : TemperatureControlsState {
        override val current get() = state.value
        override val count by remember { derivedStateOf { current.size } }
        override fun onSetTemperature(component: TemperatureDataRepository.TemperatureSnapshot) {
            vm.changeTemperature(
                context = context,
                component = component,
                navController = navController()
            )
        }
    }
}

//region Internals
@Composable
private fun TemperatureControlsInternals(
    temperatures: () -> List<TemperatureDataRepository.TemperatureSnapshot?>,
    count: () -> Int,
    onSetTemperature: (TemperatureDataRepository.TemperatureSnapshot) -> Unit
) = Column(
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
) {
    val grouped by remember(Unit) {
        derivedStateOf {
            val mapped = temperatures().mapIndexed { i, it -> i to (it?.canControl == true) }.toMap()
            val controllable = mapped.filter { (_, controllable) -> controllable }
            val observable = mapped.filter { (_, controllable) -> !controllable }
            controllable.keys.toList().sorted() to observable.keys.toList().sorted()
        }
    }
    val (controllableIndices, observableIndices) = grouped

    ComponentRows(
        temperatures = temperatures,
        onSetTemperature = onSetTemperature,
        indices = controllableIndices
    )

    ComponentRows(
        temperatures = temperatures,
        onSetTemperature = null,
        indices = observableIndices
    )
}

@Composable
private fun ComponentRows(
    temperatures: () -> List<TemperatureDataRepository.TemperatureSnapshot?>,
    onSetTemperature: ((TemperatureDataRepository.TemperatureSnapshot) -> Unit)?,
    indices: List<Int>,
) {
    val columnCount = 2
    indices.chunked(columnCount).forEach { tempIndices ->
        Row(
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
            modifier = Modifier.fillMaxWidth()
        ) {
            // Temperatures
            tempIndices.forEach { tempIndex ->
                if (onSetTemperature != null) {
                    TemperatureControl(
                        snapshot = { temperatures()[tempIndex] },
                        modifier = Modifier.weight(1f),
                        onSetTemperature = onSetTemperature
                    )
                } else {
                    TemperatureDisplay(
                        snapshot = { temperatures().getOrNull(tempIndex) },
                        modifier = Modifier.weight(1f),
                    )
                }
            }

            // Fill row up
            repeat(columnCount - tempIndices.size) {
                Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}

@Composable
private fun TemperatureDisplay(
    snapshot: () -> TemperatureDataRepository.TemperatureSnapshot?,
    modifier: Modifier = Modifier,
) = Row(
    modifier = modifier
        .clip(MaterialTheme.shapes.large)
        .fillMaxWidth()
        .animateTemperatureColorBackground(snapshot)
        .temperatureGraphBackground(snapshotFactory = snapshot, bottomMargin = 0f)
        .padding(horizontal = OctoAppTheme.dimens.margin2),
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
) {
    val component by remember(snapshot) { derivedStateOf { snapshot()?.component } }
    val label by remember(snapshot) { derivedStateOf { snapshot()?.componentLabel } }
    val actual by remember(snapshot) { derivedStateOf { snapshot()?.current?.actual } }

    //region Label
    Text(
        text = label ?: "",
        style = OctoAppTheme.typography.labelSmall,
        color = OctoAppTheme.colors.temperatureText,
        overflow = TextOverflow.Ellipsis,
        maxLines = 2,
        modifier = Modifier
            .testTag(TestTags.Temperature.Label(component))
            .weight(1f)
    )
    //endregion
    //region Actual
    Crossfade(actual) {
        Text(
            text = it?.formatAsTemperature(minDecimals = 1, maxDecimals = 1) ?: "",
            style = OctoAppTheme.typography.data,
            color = OctoAppTheme.colors.temperatureText,
            textAlign = TextAlign.End,

            modifier = Modifier
                .testTag(TestTags.Temperature.Actual(component))
                .padding(vertical = OctoAppTheme.dimens.margin12)
                .weight(1f)
        )
    }
    //endregion
}

@Composable
private fun TemperatureControl(
    snapshot: () -> TemperatureDataRepository.TemperatureSnapshot?,
    onSetTemperature: (TemperatureDataRepository.TemperatureSnapshot) -> Unit,
    modifier: Modifier = Modifier,
) {
    var buttonHeight by remember { mutableStateOf(0f) }
    val context = LocalContext.current
    val component by remember(snapshot) { derivedStateOf { snapshot()?.component } }
    val label by remember(snapshot) { derivedStateOf { snapshot()?.componentLabel } }
    val actual by remember(snapshot) { derivedStateOf { snapshot()?.current?.actual } }
    val detail by remember(snapshot) {
        derivedStateOf {
            val target = snapshot()?.current?.target?.toInt()
            val offset = snapshot()?.offset?.toInt()
            when {
                target == null -> ""
                target == 0 -> context.getString(R.string.target_off)
                offset != null && offset != 0 -> context.getString(R.string.target_x_offset_y, target.toString(), offset.toString())
                else -> context.getString(R.string.target_x, target.toString())
            }
        }
    }

    Column(
        modifier = modifier
            .clip(MaterialTheme.shapes.large)
            .animateTemperatureColorBackground(snapshot)
            .temperatureGraphBackground(snapshotFactory = snapshot, bottomMargin = buttonHeight)
    ) {

        //region Label
        Text(
            text = label ?: "",
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.temperatureText,
            maxLines = 1,
            modifier = Modifier
                .testTag(TestTags.Temperature.Label(component))
                .padding(horizontal = OctoAppTheme.dimens.margin2)
                .padding(top = OctoAppTheme.dimens.margin2)
        )
        //endregion
        //region Actual
        Crossfade(actual) {
            Text(
                text = it?.formatAsTemperature(minDecimals = 1, maxDecimals = 1) ?: "",
                style = OctoAppTheme.typography.dataLarge,
                color = OctoAppTheme.colors.temperatureText,
                modifier = Modifier
                    .testTag(TestTags.Temperature.Actual(component))
                    .padding(horizontal = OctoAppTheme.dimens.margin2)
            )
        }
        //endregion
        //region Target
        Text(
            text = detail,
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.temperatureText,
            modifier = Modifier
                .testTag(TestTags.Temperature.Target(component))
                .padding(horizontal = OctoAppTheme.dimens.margin2)
                .padding(bottom = OctoAppTheme.dimens.margin1)
        )
        //endregion
        //region Button
        OctoButton(
            type = OctoButtonType.Surface,
            small = true,
            text = stringResource(id = R.string.set),
            modifier = Modifier
                .testTag(TestTags.Temperature.SetButton(component))
                .onGloballyPositioned { buttonHeight = it.size.height * 0.66f },
            onClick = { snapshot()?.let(onSetTemperature) },
        )
        //endregion
    }
}

//endregion
//region Graph
private data class Holder<T>(var value: T)

private fun Modifier.temperatureGraphBackground(snapshotFactory: () -> TemperatureDataRepository.TemperatureSnapshot?, bottomMargin: Float) = composed {
    val baseTime = remember { System.currentTimeMillis() }
    fun Long.localTime() = (this - baseTime).toInt()
    val snapshot = snapshotFactory()

    var targetAlpha by remember { mutableStateOf(0f) }
    val alpha by animateFloatAsState(targetValue = targetAlpha)

    if (snapshot != null && snapshot.history.size >= 2) {
        //region State
        var chartSize by remember { mutableStateOf(Size(1f, 1f)) }
        val lastHistory = remember { Holder((emptyList<TemperatureDataRepository.TemperatureHistoryPoint>())) }
        var path by remember { mutableStateOf(Path()) }
        val chartColor = Color.White.copy(alpha = alpha)
        //endregion
        //region Animations
//        var targetScaleX by remember { mutableStateOf(1f) }
//        val animatedScaleX by animateFloatAsState(
//            targetValue = targetScaleX,
//            animationSpec = spring(dampingRatio = Spring.DampingRatioNoBouncy, stiffness = Spring.StiffnessMedium)
//        )
//
//        var targetTranslationX by remember { mutableStateOf(0f) }
//        val animatedTranslationX by animateFloatAsState(
//            targetValue = targetTranslationX,
//            animationSpec = spring(dampingRatio = Spring.DampingRatioNoBouncy, stiffness = Spring.StiffnessMedium)
//        )
        //endregion
        //region Calculate path
        LaunchedEffect(snapshot, chartSize) {
            withContext(Dispatchers.Default) {
                val maxTemp = snapshot.maxTemp.coerceAtLeast(35f)
                val startTime = snapshot.history.first().time
                val lastStartTime = lastHistory.value.firstOrNull()?.time ?: startTime
                val endTime = snapshot.history.last().time
//                val lastEndTime = lastHistory.value.lastOrNull()?.time ?: endTime
                val deletedPoints = lastHistory.value.takeWhile { it.time < startTime }

                fun Long.toX() = chartSize.width - (((endTime - this) / (endTime - startTime).toFloat()) * chartSize.width)
                fun Float.toY() = chartSize.height - ((this / maxTemp) * chartSize.height * 0.8f)

                val newPoints = (deletedPoints + snapshot.history).map {
                    Offset(x = it.time.toX(), it.temperature.toY())
                }

                // Generate path
                val localPath = Path()
                addPointsToPathAsCubicCurves(localPath, newPoints)

                // Close Area
                localPath.lineTo(endTime.toX(), chartSize.height)
                localPath.lineTo(lastStartTime.toX(), chartSize.height)
                localPath.close()


//                // Animation
//                if (lastStartTime != startTime) {
//                    if (lastStartTime > 0) {
//                        val target = -lastStartTime.toX()
//                        targetTranslationX = animatedTranslationX + target
//                    }
//                } else {
//                    val target = (endTime.localTime().toDouble() / lastEndTime.localTime().toDouble()).toFloat()
//                    targetScaleX = animatedScaleX + (target - 1f)
//                }

                // Update values
                targetAlpha = 0.15f
                lastHistory.value = snapshot.history
                path = localPath
            }
        }
        //endregion
        //region Draw
        drawBehind {
            chartSize = size.copy(height = size.height - bottomMargin)

//            translate(left = targetTranslationX - animatedTranslationX) {
//                scale(scaleY = 1f, scaleX = 1f + (targetScaleX - animatedScaleX), pivot = Offset.Zero) {
            drawPath(path = path, color = chartColor, style = Fill)
//                }
//            }
        }
        //endregion
    } else {
        this
    }
}

private const val CUBIC_INTENSITY = 0.1f
private fun addPointsToPathAsCubicCurves(
    path: Path,
    points: List<Offset>,
    strokeWidth: Float = 0f,
    lineToFirst: Boolean = false
) {
    if (points.isEmpty()) return

    val sX = points.first().x - strokeWidth * 2
    val sY = points.first().y
    if (lineToFirst) {
        path.lineTo(sX, sY)
    } else {
        path.moveTo(sX, sY)
    }

    var prevPrevX: Float
    var prevPrevY: Float
    var prevX = sX
    var prevY = sY
    var curX = sX
    var curY = sY

    // Compile all points as a cubic bezir path. This logic comes from drawCubicBezir() here:
    // https://github.com/PhilJay/MPAndroidChart/blob/5a732b04278a983d32856c75ad552f2dcbbd922d/MPChartLib/src/main/java/com/github/mikephil/charting/renderer/LineChartRenderer.java#L186
    (points.indices).forEach { index ->
        prevPrevX = prevX
        prevPrevY = prevY
        prevX = curX
        prevY = curY
        val cur = points[index].also {
            curX = it.x
            curY = it.y
        }
        if (points.indices.last == index) {
            // We move last point in X by stroke width to prevent the
            // lines from being cut of vertically
            curX += strokeWidth * 2
        }
        val next = points.getOrNull(index + 1) ?: cur
        val nextX = next.x
        val nextY = next.y

        val prevDx = (curX - prevPrevX) * CUBIC_INTENSITY
        val prevDy = (curY - prevPrevY) * CUBIC_INTENSITY
        val curDx = (nextX - prevX) * CUBIC_INTENSITY
        val curDy = (nextY - prevY) * CUBIC_INTENSITY

        path.cubicTo(
            prevX + prevDx,
            prevY + prevDy,
            curX - curDx,
            curY - curDy,
            curX,
            curY,
        )
    }
}
//endregion

//region Colors
private fun Modifier.animateTemperatureColorBackground(snapshot: () -> TemperatureDataRepository.TemperatureSnapshot?) = composed {
    val context = LocalContext.current
    val target by remember(snapshot) {
        derivedStateOf {
            val actual = snapshot()?.current?.actual?.let { it - (it % 5) }
            val max = snapshot()?.maxTemp ?: 0f
            getTemperatureColor(context = context, temp = actual, maxTemp = max)
        }
    }
    val color = animateColorAsState(
        targetValue = target,
        animationSpec = tween(durationMillis = 300)
    ).value

    drawBehind {
        drawRect(color)
    }
}

private var temperatureGradient: Bitmap? = null
private fun createTemperatureGradient(context: Context): Bitmap {
    val gradient = Bitmap.createBitmap(1, 512, Bitmap.Config.ARGB_8888).applyCanvas {
        val colors = arrayOf(ContextCompat.getColor(context, R.color.color_hot), ContextCompat.getColor(context, R.color.color_cold)).toIntArray()
        val positions = arrayOf(0.3f, 1f).toFloatArray()
        val paint = Paint().also {
            it.style = Paint.Style.FILL
            it.shader = LinearGradient(0f, 0f, 0f, height.toFloat(), colors, positions, Shader.TileMode.CLAMP)
        }
        drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
    }
    temperatureGradient = gradient
    return gradient
}

private fun getTemperatureColor(context: Context, temp: Float?, maxTemp: Float): Color = try {
    val gradient = temperatureGradient ?: createTemperatureGradient(context)
    val tempRange = 35..(maxTemp.toInt().coerceAtLeast(36))
    val cappedTemp = temp?.toInt()?.coerceIn(tempRange) ?: tempRange.first
    val tempPercent = ((cappedTemp - tempRange.first) / (tempRange.last - tempRange.first).toFloat())
    val y = (gradient.height - (tempPercent * gradient.height)).coerceAtMost(gradient.height - 1f)
    Color(gradient[0, y.toInt()]).copy(alpha = 0.4f)
} catch (e: Exception) {
    Napier.e(tag = "TemperatureColor", message = "Failed to get color", throwable = e)
    Color.Gray
}
//endregion

//region Previews
@Preview
@Composable
private fun PreviewInitial() = OctoAppThemeForPreview {
    TemperatureControlsInternals(temperatures = { listOf(null, null) }, onSetTemperature = {}, count = { 2 })
}

@Preview
@Composable
private fun PreviewNormal() = OctoAppThemeForPreview {
    val bedLoop = listOf(13, 18, 24, 35, 54, 60, 51, 32, 25, 20)
    val hotendLoop = listOf(150, 160, 170, 180, 190, 200, 190, 180, 170, 160)
    val bedHistory = remember { mutableStateListOf<TemperatureDataRepository.TemperatureHistoryPoint>() }
    val hotendHistory = remember { mutableStateListOf<TemperatureDataRepository.TemperatureHistoryPoint>() }

    LaunchedEffect(Unit) {
        while (isActive) {
            bedLoop.indices.forEach {
                bedHistory.add(
                    TemperatureDataRepository.TemperatureHistoryPoint(
                        temperature = bedLoop[it].toFloat(),
                        time = System.currentTimeMillis()
                    )
                )

                hotendHistory.add(
                    TemperatureDataRepository.TemperatureHistoryPoint(
                        temperature = hotendLoop[it].toFloat(),
                        time = System.currentTimeMillis()
                    )
                )

                val overCount = (bedHistory.size - 25).coerceAtLeast(0)
                if (overCount > 0) {
                    bedHistory.removeRange(0, overCount)
                    hotendHistory.removeRange(0, overCount)
                }

                delay(500)
            }
        }
    }

    TemperatureControlsInternals(
        temperatures = {
            listOf(
                TemperatureDataRepository.TemperatureSnapshot(
                    component = "bed",
                    maxTemp = 70f,
                    canControl = true,
                    componentLabel = "Bed",
                    current = ComponentTemperature(actual = bedHistory.lastOrNull()?.temperature, target = 65f),
                    offset = 5f,
                    history = bedHistory.toList(),
                    isHidden = false
                ),
                TemperatureDataRepository.TemperatureSnapshot(
                    component = "hotend",
                    maxTemp = 200f,
                    canControl = true,
                    componentLabel = "Tool 0",
                    current = ComponentTemperature(actual = hotendHistory.lastOrNull()?.temperature, target = 200f),
                    offset = 0f,
                    history = hotendHistory.toList(),
                    isHidden = false
                ),
                TemperatureDataRepository.TemperatureSnapshot(
                    component = "extra1",
                    maxTemp = 0f,
                    canControl = false,
                    componentLabel = "Extra with super long title so we can see line break",
                    current = ComponentTemperature(actual = 19f, target = 200f),
                    offset = 0f,
                    history = emptyList(),
                    isHidden = false
                ),
                TemperatureDataRepository.TemperatureSnapshot(
                    component = "extra2",
                    maxTemp = 0f,
                    canControl = false,
                    componentLabel = "Extra",
                    current = ComponentTemperature(actual = 19f, target = 200f),
                    offset = 0f,
                    history = emptyList(),
                    isHidden = false
                ),
                TemperatureDataRepository.TemperatureSnapshot(
                    component = "extra3",
                    maxTemp = 0f,
                    canControl = false,
                    componentLabel = "Extra",
                    current = ComponentTemperature(actual = 19f, target = 200f),
                    offset = 0f,
                    history = emptyList(),
                    isHidden = false
                ),
            )
        },
        onSetTemperature = {},
        count = { 3 },
    )
}

//endregion