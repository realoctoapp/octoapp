package de.crysxd.baseui.compose.framework.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview

@Composable
fun OctoListItem(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    onLongClick: (() -> Unit)? = null,
    title: @Composable () -> Unit,
    detail: @Composable () -> Unit,
    thumbnail: (@Composable () -> Unit)? = null,
    thumbnailBadge: (@Composable () -> Unit)? = null,
) = OctoListItem(
    modifier = modifier,
    onClick = onClick,
    onLongClick = onLongClick,
    thumbnail = thumbnail,
    thumbnailBadge = thumbnailBadge,
    content = {
        CompositionLocalProvider(
            LocalTextStyle provides OctoAppTheme.typography.base.copy(color = OctoAppTheme.colors.darkText),
            content = { title() }
        )

        Spacer(Modifier.height(OctoAppTheme.dimens.margin01))

        CompositionLocalProvider(
            LocalTextStyle provides OctoAppTheme.typography.labelSmall.copy(color = OctoAppTheme.colors.lightText),
            content = { detail() }
        )
    }
)


@Composable
@OptIn(ExperimentalFoundationApi::class)
fun OctoListItem(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    onLongClick: (() -> Unit)? = null,
    thumbnail: (@Composable () -> Unit)? = null,
    thumbnailBadge: (@Composable () -> Unit)? = null,
    content: @Composable ColumnScope.() -> Unit,
) = Box(
    modifier = modifier.height(intrinsicSize = IntrinsicSize.Min)
) {

    //region Click area
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = OctoAppTheme.dimens.margin12)
            .padding(vertical = OctoAppTheme.dimens.margin1)
            .clip(RoundedCornerShape(topStart = OctoAppTheme.dimens.cornerRadius, bottomStart = OctoAppTheme.dimens.cornerRadius))
            .combinedClickable(
                onLongClick = onLongClick,
                onClick = onClick,
            )
    )
    //endregion

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12),
        modifier = Modifier
            .height(intrinsicSize = IntrinsicSize.Min)
            .padding(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin01)

    ) {
        //region Thumbnail
        thumbnail?.let {
            Box(
                modifier = Modifier
                    .padding(vertical = OctoAppTheme.dimens.margin1)
                    .height(IntrinsicSize.Min)
            ) {
                Box(
                    modifier = Modifier
                        .clip(MaterialTheme.shapes.large)
                        .background(OctoAppTheme.colors.inputBackground),
                    content = {
                        thumbnail()
                    }
                )

                thumbnailBadge?.let {
                    Box(
                        modifier = Modifier
                            .align(Alignment.BottomEnd)
                            .offset(x = 4.dp, y = 4.dp)
                            .clip(CircleShape)
                            .background(OctoAppTheme.colors.darkText.copy(alpha = 0.2f))
                            .padding(1.dp),
                        content = {
                            CompositionLocalProvider(LocalContentColor provides OctoAppTheme.colors.darkText) {
                                thumbnailBadge()
                            }
                        }
                    )
                }
            }
        }
        //endregion
        //region Info
        val lineColor = OctoAppTheme.colors.lightText.copy(alpha = 0.3f)
        val lineExtraWidth = OctoAppTheme.dimens.margin2
        val lineOffset = OctoAppTheme.dimens.margin01
        Column(
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .weight(1f)
                .drawBehind {
                    val height = 1.dp.toPx()
                    drawRect(
                        color = lineColor,
                        topLeft = Offset(0f, size.height - height + lineOffset.toPx()),
                        size = size.copy(height = height, width = size.width + lineExtraWidth.toPx())
                    )
                }
                .padding(vertical = OctoAppTheme.dimens.margin12)
                .fillMaxHeight()
        ) {
            content()
        }
        //endregion
    }
    //endregion
}

//region Preview
@Composable
@Preview
private fun PreviewNoThumbnail() = OctoAppThemeForPreview {
    Column {
        repeat(4) {
            OctoListItem(
                onClick = {},
                content = {
                    Text(
                        text = "Line 1", modifier = Modifier
                            .fillMaxWidth()
                            .border(1.dp, Color.Red)
                    )
                    Text(
                        text = "Line 2", modifier = Modifier
                            .fillMaxWidth()
                            .border(1.dp, Color.Red)
                    )
                }
            )
        }
    }
}

@Composable
@Preview
private fun PreviewThumbnail() = OctoAppThemeForPreview {
    Column {
        repeat(4) {
            OctoListItem(
                onClick = {},
                thumbnail = {
                    CircularProgressIndicator(
                        modifier = Modifier.scale(0.66f)
                    )
                },
                thumbnailBadge = {
                    Icon(
                        painter = painterResource(R.drawable.ic_round_print_24),
                        contentDescription = null,
                    )
                },
                content = {
                    Text(
                        text = "Line 1", modifier = Modifier
                            .fillMaxWidth()
                            .border(1.dp, Color.Red)
                    )
                    Text(
                        text = "Line 2", modifier = Modifier
                            .fillMaxWidth()
                            .border(1.dp, Color.Red)
                    )
                }
            )
        }
    }
}
//endregion