package de.crysxd.baseui.compose.menu

import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.core.view.WindowCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import com.benasher44.uuid.uuid4
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.ComposeContent
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.ext.getParcelableCompat
import de.crysxd.octoapp.base.ext.getSerializableCompat
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.menu.Menu
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import kotlin.math.absoluteValue
import kotlin.time.Duration.Companion.milliseconds


open class MenuBottomSheetFragment : DialogFragment() {

    val menuInstanceId: String by lazy { requireNotNull(arguments?.getString(KEY_MENU_INSTANCE_ID)) }
    private val menu: Menu by lazy { requireNotNull(arguments?.getParcelableCompat(KEY_MENU)) }
    private val menuId: MenuId by lazy { requireNotNull(arguments?.getSerializableCompat(KEY_MENU_ID)) }
    private val instanceId: String? by lazy { arguments?.getString(KEY_INSTANCE_ID) }
    private val onBackPressEventFlow = MutableSharedFlow<Unit>(replay = 0)
    private val playDismissAnimation = MutableSharedFlow<() -> Unit>(replay = 0)
    private val tag = "MenuBottomSheetFragment"
    private var targetAlpha by mutableStateOf(0f)

    companion object {
        private const val KEY_MENU = "menu"
        private const val KEY_MENU_ID = "menuId"
        private const val KEY_INSTANCE_ID = "instanceId"
        private const val KEY_MENU_INSTANCE_ID = "menu_instanceId"

        private val dismissCallbacks = mutableMapOf<String, (suspend () -> Unit)?>()
        private val resultCallbacks = mutableMapOf<String, (suspend (String?) -> Unit)?>()

        fun createForMenu(
            menu: Menu,
            menuId: MenuId = MenuId.Other,
            onDismiss: (suspend () -> Unit)? = null,
            onResult: (suspend (String?) -> Unit)? = null,
            instanceId: String?,
        ) = MenuBottomSheetFragment().also {
            val iid = "${menu.id}/${uuid4()}"
            dismissCallbacks[iid] = onDismiss
            resultCallbacks[iid] = onResult

            it.arguments = bundleOf(
                KEY_MENU to menu,
                KEY_MENU_ID to menuId,
                KEY_INSTANCE_ID to instanceId,
                KEY_MENU_INSTANCE_ID to iid,
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeContent(instanceId = instanceId) {
        val androidMenuHost = remember { DefaultAndroidMenuHost(parentFragment ?: this) }

        CompositionLocalProvider(
            LocalViewModelStoreOwner provides this,
            LocalAndroidMenuHost provides androidMenuHost,
        ) {
            val state = rememberMenuHostState(
                menu = menu,
                menuId = menuId,
                onCloseMenu = { result ->
                    resultCallbacks[menuInstanceId]?.invoke(result) ?: Napier.w(
                        tag = tag,
                        message = "Dropped result for menu $menuInstanceId, no result callback"
                    )
                    dismissAllowingStateLoss()
                },
                onOpenLink = { uri ->
                    emitDismissAnimation {
                        uri.open()
                    }
                }
            )

            LaunchedEffect(Unit) {
                onBackPressEventFlow.collect {
                    state.navigateUp()
                }
            }

            BottomSheet {
                MenuHost(
                    state = state,
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
    }

    override fun dismiss() {
        emitDismissAnimation {
            super.dismiss()
        }
    }

    override fun dismissAllowingStateLoss() {
        emitDismissAnimation {
            super.dismissAllowingStateLoss()
        }
    }

    private fun emitDismissAnimation(callback: () -> Unit) {
        if (playDismissAnimation.subscriptionCount.value == 0) {
            Napier.w(tag = tag, message = "Skipping dismiss animation, not ready")
            callback()
        } else lifecycleScope.launch {
            playDismissAnimation.emit {
                callback()
            }
        }
    }

    override fun getTheme(): Int = R.style.OctoTheme_BottomSheetDialog

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        Napier.w(
            tag = tag,
            message = "$menuInstanceId got dismissed"
        )
        AppScope.launch {
            dismissCallbacks[menuInstanceId]?.invoke()
            dismissCallbacks.remove(menuInstanceId)
            resultCallbacks.remove(menuInstanceId)
        }
    }

    @Composable
    private fun BottomSheet(content: @Composable () -> Unit) = with(LocalDensity.current) {
        val scope = rememberCoroutineScope()
        val density = LocalDensity.current
        val initialHeight = 500.dp
        var maxHeight by remember { mutableStateOf(initialHeight) }
        var actualHeight by remember { mutableStateOf(initialHeight) }
        val dragController = remember { SheetDragController(scope, density, { actualHeight }, ::dismissAllowingStateLoss) }
        val background = OctoAppTheme.colors.windowBackground
        var targetAlpha by rememberSaveable { mutableStateOf(0f) }
        var didAnimateIn by rememberSaveable { mutableStateOf(true) }
        val sheetAlpha by animateFloatAsState(targetValue = targetAlpha)
        val maxCorners = OctoAppTheme.dimens.cornerRadius
        val hasCorners by remember { derivedStateOf { (actualHeight - dragController.value.toDp()) >= maxHeight } }
        val corners by animateDpAsState(targetValue = if (hasCorners) 0.dp else maxCorners)
        val nestedScrollConnection = object : NestedScrollConnection {
            override fun onPostScroll(consumed: Offset, available: Offset, source: NestedScrollSource) = if (hasCorners || available.y > 0) {
                dragController.consumeOffset(available)
            } else {
                Offset.Zero
            }
        }

        //region Animate in
        LaunchedEffect(didAnimateIn, actualHeight != initialHeight) {
            if (!didAnimateIn && actualHeight != initialHeight && actualHeight > 25.dp) scope.launch {
                Napier.d(tag = tag, message = "Animating from $actualHeight to 0")
                didAnimateIn = true
                dragController.snapTo(actualHeight.toPx())
                targetAlpha = 1f
                dragController.animateTo(0f)
                Napier.d(tag = tag, message = "Animation done")
            } else {
                Napier.d(tag = tag, message = "Actual height is now $actualHeight")
            }
        }
        LaunchedEffect(Unit) {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                Napier.d(tag = tag, message = "Dialog resumed")
                delay(300.milliseconds)
                didAnimateIn = false
            }
        }
        //endregion
        //region Animate out
        LaunchedEffect(Unit) {
            playDismissAnimation.collect { callback ->
                Napier.d(tag = tag, message = "Playing dismiss aimation")
                targetAlpha = 0f
                dragController.animateTo(actualHeight.toPx() - dragController.value)
                callback()
            }
        }
        //endregion

        Box(
            contentAlignment = Alignment.BottomCenter,
            modifier = Modifier
                .fillMaxSize()
                .graphicsLayer { alpha = sheetAlpha }
                .onGloballyPositioned { maxHeight = it.size.height.toDp() }
                .background(OctoAppTheme.colors.controlCenterBackground)
                .clickable(
                    interactionSource = MutableInteractionSource(),
                    indication = null,
                    onClick = ::dismissAllowingStateLoss
                )
        ) {
            Box(
                modifier = Modifier
                    .widthIn(max = 650.dp)
                    .drawBehind {
                        val h = actualHeight.toPx() - corners.toPx()
                        drawRect(
                            color = background,
                            size = size.copy(height = h),
                            topLeft = Offset(x = 0f, y = size.height - h + dragController.value.coerceAtLeast(0f))
                        )
                    }
                    .bottomSheetDrag(dragController)
                    .fillMaxWidth()
                    .animateContentSize(spring())
                    .onGloballyPositioned { actualHeight = it.size.height.toDp() }
                    .heightIn(max = maxHeight, min = 100.dp)
                    .navigationBarsPadding()
                    .clickable(
                        interactionSource = MutableInteractionSource(),
                        indication = null,
                        onClick = { /* Consume to prevent dismiss */ }
                    )
                    .drawBehind {
                        drawRoundRect(
                            color = background,
                            cornerRadius = CornerRadius(corners.toPx()),
                            size = size.copy(height = size.height + corners.toPx())
                        )
                    }
                    .clip(RoundedCornerShape(topStart = maxCorners, topEnd = maxCorners))
            ) {
                Box(
                    modifier = Modifier
                        .padding(top = OctoAppTheme.dimens.margin1)
                        .size(width = 60.dp, height = 3.dp)
                        .background(OctoAppTheme.colors.lightText.copy(alpha = 0.5f), RoundedCornerShape(1.5.dp))
                        .align(Alignment.TopCenter)
                )

                CompositionLocalProvider(
                    LocalNestedScrollConnection provides nestedScrollConnection
                ) {
                    content()
                }
            }
        }
    }

    private fun Modifier.bottomSheetDrag(dragController: SheetDragController): Modifier = composed {
        this
            .offset { IntOffset(0, dragController.value.toInt()) }
            .nestedScroll(LocalNestedScrollConnection.current)
            .pointerInput(Unit) {
                detectDragGestures(
                    onDrag = { change, dragAmount ->
                        change.consume()
                        dragController.consumeOffset(offset = dragAmount)
                    },
                    onDragEnd = {
                        dragController.resetIfNotDismissed()
                    }
                )
            }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = super.onCreateDialog(savedInstanceState).apply {
        setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                lifecycleScope.launch {
                    onBackPressEventFlow.emit(Unit)
                }
            }

            true
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.run {
            setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            WindowCompat.setDecorFitsSystemWindows(this, false)
        }
    }

    fun show(fm: FragmentManager) = try {
        show(fm, menu.id)
    } catch (e: IllegalStateException) {
        Napier.w(
            tag = tag,
            message = "Can't show menu, after onSaveInstanceState()"
        )
    }

    class SheetDragController(
        private val scope: CoroutineScope,
        private val density: Density,
        private val height: () -> Dp,
        private val dismiss: () -> Unit,
    ) {
        private val totalOffset = Animatable(initialValue = 0f)
        val value get() = totalOffset.value
        private var dismissed = false
        private var lastChangeJob: Job? = null

        fun consumeOffset(offset: Offset): Offset = with(density) {
            if (dismissed || offset.y == 0f) return@with Offset.Zero

            val current = totalOffset.value
            val multiplier = if (current < 0) 1f / (current.absoluteValue / 2f) else 1f

            lastChangeJob?.cancel()
            lastChangeJob = scope.launch {
                totalOffset.snapTo(current + (offset.y * multiplier))

                val shouldDismissedByTotalDistance = totalOffset.value.toDp() > 250.dp
                val shouldDismissedByRelativeDistance = totalOffset.value.toDp() > height() / 2
                if (shouldDismissedByTotalDistance || shouldDismissedByRelativeDistance) {
                    dismissed = true
                    totalOffset.animateTo(height().toPx())
                    dismiss()
                } else {
                    delay(50.milliseconds)
                    resetIfNotDismissed()
                }
            }

            return offset.copy(x = 0f)
        }

        suspend fun snapTo(offset: Float) = totalOffset.snapTo(offset)

        suspend fun animateTo(offset: Float) = totalOffset.animateTo(offset)

        fun resetIfNotDismissed() {
            if (!dismissed) {
                lastChangeJob?.cancel()

                scope.launch {
                    totalOffset.animateTo(targetValue = 0f)
                }
            }
        }
    }
}
