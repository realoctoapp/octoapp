package de.crysxd.baseui.compose.screens.terminal

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.viewmodels.TerminalViewModelCore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TerminalViewModel(val instanceId: String) : ViewModelWithCore() {

    override val core = TerminalViewModelCore(instanceId)
    val state = core.state

    suspend fun executeGcode(gcode: String) = withContext(Dispatchers.Default) { core.executeGcode(gcode) }

    suspend fun toggleStyledLogs() = core.toggleStyledLogs()

    fun getShortcuts() = core.getShortcuts()

    fun clear() = core.clear()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = TerminalViewModel(
            instanceId = instanceId,
        ) as T
    }
}
