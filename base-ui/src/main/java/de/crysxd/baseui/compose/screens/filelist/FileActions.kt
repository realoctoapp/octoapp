package de.crysxd.baseui.compose.screens.filelist

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.helpers.rememberPrinterConfig
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsBottomPadding
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.models.PrinterConfigurationV3
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.viewmodels.FileActionsViewModelCore


interface FileActionsController {
    var activePath: Path?
    val state: FileActionsViewModelCore.State
    val sourceConfig: PrinterConfigurationV3?
    val destinationConfig: PrinterConfigurationV3?

    fun cancel()
    fun copy(instanceId: String, path: String, display: String)
    fun cut(instanceId: String, path: String, display: String)
    fun paste()

    data class Path(
        val instanceId: String,
        val path: String
    )
}

val LocalFileActionsController = compositionLocalOf<FileActionsController> { throw IllegalStateException("No FileListCopyScope") }

@Composable
fun rememberFileActionsController(): FileActionsController {
    val vm = viewModel(FileActionsViewModel::class.java)
    val instanceId = LocalOctoPrint.current.id
    val state = vm.state.collectAsState()
    val activePath = remember { mutableStateOf<FileActionsController.Path?>(null) }
    val destinationConfig = rememberPrinterConfig(activePath.value?.instanceId ?: instanceId)
    val sourceConfig = rememberPrinterConfig((state.value as? FileActionsViewModelCore.State.ReadyToPaste)?.instanceId ?: instanceId)

    return remember(vm, activePath, state, sourceConfig, destinationConfig) {
        object : FileActionsController {
            override var activePath by activePath
            override val state by state
            override val sourceConfig by sourceConfig
            override val destinationConfig by destinationConfig

            override fun cancel() = vm.cancel()

            override fun copy(instanceId: String, path: String, display: String) =
                vm.copy(instanceId = instanceId, path = path, display = display)

            override fun cut(instanceId: String, path: String, display: String) =
                vm.cut(instanceId = instanceId, path = path, display = display)

            override fun paste() {
                val path = requireNotNull(activePath.value) { "Pasted without target path" }
                vm.paste(instanceId = path.instanceId, path = path.path)
            }
        }
    }
}

@Composable
fun FileActionsBar(
    modifier: Modifier = Modifier,
    placeholder: Boolean = false,
    controller: FileActionsController = LocalFileActionsController.current,
) {
    val shape = RoundedCornerShape(topStart = OctoAppTheme.dimens.margin1, topEnd = OctoAppTheme.dimens.margin1)

    AnimatedContent(
        targetState = controller.state,
        transitionSpec = { fadeIn() togetherWith fadeOut() },
        contentKey = { it::class.java },
        contentAlignment = Alignment.BottomCenter,
        modifier = modifier
            .alpha(if (placeholder) 0f else 1f)
            .fillMaxWidth()
            .shadow(15.dp, shape)
            .clip(shape)
            .background(OctoAppTheme.colors.accent),
    ) { state ->
        when (state) {
            is FileActionsViewModelCore.State.ReadyToPaste -> PasteState(state = state, controller = controller)
            is FileActionsViewModelCore.State.Loading -> LoadingState(state = state)
            else -> Spacer(modifier = Modifier.fillMaxWidth())
        }
    }
}

@Composable
private fun LoadingState(
    modifier: Modifier = Modifier,
    state: FileActionsViewModelCore.State.Loading
) {
    val animatedProgress by animateFloatAsState(targetValue = state.progress)
    val progressColor = OctoAppTheme.colors.textColoredBackground.copy(alpha = 0.2f)

    AnimatedContent(
        modifier = modifier
            .fillMaxWidth()
            .drawBehind {
                drawRect(
                    color = progressColor,
                    size = size.copy(width = size.width * animatedProgress)
                )
            }
            .octoAppInsetsBottomPadding()
            .padding(OctoAppTheme.dimens.margin2),
        targetState = state.display.takeIf { state.progress != 0f && state.progress != 1f },
        contentKey = { it != null }
    ) { text ->
        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.CenterEnd,
        ) {
            if (text == null) {
                CircularProgressIndicator(
                    modifier = Modifier.size(24.dp),
                    strokeWidth = 1.5.dp,
                    color = OctoAppTheme.colors.textColoredBackground,
                )
            }
            Text(
                text = text ?: "",
                style = OctoAppTheme.typography.subtitle,
                color = OctoAppTheme.colors.textColoredBackground,
                maxLines = 2,
                modifier = Modifier.heightIn(min = 24.dp),
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}

@Composable
private fun PasteState(
    state: FileActionsViewModelCore.State.ReadyToPaste,
    controller: FileActionsController,
) {
    if (controller.destinationConfig == null) {
        Spacer(modifier = Modifier.fillMaxWidth())
        return
    }

    ConstraintLayout(
        modifier = Modifier
            .octoAppInsetsBottomPadding()
            .fillMaxWidth()
            .padding(start = OctoAppTheme.dimens.margin2)
    ) {
        val (info, buttons) = createRefs()

        Column(
            modifier = Modifier
                .padding(vertical = OctoAppTheme.dimens.margin12)
                .padding(end = OctoAppTheme.dimens.margin1)
                .constrainAs(info) {
                    centerVerticallyTo(parent)
                    start.linkTo(parent.start)
                    end.linkTo(buttons.start)
                    width = Dimension.fillToConstraints
                },
        ) {
            Text(
                text = state.display,
                style = OctoAppTheme.typography.label,
                color = OctoAppTheme.colors.textColoredBackground,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
            )

            AnimatedVisibility(state.instanceId != controller.activePath?.instanceId) {
                RemoteCopyIndicator(sourceLabel = controller.sourceConfig?.label ?: "", destinationLabel = controller.destinationConfig?.label ?: "")
            }
        }

        Buttons(
            controller = controller,
            modifier = Modifier
                .padding(end = OctoAppTheme.dimens.margin1)
                .constrainAs(buttons) {
                    end.linkTo(parent.end)
                    centerVerticallyTo(parent)
                }
        )

    }
}

@Composable
private fun Buttons(
    modifier: Modifier = Modifier,
    controller: FileActionsController,
) = Row(modifier) {
    OctoIconButton(
        painter = painterResource(R.drawable.ic_round_content_paste_24),
        contentDescription = stringResource(R.string.file_manager___file_list___paste_here),
        onClick = { controller.paste() },
        tint = OctoAppTheme.colors.textColoredBackground
    )

    OctoIconButton(
        painter = painterResource(R.drawable.ic_round_close_24),
        contentDescription = stringResource(R.string.file_manager___file_list___cancel_paste),
        onClick = { controller.cancel() },
        tint = OctoAppTheme.colors.textColoredBackground
    )
}

@Composable
private fun RemoteCopyIndicator(
    modifier: Modifier = Modifier,
    sourceLabel: String,
    destinationLabel: String,
) = Row(
    modifier = modifier
        .padding(top = OctoAppTheme.dimens.margin01)
        .background(OctoAppTheme.colors.textColoredBackground.copy(alpha = 0.2f), RoundedCornerShape(percent = 50))
        .padding(horizontal = OctoAppTheme.dimens.margin1, vertical = OctoAppTheme.dimens.margin0)
) {
    val color = OctoAppTheme.colors.textColoredBackground.copy(alpha = 0.8f)

    Text(
        text = sourceLabel,
        style = OctoAppTheme.typography.labelSmall,
        color = color,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier
    )

    Icon(
        tint = color,
        painter = painterResource(R.drawable.ic_round_settings_ethernet_24),
        contentDescription = null,
        modifier = Modifier
            .padding(horizontal = OctoAppTheme.dimens.margin01)
            .size(14.dp)

    )

    Text(
        text = destinationLabel,
        style = OctoAppTheme.typography.labelSmall,
        color = color,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier
    )
}

//region Preview
@Composable
private fun Preview(
    path: FileActionsController.Path,
    state: FileActionsViewModelCore.State = FileActionsViewModelCore.State.ReadyToPaste(
        display = "Some file with long name that should break.gcode",
        path = "/",
        instanceId = "a",
        mode = FileActionsViewModelCore.CopyMode.Move,
    )
) = OctoAppThemeForPreview {
    val controller = object : FileActionsController {
        override var activePath: FileActionsController.Path? = path
        override val destinationConfig = PrinterConfigurationV3(
            id = "a",
            webUrl = "".toUrl(),
            apiKey = "",
            settings = Settings(appearance = Settings.Appearance(name = "Some printer with really long name"))
        )
        override val sourceConfig = PrinterConfigurationV3(
            id = "b",
            webUrl = "".toUrl(),
            apiKey = "",
            settings = Settings(appearance = Settings.Appearance(name = "Some other printer"))
        )
        override val state = state

        override fun cancel() = Unit
        override fun copy(instanceId: String, path: String, display: String) = Unit
        override fun cut(instanceId: String, path: String, display: String) = Unit
        override fun paste() = Unit

    }

    FileActionsBar(controller = controller)
}

@Preview
@Composable
private fun PreviewLocal() = Preview(FileActionsController.Path(instanceId = "a", path = "/"))

@Preview
@Composable
private fun PreviewRemote() = Preview(FileActionsController.Path(instanceId = "b", path = "/"))

@Preview
@Composable
private fun PreviewLoading() = Preview(
    path = FileActionsController.Path(instanceId = "b", path = "/"),
    state = FileActionsViewModelCore.State.Loading("40%", 0.4f)
)


@Preview
@Composable
private fun PreviewLoading0() = Preview(
    path = FileActionsController.Path(instanceId = "b", path = "/"),
    state = FileActionsViewModelCore.State.Loading("0%", 0f)
)


//endregion