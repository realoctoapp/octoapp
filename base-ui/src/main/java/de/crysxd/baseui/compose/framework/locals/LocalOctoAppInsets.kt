package de.crysxd.baseui.compose.framework.locals

import android.graphics.Rect
import androidx.compose.runtime.compositionLocalOf

val LocalOctoAppInsets = compositionLocalOf { Rect() }