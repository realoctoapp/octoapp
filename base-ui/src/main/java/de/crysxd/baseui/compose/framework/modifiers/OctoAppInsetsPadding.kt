package de.crysxd.baseui.compose.framework.modifiers

import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.compose.framework.locals.LocalOctoAppInsets

fun Modifier.octoAppInsetsPadding(
    top: Boolean = false,
    bottom: Boolean = false,
    left: Boolean = false,
    right: Boolean = false,
): Modifier = composed {
    with(LocalDensity.current) {
        val insets = LocalOctoAppInsets.current
        this@octoAppInsetsPadding
//            .drawBehind {
//                if (top) {
//                    drawRect(
//                        color = Color.Companion.Red.copy(alpha = 0.4f),
//                        size = size.copy(height = insets.top.toFloat())
//                    )
//                }
//                if (bottom) {
//                    drawRect(
//                        color = Color.Companion.Green.copy(alpha = 0.4f),
//                        size = size.copy(height = insets.bottom.toFloat()),
//                        topLeft = Offset(x = 0f, y = size.height - insets.bottom)
//                    )
//                }
//            }
            .padding(
                top = if (top) insets.top.toDp() else 0.dp,
                start = if (left) insets.left.toDp() else 0.dp,
                end = if (right) insets.right.toDp() else 0.dp,
                bottom = if (bottom) insets.bottom.toDp() else 0.dp,
            )
    }
}

fun Modifier.octoAppInsetsTopPadding() = octoAppInsetsPadding(top = true)
fun Modifier.octoAppInsetsBottomPadding() = octoAppInsetsPadding(bottom = true)