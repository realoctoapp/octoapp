package de.crysxd.baseui.compose.controls.printconfidence

import android.net.Uri
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence

@Composable
fun rememberPrintConfidenceState(): PrintConfidenceControlsState {
    val vmFactory = PrintConfidenceControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = PrintConfidenceControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    val state = vm.state.collectAsStateWhileActive(key = "confidence", initial = null)
    return object : PrintConfidenceControlsState {
        override val current get() = state.value
        override val contentClickUrl
            get() = when (state.value?.origin) {
                PrintConfidence.Origin.OctoEverywhere -> Uri.parse("https://octoeverywhere.com/gadget")
                PrintConfidence.Origin.Obico -> Uri.parse("https://app.obico.io/printers/")
                else -> null
            }

        @Composable
        override fun backgroundColorFor(confidence: PrintConfidence): Color = when (state.value?.origin) {
            PrintConfidence.Origin.OctoEverywhere -> colorResource(id = R.color.octoeverywhere)
            PrintConfidence.Origin.Obico -> colorResource(id = R.color.obico)
            else -> Color.Transparent
        }

        @Composable
        override fun disclaimerFor(confidence: PrintConfidence): String = when (state.value?.origin) {
            PrintConfidence.Origin.OctoEverywhere -> stringResource(id = R.string.print_confidence_disclaimer_octoeverywhere)
            else -> stringResource(id = R.string.print_confidence_disclaimer_obico)
        }

        @Composable
        override fun iconFor(confidence: PrintConfidence): Painter = state.value?.run {
            when (level) {
                PrintConfidence.Level.High -> when (origin) {
                    PrintConfidence.Origin.OctoEverywhere -> painterResource(id = R.drawable.ic_gadget_green)
                    PrintConfidence.Origin.Obico -> painterResource(id = R.drawable.ic_obico_24px)
                }

                PrintConfidence.Level.Medium -> when (origin) {
                    PrintConfidence.Origin.OctoEverywhere -> painterResource(id = R.drawable.ic_gadget_yellow)
                    PrintConfidence.Origin.Obico -> painterResource(id = R.drawable.ic_obico_24px)
                }

                PrintConfidence.Level.Low,
                PrintConfidence.Level.Unknown -> when (origin) {
                    PrintConfidence.Origin.OctoEverywhere -> painterResource(id = R.drawable.ic_gadget_red)
                    PrintConfidence.Origin.Obico -> painterResource(id = R.drawable.ic_obico_24px)
                }
            }
        } ?: painterResource(id = R.drawable.ic_empty_16)
    }
}

interface PrintConfidenceControlsState {
    val current: PrintConfidence?
    val contentClickUrl: Uri?

    @Composable
    fun backgroundColorFor(confidence: PrintConfidence): Color

    @Composable
    fun iconFor(confidence: PrintConfidence): Painter

    @Composable
    fun disclaimerFor(confidence: PrintConfidence): String
}