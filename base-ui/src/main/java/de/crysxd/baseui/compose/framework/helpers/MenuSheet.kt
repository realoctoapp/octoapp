package de.crysxd.baseui.compose.framework.helpers

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.menu.MenuBottomSheetFragment
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.menu.Menu
import io.github.aakira.napier.Napier
import kotlinx.coroutines.launch

@Composable
fun rememberMenuSheetState(
    onResult: suspend (String?) -> Unit = {},
    menu: () -> Menu,
): MenuSheetState {
    val isPreview = OctoAppTheme.isPreview
    var showMenu by remember { mutableStateOf(false) }
    if (showMenu) {
        MenuSheet(
            menu = remember { menu() },
            onResult = onResult,
            onDismiss = { showMenu = false }
        )
    }

    return remember {
        object : MenuSheetState {
            override fun show() {
                showMenu = !isPreview
            }
        }
    }
}

interface MenuSheetState {
    fun show()
}

@Composable
fun MenuSheet(
    menu: Menu,
    menuId: MenuId = MenuId.Other,
    onResult: suspend (String?) -> Unit = {},
    onDismiss: suspend () -> Unit,
) {
    @Suppress("DEPRECATION")
    val fragmentManager = OctoAppTheme.fragmentManager
    val activity = OctoAppTheme.octoActivity
    val instanceId = LocalOctoPrint.current.id
    val scope = rememberCoroutineScope()
    fun runSafe(block: suspend () -> Unit) = scope.launch {
        try {
            block()
        } catch (e: Exception) {
            activity?.showDialog(e)
            Napier.e(tag = "MenuSheet", throwable = e, message = "Failed to run safe")
        }
    }

    DisposableEffect(menu.id) {
        val fragment = MenuBottomSheetFragment.createForMenu(
            menu = menu,
            menuId = menuId,
            onDismiss = { runSafe { onDismiss() } },
            onResult = { runSafe { onResult(it) } },
            instanceId = instanceId
        )

        val tag = fragment.menuInstanceId
        fragment.show(fragmentManager(), tag)

        onDispose {
            val toRemove = fragmentManager().findFragmentByTag(tag) ?: fragment
            (toRemove as? MenuBottomSheetFragment)?.dismissAllowingStateLoss()
        }
    }
}
