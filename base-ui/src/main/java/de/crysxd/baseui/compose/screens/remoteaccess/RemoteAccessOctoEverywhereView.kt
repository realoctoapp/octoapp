package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.net.toUri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoButtonType
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.connectCore
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.viewmodels.RemoteAccessBaseViewModelCore
import de.crysxd.octoapp.viewmodels.RemoteAccessOctoEverywhereViewModelCore

@Composable
fun RemoteAccessOctoEverywhereView(
    controller: ConfigureOctoEverywhereViewController = rememberOctoEverywhereController()
) = RemoteAccessServiceView(
    controller = controller,
    backgroundColor = colorResource(id = R.color.octoeverywhere),
    titleBarBackgroundColor = Color(0xFF25272a),
    boxBackgroundColor = colorResource(id = R.color.white_translucent_3),
    foregroundColor = OctoAppTheme.colors.textColoredBackground,
    titleLogo = painterResource(R.drawable.octoeverywhere_logo),
    descriptionText = stringResource(R.string.configure_remote_acces___octoeverywhere___description_1),
    modifier = Modifier,
    usps = listOf(
        RemoteAccessUsp(
            icon = R.drawable.ic_round_network_check_24,
            description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_1)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_videocam_24,
            description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_2)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_star_24,
            description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_3)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_lock_24,
            description = stringResource(R.string.configure_remote_acces___octoeverywhere___usp_4)
        )
    )
) { state ->
    Column(
        modifier = Modifier.padding(horizontal = OctoAppTheme.dimens.margin2),
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin2),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        val buttonColor = Color(0xFF6D9AF5)

        if (state.connected) {
            RemoteAccessConnected(
                text = stringResource(id = R.string.configure_remote_acces___octoeverywhere___connected),
            )

            OctoButton(
                text = stringResource(id = R.string.configure_remote_acces___octoeverywhere___disconnect_button),
                onClick = { controller.disconnect() },
                loading = state.loading,
                type = OctoButtonType.Special(
                    background = { Color.White },
                    foreground = { buttonColor },
                    shape = { RoundedCornerShape(percent = 50) },
                    stroke = { buttonColor },
                    suppressShadow = false,
                )
            )
        } else {
            val activity = OctoAppTheme.octoActivity
            OctoButton(
                text = stringResource(id = R.string.configure_remote_acces___octoeverywhere___connect_button),
                onClick = {
                    val uri = controller.getLoginUrl()?.toUri() ?: return@OctoButton
                    activity?.showDialog(
                        OctoActivity.Message.DialogMessage(
                            title = { getString(R.string.configure_remote_acces___leaving_app___title) },
                            text = { getString(R.string.configure_remote_acces___leaving_app___text, "OctoEverywhere") },
                            positiveButton = { getString(R.string.sign_in___continue) },
                            positiveAction = { uri.open() },

                            )
                    )
                },
                loading = state.loading,
                type = OctoButtonType.Special(
                    background = { buttonColor },
                    foreground = { Color.White },
                    shape = { RoundedCornerShape(percent = 50) },
                    stroke = { Color.Transparent },
                    suppressShadow = false
                )
            )
        }
    }
}

//region State
@Composable
private fun rememberOctoEverywhereController(): ConfigureOctoEverywhereViewController {
    val instanceId = LocalOctoPrint.current.id
    val vmFactory = ConfigureOctoEverywhereViewModel.Factory(instanceId)
    val viewModel = viewModel<ConfigureOctoEverywhereViewModel>(
        factory = vmFactory,
        key = vmFactory.id,
    )
    return object : ConfigureOctoEverywhereViewController {
        override val state by viewModel.connectedState.collectAsState(
            initial = RemoteAccessBaseViewModelCore.State(
                connected = false,
                loading = false,
                failure = null,
                url = null
            )
        )

        override suspend fun getLoginUrl() = viewModel.getLoginUrl()
        override suspend fun disconnect() = viewModel.disconnect()
    }
}

interface ConfigureOctoEverywhereViewController : ConfigureRemoteAccessViewController {
    suspend fun getLoginUrl(): String?
}

private class ConfigureOctoEverywhereViewModel(instanceId: String) : BaseViewModel() {
    val core = connectCore(RemoteAccessOctoEverywhereViewModelCore(instanceId))
    val connectedState = core.connectedState

    suspend fun getLoginUrl() = core.getLoginUrl()
    suspend fun disconnect() = core.disconnect()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ConfigureOctoEverywhereViewModel(
            instanceId = instanceId,
        ) as T
    }
}

//endregion
//region Preview
@Composable
private fun PreviewBase(connected: Boolean, failure: RemoteConnectionFailure?) = OctoAppThemeForPreview {
    RemoteAccessOctoEverywhereView(
        controller = object : ConfigureOctoEverywhereViewController {
            override val state = RemoteAccessBaseViewModelCore.State(connected = connected, loading = false, failure = failure, url = null)
            override suspend fun getLoginUrl(): String? = null
            override suspend fun disconnect() = false
        }
    )
}

@Composable
@Preview(showSystemUi = true)
private fun PreviewIdle() = PreviewBase(connected = false, failure = null)

@Composable
@Preview(showSystemUi = true)
private fun PreviewFailure() = PreviewBase(
    connected = false,
    failure = RemoteConnectionFailure(
        errorMessage = "Some",
        errorMessageStack = "",
        stackTrace = "",
        remoteServiceName = "",
        dateMillis = 0
    )
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewExpired() = PreviewBase(
    connected = false,
    failure = RemoteConnectionFailure(
        errorMessage = "Some",
        errorMessageStack = "",
        stackTrace = "",
        remoteServiceName = "",
        dateMillis = 0,
        message = "OctoEverywhere supporter status expired",
        learnMoreUrl = "https://octoeverywhere.com/appportal/v1/nosupporterperks?appid=octoapp"
    )
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnected() = PreviewBase(connected = true, failure = null)
//endregion