package de.crysxd.baseui.compose.controls.movetool

import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Rect
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.animateIntOffsetAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ActionButton
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.framework.components.EnterValueDialogInputConfig
import de.crysxd.baseui.compose.framework.components.OctoDataButton
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.components.rememberEnterValueDialogProvider
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.controls.MoveControlsSettingsMenu
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import de.crysxd.octoapp.viewmodels.MoveControlsViewModelCore
import de.crysxd.octoapp.viewmodels.MoveControlsViewModelCore.Axis
import de.crysxd.octoapp.viewmodels.MoveControlsViewModelCore.Direction
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

@Composable
fun MoveToolControls(
    state: MoveToolControlsState,
    editState: EditState,
) = AnimatedVisibility(
    visible = state.visible || editState.editing,
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically()
) {
    ControlsScaffold(
        title = stringResource(id = R.string.widget_move),
        editState = editState,
        actionArea = {
            var showSettings by remember { mutableStateOf(false) }
            if (showSettings) {
                MenuSheet(
                    menu = MoveControlsSettingsMenu(LocalOctoPrint.current.id),
                    onDismiss = { showSettings = false }
                )
            }

            if (!state.limitedControls && state.steppersEnabled != false) {
                ActionButton(
                    painter = painterResource(id = R.drawable.ic_round_power_off_24),
                    onClick = { state.turnMotorsOff() }
                )
            }

            ActionButton(
                painter = painterResource(id = R.drawable.ic_round_settings_24),
                onClick = { showSettings = true }
            )
        },
        content = {
            ControlsRow {
                ControlsXY(
                    modifier = Modifier.box(),
                    state = state
                )
                ControlsZ(
                    modifier = Modifier.box(),
                    state = state
                )
                ResolutionSelector(
                    state = state,
                    modifier = Modifier.box()
                )
            }

            state.realTimePosition?.let { position ->
                RealTimePosition(
                    position = position,
                    onMoveToPosition = if (state.canMoveToPosition) {
                        { x, y, z -> state.moveTo(x, y, z) }
                    } else {
                        null
                    }
                )
            }
        }
    )
}

interface MoveToolControlsState {
    val resolutionOptions: List<Float> get() = listOf(100f, 10f, 1f, 0.1f, 0.025f)
    var selectedResolution: Float
    val visible: Boolean
    val limitedControls: Boolean
    var steppersEnabled: Boolean?
    val realTimePosition: MoveControlsViewModelCore.Position?
    val canMoveToPosition: Boolean
    val xyHomed: Boolean?
    val zHomed: Boolean?

    fun jog(axis: Axis, direction: Direction)
    suspend fun homeXYAxis()
    suspend fun homeZAxis()
    suspend fun moveTo(x: Float, y: Float, z: Float)
    suspend fun turnMotorsOff()
}

private interface ControlsRowScope {
    val arrowIcon: Painter
    val homeIcon: Painter
}

@Composable
fun rememberMoveToolControlState(): MoveToolControlsState {
    val vmFactory = MoveToolControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = MoveToolControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )
    val state by vm.state.collectAsState(initial = vm.getState())

    return object : MoveToolControlsState {
        override var selectedResolution by rememberSaveable { mutableStateOf(resolutionOptions[1]) }
        override val visible get() = state.visible
        override val limitedControls get() = state.limitedControls
        override val realTimePosition = state.position
        override val canMoveToPosition = state.canMoveToPosition
        override val xyHomed = state.xyHomed
        override val zHomed = state.zHomed
        override var steppersEnabled = state.steppersEnabled


        override fun jog(axis: Axis, direction: Direction) {
            vm.move(axis = axis, direction = direction, distance = selectedResolution.toDouble())
        }

        override suspend fun moveTo(x: Float, y: Float, z: Float) {
            vm.moveToPosition(x = x, y = y, z = z)
        }

        override suspend fun homeXYAxis() {
            vm.homeXYAxis()
        }

        override suspend fun homeZAxis() {
            vm.homeZAxis()
        }

        override suspend fun turnMotorsOff() {
            vm.turnMotorsOff()
        }
    }
}

//region Internals

@Composable
private fun RealTimePosition(
    position: MoveControlsViewModelCore.Position,
    onMoveToPosition: (suspend (Float, Float, Float) -> Unit)?
) {
    val provider = rememberEnterValueDialogProvider()
    val title = stringResource(id = R.string.move_controls___move_to_position)
    val confirmButton = stringResource(id = R.string.move)
    val error = stringResource(id = R.string.error_please_enter_a_value)

    OctoDataButton(
        icon = painterResource(id = R.drawable.ic_round_edit_24),
        contentDescription = "Move to position",
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin12),
        onClick = onMoveToPosition?.run {
            suspend {
                fun String.parse() = replace(",", ".").toFloatOrNull()
                fun String.validate() = if (parse() == null) error else null

                provider.requestEnterValues(
                    title = title,
                    confirmButton = confirmButton,
                    inputs = listOf(
                        EnterValueDialogInputConfig(
                            label = "X",
                            labelActive = "X",
                            value = position.x?.format(maxDecimals = 1) ?: "",
                            keyboard = InputMenuItem.KeyboardType.Numbers,
                            validator = { it.validate() }
                        ),
                        EnterValueDialogInputConfig(
                            label = "Y",
                            labelActive = "Y",
                            value = position.y?.format(maxDecimals = 1) ?: "",
                            keyboard = InputMenuItem.KeyboardType.Numbers,
                            validator = { it.validate() }
                        ),
                        EnterValueDialogInputConfig(
                            label = "Z",
                            labelActive = "Z",
                            value = position.z?.format(maxDecimals = 1) ?: "",
                            keyboard = InputMenuItem.KeyboardType.Numbers,
                            validator = { it.validate() }
                        )
                    )
                )?.map {
                    it.parse()
                }?.let { inputs ->
                    onMoveToPosition(
                        inputs[0] ?: return@let,
                        inputs[1] ?: return@let,
                        inputs[2] ?: return@let,
                    )
                }

                Unit
            }
        },
    ) {
        Row(
            modifier = Modifier
                .clip(MaterialTheme.shapes.large)
                .background(OctoAppTheme.colors.inputBackground)
                .padding(horizontal = OctoAppTheme.dimens.margin12),
            horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin12),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            val alphaAnimation = remember { Animatable(1f) }
            val anyUnavailable = position.x == null || position.y == null || position.z == null
            LaunchedEffect(anyUnavailable) {
                while (anyUnavailable) {
                    alphaAnimation.animateTo(0.4f, tween(1000))
                    alphaAnimation.animateTo(1f, tween(1000))
                }
            }

            PositionComponent(label = "X", value = position.x) { alphaAnimation.value }
            PositionComponent(label = "Y", value = position.y) { alphaAnimation.value }
            PositionComponent(label = "Z", value = position.z) { alphaAnimation.value }
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}


@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun PositionComponent(
    label: String,
    value: Float?,
    unavailableAlpha: () -> Float,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin0)
) {
    Text(
        text = label,
        style = OctoAppTheme.typography.base,
        color = OctoAppTheme.colors.darkText
    )
    AnimatedContent(
        targetState = value?.format(minDecimals = 2, maxDecimals = 2),
        transitionSpec = { fadeIn() with fadeOut() }
    ) { target ->
        Text(
            text = target ?: "???",
            style = OctoAppTheme.typography.label,
            color = OctoAppTheme.colors.normalText,
            modifier = if (target == null) Modifier.graphicsLayer { alpha = unavailableAlpha() } else Modifier
        )
    }
}

@Composable
private fun ControlsRow(
    content: @Composable ControlsRowScope.() -> Unit,
) {
    val scope = object : ControlsRowScope {
        override val arrowIcon = painterResource(id = R.drawable.ic_round_keyboard_arrow_up_24)
        override val homeIcon = painterResource(id = R.drawable.ic_round_home_24)
    }

    SubcomposeLayout { constraints ->
        val width = constraints.maxWidth
        val height = subcompose(slotId = 0) { scope.content() }.map { it.measure(constraints) }.maxOf { it.height }
        val placeables = subcompose(slotId = 1) { scope.content() }.map { it.measure(constraints.copy(minHeight = height, maxHeight = height)) }
        val gap = (width - placeables.sumOf { it.width }) / (placeables.size - 1).toFloat()

        layout(width, height) {
            var x = 0f
            placeables.forEach {
                it.place(x.roundToInt(), 0)
                x += it.width + gap
            }
        }
    }
}

fun Modifier.box() = composed {
    clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground)
}

@Composable
private fun ControlsRowScope.ControlsXY(
    modifier: Modifier,
    state: MoveToolControlsState
) = Column(
    modifier
        .drawAxis(verticalAxisLabel = "Y", horizonAxisLabel = "X")
        .padding(OctoAppTheme.dimens.margin1)
        .height(IntrinsicSize.Min)
        .width(IntrinsicSize.Min)
) {
    Row(modifier = Modifier.weight(1f)) {
        RowSpacer()
        Button(
            painter = arrowIcon,
            contentDescription = stringResource(id = R.string.cd_move_plus_y),
            onClick = { state.jog(axis = Axis.Y, direction = Direction.Positive) },
            animationDirectionY = -1,
            painterRotation = 0,
            enabled = state.xyHomed != false,
        )
        RowSpacer()
    }

    Row(modifier = Modifier.weight(1f)) {
        Button(
            painter = arrowIcon,
            painterRotation = 270,
            contentDescription = stringResource(id = R.string.cd_move_minus_x),
            onClick = { state.jog(axis = Axis.X, direction = Direction.Negative) },
            animationDirectionX = -1,
            enabled = state.xyHomed != false,
        )
        Button(
            painter = homeIcon,
            painterRotation = 0,
            contentDescription = stringResource(id = R.string.cd_home_xy),
            onClick = state::homeXYAxis,
            enabled = !state.limitedControls
        )
        Button(
            painter = arrowIcon,
            painterRotation = 90,
            contentDescription = stringResource(id = R.string.cd_move_plus_x),
            onClick = { state.jog(axis = Axis.X, direction = Direction.Positive) },
            animationDirectionX = 1,
            enabled = state.xyHomed != false,
        )
    }

    Row(modifier = Modifier.weight(1f)) {
        RowSpacer()
        Button(
            painter = arrowIcon,
            painterRotation = 180,
            contentDescription = stringResource(id = R.string.cd_move_minus_y),
            onClick = { state.jog(axis = Axis.Y, direction = Direction.Negative) },
            animationDirectionY = 1,
            enabled = state.xyHomed != false,
        )
        RowSpacer()
    }
}

@Composable
private fun ControlsRowScope.ControlsZ(
    modifier: Modifier,
    state: MoveToolControlsState
) = Column(
    modifier
        .drawAxis(verticalAxisLabel = "Z")
        .padding(OctoAppTheme.dimens.margin1)
        .height(IntrinsicSize.Min)
        .width(IntrinsicSize.Min)
) {
    Button(
        painter = arrowIcon,
        painterRotation = 0,
        contentDescription = stringResource(id = R.string.cd_move_z_up),
        onClick = { state.jog(axis = Axis.Z, direction = Direction.Positive) },
        animationDirectionY = -1,
        modifier = Modifier.weight(1f),
        enabled = state.zHomed != false,
    )

    Button(
        painter = homeIcon,
        painterRotation = 0,
        contentDescription = stringResource(id = R.string.cd_home_z),
        onClick = state::homeZAxis,
        modifier = Modifier.weight(1f),
        enabled = !state.limitedControls
    )

    Button(
        painter = arrowIcon,
        painterRotation = 180,
        contentDescription = stringResource(id = R.string.cd_move_z_down),
        onClick = { state.jog(axis = Axis.Z, direction = Direction.Negative) },
        animationDirectionY = 1,
        modifier = Modifier.weight(1f),
        enabled = state.zHomed != false,
    )
}

@Composable
private fun RowScope.RowSpacer() = Spacer(modifier = Modifier.weight(1f))

@Composable
private fun ResolutionSelector(
    state: MoveToolControlsState,
    modifier: Modifier
) = Column(
    modifier = modifier.height(IntrinsicSize.Min),
    verticalArrangement = Arrangement.Center
) {
    val diameterRange = 8.dp..22.dp
    val diameterStep = (diameterRange.endInclusive - diameterRange.start) / state.resolutionOptions.size

    state.resolutionOptions.forEachIndexed { index, mm ->
        ResolutionOption(
            selected = state.selectedResolution == mm,
            resolutionMm = mm,
            diameterDp = diameterRange.endInclusive - diameterStep * index,
            onClick = { state.selectedResolution = mm }
        )
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun ColumnScope.ResolutionOption(
    selected: Boolean,
    resolutionMm: Float,
    diameterDp: Dp,
    onClick: () -> Unit,
) = AnimatedContent(
    targetState = selected,
    modifier = Modifier
        .weight(1f)
        .width(100.dp)
        .clickable(onClick = onClick),
    transitionSpec = {
        fadeIn() + scaleIn(spring(dampingRatio = 0.6f, stiffness = 400f)) with fadeOut() + scaleOut()
    }
) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Center
    ) {
        if (it) {
            Text(
                text = resolutionMm.formatAsLength(onlyMilli = true, maxDecimals = 4),
                style = OctoAppTheme.typography.label,
                color = OctoAppTheme.colors.darkText
            )
        } else {
            Box(
                modifier = Modifier
                    .clip(CircleShape)
                    .size(diameterDp)
                    .background(OctoAppTheme.colors.primaryButtonBackground)
            )
        }
    }
}

private fun Modifier.drawAxis(verticalAxisLabel: String, horizonAxisLabel: String? = null) = composed {
    val color = OctoAppTheme.colors.darkText.copy(alpha = 0.3f)
    val bounds = remember { Rect() }
    val matrix = remember { Matrix() }
    val textDimension = remember { FloatArray(2) }
    val textPaint = remember {
        Paint().apply {
            setColor(color.toArgb())
        }
    }

    drawBehind {
        drawIntoCanvas {
            val dashHeight = 14.dp.toPx()
            val dashWidth = 1.dp.toPx()
            val dashPadding = 2.dp.toPx()

            fun drawAxisInternal(label: String, labelRotation: Float = 0f) {
                textPaint.textSize = dashHeight * 0.6f
                textPaint.getTextBounds(label, 0, label.length, bounds)
                textDimension[0] = bounds.width().toFloat()
                textDimension[1] = bounds.height().toFloat()
                matrix.reset()
                matrix.postRotate(labelRotation)
                matrix.mapPoints(textDimension)
                val textWidth = textDimension[0]
                val textHeight = textDimension[1]
                val textXStart = size.center.x - (textWidth / 2)
                val dashX = size.center.x - dashWidth / 2
                val textYBottom = (dashHeight / 2) + (textHeight / 2)
                val textYTop = textYBottom - textHeight
                val textRect = androidx.compose.ui.geometry.Rect(
                    offset = Offset(x = textXStart, y = textYTop),
                    size = Size(textWidth, textHeight)
                )

                drawRect(
                    color = color,
                    size = Size(dashWidth, textYTop - dashPadding),
                    topLeft = Offset(x = dashX, y = 0f)
                )
                drawRect(
                    color = color,
                    size = Size(dashWidth, dashHeight - textYBottom - dashPadding),
                    topLeft = Offset(x = dashX, y = textYBottom + dashPadding)
                )
                drawRect(
                    color = color,
                    size = Size(dashWidth, dashHeight),
                    topLeft = Offset(x = dashX, y = size.height - dashHeight)
                )

                rotate(degrees = labelRotation, pivot = textRect.center) {
                    with(it.nativeCanvas) {
                        drawText(
                            label,
                            textRect.center.x - bounds.width() / 2,
                            textRect.center.y + bounds.height() / 2,
                            textPaint
                        )
                    }
                }
            }

            drawAxisInternal(label = verticalAxisLabel)


            if (horizonAxisLabel != null) {

                withTransform(transformBlock = { rotate(-90f) }) {
                    drawAxisInternal(label = horizonAxisLabel, labelRotation = 90f)
                }
            }
        }
    }
}

@Composable
private fun Button(
    painter: Painter,
    painterRotation: Int,
    contentDescription: String,
    onClick: suspend () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    animationDirectionX: Int = 0,
    animationDirectionY: Int = 0,
) {
    var targetOffset by remember { mutableStateOf(IntOffset.Zero) }
    val offset by animateIntOffsetAsState(targetValue = targetOffset, spring(dampingRatio = 0.5f, stiffness = 350f))
    val scope = rememberCoroutineScope()
    val animationRange = with(LocalDensity.current) { 15.dp.roundToPx() }
    val feedback = LocalHapticFeedback.current

    OctoIconButton(
        modifier = modifier
            .offset { offset }
            .rotate(painterRotation.toFloat()),
        enabled = enabled,
        onClick = {
            onClick()

            if (animationDirectionX != 0 || animationDirectionY != 0) {
                scope.launch {
                    feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                    targetOffset = IntOffset(x = animationRange * animationDirectionX, y = animationRange * animationDirectionY)
                    delay(300)
                    feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                    targetOffset = IntOffset.Zero
                }
            }
        },
        painter = painter,
        contentDescription = contentDescription,
    )
}
//endregion

//region Preview
@Composable
@Preview
private fun PreviewOctoPrint() = OctoAppThemeForPreview {
    MoveToolControls(
        editState = EditState.ForPreview,
        state = object : MoveToolControlsState {
            override val visible = true
            override val limitedControls = false
            override var steppersEnabled: Boolean? = null
            override val realTimePosition = null
            override var canMoveToPosition = false
            override val xyHomed: Boolean? = null
            override val zHomed: Boolean? = null
            override var selectedResolution by remember { mutableStateOf(resolutionOptions[1]) }
            override fun jog(axis: Axis, direction: Direction) = Unit
            override suspend fun moveTo(x: Float, y: Float, z: Float) = Unit
            override suspend fun homeXYAxis() = Unit
            override suspend fun homeZAxis() = Unit
            override suspend fun turnMotorsOff() = Unit
        },
    )
}

@Composable
@Preview
private fun PreviewKlipper() = OctoAppThemeForPreview {
    MoveToolControls(
        editState = EditState.ForPreview,
        state = object : MoveToolControlsState {
            override val visible = true
            override val limitedControls = false
            override val realTimePosition = MoveControlsViewModelCore.Position(123.1111f, 43.231f, 98.32f)
            override var canMoveToPosition = true
            override var steppersEnabled: Boolean? = true
            override val xyHomed = true
            override val zHomed = false
            override var selectedResolution by remember { mutableStateOf(resolutionOptions[1]) }
            override fun jog(axis: Axis, direction: Direction) = Unit
            override suspend fun moveTo(x: Float, y: Float, z: Float) = Unit
            override suspend fun homeXYAxis() = Unit
            override suspend fun homeZAxis() = Unit
            override suspend fun turnMotorsOff() = Unit
        },
    )
}
//endregion