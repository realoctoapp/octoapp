package de.crysxd.baseui.compose.framework.layout

import android.app.Dialog
import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.WindowManager
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.core.view.ViewCompat.setOnApplyWindowInsetsListener
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsCompat.Type.systemBars
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.children
import androidx.lifecycle.lifecycleScope
import de.crysxd.baseui.compose.framework.locals.WithOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.requireOctoActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


abstract class ComposeBottomSheetFragment : AppCompatDialogFragment() {

    abstract val instanceId: String?
    private lateinit var composeView: ComposeView
    private var insets by mutableStateOf(Rect())
    private var visible by mutableStateOf(false)
    private lateinit var insetController: WindowInsetsControllerCompat

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DialogLayout(requireContext()).apply {
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        clipToPadding = false
        clipChildren = false

        insetController = WindowInsetsControllerCompat(dialog!!.window!!, this).apply {
            isAppearanceLightStatusBars = false
            isAppearanceLightNavigationBars = true
        }

        composeView = ComposeView(requireContext())
        addView(composeView, ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT))
        composeView.setContent {
            OctoAppTheme(
                viewModelStoreOwner = this@ComposeBottomSheetFragment,
                octoActivity = requireOctoActivity(),
                fragmentManager = { childFragmentManager }
            ) {
                WithOctoPrint(instanceId = instanceId) {
                    Wrapper()
                }
            }
        }
    }

    @Composable
    private fun Wrapper() = with(LocalDensity.current) {
        val enterFraction by animateFloatAsState(targetValue = if (visible) 1f else 0f)
        var dragOffset by remember { mutableStateOf(0f) }
        val navBarScrimAlpha = 1f //by animateFloatAsState(if (!canHaveLightNavBar() || dragOffset > OctoAppTheme.dimens.margin1.toPx()) 1f else 0f)
        var windowHeight by remember { mutableStateOf(1000.dp) }

        Box(
            contentAlignment = Alignment.BottomCenter,
            modifier = Modifier
                .fillMaxSize()
                .drawBehind { drawRect(Color.Black.copy(alpha = 0.4f * enterFraction)) }
                .onGloballyPositioned { windowHeight = it.size.height.toDp() }
                .clickable(
                    interactionSource = MutableInteractionSource(),
                    indication = null,
                    onClick = { dismiss() }
                )
        ) {
            val dragHandleColor = OctoAppTheme.colors.lightText.copy(alpha = 0.2f)
            val dragHandleTop = OctoAppTheme.dimens.margin1
            val enterOffset = OctoAppTheme.dimens.margin4.toPx()
            val scope = rememberCoroutineScope()

            AnimatedVisibility(
                visible = visible,
                enter = slideInVertically { it },
                exit = slideOutVertically { it },
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .widthIn(max = 500.dp)
                        .heightIn(max = windowHeight * 0.9f)
                        .offset { IntOffset(x = 0, y = dragOffset.toInt()) }
                        .background(OctoAppTheme.colors.windowBackground)
                        .clickable(
                            interactionSource = MutableInteractionSource(),
                            indication = null,
                            onClick = {}
                        )
                        .draggable(
                            orientation = Orientation.Horizontal,
                            state = rememberDraggableState { delta ->
                                dragOffset = (dragOffset + delta).coerceAtLeast(0f)
                            }
                        )
                        .pointerInput(Unit) {
                            fun onDragEnd() {
                                if (dragOffset > 200.dp.toPx() || dragOffset > size.height / 2) {
                                    dismiss()
                                } else scope.launch {
                                    Animatable(initialValue = dragOffset).animateTo(0f) {
                                        dragOffset = value
                                    }
                                }
                            }
                            detectDragGestures(
                                onDragCancel = ::onDragEnd,
                                onDragEnd = ::onDragEnd,
                                onDrag = { change, delta ->
                                    change.consume()
                                    dragOffset = (dragOffset + delta.y).coerceAtLeast(0f)
                                },
                            )
                        }
                        .drawBehind {
                            val handleWidth = 80.dp.toPx()
                            val handleHeight = 3.dp.toPx()
                            val x = (size.width - handleWidth) / 2
                            val y = dragHandleTop.toPx() - handleHeight / 2
                            drawRoundRect(
                                color = dragHandleColor,
                                topLeft = Offset(x, y),
                                size = Size(handleWidth, handleHeight),
                                cornerRadius = CornerRadius(x = handleHeight / 2, y = handleHeight / 2),
                                alpha = enterFraction
                            )
                        }
                        .padding(
                            start = insets.left.toDp(),
                            end = insets.right.toDp(),
                            bottom = insets.bottom.toDp(),
                            top = OctoAppTheme.dimens.margin2
                        )
                        .graphicsLayer {
                            translationY = enterOffset * (1 - enterFraction)
                        }
                ) {
                    Content()
                }
            }

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(insets.bottom.toDp())
                    .graphicsLayer {
                        alpha = navBarScrimAlpha
                        scaleY = navBarScrimAlpha
                        transformOrigin = TransformOrigin(pivotFractionX = 0.5f, pivotFractionY = 1f)
                    }
                    .background(Color.Black.copy(alpha = 0.15f))
                    .align(Alignment.BottomCenter)
            )
        }

        LaunchedEffect(Unit) {
            visible = true
        }
    }

    @Composable
    abstract fun Content()

    @CallSuper
    open fun onBackPressed(): Boolean = false

    override fun onCreateDialog(savedInstanceState: Bundle?) = Dialog(requireActivity(), android.R.style.Theme_Translucent_NoTitleBar).apply {
        with(window!!) {
            setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            setOnApplyWindowInsetsListener(decorView) { _, i ->
                val si = i.getInsets(systemBars())
                insets = Rect(si.left, si.top, si.right, si.bottom)
                WindowInsetsCompat.CONSUMED
            }

            WindowCompat.setDecorFitsSystemWindows(this, false)
            attributes.windowAnimations = de.crysxd.baseui.R.style.OctoTheme_BottomSheetAnimation

            navigationBarColor = Color.White.toArgb()
            statusBarColor = Color.Transparent.toArgb()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                isNavigationBarContrastEnforced = false
                isStatusBarContrastEnforced = false
            }
        }

        setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP && !this@ComposeBottomSheetFragment.onBackPressed()) {
                this@ComposeBottomSheetFragment.dismiss()
            }
            true
        }
    }

    override fun dismiss() {
        visible = false
        lifecycleScope.launchWhenCreatedFixed {
            delay(300)
            super.dismissAllowingStateLoss()
        }
    }

    override fun dismissAllowingStateLoss() {
        dismiss()
    }

    class DialogLayout(context: Context) : ViewGroup(context) {

        override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
            children.forEach {
                it.layout(0, 0, r - l, b - t)
            }
        }

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            children.forEach {
                it.measure(
                    MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY),
                )
            }

            setMeasuredDimension(
                MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec)
            )
        }

        override fun generateDefaultLayoutParams() = LayoutParams(MATCH_PARENT, MATCH_PARENT)

    }
}