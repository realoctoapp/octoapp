package de.crysxd.baseui.compose.screens.pluginlibrary

import androidx.annotation.DrawableRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.AnnouncementId
import de.crysxd.baseui.compose.framework.components.OctoAnnouncementController
import de.crysxd.baseui.compose.framework.components.OctoLinkButtonRow
import de.crysxd.baseui.compose.framework.components.OctoListItem
import de.crysxd.baseui.compose.framework.components.OctoScrollableTabRow
import de.crysxd.baseui.compose.framework.components.SmallAnnouncement
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.layout.StateBox
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.utils.ThemePlugin
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.viewmodels.PluginsLibraryViewModelCore
import io.noties.markwon.Markwon
import kotlinx.coroutines.launch

@Composable
@OptIn(ExperimentalFoundationApi::class)
fun PluginLibraryScreen(
    controller: PluginLibraryController = rememberPluginLibraryController(),
    initialCategory: String? = null,
) {
    val scope = rememberCoroutineScope()
    val state = rememberPagerState(
        initialPage = initialCategory?.let { ic ->
            PluginsLibraryViewModelCore.baseIndex.categories.indexOfFirst { it.id == ic }.takeIf { it >= 0 }
        } ?: 0,
        pageCount = { PluginsLibraryViewModelCore.baseIndex.categories.size }
    )

    CollapsibleHeaderScreenScaffold(
        header = { Header() },
        tabs = {
            Tabs(
                selectedTab = state.targetPage,
                onTabSelected = { scope.launch { state.animateScrollToPage(it) } }
            )
        },
        content = {
            HorizontalPager(state) { page ->
                StateBox(
                    state = controller.index,
                    isEmpty = { it.categories.isEmpty() },
                    emptyText = "No plugins found",
                ) { index ->
                    Content(
                        category = index.categories[page]
                    )
                }
            }
        }
    )
}

//region Content
@Composable
private fun Tabs(
    modifier: Modifier = Modifier,
    selectedTab: Int,
    onTabSelected: (Int) -> Unit,
) = OctoScrollableTabRow(
    modifier = Modifier.padding(vertical = OctoAppTheme.dimens.margin1),
    selectedIndex = selectedTab,
    options = PluginsLibraryViewModelCore.baseIndex.categories.map { it.name },
    onSelection = onTabSelected,
)

@Composable
private fun Header(modifier: Modifier = Modifier) = Column(
    modifier = modifier
        .padding(top = OctoAppTheme.dimens.margin5, bottom = OctoAppTheme.dimens.margin2)
        .padding(horizontal = OctoAppTheme.dimens.margin2),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1),
) {
    Text(
        text = stringResource(R.string.plugin_library___title),
        style = OctoAppTheme.typography.titleBig,
        color = OctoAppTheme.colors.darkText,
    )

    Text(
        text = stringResource(R.string.plugin_library___description),
        style = OctoAppTheme.typography.label,
        color = OctoAppTheme.colors.normalText,
        textAlign = TextAlign.Center,
    )

    AnimatedVisibility(LocalOctoPrint.current.hasCompanionPlugin() == false) {
        OctoAnnouncementController(announcementId = AnnouncementId.Default("companion_plugin_announcement_in_library")) {
            SmallAnnouncement(
                text = stringResource(R.string.main_menu___companion_accouncement___subtitle).toHtml().asAnnotatedString(),
                learnMoreButton = stringResource(R.string.plugin_library___plugin_page),
                onLearnMore = { "https://plugins.octoprint.org/plugins/octoapp/".toUrl().open() },
                hideButton = null,
                backgroundColor = OctoAppTheme.colors.yellowTranslucent,
                foregroundColor = OctoAppTheme.colors.darkText,
            )
        }
    }
}

@Composable
private fun Content(category: PluginsLibraryViewModelCore.PluginCategory) {
    val context = LocalContext.current
    val markdown = remember {
        Markwon.builder(context)
            .usePlugin(ThemePlugin(context))
            .build()
    }

    LazyColumn(
        modifier = Modifier.nestedScroll(LocalNestedScrollConnection.current),
    ) {
        items(
            items = category.plugins,
            key = { it.key }
        ) { plugin ->
            OctoListItem(
                onClick = { },
                title = {
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        if (plugin.installed == true) {
                            Badge(
                                icon = R.drawable.ic_round_check_24,
                                color = OctoAppTheme.colors.green
                            )
                        } else if (plugin.highlight) {
                            Badge(
                                icon = R.drawable.ic_round_star_24,
                                color = OctoAppTheme.colors.yellow
                            )
                        }

                        Text(
                            text = plugin.name,
                            style = OctoAppTheme.typography.title
                        )
                    }
                },
                detail = {
                    Column(verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin2)) {
                        val text = remember(plugin.description) {
                            val node = markdown.parse(plugin.description)
                            markdown.render(node)
                        }

                        Text(
                            text = text.asAnnotatedString(),
                            modifier = Modifier.padding(OctoAppTheme.dimens.margin01)
                        )

                        val options = listOf(
                            stringResource(R.string.plugin_library___configure) to plugin.configureLink,
                            stringResource(R.string.plugin_library___octoapp_tutorial) to plugin.octoAppTutorial,
                            stringResource(R.string.plugin_library___plugin_page) to plugin.pluginPage,
                        ).filter {
                            it.second != null
                        }

                        OctoLinkButtonRow(
                            alignment = Alignment.End,
                            modifier = Modifier.fillMaxWidth(),
                            texts = options.map { it.first },
                            onClicks = options.map {
                                { it.second?.open() }
                            },
                        )
                    }
                },
            )
        }
    }
}

@Composable
fun Badge(
    modifier: Modifier = Modifier,
    color: Color,
    @DrawableRes icon: Int
) = Icon(
    painter = painterResource(icon),
    contentDescription = null,
    tint = OctoAppTheme.colors.textColoredBackground,
    modifier = modifier
        .background(color, CircleShape)
        .size(18.dp, 18.dp)
        .padding(2.dp)
)

//endregion
//region Controller
@Composable
fun rememberPluginLibraryController(): PluginLibraryController {
    val factory = PluginLibraryViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = PluginLibraryViewModel::class.java,
        factory = factory,
        key = factory.id,
    )
    val index = vm.index.collectAsState(initial = FlowState.Loading())

    return remember(vm, index) {
        object : PluginLibraryController {
            override val index by index
        }
    }
}

interface PluginLibraryController {
    val index: FlowState<PluginsLibraryViewModelCore.PluginsIndex>
}

//endregion
//region Preview
@Composable
fun BasePreview(state: FlowState<PluginsLibraryViewModelCore.PluginsIndex>) = OctoAppThemeForPreview {
    PluginLibraryScreen(
        controller = object : PluginLibraryController {
            override val index get() = state
        }
    )
}

@Composable
@Preview(showSystemUi = true)
fun PreviewLoading() = BasePreview(state = FlowState.Loading())

@Composable
@Preview(showSystemUi = true)
fun PreviewError() = BasePreview(state = FlowState.Error(IllegalStateException("Test")))

@Composable
@Preview(showSystemUi = true)
fun PreviewData() {
    BasePreview(
        state = FlowState.Ready(
            PluginsLibraryViewModelCore.baseIndex
        )
    )
}
//endregion