package de.crysxd.baseui.compose.controls.webcam

import de.crysxd.baseui.OctoActivity
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration

@Suppress("FunctionName")
fun AppVisibleWatchdog(
    scope: CoroutineScope,
    interval: Duration,
    backgroundTimeout: Duration
): Flow<Boolean> {
    val tag = "AppVisibleWatchdog"
    var job: Job? = null
    val flow = MutableStateFlow(false)

    return flow.onStart {
        // Watchdog. We had a case where the webcam wasn't stopped when the app moved to the background, draining the battery
        job?.cancel()
        flow.value = false
        Napier.d(tag = tag, message = "Flow started, starting watchdog")
        job = scope.launch {
            var invisibleSince = Instant.DISTANT_FUTURE
            while (isActive) {
                delay(interval)
                val now = Clock.System.now()

                // Determine since when we are invisible
                invisibleSince = if (OctoActivity.instance?.isVisible != true) {
                    minOf(now, invisibleSince)
                } else {
                    Instant.DISTANT_FUTURE
                }

                if (now >= invisibleSince) {
                    Napier.d(tag = tag, message = "App not visible since $invisibleSince")
                } else {
                    Napier.v(tag = tag, message = "App visible")
                    flow.value = false
                }

                // Stop if timed out
                if (now > invisibleSince + backgroundTimeout) {
                    if (flow.subscriptionCount.value == 0) {
                        Napier.d(tag = tag, message = "App not visible but flow inactive, nothing to do")
                    } else {
                        val exception = IllegalStateException("App visible watchdog fired. Invisible since $invisibleSince, timeout is $backgroundTimeout")
                        Napier.e(tag = tag, message = "App not visible but flow active, cancelling now", throwable = exception)
                        flow.value = true
                    }
                }
            }
        }
    }.onCompletion {
        job?.cancel()
        Napier.d(tag = tag, message = "Flow ended, stopping watchdog")
    }
}