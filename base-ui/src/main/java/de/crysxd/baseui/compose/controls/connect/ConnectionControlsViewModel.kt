package de.crysxd.baseui.compose.controls.connect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.octoapp.base.usecase.ConnectPrinterUseCase
import de.crysxd.octoapp.viewmodels.ConnectionControlsViewModelCore

class ConnectionControlsViewModel(
    private val instanceId: String
) : ViewModel() {

    private val core = ConnectionControlsViewModelCore(instanceId = instanceId)
    val state = core.state

    suspend fun executeAction(action: ConnectPrinterUseCase.ActionType) = core.executeAction(action)

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ConnectionControlsViewModel(instanceId = instanceId) as T
    }
}