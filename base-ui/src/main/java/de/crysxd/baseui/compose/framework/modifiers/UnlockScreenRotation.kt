package de.crysxd.baseui.compose.framework.modifiers

import android.content.pm.ActivityInfo
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import de.crysxd.baseui.compose.theme.LocalOctoActivity

fun Modifier.unlockScreenRotation() = composed {
    val activity = LocalOctoActivity.current
    DisposableEffect(Unit) {
        val backup = activity?.requestedOrientation ?: ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_USER
        onDispose {
            activity?.requestedOrientation = backup
        }
    }
    this
}


