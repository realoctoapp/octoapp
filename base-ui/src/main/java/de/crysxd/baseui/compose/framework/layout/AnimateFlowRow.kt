package de.crysxd.baseui.compose.framework.layout

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.AnimationVector2D
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toOffset
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

@Composable
fun AnimatedFlowRow(
    modifier: Modifier = Modifier,
    mainAxisSpacing: Dp = 0.dp,
    crossAxisSpacing: Dp = 0.dp,
    content: AnimatedFlowRowScope.() -> Unit,
) {
    val positions = remember { mutableStateMapOf<Any?, Offset>() }
    val animations = remember { mutableStateMapOf<Any?, Animatable<Offset, AnimationVector2D>>() }
    val alphaValues = remember { mutableStateMapOf<Any?, Animatable<Float, AnimationVector1D>>() }
    val coroutine = rememberCoroutineScope()
    val initialAlpha = remember { mutableStateOf(1f) }

    Layout(
        modifier = modifier,
        measurePolicy = { measurables, constraints ->
            val placeables = measurables.map { it.layoutId to it.measure(constraints) }

            val width = constraints.maxWidth
            var availableRowSpace = width
            var rowTop = 0
            var rowBottom = 0
            val xPadding = mainAxisSpacing.roundToPx()
            val yPadding = crossAxisSpacing.roundToPx()

            //region Position calculations
            val placedPositions = placeables.map {
                val (key, size) = it
                if (availableRowSpace - size.width > 0) {
                    val x = width - availableRowSpace
                    val y = rowTop
                    availableRowSpace -= size.width + xPadding
                    rowBottom = (rowTop + size.height).coerceAtLeast(rowBottom)
                    key to IntOffset(x = x, y = y)
                } else {
                    rowTop = if (rowBottom == 0) 0 else rowBottom + yPadding
                    availableRowSpace = width - size.width - xPadding
                    rowBottom = rowTop + size.height
                    key to IntOffset(x = 0, y = rowTop)
                }
            }
            //endregion

            //region Position animations
            placedPositions.forEach {
                val (key, position) = it
                val newPosition = position.toOffset()
                val oldPosition = positions[key]
                positions[key] = newPosition
                oldPosition ?: return@forEach

                // Skip if we haven't been moved
                if (newPosition.toInt() != oldPosition.toInt()) {
                    val anim = animations.getOrPut(key) { Animatable(Offset.Zero, Offset.VectorConverter) }
                    coroutine.launch {
                        anim.snapTo(oldPosition - newPosition)
                        anim.animateTo(Offset.Zero, animationSpec = spring())
                    }
                }
            }
            //endregion

            layout(constraints.maxWidth, rowBottom) {
                placeables.forEach { p ->
                    val position = placedPositions.firstOrNull { it.first == p.first }?.second ?: return@forEach
                    p.second.place(position)
                }
            }
        },
        content = {
            val items = mutableListOf<Pair<Any, @Composable () -> Unit>>()
            val scope = object : AnimatedFlowRowScope {
                override fun item(key: Any, content: @Composable () -> Unit) {
                    require(items.none { it.first == key }) { "Key $key was already used but must be unique" }
                    items += key to content
                }
            }

            scope.content()

            items.forEach { item ->
                val (key, compose) = item
                Box(
                    modifier = Modifier
                        .layoutId(key)
                        .graphicsLayer {
                            val offset = animations[key]?.value?.toInt() ?: IntOffset.Zero
                            val targetAlpha = alphaValues.getOrPut(key) { Animatable(initialValue = initialAlpha.value, Float.VectorConverter) }.value
                            alpha = targetAlpha
                            translationX = offset.x.toFloat()
                            translationY = offset.y.toFloat()
                        }
                ) {
                    compose.invoke()
                }
            }

            //region Visibility Animations
            val keys = items.map { it.first }.toSet()
            LaunchedEffect(alphaValues.keys.toList(), keys) {
                // From now on we initialize new items with alpha 0 and fade in
                // Give a grace period of 100ms
                launch {
                    delay(100)
                    initialAlpha.value = 0f
                }

                // Remove old values no longer needed
                (alphaValues.keys - keys).forEach { alphaValues.remove(it) }
                (positions.keys - keys).forEach { positions.remove(it) }

                // Delay if we are moving something
                if (animations.any { it.value.isRunning }) {
                    delay(100)
                }

                // Animate all to 1f (doesn't do anything if they are already at 1)
                alphaValues.forEach {
                    if (it.value.value == 0f) {
                        launch {
                            it.value.animateTo(targetValue = 1f)
                        }
                    }
                }
            }
            //endregion
        }
    )
}

private fun Offset.toInt() = IntOffset(x = x.toInt(), y = y.toInt())

interface AnimatedFlowRowScope {
    fun item(key: Any, content: @Composable () -> Unit)
}

@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    var items by remember { mutableStateOf(listOf("Button 1", "Button 3", "Button 4", "Button 5", "Button 2")) }

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(1000)
            items = listOf("Button 3", "Button 1", "Button 4", "Button 5")
            delay(1000)
            items = listOf("Button 1", "Button 3", "Button 4", "Button 5", "Button 2")
            delay(1000)
            items = listOf("Button 2", "Button 3", "Button 4", "Button 5", "Button 1")
            delay(1000)
            items = listOf("Button 2", "Button 3", "Button 5")
            delay(1000)
            items = listOf("Button 3", "Button 1", "Button 4", "Button 5")
            delay(1000)
            items = listOf("Button 2", "Button 1", "Button 3", "Button 4", "Button 5")
        }
    }

    AnimatedFlowRow(
        mainAxisSpacing = OctoAppTheme.dimens.margin01,
        crossAxisSpacing = OctoAppTheme.dimens.margin01,
        modifier = Modifier.background(Color.Red)
    ) {

        items.forEach {
            item(it) {
                OctoButton(small = true, text = it) {}
            }
        }
    }
}