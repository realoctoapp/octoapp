package de.crysxd.baseui.compose.controls.multitool

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.ext.WhileSubscribedOctoDelay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class MultiToolControlsViewModel(instanceId: String) : ViewModelWithCore() {

    override val core = de.crysxd.octoapp.viewmodels.MultiToolControlsViewModelCore(instanceId = instanceId)
    val state = core.state.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribedOctoDelay,
        initialValue = core.initialState
    )

    fun activateTool(toolIndex: Int?) {
        viewModelScope.launch {
            core.setActiveTool(toolIndex)
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = MultiToolControlsViewModel(
            instanceId = instanceId
        ) as T
    }
}
