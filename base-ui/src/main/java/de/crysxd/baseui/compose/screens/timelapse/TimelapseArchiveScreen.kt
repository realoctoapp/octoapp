package de.crysxd.baseui.compose.screens.timelapse

import android.net.Uri
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.net.toUri
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoListItem
import de.crysxd.baseui.compose.framework.components.OctoPicassoImage
import de.crysxd.baseui.compose.framework.components.OctoVideoPlayer
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.layout.CollapsibleHeaderScreenScaffold
import de.crysxd.baseui.compose.framework.layout.StateBox
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.framework.modifiers.octoAppInsetsBottomPadding
import de.crysxd.baseui.compose.framework.modifiers.unlockScreenRotation
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.menu.main.octoprint.TimelapseArchiveMenu
import de.crysxd.octoapp.menu.main.octoprint.TimelapseMenu
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.viewmodels.TimelapseViewModelCore
import kotlinx.datetime.Instant
import java.io.File

@Composable
fun TimelapseArchiveScreen(
    controller: TimelapseArchiveController = rememberTimelapseArchiveState(),
) {
    var playerVideoUri by rememberSaveable { mutableStateOf<Uri?>(null) }
    BackHandler(playerVideoUri != null) {
        playerVideoUri = null
    }

    AnimatedContent(playerVideoUri) { uri ->
        when (uri) {
            null -> MainContent(controller) { playerVideoUri = it }
            else -> PlayerContent(uri)
        }
    }
}

@Composable
fun PlayerContent(
    videoUri: Uri,
) = OctoVideoPlayer(
    videoUri = videoUri,
    modifier = Modifier
        .fillMaxSize()
        .background(Color.Black)
        .unlockScreenRotation()
        .octoAppInsetsBottomPadding()
)

@Composable
private fun MainContent(
    controller: TimelapseArchiveController,
    onPlayVideo: (Uri) -> Unit,
) {
    var showMenu by remember { mutableStateOf(false) }
    if (showMenu) {
        MenuSheet(
            menu = TimelapseMenu(LocalOctoPrint.current.id, offerStartPrint = false),
            onDismiss = { showMenu = false },
        )
    }

    CollapsibleHeaderScreenScaffold(
        title = stringResource(id = R.string.timelapse_archive___title).asAnnotatedString(),
        subtitle = stringResource(id = R.string.timelapse_archive___subtitle).toHtml().asAnnotatedString(),
        headerModifier = Modifier.clickable { showMenu = true },
        onRefresh = controller::reload,
    ) {
        StateBox(
            state = controller.state,
            isEmpty = { it.rendered.isEmpty() && it.unrendered.isEmpty() },
            emptyText = stringResource(id = R.string.timelapse_archive___no_timelapse_found),
            onRetry = controller::reload
        ) { state ->
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .nestedScroll(LocalNestedScrollConnection.current),
                contentPadding = PaddingValues(vertical = OctoAppTheme.dimens.margin1)
            ) {
                items(
                    items = state.unrendered + state.rendered,
                    key = { it.name }
                ) {
                    TimelapseItem(
                        timelapseFile = it,
                        controller = controller,
                        onPlayVideo = onPlayVideo
                    )
                }
            }
        }
    }
}

//region Item
@Composable
private fun TimelapseItem(
    timelapseFile: TimelapseFile,
    controller: TimelapseArchiveController,
    onPlayVideo: (Uri) -> Unit,
) = Box(
    modifier = Modifier
        .height(intrinsicSize = IntrinsicSize.Min)
) {
    //region Menu
    var showMenu by rememberSaveable { mutableStateOf(false) }
    if (showMenu) {
        MenuSheet(
            menu = TimelapseArchiveMenu(LocalOctoPrint.current.id, timelapseFile),
            onDismiss = { showMenu = false },
            onResult = { result ->
                result ?: return@MenuSheet
                TimelapseArchiveMenu.getSharePath(result)?.let {
                    controller.share(path = it)
                }

                TimelapseArchiveMenu.getPlayPath(result)?.let {
                    onPlayVideo(File(it).toUri())
                }

                TimelapseArchiveMenu.getExportPath(result)?.let {
                    controller.export(timelapseFile = timelapseFile, path = it)
                }
            }
        )
    }
    //endregion

    OctoListItem(
        onClick = { showMenu = true },
        thumbnail = { Thumbnail(timelapseFile = timelapseFile) },
        title = {
            Text(
                maxLines = 2,
                text = timelapseFile.name,
                overflow = TextOverflow.Ellipsis
            )
        },
        detail = {
            Text(
                maxLines = 1,
                text = listOfNotNull(
                    timelapseFile.date?.format(),
                    timelapseFile.bytes.takeIf { it > 0 }?.formatAsFileSize(),
                ).joinToString()
            )
        },
    )
}

@Composable
private fun Thumbnail(
    modifier: Modifier = Modifier,
    timelapseFile: TimelapseFile
) = Box(
    contentAlignment = Alignment.Center,
    modifier = modifier
        .width(100.dp)
        .aspectRatio(3 / 2f)
) {
    if (timelapseFile.processing) {
        CircularProgressIndicator(
            color = OctoAppTheme.colors.accent,
            modifier = Modifier.scale(0.66f)
        )
    } else {
        OctoPicassoImage(
            path = timelapseFile.thumbnail,
            fallback = R.drawable.ic_round_videocam_24,
        )
    }
}

//endregion
//region State
@Composable
fun rememberTimelapseArchiveState(): TimelapseArchiveController {
    val instanceId = LocalOctoPrint.current.id
    val factory = TimelapseArchiveViewModel.Factory(instanceId)
    val viewModel = viewModel<TimelapseArchiveViewModel>(factory = factory, key = factory.id)
    val state = viewModel.state.collectAsState(initial = FlowState.Loading())
    val context = LocalContext.current

    return object : TimelapseArchiveController {
        override val state by state
        override suspend fun reload() = viewModel.reload()
        override suspend fun export(timelapseFile: TimelapseFile, path: String) = viewModel.export(context, timelapseFile, path)
        override suspend fun share(path: String) = viewModel.share(context, path)
    }
}

interface TimelapseArchiveController {
    val state: FlowState<TimelapseViewModelCore.State>
    suspend fun reload()
    suspend fun export(timelapseFile: TimelapseFile, path: String)
    suspend fun share(path: String)
}

//endregion
//region Preview
@Composable
private fun BasePreview(state: FlowState<TimelapseViewModelCore.State>) = OctoAppThemeForPreview {
    TimelapseArchiveScreen(
        controller = object : TimelapseArchiveController {
            override val state get() = state
            override suspend fun reload() = Unit
            override suspend fun export(timelapseFile: TimelapseFile, path: String) = Unit
            override suspend fun share(path: String) = Unit
        }
    )
}

@Preview(showSystemUi = true)
@Composable
private fun PreviewLoading() = BasePreview(state = FlowState.Loading())


@Preview(showSystemUi = true)
@Composable
private fun PreviewError() = BasePreview(state = FlowState.Error(IllegalStateException("Test")))


@Preview(showSystemUi = true)
@Composable
private fun PreviewEmpty() = BasePreview(state = FlowState.Ready(TimelapseViewModelCore.State(emptyList(), emptyList())))


@Preview(showSystemUi = true)
@Composable
private fun PreviewData() = BasePreview(
    state = FlowState.Ready(
        TimelapseViewModelCore.State(
            unrendered = listOf(
                TimelapseFile(
                    name = "Test Z",
                    date = null,
                    bytes = 0,
                    processing = false,
                    rendering = false,
                    recording = false,
                )
            ),
            rendered = listOf(
                TimelapseFile(
                    name = "Test A",
                    date = Instant.DISTANT_PAST,
                    bytes = 93927823,
                    processing = false,
                    rendering = false,
                    recording = false,
                ),
                TimelapseFile(
                    name = "Test B with a super long title that should break and be ellipsised for good measure",
                    date = Instant.DISTANT_PAST,
                    bytes = 93774,
                    processing = false,
                    rendering = false,
                    recording = false,
                )
            ),
        )
    )
)

//endregion