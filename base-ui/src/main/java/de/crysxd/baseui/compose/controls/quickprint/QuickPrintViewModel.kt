package de.crysxd.baseui.compose.controls.quickprint

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel
import de.crysxd.baseui.ext.connectCore
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.QuickPrintControlsViewModelCore
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach

class QuickPrintViewModel(
    private val instanceId: String,
) : GenericLoadingControlsViewModel<List<QuickPrintControlsViewModelCore.FileLink>>() {

    val core = QuickPrintControlsViewModelCore(instanceId = instanceId)
    var stateCache: ViewState<List<QuickPrintControlsViewModelCore.FileLink>> = ViewState.Loading
        private set
    val state = core.state.map { state ->
        when (state) {
            is FlowState.Error -> ViewState.Error(state.throwable)
            is FlowState.Loading -> ViewState.Loading
            is FlowState.Ready -> if (state.data.files.isEmpty()) {
                ViewState.Hidden
            } else {
                ViewState.Data(state.data.files, contentKey = "")
            }
        }
    }.onEach { state ->
        stateCache = state
    }

    init {
        connectCore { core }
    }

    override fun retry() = core.reload()

    suspend fun startPrint(
        path: String,
        materialConfirmed: Boolean,
        timelapseConfirmed: Boolean,
    ) = core.startPrint(
        path = path,
        materialConfirmed = materialConfirmed,
        timelapseConfirmed = timelapseConfirmed,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = QuickPrintViewModel(
            instanceId = instanceId,
        ) as T
    }
}