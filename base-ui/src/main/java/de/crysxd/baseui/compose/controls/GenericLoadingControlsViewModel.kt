package de.crysxd.baseui.compose.controls

import androidx.lifecycle.ViewModel

abstract class GenericLoadingControlsViewModel<T> : ViewModel() {

    abstract fun retry()

    sealed class ViewState<out T> {
        data object Loading : ViewState<Nothing>()
        data object Hidden : ViewState<Nothing>()
        data class Error(val exception: Throwable) : ViewState<Nothing>()
        data class Data<T>(val data: T, val contentKey: String) : ViewState<T>()
    }
}