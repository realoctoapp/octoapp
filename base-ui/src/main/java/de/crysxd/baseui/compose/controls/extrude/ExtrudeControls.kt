package de.crysxd.baseui.compose.controls.extrude

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControl
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControlState
import de.crysxd.baseui.compose.framework.helpers.MenuSheet
import de.crysxd.baseui.compose.framework.helpers.collectAsStateWhileActive
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.data.models.ExtrusionHistoryItem
import de.crysxd.octoapp.menu.controls.ExtrudeControlsSettingsMenu
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

@Composable
fun ExtrudeControls(
    editState: EditState,
    state: ExtrudeControlsState,
) {
    var showSettings by remember { mutableStateOf(false) }

    if (showSettings) {
        MenuSheet(
            menu = ExtrudeControlsSettingsMenu(LocalOctoPrint.current.id),
            onDismiss = { showSettings = false }
        )
    }

    GenericHistoryItemsControl(
        editState = editState,
        state = state,
        title = stringResource(id = R.string.widget_extrude),
        actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
        onAction = { _, _ -> showSettings = true }
    )
}

@Composable
fun rememberExtrudeControlsState(): ExtrudeControlsState {
    val context = LocalContext.current
    val navController = OctoAppTheme.navController

    val vmFactory = ExtrudeControlsViewModel.Factory(LocalOctoPrint.current.id)
    val vm = viewModel(
        modelClass = ExtrudeControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    val items = vm.items.collectAsStateWhileActive(key = "extrude", initial = emptyList())
    val visible = vm.visible.collectAsStateWhileActive(key = "extrude2", initial = false)

    return object : ExtrudeControlsState {
        override val visible by visible
        override val itemsList by items
        override val context = context

        override suspend fun onClick(item: ExtrusionHistoryItem) {
            vm.extrude(distanceMm = item.distanceMm)
        }

        override suspend fun onLongClick(item: ExtrusionHistoryItem) {
            vm.toggleFavourite(distanceMm = item.distanceMm)
        }

        override suspend fun onOtherOptionClicked() {
            vm.extrudeOther(context = context, navContoller = navController())
        }
    }
}

interface ExtrudeControlsState : GenericHistoryItemsControlState<ExtrusionHistoryItem> {
    val itemsList: List<ExtrusionHistoryItem>
    val context: Context

    override val otherOptionText get() = context.getString(R.string.other)
    override val items get() = listOf(null to itemsList)

    override fun key(item: ExtrusionHistoryItem) = item.distanceMm.toString()
    override fun label(item: ExtrusionHistoryItem) = item.distanceMm.formatAsLength(onlyMilli = true)
    override fun isPinned(item: ExtrusionHistoryItem) = item.isFavorite
}

//region Previews
@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    var items by remember {
        mutableStateOf(
            listOf(
                ExtrusionHistoryItem(-10),
                ExtrusionHistoryItem(10),
                ExtrusionHistoryItem(50),
                ExtrusionHistoryItem(100),
                ExtrusionHistoryItem(120),
            )
        )
    }

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(1000)
            items = listOf(
                ExtrusionHistoryItem(10),
                ExtrusionHistoryItem(-10),
                ExtrusionHistoryItem(50),
                ExtrusionHistoryItem(100),
                ExtrusionHistoryItem(120),
            )
            delay(1000)
            items = listOf(
                ExtrusionHistoryItem(-10),
                ExtrusionHistoryItem(10),
                ExtrusionHistoryItem(50),
                ExtrusionHistoryItem(100),
                ExtrusionHistoryItem(120),
            )
        }
    }


    val context = LocalContext.current
    ExtrudeControls(
        editState = EditState.ForPreview,
        state = object : ExtrudeControlsState {
            override val visible = true
            override val itemsList = items
            override val context = context
            override suspend fun onClick(item: ExtrusionHistoryItem) = Unit
            override suspend fun onLongClick(item: ExtrusionHistoryItem) = Unit
            override suspend fun onOtherOptionClicked() = Unit
        }
    )
}
//endregion