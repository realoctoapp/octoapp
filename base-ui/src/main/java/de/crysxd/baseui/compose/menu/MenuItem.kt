package de.crysxd.baseui.compose.menu

import android.net.Uri
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.graphics.ExperimentalAnimationGraphicsApi
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.with
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.DropdownMenu
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.hapticfeedback.HapticFeedback
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.commit
import androidx.lifecycle.asFlow
import androidx.navigation.NavController
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.benasher44.uuid.uuid4
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.EnterValueDialogInputConfig
import de.crysxd.baseui.compose.framework.components.EnterValueProvider
import de.crysxd.baseui.compose.framework.components.LocalEnterValueProvider
import de.crysxd.baseui.compose.framework.components.OctoText
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.framework.helpers.staticStateOf
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.backgroundColor
import de.crysxd.baseui.ext.foregroundColor
import de.crysxd.baseui.ext.open
import de.crysxd.baseui.ext.painter
import de.crysxd.baseui.ext.toCompose
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.ConfirmedMenuItem
import de.crysxd.octoapp.menu.FilePickerMenuItem
import de.crysxd.octoapp.menu.InputMenuItem
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.MenuItemStyle
import de.crysxd.octoapp.menu.PreviewMenuItems
import de.crysxd.octoapp.menu.ToggleMenuItem
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

private const val TAG = "MenuItem"

@Composable
fun SimpleMenuItem(
    title: String,
    icon: Painter?,
    style: MenuItemStyle,
    modifier: Modifier = Modifier,
    showAsOutline: Boolean = false,
    showAsSubMenu: Boolean = false,
    slim: Boolean = false,
    badgeCount: Int = 0,
    description: String? = null,
    onClick: suspend () -> Unit,
) {
    val loadingDoneAt = remember { mutableStateOf(0L) }
    val clickCompletedAt = remember { mutableStateOf(0L) }
    val loading = remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val feedback = LocalHapticFeedback.current
    val activity = OctoAppTheme.octoActivity
    val navController = OctoAppTheme.navController
    val enterValueProvider = LocalEnterValueProvider.current

    CompositionLocalProvider(
        //region MenuContext
        LocalMenuItemContext provides object : MenuItemContext {
            override val item = null
            override val title = title
            override val icon @Composable get() = icon
            override val showAsOutline = showAsOutline
            override val showAsSubMenu = showAsSubMenu
            override val slim = slim
            override val colors = MenuColors(
                foreground = style.foregroundColor,
                background = style.backgroundColor
            )
            override val isEnabled = true
            override val isPinned = false
            override val isFeatureEnabled = true
            override var isLoading by loading
            override val loadingDoneAt by loadingDoneAt
            override val clickCompletedAt by clickCompletedAt
            override val badgeCount = badgeCount
            override val navController get() = navController()
            override val host: MenuHost? = null
            override val enterValueProvider = enterValueProvider
            override val activity: OctoActivity? get() = activity
        }
        //endregion
    ) {
        Column(modifier = modifier.height(IntrinsicSize.Min)) {
            Row {
                MainButton(
                    onClick = {
                        executeClickAction(
                            scope = scope,
                            feedback = feedback,
                            loading = loading,
                            clickCompletedAt = clickCompletedAt,
                            loadingDoneAt = loadingDoneAt,
                            action = onClick,
                            playSuccess = true,
                        )
                    },
                    onLongClick = null,
                )
            }

            Description(description = description)
        }
    }
}

@Composable
fun MenuItem(
    item: MenuItem,
    host: MenuHost? = null,
    useDescriptions: Boolean = true,
    menuId: MenuId = MenuId.Other,
) = Column(
    modifier = Modifier.testTag(TestTags.Menu.Item)
) {
    //region State
    val feedback = LocalHapticFeedback.current
    val scope = rememberCoroutineScope()
    val context = rememberMenuContext()
    val loading = remember { mutableStateOf(false) }
    val loadingDoneAt = remember { mutableStateOf(0L) }
    val clickCompletedAt = remember { mutableStateOf(0L) }
    val canBePinned = item.canBePinned
    var isEnabled by remember { mutableStateOf(true) }
    val systemInfo = LocalOctoPrint.current.systemInfo ?: SystemInfo()
    val isFeatureEnabled = if (OctoAppTheme.isPreview) {
        flowOf(LocalFeatureEnabledDefault.current)
    } else {
        remember { item.billingManagerFeature?.let { BillingManager.isFeatureEnabledFlow(it) } ?: flowOf(true) }
    }.collectAsState(initial = LocalFeatureEnabledDefault.current)
    val colors = MenuColors(
        foreground = item.iconColorOverwrite?.toCompose() ?: item.style.foregroundColor,
        background = item.style.backgroundColor,
    )
    LaunchedEffect(context) {
        isEnabled = item.isEnabled(context, systemInfo)
    }
    //endregion
    //region Pinned item repository
    val pinnedItemsRepository = if (OctoAppTheme.isPreview) null else BaseInjector.get().pinnedMenuItemsRepository()
    val pinnedItems by (pinnedItemsRepository?.observePinnedMenuItems(menuId)?.collectAsState() ?: staticStateOf(emptySet()))
    //endregion
    //region MenuContext
    val navController = OctoAppTheme.navController
    val octoActivity = OctoAppTheme.octoActivity
    val enterValueProvider = LocalEnterValueProvider.current
    val menuItemContext = object : MenuItemContext {
        override val item get() = item
        override val title get() = item.title
        override val icon @Composable get() = item.icon.painter
        override val showAsOutline get() = false
        override val showAsSubMenu get() = item.showAsSubMenu
        override val slim: Boolean get() = false
        override val colors get() = colors
        override val isPinned get() = pinnedItems.contains(item.itemId)
        override val isEnabled get() = isEnabled
        override val isFeatureEnabled by isFeatureEnabled
        override var isLoading by loading
        override val loadingDoneAt by loadingDoneAt
        override val clickCompletedAt by clickCompletedAt
        override val badgeCount get() = item.badgeCount
        override val navController get() = navController()
        override val host: MenuHost? = host
        override val enterValueProvider = enterValueProvider
        override val activity: OctoActivity? get() = octoActivity
    }
    //endregion
    //region Item
    CompositionLocalProvider(LocalMenuItemContext provides menuItemContext) {
        val showPinPopUp = remember { mutableStateOf(false) }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min)
        ) {

            MainButton(
                onLongClick = { showPinPopUp.value = true }.takeIf { canBePinned },
                onClick = {
                    executeClickAction(
                        action = { item.handleClick(menuItemContext) },
                        loadingDoneAt = loadingDoneAt,
                        loading = loading,
                        playSuccess = item.showSuccess,
                        clickCompletedAt = clickCompletedAt,
                        feedback = feedback,
                        scope = scope,
                    )
                },
            )

            if (LocalMenuItemContext.current.isFeatureEnabled) {
                SecondaryButton(
                    onClick = {
                        executeClickAction(
                            action = { item.handleSecondaryClick(menuItemContext) },
                            loadingDoneAt = loadingDoneAt,
                            loading = loading,
                            playSuccess = item.showSuccess,
                            clickCompletedAt = clickCompletedAt,
                            feedback = feedback,
                            scope = scope,
                        )
                    }
                )
            }

            PinPopUpMenu(
                visible = showPinPopUp,
                itemId = item.itemId,
                localMenuId = menuId,
                pinnedItems = pinnedItemsRepository,
                host = host,
            )
        }
    }
    //endregion
    //region Description
    Description(
        description = item.description.takeIf { useDescriptions }
    )
    //endregion
}

//region Description
@Composable
fun Description(modifier: Modifier = Modifier, description: String?) = description?.let {
    OctoText(
        text = it.toHtml().asAnnotatedString(),
        style = OctoAppTheme.typography.labelSmall,
        color = OctoAppTheme.colors.lightText,
        textAlign = TextAlign.Center,
        modifier = modifier
            .fillMaxWidth()
            .padding(top = OctoAppTheme.dimens.margin01, bottom = OctoAppTheme.dimens.margin1)
    )
}

//endregion
//region Popup menu
@Composable
private fun PinPopUpMenu(
    visible: MutableState<Boolean>,
    localMenuId: MenuId,
    itemId: String,
    pinnedItems: PinnedMenuItemRepository?,
    host: MenuHost?,
) {
    val scope = rememberCoroutineScope()
    val pinned = remember(visible.value) {
        MenuId.entries.associateWith {
            mutableStateOf(pinnedItems?.isPinned(itemId = itemId, menuId = it) == true)
        }
    }

    DropdownMenu(
        expanded = visible.value,
        onDismissRequest = {
            visible.value = false
            scope.launch {
                try {
                    pinned.forEach {
                        pinnedItems?.setMenuItemPinned(itemId = itemId, menuId = it.key, pinned = it.value.value)
                    }
                    host?.reloadMenu()
                } catch (e: Exception) {
                    Napier.e(tag = TAG, throwable = e, message = "Reload failed")
                }
            }
        },
    ) {
        Text(
            text = stringResource(id = R.string.menu_controls___pin_to_x, ""),
            style = OctoAppTheme.typography.sectionHeader,
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin12)
                .padding(horizontal = OctoAppTheme.dimens.margin12)
        )

        val specialMenu = localMenuId in listOf(MenuId.Other, MenuId.MainMenu)
        if (!specialMenu) {
            PinMenuItem(menuId = null, isPinned = pinned[localMenuId]!!)
            Divider(color = OctoAppTheme.colors.lightText.copy(alpha = 0.3f))
        }

        MenuId.entries
            .filter { it != MenuId.Other && (it != localMenuId || specialMenu) }
            .filter { it != MenuId.Widget || (LocalMenuItemContext.current.item?.canRunWithAppInBackground != false) }
            .forEach { PinMenuItem(menuId = it, isPinned = pinned[it]!!) }
    }
}

@Composable
private fun PinMenuItem(
    menuId: MenuId? = null,
    isPinned: MutableState<Boolean>,
) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier.padding(start = OctoAppTheme.dimens.margin12, end = OctoAppTheme.dimens.margin1),
    horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin2)
) {
    Text(
        text = menuId?.label ?: stringResource(id = R.string.menu_controls___here),
        style = OctoAppTheme.typography.base,
        color = OctoAppTheme.colors.darkText,
        modifier = Modifier.weight(1f)
    )

    Switch(
        checked = isPinned.value,
        onCheckedChange = { isPinned.value = !isPinned.value },
    )
}

//endregion
//region Secondary button
@Composable
private fun SecondaryButton(
    onClick: (() -> Unit)? = null,
) = LocalMenuItemContext.current.item?.secondaryButtonIcon?.let { icon ->
    Box(
        modifier = Modifier
            .padding(start = OctoAppTheme.dimens.margin01)
            .clip(MaterialTheme.shapes.small)
            .fillMaxHeight()
            .testTag(TestTags.Menu.ItemSecondaryButton(LocalMenuItemContext.current.title))
            .aspectRatio(1f)
            .menuButton(onClick = onClick)
    ) {
        androidx.compose.material.Icon(
            painter = icon.painter,
            contentDescription = null,
            tint = LocalMenuItemContext.current.colors.foreground,
            modifier = Modifier
                .size(24.dp)
                .align(Alignment.Center)
                .loadingSensitive()
        )
    }
}

//endregion
// region Main button
@Composable
private fun RowScope.MainButton(
    onClick: (() -> Unit)?,
    onLongClick: (() -> Unit)?,
) = Row(
    modifier = Modifier
        .weight(1f)
        .clip(MaterialTheme.shapes.small)
        .menuButton(onClick = onClick, onLongClick = onLongClick)
        .alpha(if (LocalMenuItemContext.current.isEnabled) 1f else 0.4f)
        .testTag(TestTags.Menu.ItemMainButton(LocalMenuItemContext.current.title))
        .heightIn(min = if (LocalMenuItemContext.current.slim) 32.dp else 40.dp),
    verticalAlignment = Alignment.CenterVertically
) {
    val context = LocalMenuItemContext.current
    context.Icon()
    Title()

    if (context.isFeatureEnabled) {
        context.Switch()
        context.RightDetail()
        context.SubMenuIcon()
        Spacer(modifier = Modifier.width(OctoAppTheme.dimens.margin1))
    } else {
        context.FeatureDisabledIcon()
    }
}

//region Right elements
@Composable
private fun MenuItemContext.Switch() {
    if (item is ToggleMenuItem) {
        val isChecked by (item as ToggleMenuItem).state.collectAsState(initial = false)
        Box(modifier = Modifier.height(24.dp)) {
            Switch(
                checked = isChecked,
                onCheckedChange = null,
                modifier = Modifier
                    .testTag(TestTags.Menu.ItemRigthSwitch(LocalMenuItemContext.current.title))
                    .padding(start = OctoAppTheme.dimens.margin1)
                    .loadingSensitive(),
                colors = SwitchDefaults.colors(
                    checkedThumbColor = colors.foreground,
                    checkedTrackColor = colors.foreground,
                ),
            )
        }
    }
}

@Composable
private fun MenuItemContext.SubMenuIcon() {
    if (showAsSubMenu) {
        androidx.compose.material.Icon(
            painter = painterResource(id = R.drawable.ic_round_chevron_right_24),
            contentDescription = null,
            tint = colors.foreground,
            modifier = Modifier.loadingSensitive(),
        )
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun MenuItemContext.RightDetail() {
    val text by (item?.rightDetail?.collectAsState(initial = "") ?: staticStateOf(null))
    AnimatedContent(
        targetState = text,
        transitionSpec = { fadeIn() with fadeOut() },
    ) { target ->
        if (target != null) {
            Text(
                text = target.asAnnotatedString(),
                style = OctoAppTheme.typography.sectionHeader,
                color = OctoAppTheme.colors.normalText,
                maxLines = 2,
                textAlign = TextAlign.End,
                modifier = Modifier
                    .loadingSensitive()
                    .padding(start = OctoAppTheme.dimens.margin1)
                    .testTag(TestTags.Menu.ItemRightDetail(LocalMenuItemContext.current.title))
                    .widthIn(max = 200.dp),
            )
        }
    }
}

@Composable
private fun MenuItemContext.FeatureDisabledIcon() = Box(
    contentAlignment = Alignment.Center,
    modifier = Modifier
        .fillMaxHeight()
        .aspectRatio(1f),
) {
    androidx.compose.material.Icon(
        modifier = Modifier
            .size(22.dp)
            .clip(CircleShape)
            .background(colors.background)
            .padding(4.dp),
        painter = painterResource(id = R.drawable.ic_round_favorite_18),
        contentDescription = null,
        tint = colors.foreground
    )
}

//endregion
//region Title
@Composable
private fun RowScope.Title() = Text(
    text = LocalMenuItemContext.current.title,
    style = OctoAppTheme.typography.buttonSmall,
    color = OctoAppTheme.colors.normalText,
    maxLines = 1,
    overflow = if (LocalMenuItemContext.current.showAsOutline) TextOverflow.Visible else TextOverflow.Ellipsis,
    modifier = Modifier
        .padding(start = OctoAppTheme.dimens.margin1)
        .weight(1f)
        .loadingSensitive(scaleStart = true),
)


//endregion
//region Left elements
@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun MenuItemContext.Icon() {
    val icon = icon ?: return Spacer(modifier = Modifier.width(OctoAppTheme.dimens.margin01))

    var height by remember { mutableStateOf(0.dp) }
    val size = 24.dp
    val density = LocalDensity.current
    Box(
        modifier = Modifier
            .fillMaxHeight()
            .height(IntrinsicSize.Min)
            .aspectRatio(1f, matchHeightConstraintsFirst = false)
            .background(colors.background, CircleShape)
            .onGloballyPositioned { height = with(density) { it.size.height.toDp() } }
    ) {
        var showSuccess by remember { mutableStateOf(false) }
        val badgeSize = 12.dp
        val scaleSpec = spring<Float>()
        val enterTransition = scaleIn(animationSpec = scaleSpec, initialScale = 0.66f) + fadeIn()
        val exitTransition = scaleOut(animationSpec = scaleSpec, targetScale = 0.66f) + fadeOut()
        val mainModifier = {
            Modifier
                .size(size)
                .align(Alignment.Center)
        }

        //region Icon
        AnimatedVisibility(
            visible = !isLoading && !showSuccess,
            enter = enterTransition,
            exit = exitTransition,
            modifier = mainModifier()
        ) {
            androidx.compose.material.Icon(
                painter = icon,
                contentDescription = null,
                tint = colors.foreground,
            )
        }
        //endregion
        //region Laoding spinner
        AnimatedVisibility(
            visible = isLoading,
            enter = enterTransition,
            exit = exitTransition,
            modifier = mainModifier()
        ) {
            CircularProgressIndicator(
                color = colors.foreground,
                strokeWidth = 2.dp,
                modifier = Modifier
                    .size(size)
                    .padding(2.dp),
            )
        }
        //endregion
        //region Confirm
        if (showSuccess) {
            val composition by rememberLottieComposition(spec = LottieCompositionSpec.RawRes(R.raw.success_check))
            LottieAnimation(
                restartOnPlay = true,
                isPlaying = true,
                composition = composition,
                modifier = Modifier
                    .size(height)
                    .scale(1.9f)
            )
        }
        LaunchedEffect(loadingDoneAt) {
            if (loadingDoneAt > 0) showSuccess = true
            delay(2.seconds)
            showSuccess = false
        }
        //endregion
        //region Badges
        AnimatedVisibility(
            visible = !isLoading,
            exit = fadeOut(),
            enter = fadeIn(),
            modifier = Modifier
                .height(badgeSize)
                .aspectRatio(1f)
                .offset(y = -(4.dp), x = 2.dp)
                .clip(CircleShape)
                .align(Alignment.BottomEnd),
        ) {
            when {
                badgeCount > 0 -> Text(
                    text = badgeCount.toString(),
                    style = OctoAppTheme.typography.labelSmall.copy(fontSize = with(density) { badgeSize.toSp() }),
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(OctoAppTheme.colors.red)
                        .offset(y = -(2.dp))
                )

                isPinned -> androidx.compose.material.Icon(
                    painter = painterResource(id = R.drawable.ic_round_push_pin_10),
                    contentDescription = null,
                    tint = OctoAppTheme.colors.menuPinForeground,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(OctoAppTheme.colors.menuPinBackground)
                        .padding(1.dp)
                )
            }
        }
        //endregion
    }
}

//endregion
//endregion
//region Modifiers
@OptIn(ExperimentalFoundationApi::class)
private fun Modifier.menuButton(onLongClick: (() -> Unit)? = null, onClick: (() -> Unit)?) = composed {
    val context = LocalMenuItemContext.current
    val openPurchase = { UriLibrary.getPurchaseUri().open() }
    val noop = { }

    this
        .combinedClickable(
            enabled = !context.isLoading && context.isEnabled,
            interactionSource = MutableInteractionSource(),
            indication = rememberRipple(color = context.colors.background),
            onClick = when {
                !context.isFeatureEnabled -> openPurchase
                context.isLoading -> noop
                else -> onClick ?: noop
            },
            onLongClick = if (context.isFeatureEnabled) onLongClick else null
        )
        .background(if (LocalMenuItemContext.current.showAsOutline) Color.Companion.Transparent else context.colors.background)
        .border(
            shape = MaterialTheme.shapes.small,
            color = if (LocalMenuItemContext.current.showAsOutline) context.colors.foreground else Color.Transparent,
            width = 1.dp
        )
}

private fun Modifier.loadingSensitive(scaleStart: Boolean = false) = composed {
    val isLoading = LocalMenuItemContext.current.isLoading
    val alpha by animateFloatAsState(if (isLoading) LoadingAlpha else 1f)
    val scale by animateFloatAsState(if (isLoading) LoadingScale else 1f)

    this.graphicsLayer {
        this.alpha = alpha
        this.scaleY = scale
        this.scaleX = scale
        this.transformOrigin = TransformOrigin(pivotFractionX = if (scaleStart) 0f else 0.5f, pivotFractionY = 0.5f)
    }
}


private const val LoadingAlpha = 0.33f
private const val LoadingScale = 1f

//endregion
//region Click
private suspend fun MenuItem.handleClick(menuItemContext: MenuItemContext) = when (this) {
    is InputMenuItem -> handleInput(menuItemContext)
    is ConfirmedMenuItem -> handleConfirmedClick(menuItemContext, this::onClicked)
    is FilePickerMenuItem -> handleFilePicker(menuItemContext)
    else -> onClicked(host = menuItemContext.host)
}

private suspend fun MenuItem.handleSecondaryClick(menuItemContext: MenuItemContext) = when (this) {
    is ConfirmedMenuItem -> handleConfirmedClick(menuItemContext, this::onSecondaryClicked)
    else -> onSecondaryClicked(host = menuItemContext.host)
}

private suspend fun FilePickerMenuItem.handleFilePicker(menuItemContext: MenuItemContext) {
    val activity = requireNotNull(menuItemContext.activity)
    val (resultId, data) = NavigationResultMediator.registerResultCallback<Uri>()
    activity.supportFragmentManager.commit {
        add(FilePickerMenuItemHelperFragment.createForResultId(resultId), uuid4().toString())
    }

    Napier.i(tag = TAG, message = "File picker waiting")

    data.asFlow().first()?.let { uri ->
        Napier.i(tag = TAG, message = "File picked: $uri")
        onFileSelected(menuItemContext.host, uri.toString())
    }

    Napier.i(tag = TAG, message = "File picker done")
}

private suspend fun ConfirmedMenuItem.handleConfirmedClick(menuItemContext: MenuItemContext, action: suspend (MenuHost?) -> Unit) {
    val needsConfirmation = !confirmationMessage.isNullOrBlank() && !confirmationPositiveAction.isNullOrBlank() && !canSkipConfirmation
    val activity = requireNotNull(menuItemContext.activity)

    val confirmed = if (needsConfirmation) {
        val resultFlow = MutableSharedFlow<Boolean>()
        activity.showDialog(
            OctoActivity.Message.DialogMessage(
                text = { confirmationMessage ?: "???" },
                positiveButton = { confirmationPositiveAction ?: "???" },
                negativeButton = { getString(R.string.cancel) },
                positiveAction = { AppScope.launch { resultFlow.emit(true) } },
                dismissAction = { AppScope.launch { resultFlow.emit(false) } }
            )
        )
        resultFlow.first()
    } else {
        true
    }

    if (confirmed) {
        action(menuItemContext.host)
    } else {
        throw CancellationException("Not confirmed")
    }
}

private suspend fun InputMenuItem.handleInput(menuItemContext: MenuItemContext) = menuItemContext.run {
    menuItemContext.enterValueProvider.requestEnterValues(
        title = inputTitle,
        confirmButton = confirmAction,
        inputs = listOf(
            EnterValueDialogInputConfig(
                label = inputHint,
                labelActive = inputHint,
                validator = { onValidateInput(it, it.toFloatSafe()) },
                value = currentValue.first(),
                keyboard = keyboardType,
            )
        )
    )?.let { (result) ->
        onNewInput(
            menuHost = menuItemContext.host,
            input = result,
            inputAsNumber = result.toFloatSafe(),
        )
    }
}


private fun String.toFloatSafe() = replace(",", ".").toFloatOrNull()

private fun executeClickAction(
    scope: CoroutineScope,
    loading: MutableState<Boolean>,
    clickCompletedAt: MutableState<Long>,
    loadingDoneAt: MutableState<Long>,
    feedback: HapticFeedback,
    playSuccess: Boolean,
    action: suspend () -> Unit,
) = scope.launch {
    if (loading.value) {
        Napier.i(tag = TAG, message = "Skipping click, already loading")
        return@launch
    }

    try {
        // Set loading after a grace period of 200ms. Most items will be done instantly.
        val loadingJob = launch {
            delay(200)
            loading.value = true
            feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
        }

        // Perform action, then cancel loading (in case it didn't fire yet) and refresh values
        action()
        loadingJob.cancel()
        clickCompletedAt.value = System.currentTimeMillis()

        if (playSuccess) {
            loadingDoneAt.value = System.currentTimeMillis()
            delay(100)
        }
    } catch (e: Exception) {
        Napier.e(tag = TAG, message = "Click failed", throwable = e)
        ExceptionReceivers.dispatchException(e)
    }
}.invokeOnCompletion {
    feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
    loading.value = false
}

//endregion
//region Context
val LocalMenuItemContext = compositionLocalOf<MenuItemContext> { throw UninitializedPropertyAccessException() }
private val LocalFeatureEnabledDefault = compositionLocalOf { true }

interface MenuItemContext {
    val item: MenuItem?
    val title: String
    val showAsOutline: Boolean
    val showAsSubMenu: Boolean
    val slim: Boolean
    val colors: MenuColors
    val isPinned: Boolean
    val isEnabled: Boolean
    val isFeatureEnabled: Boolean
    var isLoading: Boolean
    val loadingDoneAt: Long
    val clickCompletedAt: Long
    val badgeCount: Int
    val navController: NavController
    val host: MenuHost?
    val enterValueProvider: EnterValueProvider
    val activity: OctoActivity?

    @get:Composable
    val icon: Painter?
}

data class MenuColors(
    val foreground: Color,
    val background: Color
)

//endregion
//region Preview
@Composable
@Preview(group = "Items")
private fun PreviewNormal() = MenuItemPreview(
    item = PreviewMenuItems.Normal,
)

@Composable
@Preview(group = "Items")
private fun PreviewRevolving() = MenuItemPreview(
    item = PreviewMenuItems.RevolvingOptions,
)

@Composable
@Preview(group = "Items")
private fun PreviewSecondary() = MenuItemPreview(
    item = PreviewMenuItems.Secondary,
)

@Composable
@Preview(group = "Items")
private fun PreviewToggle() = MenuItemPreview(
    item = PreviewMenuItems.Toggle,
)

@Composable
@Preview(group = "Items")
private fun PreviewInput() = MenuItemPreview(
    item = PreviewMenuItems.Input,
)

@Composable
@Preview(group = "Items")
private fun PreviewConfirmed() = MenuItemPreview(
    item = PreviewMenuItems.Confirmed,
)

@Composable
@Preview(group = "Items")
private fun PreviewSlim() = OctoAppThemeForPreview {
    SimpleMenuItem(
        title = "Slim",
        slim = true,
        icon = painterResource(id = R.drawable.ic_round_favorite_24),
        onClick = {},
        style = MenuItemStyle.OctoPrint,
    )
}

@Composable
@Preview(group = "Items")
private fun PreviewDisabled() = MenuItemPreview(
    item = PreviewMenuItems.FeatureDisabled,
    defaultEnabled = false
)

@Composable
@Preview(group = "Items")
private fun PreviewNoIcon() = OctoAppThemeForPreview {
    SimpleMenuItem(
        title = stringResource(id = R.string.help___octoprint_discord),
        icon = null,
        style = MenuItemStyle.OctoPrint,
        onClick = { UriLibrary.getPluginLibraryUri().open() }
    )
}

@Composable
private fun MenuItemPreview(item: MenuItem, defaultEnabled: Boolean = true) = OctoAppThemeForPreview {
    CompositionLocalProvider(LocalFeatureEnabledDefault provides defaultEnabled) {
        MenuItem(item = item)
    }
}
//endregion