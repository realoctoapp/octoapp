package de.crysxd.baseui.compose.screens.filelist

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectVerticalDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewControlsViewModel
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewScope
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewView
import de.crysxd.baseui.compose.controls.quickprint.rememberStartPrintState
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoPicassoImage
import de.crysxd.baseui.compose.framework.helpers.rememberMenuSheetState
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.menu.controls.GcodeSettingsMenu
import de.crysxd.octoapp.viewmodels.helper.startprint.StartPrintHelper


@Composable
@OptIn(ExperimentalFoundationApi::class)
fun FileInfoContent(
    modifier: Modifier = Modifier,
    pagerState: PagerState,
    canStartPrint: Boolean,
    file: FileObject.File,
) = HorizontalPager(
    modifier = modifier,
    state = pagerState
) { page ->
    when (page) {
        0 -> FileInfo(file = file, canStartPrint = canStartPrint)
        1 -> GcodePreview(file = file)
    }
}

@Composable
private fun FileInfo(
    modifier: Modifier = Modifier,
    file: FileObject.File,
    canStartPrint: Boolean,
) = Box {
    LazyColumn(
        modifier = modifier
            .fillMaxSize()
            .nestedScroll(LocalNestedScrollConnection.current),
        contentPadding = PaddingValues(
            bottom = OctoAppTheme.dimens.margin2,
            start = OctoAppTheme.dimens.margin2,
            end = OctoAppTheme.dimens.margin2
        ),
    ) {
        file.largeThumbnail?.let { thumbnail ->
            item(key = "thumbnail") {
                Thumbnail(thumbnail = thumbnail)
            }
        }

        file.metadata.forEach { group ->
            item(key = group.id) {
                MetadataGroupHeader(label = group.label)
            }

            group.items.forEachIndexed { index, item ->
                item(key = "${group.id}:${item.id}") {
                    MetadataItem(
                        value = item.value,
                        label = item.label,
                        isLast = index == group.items.lastIndex
                    )
                }
            }
        }

        if (canStartPrint) {
            item(key = "button-placeholder") {
                FileInfoPrintButton(
                    onStartPrint = { _, _ -> StartPrintHelper.Result.Started },
                    file = file,
                    modifier = Modifier.alpha(0f)
                )
            }
        }
    }
}

@Composable
fun FileInfoPrintButton(
    modifier: Modifier = Modifier,
    file: FileObject.File,
    onStartPrint: suspend (Boolean, Boolean) -> StartPrintHelper.Result,
) {
    val startPrintState = rememberStartPrintState(file, startPrint = onStartPrint)

    OctoButton(
        modifier = modifier.padding(OctoAppTheme.dimens.margin2),
        text = stringResource(R.string.start_printing),
        onClick = { startPrintState.startPrint() }
    )
}

@Composable
private fun GcodePreview(
    modifier: Modifier = Modifier,
    file: FileObject.File
) = Box(
    modifier = modifier.pointerInput(Unit) {
        detectVerticalDragGestures { _, _ -> }
    }
) {
    val settingsMenu = rememberMenuSheetState { GcodeSettingsMenu() }
    val instanceId = LocalOctoPrint.current.id
    val vmFactory = GcodePreviewControlsViewModel.Factory(instanceId)
    val vm = viewModel(
        modelClass = GcodePreviewControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    LaunchedEffect(file) {
        vm.useManual(layerIndex = 0, layerProgress = 1f, file = file)
    }

    val scope = remember(vm, settingsMenu) {
        object : GcodePreviewScope {
            override fun setManualProgress(layerIndex: Int, layerProgress: Float) = vm.useManual(layerIndex = layerIndex, layerProgress = layerProgress, file = file)
            override fun onRetry() = vm.retry()
            override fun onDownload() = vm.allowLargeDownloads()
            override fun syncWithLive() = Unit
            override fun showSettings() = settingsMenu.show()
        }
    }

    val state by vm.state.collectAsState(GcodePreviewControlsViewModel.ViewState.Loading())
    scope.GcodePreviewView(
        state = state,
        canSyncWithLive = false
    )
}

@Composable
private fun MetadataGroupHeader(
    modifier: Modifier = Modifier,
    label: String,
) = Text(
    modifier = modifier.padding(bottom = OctoAppTheme.dimens.margin1, top = OctoAppTheme.dimens.margin2),
    text = label,
    style = OctoAppTheme.typography.subtitle,
    color = OctoAppTheme.colors.normalText,
)

@Composable
private fun MetadataItem(
    modifier: Modifier = Modifier,
    isLast: Boolean,
    label: String,
    value: String,
) {
    val color = OctoAppTheme.colors.lightText.copy(alpha = 0.15f)
    Row(modifier = modifier
        .padding(bottom = OctoAppTheme.dimens.margin1)
        .drawBehind {
            if (!isLast) {
                drawRect(
                    color = color,
                    size = size.copy(height = 1.dp.toPx()),
                    topLeft = Offset(x = 0f, y = size.height - 1.dp.toPx())
                )
            }
        }
        .padding(bottom = OctoAppTheme.dimens.margin1)
    ) {
        Text(
            modifier = Modifier.padding(end = OctoAppTheme.dimens.margin2),
            text = label,
            style = OctoAppTheme.typography.focus,
            color = OctoAppTheme.colors.darkText,
        )

        SelectionContainer {
            Text(
                modifier = modifier.fillMaxWidth(),
                text = value,
                textAlign = TextAlign.End,
                style = OctoAppTheme.typography.label,
                color = OctoAppTheme.colors.normalText,
            )
        }
    }
}

@Composable
private fun Thumbnail(
    modifier: Modifier = Modifier,
    thumbnail: FileObject.File.Thumbnail,
) = OctoPicassoImage(
    modifier = modifier
        .fillMaxWidth()
        .aspectRatio((thumbnail.height / thumbnail.width.toFloat()).takeUnless { it.isNaN() } ?: 1f)
        .padding(top = OctoAppTheme.dimens.margin2)
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground),
    path = thumbnail.path,
    fallback = R.drawable.ic_round_print_24,
)