package de.crysxd.baseui.compose.screens.terminal

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.layout.ScrollEdge
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.viewmodels.TerminalViewModelCore.TerminalLogLine
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Composable
internal fun TerminalLogs(
    modifier: Modifier = Modifier,
    isStyled: Boolean,
    logs: List<TerminalLogLine>,
    onExecuteGcode: suspend (String) -> Unit,
    canExecute: Boolean,
    state: LazyListState,
) = Box(modifier = modifier) {
    val scope = rememberCoroutineScope()
    val scrolledToBottom by remember {
        derivedStateOf { (state.layoutInfo.visibleItemsInfo.firstOrNull()?.index ?: 0) <= 5 }
    }
    val scrolledToTop by remember(logs.size) {
        derivedStateOf { (state.layoutInfo.visibleItemsInfo.lastOrNull()?.index ?: 0) >= (logs.size - 1) }
    }

    // If we are at the bottom, stick to bottom when new items are added
    LaunchedEffect(logs.size) {
        if (logs.isNotEmpty() && scrolledToBottom) {
            state.animateScrollToItem(0)
        }
    }

    SelectionContainer {
        LazyColumn(
            reverseLayout = true,
            state = state,
            modifier = Modifier.fillMaxSize()
        ) {
            items(
                items = logs.asReversed(),
                key = { it.id }
            ) {
                val line = it

                if (isStyled) {
                    StyledTerminalLine(
                        line = line,
                        onExecute = { onExecuteGcode(line.textStyled) },
                        canExecute = canExecute,
                    )
                } else {
                    PlainTerminalLine(
                        line = line
                    )
                }
            }
        }
    }

    AnimatedVisibility(
        visible = !scrolledToBottom,
        label = "scroll to bottom button",
        enter = fadeIn() + slideInVertically { it },
        exit = fadeOut() + slideOutVertically { it },
        modifier = Modifier.align(Alignment.BottomCenter)
    ) {
        OctoButton(
            onClick = {
                // Launch so the button doesn't go into loading state
                scope.launch {
                    state.animateScrollToItem(0)
                }
            },
            small = true,
            modifier = Modifier
                .padding(OctoAppTheme.dimens.margin12)
                .shadow(elevation = 2.dp, shape = CircleShape),
        ) {
            Icon(
                painter = painterResource(R.drawable.ic_round_keyboard_arrow_down_24),
                contentDescription = stringResource(R.string.cd_scroll_to_latest),
                modifier = Modifier.aspectRatio(1f),
            )
        }
    }

    ScrollEdge(
        visible = { !scrolledToTop },
        alignment = Alignment.TopCenter
    )

    ScrollEdge(
        visible = { !scrolledToBottom },
        alignment = Alignment.BottomCenter
    )
}