package de.crysxd.baseui.compose.controls

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.updateTransition
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel.ViewState
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
@OptIn(ExperimentalAnimationApi::class)
fun <T> GenericLoadingControls(
    editState: EditState,
    state: GenericLoadingControlsState<T>,
    title: String,
    actionIcon: Painter? = null,
    onAction: (FragmentManager, NavController) -> Unit = { _, _ -> },
    dataState: @Composable (T) -> Unit,
) = AnimatedVisibility(
    visible = state.current.let { s ->
        (s !is ViewState.Loading || state.wasError) && s !is ViewState.Hidden
    },
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically()
) {
    ControlsScaffold(
        title = title,
        editState = editState,
        actionIcon = actionIcon,
        onAction = onAction
    ) {
        updateTransition(targetState = state.current, label = "AnimatedContent").AnimatedContent(
            transitionSpec = { fadeIn() with fadeOut() },
            contentKey = {
                when (it) {
                    is ViewState.Data -> it.contentKey
                    is ViewState.Error -> "Error"
                    ViewState.Loading -> "Loading"
                    ViewState.Hidden -> "Empty"
                }
            }
        ) { current ->
            when (current) {
                is ViewState.Loading -> if (state.wasError) ErrorState(state = null, onRetry = state::onRetry)
                is ViewState.Data -> dataState(current.data)
                is ViewState.Error -> ErrorState(state = current, onRetry = state::onRetry)
                is ViewState.Hidden -> Unit
            }
        }
    }
}

//region Internals
@Composable
private fun ErrorState(state: ViewState.Error?, onRetry: () -> Unit) = Box(
    modifier = Modifier
        .fillMaxWidth()
        .clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground)
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(OctoAppTheme.dimens.margin2)
            .align(Alignment.Center)
    ) {
        Text(
            text = stringResource(id = R.string.error_general),
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.darkText,
            textAlign = TextAlign.Center,
            maxLines = 4,
            overflow = TextOverflow.Ellipsis,
        )
        OctoButton(
            text = stringResource(id = R.string.retry_general),
            modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
            onClick = onRetry,
            enabled = state != null,
            small = true,
        )
    }

    if (state != null) {
        val activity = OctoAppTheme.octoActivity
        OctoIconButton(
            painter = painterResource(id = R.drawable.ic_round_info_24),
            contentDescription = stringResource(id = R.string.cd_details),
            modifier = Modifier.align(Alignment.BottomEnd)
        ) {
            activity?.showDialog(state.exception)
        }
    }
}
//endregion

interface GenericLoadingControlsState<T> {
    val wasError: Boolean
    val current: ViewState<T>
    fun onRetry()
}
