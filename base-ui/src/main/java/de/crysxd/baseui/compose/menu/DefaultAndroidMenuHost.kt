package de.crysxd.baseui.compose.menu

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.compose.runtime.compositionLocalOf
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import de.crysxd.baseui.R
import de.crysxd.baseui.common.controls.ControlsFragment
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.ext.toMarkdown
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.BillingProductTransferAnnounceResult
import de.crysxd.octoapp.base.billing.announceTransfer
import de.crysxd.octoapp.menu.main.settings.AndroidMenuHost
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DefaultAndroidMenuHost(private val fragment: Fragment) : AndroidMenuHost {
    override fun startLiveNotificationService() {
        fragment.requireOctoActivity().startPrintNotificationService()
    }

    override fun requestNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            fragment.requireOctoActivity().requestPermissions(arrayOf(android.Manifest.permission.POST_NOTIFICATIONS), 1)
        }
    }

    override fun customizeControls() {
        (fragment as? ControlsFragment)?.startEdit()
    }

    override suspend fun transferPurchase() {
        val octoActivity = fragment.requireOctoActivity()
        val qrCodeBitmap = withContext(Dispatchers.SharedIO) {
            val url = when (val result = BillingManager.announceTransfer()) {
                is BillingProductTransferAnnounceResult.Error -> throw result.exception
                is BillingProductTransferAnnounceResult.Success -> result.url
            }
            val qrCodeBitMatrix = QRCodeWriter().encode(url, BarcodeFormat.QR_CODE, 1024, 1024)
            qrCodeBitMatrix.toBitmap()
        }

        MaterialAlertDialogBuilder(octoActivity)
            .setView(
                LinearLayout(octoActivity).apply {
                    orientation = LinearLayout.VERTICAL
                    addView(
                        ImageView(octoActivity).apply {
                            setImageBitmap(qrCodeBitmap)
                            adjustViewBounds = true
                            scaleType = ImageView.ScaleType.CENTER_INSIDE
                        },
                        ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    )
                    addView(
                        TextView(octoActivity).apply {
                            text = context.getString(R.string.billing_menu___transfer_steps).toMarkdown()
                            setPadding(resources.getDimensionPixelSize(R.dimen.margin_2))
                        },
                        ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    )
                }
            )
            .setPositiveButton(R.string.done, null)
            .show()
    }

    private fun BitMatrix.toBitmap() = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888).also { bitmap ->
        (0 until width).forEach { x ->
            (0 until height).forEach { y ->
                bitmap.setPixel(x, y, if (get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
    }
}

object DummyAndroidMenuHost : AndroidMenuHost {
    override fun startLiveNotificationService() = throw IllegalStateException("Using dummy host")
    override fun requestNotificationPermission() = throw IllegalStateException("Using dummy host")
    override fun customizeControls() = throw IllegalStateException("Using dummy host")
    override suspend fun transferPurchase() = throw IllegalStateException("Using dummy host")
}

val LocalAndroidMenuHost = compositionLocalOf<AndroidMenuHost> { DummyAndroidMenuHost }