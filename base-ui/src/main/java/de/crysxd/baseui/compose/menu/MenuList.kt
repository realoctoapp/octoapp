package de.crysxd.baseui.compose.menu

import android.annotation.SuppressLint
import android.os.Parcel
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.with
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.platform.UriHandler
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import de.crysxd.baseui.compose.framework.components.OctoButton
import de.crysxd.baseui.compose.framework.components.OctoText
import de.crysxd.baseui.compose.framework.helpers.TestTags
import de.crysxd.baseui.compose.framework.locals.LocalNestedScrollConnection
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.rememberLottieComposition
import de.crysxd.octoapp.base.data.models.MenuId
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.models.MenuContext
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.menu.Menu
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuHost
import de.crysxd.octoapp.menu.MenuItem
import de.crysxd.octoapp.menu.PreviewMenuItems
import de.crysxd.octoapp.menu.main.MainMenu
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.http.Url
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking

@Composable
@OptIn(ExperimentalAnimationApi::class)
fun MenuList(
    menu: PreparedMenu,
    menuHost: MenuHost,
    modifier: Modifier = Modifier,
    useGrouping: Boolean = true,
    useDescriptions: Boolean = true,
    useEmptyTitle: Boolean = true,
    horizontalMargin: Dp,
    menuId: MenuId,
) = AnimatedContent(
    targetState = menu,
    modifier = modifier,
    transitionSpec = {
        val onlyTexts = targetState.copy(subtitle = null, bottomText = null) == initialState.copy(subtitle = null, bottomText = null)
        val onlyItems = targetState.copy(items = emptyList()) == initialState.copy(items = emptyList())
        if (onlyTexts || onlyItems) {
            //region Only text or items changed, do simple fade
            fadeIn() with fadeOut()
            //endregion
        } else {
            //region "Default", copied from AnimatedContent
            fadeIn(animationSpec = tween(220, delayMillis = 90)) +
                    scaleIn(initialScale = 0.92f, animationSpec = tween(220, delayMillis = 90)) with
                    fadeOut(animationSpec = tween(90))
            //endregion
        }
    }
) { targetState ->
    Column(
        modifier = Modifier.height(IntrinsicSize.Min)
    ) {
        Header(menu = targetState, menuHost = menuHost, horizontalMargin = horizontalMargin, emptyTitle = useEmptyTitle)
        Items(menu = targetState, host = menuHost, id = menuId, horizontalMargin = horizontalMargin, useGrouping = useGrouping, useDescriptions = useDescriptions)
        EmptyDisplay(menu = targetState, horizontalMargin = horizontalMargin)
        Footer(menu = targetState, menuHost = menuHost, horizontalMargin = horizontalMargin)
    }
}

//region Header + Footer
@Composable
@SuppressLint("StateFlowValueCalledInComposition")
private fun Header(
    menu: PreparedMenu,
    menuHost: MenuHost,
    horizontalMargin: Dp,
    emptyTitle: Boolean,
) = Column(
    modifier = Modifier.animateContentSize(spring())
) {
    val title = menu.title?.toHtml()
    val initialSubtitle = (menu.subtitle as? StateFlow)?.value
    val subtitle = menu.subtitle?.collectAsState(initialSubtitle)?.value?.toHtml()
    val customHeaderViewType = menu.customHeaderViewType
    val customViewType = menu.customViewType

    if (customHeaderViewType == null) {
        Column(
            modifier = Modifier
                .padding(horizontal = horizontalMargin)
                .padding(
                    top = if (!title.isNullOrBlank() || subtitle != null || emptyTitle) OctoAppTheme.dimens.margin3 else 0.dp,
                    bottom = if (!title.isNullOrBlank() || subtitle != null) OctoAppTheme.dimens.margin2 else 0.dp
                )
        ) {
            if (!title.isNullOrBlank()) {
                OctoText(
                    text = title,
                    style = OctoAppTheme.typography.title,
                    modifier = Modifier
                        .fillMaxWidth()
                        .testTag(TestTags.Menu.Title),
                    textAlign = TextAlign.Center,
                    onUrlOpened = { menu.raw?.onUrlOpened(menuHost, it) }

                )
            }

            if (subtitle != null) {
                OctoText(
                    text = subtitle,
                    style = OctoAppTheme.typography.label,
                    textAlign = TextAlign.Center,
                    onUrlOpened = { menu.raw?.onUrlOpened(menuHost, it) },
                    modifier = Modifier
                        .fillMaxWidth()
                        .testTag(TestTags.Menu.Subtitle)
                        .padding(top = OctoAppTheme.dimens.margin01),
                )
            }

            if (customViewType != null) {
                CustomMenuHeader(
                    customMenuHeaderType = customViewType,
                    menuHost = menuHost,
                )
            }
        }
    } else {
        CustomMenuHeader(
            customMenuHeaderType = customHeaderViewType,
            menuHost = menuHost,
        )
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
@SuppressLint("StateFlowValueCalledInComposition")
private fun Footer(menu: PreparedMenu, menuHost: MenuHost, horizontalMargin: Dp) = AnimatedContent(
    targetState = menu.bottomText?.collectAsState(initial = (menu.bottomText as? StateFlow)?.value)?.value
) { target ->
    val hostUriHandler = remember(menuHost) {
        object : UriHandler {
            override fun openUri(uri: String) {
                menuHost.openUrl(uri.toUrl())
            }
        }
    }

    if (!target.isNullOrBlank()) {
        CompositionLocalProvider(LocalUriHandler provides hostUriHandler) {
            OctoText(
                text = target.toHtml(),
                style = OctoAppTheme.typography.base,
                modifier = Modifier
                    .fillMaxWidth()
                    .animateContentSize(spring())
                    .padding(horizontal = horizontalMargin)
                    .testTag(TestTags.Menu.Footer)
                    .padding(bottom = OctoAppTheme.dimens.margin2),
                textAlign = TextAlign.Center,
                onUrlOpened = { menu.raw?.onUrlOpened(menuHost, it) }
            )
        }
    }
}

//endregion
//region Items
@Composable
private fun Items(
    menu: PreparedMenu,
    host: MenuHost,
    id: MenuId,
    useGrouping: Boolean,
    useDescriptions: Boolean,
    horizontalMargin: Dp,
) {
    val scrollState = rememberScrollState()
    val hairlineColorTop by animateColorAsState(
        when {
            !scrollState.canScrollBackward -> Color.Transparent
            OctoAppTheme.isDarkMode -> OctoAppTheme.colors.whiteTranslucent3
            else -> OctoAppTheme.colors.veryLightGrey
        }
    )
    val hairlineColorBottom by animateColorAsState(
        when {
            !scrollState.canScrollForward -> Color.Transparent
            OctoAppTheme.isDarkMode -> OctoAppTheme.colors.whiteTranslucent3
            else -> OctoAppTheme.colors.veryLightGrey
        }
    )
    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01),
        modifier = Modifier
            .drawWithContent {
                drawContent()
                val lineSize = size.copy(height = 2.dp.toPx())
                drawRect(color = hairlineColorTop, size = lineSize)
                drawRect(color = hairlineColorBottom, size = lineSize, topLeft = Offset(x = 0f, y = size.height - lineSize.height))
            }
            .run {
                if (scrollState.canScrollBackward) {
                    this
                } else {
                    nestedScroll(LocalNestedScrollConnection.current)
                }
            }
            .verticalScroll(scrollState)
            .padding(horizontal = horizontalMargin)
            .padding(bottom = OctoAppTheme.dimens.margin2)

    ) {
        menu.items.forEachIndexed { index, item ->
            //region Group boundary
            val prevItem = menu.items.getOrNull(index - 1)
            val mainMenu = menu.raw is MainMenu
            val groupChanged = prevItem != null && prevItem.groupId != item.groupId
            if (useGrouping && !mainMenu && groupChanged) {
                Spacer(modifier = Modifier.height(OctoAppTheme.dimens.margin12))
            }
            //endregion
            //region Item
            MenuItem(
                item = item,
                host = host,
                menuId = id,
                useDescriptions = useDescriptions
            )
            //endregion
        }
    }
}

//endregion
//region Empty
@Composable
fun EmptyDisplay(menu: PreparedMenu, horizontalMargin: Dp) = menu.emptyState.takeIf { menu.items.isEmpty() }?.let { empty ->
    Column(
        verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin2)
    ) {
        empty.animation?.let {
            val composition by it.rememberLottieComposition()
            LottieAnimation(
                composition = composition,
                modifier = Modifier
                    .aspectRatio(586 / 256f)
                    .fillMaxWidth()
            )
        }

        empty.actionText?.let { text ->
            OctoButton(
                small = true,
                text = text,
                onClick = empty.action,
                modifier = Modifier
                    .padding(bottom = OctoAppTheme.dimens.margin2)
                    .padding(horizontal = horizontalMargin)
                    .fillMaxWidth(),
            )
        }
    }
}

//endregion

//region Previews
@Composable
private fun PreviewBase(menu: Menu) = OctoAppThemeForPreview {
    MenuList(
        menu = runBlocking { menu.prepare(null, MenuContext.Prepare, SystemInfo()) },
        menuHost = previewHost,
        menuId = MenuId.Other,
        horizontalMargin = 0.dp
    )
}

@Composable
@Preview(group = "Menus")
private fun PreviewMenuListFull() = PreviewBase(
    menu = object : Menu {
        override val id: String = "test"
        override val title: String = "Full menu"
        override val subtitleFlow = flowOf("Some subtitle goes here")
        override val bottomTextFlow = flowOf("Bottom text for <a href=\"\">you</a>?")
        override fun describeContents() = 0
        override fun writeToParcel(p0: Parcel, p1: Int) = Unit
        override suspend fun getMenuItems(): List<MenuItem> = listOf(
            PreviewMenuItems.RevolvingOptions,
            PreviewMenuItems.Secondary,
            PreviewMenuItems.Toggle
        )
    }
)

@Composable
@Preview(group = "Menus")
private fun PreviewMenuListMinimal() = PreviewBase(
    menu = object : Menu {
        override val id: String = "test"
        override val title: String = ""
        override fun describeContents() = 0
        override fun writeToParcel(p0: Parcel, p1: Int) = Unit
        override suspend fun getMenuItems(): List<MenuItem> = listOf(
            PreviewMenuItems.RevolvingOptions,
            PreviewMenuItems.Toggle,
            PreviewMenuItems.Secondary,
        )
    }
)

@Composable
@Preview(group = "Menus")
private fun PreviewMenuListEmpty() = PreviewBase(
    menu = object : Menu {
        override val id: String = "test"
        override val title: String = "Empty Menu"
        override val subtitleFlow = flowOf("Some subtitle goes here")
        override val bottomTextFlow = flowOf("Bottom text for <a href=\"\">you</a>?")
        override val emptyStateActionText = "Some action"
        override val emptyStateAnimation = MenuAnimation.Materials
        override fun describeContents() = 0
        override fun writeToParcel(p0: Parcel, p1: Int) = Unit
        override suspend fun getMenuItems(): List<MenuItem> = emptyList()
    }
)

private val previewHost by lazy {
    object : MenuHost {
        override suspend fun pushMenu(subMenu: Menu) = throw NotImplementedError()
        override suspend fun popMenu() = throw NotImplementedError()
        override suspend fun closeMenu(result: String?) = throw NotImplementedError()
        override fun openUrl(url: Url) = throw NotImplementedError()
        override suspend fun reloadMenu() = throw NotImplementedError()
    }
}
//endregion
