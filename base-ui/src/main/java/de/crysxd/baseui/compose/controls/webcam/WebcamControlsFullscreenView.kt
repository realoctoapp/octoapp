package de.crysxd.baseui.compose.controls.webcam

import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.progress.ProgressControlsState
import de.crysxd.baseui.compose.controls.progress.ProgressValues
import de.crysxd.baseui.compose.framework.components.OctoIconButton
import de.crysxd.baseui.compose.framework.helpers.ScreenPreview
import de.crysxd.baseui.compose.framework.helpers.ScreenPreviewTablet
import de.crysxd.baseui.compose.theme.LocalOctoAppTypography
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.compose.theme.OctoAppTypography
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.models.ProgressControlsRole
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.ProgressControlsViewModelCore
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore
import de.crysxd.octoapp.viewmodels.helper.LowOverheadMediator
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import de.crysxd.octoapp.viewmodels.WebcamControlsViewModelCore.State as WebcamState

@Composable
fun WebcamControlsFullscreenView(
    onNativeAspectRatioChanged: (Float) -> Unit,
    webcamState: WebcamControlsState,
    progressState: ProgressControlsState,
    onShowProgressSettings: () -> Unit,
    insets: PaddingValues,
) {
    val boxState = remember { AspectRatioBoxScope(webcamState.getInitialAspectRatio()) }

    //region Forward aspect ratio to fragment for rotation
    LaunchedEffect(boxState.aspectRatio) {
        onNativeAspectRatioChanged(boxState.aspectRatio)
    }
    //endregion

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
    ) {
        //region Webcam
        WebcamView(
            state = webcamState,
            boxState = boxState,
            allowTouch = true,
            insets = insets,
        )
        //endregion
        if (LocalConfiguration.current.orientation == ORIENTATION_PORTRAIT) {
            OverlayPortrait(
                progressState = progressState,
                webcamState = webcamState,
                insets = insets,
                onShowProgressSettings = onShowProgressSettings
            )
        } else {
            OverlayLandscape(
                progressState = progressState,
                webcamState = webcamState,
                insets = insets,
                onShowProgressSettings = onShowProgressSettings
            )
        }
    }

    InsetOverlays(insets = insets)
}

@Composable
private fun BoxScope.OverlayLandscape(
    progressState: ProgressControlsState,
    webcamState: WebcamControlsState,
    insets: PaddingValues,
    onShowProgressSettings: () -> Unit,
) = Box(
    modifier = Modifier
        .animateContentSize()
        .align(Alignment.BottomStart),
) {
    ProgressWrapper(
        progressState = progressState,
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(top = OctoAppTheme.dimens.margin2)
            .align(Alignment.BottomStart),
        innerModifier = Modifier
            .background(WebcamOverlayBrush)
            .padding(insets)
    )
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom,
        modifier = Modifier
            .padding(insets)
            .padding(horizontal = OctoAppTheme.dimens.margin1)
            .align(Alignment.BottomEnd)
    ) {
        WebcamActions(webcamState = webcamState, horizontal = false) {
            SettingsAction(onShowProgressSettings = onShowProgressSettings)
        }
    }
}


@Composable
private fun BoxScope.OverlayPortrait(
    progressState: ProgressControlsState,
    webcamState: WebcamControlsState,
    insets: PaddingValues,
    onShowProgressSettings: () -> Unit,
) = Box(
    modifier = Modifier
        .fillMaxWidth()
        .align(Alignment.BottomStart),
) {
    ProgressWrapper(
        progressState = progressState,
        innerModifier = Modifier
            .background(WebcamOverlayBrush)
            .padding(insets)
            .padding(bottom = OctoAppTheme.dimens.margin3)
    )
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = OctoAppTheme.dimens.margin1)
            .align(Alignment.BottomStart)
            .padding(insets)
    ) {
        WebcamActions(webcamState = webcamState) {
            if (progressState.state.printing) {
                SettingsAction(onShowProgressSettings = onShowProgressSettings)
            }

            Spacer(modifier = Modifier.weight(1f))
        }
    }
}

@Composable
private fun SettingsAction(
    onShowProgressSettings: () -> Unit,
) = OctoIconButton(
    painter = painterResource(id = R.drawable.ic_round_settings_24),
    contentDescription = stringResource(id = R.string.cd_settings),
    tint = OctoAppTheme.colors.white,
    onClick = onShowProgressSettings,
)

@Composable
private fun ProgressWrapper(
    progressState: ProgressControlsState,
    modifier: Modifier = Modifier,
    innerModifier: Modifier = Modifier,
) = Box(modifier) {
    AnimatedVisibility(
        visible = progressState.state.printing,
        enter = fadeIn() + slideInVertically { it / 2 },
        exit = fadeOut() + slideOutVertically { it / 2 },
    ) {
        CompositionLocalProvider(LocalOctoAppTypography provides InvertedOctoAppTypography(OctoAppTheme.typography)) {
            ProgressValues(
                state = { progressState },
                modifier = innerModifier.padding(OctoAppTheme.dimens.margin2),
                role = ProgressControlsRole.ForWebcamFullscreen
            )
        }
    }
}

//region Inset Overlays
@Composable
private fun InsetOverlays(insets: PaddingValues) = Box(Modifier.fillMaxSize()) {
    val color = Color.White.copy(alpha = 0.1f)
    Box(
        modifier = Modifier
            .background(color)
            .fillMaxWidth()
            .height(insets.calculateBottomPadding())
            .align(Alignment.BottomCenter)
    )

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .padding(top = insets.calculateTopPadding(), bottom = insets.calculateBottomPadding())
            .background(color)
            .width(insets.calculateStartPadding(LocalLayoutDirection.current))
            .align(Alignment.CenterStart)
    )

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .padding(top = insets.calculateTopPadding(), bottom = insets.calculateBottomPadding())
            .background(color)
            .width(insets.calculateEndPadding(LocalLayoutDirection.current))
            .align(Alignment.CenterEnd)
    )

    Box(
        modifier = Modifier
            .background(color)
            .fillMaxWidth()
            .height(insets.calculateTopPadding())
            .align(Alignment.TopCenter)
    )
}

//endregion
//region Custom Theme
class InvertedOctoAppTypography(base: OctoAppTypography) : OctoAppTypography by base {
    override val label: TextStyle
        @Composable get() = super.label.copy(color = Color.White)

    override val labelSmall: TextStyle
        @Composable get() = super.labelSmall.copy(color = Color.White)

    override val base: TextStyle
        @Composable get() = super.base.copy(color = Color.White)

    override val data: TextStyle
        @Composable get() = super.data.copy(color = Color.White)
}
//endregion

//region Preview
@Composable
private fun PreviewMjpeg() = OctoAppThemeForPreview {
    var enforcedAspectRatio by remember { mutableStateOf<String?>("16:9") }
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        while (isActive) {
            delay(2000)
            enforcedAspectRatio = null
            delay(2000)
            enforcedAspectRatio = "16:9"
        }
    }

    WebcamControlsFullscreenView(
        onNativeAspectRatioChanged = {},
        insets = PaddingValues(),
        webcamState = object : WebcamControlsState {
            override val isFullscreen = true
            override val enforcedAspectRatio = null
            override val showResolution = true
            override val showName = false
            override var grabImage: (suspend () -> Bitmap)? = null
            override val current = WebcamState.MjpegReady(
                webcamCount = 2,
                flipH = false,
                flipV = false,
                rotate90 = false,
                frameInterval = 0,
                frameWidth = 1280,
                frameHeight = 720,
                displayName = null,
                activeWebcam = 1,
                warning = null,
                frames = LowOverheadMediator(
                    WebcamControlsViewModelCore.State.MjpegReady.Frame(
                        image = ContextCompat.getDrawable(context, R.drawable.webcam_demo)!!.toBitmap(),
                        frameInterval = 0,
                        fps = 13.37f,
                        frameTime = System.currentTimeMillis(),
                    )
                ),
            )

            override fun onRetry() = Unit
            override fun onSwitchWebcam() = Unit
            override fun onShareImage(instanceId: String, img: suspend () -> Bitmap) = Unit
            override fun onFullscreenClicked() = Unit
            override fun onTroubleshoot() = Unit
            override fun getInitialAspectRatio() = 16 / 9f
            override fun updateAspectRatio(ratio: Float) = Unit
            override fun getInitialScaleType() = ImageView.ScaleType.FIT_CENTER
            override fun updateScaleToFillType(type: ImageView.ScaleType) = Unit
        },
        progressState = object : ProgressControlsState {
            override val state = ProgressControlsViewModelCore.State(
                settings = ProgressWidgetSettings(),
                activeFile = FlowState.Loading(),
                fullscreenSettings = ProgressWidgetSettings(printNameStyle = ProgressWidgetSettings.PrintNameStyle.None),
                printing = false,
                fullscreenDataItems = emptyList(),
                dataItems = emptyList(),
                completion = 0f,
                status = "",
            )
        },
        onShowProgressSettings = {}
    )
}

@Composable
@ScreenPreview
private fun PreviewMjpegPortrait() = PreviewMjpeg()

@Composable
@ScreenPreviewTablet
private fun PreviewMjpegLand() = PreviewMjpeg()
//endregion