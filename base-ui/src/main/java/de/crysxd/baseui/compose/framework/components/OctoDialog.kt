package de.crysxd.baseui.compose.framework.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import de.crysxd.baseui.compose.theme.LocalOctoActivity
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
fun OctoDialog(
    title: String?,
    message: String,
    positiveButton: String = stringResource(android.R.string.ok),
    negativeButton: String? = null,
    onPositive: suspend () -> Unit = {},
    onDismissRequest: () -> Unit,
) = OctoDialog(
    title = title,
    message = AnnotatedString(message),
    positiveButton = positiveButton,
    negativeButton = negativeButton,
    onPositive = onPositive,
    onDismissRequest = onDismissRequest,
)

@Composable
fun OctoDialog(
    title: String?,
    message: AnnotatedString,
    positiveButton: String = stringResource(android.R.string.ok),
    negativeButton: String? = null,
    onPositive: suspend () -> Unit = {},
    onDismissRequest: () -> Unit,
) = OctoDialog(
    title = title,
    content = {
        Text(
            text = message,
            style = OctoAppTheme.typography.base,
            color = OctoAppTheme.colors.normalText,
        )
    },
    positiveButton = positiveButton,
    negativeButton = negativeButton,
    onPositive = onPositive,
    onDismissRequest = onDismissRequest,
)

@Composable
fun OctoErrorDialog(
    throwable: Throwable,
    onDismissRequest: () -> Unit,
) {
    val activity = LocalOctoActivity.current
    LaunchedEffect(throwable) {
        activity?.showDialog(throwable)
    }
}

@Composable
fun OctoDialog(
    title: String?,
    content: @Composable () -> Unit,
    positiveButton: String,
    negativeButton: String?,
    onPositive: suspend () -> Unit,
    onDismissRequest: () -> Unit,
) = AlertDialog(
    onDismissRequest = onDismissRequest,
    confirmButton = {
        Row {
            negativeButton?.let {
                OctoButton(
                    text = negativeButton,
                    onClick = onDismissRequest,
                    small = true,
                    type = OctoButtonType.Link,
                    modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin1)
                )
            }
            OctoButton(
                text = positiveButton,
                onClick = {
                    onPositive()
                    onDismissRequest()
                },
                small = true,
                type = OctoButtonType.Primary,
                modifier = Modifier.padding(bottom = OctoAppTheme.dimens.margin1, end = OctoAppTheme.dimens.margin1)
            )
        }
    },
    title = if (title != null) {
        {
            Text(
                text = title,
                style = OctoAppTheme.typography.title,
                color = OctoAppTheme.colors.darkText,
            )
        }
    } else {
        null
    },
    text = content
)