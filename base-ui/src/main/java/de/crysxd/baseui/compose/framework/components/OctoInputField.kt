package de.crysxd.baseui.compose.framework.components

import androidx.compose.animation.Crossfade
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.seconds


@Composable
fun OctoInputField(
    modifier: Modifier = Modifier,
    value: String,
    onValueChanged: (String) -> Unit,
    placeholder: String,
    label: String,
    labelActive: String,
    readOnly: Boolean = false,
    maxLines: Int = 1,
    focusInitially: Boolean = false,
    alternativeBackground: Boolean = false,
    keyboardOptions: KeyboardOptions = KeyboardOptions(),
    keyboardActions: KeyboardActions = KeyboardActions { },
    decoration: (@Composable () -> Unit)? = null
) {
    var textFieldValue by remember { mutableStateOf(TextFieldValue(text = value, selection = TextRange(value.length))) }
    LaunchedEffect(value) {
        textFieldValue = textFieldValue.copy(text = value)
    }
    LaunchedEffect(textFieldValue) {
        onValueChanged(textFieldValue.text)
    }

    OctoInputField(
        value = textFieldValue,
        onValueChanged = { textFieldValue = it },
        placeholder = placeholder,
        label = label,
        labelActive = labelActive,
        modifier = modifier,
        readOnly = readOnly,
        maxLines = maxLines,
        alternativeBackground = alternativeBackground,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        focusInitially = focusInitially,
        decoration = decoration,
    )
}

@Composable
fun OctoInputField(
    value: TextFieldValue,
    onValueChanged: (TextFieldValue) -> Unit,
    placeholder: String,
    label: String,
    labelActive: String,
    modifier: Modifier = Modifier,
    readOnly: Boolean = false,
    maxLines: Int = 1,
    focusInitially: Boolean = false,
    alternativeBackground: Boolean = false,
    keyboardOptions: KeyboardOptions = KeyboardOptions(),
    keyboardActions: KeyboardActions = KeyboardActions { },
    decoration: (@Composable () -> Unit)? = null
) = Box {
    val focusManager = LocalFocusManager.current
    var isFocused by remember { mutableStateOf(false) }
    val shape = MaterialTheme.shapes.large
    val borderColor by animateColorAsState(targetValue = OctoAppTheme.colors.accent.copy(alpha = if (isFocused) 0.5f else 0f), label = "borderColor")
    val borderWidth by animateDpAsState(targetValue = if (isFocused) 1.dp else 0.dp, label = "borderWidth")
    val keyboardOpen by keyboardAsState()
    val createdAt = remember { Clock.System.now() }
    val focusRequester = remember { FocusRequester() }

    LaunchedEffect(keyboardOpen) {
        if (!keyboardOpen && Clock.System.now() - createdAt > 1.seconds) {
            focusManager.clearFocus()
        }
    }

    LaunchedEffect(Unit) {
        if (focusInitially) {
            focusRequester.requestFocus()
        }
    }

    TextField(
        value = value,
        onValueChange = onValueChanged,
        readOnly = readOnly,
        placeholder = {
            Text(
                text = placeholder,
                style = OctoAppTheme.typography.input,
                color = OctoAppTheme.colors.lightText
            )
        },
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            textColor = OctoAppTheme.colors.darkText,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
        ),
        shape = RectangleShape,
        modifier = modifier
            .fillMaxWidth()
            .focusRequester(focusRequester)
            .onFocusChanged { isFocused = it.isFocused && !readOnly }
            .border(width = borderWidth, color = borderColor, shape = shape)
            .clip(shape)
            .background(if (alternativeBackground) OctoAppTheme.colors.inputBackgroundAlternative else OctoAppTheme.colors.inputBackground)
            .padding(vertical = OctoAppTheme.dimens.margin1, horizontal = OctoAppTheme.dimens.margin01),
        textStyle = OctoAppTheme.typography.input,
        maxLines = maxLines,
        singleLine = maxLines == 1,
        label = {
            Crossfade(targetState = isFocused || value.text.isNotEmpty(), label = "focused") {
                Text(
                    text = if (it) labelActive else label,
                    style = if (it) OctoAppTheme.typography.label else OctoAppTheme.typography.input,
                    color = if (it) OctoAppTheme.colors.accent else OctoAppTheme.colors.lightText,
                    maxLines = 1
                )
            }
        },
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        trailingIcon = decoration
    )
}

@Composable
private fun keyboardAsState(): State<Boolean> {
    val isImeVisible = WindowInsets.ime.getBottom(LocalDensity.current) > 0
    return rememberUpdatedState(isImeVisible)
}

@Preview
@Composable
private fun Preview() = OctoAppThemeForPreview {
    var value by remember { mutableStateOf(TextFieldValue("")) }

    OctoInputField(
        value = value,
        onValueChanged = { value = it },
        modifier = Modifier.padding(20.dp),
        label = "OctoPrint address",
        placeholder = "e.g http://octopi.local",
        labelActive = "OctoPrint address - copy this from your browser!"
    )
}
