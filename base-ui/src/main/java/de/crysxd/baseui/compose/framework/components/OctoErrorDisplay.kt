package de.crysxd.baseui.compose.framework.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.helpers.asAnnotatedString
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.octoapp.base.ext.composeErrorMessage


@Composable
fun OctoErrorDisplay(
    modifier: Modifier = Modifier,
    description: String? = null,
    throwable: Throwable? = null,
    actionLabel: String? = stringResource(id = R.string.retry_general),
    onAction: (suspend () -> Unit)? = null,
    retryLabel: String? = stringResource(id = R.string.retry_general),
    onRetry: (suspend () -> Unit)? = null,
    textColor: Color? = null,
) = OctoErrorDisplay(
    modifier = modifier,
    description = description?.asAnnotatedString(),
    throwable = throwable,
    actionLabel = actionLabel,
    onAction = onAction,
    retryLabel = retryLabel,
    onRetry = onRetry,
    textColor = textColor,
)


@Composable
fun OctoErrorDisplay(
    modifier: Modifier = Modifier,
    title: String = stringResource(id = R.string.error_general),
    description: String? = null,
    throwable: Throwable? = null,
    actionLabel: String? = stringResource(id = R.string.retry_general),
    onAction: (suspend () -> Unit)? = null,
    retryLabel: String? = stringResource(id = R.string.retry_general),
    onRetry: (suspend () -> Unit)? = null,
    textColor: Color? = null,
) = OctoErrorDisplay(
    modifier = modifier,
    title = title.asAnnotatedString(),
    description = description?.asAnnotatedString(),
    throwable = throwable,
    actionLabel = actionLabel,
    onAction = onAction,
    retryLabel = retryLabel,
    onRetry = onRetry,
    textColor = textColor,
)

@Composable
fun OctoErrorDisplay(
    modifier: Modifier = Modifier,
    title: AnnotatedString = stringResource(id = R.string.error_general).asAnnotatedString(),
    description: AnnotatedString? = null,
    throwable: Throwable? = null,
    actionLabel: String? = stringResource(id = R.string.retry_general),
    onAction: (suspend () -> Unit)? = null,
    retryLabel: String? = stringResource(id = R.string.retry_general),
    onRetry: (suspend () -> Unit)? = null,
    textColor: Color? = null,
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
    modifier = modifier
) {
    Title(text = title, textColor = textColor)
    Detail(text = description ?: throwable?.composeErrorMessage(), textColor = textColor)
    Row(
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
    ) {
        if (actionLabel != null && onAction != null) {
            OctoButton(
                type = OctoButtonType.Secondary,
                small = true,
                text = actionLabel,
                modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
                onClick = onAction,
            )
        }

        if (retryLabel != null && onRetry != null) {
            OctoButton(
                small = true,
                text = retryLabel,
                modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
                onClick = onRetry
            )
        }
    }
}

@Composable
private fun Title(
    text: AnnotatedString,
    textColor: Color?,
    maxLines: Int = 3,
) = Text(
    text = text,
    style = OctoAppTheme.typography.subtitle,
    color = textColor ?: OctoAppTheme.colors.darkText,
    textAlign = TextAlign.Center,
    maxLines = maxLines,
    overflow = TextOverflow.Ellipsis,
)

@Composable
private fun Detail(
    text: CharSequence?,
    textColor: Color?,
) = text?.let {
    Text(
        text = text.asAnnotatedString(),
        style = OctoAppTheme.typography.label,
        color = textColor ?: OctoAppTheme.colors.lightText,
        textAlign = TextAlign.Center,
        maxLines = 5,
        modifier = Modifier.padding(top = OctoAppTheme.dimens.margin01),
        overflow = TextOverflow.Ellipsis,
    )
}