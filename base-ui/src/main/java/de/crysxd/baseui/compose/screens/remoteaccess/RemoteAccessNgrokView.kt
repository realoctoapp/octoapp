package de.crysxd.baseui.compose.screens.remoteaccess

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import de.crysxd.baseui.BaseViewModel
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.locals.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.connectCore
import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.viewmodels.RemoteAccessBaseViewModelCore
import de.crysxd.octoapp.viewmodels.RemoteAccessNgrokViewModelCore

@Composable
fun RemoteAccessNgrokView(
    controller: ConfigureNgrokViewController = rememberNgrokController()
) = RemoteAccessServiceView(
    controller = controller,
    backgroundColor = Color.White,
    titleBarBackgroundColor = Color.White,
    boxBackgroundColor = Color(0xFFF1F4F6),
    foregroundColor = Color.Black,
    titleBarElevation = 8.dp,
    bottomText = stringResource(R.string.configure_remote_acces___ngrok___disclaimer),
    titleLogo = painterResource(R.drawable.ngrok_logo),
    titleBarTagColor = Color.Black,
    titleBarTagText = stringResource(id = R.string.configure_remote_acces___free_tier_available),
    descriptionText = stringResource(R.string.configure_remote_acces___ngrok___description),
    modifier = Modifier,
    usps = listOf(
        RemoteAccessUsp(
            icon = R.drawable.ic_round_network_check_24,
            description = stringResource(R.string.configure_remote_acces___ngrok___usp_1)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_warning_amber_24,
            description = stringResource(R.string.configure_remote_acces___ngrok___usp_2)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_star_24,
            description = stringResource(R.string.configure_remote_acces___ngrok___usp_3)
        ),
        RemoteAccessUsp(
            icon = R.drawable.ic_round_lock_24,
            description = stringResource(R.string.configure_remote_acces___ngrok___usp_4)
        )
    )
) { state ->
    Column(
        modifier = Modifier
            .padding(horizontal = OctoAppTheme.dimens.margin2)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        if (state.connected) {
            RemoteAccessConnected(
                text = stringResource(id = R.string.configure_remote_acces___ngrok___connected),
            )

            Text(
                text = state.url ?: "",
                style = OctoAppTheme.typography.sectionHeader,
                color = Color.Black
            )
        } else {
            Text(
                text = stringResource(
                    id = if (controller.supportsAutoConnect) {
                        R.string.configure_remote_acces___ngrok___info_auto_connect
                    } else {
                        R.string.configure_remote_acces___ngrok___info_manual_connect
                    }
                ),
                style = OctoAppTheme.typography.sectionHeader,
                color = Color.Black,
                textAlign = TextAlign.Center,
            )

        }
    }
}

//region State
@Composable
private fun rememberNgrokController(): ConfigureNgrokViewController {
    val instanceId = LocalOctoPrint.current.id
    val vmFactory = ConfigureNgrokViewModel.Factory(instanceId)
    val viewModel = viewModel<ConfigureNgrokViewModel>(
        factory = vmFactory,
        key = vmFactory.id,
    )
    return object : ConfigureNgrokViewController {
        override val state by viewModel.connectedState.collectAsState(
            initial = RemoteAccessBaseViewModelCore.State(
                connected = false,
                loading = false,
                failure = null,
                url = null
            )
        )
        override val supportsAutoConnect by viewModel.supportsAutoConnect.collectAsState(initial = false)

        override suspend fun disconnect() = viewModel.disconnect()
    }
}

interface ConfigureNgrokViewController : ConfigureRemoteAccessViewController {
    val supportsAutoConnect: Boolean
}

private class ConfigureNgrokViewModel(instanceId: String) : BaseViewModel() {
    val core = connectCore(RemoteAccessNgrokViewModelCore(instanceId))
    val connectedState = core.connectedState
    val supportsAutoConnect = core.supportsAutoConnect

    suspend fun disconnect() = core.disconnect()

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ConfigureNgrokViewModel(
            instanceId = instanceId,
        ) as T
    }
}

//endregion
//region Preview
@Composable
private fun PreviewBase(connected: Boolean, failure: RemoteConnectionFailure?) = OctoAppThemeForPreview {
    RemoteAccessNgrokView(
        controller = object : ConfigureNgrokViewController {
            override val supportsAutoConnect = true
            override val state = RemoteAccessBaseViewModelCore.State(connected = connected, loading = false, failure = failure, url = null)
            override suspend fun disconnect() = false
        }
    )
}

@Composable
@Preview(showSystemUi = true)
private fun PreviewIdle() = PreviewBase(connected = false, failure = null)

@Composable
@Preview(showSystemUi = true)
private fun PreviewFailure() = PreviewBase(
    connected = false,
    failure = RemoteConnectionFailure(
        errorMessage = "Some",
        errorMessageStack = "",
        stackTrace = "",
        remoteServiceName = "",
        dateMillis = 0
    )
)

@Composable
@Preview(showSystemUi = true)
private fun PreviewConnected() = PreviewBase(connected = true, failure = null)
//endregion