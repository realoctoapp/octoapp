package de.crysxd.baseui.compose.screens.pluginlibrary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.crysxd.baseui.utils.ViewModelWithCore
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.viewmodels.PluginsLibraryViewModelCore
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

class PluginLibraryViewModel(val instanceId: String) : ViewModelWithCore() {

    override val core by lazy { PluginsLibraryViewModelCore(instanceId) }
    val index
        get() = core.index
            .map { FlowState.Ready(it) as FlowState<PluginsLibraryViewModelCore.PluginsIndex> }
            .onStart { emit(FlowState.Loading()) }
            .catch { emit(FlowState.Error(it)) }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "${this::class.qualifiedName}@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = PluginLibraryViewModel(
            instanceId = instanceId,
        ) as T
    }
}