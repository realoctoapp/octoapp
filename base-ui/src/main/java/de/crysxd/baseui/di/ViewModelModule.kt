package de.crysxd.baseui.di

import android.app.Application
import androidx.lifecycle.ViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import de.crysxd.baseui.BaseViewModelFactory
import de.crysxd.baseui.common.NetworkStateViewModel
import de.crysxd.baseui.common.enter_value.EnterValueViewModel
import de.crysxd.baseui.common.feedback.SendFeedbackViewModel
import de.crysxd.baseui.common.gcode.GcodePreviewViewModel
import de.crysxd.baseui.purchase.PurchaseViewModel
import de.crysxd.baseui.usecase.OpenEmailClientForFeedbackUseCase
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.PrinterConfigurationRepository
import de.crysxd.octoapp.base.di.ViewModelKey
import de.crysxd.octoapp.base.network.PrinterEngineProvider
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import javax.inject.Provider

@Module
open class ViewModelModule {

    @Provides
    fun bindViewModelFactory(creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>): BaseViewModelFactory =
        BaseViewModelFactory(creators)

    @Provides
    @IntoMap
    @ViewModelKey(EnterValueViewModel::class)
    open fun provideEnterValueViewModel(): ViewModel = EnterValueViewModel()

    @Provides
    @IntoMap
    @ViewModelKey(SendFeedbackViewModel::class)
    open fun provideSendFeedbackViewModel(
        sendUseCase: OpenEmailClientForFeedbackUseCase,
        bugReportUseCase: CreateBugReportUseCase,
    ): ViewModel = SendFeedbackViewModel(
        createBugReportUseCase = bugReportUseCase,
        sendFeedbackUseCase = sendUseCase
    )


    @Provides
    @IntoMap
    @ViewModelKey(GcodePreviewViewModel::class)
    open fun provideGcodePreviewViewModel(
        printerEngineProvider: PrinterEngineProvider,
        printerConfigurationRepository: PrinterConfigurationRepository,
        gcodeFileRepository: GcodeFileRepository,
        octoPreferences: OctoPreferences,
    ): ViewModel = GcodePreviewViewModel(
        printerConfigurationRepository = printerConfigurationRepository,
        printerEngineProvider = printerEngineProvider,
        gcodeFileRepository = gcodeFileRepository,
        octoPreferences = octoPreferences,
    )

    @Provides
    @IntoMap
    @ViewModelKey(PurchaseViewModel::class)
    open fun providePurchaseViewModel(
    ): ViewModel = PurchaseViewModel(
    )

    @Provides
    @IntoMap
    @ViewModelKey(NetworkStateViewModel::class)
    open fun provideNetworkStateViewModel(
        application: Application
    ): ViewModel = NetworkStateViewModel(
        application = application
    )
}