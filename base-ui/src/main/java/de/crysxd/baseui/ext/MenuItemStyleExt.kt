package de.crysxd.baseui.ext

import androidx.annotation.ColorRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import de.crysxd.baseui.R
import de.crysxd.octoapp.menu.MenuItemStyle

val MenuItemStyle.foregroundColor
    @Composable get() = colorResource(
        id = foregroundColorRes
    )

val MenuItemStyle.foregroundColorRes
    @ColorRes get() = when (this) {
        MenuItemStyle.Support -> R.color.menu_style_support_foreground
        MenuItemStyle.Settings -> R.color.menu_style_settings_foreground
        MenuItemStyle.OctoPrint -> R.color.menu_style_octoprint_foreground
        MenuItemStyle.Printer -> R.color.menu_style_printer_foreground
        MenuItemStyle.Neutral -> R.color.menu_style_neutral_foreground
    }

val MenuItemStyle.backgroundColor
    @Composable get() = colorResource(
        id = backgroundColorRes
    )

val MenuItemStyle.backgroundColorRes
    @ColorRes get() = when (this) {
        MenuItemStyle.Support -> R.color.menu_style_support_background
        MenuItemStyle.Settings -> R.color.menu_style_settings_background
        MenuItemStyle.OctoPrint -> R.color.menu_style_octoprint_background
        MenuItemStyle.Printer -> R.color.menu_style_printer_background
        MenuItemStyle.Neutral -> R.color.menu_style_neutral_background
    }
