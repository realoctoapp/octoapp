package de.crysxd.baseui.ext

import androidx.compose.runtime.Composable
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import de.crysxd.baseui.R
import de.crysxd.octoapp.menu.MenuAnimation
import de.crysxd.octoapp.menu.MenuAnimation.Materials
import de.crysxd.octoapp.menu.MenuAnimation.Notificiations
import de.crysxd.octoapp.menu.MenuAnimation.PowerDevices

@Composable
fun MenuAnimation.rememberLottieComposition() = rememberLottieComposition(
    LottieCompositionSpec.RawRes(
        when (this) {
            Notificiations -> R.raw.octo_wave
            PowerDevices -> R.raw.octo_power
            Materials -> R.raw.octo_material
        }
    )
)