package de.crysxd.baseui.ext

import de.crysxd.octoapp.sharedcommon.utils.HexColor
import androidx.compose.ui.graphics.Color as ComposeColor

fun HexColor.toCompose() = ComposeColor(red = red, blue = blue, green = green, alpha = 1f)