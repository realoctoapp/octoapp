package de.crysxd.baseui.ext

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.viewmodels.BaseViewModelCore
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch

fun ViewModel.connectCore(core: () -> BaseViewModelCore) {
    viewModelScope.launch {
        delay(10)
        core().errors.filterNotNull().collectLatest {
            ExceptionReceivers.dispatchException(it)
        }
    }
}

fun <T : BaseViewModelCore> ViewModel.connectCore(core: T): T {
    viewModelScope.launch {
        core.errors.filterNotNull().collectLatest {
            ExceptionReceivers.dispatchException(it)
        }
    }

    return core
}