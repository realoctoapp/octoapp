package de.crysxd.baseui.ext

import de.crysxd.baseui.utils.ThemePlugin
import de.crysxd.octoapp.base.di.BaseInjector
import io.noties.markwon.Markwon

private val markdown by lazy {
    Markwon.builder(BaseInjector.get().context())
        .usePlugin(ThemePlugin(BaseInjector.get().context()))
        .build()
}

fun String.toMarkdown(): CharSequence = markdown.toMarkdown(this)