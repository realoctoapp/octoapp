package de.crysxd.baseui

import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.sharedcommon.ext.withLocale
import kotlinx.coroutines.runBlocking
import java.util.Locale

abstract class LocalizedActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        val locale = runBlocking {
            BaseInjector.get().getAppLanguageUseCase().execute(Unit)
        }.appLanguage.let {
            Locale.forLanguageTag(it)
        }

        super.attachBaseContext(newBase.withLocale(locale))
    }

    protected fun removeTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            overrideActivityTransition(Activity.OVERRIDE_TRANSITION_OPEN, 0, 0)
            overrideActivityTransition(Activity.OVERRIDE_TRANSITION_CLOSE, 0, 0)
        } else {
            @Suppress("DEPRECATION")
            overridePendingTransition(0, 0)
        }
    }
}