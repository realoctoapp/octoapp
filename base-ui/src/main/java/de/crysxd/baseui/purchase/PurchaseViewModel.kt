package de.crysxd.baseui.purchase

import androidx.lifecycle.asLiveData
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.models.BillingState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged

class PurchaseViewModel : BaseViewModel() {

    private val viewStateFlow = MutableStateFlow<ViewState>(ViewState.InitState)
    val viewState = BillingManager.state.combine(viewStateFlow) { billingState, viewState ->
        when {
            !billingState.isBillingAvailable -> ViewState.Unsupported
            viewState is ViewState.SkuSelectionState -> viewState.copy(
                billingState = billingState,
            )

            else -> viewState
        }
    }.distinctUntilChanged().asLiveData()

    fun moveToSkuListState() {
        viewStateFlow.value = ViewState.SkuSelectionState(BillingManager.state.value)
    }

    fun moveToInitState() {
        viewStateFlow.value = ViewState.InitState
    }

    sealed class ViewState {
        data object InitState : ViewState()
        data object Unsupported : ViewState()
        data class SkuSelectionState(val billingState: BillingState) : ViewState()
    }
}