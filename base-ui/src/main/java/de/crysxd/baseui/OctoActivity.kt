package de.crysxd.baseui

import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.ext.launchWhenCreatedFixed
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.UiEvent
import de.crysxd.octoapp.base.exceptions.BrokenSetupException
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.ext.composeMessageStack
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.RemoteServiceConnectionBrokenException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch

abstract class OctoActivity : LocalizedActivity() {

    companion object {
        var instance: OctoActivity? = null
            private set
    }

    private val tag = "OctoActivity"
    private var visibleDialog: AlertDialog? = null
    private var visibleDialogHash: Int = 0
    private var dialogHasHighPriority = false
    abstract val lastInsets: Rect
    abstract val rootLayout: FrameLayout
    abstract val navController: NavController
    private val handler = Handler(Looper.getMainLooper())
    private val snackbarMessageChannel = MutableStateFlow<Message.SnackbarMessage?>(null)
    protected var lightStatusBarRequested = false
    var isVisible = false
        private set

    @Suppress("EXPERIMENTAL_API_USAGE")
    @OptIn(FlowPreview::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        instance = this
        super.onCreate(savedInstanceState)

        // Debounce snackbars to prevent them from "flashing up"
        lifecycleScope.launchWhenCreatedFixed {
            snackbarMessageChannel
                .debounce(300)
                .filterNotNull()
                .collect(::doShowSnackbar)
        }

        // Register as ExceptionReceiver
        lifecycleScope.launch {
            ExceptionReceivers.flow.collectLatest(::showDialog)
        }
    }

    fun setLightStatusBar(light: Boolean) {
        lightStatusBarRequested = light
        applySystemBarColors()
    }

    protected abstract fun applySystemBarColors()

    override fun onStart() {
        super.onStart()
        // Remove splash background
        window.decorView.background = ColorDrawable(ContextCompat.getColor(this, R.color.window_background))
        isVisible = true
    }

    override fun onStop() {
        super.onStop()
        isVisible = false
    }

    override fun onDestroy() {
        try {
            super.onDestroy()
            instance = null
        } catch (e: java.lang.IllegalStateException) {
            // Testing crash...
        }
    }

    fun observeErrorEvents(events: LiveData<UiEvent<Throwable>>) = events.observe(this) {
        it.value?.let(this::showDialog)
    }

    abstract fun applyInsetsToView(view: View)

    abstract fun enforceAllowAutomaticNavigationFromCurrentDestination()

    fun observerMessageEvents(events: LiveData<UiEvent<Message>>) = events.observe(this) { event ->
        when (val message = event.value) {
            null -> Unit
            is Message.SnackbarMessage -> showSnackbar(message)
            is Message.DialogMessage -> showDialog(message)
        }
    }

    fun showSnackbar(message: Message.SnackbarMessage) {
        if (message.debounce) {
            snackbarMessageChannel.value = message
        } else {
            doShowSnackbar(message)
        }
    }

    private fun doShowSnackbar(message: Message.SnackbarMessage) = handler.post {
        snackbarMessageChannel.value = null
        Snackbar.make(rootLayout, message.text(this), message.duration).apply {
            message.actionText(this@OctoActivity)?.let {
                setAction(it) { message.action(this@OctoActivity) }
            }

            setBackgroundTint(
                ContextCompat.getColor(
                    this@OctoActivity, when (message.type) {
                        Message.SnackbarMessage.Type.Neutral -> R.color.snackbar_neutral
                        Message.SnackbarMessage.Type.Positive -> R.color.snackbar_positive
                        Message.SnackbarMessage.Type.Negative -> R.color.snackbar_negative
                    }
                )
            )

            val foregroundColor = ContextCompat.getColor(this@OctoActivity, R.color.text_colored_background)
            setTextColor(foregroundColor)
            setActionTextColor(foregroundColor)
        }.also {
            it.view.updateLayoutParams<FrameLayout.LayoutParams> {
                gravity = Gravity.TOP
            }
        }.show()
    }

    fun showDialog(message: Message.DialogMessage) {
        showDialog(
            message = message.text(this),
            positiveButton = message.positiveButton(this),
            positiveAction = message.positiveAction,
            neutralButton = message.neutralButton(this),
            neutralAction = message.neutralAction,
            title = message.title.invoke(this),
            negativeAction = message.negativeAction,
            negativeButton = message.negativeButton(this),
            dismissAction = message.dismissAction,
        )
    }

    fun showDialog(e: Throwable) {
        // Safeguard that we don't show an error for cancellation exceptions
        val hasLearnMoreLink = e is NetworkException && e.learnMoreLink != null
        if (e !is CancellationException && (e.cause !is CancellationException || e.cause is TimeoutCancellationException)) {
            Napier.e(tag = tag, throwable = e, message = "Failed")

            if (e is BrokenSetupException) {
                showDialog(
                    message = e.userMessage,
                    positiveAction = {
                        if (e.needsRepair) {
                            UriLibrary.getFixOctoPrintConnectionUri(baseUrl = e.instance.webUrl, instanceId = e.instance.id).open(this)
                        }

                        if (e.cause is RemoteServiceConnectionBrokenException) {
                            UriLibrary.getConfigureRemoteAccessUri().open()
                        }
                    },
                    positiveButton = getString(R.string.sign_in___continue),
                    highPriority = true
                )
            } else {
                showDialog(
                    message = e.composeErrorMessage(),
                    neutralAction = {
                        if (hasLearnMoreLink) {
                            Uri.parse((e as NetworkException).learnMoreLink).open(this)
                        } else {
                            showErrorDetailsDialog(e)
                        }
                    },
                    neutralButton = if (hasLearnMoreLink) {
                        getString(R.string.learn_more)
                    } else {
                        getString(R.string.show_details)
                    },
                    highPriority = hasLearnMoreLink
                )
            }
        }
    }

    fun showErrorDetailsDialog(e: Throwable, offerSupport: Boolean = true) = showDialog(
        message = e.composeMessageStack(),
        neutralAction = {
            OctoAnalytics.logEvent(OctoAnalytics.Event.SupportFromErrorDetails)
            UriLibrary.getHelpUri().open(this)
        },
        neutralButton = if (offerSupport) {
            getString(R.string.get_support)
        } else {
            null
        }
    )

    private fun showDialog(
        message: CharSequence,
        positiveButton: CharSequence = getString(android.R.string.ok),
        positiveAction: (Context) -> Unit = {},
        neutralButton: CharSequence? = null,
        neutralAction: (Context) -> Unit = {},
        negativeButton: CharSequence? = null,
        negativeAction: (Context) -> Unit = {},
        dismissAction: Context.() -> Unit = {},
        highPriority: Boolean = false,
        title: CharSequence? = null,
    ) = handler.post {
        val hash = calculateDialogHash(
            message = message,
            positiveButton = positiveButton,
            negativeButton = negativeButton,
            neutralButton = neutralButton,
            title = title,
        )

        if (visibleDialogHash == hash) return@post

        // Check activity state before showing dialog
        if (lifecycle.currentState >= Lifecycle.State.CREATED) {
            // If message is aplain string, format with HTML
            val formattedMessage = (message as? String)?.toHtml() ?: message

            if (visibleDialog?.isShowing == true && dialogHasHighPriority) {
                // A high priority dialog is visible, we can't overrule this
                return@post
            }

            Napier.i(tag = tag, message = "Showing dialog: [message=$message, positiveButton=$positiveButton, neutralButton=$neutralButton")
            visibleDialog?.dismiss()
            visibleDialogHash = hash
            dialogHasHighPriority = highPriority
            visibleDialog = MaterialAlertDialogBuilder(this).let { builder ->
                builder.setTitle(title)
                builder.setMessage(formattedMessage)
                builder.setPositiveButton(positiveButton) { _, _ -> positiveAction(this) }
                neutralButton?.let {
                    builder.setNeutralButton(it) { _, _ -> neutralAction(this) }
                }
                negativeButton?.let {
                    builder.setNegativeButton(it) { _, _ -> negativeAction(this) }
                }
                builder.setCancelable(!highPriority)
                builder.setOnDismissListener {
                    dismissAction(this)
                    visibleDialogHash = 0
                }
                builder.show().also {
                    // Allow links to be clicked
                    it.findViewById<TextView>(android.R.id.message)?.movementMethod =
                        LinkClickMovementMethod(LinkClickMovementMethod.OpenWithIntentLinkClickedListener(this))
                }
            }
        } else {
            Napier.w(tag = tag, message = "Dialog skipped, activity finished")
        }
    }

    private fun calculateDialogHash(
        message: CharSequence,
        positiveButton: CharSequence = getString(android.R.string.ok),
        neutralButton: CharSequence? = null,
        negativeButton: CharSequence? = null,
        highPriority: Boolean = false,
        title: CharSequence? = null,
    ) = "$message$positiveButton$neutralButton$negativeButton$highPriority$title".hashCode()

    abstract fun startPrintNotificationService()

    sealed class Message {
        data class SnackbarMessage(
            val duration: Int = Snackbar.LENGTH_SHORT,
            val type: Type = Type.Neutral,
            val actionText: (Context) -> CharSequence? = { null },
            val action: (Context) -> Unit = {},
            val debounce: Boolean = false,
            val text: (Context) -> CharSequence
        ) : Message() {
            sealed class Type {
                data object Neutral : Type()
                data object Positive : Type()
                data object Negative : Type()
            }
        }

        data class DialogMessage(
            val title: Context.() -> CharSequence? = { null },
            val text: Context.() -> CharSequence,
            val positiveButton: Context.() -> CharSequence = { getString(android.R.string.ok) },
            val neutralButton: Context.() -> CharSequence? = { null },
            val negativeButton: Context.() -> CharSequence? = { null },
            val positiveAction: Context.() -> Unit = {},
            val neutralAction: Context.() -> Unit = {},
            val negativeAction: Context.() -> Unit = {},
            val dismissAction: Context.() -> Unit = {},
        ) : Message()
    }
}