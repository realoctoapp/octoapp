package de.crysxd.baseui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.navigation.NavController
import de.crysxd.octoapp.base.data.models.UiEvent
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineExceptionHandler

abstract class BaseViewModel : ViewModel() {

    private val mutableErrorLiveData = MutableLiveData<UiEvent<Throwable>>()
    val errorLiveData = mutableErrorLiveData.map { it }
    protected val coroutineExceptionHandler = CoroutineExceptionHandler { _, e ->
        postException(e)
        Napier.e(tag = "BaseViewModel", throwable = e, message = "Exception in view model scope")
    }

    private val mutableMessages = MutableLiveData<UiEvent<OctoActivity.Message>>()
    val messages = mutableMessages.map { it }

    lateinit var navContoller: NavController

    protected fun postException(e: Throwable) {
        mutableErrorLiveData.postValue(UiEvent(e))
    }

    protected fun postMessage(message: OctoActivity.Message) {
        mutableMessages.postValue(UiEvent(message))
    }
}