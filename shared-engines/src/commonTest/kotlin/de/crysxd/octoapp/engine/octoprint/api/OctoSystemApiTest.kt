package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemCommandList
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Instant
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoSystemApiTest {

    @Test
    fun WHEN_system_info_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/system/info"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                       {
                            "systeminfo": {
                                "browser.user_agent": "PostmanRuntime/7.29.2",
                                "connectivity.connection_check": "1.1.1.1:53",
                                "connectivity.connection_ok": true,
                                "connectivity.enabled": true,
                                "connectivity.online": true,
                                "connectivity.resolution_check": "octoprint.org",
                                "connectivity.resolution_ok": true,
                                "env.hardware.cores": 4,
                                "env.hardware.freq": 0.0,
                                "env.hardware.ram": 12562259968,
                                "env.os.bits": 64,
                                "env.os.id": "linux",
                                "env.os.platform": "linux",
                                "env.python.pip": "21.2.4",
                                "env.python.version": "3.8.12",
                                "env.python.virtualenv": false,
                                "octoprint.last_safe_mode.date": "2022-09-22T05:15:00Z",
                                "octoprint.last_safe_mode.reason": "settings",
                                "octoprint.safe_mode": false,
                                "octoprint.version": "1.8.3",
                                "printer.firmware": "Virtual Marlin 1.0",
                                "systeminfo.generated": "2022-09-26T09:22:28Z",
                                "systeminfo.generator": "systemapi"
                            }
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoSystemApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSystemInfo()
        //endregion
        //region THEN
        assertEquals(
            expected = SystemInfo(
                generatedAt = Instant.fromEpochMilliseconds(1664184148000),
                safeMode = false,
                printerFirmware = "Virtual Marlin 1.0",
                printerFirmwareFamily = SystemInfo.FirmwareFamily.Generic,
                capabilities = listOf(
                    SystemInfo.Capability.PrettyTerminal,
                    SystemInfo.Capability.Timelapse,
                    SystemInfo.Capability.Plugins,
                    SystemInfo.Capability.AutoNgrok,
                    SystemInfo.Capability.TemperatureOffset,
                )
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_system_commands_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/system/commands"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                      {
                        "core": [
                                {
                                    "action": "restart",
                                    "confirm": "<strong>You are about to restart",
                                    "name": "Restart OctoPrint",
                                    "resource": "http://doggy-daycare:5001/api/system/commands/core/restart",
                                    "source": "core"
                                },
                                {
                                    "action": "restart_safe",
                                    "confirm": "<strong>You are about to ",
                                    "name": "Restart OctoPrint in safe mode",
                                    "resource": "http://doggy-daycare:5001/api/system/commands/core/restart_safe",
                                    "source": "core"
                                }
                            ],
                            "custom": [],
                            "plugin": []
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoSystemApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSystemCommands()
        //endregion
        //region THEN
        assertEquals(
            expected = SystemCommandList(
                core = listOf(
                    SystemCommand(
                        name = "Restart OctoPrint",
                        confirmation = "<strong>You are about to restart",
                        action = "restart",
                        source = "core",
                        type = SystemCommand.Type.Restart,
                    ),
                    SystemCommand(
                        name = "Restart OctoPrint in safe mode",
                        confirmation = "<strong>You are about to ",
                        action = "restart_safe",
                        source = "core",
                        type = SystemCommand.Type.RestartSafeMode,
                    )
                )
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_system_command_is_executed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = OctoPrintEngineBuilder.HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/system/commands/core/restart_safe"),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = "",
                )
            }
        )
        val target = OctoSystemApi(rotator, http)
        //endregion
        //region WHEN
        target.executeSystemCommand(
            SystemCommand(
                name = "Restart OctoPrint",
                confirmation = "<strong>You are about to restart",
                action = "restart_safe",
                source = "core",
            ),
        )
        //endregion
        //region THEN
        //endregion
    }
}