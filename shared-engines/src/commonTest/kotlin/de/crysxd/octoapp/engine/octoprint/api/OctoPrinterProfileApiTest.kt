package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.URLBuilder
import io.ktor.http.appendEncodedPathSegments
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals


internal class OctoPrinterProfileApiTest {

    @Test
    fun WHEN_the_profiles_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments("api", "printerprofiles").build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                       {"profiles":{"_default":{"axes":{"e":{"inverted":false,"speed":300},"x":{"inverted":false,"speed":6000},"y":{"inverted":false,"speed":6000},"z":{"inverted":false,"speed":200}},"color":"default","current":true,"default":true,"extruder":{"count":1,"defaultExtrusionLength":5,"nozzleDiameter":0.4,"offsets":[[0.0,0.0]],"sharedNozzle":false},"heatedBed":true,"heatedChamber":false,"id":"_default","model":"Generic RepRap Printer","name":"Default","resource":"http://localhost:8080/api/printerprofiles/_default","volume":{"custom_box":{"x_max":310.0,"x_min":-10.0,"y_max":300.0,"y_min":0.0,"z_max":400.0,"z_min":1.0},"depth":100.0,"formFactor":"rectangular","height":100.0,"origin":"lowerleft","width":100.0}}}}
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoPrinterProfileApi(rotator, http)
        //endregion
        //region WHEN
        val user = target.getPrinterProfiles()
        //endregion
        //region THEN
        assertEquals(
            expected = mutableMapOf(
                "_default" to PrinterProfile(
                    axes = PrinterProfile.Axes(
                        e = PrinterProfile.Axis(inverted = false, speed = 300f),
                        x = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        y = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        z = PrinterProfile.Axis(inverted = false, speed = 200f),
                    ),
                    color = "default",
                    current = true,
                    default = true,
                    extruders = listOf(
                        PrinterProfile.Extruder(
                            nozzleDiameter = 0.4f,
                            componentName = "tool0"
                        )
                    ),
                    heatedBed = true,
                    heatedChamber = false,
                    id = "_default",
                    model = "Generic RepRap Printer",
                    name = "Default",
                    volume = PrinterProfile.Volume(
                        width = 100f,
                        depth = 100f,
                        height = 100f,
                        origin = PrinterProfile.Origin.LowerLeft,
                        formFactor = PrinterProfile.FormFactor.Rectangular,
                        boundingBox = PrinterProfile.CustomBox(
                            xMin = -10f,
                            xMax = 310f,
                            yMin = 0f,
                            yMax = 300f,
                            zMin = 1f,
                            zMax = 400f
                        )
                    )
                )
            ),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_the_profiles_are_loaded_without_bounding_box_and_lower_left_origin_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments("api", "printerprofiles").build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                       {"profiles":{"_default":{"axes":{"e":{"inverted":false,"speed":300},"x":{"inverted":false,"speed":6000},"y":{"inverted":false,"speed":6000},"z":{"inverted":false,"speed":200}},"color":"default","current":true,"default":true,"extruder":{"count":1,"defaultExtrusionLength":5,"nozzleDiameter":0.4,"offsets":[[0.0,0.0]],"sharedNozzle":false},"heatedBed":true,"heatedChamber":false,"id":"_default","model":"Generic RepRap Printer","name":"Default","resource":"http://localhost:8080/api/printerprofiles/_default","volume":{"depth":200.0,"formFactor":"rectangular","height":300.0,"origin":"lowerleft","width":100.0}}}}
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoPrinterProfileApi(rotator, http)
        //endregion
        //region WHEN
        val user = target.getPrinterProfiles()
        //endregion
        //region THEN
        assertEquals(
            expected = mutableMapOf(
                "_default" to PrinterProfile(
                    axes = PrinterProfile.Axes(
                        e = PrinterProfile.Axis(inverted = false, speed = 300f),
                        x = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        y = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        z = PrinterProfile.Axis(inverted = false, speed = 200f),
                    ),
                    color = "default",
                    current = true,
                    default = true,
                    extruders = listOf(
                        PrinterProfile.Extruder(
                            nozzleDiameter = 0.4f,
                            componentName = "tool0"
                        )
                    ),
                    heatedBed = true,
                    heatedChamber = false,
                    id = "_default",
                    model = "Generic RepRap Printer",
                    name = "Default",
                    volume = PrinterProfile.Volume(
                        width = 100f,
                        depth = 200f,
                        height = 300f,
                        origin = PrinterProfile.Origin.LowerLeft,
                        formFactor = PrinterProfile.FormFactor.Rectangular,
                        boundingBox = PrinterProfile.CustomBox(
                            xMin = 0f,
                            xMax = 100f,
                            yMin = 0f,
                            yMax = 200f,
                            zMin = 0f,
                            zMax = 300f
                        )
                    )
                )
            ),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_the_profiles_are_loaded_without_bounding_box_and_center_origin_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments("api", "printerprofiles").build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                       {"profiles":{"_default":{"axes":{"e":{"inverted":false,"speed":300},"x":{"inverted":false,"speed":6000},"y":{"inverted":false,"speed":6000},"z":{"inverted":false,"speed":200}},"color":"default","current":true,"default":true,"extruder":{"count":1,"defaultExtrusionLength":5,"nozzleDiameter":0.4,"offsets":[[0.0,0.0]],"sharedNozzle":false},"heatedBed":true,"heatedChamber":false,"id":"_default","model":"Generic RepRap Printer","name":"Default","resource":"http://localhost:8080/api/printerprofiles/_default","volume":{"depth":200.0,"formFactor":"rectangular","height":300.0,"origin":"center","width":100.0}}}}
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoPrinterProfileApi(rotator, http)
        //endregion
        //region WHEN
        val user = target.getPrinterProfiles()
        //endregion
        //region THEN
        assertEquals(
            expected = mutableMapOf(
                "_default" to PrinterProfile(
                    axes = PrinterProfile.Axes(
                        e = PrinterProfile.Axis(inverted = false, speed = 300f),
                        x = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        y = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        z = PrinterProfile.Axis(inverted = false, speed = 200f),
                    ),
                    color = "default",
                    current = true,
                    default = true,
                    extruders = listOf(
                        PrinterProfile.Extruder(
                            nozzleDiameter = 0.4f,
                            componentName = "tool0",
                        )
                    ),
                    heatedBed = true,
                    heatedChamber = false,
                    id = "_default",
                    model = "Generic RepRap Printer",
                    name = "Default",
                    volume = PrinterProfile.Volume(
                        width = 100f,
                        depth = 200f,
                        height = 300f,
                        origin = PrinterProfile.Origin.Center,
                        formFactor = PrinterProfile.FormFactor.Rectangular,
                        boundingBox = PrinterProfile.CustomBox(
                            xMin = -50f,
                            xMax = 50f,
                            yMin = -100f,
                            yMax = 100f,
                            zMin = 0f,
                            zMax = 300f
                        )
                    )
                )
            ),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_the_profiles_are_loaded_with_false_bounding_box_and_center_origin_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertEquals(
                    expected = URLBuilder(rotator.activeUrl.value).appendEncodedPathSegments("api", "printerprofiles").build(),
                    actual = request.url,
                    message = "Expected URL to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                       {"profiles":{"_default":{"axes":{"e":{"inverted":false,"speed":300},"x":{"inverted":false,"speed":6000},"y":{"inverted":false,"speed":6000},"z":{"inverted":false,"speed":200}},"color":"default","current":true,"default":true,"extruder":{"count":1,"defaultExtrusionLength":5,"nozzleDiameter":0.4,"offsets":[[0.0,0.0]],"sharedNozzle":false},"heatedBed":true,"heatedChamber":false,"id":"_default","model":"Generic RepRap Printer","name":"Default","resource":"http://localhost:8080/api/printerprofiles/_default","volume":{"depth":200.0,"formFactor":"rectangular","height":300.0,"custom_box":false,"origin":"center","width":100.0}}}}
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoPrinterProfileApi(rotator, http)
        //endregion
        //region WHEN
        val user = target.getPrinterProfiles()
        //endregion
        //region THEN
        assertEquals(
            expected = mutableMapOf(
                "_default" to PrinterProfile(
                    axes = PrinterProfile.Axes(
                        e = PrinterProfile.Axis(inverted = false, speed = 300f),
                        x = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        y = PrinterProfile.Axis(inverted = false, speed = 6000f),
                        z = PrinterProfile.Axis(inverted = false, speed = 200f),
                    ),
                    color = "default",
                    current = true,
                    default = true,
                    extruders = listOf(
                        PrinterProfile.Extruder(
                            nozzleDiameter = 0.4f,
                            componentName = "tool0",
                        )
                    ),
                    heatedBed = true,
                    heatedChamber = false,
                    id = "_default",
                    model = "Generic RepRap Printer",
                    name = "Default",
                    volume = PrinterProfile.Volume(
                        width = 100f,
                        depth = 200f,
                        height = 300f,
                        origin = PrinterProfile.Origin.Center,
                        formFactor = PrinterProfile.FormFactor.Rectangular,
                        boundingBox = PrinterProfile.CustomBox(
                            xMin = -50f,
                            xMax = 50f,
                            yMin = -100f,
                            yMax = 100f,
                            zMin = 0f,
                            zMax = 300f
                        )
                    )
                )
            ),
            actual = user,
            message = "Expected response to match"
        )
        //endregion
    }
}
