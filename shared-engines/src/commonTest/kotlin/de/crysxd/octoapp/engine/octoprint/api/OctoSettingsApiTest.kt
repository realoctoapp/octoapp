package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertHead
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoSettingsApiTest {

    @Test
    fun WHEN_settings_are_loaded_on_OctoPrint_1_8_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val etag = "etag1234"
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/settings"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf(
                        "Content-Type" to listOf("application/json"),
                        "Etag" to listOf(etag)
                    ),
                    //region content = ...
                    content = """
                      {
                            "api": {
                                "allowCrossOrigin": false,
                                "key": "522217826CF841A78537F56765092AA8"
                            },
                            "appearance": {
                                "closeModalsWithClick": true,
                                "color": "orange",
                                "colorIcon": true,
                                "colorTransparent": false,
                                "defaultLanguage": "_default",
                                "fuzzyTimes": true,
                                "name": "Beagle",
                                "showFahrenheitAlso": false,
                                "showInternalFilename": true
                            },
                            "devel": {
                                "pluginTimings": false
                            },
                            "feature": {
                                "autoUppercaseBlacklist": [
                                    "M117",
                                    "M118"
                                ],
                                "g90InfluencesExtruder": false,
                                "keyboardControl": true,
                                "modelSizeDetection": true,
                                "pollWatched": false,
                                "printCancelConfirmation": true,
                                "printStartConfirmation": false,
                                "rememberFileFolder": false,
                                "sdSupport": true,
                                "temperatureGraph": true,
                                "uploadOverwriteConfirmation": true
                            },
                            "folder": {
                                "timelapse": "/octoprint/octoprint/timelapse",
                                "uploads": "/octoprint/octoprint/uploads",
                                "watched": "/octoprint/octoprint/watched"
                            },
                            "gcodeAnalysis": {
                                "bedZ": 0.0,
                                "runAt": "idle"
                            },
                            "plugins": {
                                "action_command_notification": {
                                    "enable": true,
                                    "enable_popups": false
                                },
                                "action_command_prompt": {
                                    "command": "M876",
                                    "enable": "detected",
                                    "enable_emergency_sending": true,
                                    "enable_signal_support": true
                                },
                                "announcements": {
                                    "channel_order": [
                                        "_important",
                                        "_releases",
                                        "_blog",
                                        "_plugins",
                                        "_octopi"
                                    ],
                                    "channels": {
                                        "_blog": {
                                            "description": "Development news, community spotlights, OctoPrint On Air episodes and more from the official OctoBlog.",
                                            "name": "On the OctoBlog",
                                            "priority": 2,
                                            "read_until": 1661175900,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/octoblog.xml"
                                        },
                                        "_important": {
                                            "description": "Important announcements about OctoPrint.",
                                            "name": "Important Announcements",
                                            "priority": 1,
                                            "read_until": 1521111600,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/important.xml"
                                        },
                                        "_octopi": {
                                            "description": "News around OctoPi, the Raspberry Pi image including OctoPrint.",
                                            "name": "OctoPi News",
                                            "priority": 2,
                                            "read_until": 1659616200,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/octopi.xml"
                                        },
                                        "_plugins": {
                                            "description": "Announcements of new plugins released on the official Plugin Repository.",
                                            "name": "New Plugins in the Repository",
                                            "priority": 2,
                                            "read_until": 1662958800,
                                            "type": "rss",
                                            "url": "https://plugins.octoprint.org/feed.xml"
                                        },
                                        "_releases": {
                                            "description": "Announcements of new releases and release candidates of OctoPrint.",
                                            "name": "Release Announcements",
                                            "priority": 2,
                                            "read_until": 1663673400,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/releases.xml"
                                        }
                                    },
                                    "display_limit": 3,
                                    "enabled_channels": [
                                        "_important",
                                        "_releases",
                                        "_blog",
                                        "_plugins",
                                        "_octopi"
                                    ],
                                    "forced_channels": [
                                        "_important"
                                    ],
                                    "summary_limit": 300,
                                    "ttl": 360
                                },
                                "backup": {
                                    "restore_unsupported": false
                                },
                                "cancelobject": {
                                    "aftergcode": null,
                                    "allowed": "",
                                    "beforegcode": null,
                                    "ignored": "ENDGCODE,STARTGCODE",
                                    "markers": true,
                                    "object_regex": [
                                        {
                                            "objreg": "; process (.*)"
                                        },
                                        {
                                            "objreg": ";MESH:(.*)"
                                        },
                                        {
                                            "objreg": "; printing object (.*)"
                                        },
                                        {
                                            "objreg": ";PRINTING: (.*)"
                                        }
                                    ],
                                    "reptag": "Object",
                                    "shownav": true,
                                    "stoptags": false
                                },
                                "discovery": {
                                    "addresses": null,
                                    "httpPassword": null,
                                    "httpUsername": null,
                                    "ignoredAddresses": null,
                                    "ignoredInterfaces": null,
                                    "interfaces": null,
                                    "model": {
                                        "description": null,
                                        "name": null,
                                        "number": null,
                                        "serial": null,
                                        "url": null,
                                        "vendor": null,
                                        "vendorUrl": null
                                    },
                                    "pathPrefix": null,
                                    "publicHost": null,
                                    "publicPort": null,
                                    "upnpUuid": "bda5fb9e-3a29-4e2b-af2f-fbfe4dc97e99",
                                    "zeroConf": []
                                },
                                "errortracking": {
                                    "enabled": true,
                                    "enabled_unreleased": false,
                                    "unique_id": "4a1caac0-a9df-4b26-a72f-ef4ee91d24bc",
                                    "url_coreui": "https://e1d6b5d760f241408382d62a3e7fb416@o118517.ingest.sentry.io/1374096",
                                    "url_server": "https://af30d06358144fb8af076ffd8984136d@o118517.ingest.sentry.io/1373987"
                                },
                                "eventmanager": {
                                    "availableEvents": [
                                        "Startup",
                                        "Shutdown",
                                        "ConnectivityChanged",
                                        "Connecting",
                                        "Connected",
                                        "Disconnecting",
                                        "Disconnected",
                                        "PrinterStateChanged",
                                        "PrinterReset",
                                        "ClientOpened",
                                        "ClientClosed",
                                        "ClientAuthed",
                                        "ClientDeauthed",
                                        "UserLoggedIn",
                                        "UserLoggedOut",
                                        "Upload",
                                        "FileSelected",
                                        "FileDeselected",
                                        "UpdatedFiles",
                                        "MetadataAnalysisStarted",
                                        "MetadataAnalysisFinished",
                                        "MetadataStatisticsUpdated",
                                        "FileAdded",
                                        "FileRemoved",
                                        "FileMoved",
                                        "FolderAdded",
                                        "FolderRemoved",
                                        "FolderMoved",
                                        "TransferStarted",
                                        "TransferDone",
                                        "TransferFailed",
                                        "PrintStarted",
                                        "PrintDone",
                                        "PrintFailed",
                                        "PrintCancelling",
                                        "PrintCancelled",
                                        "PrintPaused",
                                        "PrintResumed",
                                        "Error",
                                        "PowerOn",
                                        "PowerOff",
                                        "Home",
                                        "ZChange",
                                        "Waiting",
                                        "Dwelling",
                                        "Cooling",
                                        "Alert",
                                        "Conveyor",
                                        "Eject",
                                        "EStop",
                                        "PositionUpdate",
                                        "FirmwareData",
                                        "ToolChange",
                                        "RegisteredMessageReceived",
                                        "CommandSuppressed",
                                        "InvalidToolReported",
                                        "FilamentChange",
                                        "CaptureStart",
                                        "CaptureDone",
                                        "CaptureFailed",
                                        "PostRollStart",
                                        "PostRollEnd",
                                        "MovieRendering",
                                        "MovieDone",
                                        "MovieFailed",
                                        "SlicingStarted",
                                        "SlicingDone",
                                        "SlicingFailed",
                                        "SlicingCancelled",
                                        "SlicingProfileAdded",
                                        "SlicingProfileModified",
                                        "SlicingProfileDeleted",
                                        "PrinterProfileAdded",
                                        "PrinterProfileModified",
                                        "PrinterProfileDeleted",
                                        "SettingsUpdated",
                                        "plugin_backup_backup_created",
                                        "plugin_firmware_check_warning",
                                        "plugin_ngrok_connected",
                                        "plugin_ngrok_closed",
                                        "plugin_pluginmanager_install_plugin",
                                        "plugin_pluginmanager_uninstall_plugin",
                                        "plugin_pluginmanager_enable_plugin",
                                        "plugin_pluginmanager_disable_plugin",
                                        "plugin_psucontrol_psu_state_changed",
                                        "plugin_softwareupdate_update_succeeded",
                                        "plugin_softwareupdate_update_failed"
                                    ],
                                    "subscriptions": []
                                },
                                "firmware_check": {
                                    "ignore_infos": false
                                },
                                "gcodeviewer": {
                                    "mobileSizeThreshold": 2097152,
                                    "sizeThreshold": 20971520,
                                    "skipUntilThis": null
                                },
                                "mmu2filamentselect": {
                                    "filament1": "Test 1",
                                    "filament2": "Test 2",
                                    "filament3": "",
                                    "filament4": "",
                                    "filament5": "",
                                    "labelSource": "manual",
                                    "timeout": 30,
                                    "timeoutAction": "printerDialog"
                                },
                                "multicam": {
                                    "multicam_profiles": [
                                        {
                                            "URL": "http://192.168.1.242/webcam/?action=stream",
                                            "flipH": false,
                                            "flipV": false,
                                            "isButtonEnabled": false,
                                            "name": "Default",
                                            "rotate90": false,
                                            "snapshot": "http:/192.168.1.242/webcam/?action=snapshot",
                                            "streamRatio": "16:9"
                                        },
                                        {
                                            "URL": "http://octopi.local/webcam/?action=stream",
                                            "flipH": false,
                                            "flipV": false,
                                            "isButtonEnabled": true,
                                            "name": "Webcam 1",
                                            "rotate90": false,
                                            "snapshot": "http://octopi.local/webcam/?action=stream",
                                            "streamRatio": "16:9"
                                        }
                                    ]
                                },
                                "ngrok": {
                                    "auth_name": "name",
                                    "auth_pass": "pass",
                                    "auto_connect": true,
                                    "disable_local_ip_check": false,
                                    "hostname": "",
                                    "port": 5000,
                                    "region": "ap",
                                    "show_qr_code": false,
                                    "subdomain": "",
                                    "token": "1x5BkMgxgzwjwfLQ7qQweO0cg7U_5GK47r7JPgLcEXNbAPmap",
                                    "trust_basic_authentication": false
                                },
                                "octoapp": {
                                    "encryptionKey": "4c7cf1e0-9720-4a18-ad66-4acaf3790917",
                                    "version": "1.0.12"
                                },
                                "octoeverywhere": {
                                    "AddPrinterUrl": "https://octoeverywhere.com/getstarted?isFromOctoPrint=true&printerid=347R9DNGCJZ5IS3D9VYK7JLL1J5REM7ZRKZZN9K2BANG455618ZZZY4QBYC0",
                                    "ConnectedAccounts": "",
                                    "HasConnectedAccounts": false,
                                    "HttpFrontendIsHttps": false,
                                    "HttpFrontendPort": 5001,
                                    "NoAccountConnectedLastInformDateTime": "Mon, 26 Sep 2022 08:40:02 GMT",
                                    "Pid": "pid",
                                    "PluginUpdateRequired": false,
                                    "PluginVersion": "1.10.5",
                                    "PrinterKey": "key"
                                },
                                "pluginmanager": {
                                    "confirm_disable": false,
                                    "dependency_links": false,
                                    "hidden": [],
                                    "ignore_throttled": false,
                                    "notices": "https://plugins.octoprint.org/notices.json",
                                    "notices_ttl": 360,
                                    "pip_args": null,
                                    "pip_force_user": false,
                                    "repository": "https://plugins.octoprint.org/plugins.json",
                                    "repository_ttl": 1440
                                },
                                "psucontrol": {
                                    "GPIODevice": "",
                                    "autoOn": false,
                                    "autoOnTriggerGCodeCommands": "G0,G1,G2,G3,G10,G11,G28,G29,G32,M104,M106,M109,M140,M190",
                                    "connectOnPowerOn": false,
                                    "disconnectOnPowerOff": false,
                                    "enablePowerOffWarningDialog": true,
                                    "enablePseudoOnOff": false,
                                    "idleIgnoreCommands": "M105",
                                    "idleTimeout": 30,
                                    "idleTimeoutWaitTemp": 50,
                                    "invertonoffGPIOPin": false,
                                    "invertsenseGPIOPin": false,
                                    "offGCodeCommand": "M81",
                                    "offSysCommand": "",
                                    "onGCodeCommand": "M80",
                                    "onSysCommand": "",
                                    "onoffGPIOPin": 0,
                                    "postOnDelay": 0.0,
                                    "powerOffWhenIdle": false,
                                    "pseudoOffGCodeCommand": "M81",
                                    "pseudoOnGCodeCommand": "M80",
                                    "senseGPIOPin": 0,
                                    "senseGPIOPinPUD": "",
                                    "sensePollingInterval": 5,
                                    "senseSystemCommand": "",
                                    "sensingMethod": "INTERNAL",
                                    "sensingPlugin": "",
                                    "switchingMethod": "GCODE",
                                    "switchingPlugin": "",
                                    "turnOffWhenError": false,
                                    "turnOnWhenApiUploadPrint": false
                                },
                                "softwareupdate": {
                                    "cache_ttl": 1440,
                                    "check_overlay_py2_url": "https://plugins.octoprint.org/update_check_overlay_py2.json",
                                    "check_overlay_ttl": 360,
                                    "check_overlay_url": "https://plugins.octoprint.org/update_check_overlay.json",
                                    "credentials": {},
                                    "ignore_throttled": false,
                                    "minimum_free_storage": 150,
                                    "notify_users": true,
                                    "octoprint_branch_mappings": [
                                        {
                                            "branch": "master",
                                            "commitish": [
                                                "master"
                                            ],
                                            "name": "Stable"
                                        },
                                        {
                                            "branch": "rc/maintenance",
                                            "commitish": [
                                                "rc/maintenance"
                                            ],
                                            "name": "Maintenance RCs"
                                        },
                                        {
                                            "branch": "rc/devel",
                                            "commitish": [
                                                "rc/maintenance",
                                                "rc/devel"
                                            ],
                                            "name": "Devel RCs"
                                        }
                                    ],
                                    "octoprint_checkout_folder": null,
                                    "octoprint_method": "pip",
                                    "octoprint_pip_target": "https://github.com/OctoPrint/OctoPrint/archive/{target_version}.zip",
                                    "octoprint_release_channel": "master",
                                    "octoprint_tracked_branch": "staging/bugfix",
                                    "octoprint_type": "github_release",
                                    "pip_command": null,
                                    "pip_enable_check": false,
                                    "queued_updates": [],
                                    "updatelog_cutoff": 43200
                                },
                                "tracking": {
                                    "enabled": false,
                                    "events": {
                                        "commerror": true,
                                        "plugin": true,
                                        "pong": true,
                                        "printer": true,
                                        "printer_safety_check": true,
                                        "printjob": true,
                                        "slicing": true,
                                        "startup": true,
                                        "throttled": true,
                                        "update": true,
                                        "webui_load": true
                                    },
                                    "ping": null,
                                    "pong": 86400,
                                    "server": null,
                                    "unique_id": "1499cbbc-a778-42ab-b32c-c557f1ed59bd"
                                },
                                "virtual_printer": {
                                    "ambientTemperature": 21.3,
                                    "brokenM29": true,
                                    "brokenResend": false,
                                    "busyInterval": 2.0,
                                    "capabilities": {
                                        "AUTOREPORT_POS": false,
                                        "AUTOREPORT_SD_STATUS": true,
                                        "AUTOREPORT_TEMP": true,
                                        "EMERGENCY_PARSER": true,
                                        "EXTENDED_M20": false
                                    },
                                    "commandBuffer": 4,
                                    "echoOnM117": true,
                                    "enable_eeprom": true,
                                    "enabled": true,
                                    "errors": {
                                        "checksum_mismatch": "Checksum mismatch",
                                        "checksum_missing": "Missing checksum",
                                        "command_unknown": "Unknown command {}",
                                        "lineno_mismatch": "expected line {} got {}",
                                        "lineno_missing": "No Line Number with checksum, Last Line: {}",
                                        "maxtemp": "MAXTEMP triggered!",
                                        "mintemp": "MINTEMP triggered!"
                                    },
                                    "firmwareName": "Virtual Marlin 1.0",
                                    "forceChecksum": false,
                                    "hasBed": true,
                                    "hasChamber": false,
                                    "includeCurrentToolInTemps": true,
                                    "includeFilenameInOpened": true,
                                    "klipperTemperatureReporting": false,
                                    "locked": false,
                                    "m105NoTargetFormatString": "{heater}:{actual:.2f}",
                                    "m105TargetFormatString": "{heater}:{actual:.2f}/ {target:.2f}",
                                    "m114FormatString": "X:{x} Y:{y} Z:{z} E:{e[current]} Count: A:{a} B:{b} C:{c}",
                                    "m115FormatString": "FIRMWARE_NAME:{firmware_name} PROTOCOL_VERSION:1.0",
                                    "m115ReportCapabilities": true,
                                    "numExtruders": 1,
                                    "okAfterResend": false,
                                    "okBeforeCommandOutput": false,
                                    "okFormatString": "ok",
                                    "passcode": "1234",
                                    "pinnedExtruders": null,
                                    "preparedOks": [],
                                    "repetierStyleTargetTemperature": false,
                                    "reprapfwM114": false,
                                    "resend_ratio": 0,
                                    "resetLines": [
                                        "start",
                                        "Marlin: Virtual Marlin!",
                                        "",
                                        "SD card ok"
                                    ],
                                    "rxBuffer": 64,
                                    "sdFiles": {
                                        "longname": false,
                                        "longname_quoted": true,
                                        "size": true
                                    },
                                    "sendBusy": false,
                                    "sendWait": true,
                                    "sharedNozzle": false,
                                    "simulateReset": true,
                                    "smoothieTemperatureReporting": false,
                                    "supportF": false,
                                    "supportM112": true,
                                    "support_M503": true,
                                    "throttle": 0.01,
                                    "waitInterval": 1.0
                                }
                            },
                            "scripts": {
                                "gcode": {
                                    "afterPrintCancelled": "; disable motors\nM84\n\n;disable all heaters\n{% snippet 'disable_hotends' %}\n{% snippet 'disable_bed' %}\n;disable fan\nM106 S0",
                                    "psucontrol_post_on": "",
                                    "psucontrol_pre_off": "",
                                    "snippets/disable_bed": "{% if printer_profile.heatedBed %}M140 S0\n{% endif %}",
                                    "snippets/disable_hotends": "{% if printer_profile.extruder.sharedNozzle %}M104 T0 S0\n{% else %}{% for tool in range(printer_profile.extruder.count) %}M104 T{{ tool }} S0\n{% endfor %}{% endif %}"
                                }
                            },
                            "serial": {
                                "abortHeatupOnCancel": true,
                                "ackMax": 1,
                                "additionalBaudrates": [],
                                "additionalPorts": [],
                                "alwaysSendChecksum": false,
                                "autoconnect": false,
                                "baudrate": null,
                                "baudrateOptions": [
                                    250000,
                                    230400,
                                    115200,
                                    57600,
                                    38400,
                                    19200,
                                    9600
                                ],
                                "blacklistedBaudrates": [],
                                "blacklistedPorts": [],
                                "blockWhileDwelling": false,
                                "blockedCommands": [
                                    "M0",
                                    "M1"
                                ],
                                "capAutoreportPos": true,
                                "capAutoreportSdStatus": true,
                                "capAutoreportTemp": true,
                                "capBusyProtocol": true,
                                "capEmergencyParser": true,
                                "capExtendedM20": true,
                                "checksumRequiringCommands": [
                                    "M110"
                                ],
                                "disableSdPrintingDetection": false,
                                "disconnectOnErrors": true,
                                "emergencyCommands": [
                                    "M112",
                                    "M108",
                                    "M410"
                                ],
                                "enableShutdownActionCommand": false,
                                "encoding": "ascii",
                                "exclusive": true,
                                "externalHeatupDetection": true,
                                "firmwareDetection": true,
                                "helloCommand": "M110 N0",
                                "ignoreEmptyPorts": false,
                                "ignoreErrorsFromFirmware": false,
                                "ignoreIdenticalResends": false,
                                "ignoredCommands": [],
                                "log": false,
                                "logPositionOnCancel": false,
                                "logPositionOnPause": true,
                                "longRunningCommands": [
                                    "G4",
                                    "G28",
                                    "G29",
                                    "G30",
                                    "G32",
                                    "M400",
                                    "M226",
                                    "M600"
                                ],
                                "lowLatency": false,
                                "maxTimeoutsIdle": 2,
                                "maxTimeoutsLong": 5,
                                "maxTimeoutsPrinting": 5,
                                "neverSendChecksum": false,
                                "notifySuppressedCommands": "warn",
                                "pausingCommands": [
                                    "M0",
                                    "M1",
                                    "M25"
                                ],
                                "port": null,
                                "portOptions": [
                                    "VIRTUAL"
                                ],
                                "repetierTargetTemp": false,
                                "resendRatioStart": 100,
                                "resendRatioThreshold": 10,
                                "sanityCheckTools": true,
                                "sdAlwaysAvailable": false,
                                "sdCancelCommand": "M25",
                                "sdLowerCase": false,
                                "sdRelativePath": false,
                                "sendChecksumWithUnknownCommands": false,
                                "sendM112OnError": true,
                                "supportResendsWithoutOk": "detect",
                                "swallowOkAfterResend": true,
                                "timeoutBaudrateDetectionPause": 1.0,
                                "timeoutCommunication": 30.0,
                                "timeoutCommunicationBusy": 3.0,
                                "timeoutConnection": 10.0,
                                "timeoutDetectionConsecutive": 2.0,
                                "timeoutDetectionFirst": 10.0,
                                "timeoutPosAutoreport": 5.0,
                                "timeoutPositionLogWait": 10.0,
                                "timeoutSdStatus": 1.0,
                                "timeoutSdStatusAutoreport": 1.0,
                                "timeoutTemperature": 5.0,
                                "timeoutTemperatureAutoreport": 2.0,
                                "timeoutTemperatureTargetSet": 2.0,
                                "triggerOkForM29": true,
                                "unknownCommandsNeedAck": false,
                                "useParityWorkaround": "detect",
                                "waitForStart": false
                            },
                            "server": {
                                "allowFraming": false,
                                "commands": {
                                    "serverRestartCommand": "s6-svc -r /var/run/s6/services/octoprint",
                                    "systemRestartCommand": null,
                                    "systemShutdownCommand": null
                                },
                                "diskspace": {
                                    "critical": 209715200,
                                    "warning": 524288000
                                },
                                "onlineCheck": {
                                    "enabled": true,
                                    "host": "1.1.1.1",
                                    "interval": 15,
                                    "name": "octoprint.org",
                                    "port": 53
                                },
                                "pluginBlacklist": {
                                    "enabled": true,
                                    "ttl": 15,
                                    "url": "https://plugins.octoprint.org/blacklist.json"
                                }
                            },
                            "slicing": {
                                "defaultSlicer": null
                            },
                            "system": {
                                "actions": [],
                                "events": null
                            },
                            "temperature": {
                                "cutoff": 30,
                                "profiles": [
                                    {
                                        "bed": "nul",
                                        "chamber": null,
                                        "extruder": 210,
                                        "name": "ABS"
                                    },
                                    {
                                        "bed": 60,
                                        "chamber": null,
                                        "extruder": 180,
                                        "name": "PLA"
                                    }
                                ],
                                "sendAutomatically": false,
                                "sendAutomaticallyAfter": 1
                            },
                            "terminalFilters": [
                                {
                                    "name": "Suppress temperature messages",
                                    "regex": "regex"
                                },
                                {
                                    "name": "Suppress SD status messages",
                                    "regex": "regex"                                
                                },
                                {
                                    "name": "Suppress position messages",
                                    "regex": "regex" 
                                },
                                {
                                    "name": "Suppress wait responses",
                                    "regex": "regex"                               
                                },
                                {
                                    "name": "Suppress processing responses",
                                    "regex": "regex"
                                 }
                            ],
                            "webcam": {
                                "bitrate": "10000k",
                                "cacheBuster": false,
                                "ffmpegCommandline": "{ffmpeg} -framerate {fps} -i \"{input}\" -vcodec {videocodec} -threads {threads} -b:v {bitrate} -f {containerformat} -y {filters} \"{output}\"",
                                "ffmpegPath": "/usr/bin/ffmpeg",
                                "ffmpegThreads": 1,
                                "ffmpegVideoCodec": "libx264",
                                "flipH": false,
                                "flipV": false,
                                "rotate90": false,
                                "snapshotSslValidation": true,
                                "snapshotTimeout": 5,
                                "snapshotUrl": "http://192.168.1.242/webcam/?action=snapshot",
                                "streamRatio": "16:9",
                                "streamTimeout": 5,
                                "streamUrl": "http://192.168.1.242/webcam/?action=stream",
                                "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                ],
                                "timelapseEnabled": true,
                                "watermark": true,
                                "webcamEnabled": true
                            }
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoSettingsApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSettings()
        //endregion
        //region THEN
        assertEquals(
            expected = Settings(
                hash = etag,
                webcam = WebcamSettings(
                    webcamEnabled = true,
                    webcams = listOf(
                        WebcamSettings.Webcam(
                            streamUrl = "http://192.168.1.242/webcam/?action=stream",
                            provider = "octoprint",
                            flipH = false,
                            flipV = false,
                            rotate90 = false,
                            streamRatio = "16:9",
                            snapshotUrl = "http://192.168.1.242/webcam/?action=snapshot",
                            extras = null,
                            snapshotDisplay = null,
                            canSnapshot = true,
                            displayName = "OctoPrint",
                            name = "octoprint",
                            type = WebcamSettings.Webcam.Type.Mjpeg,
                        )
                    )
                ),
                plugins = Settings.PluginSettingsGroup(
                    gcodeViewer = Settings.GcodeViewer(
                        sizeThreshold = 20971520,
                        mobileSizeThreshold = 2097152,
                    ),
                    octoEverywhere = Settings.OctoEverywhere,
                    ngrok = Settings.Ngrok(
                        authPassword = "pass",
                        authName = "name",
                    ),
                    octoAppCompanion = Settings.OctoAppCompanion(
                        encryptionKey = "4c7cf1e0-9720-4a18-ad66-4acaf3790917",
                        version = "1.0.12",
                        running = true,
                    ),
                    multiCamSettings = Settings.MultiCam(
                        webcams = listOf(
                            WebcamSettings.Webcam(
                                streamUrl = "http://octopi.local/webcam/?action=stream",
                                provider = "multicam_legacy",
                                flipH = false,
                                flipV = false,
                                rotate90 = false,
                                streamRatio = "16:9",
                                snapshotUrl = null,
                                name = "Webcam 1",
                                displayName = "Webcam 1 (MultiCam)",
                                canSnapshot = false,
                                snapshotDisplay = null,
                                extras = null,
                                type = WebcamSettings.Webcam.Type.Mjpeg,
                            ),
                        ),
                    ),
                    discovery = Settings.Discovery(
                        uuid = "bda5fb9e-3a29-4e2b-af2f-fbfe4dc97e99"
                    ),
                    cancelObject = Settings.CancelObject,
                    psuControl = Settings.PsuControl,
                    mmu = Settings.Mmu(
                        filament = listOf(
                            Settings.Mmu.Filament(name = "Test 1", toolIndex = 0, color = null),
                            Settings.Mmu.Filament(name = "Test 2", toolIndex = 1, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 2, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 3, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 4, color = null),
                        ),
                        labelSource = Settings.Mmu.LabelSource.Manual,
                        plugin = Settings.Mmu.Plugin.Mmu2FilamentSelect,
                    )
                ),
                temperaturePresets = listOf(
                    Settings.TemperaturePreset(
                        name = "ABS",
                        components = listOf(
                            // Skipped because of invalid number
                            // Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 100f, isBed = true),
                            Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 210f, isExtruder = true),
                        ),
                    ),
                    Settings.TemperaturePreset(
                        name = "PLA",
                        components = listOf(
                            Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 60f, isBed = true),
                            Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 180f, isExtruder = true),
                        ),
                    ),
                ),
                coolDownProfile = Settings.TemperaturePreset(
                    name = "__cooldown",
                    components = listOf(
                        Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 0f, isBed = true),
                        Settings.TemperaturePreset.Component(key = "chamber", displayName = "chamber", temperature = 0f, isChamber = true),
                        Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 0f, isExtruder = true),
                    ),
                ),
                terminalFilters = listOf(
                    Settings.TerminalFilter(name = "Suppress temperature messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress SD status messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress position messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress wait responses", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress processing responses", regex = "regex")
                ),
                appearance = Settings.Appearance(
                    name = "Beagle",
                    colors = Settings.Appearance.Colors(
                        themeName = "orange",
                        light = Settings.Appearance.ColorScheme(main = HexColor("#FF9800"), accent = HexColor("#FFE6C1")),
                        dark = Settings.Appearance.ColorScheme(main = HexColor("#FF9800"), accent = HexColor("#FFE6C1")),
                    )
                )
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_settings_are_loaded_with_broken_bed_level_visualizer_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val etag = "etag1234"
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/settings"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf(
                        "Content-Type" to listOf("application/json"),
                        "Etag" to listOf(etag)
                    ),
                    //region content = ...
                    content = """
                      {
                            "api": {
                                "allowCrossOrigin": false,
                                "key": "522217826CF841A78537F56765092AA8"
                            },
                            "appearance": {
                                "closeModalsWithClick": true,
                                "color": "orange",
                                "colorIcon": true,
                                "colorTransparent": false,
                                "defaultLanguage": "_default",
                                "fuzzyTimes": true,
                                "name": "Beagle",
                                "showFahrenheitAlso": false,
                                "showInternalFilename": true
                            },
                            "devel": {
                                "pluginTimings": false
                            },
                            "feature": {
                                "autoUppercaseBlacklist": [
                                    "M117",
                                    "M118"
                                ],
                                "g90InfluencesExtruder": false,
                                "keyboardControl": true,
                                "modelSizeDetection": true,
                                "pollWatched": false,
                                "printCancelConfirmation": true,
                                "printStartConfirmation": false,
                                "rememberFileFolder": false,
                                "sdSupport": true,
                                "temperatureGraph": true,
                                "uploadOverwriteConfirmation": true
                            },
                            "folder": {
                                "timelapse": "/octoprint/octoprint/timelapse",
                                "uploads": "/octoprint/octoprint/uploads",
                                "watched": "/octoprint/octoprint/watched"
                            },
                            "gcodeAnalysis": {
                                "bedZ": 0.0,
                                "runAt": "idle"
                            },
                            "plugins": {
                                "action_command_notification": {
                                    "enable": true,
                                    "enable_popups": false
                                },
                                "action_command_prompt": {
                                    "command": "M876",
                                    "enable": "detected",
                                    "enable_emergency_sending": true,
                                    "enable_signal_support": true
                                },
                                "announcements": {
                                    "channel_order": [
                                        "_important",
                                        "_releases",
                                        "_blog",
                                        "_plugins",
                                        "_octopi"
                                    ],
                                    "channels": {
                                        "_blog": {
                                            "description": "Development news, community spotlights, OctoPrint On Air episodes and more from the official OctoBlog.",
                                            "name": "On the OctoBlog",
                                            "priority": 2,
                                            "read_until": 1661175900,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/octoblog.xml"
                                        },
                                        "_important": {
                                            "description": "Important announcements about OctoPrint.",
                                            "name": "Important Announcements",
                                            "priority": 1,
                                            "read_until": 1521111600,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/important.xml"
                                        },
                                        "_octopi": {
                                            "description": "News around OctoPi, the Raspberry Pi image including OctoPrint.",
                                            "name": "OctoPi News",
                                            "priority": 2,
                                            "read_until": 1659616200,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/octopi.xml"
                                        },
                                        "_plugins": {
                                            "description": "Announcements of new plugins released on the official Plugin Repository.",
                                            "name": "New Plugins in the Repository",
                                            "priority": 2,
                                            "read_until": 1662958800,
                                            "type": "rss",
                                            "url": "https://plugins.octoprint.org/feed.xml"
                                        },
                                        "_releases": {
                                            "description": "Announcements of new releases and release candidates of OctoPrint.",
                                            "name": "Release Announcements",
                                            "priority": 2,
                                            "read_until": 1663673400,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/releases.xml"
                                        }
                                    },
                                    "display_limit": 3,
                                    "enabled_channels": [
                                        "_important",
                                        "_releases",
                                        "_blog",
                                        "_plugins",
                                        "_octopi"
                                    ],
                                    "forced_channels": [
                                        "_important"
                                    ],
                                    "summary_limit": 300,
                                    "ttl": 360
                                },
                                "backup": {
                                    "restore_unsupported": false
                                },
                                "bedlevelvisualizer": {
                                    "camera_position": "-1.25,-1.25,0.25",
                                    "colorscale": "[[0, \"rebeccapurple\"],[0.4, \"rebeccapurple\"],[0.45, \"blue\"],[0.5, \"green\"],[0.55, \"yellow\"],[0.6, \"red\"],[1, \"red\"]]",
                                    "command": "command",
                                    "commands": [],
                                    "date_locale_format": "",
                                    "debug_logging": false,
                                    "descending_x": false,
                                    "descending_y": false,
                                    "flipX": false,
                                    "flipY": false,
                                    "graph_height": "450px",
                                    "graph_z_limits": "-0.2,0.2",
                                    "ignore_correction_matrix": false,
                                    "imperial": false,
                                    "mesh_timestamp": "6/6/2023, 12:06:35 PM",
                                    "mesh_unit": 1,
                                    "reverse": false,
                                    "rotation": 0,
                                    "save_mesh": true,
                                    "save_snapshots": false,
                                    "screw_hub": 0.5,
                                    "show_labels": true,
                                    "show_prusa_adjustments": false,
                                    "show_stored_mesh_on_tab": false,
                                    "show_webcam": false,
                                    "showdegree": false,
                                    "stored_mesh": [
                                        [
                                            ".",
                                            ".",
                                            ".",
                                            "."
                                        ],
                                        [
                                            "-0.030",
                                            "-0.028",
                                            "+0.010",
                                            "+0.012"
                                        ],
                                        [
                                            "-0.025",
                                            "-0.053",
                                            "-0.003",
                                            "+0.017"
                                        ],
                                        [
                                            "-0.010",
                                            "-0.038",
                                            "-0.015",
                                            "+0.012"
                                        ]
                                    ],
                                    "stored_mesh_x": [
                                        0,
                                        78,
                                        157,
                                        235
                                    ],
                                    "stored_mesh_y": [
                                        0,
                                        78,
                                        157,
                                        235
                                    ],
                                    "stored_mesh_z_height": 220,
                                    "stripFirst": false,
                                    "timeout": 1800,
                                    "use_center_origin": false,
                                    "use_relative_offsets": false
                                },
                                "cancelobject": {
                                    "aftergcode": null,
                                    "allowed": "",
                                    "beforegcode": null,
                                    "ignored": "ENDGCODE,STARTGCODE",
                                    "markers": true,
                                    "object_regex": [
                                        {
                                            "objreg": "; process (.*)"
                                        },
                                        {
                                            "objreg": ";MESH:(.*)"
                                        },
                                        {
                                            "objreg": "; printing object (.*)"
                                        },
                                        {
                                            "objreg": ";PRINTING: (.*)"
                                        }
                                    ],
                                    "reptag": "Object",
                                    "shownav": false,
                                    "stoptags": false
                                },
                                "cancelobject": {
                                    "aftergcode": null,
                                    "allowed": "",
                                    "beforegcode": null,
                                    "ignored": "ENDGCODE,STARTGCODE",
                                    "markers": true,
                                    "object_regex": [
                                        {
                                            "objreg": "; process (.*)"
                                        },
                                        {
                                            "objreg": ";MESH:(.*)"
                                        },
                                        {
                                            "objreg": "; printing object (.*)"
                                        },
                                        {
                                            "objreg": ";PRINTING: (.*)"
                                        }
                                    ],
                                    "reptag": "Object",
                                    "shownav": true,
                                    "stoptags": false
                                },
                                "discovery": {
                                    "addresses": null,
                                    "httpPassword": null,
                                    "httpUsername": null,
                                    "ignoredAddresses": null,
                                    "ignoredInterfaces": null,
                                    "interfaces": null,
                                    "model": {
                                        "description": null,
                                        "name": null,
                                        "number": null,
                                        "serial": null,
                                        "url": null,
                                        "vendor": null,
                                        "vendorUrl": null
                                    },
                                    "pathPrefix": null,
                                    "publicHost": null,
                                    "publicPort": null,
                                    "upnpUuid": "bda5fb9e-3a29-4e2b-af2f-fbfe4dc97e99",
                                    "zeroConf": []
                                },
                                "errortracking": {
                                    "enabled": true,
                                    "enabled_unreleased": false,
                                    "unique_id": "4a1caac0-a9df-4b26-a72f-ef4ee91d24bc",
                                    "url_coreui": "https://e1d6b5d760f241408382d62a3e7fb416@o118517.ingest.sentry.io/1374096",
                                    "url_server": "https://af30d06358144fb8af076ffd8984136d@o118517.ingest.sentry.io/1373987"
                                },
                                "eventmanager": {
                                    "availableEvents": [
                                        "Startup",
                                        "Shutdown",
                                        "ConnectivityChanged",
                                        "Connecting",
                                        "Connected",
                                        "Disconnecting",
                                        "Disconnected",
                                        "PrinterStateChanged",
                                        "PrinterReset",
                                        "ClientOpened",
                                        "ClientClosed",
                                        "ClientAuthed",
                                        "ClientDeauthed",
                                        "UserLoggedIn",
                                        "UserLoggedOut",
                                        "Upload",
                                        "FileSelected",
                                        "FileDeselected",
                                        "UpdatedFiles",
                                        "MetadataAnalysisStarted",
                                        "MetadataAnalysisFinished",
                                        "MetadataStatisticsUpdated",
                                        "FileAdded",
                                        "FileRemoved",
                                        "FileMoved",
                                        "FolderAdded",
                                        "FolderRemoved",
                                        "FolderMoved",
                                        "TransferStarted",
                                        "TransferDone",
                                        "TransferFailed",
                                        "PrintStarted",
                                        "PrintDone",
                                        "PrintFailed",
                                        "PrintCancelling",
                                        "PrintCancelled",
                                        "PrintPaused",
                                        "PrintResumed",
                                        "Error",
                                        "PowerOn",
                                        "PowerOff",
                                        "Home",
                                        "ZChange",
                                        "Waiting",
                                        "Dwelling",
                                        "Cooling",
                                        "Alert",
                                        "Conveyor",
                                        "Eject",
                                        "EStop",
                                        "PositionUpdate",
                                        "FirmwareData",
                                        "ToolChange",
                                        "RegisteredMessageReceived",
                                        "CommandSuppressed",
                                        "InvalidToolReported",
                                        "FilamentChange",
                                        "CaptureStart",
                                        "CaptureDone",
                                        "CaptureFailed",
                                        "PostRollStart",
                                        "PostRollEnd",
                                        "MovieRendering",
                                        "MovieDone",
                                        "MovieFailed",
                                        "SlicingStarted",
                                        "SlicingDone",
                                        "SlicingFailed",
                                        "SlicingCancelled",
                                        "SlicingProfileAdded",
                                        "SlicingProfileModified",
                                        "SlicingProfileDeleted",
                                        "PrinterProfileAdded",
                                        "PrinterProfileModified",
                                        "PrinterProfileDeleted",
                                        "SettingsUpdated",
                                        "plugin_backup_backup_created",
                                        "plugin_firmware_check_warning",
                                        "plugin_ngrok_connected",
                                        "plugin_ngrok_closed",
                                        "plugin_pluginmanager_install_plugin",
                                        "plugin_pluginmanager_uninstall_plugin",
                                        "plugin_pluginmanager_enable_plugin",
                                        "plugin_pluginmanager_disable_plugin",
                                        "plugin_psucontrol_psu_state_changed",
                                        "plugin_softwareupdate_update_succeeded",
                                        "plugin_softwareupdate_update_failed"
                                    ],
                                    "subscriptions": []
                                },
                                "firmware_check": {
                                    "ignore_infos": false
                                },
                                "gcodeviewer": {
                                    "mobileSizeThreshold": 2097152,
                                    "sizeThreshold": 20971520,
                                    "skipUntilThis": null
                                },
                                "mmu2filamentselect": {
                                    "filament1": "",
                                    "filament2": "",
                                    "filament3": "",
                                    "filament4": "",
                                    "filament5": "",
                                    "labelSource": "manual",
                                    "timeout": 30,
                                    "timeoutAction": "printerDialog"
                                },
                                "multicam": {
                                    "multicam_profiles": [
                                        {
                                            "URL": "http://192.168.1.242/webcam/?action=stream",
                                            "flipH": false,
                                            "flipV": false,
                                            "isButtonEnabled": false,
                                            "name": "Default",
                                            "rotate90": false,
                                            "snapshot": "http:/192.168.1.242/webcam/?action=snapshot",
                                            "streamRatio": "16:9"
                                        },
                                        {
                                            "URL": "http://octopi.local/webcam/?action=stream",
                                            "flipH": false,
                                            "flipV": false,
                                            "isButtonEnabled": true,
                                            "name": "Webcam 1",
                                            "rotate90": false,
                                            "snapshot": "http://octopi.local/webcam/?action=stream",
                                            "streamRatio": "16:9"
                                        }
                                    ]
                                },
                                "ngrok": {
                                    "auth_name": "name",
                                    "auth_pass": "pass",
                                    "auto_connect": true,
                                    "disable_local_ip_check": false,
                                    "hostname": "",
                                    "port": 5000,
                                    "region": "ap",
                                    "show_qr_code": false,
                                    "subdomain": "",
                                    "token": "1x5BkMgxgzwjwfLQ7qQweO0cg7U_5GK47r7JPgLcEXNbAPmap",
                                    "trust_basic_authentication": false
                                },
                                "octoapp": {
                                    "encryptionKey": "4c7cf1e0-9720-4a18-ad66-4acaf3790917",
                                    "version": "1.0.12"
                                },
                                "octoeverywhere": {
                                    "AddPrinterUrl": "https://octoeverywhere.com/getstarted?isFromOctoPrint=true&printerid=347R9DNGCJZ5IS3D9VYK7JLL1J5REM7ZRKZZN9K2BANG455618ZZZY4QBYC0",
                                    "ConnectedAccounts": "",
                                    "HasConnectedAccounts": false,
                                    "HttpFrontendIsHttps": false,
                                    "HttpFrontendPort": 5001,
                                    "NoAccountConnectedLastInformDateTime": "Mon, 26 Sep 2022 08:40:02 GMT",
                                    "Pid": "pid",
                                    "PluginUpdateRequired": false,
                                    "PluginVersion": "1.10.5",
                                    "PrinterKey": "key"
                                },
                                "pluginmanager": {
                                    "confirm_disable": false,
                                    "dependency_links": false,
                                    "hidden": [],
                                    "ignore_throttled": false,
                                    "notices": "https://plugins.octoprint.org/notices.json",
                                    "notices_ttl": 360,
                                    "pip_args": null,
                                    "pip_force_user": false,
                                    "repository": "https://plugins.octoprint.org/plugins.json",
                                    "repository_ttl": 1440
                                },
                                "psucontrol": {
                                    "GPIODevice": "",
                                    "autoOn": false,
                                    "autoOnTriggerGCodeCommands": "G0,G1,G2,G3,G10,G11,G28,G29,G32,M104,M106,M109,M140,M190",
                                    "connectOnPowerOn": false,
                                    "disconnectOnPowerOff": false,
                                    "enablePowerOffWarningDialog": true,
                                    "enablePseudoOnOff": false,
                                    "idleIgnoreCommands": "M105",
                                    "idleTimeout": 30,
                                    "idleTimeoutWaitTemp": 50,
                                    "invertonoffGPIOPin": false,
                                    "invertsenseGPIOPin": false,
                                    "offGCodeCommand": "M81",
                                    "offSysCommand": "",
                                    "onGCodeCommand": "M80",
                                    "onSysCommand": "",
                                    "onoffGPIOPin": 0,
                                    "postOnDelay": 0.0,
                                    "powerOffWhenIdle": false,
                                    "pseudoOffGCodeCommand": "M81",
                                    "pseudoOnGCodeCommand": "M80",
                                    "senseGPIOPin": 0,
                                    "senseGPIOPinPUD": "",
                                    "sensePollingInterval": 5,
                                    "senseSystemCommand": "",
                                    "sensingMethod": "INTERNAL",
                                    "sensingPlugin": "",
                                    "switchingMethod": "GCODE",
                                    "switchingPlugin": "",
                                    "turnOffWhenError": false,
                                    "turnOnWhenApiUploadPrint": false
                                },
                                "softwareupdate": {
                                    "cache_ttl": 1440,
                                    "check_overlay_py2_url": "https://plugins.octoprint.org/update_check_overlay_py2.json",
                                    "check_overlay_ttl": 360,
                                    "check_overlay_url": "https://plugins.octoprint.org/update_check_overlay.json",
                                    "credentials": {},
                                    "ignore_throttled": false,
                                    "minimum_free_storage": 150,
                                    "notify_users": true,
                                    "octoprint_branch_mappings": [
                                        {
                                            "branch": "master",
                                            "commitish": [
                                                "master"
                                            ],
                                            "name": "Stable"
                                        },
                                        {
                                            "branch": "rc/maintenance",
                                            "commitish": [
                                                "rc/maintenance"
                                            ],
                                            "name": "Maintenance RCs"
                                        },
                                        {
                                            "branch": "rc/devel",
                                            "commitish": [
                                                "rc/maintenance",
                                                "rc/devel"
                                            ],
                                            "name": "Devel RCs"
                                        }
                                    ],
                                    "octoprint_checkout_folder": null,
                                    "octoprint_method": "pip",
                                    "octoprint_pip_target": "https://github.com/OctoPrint/OctoPrint/archive/{target_version}.zip",
                                    "octoprint_release_channel": "master",
                                    "octoprint_tracked_branch": "staging/bugfix",
                                    "octoprint_type": "github_release",
                                    "pip_command": null,
                                    "pip_enable_check": false,
                                    "queued_updates": [],
                                    "updatelog_cutoff": 43200
                                },
                                "tracking": {
                                    "enabled": false,
                                    "events": {
                                        "commerror": true,
                                        "plugin": true,
                                        "pong": true,
                                        "printer": true,
                                        "printer_safety_check": true,
                                        "printjob": true,
                                        "slicing": true,
                                        "startup": true,
                                        "throttled": true,
                                        "update": true,
                                        "webui_load": true
                                    },
                                    "ping": null,
                                    "pong": 86400,
                                    "server": null,
                                    "unique_id": "1499cbbc-a778-42ab-b32c-c557f1ed59bd"
                                },
                                "virtual_printer": {
                                    "ambientTemperature": 21.3,
                                    "brokenM29": true,
                                    "brokenResend": false,
                                    "busyInterval": 2.0,
                                    "capabilities": {
                                        "AUTOREPORT_POS": false,
                                        "AUTOREPORT_SD_STATUS": true,
                                        "AUTOREPORT_TEMP": true,
                                        "EMERGENCY_PARSER": true,
                                        "EXTENDED_M20": false
                                    },
                                    "commandBuffer": 4,
                                    "echoOnM117": true,
                                    "enable_eeprom": true,
                                    "enabled": true,
                                    "errors": {
                                        "checksum_mismatch": "Checksum mismatch",
                                        "checksum_missing": "Missing checksum",
                                        "command_unknown": "Unknown command {}",
                                        "lineno_mismatch": "expected line {} got {}",
                                        "lineno_missing": "No Line Number with checksum, Last Line: {}",
                                        "maxtemp": "MAXTEMP triggered!",
                                        "mintemp": "MINTEMP triggered!"
                                    },
                                    "firmwareName": "Virtual Marlin 1.0",
                                    "forceChecksum": false,
                                    "hasBed": true,
                                    "hasChamber": false,
                                    "includeCurrentToolInTemps": true,
                                    "includeFilenameInOpened": true,
                                    "klipperTemperatureReporting": false,
                                    "locked": false,
                                    "m105NoTargetFormatString": "{heater}:{actual:.2f}",
                                    "m105TargetFormatString": "{heater}:{actual:.2f}/ {target:.2f}",
                                    "m114FormatString": "X:{x} Y:{y} Z:{z} E:{e[current]} Count: A:{a} B:{b} C:{c}",
                                    "m115FormatString": "FIRMWARE_NAME:{firmware_name} PROTOCOL_VERSION:1.0",
                                    "m115ReportCapabilities": true,
                                    "numExtruders": 1,
                                    "okAfterResend": false,
                                    "okBeforeCommandOutput": false,
                                    "okFormatString": "ok",
                                    "passcode": "1234",
                                    "pinnedExtruders": null,
                                    "preparedOks": [],
                                    "repetierStyleTargetTemperature": false,
                                    "reprapfwM114": false,
                                    "resend_ratio": 0,
                                    "resetLines": [
                                        "start",
                                        "Marlin: Virtual Marlin!",
                                        "",
                                        "SD card ok"
                                    ],
                                    "rxBuffer": 64,
                                    "sdFiles": {
                                        "longname": false,
                                        "longname_quoted": true,
                                        "size": true
                                    },
                                    "sendBusy": false,
                                    "sendWait": true,
                                    "sharedNozzle": false,
                                    "simulateReset": true,
                                    "smoothieTemperatureReporting": false,
                                    "supportF": false,
                                    "supportM112": true,
                                    "support_M503": true,
                                    "throttle": 0.01,
                                    "waitInterval": 1.0
                                }
                            },
                            "scripts": {
                                "gcode": {
                                    "afterPrintCancelled": "; disable motors\nM84\n\n;disable all heaters\n{% snippet 'disable_hotends' %}\n{% snippet 'disable_bed' %}\n;disable fan\nM106 S0",
                                    "psucontrol_post_on": "",
                                    "psucontrol_pre_off": "",
                                    "snippets/disable_bed": "{% if printer_profile.heatedBed %}M140 S0\n{% endif %}",
                                    "snippets/disable_hotends": "{% if printer_profile.extruder.sharedNozzle %}M104 T0 S0\n{% else %}{% for tool in range(printer_profile.extruder.count) %}M104 T{{ tool }} S0\n{% endfor %}{% endif %}"
                                }
                            },
                            "serial": {
                                "abortHeatupOnCancel": true,
                                "ackMax": 1,
                                "additionalBaudrates": [],
                                "additionalPorts": [],
                                "alwaysSendChecksum": false,
                                "autoconnect": false,
                                "baudrate": null,
                                "baudrateOptions": [
                                    250000,
                                    230400,
                                    115200,
                                    57600,
                                    38400,
                                    19200,
                                    9600
                                ],
                                "blacklistedBaudrates": [],
                                "blacklistedPorts": [],
                                "blockWhileDwelling": false,
                                "blockedCommands": [
                                    "M0",
                                    "M1"
                                ],
                                "capAutoreportPos": true,
                                "capAutoreportSdStatus": true,
                                "capAutoreportTemp": true,
                                "capBusyProtocol": true,
                                "capEmergencyParser": true,
                                "capExtendedM20": true,
                                "checksumRequiringCommands": [
                                    "M110"
                                ],
                                "disableSdPrintingDetection": false,
                                "disconnectOnErrors": true,
                                "emergencyCommands": [
                                    "M112",
                                    "M108",
                                    "M410"
                                ],
                                "enableShutdownActionCommand": false,
                                "encoding": "ascii",
                                "exclusive": true,
                                "externalHeatupDetection": true,
                                "firmwareDetection": true,
                                "helloCommand": "M110 N0",
                                "ignoreEmptyPorts": false,
                                "ignoreErrorsFromFirmware": false,
                                "ignoreIdenticalResends": false,
                                "ignoredCommands": [],
                                "log": false,
                                "logPositionOnCancel": false,
                                "logPositionOnPause": true,
                                "longRunningCommands": [
                                    "G4",
                                    "G28",
                                    "G29",
                                    "G30",
                                    "G32",
                                    "M400",
                                    "M226",
                                    "M600"
                                ],
                                "lowLatency": false,
                                "maxTimeoutsIdle": 2,
                                "maxTimeoutsLong": 5,
                                "maxTimeoutsPrinting": 5,
                                "neverSendChecksum": false,
                                "notifySuppressedCommands": "warn",
                                "pausingCommands": [
                                    "M0",
                                    "M1",
                                    "M25"
                                ],
                                "port": null,
                                "portOptions": [
                                    "VIRTUAL"
                                ],
                                "repetierTargetTemp": false,
                                "resendRatioStart": 100,
                                "resendRatioThreshold": 10,
                                "sanityCheckTools": true,
                                "sdAlwaysAvailable": false,
                                "sdCancelCommand": "M25",
                                "sdLowerCase": false,
                                "sdRelativePath": false,
                                "sendChecksumWithUnknownCommands": false,
                                "sendM112OnError": true,
                                "supportResendsWithoutOk": "detect",
                                "swallowOkAfterResend": true,
                                "timeoutBaudrateDetectionPause": 1.0,
                                "timeoutCommunication": 30.0,
                                "timeoutCommunicationBusy": 3.0,
                                "timeoutConnection": 10.0,
                                "timeoutDetectionConsecutive": 2.0,
                                "timeoutDetectionFirst": 10.0,
                                "timeoutPosAutoreport": 5.0,
                                "timeoutPositionLogWait": 10.0,
                                "timeoutSdStatus": 1.0,
                                "timeoutSdStatusAutoreport": 1.0,
                                "timeoutTemperature": 5.0,
                                "timeoutTemperatureAutoreport": 2.0,
                                "timeoutTemperatureTargetSet": 2.0,
                                "triggerOkForM29": true,
                                "unknownCommandsNeedAck": false,
                                "useParityWorkaround": "detect",
                                "waitForStart": false
                            },
                            "server": {
                                "allowFraming": false,
                                "commands": {
                                    "serverRestartCommand": "s6-svc -r /var/run/s6/services/octoprint",
                                    "systemRestartCommand": null,
                                    "systemShutdownCommand": null
                                },
                                "diskspace": {
                                    "critical": 209715200,
                                    "warning": 524288000
                                },
                                "onlineCheck": {
                                    "enabled": true,
                                    "host": "1.1.1.1",
                                    "interval": 15,
                                    "name": "octoprint.org",
                                    "port": 53
                                },
                                "pluginBlacklist": {
                                    "enabled": true,
                                    "ttl": 15,
                                    "url": "https://plugins.octoprint.org/blacklist.json"
                                }
                            },
                            "slicing": {
                                "defaultSlicer": null
                            },
                            "system": {
                                "actions": [],
                                "events": null
                            },
                            "temperature": {
                                "cutoff": 30,
                                "profiles": [
                                    {
                                        "bed": 100,
                                        "chamber": null,
                                        "extruder": 210,
                                        "name": "ABS"
                                    },
                                    {
                                        "bed": 60,
                                        "chamber": null,
                                        "extruder": 180,
                                        "name": "PLA"
                                    }
                                ],
                                "sendAutomatically": false,
                                "sendAutomaticallyAfter": 1
                            },
                            "terminalFilters": [
                                {
                                    "name": "Suppress temperature messages",
                                    "regex": "regex"
                                },
                                {
                                    "name": "Suppress SD status messages",
                                    "regex": "regex"                                
                                },
                                {
                                    "name": "Suppress position messages",
                                    "regex": "regex" 
                                },
                                {
                                    "name": "Suppress wait responses",
                                    "regex": "regex"                               
                                },
                                {
                                    "name": "Suppress processing responses",
                                    "regex": "regex"
                                 }
                            ],
                            "webcam": {
                                "bitrate": "10000k",
                                "cacheBuster": false,
                                "ffmpegCommandline": "{ffmpeg} -framerate {fps} -i \"{input}\" -vcodec {videocodec} -threads {threads} -b:v {bitrate} -f {containerformat} -y {filters} \"{output}\"",
                                "ffmpegPath": "/usr/bin/ffmpeg",
                                "ffmpegThreads": 1,
                                "ffmpegVideoCodec": "libx264",
                                "flipH": false,
                                "flipV": false,
                                "rotate90": false,
                                "snapshotSslValidation": true,
                                "snapshotTimeout": 5,
                                "snapshotUrl": "http://192.168.1.242/webcam/?action=snapshot",
                                "streamRatio": "16:9",
                                "streamTimeout": 5,
                                "streamUrl": "http://192.168.1.242/webcam/?action=stream",
                                "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                ],
                                "timelapseEnabled": true,
                                "watermark": true,
                                "webcamEnabled": true
                            }
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoSettingsApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSettings()
        //endregion
        //region THEN
        assertEquals(
            expected = Settings(
                hash = etag,
                webcam = WebcamSettings(
                    webcamEnabled = true,
                    webcams = listOf(
                        WebcamSettings.Webcam(
                            streamUrl = "http://192.168.1.242/webcam/?action=stream",
                            provider = "octoprint",
                            flipH = false,
                            flipV = false,
                            rotate90 = false,
                            streamRatio = "16:9",
                            snapshotUrl = "http://192.168.1.242/webcam/?action=snapshot",
                            extras = null,
                            snapshotDisplay = null,
                            canSnapshot = true,
                            displayName = "OctoPrint",
                            name = "octoprint",
                            type = WebcamSettings.Webcam.Type.Mjpeg,
                        )
                    )
                ),
                plugins = Settings.PluginSettingsGroup(
                    gcodeViewer = Settings.GcodeViewer(
                        sizeThreshold = 20971520,
                        mobileSizeThreshold = 2097152,
                    ),
                    octoEverywhere = Settings.OctoEverywhere,
                    ngrok = Settings.Ngrok(
                        authPassword = "pass",
                        authName = "name",
                    ),
                    octoAppCompanion = Settings.OctoAppCompanion(
                        encryptionKey = "4c7cf1e0-9720-4a18-ad66-4acaf3790917",
                        version = "1.0.12",
                        running = true,
                    ),
                    multiCamSettings = Settings.MultiCam(
                        webcams = listOf(
                            WebcamSettings.Webcam(
                                streamUrl = "http://octopi.local/webcam/?action=stream",
                                provider = "multicam_legacy",
                                flipH = false,
                                flipV = false,
                                rotate90 = false,
                                streamRatio = "16:9",
                                snapshotUrl = null,
                                name = "Webcam 1",
                                displayName = "Webcam 1 (MultiCam)",
                                canSnapshot = false,
                                snapshotDisplay = null,
                                extras = null,
                                type = WebcamSettings.Webcam.Type.Mjpeg,
                            ),
                        ),
                    ),
                    discovery = Settings.Discovery(
                        uuid = "bda5fb9e-3a29-4e2b-af2f-fbfe4dc97e99"
                    ),
                    cancelObject = Settings.CancelObject,
                    psuControl = Settings.PsuControl,
                    mmu = Settings.Mmu(
                        filament = listOf(
                            Settings.Mmu.Filament(name = null, toolIndex = 0, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 1, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 2, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 3, color = null),
                            Settings.Mmu.Filament(name = null, toolIndex = 4, color = null),
                        ),
                        labelSource = Settings.Mmu.LabelSource.Manual,
                        plugin = Settings.Mmu.Plugin.Mmu2FilamentSelect,
                    ),
                    bedLevelVisualizer = Settings.BedLevelVisualizer,
                ),
                temperaturePresets = listOf(
                    Settings.TemperaturePreset(
                        name = "ABS",
                        components = listOf(
                            Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 100f, isBed = true),
                            Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 210f, isExtruder = true),
                        ),
                    ),
                    Settings.TemperaturePreset(
                        name = "PLA",
                        components = listOf(
                            Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 60f, isBed = true),
                            Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 180f, isExtruder = true),
                        ),
                    ),
                ),
                coolDownProfile = Settings.TemperaturePreset(
                    name = "__cooldown",
                    components = listOf(
                        Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 0f, isBed = true),
                        Settings.TemperaturePreset.Component(key = "chamber", displayName = "chamber", temperature = 0f, isChamber = true),
                        Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 0f, isExtruder = true),
                    ),
                ),
                terminalFilters = listOf(
                    Settings.TerminalFilter(name = "Suppress temperature messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress SD status messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress position messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress wait responses", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress processing responses", regex = "regex")
                ),
                appearance = Settings.Appearance(
                    name = "Beagle",
                    colors = Settings.Appearance.Colors(
                        themeName = "orange",
                        light = Settings.Appearance.ColorScheme(main = HexColor("#FF9800"), accent = HexColor("#FFE6C1")),
                        dark = Settings.Appearance.ColorScheme(main = HexColor("#FF9800"), accent = HexColor("#FFE6C1")),
                    ),
                ),
                bedMesh = Settings.BedMesh(
                    refreshCommand = "command",
                    colorScale = listOf(
                        -1.0f to HexColor("#663399"),
                        -0.19999999f to HexColor("#663399"),
                        -0.100000024f to HexColor("#0000FF"),
                        0.0f to HexColor("#008000"),
                        0.100000024f to HexColor("#FFFF00"),
                        0.20000005f to HexColor("#FF0000"),
                        1.0f to HexColor("#FF0000"),
                    ),
                    meshX = listOf(0.0f, 78.0f, 157.0f, 235.0f),
                    meshY = listOf(0.0f, 78.0f, 157.0f, 235.0f),
                    mesh = listOf(
                        listOf(null, null, null, null),
                        listOf(-0.03f, -0.028f, 0.01f, 0.012f),
                        listOf(-0.025f, -0.053f, -0.003f, 0.017f),
                        listOf(-0.01f, -0.038f, -0.015f, 0.012f)
                    ),
                    graphZMin = -0.2f,
                    graphZMax = 0.2f,
                )
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_settings_are_loaded_on_OctoPrint_1_9_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val etag = "etag1234"
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/settings"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf(
                        "Content-Type" to listOf("application/json"),
                        "Etag" to listOf(etag)
                    ),
                    //region content = ...
                    content = """
                      {
                          "api": {
                            "allowCrossOrigin": false,
                            "key": "0027E5A7222D431C8197D868D7EF5BC1"
                          },
                          "appearance": {
                            "closeModalsWithClick": true,
                            "color": "default",
                            "colorIcon": true,
                            "colorTransparent": false,
                            "defaultLanguage": "_default",
                            "fuzzyTimes": true,
                            "name": "",
                            "showFahrenheitAlso": false,
                            "showInternalFilename": true
                          },
                          "devel": {
                            "pluginTimings": false
                          },
                          "feature": {
                            "autoUppercaseBlacklist": [
                              "M117",
                              "M118"
                            ],
                            "g90InfluencesExtruder": false,
                            "keyboardControl": true,
                            "modelSizeDetection": true,
                            "pollWatched": false,
                            "printCancelConfirmation": true,
                            "printStartConfirmation": false,
                            "rememberFileFolder": false,
                            "sdSupport": true,
                            "temperatureGraph": true,
                            "uploadOverwriteConfirmation": true
                          },
                          "folder": {
                            "timelapse": "/home/pi/.octoprint/timelapse",
                            "uploads": "/home/pi/.octoprint/uploads",
                            "watched": "/home/pi/.octoprint/watched"
                          },
                          "gcodeAnalysis": {
                            "bedZ": 0.0,
                            "runAt": "idle"
                          },
                          "plugins": {
                            "action_command_notification": {
                              "enable": true,
                              "enable_popups": false
                            },
                            "action_command_prompt": {
                              "command": "M876",
                              "enable": "detected",
                              "enable_emergency_sending": true,
                              "enable_signal_support": true
                            },
                            "announcements": {
                              "channel_order": [
                                "_important",
                                "_releases",
                                "_blog",
                                "_plugins",
                                "_octopi"
                              ],
                              "channels": {
                                "_blog": {
                                  "description": "Development news, community spotlights, OctoPrint On Air episodes and more from the official OctoBlog.",
                                  "name": "On the OctoBlog",
                                  "priority": 2,
                                  "read_until": 1677590400,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/octoblog.xml"
                                },
                                "_important": {
                                  "description": "Important announcements about OctoPrint.",
                                  "name": "Important Announcements",
                                  "priority": 1,
                                  "read_until": 1521111600,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/important.xml"
                                },
                                "_octopi": {
                                  "description": "News around OctoPi, the Raspberry Pi image including OctoPrint.",
                                  "name": "OctoPi News",
                                  "priority": 2,
                                  "read_until": 1677062400,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/octopi.xml"
                                },
                                "_plugins": {
                                  "description": "Announcements of new plugins released on the official Plugin Repository.",
                                  "name": "New Plugins in the Repository",
                                  "priority": 2,
                                  "read_until": 1682312400,
                                  "type": "rss",
                                  "url": "https://plugins.octoprint.org/feed.xml"
                                },
                                "_releases": {
                                  "description": "Announcements of new releases and release candidates of OctoPrint.",
                                  "name": "Release Announcements",
                                  "priority": 2,
                                  "read_until": 1681398000,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/releases.xml"
                                }
                              },
                              "display_limit": 3,
                              "enabled_channels": [
                                "_important",
                                "_releases",
                                "_blog",
                                "_plugins",
                                "_octopi"
                              ],
                              "forced_channels": [
                                "_important"
                              ],
                              "summary_limit": 300,
                              "ttl": 360
                            },
                            "backup": {
                              "restore_unsupported": false
                            },
                            "bedlevelvisualizer": {
                                "camera_position": "-1.25,-1.25,0.25",
                                "command": "command\nnew line\ttab",
                                "commands": [],
                                "date_locale_format": "",
                                "debug_logging": false,
                                "descending_x": false,
                                "descending_y": false,
                                "flipX": false,
                                "flipY": false,
                                "graph_height": "450px",
                                "graph_z_limits": "-0.2,0.2",
                                "ignore_correction_matrix": false,
                                "imperial": false,
                                "mesh_timestamp": "5/28/2023, 9:34:21 AM",
                                "mesh_unit": 1,
                                "reverse": false,
                                "rotation": 0,
                                "save_mesh": true,
                                "save_snapshots": false,
                                "screw_hub": 0.5,
                                "show_labels": true,
                                "show_prusa_adjustments": false,
                                "show_stored_mesh_on_tab": false,
                                "show_webcam": false,
                                "showdegree": false,
                                "stored_mesh": [
                                    [
                                        "+0.020",
                                        "-0.010",
                                        "+0.017",
                                        "+0.012"
                                    ],
                                    [
                                        "+0.042",
                                        "-0.005",
                                        "+0.052",
                                        "+0.045"
                                    ],
                                    [
                                        "+0.057",
                                        "-0.020",
                                        "+0.025",
                                        "+0.035"
                                    ],
                                    [
                                        "+0.010",
                                        "-0.063",
                                        "-0.045",
                                        "-0.010"
                                    ]
                                ],
                                "stored_mesh_x": [
                                    0,
                                    78,
                                    157,
                                    235
                                ],
                                "stored_mesh_y": [
                                    0,
                                    78,
                                    157,
                                    235
                                ],
                                "stored_mesh_z_height": 220,
                                "stripFirst": false,
                                "timeout": 1800,
                                "use_center_origin": false,
                                "use_relative_offsets": false
                            },
                            "camerastreamer_control": {
                              "flipH": false,
                              "flipV": false,
                              "mjpg": {
                                "cacheBuster": false,
                                "url": "stream"
                              },
                              "mode": "webrtc",
                              "ratio": "16:9",
                              "rotate90": false,
                              "snapshot": {
                                "timeout": 5,
                                "url": "snapshot",
                                "validate_ssl": true
                              },
                              "timeout": 5,
                              "url": "/webcam/",
                              "webrtc": {
                                "stun": "stun:stun.l.google.com:19302",
                                "url": "webrtc"
                              }
                            },
                            "classicwebcam": {
                              "cacheBuster": false,
                              "flipH": false,
                              "flipV": false,
                              "rotate90": false,
                              "snapshot": "http://127.0.0.1:8080/?action=snapshot",
                              "snapshotSslValidation": true,
                              "snapshotTimeout": 5,
                              "stream": "/webcam/?action=stream",
                              "streamRatio": "16:9",
                              "streamTimeout": 5,
                              "streamWebrtcIceServers": [
                                "stun:stun.l.google.com:19302"
                              ]
                            },
                            "discovery": {
                              "addresses": null,
                              "httpPassword": null,
                              "httpUsername": null,
                              "ignoredAddresses": null,
                              "ignoredInterfaces": null,
                              "interfaces": null,
                              "model": {
                                "description": null,
                                "name": null,
                                "number": null,
                                "serial": null,
                                "url": null,
                                "vendor": null,
                                "vendorUrl": null
                              },
                              "pathPrefix": null,
                              "publicHost": null,
                              "publicPort": 80,
                              "upnpUuid": "7c040da3-21e9-4a0d-9060-4f0d4fcad8a3",
                              "zeroConf": []
                            },
                            "errortracking": {
                              "enabled": true,
                              "enabled_unreleased": false,
                              "unique_id": "a1218d9f-3aa9-4e20-88c5-99ebf3837e40",
                              "url_coreui": "https://547ae711243d408e86fdee27cca233f8@o118517.ingest.sentry.io/1374096",
                              "url_server": "https://4045f26d0af548729d66d42330513f84@o118517.ingest.sentry.io/1373987"
                            },
                            "eventmanager": {
                              "availableEvents": [
                                "Startup",
                                "Shutdown",
                                "ConnectivityChanged",
                                "Connecting",
                                "Connected",
                                "Disconnecting",
                                "Disconnected",
                                "ConnectionsAutorefreshed",
                                "PrinterStateChanged",
                                "PrinterReset",
                                "ClientOpened",
                                "ClientClosed",
                                "ClientAuthed",
                                "ClientDeauthed",
                                "UserLoggedIn",
                                "UserLoggedOut",
                                "Upload",
                                "FileSelected",
                                "FileDeselected",
                                "UpdatedFiles",
                                "MetadataAnalysisStarted",
                                "MetadataAnalysisFinished",
                                "MetadataStatisticsUpdated",
                                "FileAdded",
                                "FileRemoved",
                                "FileMoved",
                                "FolderAdded",
                                "FolderRemoved",
                                "FolderMoved",
                                "TransferStarted",
                                "TransferDone",
                                "TransferFailed",
                                "PrintStarted",
                                "PrintDone",
                                "PrintFailed",
                                "PrintCancelling",
                                "PrintCancelled",
                                "PrintPaused",
                                "PrintResumed",
                                "Error",
                                "ChartMarked",
                                "PowerOn",
                                "PowerOff",
                                "Home",
                                "ZChange",
                                "Waiting",
                                "Dwelling",
                                "Cooling",
                                "Alert",
                                "Conveyor",
                                "Eject",
                                "EStop",
                                "PositionUpdate",
                                "FirmwareData",
                                "ToolChange",
                                "RegisteredMessageReceived",
                                "CommandSuppressed",
                                "InvalidToolReported",
                                "FilamentChange",
                                "CaptureStart",
                                "CaptureDone",
                                "CaptureFailed",
                                "PostRollStart",
                                "PostRollEnd",
                                "MovieRendering",
                                "MovieDone",
                                "MovieFailed",
                                "SlicingStarted",
                                "SlicingDone",
                                "SlicingFailed",
                                "SlicingCancelled",
                                "SlicingProfileAdded",
                                "SlicingProfileModified",
                                "SlicingProfileDeleted",
                                "PrinterProfileAdded",
                                "PrinterProfileModified",
                                "PrinterProfileDeleted",
                                "SettingsUpdated",
                                "plugin_backup_backup_created",
                                "plugin_firmware_check_warning",
                                "plugin_pi_support_throttle_state",
                                "plugin_pluginmanager_install_plugin",
                                "plugin_pluginmanager_uninstall_plugin",
                                "plugin_pluginmanager_enable_plugin",
                                "plugin_pluginmanager_disable_plugin",
                                "plugin_softwareupdate_update_succeeded",
                                "plugin_softwareupdate_update_failed"
                              ],
                              "subscriptions": []
                            },
                            "firmware_check": {
                              "ignore_infos": false
                            },
                            "gcodeviewer": {
                              "alwaysCompress": false,
                              "compressionSizeThreshold": 209715200,
                              "mobileSizeThreshold": 2097152,
                              "sizeThreshold": 20971520,
                              "skipUntilThis": null
                            },
                            "pi_support": {
                              "ignore_default_password": false,
                              "ignore_undervoltage_on_printstart": false,
                              "ignore_unrecommended_model": false,
                              "vcgencmd_throttle_check_command": "/usr/bin/vcgencmd get_throttled",
                              "vcgencmd_throttle_check_enabled": true
                            },
                            "pluginmanager": {
                              "confirm_disable": false,
                              "dependency_links": false,
                              "hidden": [],
                              "ignore_throttled": false,
                              "notices": "https://plugins.octoprint.org/notices.json",
                              "notices_ttl": 360,
                              "pip_args": null,
                              "pip_force_user": false,
                              "repository": "https://plugins.octoprint.org/plugins.json",
                              "repository_ttl": 1440
                            },
                            "softwareupdate": {
                              "cache_ttl": 1440,
                              "check_overlay_py2_url": "https://plugins.octoprint.org/update_check_overlay_py2.json",
                              "check_overlay_ttl": 360,
                              "check_overlay_url": "https://plugins.octoprint.org/update_check_overlay.json",
                              "credentials": {
                                "bitbucket_password_set": false,
                                "bitbucket_user_set": false,
                                "github_set": false
                              },
                              "ignore_throttled": false,
                              "minimum_free_storage": 150,
                              "notify_users": true,
                              "octoprint_branch_mappings": [
                                {
                                  "branch": "master",
                                  "commitish": [
                                    "master"
                                  ],
                                  "name": "Stable"
                                },
                                {
                                  "branch": "rc/maintenance",
                                  "commitish": [
                                    "rc/maintenance"
                                  ],
                                  "name": "Maintenance RCs"
                                },
                                {
                                  "branch": "rc/devel",
                                  "commitish": [
                                    "rc/maintenance",
                                    "rc/devel"
                                  ],
                                  "name": "Devel RCs"
                                }
                              ],
                              "octoprint_checkout_folder": null,
                              "octoprint_method": "pip",
                              "octoprint_pip_target": "https://github.com/OctoPrint/OctoPrint/archive/{target_version}.zip",
                              "octoprint_release_channel": "rc/maintenance",
                              "octoprint_tracked_branch": null,
                              "octoprint_type": "github_release",
                              "pip_command": null,
                              "pip_enable_check": false,
                              "queued_updates": [],
                              "updatelog_cutoff": 43200
                            },
                            "tracking": {
                              "enabled": true,
                              "events": {
                                "commerror": true,
                                "plugin": true,
                                "pong": true,
                                "printer": true,
                                "printer_safety_check": true,
                                "printjob": true,
                                "slicing": true,
                                "startup": true,
                                "throttled": true,
                                "update": true,
                                "webui_load": true
                              },
                              "ping": null,
                              "pong": 86400,
                              "server": null,
                              "unique_id": "a9f7bf55-eb87-4500-bc1e-335fd4e6bda1"
                            },
                            "virtual_printer": {
                              "ambientTemperature": 21.3,
                              "brokenM29": true,
                              "brokenResend": false,
                              "busyInterval": 2.0,
                              "capabilities": {
                                "AUTOREPORT_POS": false,
                                "AUTOREPORT_SD_STATUS": true,
                                "AUTOREPORT_TEMP": true,
                                "EMERGENCY_PARSER": true,
                                "EXTENDED_M20": false,
                                "LFN_WRITE": false
                              },
                              "commandBuffer": 4,
                              "echoOnM117": true,
                              "enable_eeprom": true,
                              "enabled": false,
                              "errors": {
                                "checksum_mismatch": "Checksum mismatch",
                                "checksum_missing": "Missing checksum",
                                "command_unknown": "Unknown command {}",
                                "lineno_mismatch": "expected line {} got {}",
                                "lineno_missing": "No Line Number with checksum, Last Line: {}",
                                "maxtemp": "MAXTEMP triggered!",
                                "mintemp": "MINTEMP triggered!"
                              },
                              "firmwareName": "Virtual Marlin 1.0",
                              "forceChecksum": false,
                              "hasBed": true,
                              "hasChamber": false,
                              "includeCurrentToolInTemps": true,
                              "includeFilenameInOpened": true,
                              "klipperTemperatureReporting": false,
                              "locked": false,
                              "m105NoTargetFormatString": "{heater}:{actual:.2f}",
                              "m105TargetFormatString": "{heater}:{actual:.2f}/ {target:.2f}",
                              "m114FormatString": "X:{x} Y:{y} Z:{z} E:{e[current]} Count: A:{a} B:{b} C:{c}",
                              "m115FormatString": "FIRMWARE_NAME:{firmware_name} PROTOCOL_VERSION:1.0",
                              "m115ReportCapabilities": true,
                              "numExtruders": 1,
                              "okAfterResend": false,
                              "okBeforeCommandOutput": false,
                              "okFormatString": "ok",
                              "passcode": "1234",
                              "pinnedExtruders": null,
                              "preparedOks": [],
                              "repetierStyleTargetTemperature": false,
                              "reprapfwM114": false,
                              "resend_ratio": 0,
                              "resetLines": [
                                "start",
                                "Marlin: Virtual Marlin!",
                                "\u0080",
                                "SD card ok"
                              ],
                              "rxBuffer": 64,
                              "sdFiles": {
                                "longname": false,
                                "longname_quoted": true,
                                "size": true
                              },
                              "sendBusy": false,
                              "sendWait": true,
                              "sharedNozzle": false,
                              "simulateReset": true,
                              "smoothieTemperatureReporting": false,
                              "supportF": false,
                              "supportM112": true,
                              "support_M503": true,
                              "throttle": 0.01,
                              "waitInterval": 1.0
                            }
                          },
                          "scripts": {
                            "gcode": {
                              "afterPrintCancelled": "; disable motors\nM84\n\n;disable all heaters\n{% snippet 'disable_hotends' %}\n{% snippet 'disable_bed' %}\n;disable fan\nM106 S0",
                              "snippets/disable_bed": "{% if printer_profile.heatedBed %}M140 S0\n{% endif %}",
                              "snippets/disable_hotends": "{% if printer_profile.extruder.sharedNozzle %}M104 T0 S0\n{% else %}{% for tool in range(printer_profile.extruder.count) %}M104 T{{ tool }} S0\n{% endfor %}{% endif %}"
                            }
                          },
                          "serial": {
                            "abortHeatupOnCancel": true,
                            "ackMax": 1,
                            "additionalBaudrates": [],
                            "additionalPorts": [],
                            "alwaysSendChecksum": false,
                            "autoconnect": false,
                            "baudrate": null,
                            "baudrateOptions": [
                              250000,
                              230400,
                              115200,
                              57600,
                              38400,
                              19200,
                              9600
                            ],
                            "blacklistedBaudrates": [],
                            "blacklistedPorts": [],
                            "blockWhileDwelling": false,
                            "blockedCommands": [
                              "M0",
                              "M1"
                            ],
                            "capAutoreportPos": true,
                            "capAutoreportSdStatus": true,
                            "capAutoreportTemp": true,
                            "capBusyProtocol": true,
                            "capEmergencyParser": true,
                            "capExtendedM20": true,
                            "capLfnWrite": true,
                            "checksumRequiringCommands": [
                              "M110"
                            ],
                            "disableSdPrintingDetection": false,
                            "disconnectOnErrors": true,
                            "emergencyCommands": [
                              "M112",
                              "M108",
                              "M410"
                            ],
                            "enableShutdownActionCommand": false,
                            "encoding": "ascii",
                            "exclusive": true,
                            "externalHeatupDetection": true,
                            "firmwareDetection": true,
                            "helloCommand": "M110 N0",
                            "ignoreEmptyPorts": false,
                            "ignoreErrorsFromFirmware": false,
                            "ignoreIdenticalResends": false,
                            "ignoredCommands": [],
                            "log": false,
                            "logPositionOnCancel": false,
                            "logPositionOnPause": true,
                            "longRunningCommands": [
                              "G4",
                              "G28",
                              "G29",
                              "G30",
                              "G32",
                              "M400",
                              "M226",
                              "M600"
                            ],
                            "lowLatency": false,
                            "maxTimeoutsIdle": 2,
                            "maxTimeoutsLong": 5,
                            "maxTimeoutsPrinting": 5,
                            "neverSendChecksum": false,
                            "notifySuppressedCommands": "warn",
                            "pausingCommands": [
                              "M0",
                              "M1",
                              "M25"
                            ],
                            "port": null,
                            "portOptions": [],
                            "repetierTargetTemp": false,
                            "resendRatioStart": 100,
                            "resendRatioThreshold": 10,
                            "sanityCheckTools": true,
                            "sdAlwaysAvailable": false,
                            "sdCancelCommand": "M25",
                            "sdLowerCase": false,
                            "sdRelativePath": false,
                            "sendChecksumWithUnknownCommands": false,
                            "sendM112OnError": true,
                            "supportResendsWithoutOk": "detect",
                            "swallowOkAfterResend": true,
                            "timeoutBaudrateDetectionPause": 1.0,
                            "timeoutCommunication": 30.0,
                            "timeoutCommunicationBusy": 3.0,
                            "timeoutConnection": 10.0,
                            "timeoutDetectionConsecutive": 2.0,
                            "timeoutDetectionFirst": 10.0,
                            "timeoutPosAutoreport": 5.0,
                            "timeoutPositionLogWait": 10.0,
                            "timeoutSdStatus": 1.0,
                            "timeoutSdStatusAutoreport": 1.0,
                            "timeoutTemperature": 5.0,
                            "timeoutTemperatureAutoreport": 2.0,
                            "timeoutTemperatureTargetSet": 2.0,
                            "triggerOkForM29": true,
                            "unknownCommandsNeedAck": false,
                            "useParityWorkaround": "detect",
                            "waitForStart": false,
                            "waitToLoadSdFileList": false
                          },
                          "server": {
                            "allowFraming": false,
                            "commands": {
                              "serverRestartCommand": "sudo service octoprint restart",
                              "systemRestartCommand": "sudo shutdown -r now",
                              "systemShutdownCommand": "sudo shutdown -h now"
                            },
                            "diskspace": {
                              "critical": 209715200,
                              "warning": 524288000
                            },
                            "onlineCheck": {
                              "enabled": true,
                              "host": "1.1.1.1",
                              "interval": 15,
                              "name": "octoprint.org",
                              "port": 53
                            },
                            "pluginBlacklist": {
                              "enabled": true,
                              "ttl": 15,
                              "url": "https://plugins.octoprint.org/blacklist.json"
                            }
                          },
                          "slicing": {
                            "defaultSlicer": null
                          },
                          "system": {
                            "actions": [],
                            "events": null
                          },
                          "temperature": {
                            "cutoff": 30,
                            "profiles": [
                              {
                                "bed": 100,
                                "chamber": null,
                                "extruder": 210,
                                "name": "ABS"
                              },
                              {
                                "bed": 60,
                                "chamber": null,
                                "extruder": 180,
                                "name": "PLA"
                              }
                            ],
                            "sendAutomatically": false,
                            "sendAutomaticallyAfter": 1
                          },
                          "terminalFilters": [
                            {
                              "name": "Suppress temperature messages",
                              "regex": "regex"
                            },
                            {
                              "name": "Suppress SD status messages",
                              "regex": "regex"
                            },
                            {
                              "name": "Suppress position messages",
                              "regex": "regex"
                            },
                            {
                              "name": "Suppress wait responses",
                              "regex": "regex"
                            },
                            {
                              "name": "Suppress processing responses",
                              "regex": "regex"
                            }
                          ],
                          "webcam": {
                            "bitrate": "10000k",
                            "cacheBuster": false,
                            "defaultWebcam": "classic",
                            "ffmpegCommandline": "{ffmpeg} -framerate {fps} -i \"{input}\" -vcodec {videocodec} -threads {threads} -b:v {bitrate} -f {containerformat} -y {filters} \"{output}\"",
                            "ffmpegPath": "/usr/bin/ffmpeg",
                            "ffmpegThreads": 1,
                            "ffmpegVideoCodec": "libx264",
                            "flipH": false,
                            "flipV": false,
                            "rotate90": false,
                            "snapshotSslValidation": true,
                            "snapshotTimeout": 5,
                            "snapshotUrl": "http://127.0.0.1:8080/?action=snapshot",
                            "snapshotWebcam": "classic",
                            "streamRatio": "16:9",
                            "streamTimeout": 5,
                            "streamUrl": "/webcam/?action=stream",
                            "streamWebrtcIceServers": [
                              "stun:stun.l.google.com:19302"
                            ],
                            "timelapseEnabled": true,
                            "watermark": true,
                            "webcamEnabled": true,
                            "webcams": [
                              {
                                "canSnapshot": true,
                                "compat": {
                                  "cacheBuster": false,
                                  "snapshot": "http://127.0.0.1:8080/?action=snapshot",
                                  "snapshotSslValidation": true,
                                  "snapshotTimeout": 5,
                                  "stream": "/webcam/?action=stream",
                                  "streamRatio": "16:9",
                                  "streamTimeout": 5,
                                  "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                  ]
                                },
                                "displayName": "Classic Webcam",
                                "extras": {
                                  "cacheBuster": false,
                                  "stream": "/webcam/?action=stream",
                                  "streamRatio": "16:9",
                                  "streamTimeout": 5,
                                  "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                  ]
                                },
                                "flipH": false,
                                "flipV": false,
                                "name": "classic",
                                "provider": "classicwebcam",
                                "rotate90": false,
                                "snapshotDisplay": "http://127.0.0.1:8080/?action=snapshot"
                              },
                              {
                                "canSnapshot": true,
                                "compat": {
                                  "cacheBuster": false,
                                  "snapshot": "/webcam/snapshot",
                                  "snapshotSslValidation": true,
                                  "snapshotTimeout": 5,
                                  "stream": "/webcam/stream",
                                  "streamRatio": "16:9",
                                  "streamTimeout": 5,
                                  "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                  ]
                                },
                                "displayName": "Camera Streamer",
                                "extras": {
                                    "mjpg": {
                                        "cacheBuster": false
                                    },
                                    "mode": "webrtc",
                                    "url": "/webcam/webrtc",
                                    "webrtc": {
                                        "stun": "stun:stun.l.google.com:19302"
                                    }
                                },
                                "flipH": false,
                                "flipV": false,
                                "name": "camerastreamer_control",
                                "provider": "camerastreamer_control",
                                "rotate90": false,
                                "snapshotDisplay": "http://127.0.0.1/webcam/snapshot"
                              }
                            ]
                          }
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoSettingsApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSettings()
        //endregion
        //region THEN
        assertEquals(
            expected = Settings(
                hash = etag,
                webcam = WebcamSettings(
                    webcamEnabled = true,
                    webcams = listOf(
                        WebcamSettings.Webcam(
                            provider = "classicwebcam",
                            streamUrl = "/webcam/?action=stream",
                            flipH = false,
                            flipV = false,
                            rotate90 = false,
                            streamRatio = "16:9",
                            snapshotUrl = "http://127.0.0.1:8080/?action=snapshot",
                            extras = WebcamSettings.Webcam.Extras.GenericExtras(
                                JsonObject(
                                    mapOf(
                                        "cacheBuster" to JsonPrimitive(false),
                                        "stream" to JsonPrimitive("/webcam/?action=stream"),
                                        "streamRatio" to JsonPrimitive("16:9"),
                                        "streamTimeout" to JsonPrimitive(5),
                                        "streamWebrtcIceServers" to JsonArray(
                                            listOf(
                                                JsonPrimitive("stun:stun.l.google.com:19302")
                                            )
                                        )
                                    )
                                )
                            ),
                            snapshotDisplay = "http://127.0.0.1:8080/?action=snapshot",
                            canSnapshot = true,
                            displayName = "Classic Webcam",
                            name = "classic",
                            type = WebcamSettings.Webcam.Type.Mjpeg,
                        ),
                        WebcamSettings.Webcam(
                            provider = "camerastreamer_control",
                            streamUrl = "/webcam/webrtc",
                            flipH = false,
                            flipV = false,
                            rotate90 = false,
                            streamRatio = "16:9",
                            snapshotUrl = "/webcam/snapshot",
                            extras = WebcamSettings.Webcam.Extras.WebRtcExtras(
                                stunServer = "stun:stun.l.google.com:19302",
                                turnServer = null
                            ),
                            type = WebcamSettings.Webcam.Type.WebRtcCameraStreamer,
                            snapshotDisplay = "http://127.0.0.1/webcam/snapshot",
                            canSnapshot = true,
                            displayName = "Camera Streamer",
                            name = "camerastreamer_control",
                        ),
                        WebcamSettings.Webcam(
                            provider = "camerastreamer_control",
                            streamUrl = "/webcam/stream",
                            flipH = false,
                            flipV = false,
                            rotate90 = false,
                            streamRatio = "16:9",
                            snapshotUrl = "/webcam/snapshot",
                            extras = WebcamSettings.Webcam.Extras.GenericExtras(Json.decodeFromString("{\"mjpg\":{\"cacheBuster\":false},\"mode\":\"webrtc\",\"url\":\"/webcam/webrtc\",\"webrtc\":{\"stun\":\"stun:stun.l.google.com:19302\"}}")),
                            type = WebcamSettings.Webcam.Type.Mjpeg,
                            snapshotDisplay = "http://127.0.0.1/webcam/snapshot",
                            canSnapshot = true,
                            displayName = "Camera Streamer",
                            name = "camerastreamer_control",
                        )
                    )
                ),
                plugins = Settings.PluginSettingsGroup(
                    gcodeViewer = Settings.GcodeViewer(
                        sizeThreshold = 20971520,
                        mobileSizeThreshold = 2097152,
                    ),
                    discovery = Settings.Discovery(
                        uuid = "7c040da3-21e9-4a0d-9060-4f0d4fcad8a3"
                    ),
                    bedLevelVisualizer = Settings.BedLevelVisualizer
                ),
                temperaturePresets = listOf(
                    Settings.TemperaturePreset(
                        name = "ABS",
                        components = listOf(
                            Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 100f, isBed = true),
                            Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 210f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 210f, isExtruder = true),
                        ),
                    ),
                    Settings.TemperaturePreset(
                        name = "PLA",
                        components = listOf(
                            Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 60f, isBed = true),
                            Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 180f, isExtruder = true),
                            Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 180f, isExtruder = true),
                        ),
                    ),
                ),
                coolDownProfile = Settings.TemperaturePreset(
                    name = "__cooldown",
                    components = listOf(
                        Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 0f, isBed = true),
                        Settings.TemperaturePreset.Component(key = "chamber", displayName = "chamber", temperature = 0f, isChamber = true),
                        Settings.TemperaturePreset.Component(key = "tool0", displayName = "#1", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool1", displayName = "#2", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool2", displayName = "#3", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool3", displayName = "#4", temperature = 0f, isExtruder = true),
                        Settings.TemperaturePreset.Component(key = "tool4", displayName = "#5", temperature = 0f, isExtruder = true),
                    ),
                ),
                terminalFilters = listOf(
                    Settings.TerminalFilter(name = "Suppress temperature messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress SD status messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress position messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress wait responses", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress processing responses", regex = "regex")
                ),
                appearance = Settings.Appearance(
                    name = "",
                    colors = Settings.Appearance.Colors(
                        themeName = "default",
                        light = Settings.Appearance.ColorScheme(main = HexColor("#27AE60"), accent = HexColor("#D0F3CC")),
                        dark = Settings.Appearance.ColorScheme(main = HexColor("#106101"), accent = HexColor("#D0F3CC")),
                    ),
                ),
                bedMesh = Settings.BedMesh(
                    meshX = listOf(0f, 78f, 157f, 235f),
                    meshY = listOf(0f, 78f, 157f, 235f),
                    mesh = listOf(
                        listOf(+0.020f, -0.010f, +0.017f, +0.012f),
                        listOf(+0.042f, -0.005f, +0.052f, +0.045f),
                        listOf(+0.057f, -0.020f, +0.025f, +0.035f),
                        listOf(+0.010f, -0.063f, -0.045f, -0.010f),
                    ),
                    refreshCommand = "command\nnew line\ttab",
                    graphZMin = -0.2f,
                    graphZMax = 0.2f,
                    colorScale = Settings.BedMesh.DefaultColorScale,
                )
            ).toString(),
            actual = response.toString(),
            message = "Expected response to match",
        )
        //endregion
    }

    @Test
    fun WHEN_setting_hash_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val etag = "etag1234"
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertHead()
                assertEquals(
                    expected = Url("http://gstatic.com/api/settings"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf(
                        "Content-Type" to listOf("application/json"),
                        "Etag" to listOf(etag)
                    ),
                    content = "",
                )
            }
        )
        val target = OctoSettingsApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSettingsHash()
        //endregion
        //region THEN
        assertEquals(expected = etag, actual = response)
        //endregion
    }

    @Test
    fun WHEN_settings_are_loaded_on_OctoPrint_of_maker_gear_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val etag = "etag1234"
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/settings"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf(
                        "Content-Type" to listOf("application/json"),
                        "Etag" to listOf(etag)
                    ),
                    //region content = ...
                    content = """
                      {
                          "api": {
                            "allowCrossOrigin": false,
                            "key": null
                          },
                          "appearance": {
                            "closeModalsWithClick": true,
                            "color": "default",
                            "colorIcon": true,
                            "colorTransparent": false,
                            "defaultLanguage": "_default",
                            "fuzzyTimes": true,
                            "name": [
                              "MakerGear U1Printer4Z049"
                            ],
                            "showFahrenheitAlso": false,
                            "showInternalFilename": true
                          },
                          "devel": {
                            "pluginTimings": false
                          },
                          "feature": {
                            "autoUppercaseBlacklist": [
                              "M117",
                              "M118"
                            ],
                            "g90InfluencesExtruder": false,
                            "keyboardControl": true,
                            "modelSizeDetection": true,
                            "pollWatched": false,
                            "printCancelConfirmation": true,
                            "printStartConfirmation": false,
                            "rememberFileFolder": false,
                            "sdSupport": true,
                            "temperatureGraph": true,
                            "uploadOverwriteConfirmation": true
                          },
                          "folder": {
                            "timelapse": "/home/pi/.octoprint/timelapse",
                            "uploads": "/home/pi/.octoprint/uploads",
                            "watched": "/home/pi/.octoprint/watched"
                          },
                          "gcodeAnalysis": {
                            "bedZ": 0.0,
                            "runAt": "idle"
                          },
                          "plugins": {
                            "HeaterTimeout": {
                              "enabled": false,
                              "interval": 15,
                              "notifications": true,
                              "timeout": 600
                            },
                            "action_command_notification": {
                              "enable": true,
                              "enable_popups": false
                            },
                            "action_command_prompt": {
                              "command": "M876",
                              "enable": "detected",
                              "enable_emergency_sending": true,
                              "enable_signal_support": true
                            },
                            "announcements": {
                              "channel_order": [
                                "_important",
                                "_releases",
                                "_blog",
                                "_plugins",
                                "_octopi"
                              ],
                              "channels": {
                                "_blog": {
                                  "description": "Development news, community spotlights, OctoPrint On Air episodes and more from the official OctoBlog.",
                                  "name": "On the OctoBlog",
                                  "priority": 2,
                                  "read_until": 1643641200,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/octoblog.xml"
                                },
                                "_important": {
                                  "description": "Important announcements about OctoPrint.",
                                  "name": "Important Announcements",
                                  "priority": 1,
                                  "read_until": 1698310200,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/important.xml"
                                },
                                "_octopi": {
                                  "description": "News around OctoPi, the Raspberry Pi image including OctoPrint.",
                                  "name": "OctoPi News",
                                  "priority": 2,
                                  "read_until": 1611568800,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/octopi.xml"
                                },
                                "_plugins": {
                                  "description": "Announcements of new plugins released on the official Plugin Repository.",
                                  "name": "New Plugins in the Repository",
                                  "priority": 2,
                                  "read_until": 1644818400,
                                  "type": "rss",
                                  "url": "https://plugins.octoprint.org/feed.xml"
                                },
                                "_releases": {
                                  "description": "Announcements of new releases and release candidates of OctoPrint.",
                                  "name": "Release Announcements",
                                  "priority": 2,
                                  "read_until": 1642677300,
                                  "type": "rss",
                                  "url": "https://octoprint.org/feeds/releases.xml"
                                }
                              },
                              "display_limit": 3,
                              "enabled_channels": [
                                "_important"
                              ],
                              "forced_channels": [
                                "_important"
                              ],
                              "summary_limit": 300,
                              "ttl": 360
                            },
                            "backup": {
                              "restore_unsupported": false
                            },
                            "cancelobject": {
                              "aftergcode": null,
                              "allowed": "",
                              "beforegcode": null,
                              "ignored": "ENDGCODE,STARTGCODE",
                              "markers": true,
                              "object_regex": [
                                {
                                  "objreg": "; process (.*)"
                                },
                                {
                                  "objreg": ";MESH:(.*)"
                                },
                                {
                                  "objreg": "; printing object (.*)"
                                },
                                {
                                  "objreg": ";PRINTING: (.*)"
                                }
                              ],
                              "reptag": "Object",
                              "shownav": true,
                              "stoptags": false
                            },
                            "classicwebcam": {
                              "cacheBuster": true,
                              "flipH": false,
                              "flipV": false,
                              "rotate90": false,
                              "snapshot": "http://127.0.0.1:8080/?action=snapshot",
                              "snapshotSslValidation": true,
                              "snapshotTimeout": 5,
                              "stream": "/webcam/?action=stream",
                              "streamRatio": "16:9",
                              "streamTimeout": 5,
                              "streamWebrtcIceServers": [
                                "stun:stun.l.google.com:19302"
                              ]
                            },
                            "errortracking": {
                              "enabled": false,
                              "enabled_unreleased": false,
                              "unique_id": "b38cd6f9-ac4d-4adf-96b2-9171084a203b",
                              "url_coreui": "https://a115318be6faed605c6bfde4e183b7a0@o118517.ingest.sentry.io/1374096",
                              "url_server": "https://f723fd2a91da40dbae1148193ea49001@o118517.ingest.sentry.io/1373987"
                            },
                            "eventmanager": {
                              "availableEvents": [
                                "Startup",
                                "Shutdown",
                                "ConnectivityChanged",
                                "Connecting",
                                "Connected",
                                "Disconnecting",
                                "Disconnected",
                                "ConnectionsAutorefreshed",
                                "PrinterStateChanged",
                                "PrinterReset",
                                "ClientOpened",
                                "ClientClosed",
                                "ClientAuthed",
                                "ClientDeauthed",
                                "UserLoggedIn",
                                "UserLoggedOut",
                                "Upload",
                                "FileSelected",
                                "FileDeselected",
                                "UpdatedFiles",
                                "MetadataAnalysisStarted",
                                "MetadataAnalysisFinished",
                                "MetadataStatisticsUpdated",
                                "FileAdded",
                                "FileRemoved",
                                "FileMoved",
                                "FolderAdded",
                                "FolderRemoved",
                                "FolderMoved",
                                "TransferStarted",
                                "TransferDone",
                                "TransferFailed",
                                "PrintStarted",
                                "PrintDone",
                                "PrintFailed",
                                "PrintCancelling",
                                "PrintCancelled",
                                "PrintPaused",
                                "PrintResumed",
                                "Error",
                                "ChartMarked",
                                "PowerOn",
                                "PowerOff",
                                "Home",
                                "ZChange",
                                "Waiting",
                                "Dwelling",
                                "Cooling",
                                "Alert",
                                "Conveyor",
                                "Eject",
                                "EStop",
                                "PositionUpdate",
                                "FirmwareData",
                                "ToolChange",
                                "RegisteredMessageReceived",
                                "CommandSuppressed",
                                "InvalidToolReported",
                                "FilamentChange",
                                "CaptureStart",
                                "CaptureDone",
                                "CaptureFailed",
                                "PostRollStart",
                                "PostRollEnd",
                                "MovieRendering",
                                "MovieDone",
                                "MovieFailed",
                                "SlicingStarted",
                                "SlicingDone",
                                "SlicingFailed",
                                "SlicingCancelled",
                                "SlicingProfileAdded",
                                "SlicingProfileModified",
                                "SlicingProfileDeleted",
                                "PrinterProfileAdded",
                                "PrinterProfileModified",
                                "PrinterProfileDeleted",
                                "SettingsUpdated",
                                "plugin_backup_backup_created",
                                "plugin_firmware_check_warning",
                                "plugin_pi_support_throttle_state",
                                "plugin_pluginmanager_install_plugin",
                                "plugin_pluginmanager_uninstall_plugin",
                                "plugin_pluginmanager_enable_plugin",
                                "plugin_pluginmanager_disable_plugin",
                                "plugin_softwareupdate_update_succeeded",
                                "plugin_softwareupdate_update_failed"
                              ],
                              "subscriptions": []
                            },
                            "firmware_check": {
                              "ignore_infos": false
                            },
                            "gcodeviewer": {
                              "alwaysCompress": false,
                              "compressionSizeThreshold": 209715200,
                              "mobileSizeThreshold": 2097152,
                              "sizeThreshold": 20971520,
                              "skipUntilThis": null
                            },
                            "mglcd": {
                              "hostname": null,
                              "socket": "/var/run/netconnectd.sock",
                              "timeout": 10
                            },
                            "netconnectd": {
                              "country": null,
                              "forwardUrl": "http://find.mr-beam.org",
                              "hostname": null,
                              "socket": "/var/run/netconnectd.sock",
                              "timeout": 80
                            },
                            "octoapp": {
                              "encryptionKey": "64581fb5-ba42-4fbd-acc2-5bc8be8efdfe",
                              "version": "2.0.6"
                            },
                            "pi_support": {
                              "ignore_default_password": false,
                              "ignore_undervoltage_on_printstart": true,
                              "ignore_unrecommended_model": false,
                              "vcgencmd_throttle_check_command": null,
                              "vcgencmd_throttle_check_enabled": null
                            },
                            "pluginmanager": {
                              "confirm_disable": false,
                              "dependency_links": false,
                              "hidden": [],
                              "ignore_throttled": false,
                              "notices": "https://plugins.octoprint.org/notices.json",
                              "notices_ttl": 360,
                              "pip_args": null,
                              "pip_force_user": false,
                              "repository": "https://plugins.octoprint.org/plugins.json",
                              "repository_ttl": 1440
                            },
                            "prusaslicerthumbnails": {
                              "align_inline_thumbnail": false,
                              "filelist_height": "306",
                              "inline_thumbnail": false,
                              "inline_thumbnail_align_value": "left",
                              "inline_thumbnail_position_left": false,
                              "inline_thumbnail_scale_value": "50",
                              "installed": true,
                              "relocate_progress": false,
                              "resize_filelist": false,
                              "scale_inline_thumbnail": false,
                              "scale_inline_thumbnail_position": false,
                              "state_panel_thumbnail": true,
                              "state_panel_thumbnail_scale_value": "100",
                              "sync_on_refresh": false,
                              "use_uploads_folder": false
                            },
                            "softwareupdate": {
                              "cache_ttl": 1440,
                              "check_overlay_py2_url": "https://plugins.octoprint.org/update_check_overlay_py2.json",
                              "check_overlay_ttl": 360,
                              "check_overlay_url": "https://plugins.octoprint.org/update_check_overlay.json",
                              "credentials": {
                                "bitbucket_password_set": false,
                                "bitbucket_user_set": false,
                                "github_set": false
                              },
                              "ignore_throttled": false,
                              "minimum_free_storage": 150,
                              "notify_users": true,
                              "octoprint_branch_mappings": [
                                {
                                  "branch": "master",
                                  "commitish": [
                                    "master"
                                  ],
                                  "name": "Stable"
                                },
                                {
                                  "branch": "rc/maintenance",
                                  "commitish": [
                                    "rc/maintenance"
                                  ],
                                  "name": "Maintenance RCs"
                                },
                                {
                                  "branch": "rc/devel",
                                  "commitish": [
                                    "rc/maintenance",
                                    "rc/devel"
                                  ],
                                  "name": "Devel RCs"
                                }
                              ],
                              "octoprint_checkout_folder": null,
                              "octoprint_method": "pip",
                              "octoprint_pip_target": "https://github.com/OctoPrint/OctoPrint/archive/{target_version}.zip",
                              "octoprint_release_channel": "master",
                              "octoprint_tracked_branch": null,
                              "octoprint_type": "github_release",
                              "pip_command": null,
                              "pip_enable_check": false,
                              "queued_updates": [],
                              "updatelog_cutoff": 43200
                            },
                            "tracking": {
                              "enabled": null,
                              "events": {},
                              "ping": null,
                              "pong": 86400,
                              "server": null,
                              "unique_id": null
                            },
                            "usbfileman": {
                              "copyFileTypes": [
                                ".gcode",
                                ".gco",
                                ".g",
                                ".stl"
                              ],
                              "copyFolder": "/home/pi/.octoprint/uploads/USB",
                              "fileAction": "rename",
                              "userFeedback": "log",
                              "watchFolders": [
                                "/media/usb1/toprint",
                                "/media/usb2/toprint",
                                "/media/usb3/toprint",
                                "/media/usb4/toprint"
                              ]
                            },
                            "virtual_printer": {
                              "ambientTemperature": 21.3,
                              "brokenM29": true,
                              "brokenResend": false,
                              "busyInterval": 2.0,
                              "capabilities": {
                                "AUTOREPORT_POS": false,
                                "AUTOREPORT_SD_STATUS": true,
                                "AUTOREPORT_TEMP": true,
                                "EMERGENCY_PARSER": true,
                                "EXTENDED_M20": false,
                                "LFN_WRITE": false
                              },
                              "commandBuffer": 4,
                              "echoOnM117": true,
                              "enable_eeprom": true,
                              "enabled": false,
                              "errors": {
                                "checksum_mismatch": "Checksum mismatch",
                                "checksum_missing": "Missing checksum",
                                "command_unknown": "Unknown command {}",
                                "lineno_mismatch": "expected line {} got {}",
                                "lineno_missing": "No Line Number with checksum, Last Line: {}",
                                "maxtemp": "MAXTEMP triggered!",
                                "mintemp": "MINTEMP triggered!"
                              },
                              "firmwareName": "Virtual Marlin 1.0",
                              "forceChecksum": false,
                              "hasBed": true,
                              "hasChamber": false,
                              "includeCurrentToolInTemps": true,
                              "includeFilenameInOpened": true,
                              "klipperTemperatureReporting": false,
                              "locked": false,
                              "m105NoTargetFormatString": "{heater}:{actual:.2f}",
                              "m105TargetFormatString": "{heater}:{actual:.2f}/ {target:.2f}",
                              "m114FormatString": "X:{x} Y:{y} Z:{z} E:{e[current]} Count: A:{a} B:{b} C:{c}",
                              "m115FormatString": "FIRMWARE_NAME:{firmware_name} PROTOCOL_VERSION:1.0",
                              "m115ReportCapabilities": true,
                              "numExtruders": 1,
                              "okAfterResend": false,
                              "okBeforeCommandOutput": false,
                              "okFormatString": "ok",
                              "passcode": "1234",
                              "pinnedExtruders": null,
                              "preparedOks": [],
                              "repetierStyleTargetTemperature": false,
                              "reprapfwM114": false,
                              "resend_ratio": 0,
                              "resetLines": [
                                "start",
                                "Marlin: Virtual Marlin!",
                                "\u0080",
                                "SD card ok"
                              ],
                              "rxBuffer": 64,
                              "sdFiles": {
                                "longname": false,
                                "longname_quoted": true,
                                "size": true
                              },
                              "sendBusy": false,
                              "sendWait": true,
                              "sharedNozzle": false,
                              "simulateReset": true,
                              "smoothieTemperatureReporting": false,
                              "supportF": false,
                              "supportM112": true,
                              "support_M503": true,
                              "throttle": 0.01,
                              "waitInterval": 1.0
                            }
                          },
                          "scripts": {
                            
                          },
                          "serial": {
                            "abortHeatupOnCancel": true,
                            "ackMax": 1,
                            "additionalBaudrates": [],
                            "additionalPorts": [],
                            "alwaysSendChecksum": false,
                            "autoconnect": true,
                            "baudrate": 115200,
                            "baudrateOptions": [
                              250000,
                              230400,
                              115200,
                              57600,
                              38400,
                              19200,
                              9600
                            ],
                            "blacklistedBaudrates": [],
                            "blacklistedPorts": [],
                            "blockWhileDwelling": false,
                            "blockedCommands": [
                              "M1"
                            ],
                            "capAutoreportPos": true,
                            "capAutoreportSdStatus": true,
                            "capAutoreportTemp": true,
                            "capBusyProtocol": true,
                            "capEmergencyParser": true,
                            "capExtendedM20": true,
                            "capLfnWrite": true,
                            "checksumRequiringCommands": [
                              "M110"
                            ],
                            "disableSdPrintingDetection": false,
                            "disconnectOnErrors": true,
                            "emergencyCommands": [
                              "M0",
                              "M108",
                              "M410"
                            ],
                            "enableShutdownActionCommand": false,
                            "encoding": "ascii",
                            "exclusive": true,
                            "externalHeatupDetection": true,
                            "firmwareDetection": true,
                            "helloCommand": "M110 N0",
                            "ignoreEmptyPorts": false,
                            "ignoreErrorsFromFirmware": false,
                            "ignoreIdenticalResends": false,
                            "ignoredCommands": [],
                            "log": false,
                            "logPositionOnCancel": false,
                            "logPositionOnPause": true,
                            "longRunningCommands": [
                              "G4",
                              "G28",
                              "G29",
                              "G30",
                              "G32",
                              "M400",
                              "M226",
                              "M600"
                            ],
                            "lowLatency": false,
                            "maxTimeoutsIdle": 7,
                            "maxTimeoutsLong": 0,
                            "maxTimeoutsPrinting": 5,
                            "neverSendChecksum": false,
                            "notifySuppressedCommands": "warn",
                            "pausingCommands": [
                              "M0",
                              "M1",
                              "M25"
                            ],
                            "port": "/dev/ttyACM0",
                            "portOptions": [
                              "/dev/ttyACM0",
                              "/dev/ttyS0",
                              "/dev/ttyUSB0"
                            ],
                            "repetierTargetTemp": false,
                            "resendRatioStart": 100,
                            "resendRatioThreshold": 10,
                            "sanityCheckTools": true,
                            "sdAlwaysAvailable": false,
                            "sdCancelCommand": "M25",
                            "sdLowerCase": false,
                            "sdRelativePath": false,
                            "sendChecksumWithUnknownCommands": false,
                            "sendM112OnError": true,
                            "supportResendsWithoutOk": "detect",
                            "swallowOkAfterResend": true,
                            "timeoutBaudrateDetectionPause": 1.0,
                            "timeoutCommunication": 30.0,
                            "timeoutCommunicationBusy": 3.0,
                            "timeoutConnection": 10.0,
                            "timeoutDetectionConsecutive": 2.0,
                            "timeoutDetectionFirst": 10.0,
                            "timeoutPosAutoreport": 5.0,
                            "timeoutPositionLogWait": 20.0,
                            "timeoutSdStatus": 1.0,
                            "timeoutSdStatusAutoreport": 1.0,
                            "timeoutTemperature": 5.0,
                            "timeoutTemperatureAutoreport": 2.0,
                            "timeoutTemperatureTargetSet": 2.0,
                            "triggerOkForM29": true,
                            "unknownCommandsNeedAck": false,
                            "useParityWorkaround": "never",
                            "waitForStart": false,
                            "waitToLoadSdFileList": true
                          },
                          "server": {
                            "allowFraming": true,
                            "commands": {
                              "serverRestartCommand": "sudo service octoprint restart",
                              "systemRestartCommand": "sudo shutdown -r now",
                              "systemShutdownCommand": "sudo shutdown -h now"
                            },
                            "diskspace": {
                              "critical": 209715200,
                              "warning": 524288000
                            },
                            "onlineCheck": {
                              "enabled": true,
                              "host": "1.1.1.1",
                              "interval": 15,
                              "name": "octoprint.org",
                              "port": 53
                            },
                            "pluginBlacklist": {
                              "enabled": true,
                              "ttl": 15,
                              "url": "https://plugins.octoprint.org/blacklist.json"
                            }
                          },
                          "slicing": {
                            "defaultSlicer": null
                          },
                          "system": {
                            "actions": [],
                            "events": null
                          },
                          "temperature": {
                            "cutoff": 30,
                            "profiles": [
                              {
                                "bed": 80,
                                "chamber": null,
                                "extruder": 230,
                                "name": "ABS"
                              },
                              {
                                "bed": 55,
                                "chamber": null,
                                "extruder": 210,
                                "name": "PLA"
                              }
                            ],
                            "sendAutomatically": true,
                            "sendAutomaticallyAfter": 1
                          },
                          "webcam": {
                            "bitrate": "10000k",
                            "cacheBuster": true,
                            "defaultWebcam": "classic",
                            "ffmpegCommandline": "{ffmpeg} -framerate {fps} -i \"{input}\" -vcodec {videocodec} -threads {threads} -b:v {bitrate} -f {containerformat} -y {filters} \"{output}\"",
                            "ffmpegPath": "/usr/bin/ffmpeg",
                            "ffmpegThreads": 1,
                            "ffmpegVideoCodec": "mpeg2video",
                            "flipH": false,
                            "flipV": false,
                            "rotate90": false,
                            "snapshotSslValidation": true,
                            "snapshotTimeout": 5,
                            "snapshotUrl": "http://127.0.0.1:8080/?action=snapshot",
                            "snapshotWebcam": "classic",
                            "streamRatio": "16:9",
                            "streamTimeout": 5,
                            "streamUrl": "/webcam/?action=stream",
                            "streamWebrtcIceServers": [
                              "stun:stun.l.google.com:19302"
                            ],
                            "timelapseEnabled": true,
                            "watermark": true,
                            "webcamEnabled": true,
                            "webcams": [
                              {
                                "canSnapshot": true,
                                "compat": {
                                  "cacheBuster": true,
                                  "snapshot": "http://127.0.0.1:8080/?action=snapshot",
                                  "snapshotSslValidation": true,
                                  "snapshotTimeout": 5,
                                  "stream": "/webcam/?action=stream",
                                  "streamRatio": "16:9",
                                  "streamTimeout": 5,
                                  "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                  ]
                                },
                                "displayName": "Classic Webcam",
                                "extras": {
                                  "cacheBuster": true,
                                  "stream": "/webcam/?action=stream",
                                  "streamRatio": "16:9",
                                  "streamTimeout": 5,
                                  "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                  ]
                                },
                                "flipH": false,
                                "flipV": false,
                                "name": "classic",
                                "provider": "classicwebcam",
                                "rotate90": false,
                                "snapshotDisplay": "http://127.0.0.1:8080/?action=snapshot"
                              }
                            ]
                          }
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoSettingsApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSettings()
        //endregion
        //region THEN
        assertEquals(
            expected = Settings(
                webcam = WebcamSettings(
                    webcams = listOf(
                        WebcamSettings.Webcam(
                            name = "classic",
                            provider = "classicwebcam",
                            displayName = "Classic Webcam",
                            streamUrl = "/webcam/?action=stream",
                            flipH = false,
                            flipV = false,
                            rotate90 = false,
                            streamRatio = "16:9",
                            snapshotUrl = "http://127.0.0.1:8080/?action=snapshot",
                            canSnapshot = true,
                            snapshotDisplay = "http://127.0.0.1:8080/?action=snapshot",
                            type = WebcamSettings.Webcam.Type.Mjpeg,
                            extras = WebcamSettings.Webcam.Extras.GenericExtras(Json.decodeFromString("{\"cacheBuster\":true,\"stream\":\"/webcam/?action=stream\",\"streamRatio\":\"16:9\",\"streamTimeout\":5,\"streamWebrtcIceServers\":[\"stun:stun.l.google.com:19302\"]}")),
                        ),
                    ),
                ),
                plugins = Settings.PluginSettingsGroup(
                    gcodeViewer = Settings.GcodeViewer(
                        mobileSizeThreshold = 2097152,
                        sizeThreshold = 20971520
                    ),
                    octoAppCompanion = Settings.OctoAppCompanion(
                        encryptionKey = "64581fb5-ba42-4fbd-acc2-5bc8be8efdfe",
                        running = true,
                        version = "2.0.6",
                        serviceName = null
                    ),
                    cancelObject = Settings.CancelObject,
                ),
                temperaturePresets = listOf(
                    Settings.TemperaturePreset(
                        name = "ABS",
                        gcode = null,
                        components = listOf(
                            Settings.TemperaturePreset.Component(
                                key = "bed",
                                displayName = "bed",
                                temperature = 80.0f,
                                isBed = true,
                                isChamber = false,
                                isExtruder = false
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool0",
                                displayName = "#1",
                                temperature = 230.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool1",
                                displayName = "#2",
                                temperature = 230.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool2",
                                displayName = "#3",
                                temperature = 230.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool3",
                                displayName = "#4",
                                temperature = 230.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool4",
                                displayName = "#5",
                                temperature = 230.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                        )
                    ),
                    Settings.TemperaturePreset(
                        name = "PLA",
                        gcode = null,
                        components = listOf(
                            Settings.TemperaturePreset.Component(
                                key = "bed",
                                displayName = "bed",
                                temperature = 55.0f,
                                isBed = true,
                                isChamber = false,
                                isExtruder = false
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool0",
                                displayName = "#1",
                                temperature = 210.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool1",
                                displayName = "#2",
                                temperature = 210.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool2",
                                displayName = "#3",
                                temperature = 210.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool3",
                                displayName = "#4",
                                temperature = 210.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                            Settings.TemperaturePreset.Component(
                                key = "tool4",
                                displayName = "#5",
                                temperature = 210.0f,
                                isBed = false,
                                isChamber = false,
                                isExtruder = true
                            ),
                        )
                    )
                ),
                coolDownProfile = Settings.TemperaturePreset(
                    name = "__cooldown",
                    gcode = null,
                    components = listOf(
                        Settings.TemperaturePreset.Component(key = "bed", displayName = "bed", temperature = 0.0f, isBed = true, isChamber = false, isExtruder = false),
                        Settings.TemperaturePreset.Component(
                            key = "chamber",
                            displayName = "chamber",
                            temperature = 0.0f,
                            isBed = false,
                            isChamber = true,
                            isExtruder = false
                        ),
                        Settings.TemperaturePreset.Component(
                            key = "tool0",
                            displayName = "#1",
                            temperature = 0.0f,
                            isBed = false,
                            isChamber = false,
                            isExtruder = true
                        ),
                        Settings.TemperaturePreset.Component(
                            key = "tool1",
                            displayName = "#2",
                            temperature = 0.0f,
                            isBed = false,
                            isChamber = false,
                            isExtruder = true
                        ),
                        Settings.TemperaturePreset.Component(
                            key = "tool2",
                            displayName = "#3",
                            temperature = 0.0f,
                            isBed = false,
                            isChamber = false,
                            isExtruder = true
                        ),
                        Settings.TemperaturePreset.Component(
                            key = "tool3",
                            displayName = "#4",
                            temperature = 0.0f,
                            isBed = false,
                            isChamber = false,
                            isExtruder = true
                        ),
                        Settings.TemperaturePreset.Component(
                            key = "tool4",
                            displayName = "#5",
                            temperature = 0.0f,
                            isBed = false,
                            isChamber = false,
                            isExtruder = true
                        ),
                    )
                ),
                appearance = Settings.Appearance(
                    name = "MakerGear U1Printer4Z049",
                    colors = Settings.Appearance.Colors(
                        themeName = "default",
                        light = Settings.Appearance.ColorScheme(main = HexColor("#27AE60"), accent = HexColor("#D0F3CC")),
                        dark = Settings.Appearance.ColorScheme(main = HexColor("#106101"), accent = HexColor("#D0F3CC"))
                    )
                ),
                printerObjects = emptyList(),
                hash = "etag1234",
                babyStepIntervals = listOf(0.005f, 0.01f, 0.025f, 0.05f),
            ),
            actual = response,
            message = "Expected response to match",
        )
        //endregion
    }
}