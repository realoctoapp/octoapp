package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.framework.installApiKeyInterceptor
import io.ktor.client.HttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.request.get
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoApiKeyInterceptorTest {

    @Test
    fun WHEN_a_request_is_made_THEN_the_api_key_is_added() = runBlocking {
        //region GIVEN
        val key = "abc"
        val http = HttpClient(MockEngine {
            assertEquals(
                actual = it.headers.get("X-Api-Key"),
                expected = key,
                message = "Expected API key to match"
            )
            respondOk()
        }).also {
            it.installApiKeyInterceptor(apiKey = key)
        }
        //endregion
        //region WHEN
        val status = http.get("http://something").status.value
        //endregion
        //region THEN
        assertEquals(
            expected = 200,
            actual = status,
            message = "Expected 200"
        )
        //endregion
    }
}