package de.crysxd.octoapp.engine.octoprint.api.plugins.remote

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.ngrok.NgrokConfig
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class NgrokApiTest {

    @Test
    fun WHEN_tunnel_config_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/ngrok"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "tunnel": "xxxx-xxx-xx-xxx-xxx.ap.ngrok.io"
                        }
                    """.trimIndent()
                )
            }
        )
        val target = NgrokApi(rotator, http)
        //endregion
        //region WHEN
        val tunnel = target.getActiveTunnel()
        //endregion
        //region THEN
        assertEquals(
            expected = NgrokConfig("xxxx-xxx-xx-xxx-xxx.ap.ngrok.io"),
            actual = tunnel,
            message = "Expected response to match"
        )
        //endregion
    }
}