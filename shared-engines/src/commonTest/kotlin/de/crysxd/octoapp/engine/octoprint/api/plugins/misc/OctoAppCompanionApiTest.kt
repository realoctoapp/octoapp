package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.AppRegistration
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoAppCompanionApiTest {

    @Test
    fun WHEN_app_is_registered_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octoapp"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"fcmToken\":\"token\",\"fcmTokenFallback\":\"fallback\",\"instanceId\":\"1234i\",\"displayName\":\"Some phone\",\"displayDescription\":\"description\",\"model\":\"model\",\"appVersion\":\"1234\",\"appBuild\":123,\"appLanguage\":\"de\",\"expireInSecs\":1234,\"command\":\"registerForNotifications\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(status = HttpStatusCode.OK, content = "")
            }
        )
        val target = OctoPrintOctoAppCompanionApi(rotator, http)
        //endregion
        //region WHEN
        target.registerApp(
            registration = AppRegistration(
                fcmToken = "token",
                fcmTokenFallback = "fallback",
                expireInSecs = 1234,
                appBuild = 123,
                appVersion = "1234",
                appLanguage = "de",
                displayName = "Some phone",
                instanceId = "1234i",
                model = "model",
                displayDescription = "description",
                excludeNotifications = null,
            )
        )
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_printer_firmware_is_requested_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octoapp"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "FIRMWARE_NAME": "Virtual Marlin 1.0",
                            "PROTOCOL_VERSION": "1.0"
                        }
                    """.trimIndent()
                )
            }
        )
        val target = OctoPrintOctoAppCompanionApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getFirmwareInfo()
        //endregion
        //region THEN
        assertEquals(
            expected = "FIRMWARE_NAME: Virtual Marlin 1.0 PROTOCOL_VERSION: 1.0",
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_printer_firmware_is_requested_but_not_supportedTHEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octoapp"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"command\":\"getPrinterFirmware\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.BadRequest,
                    content = ""
                )
            }
        )
        val target = OctoPrintOctoAppCompanionApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getFirmwareInfo()
        //endregion
        //region THEN
        assertEquals(
            expected = null,
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_snapshot_is_requested_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octoapp"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"size\":720,\"webcamIndex\":1,\"command\":\"getWebcamSnapshot\"}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "FIRMWARE_NAME": "Virtual Marlin 1.0",
                            "PROTOCOL_VERSION": "1.0"
                        }
                    """.trimIndent()
                )
            }
        )
        val target = OctoPrintOctoAppCompanionApi(rotator, http)
        //endregion
        //region WHEN
        target.getWebcamSnapshot(
            maxSize = 720,
            webcamIndex = 1,
        )
        //endregion
        //region THEN
        Unit
        //endregion
    }
}