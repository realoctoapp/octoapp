package de.crysxd.octoapp.engine.octoprint.api.plugins.material

import de.crysxd.octoapp.engine.framework.NoopBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.mocks.assertPatch
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.HttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class FilamentManagerApiTest {

    @Test
    fun WHEN_spools_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                when (request.url.pathSegments.last()) {
                    "spools" -> {
                        assertEquals(
                            expected = Url("http://gstatic.com/plugin/filamentmanager/spools"),
                            actual = request.url,
                            message = "Expected url to match"
                        )
                        respond(
                            status = HttpStatusCode.OK,
                            headers = headersOf("Content-Type", "application/json"),
                            content = """
                                {"spools":[{"cost":20.0,"id":2,"name":"FM Fusili","profile":{"density":1.25,"diameter":1.75,"id":1,"material":"PLA","vendor":"Test"},"temp_offset":0,"used":0.0,"weight":1000.0},{"cost":20.0,"id":3,"name":"FM Fusili","profile":{"density":1.25,"diameter":1.75,"id":2,"material":"ABS","vendor":"Test"},"temp_offset":0,"used":179.40019956990523,"weight":1000.0}]}
                            """.trimIndent()
                        )
                    }

                    "selections" -> {
                        assertEquals(
                            expected = Url("http://gstatic.com/plugin/filamentmanager/selections"),
                            actual = request.url,
                            message = "Expected url to match"
                        )
                        respond(
                            status = HttpStatusCode.OK,
                            headers = headersOf("Content-Type", "application/json"),
                            content = """
                                {"selections":[{"client_id":"669d824e-01eb-11ec-a066-0242ac120002","spool":{"cost":20.0,"id":3,"name":"FM Fusili","profile":{"density":1.25,"diameter":1.75,"id":2,"material":"ABS","vendor":"Test"},"temp_offset":0,"used":179.40019956990523,"weight":1000.0},"tool":0}]}
                            """.trimIndent()
                        )
                    }

                    else -> throw IllegalStateException("No request to ${request.url} expected")
                }
            }
        )
        val target = FilamentManagerApi(rotator, http)
        //endregion
        //region WHEN
        val materials = target.getMaterials(Settings())
        //endregion
        //region THEN
        assertEquals(
            expected = listOf(
                FilamentManagerApi.Material(
                    id = UniqueId("filamentmanager", "2"),
                    displayName = "FM Fusili",
                    vendor = "Test",
                    material = "PLA",
                    color = HexColor("#00ff00"),
                    colorName = "Fusili",
                    providerDisplayName = "FilamentManager",
                    activeToolIndex = null,
                    weightGrams = 1000.0f,
                    density = null,
                ),
                FilamentManagerApi.Material(
                    id = UniqueId("filamentmanager", "3"),
                    displayName = "FM Fusili",
                    vendor = "Test",
                    material = "ABS",
                    color = HexColor("#00ff00"),
                    colorName = "Fusili",
                    providerDisplayName = "FilamentManager",
                    activeToolIndex = 0,
                    weightGrams = 820.5998f,
                    density = null,
                )
            ),
            actual = materials,
            message = "Expected response to match"
        )
        //endregion
    }

    @Test
    fun WHEN_spool_is_activated_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertPatch()
                assertEquals(
                    expected = Url("http://gstatic.com/plugin/filamentmanager/selections/1"),
                    actual = request.url,
                    message = "Expected url to match"
                )
                assertEquals(
                    expected = "{\"selection\":{\"tool\":0,\"spool\":{\"id\":\"4\"}}}",
                    actual = request.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )

                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = ""
                )
            }
        )
        val target = FilamentManagerApi(rotator, http)
        //endregion
        //region WHEN
        target.activateMaterial(UniqueId(providerId = OctoPlugins.SpoolManager, "4"), extruderComponent = "tool1")
        //endregion
        //region THEN
        //endregion
    }

    @Test
    fun WHEN_checked_if_available_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine {
                throw IllegalStateException("No request expected")
            }
        )
        val target = FilamentManagerApi(rotator, http)
        //endregion
        //region WHEN
        val available = target.isMaterialManagerAvailable(Settings(plugins = Settings.PluginSettingsGroup(filamentManager = Settings.FilamentManager)))
        //endregion
        //region THEN
        assertTrue(
            actual = available,
            message = "Expected available"
        )
        //endregion
    }

    @Test
    fun WHEN_checked_if_available_but_not_available_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = NoopBaseUrlRotator("http://gstatic.com")
        val http = createOctoPrintHttpClient(
            settings = HttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine {
                throw IllegalStateException("No request expected")
            }
        )
        val target = FilamentManagerApi(rotator, http)
        //endregion
        //region WHEN
        val available = target.isMaterialManagerAvailable(Settings())
        //endregion
        //region THEN
        assertFalse(
            actual = available,
            message = "Expected not available"
        )
        //endregion
    }
}