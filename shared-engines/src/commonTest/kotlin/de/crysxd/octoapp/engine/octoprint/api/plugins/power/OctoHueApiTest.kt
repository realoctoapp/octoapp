package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.mocks.TestApiBuilder
import de.crysxd.octoapp.engine.mocks.assertPost
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.toByteReadPacket
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OctoHueApiTest {

    @Test
    fun WHEN_devices_are_listed_with_new_plugin_version_THEN_devices_are_returned() = runBlocking {
        doDeviceListTest(pluginVersion = "0.6.0", hasFullControl = true)
    }

    @Test
    fun WHEN_devices_are_listed_with_old_plugin_version_THEN_devices_are_returned() = runBlocking {
        doDeviceListTest(pluginVersion = "0.5.0", hasFullControl = false)
    }

    @Test
    fun WHEN_devices_are_listed_with_unknown_plugin_version_THEN_devices_are_returned() = runBlocking {
        doDeviceListTest(pluginVersion = null, hasFullControl = false)
    }

    @Test
    fun WHEN_device_is_toggled_THEN_request_is_made() = doTest(
        request = "{\"command\":\"togglehue\"}",
        action = { toggle() }
    )

    @Test
    fun WHEN_device_is_turned_on_THEN_request_is_made() = doTest(
        request = "{\"command\":\"turnon\"}",
        action = { turnOn() }
    )

    @Test
    fun WHEN_device_is_turned_off_THEN_request_is_made() = doTest(
        request = "{\"command\":\"turnoff\"}",
        action = { turnOff() }
    )

    @Test
    fun WHEN_device_is_checked_and_on_THEN_request_is_made() = doTest(
        request = "{\"command\":\"getstate\"}",
        response = "{\"on\":true}",
        action = { assertEquals(expected = true, actual = isOn()) }
    )

    @Test
    fun WHEN_device_is_checked_and_off_THEN_request_is_made() = doTest(
        request = "{\"command\":\"getstate\"}",
        response = "{\"on\":false}",
        action = { assertEquals(expected = false, actual = isOn()) }
    )

    @Test
    fun WHEN_device_is_checked_and_incorrect_THEN_request_is_made() = doTest(
        request = "{\"command\":\"getstate\"}",
        response = "{}",
        action = { assertEquals(expected = null, actual = isOn()) }
    )

    private suspend fun doDeviceListTest(pluginVersion: String?, hasFullControl: Boolean) {
        //region GIVEN
        val settings = OctoSettings(
            plugins = OctoSettings.PluginSettingsGroup(
                octoHue = OctoSettings.OctoHue()
            )
        )
        val target = TestApiBuilder(
            MockEngine {
                throw IllegalStateException("No request expected")
            }
        ) { rotator, httpClient ->
            OctoHueApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        //endregion
        //region WHEN
        val devices = target.getDevices(
            settings = settings.map(),
            availablePlugins = mapOf(OctoPlugins.OctoHue to pluginVersion)
        )
        //endregion
        //region THEN
        assertEquals(
            expected = 1,
            actual = devices.size,
            message = "Expected one device"
        )
        assertEquals(
            expected = OctoHueApi.PowerDevice(owner = target, hasFullControl = hasFullControl),
            actual = devices[0],
            message = "Expected device to match"
        )
        //endregion
    }

    private fun doTest(request: String, response: String = "{}", action: suspend PowerDevice.() -> Unit) = runBlocking {
        //region GIVEN
        var requestMade = false
        val target = TestApiBuilder(
            MockEngine {
                it.assertPost()
                assertEquals(
                    expected = Url("http://gstatic.com/api/plugin/octohue"),
                    actual = it.url,
                    message = "Expected URL to match"
                )
                assertEquals(
                    expected = request,
                    actual = it.body.toByteReadPacket().readText(),
                    message = "Expected body to match"
                )
                requestMade = true
                respond(
                    content = response,
                    headers = headersOf("Content-Type" to listOf("application/json")),
                    status = HttpStatusCode.OK
                )
            }
        ) { rotator, httpClient ->
            OctoHueApi(baseUrlRotator = rotator, httpClient = httpClient)
        }
        val device = OctoHueApi.PowerDevice(
            owner = target,
            hasFullControl = true,
        )
        //endregion
        //region WHEN
        device.action()
        //endregion
        //region THEN
        assertTrue(
            actual = requestMade,
            message = "Expected request to be made"
        )
        //endregion
    }
}