package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.framework.json.EngineJson
import kotlin.test.Test
import kotlin.test.assertEquals

class MoonrakerFluiddDatabaseTest {

    @Test
    fun WHEN_json_is_parsed_THEN_model_correct() {
        //region GIVEN
        val json = """
            {
            "cameras": {
                "cameras": [
                    {
                        "id": "0fd1674f-4a97-4367-9a7d-12cb55fd3862",
                        "enabled": true,
                        "flipX": false,
                        "flipY": false,
                        "name": "Default",
                        "type": "mjpgstream",
                        "fpstarget": 30,
                        "fpsidletarget": 5,
                        "url": "/webcam?action=stream",
                        "rotate": "180"
                    },
                     {
                        "id": "0fd1674f-4a97-4367-9a7d-12cb55fd3862",
                        "enabled": true,
                        "flipX": false,
                        "flipY": false,
                        "name": "Default 2",
                        "type": "mjpgstream",
                        "fpstarget": 30,
                        "fpsidletarget": 5,
                        "url": "/webcam?action=stream",
                        "rotate": ""
                    },
                     {
                        "id": "0fd1674f-4a97-4367-9a7d-12cb55fd3862",
                        "enabled": true,
                        "flipX": false,
                        "flipY": false,
                        "name": "Default 3",
                        "type": "mjpgstream",
                        "fpstarget": 30,
                        "fpsidletarget": 5,
                        "url": "/webcam?action=stream",
                        "rotation": "270"
                    }
                ],
                "activeCamera": "all"
            },
            "charts": {},
            "console": {},
            "layout": {
                "layouts": {
                    "dashboard": {
                        "container1": [
                            {
                                "id": "printer-status-card",
                                "enabled": true,
                                "collapsed": false
                            },
                            {
                                "id": "camera-card",
                                "enabled": true,
                                "collapsed": false
                            },
                            {
                                "id": "toolhead-card",
                                "enabled": true,
                                "collapsed": false
                            },
                            {
                                "id": "macros-card",
                                "enabled": true,
                                "collapsed": false
                            },
                            {
                                "id": "outputs-card",
                                "enabled": true,
                                "collapsed": false
                            },
                            {
                                "id": "printer-limits-card",
                                "enabled": true,
                                "collapsed": false
                            },
                            {
                                "id": "retract-card",
                                "enabled": true,
                                "collapsed": false
                            }
                        ]
                    }
                }
            },
            "macros": {},
            "uiSettings": {}
        }
        """.trimIndent()
        //endregion
        //region WHEN
        val model = EngineJson.decodeFromString<MoonrakerFluiddDatabase>(json)
        //endregion
        //region THEN
        assertEquals(
            expected = MoonrakerFluiddDatabase(
                cameras = MoonrakerFluiddDatabase.Cameras(
                    cameras = listOf(
                        MoonrakerFluiddDatabase.Cameras.Camera(
                            enabled = true,
                            flipX = false,
                            flipY = false,
                            fpsidletarget = 5,
                            fpstarget = 30,
                            height = null,
                            id = "0fd1674f-4a97-4367-9a7d-12cb55fd3862",
                            name = "Default",
                            type = "mjpgstream",
                            url = "/webcam?action=stream",
                            rotation = 180
                        ),
                        MoonrakerFluiddDatabase.Cameras.Camera(
                            enabled = true,
                            flipX = false,
                            flipY = false,
                            fpsidletarget = 5,
                            fpstarget = 30,
                            height = null,
                            id = "0fd1674f-4a97-4367-9a7d-12cb55fd3862",
                            name = "Default 2",
                            type = "mjpgstream",
                            url = "/webcam?action=stream",
                            rotation = null
                        ),
                        MoonrakerFluiddDatabase.Cameras.Camera(
                            enabled = true,
                            flipX = false,
                            flipY = false,
                            fpsidletarget = 5,
                            fpstarget = 30,
                            height = null,
                            id = "0fd1674f-4a97-4367-9a7d-12cb55fd3862",
                            name = "Default 3",
                            type = "mjpgstream",
                            url = "/webcam?action=stream",
                            rotation = 270
                        )
                    )
                ),
                charts = MoonrakerFluiddDatabase.Charts,
                console = MoonrakerFluiddDatabase.Console(commandHistory = null),
                layout = MoonrakerFluiddDatabase.Layout,
                macros = MoonrakerFluiddDatabase.Macros,
                uiSettings = MoonrakerFluiddDatabase.UiSettings(
                    dashboard = null,
                    editor = null,
                    general = null,
                    theme = null
                )
            ),
            actual = model,
        )
        //endregion
    }
}