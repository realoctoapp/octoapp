package de.crysxd.octoapp.engine.octoprint.event

import kotlin.time.Duration.Companion.seconds

internal actual object OctoWebsocketConfig {
    actual val canUseLongNoMessageTimeout = false
    actual val longNoMessageTimeout = 10.seconds
    actual val shotNoMessageTimeout = 5.seconds
}