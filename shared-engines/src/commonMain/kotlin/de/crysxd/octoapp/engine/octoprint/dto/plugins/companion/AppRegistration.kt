package de.crysxd.octoapp.engine.octoprint.dto.plugins.companion

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
data class AppRegistration(
    val fcmToken: String,
    val fcmTokenFallback: String?,
    val instanceId: String,
    val displayName: String,
    val displayDescription: String?,
    val model: String,
    val appVersion: String,
    val appBuild: Long,
    val appLanguage: String,
    val expireInSecs: Long?,
    val excludeNotifications: List<String>?,
) : CommandBody {
    override val command = "registerForNotifications"
}