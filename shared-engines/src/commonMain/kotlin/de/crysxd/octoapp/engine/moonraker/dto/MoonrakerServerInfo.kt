package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerServerInfo(
    @SerialName("api_version") val apiVersion: List<Int>? = null,
    @SerialName("api_version_string") val apiVersionString: String? = null,
    @SerialName("components") val components: List<String>? = null,
    @SerialName("failed_components") val failedComponents: List<String>? = null,
    @SerialName("klippy_connected") val klippyConnected: Boolean? = null,
    @SerialName("klippy_state") val klippyState: String? = null,
    @SerialName("missing_klippy_requirements") val missingKlippyRequirements: List<String>? = null,
    @SerialName("moonraker_version") val moonrakerVersion: String? = null,
    @SerialName("registered_directories") val registeredDirectories: List<String>? = null,
    @SerialName("warnings") val warnings: List<String>? = null,
    @SerialName("websocket_count") val websocketCount: Int? = null,
)