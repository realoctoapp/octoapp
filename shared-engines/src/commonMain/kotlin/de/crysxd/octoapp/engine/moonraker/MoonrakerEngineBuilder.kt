package de.crysxd.octoapp.engine.moonraker

import de.crysxd.octoapp.engine.PrinterEngine
import de.crysxd.octoapp.engine.PrinterEngineBuilder
import de.crysxd.octoapp.engine.PrinterEngineHttpClientSettings
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.BenchmarkBaseUrlRotator
import de.crysxd.octoapp.engine.framework.ExceptionInspector
import de.crysxd.octoapp.engine.moonraker.http.createMoonrakerHttpClient
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import io.ktor.http.Url

fun MoonRakerEngineBuilder(block: MoonrakerEngineBuilder.Scope.() -> Unit): PrinterEngine {
    val scope = object : MoonrakerEngineBuilder.Scope {
        override var baseUrls: List<String> = emptyList()
        override var interpolateEvents: Boolean = false
        override var exceptionInspector: ExceptionInspector = ExceptionInspector {}
        override var httpClientSettings: HttpClientSettings? = null
        override var allowWebSocketTransport: Boolean = true
        override var apiKey: String? = null
    }

    scope.block()

    require(scope.baseUrls.isNotEmpty()) { "At least one URL required" }
    val settings = requireNotNull(scope.httpClientSettings) { "Missing HttpClientSettings" }

    val bur = BenchmarkBaseUrlRotator(baseUrls = scope.baseUrls.map { Url(it) })
    val httpSettings = MoonrakerEngineBuilder.HttpClientSettings(
        general = settings,
        exceptionInspector = scope.exceptionInspector,
        baseUrlRotator = bur,
        apiKey = scope.apiKey,
    )
    return MoonrakerEngine(
        baseUrlRotator = bur,
        httpClient = { createMoonrakerHttpClient(settings = httpSettings) },
        settings = httpSettings,
        apiKey = scope.apiKey,
        platform = SharedCommonInjector.get().platform
    )
}

class MoonrakerEngineBuilder : PrinterEngineBuilder {
    interface Scope : PrinterEngineBuilder.Scope {
        override var baseUrls: List<String>
        override var interpolateEvents: Boolean
        override var exceptionInspector: ExceptionInspector
        override var httpClientSettings: de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings?
        override var allowWebSocketTransport: Boolean
        override var apiKey: String?
    }

    internal data class HttpClientSettings(
        override val general: de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings = HttpClientSettings(logTag = Constants.MoonrakerLogTag),
        override val baseUrlRotator: BaseUrlRotator,
        override val apiKey: String?,
        override val exceptionInspector: ExceptionInspector = ExceptionInspector {},
    ) : PrinterEngineHttpClientSettings
}




