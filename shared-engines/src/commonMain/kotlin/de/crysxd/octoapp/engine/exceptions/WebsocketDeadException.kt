package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException

class WebsocketDeadException(message: String) : IllegalStateException(message + " (${instanceCounter++})"), SuppressedException {
    companion object {
        var instanceCounter = 0
    }
}
