package de.crysxd.octoapp.engine.models.connection

sealed class ConnectionQuality {
    data object Normal : ConnectionQuality()
    data class Degraded(val reason: Throwable) : ConnectionQuality()
}