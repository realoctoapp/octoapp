package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.models.timelapse.TimelapseStatus
import io.ktor.utils.io.ByteReadChannel

interface TimelapseApi {

    suspend fun updateConfig(config: TimelapseConfig): TimelapseStatus
    suspend fun getStatus(): TimelapseStatus
    suspend fun delete(timelapseFile: TimelapseFile): TimelapseStatus
    suspend fun download(timelapseFile: TimelapseFile, progressUpdate: (Float) -> Unit = {}, consume: suspend (ByteReadChannel) -> Unit)

}
