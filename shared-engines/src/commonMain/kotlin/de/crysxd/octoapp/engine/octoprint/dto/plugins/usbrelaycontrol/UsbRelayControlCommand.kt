package de.crysxd.octoapp.engine.octoprint.dto.plugins.usbrelaycontrol

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class UsbRelayControlCommand(
    override val command: String,
    val id: Int
) : CommandBody