package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerDisplayStatus(
    @SerialName("progress") val progress: Float? = null,
    @SerialName("message") val message: String? = null,
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        progress = progress ?: (previous as? MoonrakerDisplayStatus)?.progress,
        message = message ?: (previous as? MoonrakerDisplayStatus)?.message,
    )
}