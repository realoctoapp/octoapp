package de.crysxd.octoapp.engine.framework

import io.ktor.http.Url
import kotlinx.coroutines.flow.StateFlow

interface BaseUrlRotator {
    suspend fun <T> request(block: suspend (Url) -> T): T
    suspend fun considerException(baseUrl: Url, e: Throwable)

    val activeUrl: StateFlow<Url>

    fun consumeActiveFlow(
        active: StateFlow<Boolean>,
        probe: suspend (BaseUrlRotator) -> Boolean,
        onChange: () -> Unit,
    )

    suspend fun considerUpgradingConnection(trigger: String, forced: Boolean)
}
