package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerExcludeObject(
    @SerialName("current_object") val currentObject: String? = null,
    @SerialName("excluded_objects") val excludedObjects: List<String>? = null,
    @SerialName("objects") val objects: List<Object>? = null
) : MoonrakerStatusItem {

    @Serializable
    data class Object(
        @SerialName("center") val center: List<Float>? = null,
        @SerialName("name") val name: String? = null,
        @SerialName("polygon") val polygon: List<List<Float>>? = null
    )

    override fun toString() = "MoonrakerExcludeObject(currentObject=$currentObject, excludedObjects=$excludedObjects, objects=${
        objects?.let { obs ->
            "[${
                obs.joinToString { o -> o.name ?: "null" }
            }]"
        }
    })"

    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        excludedObjects = excludedObjects ?: (previous as? MoonrakerExcludeObject)?.excludedObjects,
        objects = objects ?: (previous as? MoonrakerExcludeObject)?.objects,
    )
}