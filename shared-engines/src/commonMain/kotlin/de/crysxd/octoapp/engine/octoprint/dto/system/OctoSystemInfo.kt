@file:UseSerializers(InstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.system

import de.crysxd.octoapp.engine.framework.json.InstantSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class OctoSystemInfo(
    @SerialName("systeminfo") val info: Nested
) {
    @Serializable
    data class Nested(
        @SerialName("systeminfo.generated") val generatedAt: Instant = Instant.DISTANT_PAST,
        @SerialName("printer.firmware") val printerFirmware: String? = null,
        @SerialName("octoprint.safe_mode") val safeMode: Boolean = false,
    )
}