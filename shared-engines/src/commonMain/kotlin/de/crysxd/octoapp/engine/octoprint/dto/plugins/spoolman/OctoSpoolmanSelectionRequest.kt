package de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolman


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OctoSpoolmanSelectionRequest(
    @SerialName("toolIdx") val toolIdx: Int,
    @SerialName("spoolId") val spoolId: Int?,
)