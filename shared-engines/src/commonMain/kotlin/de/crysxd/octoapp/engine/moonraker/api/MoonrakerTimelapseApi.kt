package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.TimelapseApi
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.moonraker.exception.NotSupportedOnMoonrakerException
import io.ktor.utils.io.ByteReadChannel

internal class MoonrakerTimelapseApi : TimelapseApi {
    override suspend fun updateConfig(config: TimelapseConfig) = throw NotSupportedOnMoonrakerException("timelapse/update")
    override suspend fun getStatus() = throw NotSupportedOnMoonrakerException("timelapse/status")
    override suspend fun delete(timelapseFile: TimelapseFile) = throw NotSupportedOnMoonrakerException("timelapse/delete")

    override suspend fun download(timelapseFile: TimelapseFile, progressUpdate: (Float) -> Unit, consume: suspend (ByteReadChannel) -> Unit) =
        throw NotSupportedOnMoonrakerException("timelapse/download")
}
