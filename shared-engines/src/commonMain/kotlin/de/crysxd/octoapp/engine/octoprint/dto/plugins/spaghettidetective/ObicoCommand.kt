package de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
sealed class ObicoCommand(
    override val command: String
) : CommandBody {
    @Serializable
    class GetPluginStatus : ObicoCommand("get_plugin_status")
}