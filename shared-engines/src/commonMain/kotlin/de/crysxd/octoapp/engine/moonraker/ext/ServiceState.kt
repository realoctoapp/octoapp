package de.crysxd.octoapp.engine.moonraker.ext

import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo

internal data class ServiceData(
    val name: String,
    val state: MoonrakerSystemInfo.SystemInfo.ServiceState?,
)