package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerTemperatureStore
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject

class MoonrakerTemperatureStoreSerializer : KSerializer<MoonrakerTemperatureStore> {

    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerTemperatureStore", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): MoonrakerTemperatureStore {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject
        val components = tree.mapValues { (_, value) ->
            EngineJson.decodeFromJsonElement(MoonrakerTemperatureStore.Component.serializer(), value.jsonObject)
        }
        return MoonrakerTemperatureStore(components)
    }

    override fun serialize(encoder: Encoder, value: MoonrakerTemperatureStore) = throw UnsupportedOperationException("Can't serializer")

}