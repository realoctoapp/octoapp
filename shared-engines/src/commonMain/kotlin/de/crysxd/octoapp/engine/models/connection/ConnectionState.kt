package de.crysxd.octoapp.engine.models.connection

import de.crysxd.octoapp.engine.models.printer.PrinterProfileReference

data class ConnectionState(
    val current: Connection,
    val options: Options
) {

    data class Options(
        val ports: List<String>,
        val baudrates: List<Int>,
        val printerProfiles: List<PrinterProfileReference>,
        val portPreference: String? = null,
        val baudratePreference: Int? = null,
        val printerProfilePreference: String? = null,
        val autoConnect: Boolean = false
    )
}