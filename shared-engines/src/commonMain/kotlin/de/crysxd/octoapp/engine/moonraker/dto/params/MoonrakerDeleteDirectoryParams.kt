package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerDeleteDirectoryParams(
    @SerialName("path") val path: String,
    @SerialName("force") val force: Boolean = false,
) : MoonrakerRpcParams