package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.version.VersionInfo
import de.crysxd.octoapp.engine.octoprint.dto.version.OctoVersionInfo

internal fun OctoVersionInfo.map() = VersionInfo(
    apiVersion = api,
    severVersion = server,
    serverVersionText = text
)