package de.crysxd.octoapp.engine.octoprint.dto.plugins.octohue

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoHueResponse(
    val on: Boolean? = null,
)
