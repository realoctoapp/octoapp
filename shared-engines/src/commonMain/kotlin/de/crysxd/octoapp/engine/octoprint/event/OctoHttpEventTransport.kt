package de.crysxd.octoapp.engine.octoprint.event

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.guessConnectionType
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportAuthentication
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportConfiguration
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.mappers.map
import de.crysxd.octoapp.engine.octoprint.serializer.OctoEventMessageListSerializer
import de.crysxd.octoapp.sharedcommon.http.ConnectionType
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.encodeToString
import okio.IOException
import kotlin.time.Duration.Companion.seconds

internal class OctoHttpEventTransport(
    parentJob: Job,
    parenTag: String,
    loginApi: () -> LoginApi,
    private val eventSink: OctoEventSource,
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val onCurrentReceived: (Pair<String, OctoMessage.Current>?) -> Unit,
) : OctoEventTransport(
    loginApi = loginApi,
    parentJob = parentJob,
    parenTag = parenTag,
) {

    private var reconnectCounter = 0
    override val name get() = "XHR"
    private var loadBalancerId = (100..999).random()
    var isFallbackAfterWebsocketFailure: Throwable? = null
    private var lastMessageReceivedAt = Instant.DISTANT_PAST

    override fun connect() {
        recreateScope().apply {
            val sessionId = loadBalancerId to uuid4().toString().split("-")[0]
            onCurrentReceived(null)

            launch {
                Napier.i(tag = tag, message = "Starting XHR transport session ${sessionId.first}/${sessionId.second}")
                receiveSingle(sessionId).second
                sendConfigurations(sessionId)
                receiveAll(sessionId)
                reconnectCounter = 0
            }
        }
    }

    private fun CoroutineScope.receiveAll(sessionId: Pair<Int, String>) = launch {
        var lastConnectionType: ConnectionType? = null
        while (isActive) {
            val (messages, connectionType) = receiveSingle(sessionId)

            if (lastConnectionType != connectionType) {
                lastConnectionType = connectionType
                val quality = isFallbackAfterWebsocketFailure?.let { ConnectionQuality.Degraded(it) } ?: ConnectionQuality.Normal
                eventSink.emitEvent(Event.Connected(connectionType = connectionType, connectionQuality = quality))
            }

            messages.forEach {
                if (it is OctoMessage.Current) {
                    onCurrentReceived("<unavailable>" to it)
                }

                Napier.v(tag = tag, message = "Received message ${it.toString().take(128)}")
                eventSink.emitEvent(Event.MessageReceived(it.map()))
            }


            // Simulate OctoPrint's throttle
            delay(1.seconds * config.value.throttle.throttle)
        }
    }

    private fun sendConfigurations(sessionId: Pair<Int, String>) = scope.launch {
        config.collectLatest {
            if (beforeSendingConfig()) {
                doSendConfiguration(sessionId, it)
            }
        }
    }

    private suspend fun sendConfigurationOnce(sessionId: Pair<Int, String>) {
        doSendConfiguration(sessionId, config.first())
    }

    private suspend fun doSendConfiguration(sessionId: Pair<Int, String>, config: OctoEventTransportConfiguration) {
        Napier.i(tag = tag, message = "Sending throttle: ${config.throttle}")
        send(sessionId, config.throttle.toMessageJson())
        Napier.i(tag = tag, message = "Sending subscription: ${config.subscription}")
        send(sessionId, config.subscription.toMessageJson())
    }

    private suspend fun send(sessionId: Pair<Int, String>, message: String) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "sockjs", sessionId.first.toString(), sessionId.second, "xhr_send")
            parameter("t", Clock.System.now().toEpochMilliseconds())
            setBody("[\n\"${message.replace("\"", "\\\"")}\"\n]")
        }
    }

    private suspend fun doLogIn(sessionId: Pair<Int, String>) {
        val login = logIn()
        Napier.d(tag = tag, message = "Sending auth ${login.split(":")[0]}:****")
        send(sessionId, EngineJson.encodeToString(OctoEventTransportAuthentication(login)))
        sendConfigurationOnce(sessionId)
    }

    private suspend fun receiveSingle(sessionId: Pair<Int, String>): Pair<List<OctoMessage>, ConnectionType> = baseUrlRotator.request { baseUrl ->
        httpClient.post {
            urlFromPath(baseUrl = baseUrl, "sockjs", sessionId.first.toString(), sessionId.second, "xhr")
            parameter("t", Clock.System.now().toEpochMilliseconds())
        }.let { response ->
            val text = response.bodyAsText()
            lastMessageReceivedAt = Clock.System.now()
            when {
                text.isEmpty() || response.status == HttpStatusCode.NoContent -> emptyList()
                text[0] == 'o' -> {
                    Napier.i(tag = tag, message = "Session start confirmed")
                    emptyList()
                }

                text[0] == 'a' -> parseMessage(text.drop(1)).also { messages ->
                    val connected = messages.firstNotNullOfOrNull { it as? OctoMessage.Connected }
                    if (connected != null) {
                        onConnected(connected)
                        doLogIn(sessionId)
                    } else if (messages.any { it is OctoMessage.ReAuthRequired }) {
                        logOut()
                        doLogIn(sessionId)
                    }
                }

                text[0] == 'h' -> emptyList()
                text[0] == 'c' -> emptyList()
                text.startsWith("<html>", ignoreCase = true) || text.startsWith("<!doctype html>", ignoreCase = true) -> throw IllegalHtmlResponseException()

                else -> throw IllegalStateException("Recieved illegal message: $text")
            } to response.call.guessConnectionType()
        }
    }

    private fun parseMessage(text: String): List<OctoMessage> =
        EngineJson.decodeFromString(deserializer = OctoEventMessageListSerializer(), string = text)

    override fun handleException(e: Throwable) {
        scope.launch {
            Napier.e(tag = tag, throwable = e, message = "Received error, reconnecting")

            val timeSinceLast = Clock.System.now() - lastMessageReceivedAt
            if (reconnectCounter > 1 || timeSinceLast > OctoWebsocketConfig.shotNoMessageTimeout) {
                eventSink.emitEvent(Event.Disconnected(e, connectionAttemptCounter = reconnectCounter))
            }

            delay((1.seconds * reconnectCounter).coerceAtMost(3.seconds))
            Napier.w(tag = tag, message = "Reconnecting after exception")
            reconnectCounter++
            connect()
        }
    }

    private class IllegalHtmlResponseException : IOException("Response is not a SockJS response but HTML. This is usually a setup issue.")
}