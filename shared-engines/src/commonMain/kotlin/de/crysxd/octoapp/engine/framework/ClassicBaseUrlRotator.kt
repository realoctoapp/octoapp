package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterUnavailableException
import io.github.aakira.napier.Napier
import io.ktor.client.network.sockets.SocketTimeoutException
import io.ktor.client.plugins.HttpRequestTimeoutException
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import okio.IOException
import kotlin.time.Duration.Companion.seconds

class ClassicBaseUrlRotator(private val baseUrls: List<Url>) : BaseUrlRotator {

    private val tag = "ActiveBaseUrlRotator"
    private var continuousCheckJob: Job? = null
    private var activeCollectionJob: Job? = null
    private var connectionProbe: (suspend (BaseUrlRotator) -> Boolean)? = null
    private var onConnectionChange: () -> Unit = {}
    private val job by lazy { SupervisorJob() }
    private val scope by lazy {
        CoroutineScope(job + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, throwable ->
            Napier.e(tag = "BaseUrlRotator/scope", throwable = throwable, message = "Caught NON-CONTAINED exception in scope!")
        })
    }

    private val mutex = Mutex()
    private var lastException: Throwable? = null
    private val mutableActiveUrl = MutableStateFlow(baseUrls[0])
    override val activeUrl = mutableActiveUrl.asStateFlow()
    private var activeUrlIndex: Int = 0
        set(value) {
            mutableActiveUrl.value = baseUrls[value]
            field = value
        }

    init {
        require(baseUrls.isNotEmpty()) { "Need at least one base URL, none given!" }
        Napier.d(tag = tag, message = "Creating ClassicBaseUrlRotator with selection: $baseUrls")
        Napier.i(tag = tag, message = "Initial base: ${baseUrls[0]} (${baseUrls.size - 1} alternatives)")
    }

    override suspend fun <T> request(block: suspend (Url) -> T): T {
        val baseUrlsForRequest = ((activeUrlIndex until baseUrls.size) + (0 until activeUrlIndex)).map { baseUrls[it] }
        var exception: Throwable = IllegalStateException("Should not be used")

        return baseUrlsForRequest.firstNotNullOfOrNull { baseUrl ->
            try {
                block(baseUrl)
            } catch (e: CancellationException) {
                throw e
            } catch (e: Exception) {
                if (e.canBeSolvedByBaseUrlChange()) {
                    exception = e
                    considerException(baseUrl, e)
                    null
                } else {
                    Napier.d(tag = tag, message = "Caught '${e::class.simpleName}' which can't be solved by a URL change. Escalating.")
                    throw e
                }
            }
        } ?: let {
            Napier.e("All base Urls failed. Reporting last exception")
            throw exception
        }
    }

    override suspend fun considerException(baseUrl: Url, e: Throwable) = mutex.withLock {
        if (e.canBeSolvedByBaseUrlChange()) {
            val currentBase = baseUrls[activeUrlIndex]
            if (baseUrl == currentBase && e != lastException) {
                lastException = e
                activeUrlIndex = (activeUrlIndex + 1) % baseUrls.size
                val newBase = baseUrls[activeUrlIndex]
                Napier.d(tag = tag, message = "Caught '${e::class.simpleName}', can be solved by base URL change. Re-attempting...")
                Napier.i(tag = tag, message = "Switching: $currentBase -> $newBase")
            } else {
                Napier.d(tag = tag, message = "Already switched from $baseUrl -> $currentBase")
            }
        } else {
            Napier.d(tag = tag, message = "Got '${e::class.simpleName}' reported for consideration which can't be solved by a URL change. Ignoring.")
        }
    }

    private fun Throwable.canBeSolvedByBaseUrlChange(): Boolean = if (
    // Usually it's always this one
        this is PrinterUnavailableException ||
        this is ProbeFailedException ||

        // CIO Engine
        this::class.qualifiedName == "java.nio.channels.UnresolvedAddressException" ||

        // OkHTTP Engine
        this::class.qualifiedName == "java.net.ConnectException" ||
        this::class.qualifiedName == "java.net.UnknownHostException" ||
        this::class.qualifiedName == "java.net.SocketException" ||

        // Darwin Engine
        this.message?.contains("NSURLErrorDomain") == true ||
        this.message?.contains("NSErrorFailingURLStringKey") == true ||

        // Generic
        this::class.qualifiedName == "io.ktor.client.network.sockets.ConnectTimeoutException" ||
        this::class.qualifiedName == "io.ktor.client.plugins.ConnectTimeoutException" ||
        this is HttpRequestTimeoutException ||
        this is SocketTimeoutException ||

        // Super special things that should not be needed but are
        message == "Unexpected status line: <HEAD><TITLE>Tunnel Connection Failed</TITLE></HEAD>"
    ) {
        true
    } else {
        ((this as? NetworkException)?.originalCause ?: cause)?.canBeSolvedByBaseUrlChange() == true
    }

    override suspend fun considerUpgradingConnection(trigger: String, forced: Boolean) {
        val probe = connectionProbe ?: return Napier.i(tag = tag, message = "Skipping upgrade connection check, no probe available (trigger=$trigger, forced=$forced)")

        // Test all baseUrls but last. If a baseUrl is working with an higher index than the active one we switch to it
        // We skip the last because...no point in switching to it as it will be already active if all others are offline
        baseUrls.dropLast(1).forEachIndexed { index, baseUrl ->
            try {
                val nestedRotator = NoopBaseUrlRotator(baseUrl)
                when {
                    !probe(nestedRotator) -> {
                        Napier.i(tag = tag, message = "$baseUrl OFFLINE")
                        if (activeUrlIndex == index) {
                            considerException(baseUrl, ProbeFailedException())
                        }
                    }

                    activeUrlIndex != index -> {
                        Napier.i("$baseUrl ONLINE")
                        activeUrlIndex = index
                        onConnectionChange()

                        // First one passed, not interested in others
                        return
                    }

                    else -> {
                        Napier.i(tag = tag, message = "$baseUrl ONLINE (already active)")

                        // First one passed, not interested in others
                        return
                    }
                }
            } catch (e: Exception) {
                Napier.i(tag = tag, message = "$baseUrl OFFLINE (${e::class.qualifiedName})")
            }
        }
    }

    override fun consumeActiveFlow(
        active: StateFlow<Boolean>,
        probe: suspend (BaseUrlRotator) -> Boolean,
        onChange: () -> Unit,
    ) {
        connectionProbe = probe
        onConnectionChange = onChange
        activeCollectionJob?.cancel()
        activeCollectionJob = scope.launch {
            active.collectLatest {
                if (it) {
                    continuousCheckJob?.cancel()
                    continuousCheckJob = starContinuousCheck()
                }
            }
        }
    }

    private fun starContinuousCheck() = scope.launch {
        delay(3.seconds)
        considerUpgradingConnection(trigger = "loop-start", forced = false)
        while (isActive) {
            delay(30.seconds)
            considerUpgradingConnection(trigger = "loop", forced = false)
        }
    }

    private class ProbeFailedException : IOException("Probe failed")
}
