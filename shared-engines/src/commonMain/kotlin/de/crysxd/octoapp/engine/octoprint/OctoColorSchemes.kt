package de.crysxd.octoapp.engine.octoprint

import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.sharedcommon.utils.HexColor

sealed interface OctoColorSchemes {
    val red: Settings.Appearance.ColorScheme
    val orange: Settings.Appearance.ColorScheme
    val yellow: Settings.Appearance.ColorScheme
    val green: Settings.Appearance.ColorScheme
    val blue: Settings.Appearance.ColorScheme
    val violet: Settings.Appearance.ColorScheme
    val white: Settings.Appearance.ColorScheme
    val black: Settings.Appearance.ColorScheme
    val default: Settings.Appearance.ColorScheme

    fun fromString(string: String) = when (string) {
        "red" -> red
        "orange" -> orange
        "yellow" -> yellow
        "green" -> green
        "blue" -> blue
        "violet" -> violet
        "white" -> white
        "black" -> black
        else -> default
    }

    data object Light : OctoColorSchemes {
        override val red = Settings.Appearance.ColorScheme(
            main = HexColor("#EE3F3F"),
            accent = HexColor("#FFE4E4"),
        )

        override val orange = Settings.Appearance.ColorScheme(
            main = HexColor("#FF9800"),
            accent = HexColor("#FFE6C1"),
        )

        override val yellow = Settings.Appearance.ColorScheme(
            main = HexColor("#FFC000"),
            accent = HexColor("#FFE4E4"),
        )

        override val green = Settings.Appearance.ColorScheme(
            main = HexColor("#13C100"),
            accent = HexColor("#CDEFDB"),
        )

        override val blue = Settings.Appearance.ColorScheme(
            main = HexColor("#104F80"),
            accent = HexColor("#BBDCF7"),
        )

        override val violet = Settings.Appearance.ColorScheme(
            main = HexColor("#BB6BD9"),
            accent = HexColor("#F6E3FD"),
        )

        override val white = Settings.Appearance.ColorScheme(
            main = HexColor("#999999"),
            accent = HexColor("#E8E7E7"),
        )

        override val black = Settings.Appearance.ColorScheme(
            main = HexColor("#000000"),
            accent = HexColor("#BCBCBC"),
        )

        override val default = Settings.Appearance.ColorScheme(
            main = HexColor("#27AE60"),
            accent = HexColor("#D0F3CC"),
        )
    }

    data object Dark : OctoColorSchemes {
        override val red = Settings.Appearance.ColorScheme(
            main = HexColor("#EE3F3F"),
            accent = HexColor("#9E5858"),
        )

        override val orange = Settings.Appearance.ColorScheme(
            main = HexColor("#FF9800"),
            accent = HexColor("#FFE6C1"),
        )

        override val yellow = Settings.Appearance.ColorScheme(
            main = HexColor("#FFC000"),
            accent = HexColor("#FFE4E4"),
        )

        override val green = Settings.Appearance.ColorScheme(
            main = HexColor("#13C100"),
            accent = HexColor("#CDEFDB"),
        )

        override val blue = Settings.Appearance.ColorScheme(
            main = HexColor("#0E4773"),
            accent = HexColor("#60AEEB"),
        )

        override val violet = Settings.Appearance.ColorScheme(
            main = HexColor("#BB6BD9"),
            accent = HexColor("#F6E3FD"),
        )

        override val white = Settings.Appearance.ColorScheme(
            main = HexColor("#656565"),
            accent = HexColor("#989898"),
        )

        override val black = Settings.Appearance.ColorScheme(
            main = HexColor("#707070"),
            accent = HexColor("#BCBCBC"),
        )

        override val default = Settings.Appearance.ColorScheme(
            main = HexColor("#106101"),
            accent = HexColor("#D0F3CC"),
        )
    }
}