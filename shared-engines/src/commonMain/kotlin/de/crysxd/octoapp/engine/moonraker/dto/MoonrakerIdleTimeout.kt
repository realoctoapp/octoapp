package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerIdleTimeout(
    @SerialName("printing_time") val printingTime: Float? = null,
    @SerialName("state") val state: String? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        printingTime = printingTime ?: (previous as? MoonrakerIdleTimeout)?.printingTime,
        state = state ?: (previous as? MoonrakerIdleTimeout)?.state,
    )
}