package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octocam.OctoCamCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octocam.OctoCamResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OctoCamApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = if (settings.plugins.octoCam != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun executeCommand(command: String) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoCam)
            setJsonBody(OctoCamCommand(command))
        }
    }

    private suspend fun turnOn() {
        executeCommand("turnOn")
    }

    private suspend fun turnOff() {
        executeCommand("turnOff")
    }

    private suspend fun toggle() {
        executeCommand("toggle")
    }

    private suspend fun isOn() = executeCommand("checkStatus").body<OctoCamResponse>().torchOn

    internal data class PowerDevice(
        private val owner: OctoCamApi,
    ) : IPowerDevice {
        override val id = "octocam-torch"
        override val pluginId = OctoPlugins.OctoCam
        override val displayName = "OctoCam Torch"
        override val pluginDisplayName = "OctoCam"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.turnOn()
        override suspend fun turnOff() = owner.turnOff()
        override suspend fun isOn() = owner.isOn()
        override suspend fun toggle() = owner.toggle()
    }
}