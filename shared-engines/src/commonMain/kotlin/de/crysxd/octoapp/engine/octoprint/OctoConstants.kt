package de.crysxd.octoapp.engine.octoprint

import de.crysxd.octoapp.sharedcommon.utils.getString

internal object OctoConstants {
    const val OCTOPRINT_TAG = "HTTP/OctoPrint"
    val toolLabels by lazy {
        mapOf(
            "tool0" to getString("general___hotend"),
            "tool1" to getString("general___hotend") + " #2",
            "tool2" to getString("general___hotend") + " #3",
            "tool3" to getString("general___hotend") + " #4",
            "tool4" to getString("general___hotend") + " #5",
            "tool5" to getString("general___hotend") + " #6",
            "tool6" to getString("general___hotend") + " #7",
            "tool7" to getString("general___hotend") + " #8",
            "tool8" to getString("general___hotend") + " #9",
            "tool9" to getString("general___hotend") + " #10",
            "bed" to getString("general___bed"),
            "chamber" to getString("general___chamber"),
        )
    }
}