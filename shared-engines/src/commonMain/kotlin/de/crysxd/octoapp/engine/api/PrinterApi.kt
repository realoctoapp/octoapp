package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.printer.PrinterState

@Suppress("Unused")
interface PrinterApi {
    suspend fun getPrinterState(): PrinterState
    suspend fun setTemperatureTargets(targets: Map<String, Float>)
    suspend fun setTemperatureOffsets(offsets: Map<String, Float>)
    suspend fun changeZOffset(changeMm: Float)
    suspend fun saveZOffset()
    suspend fun resetZOffset()
    suspend fun setFanSpeed(component: String, percent: Float)
    suspend fun extrudeFilament(length: Float, speedMmMin: Float?)
    suspend fun selectExtruder(component: String)
    suspend fun executePrintHeadCommand(cmd: PrintHeadCommand)
    suspend fun executeGcodeCommand(cmd: GcodeCommand)
}