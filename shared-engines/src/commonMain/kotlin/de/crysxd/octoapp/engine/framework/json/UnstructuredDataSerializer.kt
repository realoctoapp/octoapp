package de.crysxd.octoapp.engine.framework.json

import io.github.aakira.napier.Napier
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonNull
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

class UnstructuredDataSerializer : KSerializer<Any?> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("AnyUnstructured", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder) = (decoder as? JsonDecoder)?.decodeJsonElement()?.toAny()

    fun JsonElement.toAny(): Any? = try {
        when {
            // Primitive is treated as string
            (this as? JsonNull) != null -> null
            (this as? JsonPrimitive) != null -> jsonPrimitive.content
            (this as? JsonArray) != null -> jsonArray.toList().map { it.toAny() }
            (this as? JsonObject) != null -> jsonObject.toString()
            else -> null
        }
    } catch (e: Exception) {
        Napier.e(tag = "OctoUnstructuredDataSerializer", message = "Failed to represent $this", throwable = e)
        null
    }

    override fun serialize(encoder: Encoder, value: Any?) {
        throw UnsupportedOperationException("Only deserialization is supported")

    }
}