package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.SettingsApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoSettings
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.head
import io.ktor.http.HttpHeaders

internal class OctoSettingsApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : SettingsApi {

    override suspend fun getSettings() = baseUrlRotator.request {
        val response = httpClient.get {
            urlFromPath(baseUrl = it, "api", "settings")
        }

        val settings = response.body<OctoSettings>()
        val hash = response.headers[HttpHeaders.ETag]
        settings.map(hash)
    }

    override suspend fun getSettingsHash() = baseUrlRotator.request {
        val response = httpClient.head {
            urlFromPath(baseUrl = it, "api", "settings")
        }

        response.headers[HttpHeaders.ETag]
    }
}