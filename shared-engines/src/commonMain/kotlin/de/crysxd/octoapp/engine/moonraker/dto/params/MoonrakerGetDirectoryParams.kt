package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerGetDirectoryParams(
    @SerialName("path") val path: String?,
    @SerialName("extended") val extended: Boolean,
) : MoonrakerRpcParams