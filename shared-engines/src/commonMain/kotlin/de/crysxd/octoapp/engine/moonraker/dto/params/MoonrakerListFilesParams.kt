package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class MoonrakerListFilesParams(
    @SerialName("root") val root: String,
) : MoonrakerRpcParams