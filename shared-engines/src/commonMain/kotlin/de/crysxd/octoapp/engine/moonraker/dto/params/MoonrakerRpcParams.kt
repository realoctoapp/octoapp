package de.crysxd.octoapp.engine.moonraker.dto.params

import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerRpcParamsSerializer
import kotlinx.serialization.Serializable

@Serializable(with = MoonrakerRpcParamsSerializer::class)
internal sealed interface MoonrakerRpcParams