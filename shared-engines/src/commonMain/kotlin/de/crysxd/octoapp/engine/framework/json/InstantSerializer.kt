package de.crysxd.octoapp.engine.framework.json

import kotlinx.datetime.Instant
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.math.roundToLong

internal class InstantSerializer : KSerializer<Instant> {

    companion object {
        private const val wrongMultiplierTest = 315529200 // 01/01/1980
    }

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("kotlinx.datetime.Instant", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: Instant) = encoder.encodeDouble(
        value.toEpochMilliseconds() / 1000.0
    )

    override fun deserialize(decoder: Decoder): Instant {
        val raw = decoder.decodeString()
        return raw.toDoubleOrNull()?.let {
            if (wrongMultiplierTest > it) {
                Instant.fromEpochMilliseconds(it.roundToLong())
            } else {
                Instant.fromEpochMilliseconds((it * 1000).roundToLong())
            }
        } ?: Instant.parse(raw)
    }
}