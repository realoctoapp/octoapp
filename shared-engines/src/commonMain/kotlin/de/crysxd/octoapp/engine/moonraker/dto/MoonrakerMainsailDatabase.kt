package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
internal data class MoonrakerMainsailDatabase(
    @SerialName("console") val console: Console? = null,
    @SerialName("control") val control: Control? = null,
    @SerialName("gcodeViewer") val gcodeViewer: GcodeViewer? = null,
    @SerialName("gcodehistory") val gcodeHistory: GcodeHistory? = null,
    @SerialName("general") val general: General? = null,
    @SerialName("initVersion") val initVersion: String? = null,
    @SerialName("macros") val macros: Macros? = null,
    @SerialName("miscellaneous") val miscellaneous: Miscellaneous? = null,
    @SerialName("presets") val presets: Presets? = null,
    @SerialName("uiSettings") val uiSettings: UiSettings? = null,
    @SerialName("view") val view: View? = null
) {
    @Serializable
    data class Console(
        @SerialName("cleared_since") val clearedSince: Long? = null,
        @SerialName("hideWaitTemperatures") val hideWaitTemperatures: Boolean? = null
    )

    @Serializable
    data class Control(
        @SerialName("enableXYHoming") val enableXYHoming: Boolean? = null,
        @SerialName("extruder") val extruder: Extruder? = null,
        @SerialName("selectedCrossStep") val selectedCrossStep: Int? = null,
        @SerialName("style") val style: String? = null,
        @SerialName("offsetsZ") val offsetsZ: List<Float>? = null
    ) {
        @Serializable
        data class Extruder(
            @SerialName("feedamount") val feedamount: Float? = null,
            @SerialName("feedrate") val feedrate: Float? = null
        )
    }

    @Serializable
    data class GcodeViewer(
        @SerialName("klipperCache") val klipperCache: KlipperCache? = null
    ) {
        @Serializable
        data class KlipperCache(
            @SerialName("axis_maximum") val axisMaximum: List<Float?>? = null,
            @SerialName("axis_minimum") val axisMinimum: List<Float?>? = null,
            @SerialName("kinematics") val kinematics: String? = null
        )
    }

    @Serializable
    data class GcodeHistory(
        @SerialName("entries") val entries: List<String?>? = null
    )

    @Serializable
    data class General(
        @SerialName("printername") val printerName: String? = null
    )

    @Serializable
    data class Macros(
        @SerialName("mode") val mode: String? = null
    )

    @Serializable
    data class Miscellaneous(
        @SerialName("entries") val entries: Map<String, JsonObject>? = null
    )

    @Serializable
    data class Presets(
        @SerialName("cooldownGcode") val cooldownGcode: String? = null,
        @SerialName("presets") val presets: Map<String, Preset>? = null
    ) {
        @Serializable
        data class Preset(
            @SerialName("gcode") val gcode: String? = null,
            @SerialName("name") val name: String? = null,
            @SerialName("values") val values: Map<String, Component>? = null
        ) {
            @Serializable
            data class Component(
                @SerialName("bool") val bool: Boolean? = null,
                @SerialName("type") val type: String? = null,
                @SerialName("value") val value: Float? = null
            )
        }
    }

    @Serializable
    data class UiSettings(
        @SerialName("navigationStyle") val navigationStyle: String? = null,
        @SerialName("logo") val logo: String? = null,
        @SerialName("primary") val primary: String? = null,
    )

    @Serializable
    data class View(
        @SerialName("configfiles") val configfiles: Configfiles? = null,
        @SerialName("gcodefiles") val gcodefiles: Gcodefiles? = null,
        @SerialName("tempchart") val tempchart: Tempchart? = null,
        @SerialName("webcam") val webcam: Webcam? = null
    ) {
        @Serializable
        data class Configfiles(
            @SerialName("hideBackupFiles") val hideBackupFiles: Boolean? = null,
            @SerialName("showHiddenFiles") val showHiddenFiles: Boolean? = null
        )

        @Serializable
        data class Gcodefiles(
            @SerialName("countPerPage") val countPerPage: Int? = null,
            @SerialName("hideMetadataColumns") val hideMetadataColumns: List<String?>? = null,
            @SerialName("orderMetadataColumns") val orderMetadataColumns: List<String?>? = null,
            @SerialName("sortBy") val sortBy: String? = null,
            @SerialName("sortDesc") val sortDesc: Boolean? = null
        )

        @Serializable
        data class Tempchart(
            @SerialName("autoscale") val autoscale: Boolean? = null
        )

        @Serializable
        data class Webcam(
            @SerialName("currentCam") val currentCam: CurrentCam? = null
        ) {
            @Serializable
            data class CurrentCam(
                @SerialName("dashboard") val dashboard: String? = null,
                @SerialName("page") val page: String? = null
            )
        }
    }
}