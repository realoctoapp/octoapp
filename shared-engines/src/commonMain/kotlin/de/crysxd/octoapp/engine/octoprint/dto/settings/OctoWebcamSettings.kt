package de.crysxd.octoapp.engine.octoprint.dto.settings

import de.crysxd.octoapp.engine.octoprint.serializer.OctoWebcamSettingsSerialized
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable(with = OctoWebcamSettingsSerialized::class)
sealed class OctoWebcamSettings {

    abstract val webcamEnabled: Boolean

    @Serializable
    data class OneDotEightAndOlder(
        val streamUrl: String? = null,
        val flipH: Boolean = false,
        val flipV: Boolean = false,
        val rotate90: Boolean = false,
        override val webcamEnabled: Boolean = false,
        val streamRatio: String = "16:9",
        val snapshotUrl: String? = null,
    ) : OctoWebcamSettings()

    @Serializable
    data class OneDotNineAndNewer(
        val webcams: List<Webcam> = emptyList(),
        override val webcamEnabled: Boolean = false,
    ) : OctoWebcamSettings()

    @Serializable
    data class Webcam(
        val name: String,
        val displayName: String? = null,
        val provider: String? = null,
        val flipH: Boolean = false,
        val flipV: Boolean = false,
        val rotate90: Boolean = false,
        val compat: WebcamCompat? = null,
        val extras: JsonObject? = null,
        val snapshotDisplay: String? = null,
        val canSnapshot: Boolean = false,
    )

    @Serializable
    data class WebcamCompat(
        val stream: String? = null,
        val snapshot: String? = null,
        val flipH: Boolean = false,
        val flipV: Boolean = false,
        val rotate90: Boolean = false,
        val streamRatio: String = "16:9",
    )
}