package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerSpoolmanSelectSpoolParams(
    @SerialName("spool_id") val spoolId: Int?,
) : MoonrakerRpcParams