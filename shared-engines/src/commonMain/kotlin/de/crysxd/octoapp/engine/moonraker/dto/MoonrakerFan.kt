package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericFan
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerFan(
    @SerialName("rpm") override val rpm: Float? = null,
    @SerialName("speed") override val speed: Float? = null
) : MoonrakerGenericFan, MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        rpm = rpm ?: (previous as? MoonrakerFan)?.rpm,
        speed = speed ?: (previous as? MoonrakerFan)?.speed,
    )
}