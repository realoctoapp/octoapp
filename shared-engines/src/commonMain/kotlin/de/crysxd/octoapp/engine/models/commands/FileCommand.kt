package de.crysxd.octoapp.engine.models.commands

sealed class FileCommand {
    data object PrintFile : FileCommand()
    data class MoveFile(val destination: String) : FileCommand()
    data class CopyFile(val destination: String) : FileCommand()
}