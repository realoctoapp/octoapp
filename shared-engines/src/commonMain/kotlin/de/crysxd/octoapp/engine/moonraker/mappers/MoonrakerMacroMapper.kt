package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.macro.Macro
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.jsonPrimitive

private val paramsRegex =
    Regex(".*\\{%?.*?params\\.(?<name>[A-Z0-9_]+)(\\s*\\|\\s*default\\((?<default>[a-zA-Z0-9,._ ]+)\\))?(\\s*\\|\\s*(?<type>int|float))?.*?(%?\\}|\$).*")

internal fun MoonrakerConfigFile.map(): List<Macro> = config?.filter { (key, _) ->
    key.startsWith("gcode_macro ")
}?.mapNotNull { (key, value) ->
    try {
        val description = value["description"]?.jsonPrimitive?.contentOrNull
        val gcode = value["gcode"]?.jsonPrimitive?.contentOrNull ?: ""
        val inputs = gcode.extractInputsFromGcode()
        val id = key.removePrefix("gcode_macro ")

        Macro(
            id = id,
            name = key.convertToMoonrakerComponentLabel(),
            inputs = inputs,
            confirmation = null,
            description = description,
        )
    } catch (e: Exception) {
        Napier.e(tag = "MoonrakerMakroMapper", message = "Failed to parse macro: $value", throwable = e)
        null
    }
}?.filter { macro ->
    val hidden = macro.id.startsWith("_")
    val default = macro.id in listOf(
        "CANCEL_PRINT",
        "PAUSE",
        "RESUME",
        "CANCEL_PRINT",
    )

    !hidden && !default
} ?: emptyList()

private fun String.extractInputsFromGcode() = split("\n").mapNotNull { line ->
    // We can't use named capture groups because of a bug on Android 5, 6 and 7 -> causes crash
    val match = paramsRegex.matchEntire(line) ?: return@mapNotNull null
    val name = requireNotNull(match.groups[1]?.value) { "Missing group name" }
    Macro.Input(
        name = name,
        label = name.convertToMoonrakerComponentLabel(),
        defaultValue = match.groups[3]?.value,
        type = when (match.groups[5]?.value) {
            "float" -> Macro.Input.Type.Float
            "int" -> Macro.Input.Type.Int
            else -> Macro.Input.Type.Any
        },
    )
}.groupBy { input ->
    input.name
}.mapValues { (key, inputs) ->
    Macro.Input(
        name = key,
        defaultValue = inputs.firstNotNullOfOrNull { it.defaultValue },
        label = inputs.firstNotNullOfOrNull { it.label } ?: key,
        type = inputs.filter { it.type != Macro.Input.Type.Any }.firstNotNullOfOrNull { it.type } ?: Macro.Input.Type.Any,
    )
}.values.toList()