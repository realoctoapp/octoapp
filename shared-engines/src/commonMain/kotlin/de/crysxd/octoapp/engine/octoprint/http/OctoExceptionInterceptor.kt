package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.exceptions.MissingPermissionException
import de.crysxd.octoapp.engine.exceptions.PrintBootingException
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.octoprint.OctoConstants
import de.crysxd.octoapp.sharedcommon.exceptions.DownloadTooLargeException
import de.crysxd.octoapp.sharedcommon.exceptions.obico.ObicoTunnelNotFoundException
import de.crysxd.octoapp.sharedcommon.http.HttpExceptionGenerator
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.call.HttpClientCall
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin

private const val tag = "OctoExceptionInterceptor"
private val generator = HttpExceptionGenerator()

internal fun HttpClient.installOctoPrintGenerateExceptionInterceptorPlugin(
    userApiFactory: (HttpClient) -> UserApi
) = plugin(HttpSend).intercept { request ->
    generator.generateExceptionsForCall(
        request = request,
        sender = this,
    ) { call ->
        when (call.response.status.value) {
            409 -> throw PrinterNotOperationalException(call.request.url)
            403 -> throw generate403Exception(call, userApiFactory)
            413 -> throw DownloadTooLargeException(call.request.url)
            in 501..599 -> throw PrintBootingException(call.request.url)
        }
    }
}

private suspend fun generate403Exception(
    call: HttpClientCall,
    userApiFactory: (HttpClient) -> UserApi
): Exception {
    // Prevent a loop. We will below request the /currentuser endpoint to test the API key
    val invalidApiKeyException = InvalidApiKeyException(call.request.url)
    when {
        call.request.url.pathSegments.last() == "currentuser" -> {
            Napier.w(
                tag = OctoConstants.OCTOPRINT_TAG,
                message = "Got 403 on currentuser endpoint -> assume API key no longer valid but this is weird, preventing loop"
            )
            return invalidApiKeyException
        }

        else -> Unit
    }

    Napier.w(tag = OctoConstants.OCTOPRINT_TAG, message = "Got 403, trying to get user")

    // We don't know what caused the 403. Requesting the currentuser will tell us whether we are a guest, meaning the API
    // key is not valid. If we are not a guest, 403 indicates a missing permission
    try {
        val isGuest = userApiFactory.invoke(call.client).getCurrentUser().isGuest
        return if (isGuest) {
            Napier.w(tag = OctoConstants.OCTOPRINT_TAG, message = "Got 403, user is guest")
            invalidApiKeyException
        } else {
            Napier.w(tag = OctoConstants.OCTOPRINT_TAG, message = "Got 403, permission is missing")
            MissingPermissionException(call.request.url)
        }
    } catch (e: ObicoTunnelNotFoundException) {
        Napier.w(tag = OctoConstants.OCTOPRINT_TAG, message = "Got 403, caused by Obico tunnel deleted")
        throw ObicoTunnelNotFoundException(call.request.url)
    } catch (e: Exception) {
        Napier.w(tag = OctoConstants.OCTOPRINT_TAG, message = "Got 403, failed to determine user status: $e")
        return invalidApiKeyException
    }
}