package de.crysxd.octoapp.engine.octoprint.event

import kotlin.time.Duration

internal expect object OctoWebsocketConfig {
    val canUseLongNoMessageTimeout: Boolean
    val longNoMessageTimeout: Duration
    val shotNoMessageTimeout: Duration
}
