package de.crysxd.octoapp.engine.moonraker.exception

import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import io.ktor.http.Url

class MoonrakerDatabaseNamespaceNotFoundException(
    message: String,
    code: Int,
    httpUrl: Url,
) : PrinterApiException(
    httpUrl = httpUrl,
    responseCode = code,
    body = message
)