package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.AggregatorMaterialsApi
import de.crysxd.octoapp.engine.api.MaterialsApi

class MoonrakerMaterialApi(
    private vararg val aggregates: AggregatorMaterialsApi.MaterialApiAggregate,
) : MaterialsApi by AggregatorMaterialsApi(
    aggregates = aggregates.toList()
)