package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolight.OctoLightCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octolight.OctoLightState
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OctoLightApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = if (settings.plugins.octoLight != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun executeCommand(action: String) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.OctoLight)
            setJsonBody(OctoLightCommand(action))
        }
    }

    private suspend fun turnOn() {
        executeCommand("turnOn")
    }

    private suspend fun turnOff() {
        executeCommand("turnOff")
    }

    private suspend fun toggle() {
        executeCommand("toggle")
    }

    private suspend fun isOn() = baseUrlRotator.request {
        httpClient.get {
            octoLightUrl(it)
        }
    }.body<OctoLightState>().state

    private fun HttpRequestBuilder.octoLightUrl(baseUrl: Url) = urlFromPath(baseUrl = baseUrl, "api", "plugin", OctoPlugins.OctoLight)

    internal data class PowerDevice(
        private val owner: OctoLightApi,
    ) : IPowerDevice {
        override val id = "octolight"
        override val pluginId = OctoPlugins.OctoLight
        override val displayName = "OctoLight"
        override val pluginDisplayName = "OctoLight 1.0 or newer"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate, IPowerDevice.Capability.ControlPrinterPower)
        override suspend fun turnOn() = owner.turnOn()
        override suspend fun turnOff() = owner.turnOff()
        override suspend fun isOn() = owner.isOn()
        override suspend fun toggle() = owner.toggle()
    }
}