package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoBedCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoChamberCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoGcodeCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoPrintHeadCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoToolCommand
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterState
import de.crysxd.octoapp.engine.octoprint.exception.NotSupportedOnOctoPrintException
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post
import kotlin.math.roundToInt

internal class OctoPrinterApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val eventSink: EventSink,
) : PrinterApi {

    override suspend fun getPrinterState() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "printer")
        }.body<OctoPrinterState>()
    }.map()

    override suspend fun setTemperatureTargets(targets: Map<String, Float>) {
        val tools = targets.filter { (key, _) -> key.startsWith("tool") }
        val bed = targets.filter { (key, _) -> key.startsWith("bed") }
        val chamber = targets.filter { (key, _) -> key.startsWith("chamber") }

        if (tools.isNotEmpty()) {
            executeToolCommand(OctoToolCommand.SetTargetTemperature(targets = tools))
        }

        if (bed.isNotEmpty()) {
            executeBedCommand(OctoBedCommand.SetTargetTemperature(target = bed.values.first()))
        }

        if (chamber.isNotEmpty()) {
            executeChamberCommand(OctoChamberCommand.SetTargetTemperature(target = chamber.values.first()))
        }
    }

    override suspend fun setTemperatureOffsets(offsets: Map<String, Float>) {
        val tools = offsets.filter { (key, _) -> key.startsWith("tool") }
        val bed = offsets.filter { (key, _) -> key.startsWith("bed") }
        val chamber = offsets.filter { (key, _) -> key.startsWith("chamber") }

        if (tools.isNotEmpty()) {
            executeToolCommand(OctoToolCommand.SetTemperatureOffset(offsets = tools))
        }

        if (bed.isNotEmpty()) {
            executeBedCommand(OctoBedCommand.SetTemperatureOffset(offset = bed.values.first()))
        }

        if (chamber.isNotEmpty()) {
            executeChamberCommand(OctoChamberCommand.SetTemperatureOffset(offset = chamber.values.first()))
        }
    }

    override suspend fun extrudeFilament(length: Float, speedMmMin: Float?) = executeToolCommand(
        cmd = OctoToolCommand.ExtrudeFilament(
            amount = length.toInt(),
            speed = speedMmMin
        )
    )

    override suspend fun changeZOffset(changeMm: Float) = executeGcodeCommand(
        GcodeCommand.Single("M290 Z$changeMm")
    )

    override suspend fun resetZOffset() = throw UnsupportedOperationException("Can't reset Z offset on OctoPrint")

    override suspend fun saveZOffset() = executeGcodeCommand(
        GcodeCommand.Single("M500")
    )

    override suspend fun setFanSpeed(component: String, percent: Float) = executeGcodeCommand(
        GcodeCommand.Single("M106 S${(percent.coerceIn(0f..100f) * 2.55f).roundToInt()}")
    )

    override suspend fun selectExtruder(component: String) = executeToolCommand(
        cmd = OctoToolCommand.SelectTool(
            tool = component
        )
    )

    private suspend fun executeChamberCommand(cmd: OctoChamberCommand) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "chamber")
            when (cmd) {
                is OctoChamberCommand.SetTargetTemperature -> setJsonBody(OctoChamberCommand.SetTargetTemperature(cmd.target))
                is OctoChamberCommand.SetTemperatureOffset -> setJsonBody(OctoChamberCommand.SetTemperatureOffset(cmd.offset))
            }
        }.let {
            if (cmd is OctoChamberCommand.SetTargetTemperature) {
                eventSink.injectInterpolatedTemperatureTarget(mapOf("chamber" to cmd.target))
            }
        }
    }

    private suspend fun executeToolCommand(cmd: OctoToolCommand) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "tool")

            when (cmd) {
                is OctoToolCommand.ExtrudeFilament -> setJsonBody(cmd)
                is OctoToolCommand.SetTargetTemperature -> setJsonBody(cmd)
                is OctoToolCommand.SetTemperatureOffset -> setJsonBody(cmd)
                is OctoToolCommand.SelectTool -> setJsonBody(cmd)
            }
        }.let {
            when (cmd) {
                is OctoToolCommand.ExtrudeFilament -> Unit
                is OctoToolCommand.SetTemperatureOffset -> Unit
                is OctoToolCommand.SelectTool -> Unit
                is OctoToolCommand.SetTargetTemperature -> eventSink.injectInterpolatedTemperatureTarget(cmd.targets)
            }
        }
    }

    private suspend fun executeBedCommand(cmd: OctoBedCommand) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "bed")
            when (cmd) {
                is OctoBedCommand.SetTargetTemperature -> setJsonBody(cmd)
                is OctoBedCommand.SetTemperatureOffset -> setJsonBody(cmd)
            }
        }.let {
            if (cmd is OctoBedCommand.SetTargetTemperature) {
                eventSink.injectInterpolatedTemperatureTarget(mapOf("bed" to cmd.target))
            }
        }
    }

    override suspend fun executePrintHeadCommand(cmd: PrintHeadCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "printhead")

            when (cmd) {
                is PrintHeadCommand.JogPrintHeadCommand -> setJsonBody(OctoPrintHeadCommand.JogPrintHeadCommand(x = cmd.x, y = cmd.y, z = cmd.z, speed = cmd.speed))
                is PrintHeadCommand.HomePrintHeadCommand -> setJsonBody(OctoPrintHeadCommand.HomePrintHeadCommand(axes = cmd.axes))
                else -> throw NotSupportedOnOctoPrintException("absolute_move")
            }
        }
    }

    override suspend fun executeGcodeCommand(cmd: GcodeCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "command")
            when (cmd) {
                is GcodeCommand.Batch -> setJsonBody(OctoGcodeCommand(commands = cmd.commands))
                is GcodeCommand.Single -> setJsonBody(OctoGcodeCommand(commands = listOf(cmd.command)))
            }
        }
    }
}