package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.event.HistoricTemperatureData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDisplayStatus
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExcludeObject
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMove
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerHeaterBed
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerInstant
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMotionReport
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOutputPin
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerStepperEnable
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerTemperatureStore
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerToolhead
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerVirtualSdCard
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebhooks
import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericFan
import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericHeater
import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericTemperatureSensor
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel
import io.github.aakira.napier.Napier
import kotlin.math.PI
import kotlin.math.pow

private const val tag = "MoonrakerNotifyStatusMapper"

data class MoonrakerNotifyStatusMapperContext(
    var maxFlow: Float = 0f,
    var maxSpeed: Float = 0f,
)

internal fun MoonrakerPrinterObjectsQuery.StatusItems.map(
    eventTime: MoonrakerInstant,
    historyMessage: Boolean = false,
    temperatureStore: MoonrakerTemperatureStore? = null,
    gcodeStore: List<String> = emptyList(),
    activeFile: MoonrakerFileMetadata? = null,
    context: MoonrakerNotifyStatusMapperContext,
): Message.Current {
    val printStats = items[Constants.PrinterObjects.PrintStats] as? MoonrakerPrintStats
    val webhooks = items[Constants.PrinterObjects.Webhooks] as? MoonrakerWebhooks
    val virtualSdCard = items[Constants.PrinterObjects.VirtualSdCard] as? MoonrakerVirtualSdCard
    val toolhead = items[Constants.PrinterObjects.Toolhead] as? MoonrakerToolhead
    val extruder = items[Constants.PrinterObjects.Extruder] as? MoonrakerExtruder
    val motionReport = items[Constants.PrinterObjects.MotionReport] as? MoonrakerMotionReport
    val gcodeMove = items[Constants.PrinterObjects.GcodeMove] as? MoonrakerGcodeMove
    val configFile = items[Constants.PrinterObjects.ConfigFile] as? MoonrakerConfigFile
    val display = items[Constants.PrinterObjects.DisplayStatus] as? MoonrakerDisplayStatus
    val stepperEnable = items[Constants.PrinterObjects.StepperEnable] as? MoonrakerStepperEnable
    val config = items[Constants.PrinterObjects.ConfigFile] as? MoonrakerConfigFile
    val excludeObject = items[Constants.PrinterObjects.ExcludeObject] as? MoonrakerExcludeObject

    val temperatureSensors = items
        .filterValues { it is MoonrakerGenericTemperatureSensor }
        .mapValues { (_, value) -> value as MoonrakerGenericTemperatureSensor }

    val fans = items
        .filterValues { it is MoonrakerGenericFan }
        .mapValues { (_, value) -> value as MoonrakerGenericFan }

    val fanSurrogates = items
        .filterValues { it is MoonrakerOutputPin }
        .mapValues { (_, value) -> value as MoonrakerOutputPin }

    return Message.Current(
        serverTime = eventTime,
        isHistoryMessage = historyMessage,
        pendingSettingsSave = config?.saveConfigPending == true,
        state = MoonrakerPrinterStateMapperInput(
            stats = printStats,
            webhooks = webhooks,
        ).map(),
        progress = MoonrakerProgressInformationMapperInput(
            virtualSdCard = virtualSdCard,
            printStats = printStats,
            activeFile = activeFile,
            gcodeMove = gcodeMove,
        ).map(),
        job = MoonrakerPrintJobMapperInput(
            virtualSdCard = virtualSdCard,
            printStats = printStats,
            file = activeFile,
        ).map(),
        temps = listOfNotNull(
            temperatureStore?.map(time = eventTime.toEpochMilliseconds()),
            listOf(temperatureSensors.map(time = eventTime.toEpochMilliseconds()))
        ).flatten(),
        tempLabels = temperatureSensors.mapValues { (key, _) ->
            key.convertToMoonrakerComponentLabel()
        },
        tempMaxs = temperatureSensors.mapValues { (_, value) ->
            when (value) {
                is MoonrakerHeaterBed -> 120f
                is MoonrakerExtruder -> 285f
                else -> 100f
            }
        },
        tempEditables = temperatureSensors.entries.mapNotNull { (key, values) ->
            key.takeIf { values is MoonrakerGenericHeater }
        },
        displayMessage = display?.message,
        offsets = null,
        logs = gcodeStore,
        tuneState = MoonrakerTuneStateMapperInput(
            toolhead = toolhead,
            extruder = extruder,
            gcodeMove = gcodeMove,
            fans = fans,
            fanSurrogates = fanSurrogates
        ).map(),
        realTimeStats = Message.Current.RealTimeStats(
            toolhead = toolhead?.map(motionReport, context),
            extruder = printStats?.map(motionReport, context, configFile, toolhead),
            steppers = stepperEnable?.map()
        ),
        printObjects = excludeObject?.map(),
    )
}

private fun MoonrakerStepperEnable.map() = Message.Current.RealTimeStats.Steppers(
    enabled = steppers,
)

private fun MoonrakerToolhead.map(
    motionReport: MoonrakerMotionReport?,
    context: MoonrakerNotifyStatusMapperContext
): Message.Current.RealTimeStats.Toolhead? = try {
    Message.Current.RealTimeStats.Toolhead(
        xyHomed = homedAxes?.let { "x" in it && "y" in it },
        zHomed = homedAxes?.let { "z" in it },
        positionX = position?.getOrNull(0),
        positionY = position?.getOrNull(1),
        positionZ = position?.getOrNull(2),
        speedMmPerS = motionReport?.liveVelocity?.coerceAtLeast(0f),
        maxpeedMmPerS = motionReport?.liveVelocity?.let { currentSpeed ->
            context.maxSpeed = maxOf(currentSpeed, context.maxSpeed)
            context.maxSpeed
        }?.coerceAtLeast(0f)
    )
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map toolhead", throwable = e)
    null
}

private fun MoonrakerPrintStats.map(
    motionReport: MoonrakerMotionReport?,
    context: MoonrakerNotifyStatusMapperContext,
    config: MoonrakerConfigFile?,
    toolhead: MoonrakerToolhead?,
): Message.Current.RealTimeStats.Extruder? = try {
    val extruder = toolhead?.extruder ?: "extruder"
    val filamentDiameter = try {
        config?.getConfigItem<MoonrakerConfigFile.Extruder>(extruder)?.filamentDiameter ?: let {
            Napier.w(tag = tag, message = "Failed to determine filament diameter for $extruder, assuming 1.75mm: ${config?.config?.get(extruder)}")
            null
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Error while getting filament diameter", throwable = e)
        null
    } ?: 1.75f

    val crossSection = (filamentDiameter / 2).pow(2) * PI
    val currentFlow = motionReport?.liveExtruderVelocity?.times(crossSection)?.toFloat()?.coerceAtLeast(0f)
    context.maxFlow = maxOf(currentFlow ?: 0f, context.maxFlow)

    Message.Current.RealTimeStats.Extruder(
        filamentUsedMm = filamentUsed,
        flowMm3PerS = currentFlow,
        maxFlowMm3PerS = context.maxFlow
    )
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map prin stats", throwable = e)
    null
}

private fun MoonrakerTemperatureStore.map(time: Long): List<HistoricTemperatureData> {
    if (components.isEmpty()) return emptyList()
    val sampleCount = components.values.first().temperatures?.size ?: return let {
        Napier.w(tag = tag, message = "MoonrakerTemperatureStore has item without temperatures: $this")
        emptyList()
    }

    return (0 until sampleCount).map { index ->
        HistoricTemperatureData(
            time = time + (sampleCount - index) * 1000,
            components = components.mapValues { (_, value) ->
                ComponentTemperature(
                    actual = value.temperatures?.getOrNull(index),
                    target = value.targets?.getOrNull(index),
                )
            }
        )
    }
}

private fun Map<String, MoonrakerGenericTemperatureSensor>.map(time: Long) = HistoricTemperatureData(
    time = time,
    components = mapValues { (_, value) ->
        ComponentTemperature(
            actual = value.temperature,
            target = (value as? MoonrakerGenericHeater)?.target,
        )
    }
)