package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerPrinterObjectsQueryParams(
    @SerialName("objects") val objects: Map<String, String?>
) : MoonrakerRpcParams