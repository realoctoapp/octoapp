package de.crysxd.octoapp.engine.moonraker.dto.incoming

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerUnparsedRpcResponse(
    @SerialName("jsonrpc") val jsonRpc: String,
    val id: Int? = null,
    val error: Error? = null
) : MoonrakerIncomingMessage {
    @Serializable
    data class Error(
        val code: Int,
        val message: String,
    )
}