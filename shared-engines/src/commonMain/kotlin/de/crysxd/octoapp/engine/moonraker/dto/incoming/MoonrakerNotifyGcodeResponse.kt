package de.crysxd.octoapp.engine.moonraker.dto.incoming

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerNotifyGcodeResponse(
    @SerialName("jsonrpc") val jsonrpc: String? = null,
    @SerialName("method") val method: String? = null,
    @SerialName("params") val params: List<String>? = null
) : MoonrakerIncomingMessage