package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.framework.guessWebcamUrlType
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings.Webcam.Extras.GenericExtras
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerFluidd
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerMainsail
import de.crysxd.octoapp.engine.models.system.SystemInfo.Interface.MoonrakerMixed
import de.crysxd.octoapp.engine.moonraker.Constants.PrinterObjects.Extruder
import de.crysxd.octoapp.engine.moonraker.Constants.PrinterObjects.HeaterBed
import de.crysxd.octoapp.engine.moonraker.Constants.PrinterObjects.HeaterChamber
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerBedMesh
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFluiddDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerLegacyWebcamDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMainsailDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOctoAppDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPowerDevices
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerServerInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo.SystemInfo.ServiceState.ActiveState.Active
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.HlsStream
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.IpCamera
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.JmuxerRaw
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.MjpegStreamer
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.MjpegStreamerAdaptive
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.Uv4lMjpeg
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.WebRtcCameraStreamer
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.WebRtcGo2Rtc
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.WebRtcJanus
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList.Webcam.Service.WebRtcMediaMtx
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel
import de.crysxd.octoapp.engine.moonraker.ext.description
import de.crysxd.octoapp.engine.moonraker.ext.obico
import de.crysxd.octoapp.engine.moonraker.ext.octoEverywhere
import de.crysxd.octoapp.engine.moonraker.ext.octoapp
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import io.github.aakira.napier.Napier
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.minutes

private const val tag = "MoonrakerSettingsMapper"

internal data class MoonrakerSettingsMapperInput(
    val mainsailDatabase: MoonrakerMainsailDatabase?,
    val fluiddDatabase: MoonrakerFluiddDatabase?,
    val octoAppDatabase: MoonrakerOctoAppDatabase?,
    val webcamList: MoonrakerWebcamList,
    val legacyWebcamDatabase: MoonrakerLegacyWebcamDatabase?,
    val printerObjects: List<String>,
    val bedMesh: MoonrakerBedMesh?,
    val serviceStates: Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?,
    val powerDevices: MoonrakerPowerDevices?,
    val info: MoonrakerServerInfo?,
    val preferredInterface: SystemInfo.Interface,
)

internal fun MoonrakerSettingsMapperInput.map(): Settings {
    val mainsailDatabase = mainsailDatabase.takeIf { preferredInterface in listOf(MoonrakerMainsail, MoonrakerMixed) }
    val fluiddDatabase = fluiddDatabase.takeIf { preferredInterface in listOf(MoonrakerFluidd, MoonrakerMixed) }

    return Settings(
        temperaturePresets = listOfNotNull(
            mainsailDatabase?.mapTemperaturePresets(),
            fluiddDatabase?.mapTemperaturePresets(),
        ).flatten(),
        coolDownProfile = createCoolDownProfile(mainsailDatabase),
        webcam = webcamList.map() + fluiddDatabase.mapCameras(),
        appearance = mainsailDatabase?.mapAppearance()
            ?: fluiddDatabase?.mapAppearance()
            ?: Settings.Appearance(),
        plugins = Settings.PluginSettingsGroup(
            octoAppCompanion = (serviceStates to octoAppDatabase).mapCompanionSettings(),
            moonrakerPowerDevices = powerDevices?.map(),
            octoEverywhere = if (serviceStates.octoEverywhere != null) Settings.OctoEverywhere else null,
            obico = if (serviceStates.obico != null) Settings.Obico else null,
            spoolman = if (info?.components?.contains("spoolman") == true) Settings.Spoolman() else null
        ),
        terminalFilters = createTerminalFilters(),
        bedMesh = bedMesh?.map(),
        hash = null,
        printerObjects = printerObjects,
        babyStepIntervals = fluiddDatabase?.uiSettings?.general?.zAdjustDistances?.takeIf { it.isNotEmpty() }
            ?: mainsailDatabase?.control?.offsetsZ?.takeIf { it.isNotEmpty() }
            ?: Settings().babyStepIntervals,
    ).let { settings ->
        // If we do not have webcams yet, consider legacy location (SonicPad, yey)
        if (settings.webcam.webcams.isEmpty()) {
            settings.copy(webcam = legacyWebcamDatabase?.map() ?: WebcamSettings())
        } else {
            settings
        }
    }
}

private fun createTerminalFilters() = listOf(
    Settings.TerminalFilter(
        name = "Suppress temperature messages",
        regex = "^(([a-zA-Z_]+\\d?:)\\d+.\\d+\\s/\\d+.\\d+\\s?)+\$"
    )
)

private fun MoonrakerPowerDevices.map() = try {
    devices?.let {
        Settings.MoonrakerPowerDevices(
            devices = it.filterNotNull().mapNotNull { device ->
                Napier.d(tag = tag, message = "Mapping power device ${device.device} from Moonraker")
                Settings.MoonrakerPowerDevices.Device(
                    device = device.device ?: return@mapNotNull null,
                    type = device.type ?: "other",
                )
            }
        )
    }
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Moonraker power devices", throwable = e)
    null
}

private fun Pair<Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>?, MoonrakerOctoAppDatabase?>.mapCompanionSettings() = try {
    val (serviceStates, octoAppDatabase) = this
    val companionServiceState = serviceStates?.octoapp
    val lastSeenAt = octoAppDatabase?.public?.lastSeenAt
    val lastSeen = Clock.System.now() - (lastSeenAt ?: Instant.DISTANT_PAST)

    if (companionServiceState != null) {
        Napier.d(
            tag = tag,
            message = "Plugin last seen $lastSeen ago ($lastSeenAt), service ${companionServiceState.name} is ${companionServiceState.state?.activeState}: ${serviceStates.description}"
        )
        Settings.OctoAppCompanion(
            version = octoAppDatabase?.public?.pluginVersion,
            running = companionServiceState.state?.activeState == Active,
            encryptionKey = octoAppDatabase?.public?.encryptionKey,
            serviceName = companionServiceState.name,
        )
    } else if (lastSeenAt != null && lastSeen < 30.minutes) {
        Napier.d(
            tag = tag,
            message = "Plugin service missing but was last seen $lastSeen ago ($lastSeenAt)"
        )
        Settings.OctoAppCompanion(
            version = octoAppDatabase.public.pluginVersion,
            running = true,
            encryptionKey = octoAppDatabase.public.encryptionKey,
            serviceName = companionServiceState?.name,
        )
    } else {
        Napier.d(
            tag = tag,
            message = "Missing companion service state and lastSeenAt, assuming no companion (${serviceStates.description})"
        )
        null
    }
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Companion service state", throwable = e)
    null
}

private fun MoonrakerMainsailDatabase.mapTemperaturePresets() = try {
    presets?.presets?.mapValues { (key, value) ->
        Napier.d(tag = tag, message = "Mapping temperature profile ${value.name} from Mainsail")
        Settings.TemperaturePreset(
            name = value.name ?: key,
            gcode = value.gcode?.takeUnless { it.isBlank() },
            components = value.values?.mapNotNull { (key, value) ->
                if (value.bool == false || value.value == null) return@mapNotNull null

                Settings.TemperaturePreset.Component(
                    key = key,
                    displayName = key.convertToMoonrakerComponentLabel(),
                    temperature = value.value,
                    isBed = key == HeaterBed,
                    isChamber = key == HeaterChamber,
                    isExtruder = key.startsWith(Extruder)
                )
            } ?: emptyList(),
        )
    }?.values
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Mainsail temperature profiles", throwable = e)
    null
}


private fun MoonrakerFluiddDatabase.mapTemperaturePresets() = try {
    uiSettings?.dashboard?.tempPresets?.map { preset ->
        Napier.d(tag = tag, message = "Mapping temperature profile ${preset.name} from Fluidd")
        Settings.TemperaturePreset(
            name = preset.name ?: preset.id ?: "???",
            gcode = preset.gcode?.takeUnless { it.isBlank() },
            components = preset.values?.mapNotNull { (key, value) ->
                if (value.active == false || value.value == null) return@mapNotNull null

                Settings.TemperaturePreset.Component(
                    key = key,
                    displayName = key.convertToMoonrakerComponentLabel(),
                    temperature = value.value,
                    isBed = key == HeaterBed,
                    isChamber = key == HeaterChamber,
                    isExtruder = key.startsWith(Extruder),
                )
            } ?: emptyList(),
        )
    }
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Fluidd temperature profiles", throwable = e)
    null
}

private fun MoonrakerLegacyWebcamDatabase.map() = try {
    WebcamSettings(
        webcams = mapNotNull { (id, webcam) ->
            if (webcam == null) {
                Napier.d(tag = tag, message = "Mapping webcam $id from legacy database, but value is null!")
                return@mapNotNull null
            }

            if (webcam.enabled == false) {
                return@mapNotNull null
            }

            Napier.d(tag = tag, message = "Mapping webcam $id from legacy database")
            val (flipH, flipV, rotate90) = mapImageTransform(
                flipH = webcam.flipX == true,
                flipV = webcam.flipY == true,
                rotation = webcam.rotation?.toInt() ?: 0
            )

            WebcamSettings.Webcam(
                name = id,
                provider = listOfNotNull("legacy", webcam.service).joinToString(":"),
                canSnapshot = false,
                displayName = webcam.name ?: "???",
                extras = null,
                flipH = flipH,
                flipV = flipV,
                rotate90 = rotate90,
                snapshotDisplay = null,
                snapshotUrl = null,
                streamRatio = "16:9",
                streamUrl = webcam.urlStream,
                type = when (webcam.service) {
                    "mjpegstreamer",
                    "uv4l-mjpeg",
                    "mjpegstreamer-adaptive" -> WebcamSettings.Webcam.Type.Mjpeg
                    else -> null
                }
            )
        }
    )
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map legacy webcam database", throwable = e)
    WebcamSettings(webcams = emptyList())
}

private fun MoonrakerFluiddDatabase?.mapCameras() = try {
    this?.cameras?.cameras
        ?.filterNotNull()
        ?.filter { it.enabled != false }
        ?.map { webcam ->
            Napier.d(tag = tag, message = "Mapping webcam ${webcam.id} from Fluidd")
            val (flipH, flipV, rotate90) = mapImageTransform(
                flipH = webcam.flipX == true,
                flipV = webcam.flipY == true,
                rotation = webcam.rotation ?: 0
            )

            val typeOverride = webcam.url?.guessWebcamUrlType()
                ?.first
                ?.takeUnless { it == WebcamSettings.Webcam.Type.Mjpeg }

            WebcamSettings.Webcam(
                name = webcam.id ?: "???",
                provider = listOfNotNull("fluidd", webcam.type).joinToString(":"),
                canSnapshot = false,
                displayName = webcam.name ?: "???",
                extras = null,
                flipH = flipH,
                flipV = flipV,
                rotate90 = rotate90,
                snapshotDisplay = null,
                snapshotUrl = null,
                streamRatio = "16:9",
                streamUrl = webcam.url?.trim(),
                type = when (typeOverride) {
                    null -> when (webcam.type) {
                        "iframe" -> WebcamSettings.Webcam.Type.Iframe
                        else -> WebcamSettings.Webcam.Type.Mjpeg
                    }

                    else -> typeOverride
                }
            )
        }
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Fluidd cameras", throwable = e)
    null
} ?: emptyList()

private fun createCoolDownProfile(
    mainsailDatabase: MoonrakerMainsailDatabase?,
) = Settings.TemperaturePreset(
    name = "__cooldown",
    components = emptyList(),
    gcode = mainsailDatabase?.presets?.cooldownGcode ?: "TURN_OFF_HEATERS",
)

private fun MoonrakerMainsailDatabase.mapAppearance() = try {
    Napier.d(tag = tag, message = "Mapping appearance from Mainsail")
    Settings.Appearance(
        name = general?.printerName,
        colors = HexColor(uiSettings?.logo ?: "#D41216").generateColors(),
    )
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Mainsail appearance", throwable = e)
    null
}

private fun MoonrakerFluiddDatabase.mapAppearance() = try {
    Napier.d(tag = tag, message = "Mapping appearance from Fluidd")
    Settings.Appearance(
        name = uiSettings?.general?.instanceName,
        colors = HexColor(uiSettings?.theme?.currentTheme?.primary ?: "#2196f3").generateColors()
    )
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map Fluidd appearance", throwable = e)
    null
}

private fun HexColor.generateColors() = Settings.Appearance.Colors(
    light = Settings.Appearance.ColorScheme(
        main = this,
        accent = this.shade(brightness = 1.85f, min = 0.85f, max = 0.94f, minOffsetCurrent = 0.2f),
    ),
    dark = Settings.Appearance.ColorScheme(
        main = this.shade(brightness = 0.9f, max = 0.8f),
        accent = this.shade(brightness = 1.4f, min = 0.65f, max = 0.9f, minOffsetCurrent = 0.2f),
    )
)

private fun MoonrakerWebcamList.map() = try {
    WebcamSettings(
        webcams = webcams?.filterNotNull()?.filter {
            it.enabled != false
        }?.map { webcam ->
            Napier.d(tag = tag, message = "Mapping webcam ${webcam.name} from Moonraker")
            val (flipH, flipV, rotate90) = mapImageTransform(
                flipH = webcam.flipHorizontal == true,
                flipV = webcam.flipVertical == true,
                rotation = webcam.rotation?.toInt() ?: 0
            )
            val typeOverride = webcam.streamUrl
                ?.guessWebcamUrlType()
                ?.first
                ?.takeUnless { it == WebcamSettings.Webcam.Type.Mjpeg }

            WebcamSettings.Webcam(
                name = webcam.name ?: "???",
                provider = webcam.service?.name ?: "Unknown",
                canSnapshot = webcam.snapshotUrl != null,
                displayName = webcam.name ?: "???",
                extras = webcam.extraData?.let { GenericExtras(it) },
                flipH = flipH,
                flipV = flipV,
                rotate90 = rotate90,
                snapshotDisplay = webcam.snapshotUrl?.trim(),
                snapshotUrl = webcam.snapshotUrl?.trim(),
                streamRatio = webcam.aspectRatio ?: "16:9",
                streamUrl = webcam.streamUrl,
                type = when (typeOverride) {
                    null -> when (webcam.service) {
                        WebRtcCameraStreamer -> WebcamSettings.Webcam.Type.WebRtcCameraStreamer
                        MjpegStreamer -> WebcamSettings.Webcam.Type.Mjpeg
                        MjpegStreamerAdaptive -> WebcamSettings.Webcam.Type.Mjpeg
                        WebRtcJanus -> WebcamSettings.Webcam.Type.WebRtcJanus
                        JmuxerRaw -> WebcamSettings.Webcam.Type.JmuxerRaw
                        HlsStream -> WebcamSettings.Webcam.Type.Hls
                        WebRtcMediaMtx -> WebcamSettings.Webcam.Type.WebRtcMediaMtx
                        WebRtcGo2Rtc -> WebcamSettings.Webcam.Type.WebRtcGoRtc
                        IpCamera -> WebcamSettings.Webcam.Type.IpCamera
                        Uv4lMjpeg -> WebcamSettings.Webcam.Type.Mjpeg
                        null -> null
                    }

                    else -> typeOverride
                }
            )
        } ?: emptyList(),
        webcamEnabled = true,
    )
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to map webcams", throwable = e)
    WebcamSettings(
        webcams = emptyList(),
        webcamEnabled = true,
    )
}

private fun mapImageTransform(flipH: Boolean, flipV: Boolean, rotation: Int): Triple<Boolean, Boolean, Boolean> {
    var h = flipH
    var v = flipV
    val r = when (rotation) {
        // 90 -> Just rotate but also flip as we need to rotate the other direction
        90 -> {
            h = !h
            v = !v
            true
        }

        // 180 -> Flip image upside down
        180 -> {
            h = !h
            v = !v
            false
        }

        // 270 -> Flip image and rotate
        270 -> true

        // Ignore any other value
        else -> false
    }

    return Triple(h, v, r)
}

private operator fun WebcamSettings.plus(webcams: List<WebcamSettings.Webcam>) = copy(
    webcams = this.webcams + webcams,
)

private fun MoonrakerBedMesh.map(): Settings.BedMesh? = try {
    val invalid = -999999f
    val graphZMin = probedMatrix?.minOfOrNull { it.minOrNull() ?: invalid }?.takeIf { it > invalid }
    val graphZMax = probedMatrix?.maxOfOrNull { it.maxOrNull() ?: invalid }?.takeIf { it > invalid }

    if (meshMin?.size != 2 || meshMax?.size != 2 || probedMatrix.isNullOrEmpty() || graphZMin == null || graphZMax == null) {
        Napier.e(tag = tag, message = "Failed to parse bed mesh, invalid data: $this")
        null
    } else {
        fun Pair<Float, Float>.steps(steps: Int) = when (steps) {
            in 0..1 -> listOf((first + second) / 2)
            else -> (first..second).steps(steps)
        }

        Settings.BedMesh(
            graphZMin = graphZMin,
            graphZMax = graphZMax,
            refreshCommand = "G28\nBED_MESH_CALIBRATE${if (profileName.isNullOrBlank()) "" else " PROFILE=$profileName"}",
            colorScale = Settings.BedMesh.DefaultColorScale,
            meshX = (meshMin[0] to meshMax[0]).steps(probedMatrix[0].size),
            meshY = (meshMin[1] to meshMax[1]).steps(probedMatrix.size),
            mesh = probedMatrix,
            activeProfile = profileName,
            profiles = profiles?.map { (key, _) ->
                Settings.BedMesh.Profile(
                    id = key,
                    enableCommand = "BED_MESH_PROFILE LOAD=$key",
                    label = key
                )
            } ?: emptyList()
        )
    }
} catch (e: Exception) {
    Napier.e(tag = tag, message = "Failed to parse bed mesh", throwable = e)
    null
}

private fun ClosedFloatingPointRange<Float>.steps(count: Int): List<Float> {
    val step = (endInclusive - start) / (count - 1)
    return (0..<count).map {
        start + (step * it)
    }
}