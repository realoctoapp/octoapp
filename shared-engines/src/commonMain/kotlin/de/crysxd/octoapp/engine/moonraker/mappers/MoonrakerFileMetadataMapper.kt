package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileObject.Companion.FILE_TYPE_MACHINE_CODE
import de.crysxd.octoapp.engine.models.files.FileObject.Companion.FILE_TYPE_MACHINE_CODE_GCODE
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.sharedcommon.ext.format
import de.crysxd.octoapp.sharedcommon.ext.formatAsFileSize
import de.crysxd.octoapp.sharedcommon.ext.formatAsLayerHeight
import de.crysxd.octoapp.sharedcommon.ext.formatAsLength
import de.crysxd.octoapp.sharedcommon.ext.formatAsSecondsDuration
import de.crysxd.octoapp.sharedcommon.ext.formatAsTemperature
import de.crysxd.octoapp.sharedcommon.ext.formatAsWeight
import de.crysxd.octoapp.sharedcommon.utils.getString
import io.github.aakira.napier.Napier
import io.ktor.http.URLBuilder
import io.ktor.http.appendPathSegments
import io.ktor.http.encodedPath
import kotlinx.datetime.Instant

internal fun MoonrakerFileMetadata.map(path: String, origin: FileOrigin): FileObject.File = FileObject.File(
    name = path.split("/").last(),
    display = path.split("/").last(),
    path = path,
    origin = origin,
    type = FILE_TYPE_MACHINE_CODE,
    typePath = listOf(FILE_TYPE_MACHINE_CODE, FILE_TYPE_MACHINE_CODE_GCODE),
    date = modified ?: Instant.DISTANT_PAST,
    size = size,
    hash = uuid,
    id = uuid ?: path,
    prints = null,
    thumbnails = thumbnails?.mapNotNull { thumb ->
        val encodedThumbPath = URLBuilder("http://localhost").apply {
            appendPathSegments("server", "files", "gcodes")
            appendPathSegments(path.removePrefix("/").split("/").dropLast(1))
            appendPathSegments(thumb.relativePath?.split("/") ?: return@mapNotNull null)
        }.encodedPath + "?timestamp=${modified?.toEpochMilliseconds()?.toString() ?: "0"}"

        FileObject.File.Thumbnail(
            width = thumb.width ?: Int.MAX_VALUE,
            height = thumb.height ?: Int.MAX_VALUE,
            path = encodedThumbPath,
        )
    } ?: emptyList(),
    metadata = listOf(
        FileObject.File.MetadataGroup(
            label = getString("file_manager___file_details___print_info"),
            items = listOf(
                getString("file_manager___file_details___height") to objectHeight?.formatAsLength(),
                getString("file_manager___file_details___nozzle_diameter") to nozzleDiameter?.formatAsLength(),
                getString("file_manager___file_details___print_time") to estimatedTime?.formatAsSecondsDuration(),
                getString("file_manager___file_details___layer_height") to layerHeight?.formatAsLayerHeight(),
                getString("file_manager___file_details___slicer") to listOfNotNull(slicer, slicerVersion).joinToString(" "),
            )
        ),
        FileObject.File.MetadataGroup(
            label = getString("file_manager___file_details___first_layer"),
            items = listOf(
                getString("file_manager___file_details___height") to firstLayerHeight?.formatAsLayerHeight(),
                getString("file_manager___file_details___extruder_temperature") to firstLayerExtrTemp?.formatAsTemperature(),
                getString("file_manager___file_details___bed_temperature") to firstLayerBedTemp?.formatAsTemperature(),
                getString("file_manager___file_details___chamber_temperature") to firstLayerChamberTemp?.formatAsTemperature(),
            )
        ),
        FileObject.File.MetadataGroup(
            label = getString("file_manager___file_details___filament_use"),
            items = filaments() + listOf(
                getString("file_manager___file_details___filament_total_length") to filamentTotal?.formatAsLength(onlyMilli = false),
                getString("file_manager___file_details___filament_total_weight") to filamentWeightTotal?.formatAsWeight(),
            )
        ),
        FileObject.File.MetadataGroup(
            label = getString("file_manager___file_details___file"),
            items = listOf(
                getString("file_manager___file_details___name") to filename,
                getString("file_manager___file_details___path") to path.split("/").dropLast(1).joinToString("/").plus("/"),
                getString("file_manager___file_details___file_size") to this.size?.formatAsFileSize(),
                getString("file_manager___file_details___uploaded") to this.modified?.format(),
            )
        ),
    )
)

private fun MoonrakerFileMetadata.filaments(): List<Pair<String, String?>> = try {
    val names = filamentName?.replace("\";\"", ";")?.split(";") ?: emptyList()
    val types = filamentType?.replace("\";\"", ";")?.split(";") ?: emptyList()
    names.zip(types).map { (name, type) ->
        name to type
    }.distinct()
} catch (e: Exception) {
    Napier.e(tag = "MoonrakerFileMetadataMapper", message = "Failed to separate filaments: name=$filamentName type=$filamentType", throwable = e)
    filamentName?.let {
        listOf(it to filamentType)
    } ?: emptyList()
}