package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.ophom.OphomState
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OphomApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = if (settings.plugins.ophom != null) {
        listOf(PowerDevice(this))
    } else {
        emptyList()
    }

    private suspend fun executeCommand(action: String) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.Ophom)
            parameter("action", action)
        }
    }

    private suspend fun toggle() {
        executeCommand("toggle")
    }

    private suspend fun isOn() = executeCommand("checkstatus").body<OphomState>().isOn

    internal data class PowerDevice(
        private val owner: OphomApi,
    ) : IPowerDevice {
        override val id = "ophom"
        override val pluginId = OctoPlugins.Ophom
        override val displayName = "Ophom Light"
        override val pluginDisplayName = "Ophom"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate, IPowerDevice.Capability.ControlPrinterPower)
        override suspend fun turnOn() = if (isOn()) Unit else toggle()
        override suspend fun turnOff() = if (isOn()) toggle() else Unit
        override suspend fun isOn() = owner.isOn()
        override suspend fun toggle() = owner.toggle()
    }
}