package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerPrinterGcodeScriptParams(
    @SerialName("script") val script: String,
) : MoonrakerRpcParams