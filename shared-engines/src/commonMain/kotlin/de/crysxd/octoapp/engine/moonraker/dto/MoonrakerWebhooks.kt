package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerWebhooks(
    @SerialName("state") val state: State? = null,
    @SerialName("state_message") val stateMessage: String? = null
) : MoonrakerStatusItem {

    @Serializable
    enum class State {
        @SerialName("ready")
        Ready,

        @SerialName("shutdown")
        Shutdown,

        @SerialName("startup")
        Startup,

        @SerialName("other")
        Other
    }

    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        state = state ?: (previous as? MoonrakerWebhooks)?.state,
        stateMessage = stateMessage ?: (previous as? MoonrakerWebhooks)?.stateMessage,
    )
}