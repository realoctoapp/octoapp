package de.crysxd.octoapp.engine.models.printer

import de.crysxd.octoapp.sharedcommon.ext.formatAsPercent
import de.crysxd.octoapp.sharedcommon.utils.getString
import kotlin.math.roundToInt

@Suppress("Unused")
data class PrinterState(
    val state: State = State(),
    val temperature: Map<String, ComponentTemperature> = emptyMap()
) {

    data class State(
        val text: String? = null,
        val flags: Flags = Flags()
    ) {

        fun createStatusText(completion: Float?) = when {
            flags.pausing -> getString("pausing")
            flags.paused -> getString("paused")
            flags.cancelling -> getString("cancelling")
            flags.isPrinting() && completion != null -> completion.roundToInt().formatAsPercent()
            flags.isOperational() -> getString("wear___ambient_state___idle")
            flags.isError() -> getString("wear___ambient_state___error")
            else -> getString("other")
        }
    }

    data class Flags(
        val operational: Boolean = false,
        val paused: Boolean = false,
        val printing: Boolean = false,
        val cancelling: Boolean = false,
        val pausing: Boolean = false,
        val sdReady: Boolean = false,
        val error: Boolean = false,
        val ready: Boolean = false,
        val finishing: Boolean = false,
        val resuming: Boolean = false,
        val closedOrError: Boolean = false,
        val starting: Boolean = false,
    ) {
        fun isPrinting() = listOf(printing, paused, pausing, cancelling).any { it }
        fun isOperational() = listOf(operational).any { it }
        fun isStarting() = listOf(starting).any { it }
        fun isError() = listOf(error, closedOrError).any { it }
    }
}