package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.models.login.LoginBody
import de.crysxd.octoapp.engine.moonraker.exception.NotSupportedOnMoonrakerException

internal class MoonrakerLoginApi : LoginApi {
    override suspend fun passiveLogin(body: LoginBody) = throw NotSupportedOnMoonrakerException("login")
}
