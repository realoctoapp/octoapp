package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoToolCommand {

    @Serializable
    data class SetTargetTemperature(val targets: Map<String, Float>) : OctoToolCommand() {
        val command = "target"
    }

    @Serializable
    data class SetTemperatureOffset(val offsets: Map<String, Float>) : OctoToolCommand() {
        val command = "offset"
    }

    @Serializable
    data class SelectTool(val tool: String) : OctoToolCommand() {
        val command = "select"
    }

    @Serializable
    data class ExtrudeFilament(val amount: Int, val speed: Float? = null) : OctoToolCommand() {
        val command = "extrude"
    }
}

