package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerJobList(
    @SerialName("count") val count: Int? = null,
    @SerialName("jobs") val jobs: List<Job>? = null
) {
    @Serializable
    data class Job(
        @SerialName("end_time") val endTime: MoonrakerInstant? = null,
        @SerialName("exists") val exists: Boolean? = null,
        @SerialName("filament_used") val filamentUsed: Float? = null,
        @SerialName("filename") val filename: String? = null,
        @SerialName("job_id") val jobId: String? = null,
        @SerialName("metadata") val metadata: Metadata? = null,
        @SerialName("print_duration") val printDuration: Float? = null,
        @SerialName("start_time") val startTime: MoonrakerInstant? = null,
        @SerialName("status") val status: Status? = null,
        @SerialName("total_duration") val totalDuration: Float? = null
    ) {
        @Serializable
        data class Metadata(
            @SerialName("uuid") val uuid: String? = null
        )

        enum class Status {
            @SerialName("completed")
            Completed
        }
    }
}