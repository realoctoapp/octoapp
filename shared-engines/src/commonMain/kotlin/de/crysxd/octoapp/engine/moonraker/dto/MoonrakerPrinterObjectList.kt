package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerPrinterObjectList(
    @SerialName("objects") val objects: List<String>? = null
)