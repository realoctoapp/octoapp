package de.crysxd.octoapp.engine

import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileReference

internal interface EventSink {
    suspend fun injectInterpolatedEvent(event: (Message.Current?) -> Message.Event)
    suspend fun injectInterpolatedPrintStart(file: FileReference.File)
    suspend fun injectInterpolatedTemperatureTarget(targets: Map<String, Float>)
    suspend fun emitEvent(e: Event)
}