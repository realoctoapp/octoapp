package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.OctoEverywhereApi
import de.crysxd.octoapp.engine.models.remote.OctoEverywhereStatus
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDatabaseItem
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerDatabaseGetItemParams
import io.github.aakira.napier.Napier
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.jsonPrimitive

internal class MoonrakerOctoEverywhereApi(
    private val requestHandler: MoonrakerRequestHandler
) : OctoEverywhereApi {

    override suspend fun getInfo() = requestHandler.sendRequest<MoonrakerDatabaseItem>(
        method = "server.database.get_item",
        params = MoonrakerServerDatabaseGetItemParams(
            namespace = "octoeverywhere",
            key = "public"
        )
    ).let { item ->
        try {
            OctoEverywhereStatus(
                pluginVersion = item.value["pluginVersion"]?.jsonPrimitive?.contentOrNull,
                printerId = item.value["printerId"]?.jsonPrimitive?.contentOrNull,
            )
        } catch (e: Exception) {
            Napier.e(tag = "MoonrakerOctoEverywhereApi", message = "Failed to handle database item")
            OctoEverywhereStatus()
        }
    }
}