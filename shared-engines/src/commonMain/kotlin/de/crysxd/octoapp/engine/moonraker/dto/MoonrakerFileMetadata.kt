package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerFileMetadata(
    @SerialName("estimated_time") val estimatedTime: Float? = null,
    @SerialName("filament_name") val filamentName: String? = null,
    @SerialName("filament_total") val filamentTotal: Float? = null,
    @SerialName("filament_type") val filamentType: String? = null,
    @SerialName("filament_weight_total") val filamentWeightTotal: Float? = null,
    @SerialName("filename") val filename: String? = null,
    @SerialName("first_layer_bed_temp") val firstLayerBedTemp: Float? = null,
    @SerialName("first_layer_chamber_temp") val firstLayerChamberTemp: Float? = null,
    @SerialName("first_layer_extr_temp") val firstLayerExtrTemp: Float? = null,
    @SerialName("first_layer_height") val firstLayerHeight: Float? = null,
    @SerialName("gcode_end_byte") val gcodeEndByte: Long? = null,
    @SerialName("gcode_start_byte") val gcodeStartByte: Long? = null,
    @SerialName("job_id") val jobId: String? = null,
    @SerialName("layer_height") val layerHeight: Float? = null,
    @SerialName("modified") val modified: MoonrakerInstant? = null,
    @SerialName("nozzle_diameter") val nozzleDiameter: Float? = null,
    @SerialName("object_height") val objectHeight: Float? = null,
    @SerialName("print_start_time") val printStartTime: Float? = null,
    @SerialName("size") val size: Long? = null,
    @SerialName("slicer") val slicer: String? = null,
    @SerialName("slicer_version") val slicerVersion: String? = null,
    @SerialName("thumbnails") val thumbnails: List<Thumbnail>? = null,
    @SerialName("uuid") val uuid: String? = null
) {
    @Serializable
    data class Thumbnail(
        @SerialName("height") val height: Int? = null,
        @SerialName("relative_path") val relativePath: String? = null,
        @SerialName("size") val size: Int? = null,
        @SerialName("width") val width: Int? = null
    )
}
