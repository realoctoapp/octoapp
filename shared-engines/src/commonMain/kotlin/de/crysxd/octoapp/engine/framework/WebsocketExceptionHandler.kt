package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.exceptions.WebSocketUnstableException
import de.crysxd.octoapp.engine.exceptions.WebsocketDeadException
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.octoprint.event.OctoWebsocketConfig
import de.crysxd.octoapp.sharedcommon.ext.isOrIsCausedBy
import io.github.aakira.napier.Napier
import io.ktor.client.network.sockets.ConnectTimeoutException
import io.ktor.http.Url
import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

internal class WebsocketExceptionHandler(
    private val reconnect: () -> Unit,
    private val disconnect: () -> Unit,
    private val connectionNeeded: () -> Boolean,
    private val probe: suspend () -> Unit,
    private val onWebsocketBroken: suspend (Throwable) -> Unit,
    private val eventSink: EventSink,
    private val logTag: String,
    private val baseUrlRotator: BaseUrlRotator,
    private val canChangeProtocolWhenBroken: Boolean = true,
) {
    private var reconnectCounter = 0
    private var currentBaseUrl: Url? = null
    private var currentWebsocketUrl: Url? = null
    private var lastMessageReceivedAt = Instant.DISTANT_PAST

    private val guaranteedWebsocketUrl get() = currentWebsocketUrl ?: currentBaseUrl ?: baseUrlRotator.activeUrl.value
    private val guaranteedBaseUrl get() = currentBaseUrl ?: baseUrlRotator.activeUrl.value

    fun notifyMessageReceived() {
        reconnectCounter = 0
        lastMessageReceivedAt = Clock.System.now()
    }

    fun notifyConnectionAttempted(baseUrl: Url, websocketUrl: Url) {
        currentBaseUrl = baseUrl
        currentWebsocketUrl = websocketUrl
    }

    suspend fun handleException(e: Throwable) {
        when {
            !connectionNeeded() -> {
                Napier.w(tag = logTag, message = "Ignoring exception, connection not needed", throwable = e)
                disconnect()
            }

            // OkHttp throws this message when the WS does abruptly disconnect, this is a known bug in octo4a
            // if we never received a single message (lastMessageReceivedAt == distant past)
            // This might be a false alarm by now, do not include the exception so the user is not alerted
            e.isOrIsCausedBy("EOFException") && lastMessageReceivedAt == Instant.DISTANT_PAST -> {
                Napier.e(tag = logTag, throwable = e, message = "EOF exception in WebSocket, connection not possible")
                onWebsocketBroken(IllegalStateException("Websocket fails to connect (known octo4a bug)"))
            }

            // When we fail to upgrade the websocket this has either one of those two Exception as a consequence
            e.isOrIsCausedBy("java.net.ProtocolException", "NSURLErrorDomain Code=-1011") -> {
                Napier.e(tag = logTag, throwable = e, message = "API exception in WebSocket, upgrade failed")
                onWebsocketBroken(createWebSocketUnstableException(e))
            }

            e is ConnectTimeoutException -> currentBaseUrl?.let {
                Napier.e(tag = logTag, throwable = e, message = "Forwarding ConnectionTimeoutException to BaseUrlRotator")
                baseUrlRotator.considerException(baseUrl = it, e = e)
                reconnect()
            }

            else -> {
                reconnectCounter++
                val timeSinceLast = Clock.System.now() - lastMessageReceivedAt
                val shouldReportDisconnect = reconnectCounter > 1 || timeSinceLast > OctoWebsocketConfig.shotNoMessageTimeout

                // Report disconnected after the second attempt
                // If the iOS app is closed and then reopened, we get a `WebSocketDeadException` but a old timeSinceLast
                if (shouldReportDisconnect) {
                    Napier.i(tag = logTag, throwable = e, message = "Reporting disconnect (timeSinceLast=$timeSinceLast)")
                    eventSink.emitEvent(Event.Disconnected(e, connectionAttemptCounter = reconnectCounter))
                }

                if (e is WebsocketDeadException) {
                    // This is the iOS variant of the octo4a WS bug
                    if (reconnectCounter > 1 && canChangeProtocolWhenBroken) {
                        // This means the websocket is connecting but not healthy...we should fall back on an alternate transport mode
                        Napier.e(tag = logTag, throwable = e, message = "Failed to establish a healthy connection after 2 successful connections, reporting broken")
                        onWebsocketBroken(createWebSocketUnstableException(e))
                    } else {
                        // Reconnect after a short delay, we already waited for messages so no spamming
                        Napier.w(tag = logTag, message = "Reconnecting after exception (${e::class.simpleName}: ${e.message}) [1]")
                        delay(3.seconds)
                        reconnect()
                    }
                } else {
                    // On iOS basic auth, broken remote connection, invalid API key cause
                    // a weird exception when they occur on the websocket (not a "invalid response code"), so we need to probe
                    // manually
                    if (e.isOrIsCausedBy("DarwinHttpRequestException")) {
                        Napier.i(tag = logTag, throwable = e, message = "Received error indicating invalid HTTP response code, probing...")
                        probe()
                    }

                    // Reconnect after delay to prevent spamming
                    // Needs min delay: https://github.com/square/okhttp/issues/7381
                    val delay = (1.seconds * (reconnectCounter - 1)).coerceAtMost(5.seconds).coerceAtLeast(1.seconds)
                    Napier.e(
                        tag = logTag,
                        throwable = e,
                        message = "Exception in WebSocket, retrying after ${delay.inWholeSeconds}s (attempt=$reconnectCounter, ${e::class.qualifiedName}: ${e.message})"
                    )
                    delay(delay)
                    Napier.w(tag = logTag, message = "Reconnecting after exception (${e::class.simpleName}:  ${e.message}) [2]")
                    reconnect()
                }
            }
        }
    }

    private fun createWebSocketUnstableException(cause: Throwable) = WebSocketUnstableException(
        webSocketUrl = guaranteedWebsocketUrl,
        webUrl = guaranteedBaseUrl,
        cause = cause,
    )
}