package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.power.PowerDevice.ControlMethod
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octohue.OctoHueCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.octohue.OctoHueResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class OctoHueApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = if (settings.plugins.octoHue != null) {
        val hasFullControl = availablePlugins?.get(OctoPlugins.OctoHue)?.let { version ->
            version.asVersion() >= "0.6.0".asVersion()
        } ?: false

        listOf(PowerDevice(this, hasFullControl = hasFullControl))
    } else {
        emptyList()
    }

    private suspend fun toggle() {
        request("togglehue")
    }

    private suspend fun turnOn() {
        request("turnon")
    }

    private suspend fun turnOff() {
        request("turnoff")
    }

    private suspend fun isOn() = request("getstate").on

    private suspend fun request(command: String) = baseUrlRotator.request { baseUrl ->
        httpClient.post {
            urlFromPath(baseUrl, "api", "plugin", OctoPlugins.OctoHue)
            setJsonBody(OctoHueCommand(command = command))
        }
    }.body<OctoHueResponse>()


    internal data class PowerDevice(
        private val owner: OctoHueApi,
        val hasFullControl: Boolean
    ) : IPowerDevice {
        override val id = "octohue"
        override val pluginId = OctoPlugins.OctoHue
        override val displayName = "OctoHue Light"
        override val pluginDisplayName = if (hasFullControl) "OctoHue" else "OctoHue, PLUGIN OUTDATED, UPDATE REQUIRED!"
        override val capabilities = listOf(IPowerDevice.Capability.Illuminate)
        override val controlMethods: List<ControlMethod> get() = listOfNotNull(ControlMethod.TurnOnOff.takeIf { hasFullControl }, ControlMethod.Toggle)
        override suspend fun turnOn() = if (hasFullControl) owner.turnOn() else throw IllegalStateException("OctoHue has only limited control")
        override suspend fun turnOff() = if (hasFullControl) owner.turnOff() else throw IllegalStateException("OctoHue has only limited control")
        override suspend fun isOn() = if (hasFullControl) owner.isOn() else null
        override suspend fun toggle() = owner.toggle()
    }
}