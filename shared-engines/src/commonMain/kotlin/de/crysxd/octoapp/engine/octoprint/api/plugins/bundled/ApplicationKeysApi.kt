package de.crysxd.octoapp.engine.octoprint.api.plugins.bundled

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyCheckResponse
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyRequest
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyResponse
import de.crysxd.octoapp.engine.octoprint.dto.plugins.applicationkeys.ApplicationKeyStatus
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.bodyAsText
import io.ktor.client.statement.request
import io.ktor.http.HttpStatusCode

class ApplicationKeysApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {

    suspend fun probe() = try {
        baseUrlRotator.request {
            httpClient.get {
                urlFromPath(baseUrl = it, "plugin", "appkeys", "probe")
            }
        }.status.value in 200..299
    } catch (e: PrinterApiException) {
        if (e.responseCode == 404) {
            false
        } else {
            throw e
        }
    }

    suspend fun request(appName: String) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "plugin", "appkeys", "request")
            setJsonBody(ApplicationKeyRequest(app = appName))
        }.body<ApplicationKeyResponse>()
    }

    suspend fun checkStatus(appToken: String) = try {
        baseUrlRotator.request {
            httpClient.get {
                urlFromPath(baseUrl = it, "plugin", "appkeys", "request", appToken)
            }
        }.let {
            if (it.status == HttpStatusCode.OK) {
                ApplicationKeyStatus.Granted(it.body<ApplicationKeyCheckResponse>().apiKey)
            } else {
                throw PrinterApiException(it.request.url, it.status.value, it.bodyAsText())
            }
        }
    } catch (e: PrinterApiException) {
        when (val code = e.responseCode) {
            404 -> ApplicationKeyStatus.DeniedOrTimedOut
            202 -> ApplicationKeyStatus.Pending
            else -> throw IllegalStateException("Unknown request state (${code})")
        }
    }
}