package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.remote.OctoEverywhereStatus

interface OctoEverywhereApi {
    suspend fun getInfo(): OctoEverywhereStatus
}