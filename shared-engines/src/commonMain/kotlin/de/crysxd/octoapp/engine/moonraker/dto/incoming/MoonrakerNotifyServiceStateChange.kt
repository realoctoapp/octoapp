package de.crysxd.octoapp.engine.moonraker.dto.incoming


import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerNotifyServiceStateChange(
    @SerialName("jsonrpc") val jsonrpc: String? = null,
    @SerialName("method") val method: String? = null,
    @SerialName("params") val params: List<Map<String, MoonrakerSystemInfo.SystemInfo.ServiceState>>,
) : MoonrakerIncomingMessage