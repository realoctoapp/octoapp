@file:UseSerializers(InstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.event

import de.crysxd.octoapp.engine.framework.json.InstantSerializer
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileOrigin
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJobInformation
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoProgressInformation
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterState
import kotlinx.datetime.Instant
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import kotlinx.serialization.json.JsonNames

interface OctoMessage {

    @Serializable
    data class Connected(
        val version: String? = null,
        @SerialName("config_hash") val configHash: String = "none",
        @SerialName("display_version") val displayVersion: String? = null
    ) : OctoMessage

    @Serializable
    data class Current(
        val logs: List<String> = emptyList(),
        val temps: List<OctoHistoricTemperatureData> = emptyList(),
        val state: OctoPrinterState.State = OctoPrinterState.State(),
        val progress: OctoProgressInformation = OctoProgressInformation(),
        val job: OctoJobInformation = OctoJobInformation(),
        val offsets: Map<String, Float>?,
        val serverTime: Instant,
        val isHistoryMessage: Boolean = false,
        val originHash: Int = 0,
    ) : OctoMessage

    @Serializable
    class ReAuthRequired : OctoMessage

    @Serializable
    class UnknownMessage : OctoMessage

    sealed class Event : OctoMessage {

        @Serializable
        data object Connecting : Event()

        @Serializable
        data object PrinterStateChanged : Event()

        @Serializable
        data object UpdatedFiles : Event()

        @Serializable
        data object PrintStarted : Event()

        @Serializable
        data object PrintResumed : Event()

        @Serializable
        data object PrintPausing : Event()

        @Serializable
        data object PrintPaused : Event()

        @Serializable
        data object PrintCancelling : Event()

        @Serializable
        data object PrintCancelled : Event()

        @Serializable
        data object PrintFailed : Event()

        @Serializable
        data object PrintDone : Event()

        @Serializable
        data object PrinterProfileModified : Event()

        @Serializable
        data class SettingsUpdated(
            @SerialName("config_hash") val configHash: String?
        ) : Event()

        @Serializable
        data object MovieRendering : Event()

        @Serializable
        data object MovieDone : Event()

        @Serializable
        data object MovieFailed : Event()

        @Serializable
        data object Disconnected : Event()

        @Serializable
        data class Unknown(val type: String) : Event()

        @Serializable
        @OptIn(ExperimentalSerializationApi::class)
        data class FirmwareData(
            @JsonNames("name", "firmwareName") val firmwareName: String?,
            val machineType: String?,
            val extruderCount: Int?
        ) : Event()

        @Serializable
        data class FileSelected(
            val origin: OctoFileOrigin,
            val name: String,
            val path: String
        ) : Event()

        @Serializable
        data class PrinterConnected(
            val baudrate: Int?,
            val port: String?
        ) : Event()
    }
}