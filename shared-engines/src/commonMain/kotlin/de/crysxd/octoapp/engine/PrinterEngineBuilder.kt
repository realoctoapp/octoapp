package de.crysxd.octoapp.engine

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.ExceptionInspector
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings

interface PrinterEngineBuilder {
    interface Scope {
        var baseUrls: List<String>
        var interpolateEvents: Boolean
        var exceptionInspector: ExceptionInspector
        var httpClientSettings: HttpClientSettings?
        var allowWebSocketTransport: Boolean
        var apiKey: String?
    }
}

internal interface PrinterEngineHttpClientSettings {
    val general: HttpClientSettings
    val baseUrlRotator: BaseUrlRotator
    val exceptionInspector: ExceptionInspector
    val apiKey: String?
}