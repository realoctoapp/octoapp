package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemCommandList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo
import de.crysxd.octoapp.engine.moonraker.ext.convertToMoonrakerComponentLabel

internal fun MoonrakerSystemInfo.mapCommands() = SystemCommandList(
    core = listOf(
        SystemCommand(
            name = "Shutdown Host",
            action = "machine.shutdown",
            confirmation = "Do you really want to shut down the host?",
            source = "host",
            type = SystemCommand.Type.Shutdown,
        ),
        SystemCommand(
            name = "Reboot Host",
            action = "machine.reboot",
            confirmation = "Do you really want to reboot the host?",
            source = "host",
            type = SystemCommand.Type.Reboot,
        ),
    ),
    custom = systemInfo?.availableServices?.map { service ->
        SystemCommand(
            name = "Restart ${service.convertToMoonrakerComponentLabel()}".replace(oldValue = "OctoApp", newValue = "OctoApp Companion", ignoreCase = true),
            action = "machine.services.restart/$service",
            confirmation = "Do you really want to restart ${service.convertToMoonrakerComponentLabel()}?",
            source = "service",
            type = SystemCommand.Type.Restart,
        )
    } ?: emptyList(),
)