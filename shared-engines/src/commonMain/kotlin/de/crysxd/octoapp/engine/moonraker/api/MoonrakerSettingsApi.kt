package de.crysxd.octoapp.engine.moonraker.api

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.engine.api.SettingsApi
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerEventHandler
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerBedMesh
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDatabaseItem
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFluiddDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerLegacyWebcamDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMainsailDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOctoAppDatabase
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPowerDevices
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerServerInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebcamList
import de.crysxd.octoapp.engine.moonraker.dto.getItem
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerDatabaseGetItemParams
import de.crysxd.octoapp.engine.moonraker.exception.MoonrakerDatabaseNamespaceNotFoundException
import de.crysxd.octoapp.engine.moonraker.ext.description
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerSettingsMapperInput
import de.crysxd.octoapp.engine.moonraker.mappers.map
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import kotlinx.serialization.SerializationException

internal class MoonrakerSettingsApi(
    private val requestHandler: MoonrakerRequestHandler,
    private val eventHandler: MoonrakerEventHandler,
    private val systemInfo: suspend () -> SystemInfo
) : SettingsApi {

    private val logTag = "MoonrakerSettingsApi"

    override suspend fun getSettings(): Settings = withContext(Dispatchers.SharedIO) {
        //region Fire all requests
        val deferredSystemInfo = async {
            systemInfo()
        }
        val deferredWebcams = asyncWithFallback(fallback = MoonrakerWebcamList()) {
            requestHandler.sendRequest<MoonrakerWebcamList>(
                method = "server.webcams.list"
            )
        }
        val deferredInfo = asyncWithFallback(fallback = MoonrakerServerInfo()) {
            requestHandler.sendRequest<MoonrakerServerInfo>(
                method = "server.info"
            )
        }
        val deferredBedMesh = asyncWithFallback<MoonrakerBedMesh?>(fallback = null) {
            eventHandler.status.value
                ?.items
                ?.getItem(Constants.PrinterObjects.BedMesh)
                ?: let {
                    val x = requestHandler.sendRequest<MoonrakerPrinterObjectsQuery>(
                        method = "printer.objects.query",
                        params = MoonrakerPrinterObjectsQueryParams(
                            objects = mapOf(Constants.PrinterObjects.BedMesh to null)
                        )
                    )
                    x.status?.getItem(Constants.PrinterObjects.BedMesh)
                }
        }
        val deferredPrinterObjects = async {
            eventHandler.status.value?.items?.items?.keys?.toList()?.takeUnless { it.isEmpty() }
                ?: requestHandler.sendRequest<MoonrakerPrinterObjectList>(method = "printer.objects.list").objects
                ?: emptyList()
        }
        val deferredFluidd = asyncWithFallback(fallback = null) { getDatabase<MoonrakerFluiddDatabase>("fluidd") }
        val deferredMainsail = asyncWithFallback(fallback = null) { getDatabase<MoonrakerMainsailDatabase>("mainsail") }
        val deferredOctoApp = asyncWithFallback(fallback = null) { getDatabase<MoonrakerOctoAppDatabase>("octoapp") }
        val legacyWebcamDatabase = asyncWithFallback(fallback = null) { getDatabase<MoonrakerLegacyWebcamDatabase>("webcams") }
        val deferredServiceState = asyncWithFallback(fallback = null) {
            eventHandler.companionServiceState.value?.also {
                Napier.d(tag = logTag, message = "Taking service state from eventHandler: ${it.description}")
            } ?: requestHandler.sendRequest<MoonrakerSystemInfo>(method = "machine.system_info").systemInfo?.serviceState?.also {
                Napier.d(tag = logTag, message = "Loaded new service state: ${it.description}")
            }
        }
        val powerDevices = asyncWithFallback(fallback = null) {
            try {
                requestHandler.sendRequest<MoonrakerPowerDevices>(
                    method = "machine.device_power.devices",
                    params = null,
                )
            } catch (e: PrinterApiException) {
                // 404 if not configured
                Napier.d(tag = logTag, message = "Power devices returned ${e.responseCode} -> using null")
                null
            }
        }
        //endregion
        //region Map
        val x = deferredBedMesh.await()
        MoonrakerSettingsMapperInput(
            mainsailDatabase = deferredMainsail.await(),
            fluiddDatabase = deferredFluidd.await(),
            webcamList = deferredWebcams.await(),
            octoAppDatabase = deferredOctoApp.await(),
            printerObjects = deferredPrinterObjects.await(),
            serviceStates = deferredServiceState.await(),
            bedMesh = x,
            powerDevices = powerDevices.await(),
            legacyWebcamDatabase = legacyWebcamDatabase.await(),
            info = deferredInfo.await(),
            preferredInterface = deferredSystemInfo.await().interfaceType,
        ).map()
        //endregion
    }

    // Moonraker doesn't have a settings hash, we randomize it to force load every time
    override suspend fun getSettingsHash() = uuid4().toString()

    private fun <T> CoroutineScope.asyncWithFallback(fallback: T, block: suspend () -> T) = async {
        try {
            block()
        } catch (e: Exception) {
            Napier.e(tag = logTag, message = "Async operation failed, using fallback", throwable = e)
            fallback
        }
    }

    private suspend inline fun <reified T : Any> getDatabase(namespace: String) = try {
        requestHandler.sendRequest<MoonrakerDatabaseItem>(
            method = "server.database.get_item",
            params = MoonrakerServerDatabaseGetItemParams(namespace = namespace)
        ).parseValue<T>()
    } catch (e: MoonrakerDatabaseNamespaceNotFoundException) {
        null
    } catch (e: SerializationException) {
        Napier.e(tag = logTag, message = "Failed to deserialize database $namespace", throwable = e)
        null
    }
}