package de.crysxd.octoapp.engine.octoprint.dto.plugins.tuya

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class TuyaCommand(
    override val command: String,
    val label: String
) : CommandBody

