package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileList

internal fun OctoFileList.map(origin: FileOrigin) = FileList(
    total = total,
    free = free,
    files = files.map { it.map() }.let { children ->
        FileObject.Folder(
            children = children,
            name = "/",
            path = "/",
            size = total,
            origin = origin
        )
    }
)