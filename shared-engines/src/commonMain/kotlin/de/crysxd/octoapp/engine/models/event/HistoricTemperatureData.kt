package de.crysxd.octoapp.engine.models.event

import de.crysxd.octoapp.engine.models.printer.ComponentTemperature

data class HistoricTemperatureData(
    val time: Long,
    val components: Map<String, ComponentTemperature> = emptyMap()
)