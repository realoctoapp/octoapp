package de.crysxd.octoapp.engine.octoprint.dto.message

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import kotlinx.serialization.Serializable

@Serializable
data class NgrokPluginMessage(
    val tunnel: String? = null,
) : OctoMessage, Message