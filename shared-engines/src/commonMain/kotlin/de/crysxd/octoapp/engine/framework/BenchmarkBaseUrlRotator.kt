package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterUnavailableException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.framework.isLocalNetwork
import de.crysxd.octoapp.sharedcommon.url.isBasedOn
import io.github.aakira.napier.Napier
import io.ktor.client.network.sockets.SocketTimeoutException
import io.ktor.client.plugins.HttpRequestTimeoutException
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds


class BenchmarkBaseUrlRotator(
    private val baseUrls: List<Url>,
) : BaseUrlRotator {

    private val tag = "BenchmarkBaseUrlRotator"
    private var currentlyActive: () -> Boolean = { false }
    private var onConnectionChange: () -> Unit = {}
    private var probeConnection: suspend (BaseUrlRotator) -> Boolean = { throw IllegalStateException("Probe not ready") }

    private var lastPing = Instant.DISTANT_PAST
    private val mutex = Mutex()
    private var lastException: Throwable? = null
    private val mutableActiveUrl = MutableStateFlow(baseUrls[0])
    override val activeUrl = mutableActiveUrl.asStateFlow()
    private var activeUrlIndex: Int = 0
        set(value) {
            mutableActiveUrl.update { baseUrls[value] }
            field = value
        }

    init {
        require(baseUrls.isNotEmpty()) { "Need at least one base URL, none given!" }
        Napier.d(tag = tag, message = "Creating BenchmarkBaseUrlRotator with selection: $baseUrls")
        Napier.i(tag = tag, message = "Initial base: ${baseUrls[0]} (${baseUrls.size - 1} alternatives)")
    }

    override suspend fun <T> request(block: suspend (Url) -> T): T {
        performBenchmark()

        val baseUrlsForRequest = ((activeUrlIndex until baseUrls.size) + (0 until activeUrlIndex)).map { baseUrls[it] }
        var exception: Throwable = IllegalStateException("Should not be used")

        return baseUrlsForRequest.firstNotNullOfOrNull { baseUrl ->
            try {
                block(baseUrl)
            } catch (e: CancellationException) {
                throw e
            } catch (e: Exception) {
                if (e.canBeSolvedByBaseUrlChange()) {
                    exception = e
                    considerException(baseUrl, e)
                    null
                } else {
                    Napier.d(tag = tag, message = "Caught '${e::class.simpleName}' which can't be solved by a URL change. Escalating. (${e.message})")
                    throw e
                }
            }
        } ?: let {
            Napier.e("All base Urls failed. Reporting last exception")
            throw exception
        }
    }

    private suspend fun performBenchmark(forced: Boolean = false) = mutex.withLock {
        val timeSinceLast = Clock.System.now() - lastPing
        when {
            // Only one base url...skip
            baseUrls.size <= 1 -> Unit

            // Not forced and recently checked...skip
            !forced && 15.minutes > timeSinceLast -> Napier.v(tag = tag, message = "Benchmark was ${Clock.System.now() - lastPing} ago, skipping")

            // Forced but we literally finished a second ago...skip
            forced && 100.milliseconds > timeSinceLast -> Napier.v(tag = tag, message = "Benchmark was ${Clock.System.now() - lastPing} ago, skipping forced")

            // Forced or outdated...fire!
            else -> {
                Napier.d(tag = tag, message = "Benchmark required (forced=$forced, last=$lastPing)")
                doPerformBenchmark()
            }
        }
    }

    private suspend fun doPerformBenchmark() = withContext(Dispatchers.SharedIO) {
        val result = MutableStateFlow(mapOf<Url, Duration>())
        val timeout = 5.seconds
        Napier.w(tag = tag, message = "Performing benchmark on all")

        // Measure connection speed to all
        val jobs = baseUrls.map { url ->
            async { probeSingle(url, result) }
        }

        try {
            // Wait for first
            val start = Clock.System.now()
            val first = withTimeoutOrNull(timeout) {
                result.filter { it.isNotEmpty() }.first().minByOrNull { it.value }?.toPair()?.first
            } ?: let {
                Napier.d(tag = tag, message = "No test returned in time, preferring non local URL")
                baseUrls.firstOrNull { !it.isLocalNetwork() } ?: baseUrls[0]
            }

            // If best is  not local network and a local network is running, wait a bit more
            // as we'd prefer local network over a non-local connection even if it appears to be faster for some reason
            val selected: Url = if (!first.isLocalNetwork() && baseUrls.any { it.isLocalNetwork() }) {
                val elapsed = (Clock.System.now() - start)
                val localNetworkBias = (1.seconds - elapsed).coerceAtLeast(0.5.seconds)
                Napier.d(tag = tag, message = "First result is not local, waiting additional $localNetworkBias for more results")
                withTimeoutOrNull(localNetworkBias) {
                    jobs.forEach { it.join() }

                    // Now we should have another url as all jobs complete. We give the local Url a advantage, so lets see which one is best now!
                    fun Url.bias(): Duration = if (isLocalNetwork()) localNetworkBias else 0.milliseconds
                    val resultList = result.value.toList()
                    resultList.minByOrNull { (url, duration) -> (duration - url.bias()) }?.first ?: first
                } ?: first
            } else {
                first
            }

            // Publish result
            val currentBase = baseUrls[activeUrlIndex]
            Napier.i(tag = tag, message = "Benchmark result: $selected is best URL")
            if (currentBase != selected) {
                Napier.i(tag = tag, message = "Switching: $currentBase -> $selected")
                activeUrlIndex = baseUrls.indexOf(selected)
                onConnectionChange()
            } else {
                Napier.i(tag = tag, message = "Sticking with $selected, no change")
            }
            lastPing = Clock.System.now()
        } finally {
            // Ensure everything is cancelled
            jobs.forEach {
                it.cancel()
            }
            Napier.i(tag = tag, message = "Benchmark done")

        }
    }

    private suspend fun probeSingle(url: Url, result: MutableStateFlow<Map<Url, Duration>>) {
        val start = Clock.System.now()

        try {
            Napier.d(tag = tag, message = "Probing $url")
            if (probeConnection(NoopBaseUrlRotator(url))) {
                val duration = Clock.System.now() - start
                Napier.d(tag = tag, message = "Result for: $url: $duration")
                result.update { current ->
                    current.toMutableMap().also {
                        it[url] = duration
                    }
                }
            } else {
                Napier.v(tag = tag, message = "Probing to $url failed")
            }
        } catch (e: CancellationException) {
            // Nothing
        } catch (e: Exception) {
            Napier.v(tag = tag, message = "Probing to $url failed: ${e::class.simpleName}: ${e.message}")
        }
    }

    override suspend fun considerException(baseUrl: Url, e: Throwable) = mutex.withLock {
        if (e.canBeSolvedByBaseUrlChange()) {
            val currentBase = baseUrls[activeUrlIndex]
            if (baseUrl == currentBase && e != lastException) {
                lastException = e
                activeUrlIndex = (activeUrlIndex + 1) % baseUrls.size
                val newBase = baseUrls[activeUrlIndex]
                Napier.d(tag = tag, message = "Caught '${e::class.simpleName}', can be solved by base URL change. Re-attempting...")
                Napier.i(tag = tag, message = "Switching: $currentBase -> $newBase")
            } else {
                Napier.d(tag = tag, message = "Already switched from $baseUrl -> $currentBase")
            }
        } else {
            Napier.d(tag = tag, message = "Got '${e::class.simpleName}' reported for consideration which can't be solved by a URL change. Ignoring.")
        }
    }

    private fun Throwable.canBeSolvedByBaseUrlChange(): Boolean = if (this is PrinterUnavailableException) {
        // Usually it's always this on
        if (this.webUrl.isBasedOn(activeUrl.value)) {
            Napier.w(tag = tag, message = "Switching after caught ${PrinterUnavailableException::class.simpleName} which is based on ${activeUrl.value}: $webUrl")
            true
        } else {
            Napier.w(tag = tag, message = "Caught ${PrinterUnavailableException::class.simpleName} which is based not based on ${activeUrl.value}: $webUrl")
            false
        }
    } else if (
    // CIO Engine
        this::class.qualifiedName == "java.nio.channels.UnresolvedAddressException" ||

        // OkHTTP Engine
        this::class.qualifiedName == "java.net.ConnectException" ||
        this::class.qualifiedName == "java.net.UnknownHostException" ||
        this::class.qualifiedName == "java.net.SocketException" ||

        // Darwin Engine
        this.message?.contains("NSURLErrorDomain") == true ||
        this.message?.contains("NSErrorFailingURLStringKey") == true ||

        // Generic
        this::class.qualifiedName == "io.ktor.client.network.sockets.ConnectTimeoutException" ||
        this::class.qualifiedName == "io.ktor.client.plugins.ConnectTimeoutException" ||
        this is HttpRequestTimeoutException ||
        this is SocketTimeoutException ||

        // Super special things that should not be needed but are
        message == "Unexpected status line: <HEAD><TITLE>Tunnel Connection Failed</TITLE></HEAD>"
    ) {
        true
    } else {
        ((this as? NetworkException)?.originalCause ?: cause)?.canBeSolvedByBaseUrlChange() == true
    }

    override suspend fun considerUpgradingConnection(trigger: String, forced: Boolean) {
        if (currentlyActive()) {
            Napier.d(tag = tag, message = "Considering URL upgrade (trigger=$trigger, forced=$forced)")
            performBenchmark(forced = forced)
        } else {
            Napier.d(tag = tag, message = "Not active, skipping URL upgrade (trigger=$trigger, forced=$forced)")
        }
    }

    override fun consumeActiveFlow(
        active: StateFlow<Boolean>,
        probe: suspend (BaseUrlRotator) -> Boolean,
        onChange: () -> Unit,
    ) {
        currentlyActive = { active.value }
        onConnectionChange = onChange
        probeConnection = probe

    }
}
