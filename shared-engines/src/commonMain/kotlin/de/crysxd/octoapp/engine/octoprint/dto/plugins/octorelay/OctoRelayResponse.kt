package de.crysxd.octoapp.engine.octoprint.dto.plugins.octorelay

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoRelayResponse(
    val status: Boolean = false
)