package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerInstantSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlin.math.roundToLong

typealias MoonrakerInstant = @Serializable(with = MoonrakerInstantSerializer::class) Instant

fun Instant.Companion.fromEpochSeconds(secs: Double) = fromEpochMilliseconds((secs * 1000).roundToLong())