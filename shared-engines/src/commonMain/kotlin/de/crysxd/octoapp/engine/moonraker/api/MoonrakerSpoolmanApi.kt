package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.BaseMoonrakerSpoolmanApi
import de.crysxd.octoapp.engine.dto.SpoolmanSpool
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerEventHandler
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSpoolmanActiveSpool
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSpoolmanProxyResponse
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerSpoolmanProxyParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerSpoolmanSelectSpoolParams

internal class MoonrakerSpoolmanApi(
    private val requestHandler: MoonrakerRequestHandler,
    private val eventHandler: MoonrakerEventHandler
) : BaseMoonrakerSpoolmanApi() {

    override suspend fun getToolIndex(extruderComponent: String) = eventHandler.status.value
        ?.items
        ?.filter { (_, value) -> value is MoonrakerExtruder }
        ?.keys
        ?.sorted()
        ?.indexOf(extruderComponent)
        ?: -1

    override suspend fun loadSelection() = requestHandler.sendRequest<MoonrakerSpoolmanActiveSpool>(
        method = "server.spoolman.get_spool_id",
    ).let { active ->
        // At the moment only one extruder is supported, we still return selection for all
        // but only set the active for the "main" extruder
        mapOf(Constants.PrinterObjects.Extruder to active.spoolId)
    }

    override suspend fun loadSpools(): List<SpoolmanSpool> = requestHandler.sendRequest<MoonrakerSpoolmanProxyResponse>(
        method = "server.spoolman.proxy",
        params = MoonrakerSpoolmanProxyParams(
            v2Response = true,
            path = "/v1/spool",
            requestMethod = "GET",
        )
    ).let { response ->
        if (response.error != null) {
            throw Exception("Got error from Spoolman Proxy: ${response.error.message} (${response.error.statusCode})")
        } else {
            response.response ?: emptyList()
        }

    }

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId, extruderComponent: String) = requestHandler.sendRequest<Unit>(
        method = "server.spoolman.post_spool_id",
        params = MoonrakerSpoolmanSelectSpoolParams(
            spoolId = uniqueMaterialId.id.toIntOrNull(),
        )
    )
}