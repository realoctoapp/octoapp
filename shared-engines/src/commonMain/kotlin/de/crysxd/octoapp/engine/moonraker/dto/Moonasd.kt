package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer


typealias Moonasd = @Serializable(with = SortedMapSerializer::class) Map<String, MoonrakerPowerDevices.Status>

class SortedMapSerializer : KSerializer<Map<String, MoonrakerPowerDevices.Status>> by MapSerializer(String.serializer(), MoonrakerPowerDevices.Status.serializer())

