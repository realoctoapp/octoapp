package de.crysxd.octoapp.engine.framework

import io.ktor.util.AttributeKey

object Constants {
    const val ApiKeyHeader = "X-Api-Key"
    val SuppressApiKey = AttributeKey<Boolean>("suppressApiKey")
    val SuppressBrokenSetup = AttributeKey<Boolean>("suppressBrokenSetup")
}