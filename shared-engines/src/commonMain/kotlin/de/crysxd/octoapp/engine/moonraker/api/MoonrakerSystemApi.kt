package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.SystemApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerSystemInfo
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerMachineServicesRestartParams
import de.crysxd.octoapp.engine.moonraker.mappers.mapCommands
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock

internal class MoonrakerSystemApi(
    private val requestHandler: MoonrakerRequestHandler,
    private val httpClient: () -> HttpClient,
    private val baseUrlRotator: BaseUrlRotator,
) : SystemApi {

    private var cachedInterface: SystemInfo.Interface? = null
    private var cachedInterfaceLock = Mutex()

    override suspend fun getSystemCommands() = requestHandler.sendRequest<MoonrakerSystemInfo>("machine.system_info").mapCommands()

    override suspend fun getSystemInfo() = SystemInfo(
        generatedAt = Clock.System.now(),
        printerFirmware = "Klipper",
        printerFirmwareFamily = SystemInfo.FirmwareFamily.Klipper,
        safeMode = false,
        interfaceType = cachedInterfaceLock.withLock {
            cachedInterface ?: baseUrlRotator.request {
                val body = httpClient().get(it).bodyAsText()
                when {
                    body.contains("Mainsail", ignoreCase = true) -> SystemInfo.Interface.MoonrakerMainsail
                    body.contains("Fluidd", ignoreCase = true) -> SystemInfo.Interface.MoonrakerFluidd
                    else -> SystemInfo.Interface.MoonrakerMixed
                }
            }.also {
                cachedInterface = it
            }
        },
        capabilities = listOf(
            SystemInfo.Capability.Macro,
            SystemInfo.Capability.ResetZOffset,
            SystemInfo.Capability.RealTimeStats,
            SystemInfo.Capability.AbsoluteToolheadMove,
        )
    )

    override suspend fun executeSystemCommand(command: SystemCommand) {
        val restartCommand = "machine.services.restart"
        val splitCommand = command.action.split("/").first()
        if (splitCommand == restartCommand) {
            requestHandler.sendRequest<Unit>(splitCommand, MoonrakerMachineServicesRestartParams(command.action.removePrefix("$restartCommand/")))
        } else {
            requestHandler.sendRequest(command.action)
        }
    }
}