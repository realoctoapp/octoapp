package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.OctoAppCompanionApi
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDatabaseItem
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerOctoAppDatabase
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerDatabasePostItemParams
import de.crysxd.octoapp.engine.octoprint.dto.plugins.companion.AppRegistration
import kotlinx.datetime.Clock
import kotlin.time.Duration.Companion.days

internal class MoonrakerOctoAppCompanionApi(
    private val requestHandler: MoonrakerRequestHandler
) : OctoAppCompanionApi {
    override suspend fun registerApp(
        registration: AppRegistration
    ) {
        requestHandler.sendRequest<MoonrakerDatabaseItem>(
            method = "server.database.post_item",
            params = MoonrakerServerDatabasePostItemParams(
                namespace = "octoapp",
                key = "apps.${registration.fcmToken}",
                value = MoonrakerOctoAppDatabase.App(
                    instanceId = registration.instanceId,
                    displayName = registration.displayName,
                    appBuild = registration.appBuild,
                    appLanguage = registration.appLanguage,
                    appVersion = registration.appVersion,
                    displayDescription = registration.displayDescription,
                    expireAt = (Clock.System.now() + 14.days).epochSeconds.toDouble(),
                    fcmToken = registration.fcmToken,
                    fcmTokenFallback = registration.fcmTokenFallback,
                    lastSeenAt = Clock.System.now().epochSeconds.toDouble(),
                    model = registration.model,
                    excludeNotifications = registration.excludeNotifications,
                )
            )
        )
    }
}