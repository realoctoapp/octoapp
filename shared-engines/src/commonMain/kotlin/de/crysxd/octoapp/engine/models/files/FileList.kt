package de.crysxd.octoapp.engine.models.files

data class FileList(
    val files: FileReference.Folder,
    val free: Long? = null,
    val total: Long? = null
)