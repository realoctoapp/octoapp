package de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoFilamentManagerList(
    val spools: List<OctoFilamentManagerSpool>
)