package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.OctoConstants.toolLabels
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage

internal fun OctoMessage.map(): Message = when (this) {
    is OctoMessage.Current -> map()
    is OctoMessage.Connected -> map()
    is OctoMessage.Event -> map()
    is Message -> this
    is OctoMessage.ReAuthRequired,
    is OctoMessage.UnknownMessage -> Message.UnknownMessage

    else -> throw Error("Missing mapper for ${this::class.qualifiedName}")
}

internal fun OctoMessage.Event.map(): Message.Event = when (this) {
    is OctoMessage.Event.FileSelected -> Message.Event.FileSelected(
        origin = origin.map(),
        name = name,
        path = path,
    )

    is OctoMessage.Event.FirmwareData -> Message.Event.FirmwareData(
        firmwareName = firmwareName,
        machineType = machineType,
        extruderCount = extruderCount,
    )

    is OctoMessage.Event.PrinterConnected -> Message.Event.PrinterConnected(
        baudrate = baudrate,
        port = port
    )

    is OctoMessage.Event.Unknown -> Message.Event.Unknown(
        type = type
    )

    is OctoMessage.Event.SettingsUpdated -> Message.Event.SettingsUpdated(
        configHash = configHash
    )

    OctoMessage.Event.Disconnected -> Message.Event.Disconnected
    OctoMessage.Event.Connecting -> Message.Event.Connecting
    OctoMessage.Event.MovieDone -> Message.Event.MovieDone
    OctoMessage.Event.MovieFailed -> Message.Event.MovieFailed
    OctoMessage.Event.MovieRendering -> Message.Event.MovieRendering
    OctoMessage.Event.PrintCancelled -> Message.Event.PrintCancelled
    OctoMessage.Event.PrintCancelling -> Message.Event.PrintCancelling
    OctoMessage.Event.PrintDone -> Message.Event.PrintDone
    OctoMessage.Event.PrintFailed -> Message.Event.PrintFailed
    OctoMessage.Event.PrintPaused -> Message.Event.PrintPaused
    OctoMessage.Event.PrintPausing -> Message.Event.PrintPausing
    OctoMessage.Event.PrintResumed -> Message.Event.PrintResumed
    OctoMessage.Event.PrintStarted -> Message.Event.PrintStarted
    OctoMessage.Event.PrinterProfileModified -> Message.Event.PrinterProfileModified
    OctoMessage.Event.PrinterStateChanged -> Message.Event.PrinterStateChanged
    OctoMessage.Event.UpdatedFiles -> Message.Event.UpdatedFiles
}

internal fun OctoMessage.Connected.map() = Message.Connected(
    version = version,
    displayVersion = displayVersion,
    configHash = configHash,
)

internal fun OctoMessage.Current.map() = Message.Current(
    logs = logs,
    temps = temps.map { it.map() },
    tempLabels = toolLabels,
    tempMaxs = mapOf(
        "tool0" to 285f,
        "tool1" to 285f,
        "tool2" to 285f,
        "tool3" to 285f,
        "tool4" to 285f,
        "tool5" to 285f,
        "tool6" to 285f,
        "tool7" to 285f,
        "tool8" to 285f,
        "tool9" to 285f,
        "tool10" to 285f,
        "bed" to 120f,
        "chamber" to 100f,
    ),
    tempEditables = listOf(
        "tool0",
        "tool1",
        "tool2",
        "tool3",
        "tool4",
        "tool5",
        "tool6",
        "tool7",
        "tool8",
        "tool9",
        "chamber",
        "bed"
    ),
    state = state.map(),
    progress = progress.map(),
    job = job.map(),
    isHistoryMessage = isHistoryMessage,
    offsets = offsets,
    serverTime = serverTime,
)