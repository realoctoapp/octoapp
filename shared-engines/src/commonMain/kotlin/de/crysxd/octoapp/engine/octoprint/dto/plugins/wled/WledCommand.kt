package de.crysxd.octoapp.engine.octoprint.dto.plugins.wled

import de.crysxd.octoapp.engine.octoprint.CommandBody

internal data class WledCommand(
    override val command: String
) : CommandBody