package de.crysxd.octoapp.engine.octoprint.dto.plugins.cancelobject

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CancelObjectCommand(
    @SerialName("cancelled") val objectId: Int
) : CommandBody {
    override val command: String = "cancel"
}