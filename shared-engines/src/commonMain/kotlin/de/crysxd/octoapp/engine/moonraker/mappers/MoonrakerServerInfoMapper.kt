package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.version.VersionInfo
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerServerInfo

internal fun MoonrakerServerInfo.map() = VersionInfo(
    apiVersion = apiVersionString,
    serverVersionText = moonrakerVersion,
    severVersion = moonrakerVersion,
)