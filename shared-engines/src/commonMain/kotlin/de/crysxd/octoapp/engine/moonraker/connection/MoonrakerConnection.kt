package de.crysxd.octoapp.engine.moonraker.connection

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.WebsocketExceptionHandler
import de.crysxd.octoapp.engine.framework.WebsocketWatchdog
import de.crysxd.octoapp.engine.framework.guessConnectionType
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.moonraker.MoonrakerEngineBuilder
import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerRpcRequest
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerIncomingMessage
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyGcodeResponse
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyHistoryChange
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyKlippyDisconnected
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyKlippyReady
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyKlippyShutdown
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyServiceStateChange
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyStatus
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerProcStateUpdate
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerUnparsedRpcResponse
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerWebcamChange
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerRpcParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerConnectionIdentifyParams
import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerIncomingMessageSerializer
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.config.LogLevel
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.plugins.websocket.DefaultClientWebSocketSession
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.client.request.head
import io.ktor.http.URLProtocol
import io.ktor.http.Url
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlin.time.Duration.Companion.seconds

internal class MoonrakerConnection internal constructor(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: () -> HttpClient,
    private val parentJob: Job = SupervisorJob(),
    private val platform: Platform,
    private val apiKey: String?,
    settings: MoonrakerEngineBuilder.HttpClientSettings,
    probe: suspend () -> Unit,
) {

    private val requests = MutableSharedFlow<MoonrakerRpcRequest<MoonrakerRpcParams>>()
    private val responses = MutableSharedFlow<Pair<MoonrakerUnparsedRpcResponse, String>>()
    private lateinit var scope: CoroutineScope

    private val logTag = settings.general.logTag
    private var messageCounter = 0
    private var connected = MutableStateFlow(false)
    private var disconnectJob: Job? = null
    private var currentException = MutableStateFlow<Throwable?>(null)

    private val defaultRequestHandler = DefaultMoonrakerRequestHandler(
        connection = this,
        logTag = logTag,
        timeout = settings.general.timeouts.requestTimeout,
        url = { baseUrlRotator.activeUrl.value },
        debugLogging = settings.general.logLevel == LogLevel.Debug,
        disconnect = ::disconnectIfNotNeeded,
        currentIOException = { currentException.value },
        connect = {
            // If we are connected we can reuse the connection, no change needed
            connect(reconnectIfConnected = false, cause = "request")
        }
    )
    private val internalEventHandler = MoonrakerEventHandler(
        connect = {
            // When the event handler connects we need to reset the connection if already established so we start collecting events
            connect(reconnectIfConnected = true, cause = "event-flow")
        },
        disconnect = ::disconnectIfNotNeeded,
        logTag = "$logTag/Event",
        requestHandler = defaultRequestHandler,
        connectionScope = { scope }
    )
    private val exceptionHandler = WebsocketExceptionHandler(
        reconnect = {
            if (defaultRequestHandler.connectionNeeded || internalEventHandler.connectionNeeded) {
                // Full connection reset
                connect(cause = "exception-reconnect")
            } else {
                // Not needed? Just disconnect
                disconnect("exception-reconnect-not-needed")
            }
        },
        connectionNeeded = { eventHandler.connectionNeeded || defaultRequestHandler.connectionNeeded },
        logTag = "$logTag/Exception",
        baseUrlRotator = baseUrlRotator,
        eventSink = internalEventHandler,
        onWebsocketBroken = {
            // Special case for Moonraker. We only make WS requests so a issue with OctoEverywhere/Obico etc doesn't surface properly because the WS stack wraps HTTP exceptions
            // in a ProtocolException. If we get one, make a normal HTTP request to trigger the underlying issue
            baseUrlRotator.request { baseUrl ->
                Napier.i(tag = logTag, message = "Performing HEAD check on $baseUrl...")
                httpClient().head(baseUrl).status
            }
        },
        canChangeProtocolWhenBroken = false,
        probe = probe,
        disconnect = ::disconnectIfNotNeeded
    )
    private val watchdog = WebsocketWatchdog(
        reconnect = { connect(cause = "watchdog-reconnect") },
        logTag = { "$logTag/Watchdog" },
        connectTimeout = settings.general.timeouts.connectionTimeout,
        noMessageTimeout = settings.general.timeouts.webSocketPingPongTimeout,
        onWebsocketBroken = { Napier.w(tag = logTag, message = "Watchdog considers this connection permanently broken!") },
        eventSink = internalEventHandler,
        countToConsiderBroken = Int.MAX_VALUE,
    )

    val requestHandler: MoonrakerRequestHandler = defaultRequestHandler
    val eventHandler: MoonrakerEventHandler = internalEventHandler

    private fun connect(reconnectIfConnected: Boolean = true, cause: String) {
        val wasConnected = !connected.compareAndSet(expect = false, update = true)
        val lightweight = eventHandler.isLightweight
        if (!reconnectIfConnected && wasConnected) {
            disconnectJob?.cancel("Connected")
            return Napier.d(tag = logTag, message = "Skipping connect, already connected (cause=$cause)")
        }

        Napier.i(tag = logTag, message = "Connecting... (cause=$cause)")
        ensureDisconnected()
        scope = CoroutineScope(SupervisorJob(parentJob) + Dispatchers.SharedIO + CoroutineExceptionHandler { _, throwable -> handleException(throwable) })
        messageCounter = 0
        internalEventHandler.resetConnection()

        scope.launch {
            baseUrlRotator.request { baseUrl ->
                Napier.i(tag = logTag, message = "Connecting using $baseUrl (lightweight=$lightweight)")
                if (!lightweight) {
                    watchdog.notifyConnectionStarted(scope)
                }
                connectWebsocket(baseUrl, lightweight)
            }
        }
    }

    private fun disconnectIfNotNeeded() {
        when {
            eventHandler.connectionNeeded -> Napier.i(tag = logTag, message = "Ignoring disconnect, event handler needs connection")
            defaultRequestHandler.connectionNeeded -> Napier.i(tag = logTag, message = "Ignoring disconnect, request handler needs connection")
            else -> disconnect(afterDelay = true, cause = "not needed")
        }
    }

    private fun disconnect(cause: String, afterDelay: Boolean = false) {
        // We need to check scope here as this might become a race condition when we first spin up everything
        // disconnect() is called because 0 event subscriptions and requests are active.
        if (afterDelay && ::scope.isInitialized) {
            if (disconnectJob?.isActive == true) return
            val disconnectDelay = 1.seconds
            Napier.d(tag = logTag, message = "Disconnecting in $disconnectDelay unless connected again (cause=$cause)")
            if (disconnectJob?.isActive != true) {
                disconnectJob = scope.launch {
                    try {
                        delay(disconnectDelay)
                        disconnect(afterDelay = false, cause = cause)
                    } catch (e: CancellationException) {
                        Napier.d(tag = logTag, message = "Delayed disconnect cancelled (${e.message})")
                        throw e
                    }
                }
            }
        } else {
            Napier.i(tag = logTag, message = "Disconnecting... (cause=$cause)")
            connected.update { false }
            ensureDisconnected()
        }
    }

    private fun ensureDisconnected() {
        disconnectJob?.cancel("Reconnected")
        if (::scope.isInitialized && scope.isActive) {
            scope.cancel("Disconnected")
        }
    }

    fun reconnect() {
        connect(
            cause = "external-reconnect-command",
            reconnectIfConnected = true
        )
    }

    fun destroy() {
        internalEventHandler.destroy()
        disconnect("destroy")
    }

    //region Websocket
    private suspend fun connectWebsocket(baseUrl: Url, lightweight: Boolean) = httpClient().webSocket(
        request = {
            urlFromPath(baseUrl = baseUrl, "websocket") {
                protocol = if (baseUrl.protocol == URLProtocol.HTTPS) URLProtocol.WSS else URLProtocol.WS
            }
        }
    ) {
        Napier.i(tag = logTag, message = "Websocket connected using $baseUrl")
        if (!lightweight) {
            watchdog.notifyConnectionEstablished(scope)
        }
        manageWebsocket(lightweight)
    }

    private suspend fun DefaultClientWebSocketSession.manageWebsocket(lightweight: Boolean) {
        sendRequestsAsync()
        receiveResponsesAsync()
        identifyServerConnection()

        // We are READY!
        Napier.v(tag = logTag, message = "IO is ready")
        defaultRequestHandler.repeatPendingRequests()
        internalEventHandler.initializeConnection(lightweight)

        if (lightweight) {
            val delay = 60.seconds
            Napier.i(tag = logTag, message = "Waiting $delay before updating information (lightweight connection)")
            delay(delay)
            connect(reconnectIfConnected = true, cause = "lightweight-update")
        } else {
            // Keep websocket alive
            scope.coroutineContext.job.join()
        }
    }

    private suspend fun identifyServerConnection() {
        if (!apiKey.isNullOrBlank()) {
            val request = MoonrakerRpcRequest(
                method = "server.connection.identify",
                params = MoonrakerServerConnectionIdentifyParams(
                    apiKey = apiKey,
                    clientName = "octoapp",
                    url = "https://gitlab.com/realoctoapp/octoap",
                    version = platform.appVersion,
                    type = "mobile"
                ) as MoonrakerRpcParams
            )

            Napier.i(tag = logTag, message = "API key given, identifying connection...")
            val response = sendRequest(request)
            Napier.i(tag = logTag, message = "Connection identified: ${response.first}")
        }
    }

    //endregion
    //region Send messages
    private suspend fun DefaultClientWebSocketSession.sendRequestsAsync() {
        val readySignal = MutableStateFlow(false)
        scope.launch {
            requests.onCompletion {
                Napier.v(tag = logTag, message = "Done collecting outgoing")
            }.onStart {
                Napier.v(tag = logTag, message = "Started collecting outgoing")
                readySignal.value = true
            }.collect { request ->
                try {
                    val text = EngineJson.encodeToString(request)
                    outgoing.send(Frame.Text(text))
                } catch (e: CancellationException) {
                    if (e.message == "ArrayChannel was cancelled") {
                        // Bug in KTOR
                        handleException(IllegalStateException("ArrayChannel was cancelled", e))
                    } else {
                        throw e
                    }
                } catch (e: Exception) {
                    Napier.e(tag = logTag, message = "Failure in send", throwable = e)
                    responses.emit(
                        MoonrakerUnparsedRpcResponse(
                            id = request.id,
                            error = MoonrakerUnparsedRpcResponse.Error(code = -1, "Failure in send: ${e.message}"),
                            jsonRpc = "{}"
                        ) to "{}"
                    )
                }
            }
        }

        // Block until the flow is ready to receive data. Otherwise the first message(s) might be dropped
        Napier.v(tag = logTag, message = "Waiting for send pipeline to be ready")
        readySignal.first { it }
        Napier.v(tag = logTag, message = "Send pipeline is ready")
    }

    //endregion
    //region Read messages
    private suspend fun DefaultClientWebSocketSession.receiveResponsesAsync() {
        val readySignal = MutableStateFlow(false)

        scope.launch {
            incoming.consumeAsFlow().onCompletion {
                Napier.v(tag = logTag, message = "Done collecting incoming")
            }.onStart {
                Napier.v(tag = logTag, message = "Started collecting incoming")
                readySignal.value = true
            }.onEach {
                if (messageCounter++ == 0) {
                    Napier.d(tag = logTag, message = "Received first message")
                    currentException.update { null }
                    eventHandler.emitEvent(
                        e = Event.Connected(
                            connectionType = call.guessConnectionType(),
                            connectionQuality = ConnectionQuality.Normal,
                        )
                    )
                }
            }.collect { frame ->
                when (frame) {
                    is Frame.Binary -> Napier.w(tag = logTag, message = "Received binary frame of ${frame.data.size} bytes, dropping")
                    is Frame.Ping -> Napier.d(tag = logTag, message = "Received ping")
                    is Frame.Pong -> Napier.d(tag = logTag, message = "Received pong")
                    is Frame.Text -> parseResponse(frame.readText())
                    is Frame.Close -> {
                        Napier.w(tag = logTag, message = "Received close frame, reconnecting")
                        connect(cause = "close-received")
                    }

                    else -> Napier.w(tag = logTag, message = "Received unknown frame, dropping: $frame")
                }
            }
        }

        // Block until the flow is ready to receive data. Otherwise the first message(s) might be dropped
        Napier.v(tag = logTag, message = "Waiting for receive pipeline to be ready")
        readySignal.first { it }
        Napier.v(tag = logTag, message = "Receive pipeline is ready")
    }

    private suspend fun parseResponse(text: String) = try {
        when (val message = EngineJson.decodeFromString(MoonrakerIncomingMessageSerializer(), text)) {
            MoonrakerIncomingMessage.Unknown -> Napier.w(tag = logTag, message = "Received unknown message: $text")
            is MoonrakerNotifyGcodeResponse -> message.params?.let { internalEventHandler.handleGcodeResponses(it) }
            is MoonrakerProcStateUpdate -> Unit
            is MoonrakerNotifyKlippyDisconnected -> Unit
            is MoonrakerNotifyKlippyShutdown -> Unit
            is MoonrakerNotifyServiceStateChange -> message.params.forEach { state ->
                internalEventHandler.handleServiceStateChange(state)
            }

            is MoonrakerWebcamChange -> internalEventHandler.handleSettingsChanged()
            is MoonrakerNotifyHistoryChange -> internalEventHandler.emitEvent(Event.MessageReceived(Message.Event.PrintHistoryUpdated))

            is MoonrakerUnparsedRpcResponse -> responses.emit(message to text)

            is MoonrakerNotifyStatus -> {
                watchdog.notifyMessageReceived()
                internalEventHandler.handleStatusUpdate(message)
            }

            is MoonrakerNotifyKlippyReady -> {
                Napier.i(tag = logTag, message = "Received klippy ready, resetting connection")
                connect(cause = "klippy-ready")
            }
        }
    } catch (e: Exception) {
        Napier.e(tag = logTag, message = "Failed to decode incoming message: $text", throwable = e)
    }

    //endregion
    //region Requests
    internal suspend fun repeatRequest(request: MoonrakerRpcRequest<MoonrakerRpcParams>) {
        requests.emit(request)
    }

    internal suspend fun sendRequest(
        request: MoonrakerRpcRequest<MoonrakerRpcParams>,
    ): Pair<MoonrakerUnparsedRpcResponse, String> {
        requests.emit(request)
        return responses.first { (response, _) -> response.id == request.id }
    }

    //endregion
    //region Error handling
    private fun handleException(e: Throwable) {
        scope.launch {
            currentException.update { e }
            exceptionHandler.handleException(e)
        }
    }
//endregion
}