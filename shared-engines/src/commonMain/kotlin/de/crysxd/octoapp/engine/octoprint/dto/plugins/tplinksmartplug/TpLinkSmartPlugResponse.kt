package de.crysxd.octoapp.engine.octoprint.dto.plugins.tplinksmartplug

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class TpLinkSmartPlugResponse(
    val currentState: State? = null
) {

    @Serializable
    enum class State {
        @SerialName("on")
        ON,

        @SerialName("off")
        OFF,

        @SerialName("unknown")
        UNKNOWN
    }
}