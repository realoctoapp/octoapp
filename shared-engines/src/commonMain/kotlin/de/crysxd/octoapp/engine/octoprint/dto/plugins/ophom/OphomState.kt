package de.crysxd.octoapp.engine.octoprint.dto.plugins.ophom

import kotlinx.serialization.Serializable

@Serializable
internal data class OphomState(
    val response: Int? = null,
    val reponse: Int? = null,
) {
    // Ophom has a typo in their JSON...I anticipate a fix and check for both
    val isOn get() = response == 1 || reponse == 1
}