package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.TimelapseApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromEncodedPath
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.timelapse.TimelapseConfig
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import de.crysxd.octoapp.engine.octoprint.dto.timelapse.OctoTimelapseStatus
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import de.crysxd.octoapp.sharedcommon.Constants
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.onDownload
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.prepareGet
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.HttpHeaders
import io.ktor.utils.io.ByteReadChannel

internal class OctoTimelapseApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : TimelapseApi {

    override suspend fun updateConfig(config: TimelapseConfig) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "timelapse")
            parameter("unrendered", "true")
            setJsonBody(config.map())
        }.body<OctoTimelapseStatus>()
    }.map()

    override suspend fun getStatus() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "timelapse")
            parameter("unrendered", "true")
        }.body<OctoTimelapseStatus>()
    }.map()


    override suspend fun delete(timelapseFile: TimelapseFile) = baseUrlRotator.request {
        val unrendered = timelapseFile.processing || timelapseFile.rendering || timelapseFile.recording

        httpClient.delete {
            if (unrendered) {
                urlFromPath(baseUrl = it, "api", "timelapse", "unrendered", timelapseFile.name)
            } else {
                urlFromPath(baseUrl = it, "api", "timelapse", timelapseFile.name)
            }
            parameter("unrendered", "true")
        }.body<OctoTimelapseStatus>()
    }.map()

    override suspend fun download(timelapseFile: TimelapseFile, progressUpdate: (Float) -> Unit, consume: suspend (ByteReadChannel) -> Unit) = baseUrlRotator.request {
        httpClient.prepareGet {
            val path = requireNotNull(timelapseFile.downloadPath) { "Can't download without path" }
            urlFromEncodedPath(baseUrl = it, *path.split("/").toTypedArray())
            onDownload { bytesSentTotal, contentLength ->
                val total = contentLength.takeIf { it > 0 } ?: timelapseFile.bytes
                val progressPercent = bytesSentTotal / total.toFloat()
                progressUpdate(progressPercent.coerceIn(0f..1f))
            }

            // There is an issue with KTOR cache as it saves the entire download in memory at first, we can't have this of course
            // with potentially huge files. Also prevent logging on this request as logging will clog up memory as well.
            header(HttpHeaders.CacheControl, "no-store, no-cache")
            attributes.put(Constants.SuppressLogging, true)
        }.execute { response ->
            // We must use this execute function to prevent the entire body to be read into memory before the function returns
            consume(response.bodyAsChannel())
        }
    }
}
