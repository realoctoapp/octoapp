package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerDirectory(
    @SerialName("modified") val modified: MoonrakerInstant? = null,
    @SerialName("dirname") val dirname: String? = null,
    @SerialName("permissions") val permissions: String? = null,
    @SerialName("size") val size: Long? = null
)