package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.ObicoApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.models.remote.ObicoStatus
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerDatabaseItem
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerDatabaseGetItemParams
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

internal class MoonrakerObicoApi(
    private val requestHandler: MoonrakerRequestHandler,
    baseUrlRotator: BaseUrlRotator,
    httpClient: HttpClient,
) : ObicoApi(
    baseUrlRotator = baseUrlRotator,
    httpClient = httpClient,
) {

    override suspend fun getPluginStatus() = requestHandler.sendRequest<MoonrakerDatabaseItem>(
        method = "server.database.get_item",
        params = MoonrakerServerDatabaseGetItemParams(
            namespace = "obico",
        )
    ).let { item ->
        try {
            ObicoStatus(
                printerId = item.value.jsonObject["printer_id"]?.jsonPrimitive?.contentOrNull
            )
        } catch (e: Exception) {
            Napier.e(tag = "MoonrakerObicoApi", message = "Failed to handle database item")
            ObicoStatus()
        }
    }
}