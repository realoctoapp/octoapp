@file:UseSerializers(InstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.files

import de.crysxd.octoapp.engine.framework.json.InstantSerializer
import de.crysxd.octoapp.engine.framework.json.SafeFloatSerializer
import de.crysxd.octoapp.engine.octoprint.serializer.OctoFileObjectSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable(with = OctoFileObjectSerializer::class)
sealed class OctoFileObject {

    abstract val name: String
    abstract val display: String
    abstract val origin: OctoFileOrigin
    abstract val path: String
    abstract val type: String
    abstract val typePath: List<String>
    abstract val size: Long?
    abstract val refs: Reference?

    val isPrintable get() = typePath.contains(FILE_TYPE_MACHINE_CODE)

    @Serializable
    data class File(
        override val name: String,
        override val path: String,
        override val origin: OctoFileOrigin,
        override val display: String = name,
        override val type: String = "unknown",
        override val typePath: List<String> = listOf("unknown"),
        override val refs: Reference? = null,
        override val size: Long? = null,
        val thumbnail: String? = null,
        val date: Instant? = null,
        val hash: String? = null,
        val gcodeAnalysis: GcodeAnalysis? = null,
        val prints: PrintHistory? = null,
    ) : OctoFileObject() {
        val extension get() = name.split(".").lastOrNull()
    }

    @Serializable
    data class Folder(
        override val name: String,
        override val origin: OctoFileOrigin,
        override val path: String,
        override val display: String = name,
        override val type: String = FILE_TYPE_FOLDER,
        override val typePath: List<String> = listOf(FILE_TYPE_FOLDER),
        override val refs: Reference? = null,
        override val size: Long? = null,
        val children: List<OctoFileObject> = emptyList()
    ) : OctoFileObject()

    @Serializable
    data class Reference(
        val download: String?,
        val resource: String
    )

    @Serializable
    data class PrintHistory(
        val failure: Int = 0,
        val success: Int = 0,
        val last: LastPrint? = null
    ) {
        @Serializable
        data class LastPrint(
            val date: Instant,
            val success: Boolean
        )
    }

    @Serializable
    data class GcodeAnalysis(
        val dimensions: Dimensions? = null,
        @Serializable(with = SafeFloatSerializer::class) val estimatedPrintTime: Float? = null,
        val filament: Map<String, FilamentUse> = emptyMap(),
    ) {
        @Serializable
        data class Dimensions(
            val depth: Float? = null,
            val height: Float? = null,
            val width: Float? = null,
        )

        @Serializable
        data class FilamentUse(
            val length: Float? = null,
            val volume: Float? = null,
        )
    }

    companion object {
        const val FILE_TYPE_FOLDER = "folder"
        const val FILE_TYPE_MACHINE_CODE = "machinecode"
        const val FILE_TYPE_MACHINE_CODE_GCODE = "gcode"
    }
}