package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.PrinterProfileApi
import de.crysxd.octoapp.engine.models.printer.PrinterProfileList
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerConfigFile
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerExtruder
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerHeaters
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerMcu
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerToolhead
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerPrinterProfileMapperInput
import de.crysxd.octoapp.engine.moonraker.mappers.map

internal class MoonrakerPrinterProfileApi(
    private val requestHandler: MoonrakerRequestHandler
) : PrinterProfileApi {
    override suspend fun getPrinterProfiles(): PrinterProfileList {
        //region Request
        val objects = requestHandler.sendRequest<MoonrakerPrinterObjectList>(
            method = "printer.objects.list",
        ).objects?.filter { o ->
            val first = o.split(" ").first()
            first.startsWith(Constants.PrinterObjects.Extruder) || first.startsWith(Constants.PrinterObjects.Mcu)
        } ?: emptyList()
        val status = requestHandler.sendRequest<MoonrakerPrinterObjectsQuery>(
            method = "printer.objects.query",
            params = MoonrakerPrinterObjectsQueryParams(
                objects = mapOf(
                    Constants.PrinterObjects.Toolhead to null,
                    Constants.PrinterObjects.Heaters to null,
                    Constants.PrinterObjects.Mcu to null,
                ) + objects.associateWith { null }
            )
        ).status
        //endregion
        //region Map
        val profile = MoonrakerPrinterProfileMapperInput(
            toolhead = status?.get(Constants.PrinterObjects.Toolhead) as? MoonrakerToolhead,
            heaters = status?.get(Constants.PrinterObjects.Heaters) as? MoonrakerHeaters,
            extruders = status?.filterValues { it is MoonrakerExtruder }?.mapValues { (_, value) -> value as MoonrakerExtruder } ?: emptyMap(),
            mcus = status?.filterValues { it is MoonrakerMcu }?.mapValues { (_, value) -> value as MoonrakerMcu } ?: emptyMap(),
            config = status?.get(Constants.PrinterObjects.ConfigFile) as? MoonrakerConfigFile,
        ).map()
        return mapOf(profile.id to profile)
        //endregion
    }
}