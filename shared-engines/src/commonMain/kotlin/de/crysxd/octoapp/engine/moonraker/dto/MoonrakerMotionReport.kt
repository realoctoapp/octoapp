package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerMotionReport(
    @SerialName("live_extruder_velocity") val liveExtruderVelocity: Float? = null,
    @SerialName("live_position") val livePosition: List<Float?>? = null,
    @SerialName("live_velocity") val liveVelocity: Float? = null,
    @SerialName("steppers") val steppers: List<String?>? = null,
    @SerialName("trapq") val trapq: List<String?>? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        liveExtruderVelocity = liveExtruderVelocity ?: (previous as? MoonrakerMotionReport)?.liveExtruderVelocity,
        livePosition = livePosition ?: (previous as? MoonrakerMotionReport)?.livePosition,
        liveVelocity = liveVelocity ?: (previous as? MoonrakerMotionReport)?.liveVelocity,
        steppers = steppers ?: (previous as? MoonrakerMotionReport)?.steppers,
        trapq = trapq ?: (previous as? MoonrakerMotionReport)?.trapq,
    )
}