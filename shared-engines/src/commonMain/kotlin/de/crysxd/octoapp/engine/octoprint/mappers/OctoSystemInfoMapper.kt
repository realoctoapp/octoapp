package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.dto.system.OctoSystemInfo

internal fun OctoSystemInfo.map() = SystemInfo(
    generatedAt = info.generatedAt,
    printerFirmware = info.printerFirmware,
    printerFirmwareFamily = if (info.printerFirmware?.contains("klipper") == true) SystemInfo.FirmwareFamily.Klipper else SystemInfo.FirmwareFamily.Generic,
    safeMode = info.safeMode,
    capabilities = listOf(
        SystemInfo.Capability.PrettyTerminal,
        SystemInfo.Capability.Timelapse,
        SystemInfo.Capability.Plugins,
        SystemInfo.Capability.AutoNgrok,
        SystemInfo.Capability.TemperatureOffset,
    )
)