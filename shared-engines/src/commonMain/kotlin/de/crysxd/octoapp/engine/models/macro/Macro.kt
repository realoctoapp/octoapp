package de.crysxd.octoapp.engine.models.macro

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class Macro(
    val id: String,
    val name: String,
    val inputs: List<Input>,
    val description: String? = null,
    val confirmation: String? = null,
) : CommonParcelable {

    fun createGcodeCommand(params: Map<String, String?>): String {
        val paramList = params
            .filter { (key, value) ->
                // Filter out blank values and values that match the default
                !value.isNullOrBlank() && inputs.firstOrNull { it.name == key }?.defaultValue != value
            }
            .entries
            .joinToString(" ") { (name, value) -> "$name=$value" }
        return "$id $paramList"
    }

    @Serializable
    @CommonParcelize
    data class Input(
        val name: String,
        val label: String,
        val type: Type,
        val defaultValue: String? = null,
        val minValue: Float? = null,
        val maxValue: Float? = null,
    ) : CommonParcelable {

        @Serializable
        enum class Type {
            Float,
            Int,
            Any
        }
    }
}