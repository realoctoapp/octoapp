package de.crysxd.octoapp.engine.moonraker.dto


import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerGenericHeater
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerExtruder(
    @SerialName("can_extrude") val canExtrude: Boolean? = null,
    @SerialName("power") override val power: Float? = null,
    @SerialName("pressure_advance") val pressureAdvance: Float? = null,
    @SerialName("smooth_time") val smoothTime: Float? = null,
    @SerialName("target") override val target: Float? = null,
    @SerialName("temperature") override val temperature: Float? = null,
) : MoonrakerGenericHeater, MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        canExtrude = canExtrude ?: (previous as? MoonrakerExtruder)?.canExtrude,
        power = power ?: (previous as? MoonrakerExtruder)?.power,
        pressureAdvance = pressureAdvance ?: (previous as? MoonrakerExtruder)?.pressureAdvance,
        smoothTime = smoothTime ?: (previous as? MoonrakerExtruder)?.smoothTime,
        target = target ?: (previous as? MoonrakerExtruder)?.target,
        temperature = temperature ?: (previous as? MoonrakerExtruder)?.temperature,
    )
}