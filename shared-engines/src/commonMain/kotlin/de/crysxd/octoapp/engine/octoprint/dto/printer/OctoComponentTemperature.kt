package de.crysxd.octoapp.engine.octoprint.dto.printer

import de.crysxd.octoapp.engine.framework.json.SafeFloatSerializer
import kotlinx.serialization.Serializable

@Serializable
data class OctoComponentTemperature(
    // Enclosure plugin likes to send "" as null Float.
    @Serializable(with = SafeFloatSerializer::class) val actual: Float? = null,
    @Serializable(with = SafeFloatSerializer::class) val target: Float? = null,
    @Serializable(with = SafeFloatSerializer::class) val offset: Float? = null
)