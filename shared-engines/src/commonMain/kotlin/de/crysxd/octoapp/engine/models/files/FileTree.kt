package de.crysxd.octoapp.engine.models.files

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.withTimeout
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

data class FileTree(
    val files: FileReference.Folder,
    val free: Long? = null,
    val total: Long? = null
)

interface FileReference : CommonParcelable {
    val path: String
    val id: String
    val display: String
    val origin: FileOrigin
    val name get() = path.split("/").last()

    interface File : FileReference {
        val size: Long?
        val date: Instant
    }

    interface Folder : FileReference {
        val children: List<FileReference>?
        operator fun get(name: String) = children?.firstOrNull { it.name == name }
    }
}


// Timeout stuff due to Freshdesk #946
suspend fun FileReference.flatten(): List<FileReference.File> = try {
    withTimeout(timeout = 1.seconds) {
        flattenInternal(Clock.System.now())
    }
} catch (e: TimeoutCancellationException) {
    throw Exception("Timed out flattening file structure (1)")
}

fun FileReference.flattenInternal(start: Instant): List<FileReference.File> {
    if (Clock.System.now() - start > 1.1.seconds) {
        throw Exception("Timed out flattening file structure (2)")
    }

    return when (this) {
        is FileReference.File -> listOf(this)
        is FileReference.Folder -> children?.map { it.flattenInternal(start) }?.flatten() ?: listOf()
        else -> emptyList()
    }
}