package de.crysxd.octoapp.engine.octoprint.dto.plugins.camereastreamercontrol

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames

@Serializable
data class CameraStreamerControlExtras(
    @SerialName("url") val url: String? = null,
    @SerialName("webrtc") val webrtc: WebRtc? = null,
    @SerialName("mode") val mode: Mode = Mode.Mjpeg,
) {
    @Serializable
    enum class Mode {
        @SerialName("webrtc")
        WebRtc,

        @OptIn(ExperimentalSerializationApi::class)
        @SerialName("mjpg")
        @JsonNames("mjpg", "mjpeg")
        Mjpeg,
    }

    @Serializable
    data class WebRtc(
        @SerialName("stun") val stun: String? = "stun:stun.l.google.com:19302",
        @SerialName("turn") val turn: String? = null,
    )

    @Serializable
    data class Mjpeg(
        @SerialName("cacheBuster") val cacheBuster: Boolean = false
    )
}