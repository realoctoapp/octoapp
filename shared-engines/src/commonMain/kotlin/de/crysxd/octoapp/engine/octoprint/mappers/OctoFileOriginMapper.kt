package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileOrigin

internal fun OctoFileOrigin.map() = when (this) {
    OctoFileOrigin.Local -> FileOrigin.Gcode
    OctoFileOrigin.SdCard -> FileOrigin.Other("sdcard")
}

internal fun FileOrigin.map() = when (val origin = this) {
    FileOrigin.Gcode -> OctoFileOrigin.Local
    is FileOrigin.Other -> when (origin.name) {
        "sdcard" -> OctoFileOrigin.SdCard
        else -> throw UnsupportedOperationException("Can't create a OctoFileOrigin from $origin")
    }
}