package de.crysxd.octoapp.engine.models.event

import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.models.tune.TuneState
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant


interface Message {

    data class Connected(
        val version: String? = null,
        val displayVersion: String? = null,
        val configHash: String,
    ) : Message

    data class Current(
        val logs: List<String> = emptyList(),
        val temps: List<HistoricTemperatureData> = emptyList(),
        val tempLabels: Map<String, String> = emptyMap(),
        val tempMaxs: Map<String, Float> = emptyMap(),
        val tempEditables: List<String> = emptyList(),
        val state: PrinterState.State = PrinterState.State(),
        val progress: ProgressInformation? = null,
        val job: JobInformation? = null,
        val offsets: Map<String, Float>? = null,
        val serverTime: Instant,
        val localTime: Instant = Clock.System.now(),
        val isHistoryMessage: Boolean = false,
        val tuneState: TuneState? = null,
        val realTimeStats: RealTimeStats? = null,
        val displayMessage: String? = null,
        val pendingSettingsSave: Boolean = false,
        val printObjects: PrintObjects? = null,
    ) : Message {

        val statusText
            get() = state.createStatusText(progress?.completion)

        data class PrintObjects(
            val currentObject: String?,
            val excluded: List<String>,
            val all: List<Object>
        ) {
            data class Object(
                val id: String,
                val label: String?,
                val center: Pair<Float, Float>?,
            )
        }

        data class RealTimeStats(
            val toolhead: Toolhead? = null,
            val extruder: Extruder? = null,
            val steppers: Steppers? = null,
        ) {
            data class Steppers(
                val enabled: Map<String, Boolean>?,
            )

            data class Toolhead(
                val xyHomed: Boolean?,
                val zHomed: Boolean?,
                val positionX: Float?,
                val positionY: Float?,
                val positionZ: Float?,
                val speedMmPerS: Float?,
                val maxpeedMmPerS: Float?,
            )

            data class Extruder(
                val flowMm3PerS: Float?,
                val maxFlowMm3PerS: Float?,
                val filamentUsedMm: Float?,
            )
        }
    }


    object UnknownMessage : Message

    interface Event : Message {

        object Connecting : Event

        object UpdatedFiles : Event

        object PrintStarted : Event

        object PrintResumed : Event

        object PrintPausing : Event

        object PrintPaused : Event

        object PrintCancelling : Event

        object PrintCancelled : Event

        object PrintFailed : Event

        object PrintDone : Event

        object PrintHistoryUpdated : Event

        object PrinterProfileModified : Event

        data class SettingsUpdated(
            val configHash: String?,
        ) : Event

        object MovieRendering : Event

        object MovieDone : Event

        object MovieFailed : Event

        object Disconnected : Event

        object PrinterStateChanged : Event

        data class Unknown(val type: String) : Event

        data class FirmwareData(
            val firmwareName: String?,
            val machineType: String?,
            val extruderCount: Int?
        ) : Event

        data class FileSelected(
            val origin: FileOrigin,
            val name: String,
            val path: String
        ) : Event

        data class PrinterConnected(
            val baudrate: Int?,
            val port: String?
        ) : Event

    }
}