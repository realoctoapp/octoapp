package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.ktor.http.URLBuilder
import io.ktor.http.Url

fun String.guessWebcamUrlType() = with(trim()) {
    val url = toUrlOrNull()
    when {
        lowercase().startsWith("rtsp://") -> WebcamSettings.Webcam.Type.Rtsp to this
        url?.parameters?.contains("octoAppJanus") ?: false -> WebcamSettings.Webcam.Type.WebRtcJanus to url?.removeOctoParams()
        url?.parameters?.contains("octoAppGo2Rtc") ?: false -> WebcamSettings.Webcam.Type.WebRtcGoRtc to url?.removeOctoParams()
        url?.parameters?.contains("octoAppMediaMtx") ?: false -> WebcamSettings.Webcam.Type.WebRtcMediaMtx to url?.removeOctoParams()
        url?.parameters?.contains("octoAppIframe") ?: false -> WebcamSettings.Webcam.Type.Iframe to this
        url?.pathSegments?.lastOrNull()?.let { it.endsWith(".m3u") || it.endsWith(".m3u8") } ?: false -> WebcamSettings.Webcam.Type.Hls to this
        else -> WebcamSettings.Webcam.Type.Mjpeg to this
    }
}

private fun Url.removeOctoParams() = URLBuilder(this).apply {
    val new = parameters.entries().filter { (key, _) ->
        !key.startsWith("octoApp")
    }

    parameters.clear()

    new.forEach { (key, value) ->
        parameters.appendAll(key, value)
    }
}.buildString()