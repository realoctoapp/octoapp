package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerFileActionResult(
    @SerialName("item") val item: MoonrakerFileReference,
    @SerialName("action") val action: String,
)