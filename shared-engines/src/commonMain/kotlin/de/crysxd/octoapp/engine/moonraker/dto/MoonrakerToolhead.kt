package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerToolhead(
    @SerialName("axis_maximum") val axisMaximum: List<Float>? = null,
    @SerialName("axis_minimum") val axisMinimum: List<Float>? = null,
    @SerialName("estimated_print_time") val estimatedPrintTime: Float? = null,
    @SerialName("extruder") val extruder: String? = null,
    @SerialName("homed_axes") val homedAxes: String? = null,
    @SerialName("max_accel") val maxAccel: Float? = null,
    @SerialName("max_accel_to_decel") val maxAccelToDecel: Float? = null,
    @SerialName("max_velocity") val maxVelocity: Float? = null,
    @SerialName("position") val position: List<Float?>? = null,
    @SerialName("print_time") val printTime: Float? = null,
    @SerialName("square_corner_velocity") val squareCornerVelocity: Float? = null,
    @SerialName("stalls") val stalls: Float? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        axisMaximum = axisMaximum ?: (previous as? MoonrakerToolhead)?.axisMaximum,
        axisMinimum = axisMinimum ?: (previous as? MoonrakerToolhead)?.axisMinimum,
        estimatedPrintTime = estimatedPrintTime ?: (previous as? MoonrakerToolhead)?.estimatedPrintTime,
        extruder = extruder ?: (previous as? MoonrakerToolhead)?.extruder,
        homedAxes = homedAxes ?: (previous as? MoonrakerToolhead)?.homedAxes,
        maxAccel = maxAccel ?: (previous as? MoonrakerToolhead)?.maxAccel,
        maxAccelToDecel = maxAccelToDecel ?: (previous as? MoonrakerToolhead)?.maxAccelToDecel,
        maxVelocity = maxVelocity ?: (previous as? MoonrakerToolhead)?.maxVelocity,
        position = position ?: (previous as? MoonrakerToolhead)?.position,
        printTime = printTime ?: (previous as? MoonrakerToolhead)?.printTime,
        squareCornerVelocity = squareCornerVelocity ?: (previous as? MoonrakerToolhead)?.squareCornerVelocity,
        stalls = stalls ?: (previous as? MoonrakerToolhead)?.stalls,
    )
}
