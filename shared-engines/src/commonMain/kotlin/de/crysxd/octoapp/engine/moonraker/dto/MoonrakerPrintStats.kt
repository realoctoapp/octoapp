package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerPrintStats(
    @SerialName("filament_used") val filamentUsed: Float? = null,
    @SerialName("filename") val filename: String? = null,
    @SerialName("info") val info: Info? = null,
    @SerialName("message") val message: String? = null,
    @SerialName("print_duration") val printDuration: Float? = null,
    @SerialName("state") val state: State? = null,
    @SerialName("total_duration") val totalDuration: Float? = null
) : MoonrakerStatusItem {
    @Serializable
    enum class State {
        @SerialName("standby")
        Standby,

        @SerialName("printing")
        Printing,

        @SerialName("paused")
        Paused,

        @SerialName("error")
        Error,

        @SerialName("complete")
        Complete,

        @SerialName("cancelled")
        Cancelled,
    }

    @Serializable
    data class Info(
        @SerialName("current_layer") val currentLayer: Int? = null,
        @SerialName("total_layer") val totalLayer: Int? = null
    )

    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        filamentUsed = filamentUsed ?: (previous as? MoonrakerPrintStats)?.filamentUsed,
        filename = filename ?: (previous as? MoonrakerPrintStats)?.filename,
        info = info?.copy(
            currentLayer = (previous as? MoonrakerPrintStats)?.info?.currentLayer,
            totalLayer = (previous as? MoonrakerPrintStats)?.info?.totalLayer,
        ) ?: (previous as? MoonrakerPrintStats)?.info,
        message = message ?: (previous as? MoonrakerPrintStats)?.message,
        printDuration = printDuration ?: (previous as? MoonrakerPrintStats)?.printDuration,
        state = state ?: (previous as? MoonrakerPrintStats)?.state,
        totalDuration = totalDuration ?: (previous as? MoonrakerPrintStats)?.totalDuration,
    )
}