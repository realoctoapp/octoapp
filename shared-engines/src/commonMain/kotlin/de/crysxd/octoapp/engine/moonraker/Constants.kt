package de.crysxd.octoapp.engine.moonraker

internal object Constants {
    const val MoonrakerLogTag = "HTTP/Moonraker"

    object PrinterObjects {
        const val PrintStats = "print_stats"
        const val VirtualSdCard = "virtual_sdcard"
        const val IdleTimeout = "idle_timeout"
        const val SystemStats = "system_stats"
        const val MotionReport = "motion_report"
        const val DisplayStatus = "display_status"
        const val Toolhead = "toolhead"
        const val ConfigFile = "configfile"
        const val StepperEnable = "stepper_enable"
        const val ExcludeObject = "exclude_object"
        const val Extruder = "extruder"
        const val Webhooks = "webhooks"
        const val TemperatureHost = "temperature_host"
        const val HeaterBed = "heater_bed"
        const val HeaterChamber = "heater_chamber"
        const val BedMesh = "bed_mesh"
        const val HeaterFan = "heater_fan"
        const val Fan = "fan"
        const val Heaters = "heaters"
        const val GcodeMove = "gcode_move"
        const val Mcu = "mcu"

        const val TemperatureSensorKind = "temperature_sensor"
        const val TemperatureFanKind = "temperature_fan"
        const val HeaterGenericKind = "heater_generic"
        const val FanGenericKind = "fan_generic"
        const val Led = "led"
        const val Neopixel = "neopixel"
        const val Dotstar = "dotstar"
        const val Pca9533 = "pca9533"
        const val Pca9632 = "pca9632"
        const val OutputPin = "output_pin"
        const val MultiPin = "multi_pin"
    }
}