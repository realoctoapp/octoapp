package de.crysxd.octoapp.engine.moonraker.connection

import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerRpcRequest
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerRpcParams
import de.crysxd.octoapp.engine.moonraker.exception.MoonrakerDatabaseNamespaceNotFoundException
import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerRpcResponseResultSerializer
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlinx.datetime.Clock
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes

internal interface MoonrakerRequestHandler {
    val json: Json
    suspend fun sendRequest(request: MoonrakerRpcRequest<MoonrakerRpcParams>, longTimeout: Boolean = false): String
    suspend fun sendRequestIgnoringResult(request: MoonrakerRpcRequest<MoonrakerRpcParams>)
}

internal suspend inline fun <reified Result : Any> MoonrakerRequestHandler.sendRequest(
    method: String,
    params: MoonrakerRpcParams? = null,
    longTimeout: Boolean = false,
): Result {
    val response = sendRequest(
        longTimeout = longTimeout,
        request = MoonrakerRpcRequest(
            method = method,
            params = params
        )
    )

    return if (Result::class == Unit::class) {
        Unit as Result
    } else try {
        json.decodeFromString(MoonrakerRpcResponseResultSerializer(Result::class), response)
    } catch (e: SerializationException) {
        Napier.e(tag = "MoonrakerRequestHandler", message = "Failed to decode response: $response", throwable = e)
        throw e
    }
}

internal suspend inline fun MoonrakerRequestHandler.sendRequestIgnoringResult(
    method: String,
    params: MoonrakerRpcParams? = null,
) = sendRequestIgnoringResult(
    request = MoonrakerRpcRequest(
        method = method,
        params = params
    )
)

internal class DefaultMoonrakerRequestHandler(
    private val connection: MoonrakerConnection,
    private val connect: () -> Unit,
    private val disconnect: () -> Unit,
    private val logTag: String,
    private val timeout: Duration,
    private val currentIOException: () -> Throwable?,
    private val url: () -> Url,
    private val debugLogging: Boolean,
) : MoonrakerRequestHandler {

    override val json = EngineJson
    private val pendingRequests = MutableStateFlow(emptyMap<Int, MoonrakerRpcRequest<MoonrakerRpcParams>>())
    private val scope = CoroutineScope(Dispatchers.Default)
    private val MoonrakerRpcRequest<*>.logTag get() = "${this@DefaultMoonrakerRequestHandler.logTag}/Request/${id}"
    val connectionNeeded get() = pendingRequests.value.isNotEmpty()

    init {
        scope.launch {
            pendingRequests.distinctUntilChanged { oldList, newList ->
                val old = oldList.size
                val new = newList.size
                Napier.v(tag = logTag, message = "Active request count changed: $old -> $new active flows")

                if (old == 0 && new != 0) {
                    connect()
                } else if (old != 0 && new == 0) {
                    disconnect()
                }
                false
            }.collect()
        }
    }

    fun destroy() {
        scope.cancel("Destroyed")
    }

    suspend fun repeatPendingRequests() {
        pendingRequests.value.values.forEach { request ->
            Napier.i(tag = request.logTag, message = "==> jsonrpc://${request.method} (Repeat)")
            connection.repeatRequest(request)
        }
    }

    override suspend fun sendRequestIgnoringResult(request: MoonrakerRpcRequest<MoonrakerRpcParams>) {
        scope.launch {
            try {
                internalSendRequest(request, longTimeout = false)
            } catch (e: Exception) {
                Napier.e(tag = request.logTag, message = "Request with ignored result failed", throwable = e)
            }
        }
    }

    override suspend fun sendRequest(request: MoonrakerRpcRequest<MoonrakerRpcParams>, longTimeout: Boolean): String = internalSendRequest(
        request = request,
        longTimeout = longTimeout,
    )

    private suspend fun internalSendRequest(
        request: MoonrakerRpcRequest<MoonrakerRpcParams>,
        longTimeout: Boolean,
    ): String {
        val start = Clock.System.now()
        val tag = request.logTag
        try {
            //region  Add request to pending
            pendingRequests.update { requests -> requests + (request.id to request) }
            //endregion
            //region Send
            Napier.i(tag = tag, message = "==> jsonrpc://${request.method}")
            if (request.params != null) Napier.d(tag = tag, message = "    ${request.params}")
            val timeout = if (longTimeout) 10.minutes else timeout
            val (response, text) = withContext(Dispatchers.SharedIO) {
                withTimeout(timeout) {
                    connection.sendRequest(request)
                }
            }
            //endregion
            //region Receive
            val url = url().resolve("jsonrpc/${request.method}:${request.id}")
            when {
                response.error == null -> {
                    Napier.i(tag = tag, message = "<== jsonrpc://${request.method} (${Clock.System.now() - start})")
                    if (debugLogging) {
                        Napier.d(tag = tag, message = "    $text")
                    }
                    return text
                }

                response.error.code == -32601 -> {
                    Napier.i(tag = tag, message = "<== jsonrpc://${request.method} failed: ${response.error.message} (Database entry not found)")
                    throw MoonrakerDatabaseNamespaceNotFoundException(
                        message = response.error.message,
                        code = response.error.code,
                        httpUrl = url,
                    )
                }

                response.error.code == -32602 -> {
                    Napier.i(tag = tag, message = "<== jsonrpc://${request.method} failed: ${response.error.message} (Unauthorized)")
                    throw InvalidApiKeyException(
                        webUrl = url
                    )
                }

                else -> {
                    Napier.e(tag = tag, message = "<== jsonrpc://${request.method} failed: ${response.error} (${response.error.code}, ${Clock.System.now() - start})")
                    throw PrinterApiException(
                        httpUrl = url().resolve("jsonrpc/${request.method}:${request.id}"),
                        responseCode = response.error.code,
                        body = response.error.message
                    )
                }
            }
            //endregion
        } catch (e: TimeoutCancellationException) {
            val ioException = currentIOException()
            Napier.i(
                tag = tag,
                message = "<== jsonrpc://${request.method} cancelled (timeout after ${Clock.System.now() - start}, nested cause: ${ioException?.let { it::class.qualifiedName }})"
            )
            throw ioException ?: e
        } catch (e: CancellationException) {
            Napier.i(tag = tag, message = "<== jsonrpc://${request.method} cancelled after ${Clock.System.now() - start} (${e::class.qualifiedName})")
            throw e
        } catch (e: PrinterApiException) {
            // Do not double log
            throw e
        } catch (e: MoonrakerDatabaseNamespaceNotFoundException) {
            // Do not double log
            throw e
        } catch (e: Exception) {
            Napier.e(tag = tag, message = "<== jsonrpc://${request.method} failed with ${e::class.qualifiedName} (${Clock.System.now() - start})", throwable = e)
            throw e
        } finally {
            //region Remove request from pending and cancel job
            pendingRequests.update { it.filterKeys { id -> id != request.id } }
            //endregion
        }
    }
}