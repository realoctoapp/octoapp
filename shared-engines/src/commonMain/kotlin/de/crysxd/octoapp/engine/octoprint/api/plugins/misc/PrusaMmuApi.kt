package de.crysxd.octoapp.engine.octoprint.api.plugins.misc

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.mmu2filamentselect.PrusaMmuChoiceBody
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.request.post

class PrusaMmuApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) {

    suspend fun selectChoice(choice: Int) = baseUrlRotator.request {
        if (choice >= 5) {
            Napier.i(tag = "PrusaMmuApi", message = "Choice $choice means cancel but Prusa MMU does not support that -> skip")
            return@request
        }

        httpClient.post {
            urlFromPath(baseUrl = it, "api", "plugin", OctoPlugins.PrusaMmu)
            setJsonBody(PrusaMmuChoiceBody(choice))
        }
    }
}