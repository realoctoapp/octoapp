package de.crysxd.octoapp.engine.octoprint.dto.login

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoLoginResponse(
    val session: String? = null,
    val name: String? = null,
    val groups: List<String> = emptyList(),
    val admin: Boolean = false
)