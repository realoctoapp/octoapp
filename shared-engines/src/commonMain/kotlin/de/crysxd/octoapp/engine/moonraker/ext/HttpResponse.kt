package de.crysxd.octoapp.engine.moonraker.ext

import de.crysxd.octoapp.engine.moonraker.dto.framework.MoonrakerResultWrapper
import io.ktor.client.call.body
import io.ktor.client.statement.HttpResponse

internal suspend inline fun <reified T> HttpResponse.moonrakerBody() = body<MoonrakerResultWrapper<T>>().result