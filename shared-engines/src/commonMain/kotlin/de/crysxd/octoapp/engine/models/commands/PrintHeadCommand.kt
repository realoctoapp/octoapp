package de.crysxd.octoapp.engine.models.commands

@Suppress("Unused")
sealed class PrintHeadCommand {
    data class MovePrintHeadCommand(val x: Float = 0f, val y: Float = 0f, val z: Float = 0f, val speed: Int? = null) : PrintHeadCommand()
    data class JogPrintHeadCommand(val x: Float = 0f, val y: Float = 0f, val z: Float = 0f, val speed: Int? = null) : PrintHeadCommand()
    abstract class HomePrintHeadCommand(val axes: List<String>) : PrintHeadCommand()
    data object HomeXYAxisPrintHeadCommand : HomePrintHeadCommand(listOf("x", "y"))
    data object HomeZAxisPrintHeadCommand : HomePrintHeadCommand(listOf("z"))
    data object HomeAllAxisPrintHeadCommand : HomePrintHeadCommand(listOf("x", "y", "z"))
}

