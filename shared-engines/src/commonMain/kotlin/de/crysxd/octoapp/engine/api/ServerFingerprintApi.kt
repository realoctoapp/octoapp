package de.crysxd.octoapp.engine.api

interface ServerFingerprintApi {
    suspend fun getServerFingerPrint(): String
}
