package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerDirectoryContent(
    @SerialName("dirs") val dirs: List<MoonrakerDirectory>? = null,
    @SerialName("disk_usage") val diskUsage: DiskUsage? = null,
    @SerialName("files") val files: List<MoonrakerFileMetadata>? = null,
    @SerialName("root_info") val rootInfo: RootInfo? = null
) {

    @Serializable
    data class DiskUsage(
        @SerialName("free") val free: Long? = null,
        @SerialName("total") val total: Long? = null,
        @SerialName("used") val used: Long? = null
    )

    @Serializable
    data class File(
        @SerialName("filename") val filename: String? = null,
        @SerialName("modified") val modified: Double? = null,
        @SerialName("permissions") val permissions: String? = null,
        @SerialName("size") val size: Int? = null
    )

    @Serializable
    data class RootInfo(
        @SerialName("name") val name: String? = null,
        @SerialName("permissions") val permissions: String? = null
    )
}