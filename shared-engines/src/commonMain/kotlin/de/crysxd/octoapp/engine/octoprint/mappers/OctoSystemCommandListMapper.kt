package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.system.SystemCommand
import de.crysxd.octoapp.engine.models.system.SystemCommandList
import de.crysxd.octoapp.engine.octoprint.dto.system.OctoSystemCommand
import de.crysxd.octoapp.engine.octoprint.dto.system.OctoSystemCommandList

internal fun OctoSystemCommandList.map() = SystemCommandList(
    plugin = plugin.map(),
    core = core.map(),
    custom = custom.map()
)

private fun List<OctoSystemCommand>.map() = map {
    SystemCommand(
        name = it.name,
        confirmation = it.confirm,
        action = it.action,
        source = it.source,
        type = when (it.action) {
            "reboot" -> SystemCommand.Type.Reboot
            "restart" -> SystemCommand.Type.Restart
            "restart_safe" -> SystemCommand.Type.RestartSafeMode
            "shutdown" -> SystemCommand.Type.Shutdown
            else -> SystemCommand.Type.Other
        }
    )
}