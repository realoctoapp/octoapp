package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.ktor.http.Url
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class NoopBaseUrlRotator(private val baseUrl: Url) : BaseUrlRotator {
    constructor(url: String) : this(url.toUrl())

    override suspend fun <T> request(block: suspend (Url) -> T) = block(baseUrl)
    override suspend fun considerException(baseUrl: Url, e: Throwable) = Unit
    override val activeUrl = MutableStateFlow(baseUrl)
    override fun consumeActiveFlow(active: StateFlow<Boolean>, probe: suspend (BaseUrlRotator) -> Boolean, onChange: () -> Unit) = Unit
    override suspend fun considerUpgradingConnection(trigger: String, forced: Boolean) = Unit
}