package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerIncomingMessage
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyGcodeResponse
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyHistoryChange
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyKlippyDisconnected
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyKlippyReady
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyKlippyShutdown
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyServiceStateChange
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyStatus
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerProcStateUpdate
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerUnparsedRpcResponse
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerWebcamChange
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

internal class MoonrakerIncomingMessageSerializer : DeserializationStrategy<MoonrakerIncomingMessage> {

    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerRpcRequest.Params", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): MoonrakerIncomingMessage {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject
        return when {
            tree.containsKey("id") -> EngineJson.decodeFromJsonElement(MoonrakerUnparsedRpcResponse.serializer(), tree)
            tree.containsKey("method") -> when (tree["method"]!!.jsonPrimitive.content) {
                "notify_proc_stat_update" -> EngineJson.decodeFromJsonElement(MoonrakerProcStateUpdate.serializer(), tree)
                "notify_history_changed" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyHistoryChange.serializer(), tree)
                "notify_gcode_response" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyGcodeResponse.serializer(), tree)
                "notify_status_update" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyStatus.serializer(), tree)
                "notify_klippy_disconnected" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyKlippyDisconnected.serializer(), tree)
                "notify_klippy_ready" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyKlippyReady.serializer(), tree)
                "notify_klippy_shutdown" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyKlippyShutdown.serializer(), tree)
                "notify_service_state_changed" -> EngineJson.decodeFromJsonElement(MoonrakerNotifyServiceStateChange.serializer(), tree)
                "notify_webcams_changed" -> EngineJson.decodeFromJsonElement(MoonrakerWebcamChange.serializer(), tree)
                else -> MoonrakerIncomingMessage.Unknown
            }

            else -> MoonrakerIncomingMessage.Unknown
        }
    }
}