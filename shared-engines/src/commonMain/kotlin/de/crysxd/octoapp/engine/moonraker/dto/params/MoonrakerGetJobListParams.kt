package de.crysxd.octoapp.engine.moonraker.dto.params


import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerInstant
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerGetJobListParams(
    @SerialName("before") val before: MoonrakerInstant? = null,
    @SerialName("limit") val limit: Int? = null,
    @SerialName("order") val order: String? = null,
    @SerialName("since") val since: MoonrakerInstant? = null,
    @SerialName("start") val start: Int? = null
) : MoonrakerRpcParams