package de.crysxd.octoapp.engine.models.files

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.CommonTypeParceler
import de.crysxd.octoapp.sharedcommon.InstantParceler
import kotlinx.datetime.Instant
import kotlin.math.absoluteValue


sealed class FileObject : CommonParcelable, FileReference {
    abstract val type: String
    abstract val typePath: List<String>
    abstract val size: Long?

    val isPrintable get() = typePath.contains(FILE_TYPE_MACHINE_CODE)

    @CommonParcelize
    data class File(
        override val name: String,
        override val path: String,
        override val id: String = path,
        override val origin: FileOrigin,
        override val display: String = name,
        override val type: String = "unknown",
        override val typePath: List<String> = listOf("unknown"),
        override val size: Long? = null,
        @CommonTypeParceler<Instant, InstantParceler>() override val date: Instant = Instant.DISTANT_PAST,
        val thumbnails: List<Thumbnail> = emptyList(),
        val hash: String? = null,
        val gcodeAnalysis: GcodeAnalysis? = null,
        val prints: PrintHistory? = null,
        val metadata: List<MetadataGroup> = emptyList(),
    ) : FileObject(), FileReference.File {

        val extension get() = if (name.contains(".")) name.split(".").lastOrNull() else null
        val smallThumbnail get() = thumbnailForDimension(dimension = 128)
        val mediumThumbnail get() = thumbnailForDimension(dimension = 256)
        val largeThumbnail get() = thumbnailForDimension(dimension = 1024)

        constructor(path: String) : this(
            name = path.split("/").lastOrNull() ?: "",
            path = path,
            origin = FileOrigin.Gcode,
        )

        private fun thumbnailForDimension(dimension: Int): Thumbnail? {
            return thumbnails.minByOrNull { (maxOf(it.width, it.height) - dimension).absoluteValue }
        }

        @CommonParcelize
        data class Thumbnail(
            val width: Int,
            val height: Int,
            val path: String,
        ) : CommonParcelable

        @CommonParcelize
        data class MetadataGroup(
            val label: String,
            val id: String,
            val items: List<MetadataItem>,
        ) : CommonParcelable {
            internal constructor(
                label: String,
                items: List<Pair<String, String?>>
            ) : this(
                label = label,
                id = label,
                items = items.mapNotNull { (label, value) ->
                    MetadataItem(
                        label = label,
                        id = label,
                        value = value ?: return@mapNotNull null
                    )
                }
            )
        }

        @CommonParcelize
        data class MetadataItem(
            val label: String,
            val id: String,
            val value: String,
        ) : CommonParcelable
    }

    @CommonParcelize
    data class Folder(
        override val name: String,
        override val origin: FileOrigin,
        override val path: String,
        override val id: String = path,
        override val display: String = name,
        override val size: Long? = null,
        override val children: List<FileReference>? = null,
    ) : FileObject(), FileReference.Folder {

        constructor(path: String) : this(
            name = path.split("/").lastOrNull() ?: "",
            path = path,
            origin = FileOrigin.Gcode,
            id = path,
        )

        override val type: String get() = FILE_TYPE_FOLDER
        override val typePath: List<String> get() = listOf(FILE_TYPE_FOLDER)
    }

    @CommonParcelize
    data class PrintHistory(
        val failure: Int = 0,
        val success: Int = 0,
        val last: LastPrint? = null
    ) : CommonParcelable {
        @CommonParcelize
        data class LastPrint(
            @CommonTypeParceler<Instant, InstantParceler>() val date: Instant,
            val success: Boolean
        ) : CommonParcelable
    }

    @CommonParcelize
    data class GcodeAnalysis(
        val dimensions: Dimensions? = null,
        val estimatedPrintTime: Float? = null,
        val filament: Map<String, FilamentUse> = emptyMap(),
        val layerHeight: Float? = null,

        ) : CommonParcelable {
        @CommonParcelize
        data class Dimensions(
            val depth: Float? = null,
            val height: Float? = null,
            val width: Float? = null,
        ) : CommonParcelable

        @CommonParcelize
        data class FilamentUse(
            val length: Float? = null,
            val volume: Float? = null,
            val weight: Float? = null,
        ) : CommonParcelable
    }

    companion object {
        const val FILE_TYPE_FOLDER = "folder"
        const val FILE_TYPE_MACHINE_CODE = "machinecode"
        const val FILE_TYPE_MACHINE_CODE_GCODE = "gcode"
    }
}