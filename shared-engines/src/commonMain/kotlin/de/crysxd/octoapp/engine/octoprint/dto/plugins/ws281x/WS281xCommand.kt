package de.crysxd.octoapp.engine.octoprint.dto.plugins.ws281x

import de.crysxd.octoapp.engine.octoprint.CommandBody

internal data class WS281xCommand(
    override val command: String
) : CommandBody