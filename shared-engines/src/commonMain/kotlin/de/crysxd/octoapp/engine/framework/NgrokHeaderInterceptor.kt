package de.crysxd.octoapp.engine.framework

import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.request.headers

internal fun HttpClient.installNgrokHeaderInterceptor() = plugin(HttpSend).intercept { request ->
    // We are quite loose with this check, adding the header will not harm other connections
    if (request.url.host.contains("ngrok")) {
        request.headers {
            append("ngrok-skip-browser-warning", "42")
        }
    }

    execute(request)
}