package de.crysxd.octoapp.engine.moonraker.dto.incoming


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerNotifyKlippyDisconnected(
    @SerialName("jsonrpc") val jsonrpc: String? = null,
    @SerialName("method") val method: String? = null
) : MoonrakerIncomingMessage