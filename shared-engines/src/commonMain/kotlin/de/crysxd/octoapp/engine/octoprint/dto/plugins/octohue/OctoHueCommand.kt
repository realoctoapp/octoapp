package de.crysxd.octoapp.engine.octoprint.dto.plugins.octohue

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoHueCommand(
    override val command: String = "togglehue"
) : CommandBody
