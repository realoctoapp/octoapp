package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.Constants
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerServerInfo
import de.crysxd.octoapp.engine.moonraker.ext.moonrakerBody
import de.crysxd.octoapp.sharedcommon.exceptions.PrinterApiException
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.request.get

internal class MoonrakerProbeApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) {
    private val tag = "MoonrakerProbeApi"

    suspend fun probe(throwException: Boolean = false) = try {
        baseUrlRotator.request { url ->
            val response = httpClient.get {
                attributes.put(Constants.SuppressBrokenSetup, true)
                urlFromPath(baseUrl = url, "server", "info")
            }
            val body = response.moonrakerBody<MoonrakerServerInfo>()
            body.moonrakerVersion != null
        }
    } catch (e: PrinterApiException) {
        // If we get a 401 with moonraker in the body we are positive it's moonraker.
        if (e.responseCode == 401 && (e.body.contains("moonraker", ignoreCase = true) || e.body.contains("Forced login", ignoreCase = true))) {
            if (throwException) throw InvalidApiKeyException(webUrl = baseUrlRotator.activeUrl.value.resolve("server/info")) else true
        } else {
            if (throwException) throw e else false
        }
    } catch (e: Exception) {
        Napier.w(tag = tag, message = "Failed probe with error", throwable = e)
        if (throwException) throw e else false
    }
}
