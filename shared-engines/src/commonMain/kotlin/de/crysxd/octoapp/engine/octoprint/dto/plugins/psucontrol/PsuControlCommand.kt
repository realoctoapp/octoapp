package de.crysxd.octoapp.engine.octoprint.dto.plugins.psucontrol

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal class PsuControlCommand(
    override val command: String
) : CommandBody