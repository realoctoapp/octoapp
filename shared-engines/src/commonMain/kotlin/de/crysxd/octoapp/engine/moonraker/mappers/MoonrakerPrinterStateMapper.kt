package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.printer.PrinterState
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebhooks

internal data class MoonrakerPrinterStateMapperInput(
    val stats: MoonrakerPrintStats? = null,
    val webhooks: MoonrakerWebhooks? = null,
)

internal fun MoonrakerPrinterStateMapperInput?.map() = this?.let {
    val operational = webhooks?.state == MoonrakerWebhooks.State.Ready
    PrinterState.State(
        text = webhooks?.stateMessage,
        flags = PrinterState.Flags(
            operational = operational,
            paused = operational && stats?.state == MoonrakerPrintStats.State.Paused,
            printing = operational && stats?.state == MoonrakerPrintStats.State.Printing,
            resuming = false,
            cancelling = false,
            finishing = false,
            pausing = false,
            closedOrError = webhooks?.state == MoonrakerWebhooks.State.Shutdown,
            error = webhooks?.state == MoonrakerWebhooks.State.Shutdown,
            sdReady = false,
            ready = operational && stats?.state in listOf(
                MoonrakerPrintStats.State.Standby,
                MoonrakerPrintStats.State.Complete,
                MoonrakerPrintStats.State.Error
            ),
            starting = webhooks?.state == MoonrakerWebhooks.State.Startup,
        ),
    )
} ?: PrinterState.State()