package de.crysxd.octoapp.engine.models.timelapse

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.CommonTypeParceler
import de.crysxd.octoapp.sharedcommon.InstantParceler
import kotlinx.datetime.Instant

@CommonParcelize
data class TimelapseFile(
    val name: String,
    val bytes: Long,
    @CommonTypeParceler<Instant?, InstantParceler>() val date: Instant? = null,
    val downloadPath: String? = null,
    val thumbnail: String? = null,
    val processing: Boolean = false,
    val rendering: Boolean = false,
    val recording: Boolean = false,
) : CommonParcelable