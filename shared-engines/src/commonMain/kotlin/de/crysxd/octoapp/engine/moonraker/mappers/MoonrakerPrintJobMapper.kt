package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerVirtualSdCard
import kotlinx.datetime.Instant

internal data class MoonrakerPrintJobMapperInput(
    val virtualSdCard: MoonrakerVirtualSdCard?,
    val printStats: MoonrakerPrintStats?,
    val file: MoonrakerFileMetadata?,
)

internal fun MoonrakerPrintJobMapperInput.map() = printStats?.filename?.takeIf { it.isNotBlank() }?.let { name ->
    JobInformation(
        file = JobInformation.JobFile(
            name = name.split("/").last(),
            display = name.split("/").last(),
            origin = FileOrigin.Gcode,
            path = printStats.filename, // filename == path for some reason
            size = virtualSdCard?.fileSize?.toLong() ?: file?.size ?: 0,
            date = file?.modified ?: Instant.DISTANT_PAST,
            id = file?.uuid ?: printStats.filename,
        ),
    )
}