package de.crysxd.octoapp.engine.models.commands

@Suppress("Unused")
sealed class ConnectionCommand {

    data class Connect(
        val port: String? = null,
        val baudrate: Int? = null,
        val printerProfile: String? = null,
        val save: Boolean? = null,
        val autoconnect: Boolean? = null
    ) : ConnectionCommand()

    data object Disconnect : ConnectionCommand()

}