package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.settings.OctoWebcamSettings
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject

class OctoWebcamSettingsSerialized : KSerializer<OctoWebcamSettings> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("e.crysxd.octoapp.engine.octoprint.dto.settings.OctoWebcamSettings", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): OctoWebcamSettings {
        val json = (decoder as JsonDecoder).json
        val tree = decoder.decodeJsonElement().jsonObject
        val newFormat = tree.containsKey("webcams")
        return if (newFormat) {
           json.decodeFromJsonElement(OctoWebcamSettings.OneDotNineAndNewer.serializer(), tree)
        } else {
           json.decodeFromJsonElement(OctoWebcamSettings.OneDotEightAndOlder.serializer(), tree)
        }
    }

    override fun serialize(encoder: Encoder, value: OctoWebcamSettings) {
       throw UnsupportedOperationException("Only deserialization is supported")
    }
}