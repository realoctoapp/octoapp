package de.crysxd.octoapp.engine.moonraker.dto.incoming

import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerInstant
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerNotifyStatusSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable(with = MoonrakerNotifyStatusSerializer::class)
internal data class MoonrakerNotifyStatus(
    val jsonrpc: String? = null,
    val method: String? = null,
    val items: MoonrakerPrinterObjectsQuery.StatusItems = MoonrakerPrinterObjectsQuery.StatusItems(emptyMap()),
    val eventTime: MoonrakerInstant = Instant.DISTANT_PAST,
) : MoonrakerIncomingMessage
