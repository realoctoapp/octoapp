package de.crysxd.octoapp.engine.moonraker.serializer

import de.crysxd.octoapp.engine.framework.json.EngineJson
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerInstant
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.fromEpochSeconds
import de.crysxd.octoapp.engine.moonraker.dto.incoming.MoonrakerNotifyStatus
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.double
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

internal class MoonrakerNotifyStatusSerializer : KSerializer<MoonrakerNotifyStatus> {

    private val tag = "MoonrakerNotifyStatusSerializer"
    override val descriptor = PrimitiveSerialDescriptor(serialName = "MoonrakerNotifyStatus", kind = PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): MoonrakerNotifyStatus {
        val input = decoder as JsonDecoder
        val tree = input.decodeJsonElement().jsonObject
        val jsonRpc = requireNotNull(tree["jsonrpc"]?.jsonPrimitive?.content) { "Missing jsonrpc" }
        val method = requireNotNull(tree["method"]?.jsonPrimitive?.content) { "Missing method" }
        val items = requireNotNull(tree["params"]?.jsonArray?.get(0)?.jsonObject) { "Missing params" }
        val eventTime = requireNotNull(tree["params"]?.jsonArray?.get(1)?.jsonPrimitive?.double) { "Missing event time" }

        return MoonrakerNotifyStatus(
            items = EngineJson.decodeFromJsonElement(MoonrakerPrinterObjectsQuery.StatusItems.serializer(), items),
            jsonrpc = jsonRpc,
            method = method,
            eventTime = MoonrakerInstant.fromEpochSeconds(eventTime)
        )
    }

    override fun serialize(encoder: Encoder, value: MoonrakerNotifyStatus) = throw UnsupportedOperationException("Can't serialize")
}