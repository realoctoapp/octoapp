package de.crysxd.octoapp.engine.models.remote

data class ObicoStatus(
    val printerId: String? = null,
    val name: String? = null,
    val isPro: Boolean? = null,
)