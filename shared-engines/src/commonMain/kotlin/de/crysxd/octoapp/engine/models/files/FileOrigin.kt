package de.crysxd.octoapp.engine.models.files

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize


sealed class FileOrigin : CommonParcelable {
    @CommonParcelize
    data object Gcode : FileOrigin()

    @CommonParcelize
    data class Other(val name: String) : FileOrigin()
}
