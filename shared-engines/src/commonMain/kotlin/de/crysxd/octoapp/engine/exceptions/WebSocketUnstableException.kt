package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class WebSocketUnstableException(val webSocketUrl: Url, webUrl: Url, cause: Throwable) : NetworkException(
    webUrl = webUrl,
    technicalMessage = "Failed to establish a reliable connection to $webSocketUrl",
    userFacingMessage = "OctoApp was unable to establish a WebSocket connection to receive live updates from OctoPrint. This is usually caused by an improper " +
            "reverse proxy setup. Use \"Learn more\" to see exmaple configurations.\n\n" +
            "OctoApp switched to HTTP, but this will cause the app to be slower and taking longer to connect.\n\nIf you no longer want to see " +
            "this warning, disable the WebSocket transport by setting main menu > \"Settings\" > \"OctoApp Lab\" > \"Event transport\" to HTTP.",
    originalCause = cause,
    learnMoreLink = "https://community.octoprint.org/t/reverse-proxy-configuration-examples/1107",
)