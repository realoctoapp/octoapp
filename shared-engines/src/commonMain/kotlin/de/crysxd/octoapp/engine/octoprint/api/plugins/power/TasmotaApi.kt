package de.crysxd.octoapp.engine.octoprint.api.plugins.power

import de.crysxd.octoapp.engine.api.PowerDevicesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tasmota.TasmotaCommand
import de.crysxd.octoapp.engine.octoprint.dto.plugins.tasmota.TasmotaResponse
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.post
import io.ktor.http.Url
import de.crysxd.octoapp.engine.models.power.PowerDevice as IPowerDevice

class TasmotaApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
) : PowerDevicesApi {

    override suspend fun getDevices(settings: Settings, availablePlugins: Map<String, String?>?): List<IPowerDevice> = settings.plugins.tasmota?.devices?.map {
        PowerDevice(
            owner = this,
            ip = it.ip,
            idx = it.idx,
            displayName = it.label
        )
    } ?: emptyList()

    private suspend fun sendCommand(ip: String, idx: String, command: String) = baseUrlRotator.request {
        httpClient.post {
            url(it)
            setJsonBody(TasmotaCommand(ip = ip, idx = idx, command = command))
        }
    }

    private suspend fun setOn(ip: String, idx: String, on: Boolean) {
        sendCommand(ip = ip, idx = idx, command = if (on) "turnOn" else "turnOff")
    }

    private suspend fun isOn(ip: String, idx: String) =
        sendCommand(ip = ip, idx = idx, command = "checkStatus").body<TasmotaResponse>().currentState == TasmotaResponse.State.ON

    private fun HttpRequestBuilder.url(baseUrl: Url) = urlFromPath(baseUrl, "api", "plugin", OctoPlugins.Tasmota)

    internal data class PowerDevice(
        private val owner: TasmotaApi,
        private val ip: String,
        private val idx: String,
        override val displayName: String,
    ) : IPowerDevice {
        override val id: String = "$ip:$idx"
        override val pluginId: String = OctoPlugins.Tasmota
        override val pluginDisplayName = "Tasmota"
        override val capabilities = listOf(IPowerDevice.Capability.ControlPrinterPower, IPowerDevice.Capability.Illuminate)
        override suspend fun turnOn() = owner.setOn(ip = ip, idx = idx, on = true)
        override suspend fun turnOff() = owner.setOn(ip = ip, idx = idx, on = false)
        override suspend fun isOn() = owner.isOn(ip = ip, idx = idx)
    }
}