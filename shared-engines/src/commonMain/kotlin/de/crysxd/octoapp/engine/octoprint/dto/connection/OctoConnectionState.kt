package de.crysxd.octoapp.engine.octoprint.dto.connection

import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterProfileReference
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoConnectionState(
    val current: OctoPrintConnection,
    val options: Options
) {

    @Serializable
    internal data class Options(
        val ports: List<String> = emptyList(),
        val baudrates: List<Int> = emptyList(),
        val printerProfiles: List<OctoPrinterProfileReference> = emptyList(),
        val portPreference: String? = null,
        val baudratePreference: Int? = null,
        val printerProfilePreference: String? = null,
        val autoConnect: Boolean = false
    )
}