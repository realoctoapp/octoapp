package de.crysxd.octoapp.engine.octoprint.dto.files

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class OctoFileOrigin {
    @SerialName("local")
    Local,

    @SerialName("sdcard")
    SdCard;

    fun toSerialName() = when (this) {
        Local -> "local"
        SdCard -> "sdcard"
    }
}
