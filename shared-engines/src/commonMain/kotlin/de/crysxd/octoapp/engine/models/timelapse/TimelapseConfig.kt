package de.crysxd.octoapp.engine.models.timelapse


data class TimelapseConfig(
    val type: Type = Type.Off,
    val fps: Int? = null,
    val postRoll: Int? = null,
    val save: Boolean = false,
    val minDelay: Float? = null,
    val interval: Int? = null,
    val retractionZHop: Float? = null,
) {

    enum class Type {
        Off,
        Timed,
        ZChange
    }
}