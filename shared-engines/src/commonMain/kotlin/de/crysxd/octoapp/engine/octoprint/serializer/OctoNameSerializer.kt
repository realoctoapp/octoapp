package de.crysxd.octoapp.engine.octoprint.serializer

import io.github.aakira.napier.Napier
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonNull
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonPrimitive

// We had a case where the "name" field was not a string but an array. This allows us to support both cases.
class OctoNameSerializer : KSerializer<String?> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("OctoPrint name", PrimitiveKind.STRING)

    @OptIn(ExperimentalSerializationApi::class)
    override fun serialize(encoder: Encoder, value: String?) = if (value == null) {
        encoder.encodeNull()
    } else {
        encoder.encodeString(value = value)
    }

    override fun deserialize(decoder: Decoder): String? = try {
        when (val element = (decoder as JsonDecoder).decodeJsonElement()) {
            is JsonNull -> null
            is JsonArray -> element[0].jsonPrimitive.content
            is JsonPrimitive -> element.content
            else -> toString()
        }
    } catch (e: Exception) {
        Napier.e(tag = "OctoNameSerializer", message = "Failed to decode name", throwable = e)
        null
    }
}