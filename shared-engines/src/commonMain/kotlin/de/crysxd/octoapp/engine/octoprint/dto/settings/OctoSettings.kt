package de.crysxd.octoapp.engine.octoprint.dto.settings

import de.crysxd.octoapp.engine.framework.json.SafeFloat
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.serializer.OctoNameSerializer
import de.crysxd.octoapp.engine.octoprint.serializer.OctoRelaySerializer
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoSettings(
    val webcam: OctoWebcamSettings = OctoWebcamSettings.OneDotNineAndNewer(),
    val plugins: PluginSettingsGroup = PluginSettingsGroup(),
    val temperature: TemperatureSettings = TemperatureSettings(),
    val terminalFilters: List<TerminalFilter> = emptyList(),
    val appearance: Appearance = Appearance(),
) {

    @Serializable
    data class Appearance(
        @Serializable(with = OctoNameSerializer::class) val name: String? = null,
        val color: String = "default"
    )

    @Serializable
    data class TerminalFilter(
        val name: String,
        val regex: String
    )

    @Serializable
    data class TemperatureSettings(
        val profiles: List<TemperatureProfile> = emptyList()
    )

    @Serializable
    data class TemperatureProfile(
        val bed: SafeFloat,
        val chamber: SafeFloat,
        val extruder: SafeFloat,
        val name: String
    )

    interface PluginSettings

    @Serializable
    data class PluginSettingsGroup(
        @SerialName(OctoPlugins.GcodeViewer) val gcodeViewer: GcodeViewerSettings? = null,
        @SerialName(OctoPlugins.OctoEverywhere) val octoEverywhere: OctoEverywhere? = null,
        @SerialName(OctoPlugins.Ngrok) val ngrok: Ngrok? = null,
        @SerialName(OctoPlugins.Obico) val obico: Obico? = null,
        @SerialName(OctoPlugins.OctoApp) val octoAppCompanion: OctoAppCompanion? = null,
        @SerialName(OctoPlugins.MultiCam) val multiCam: MultiCam? = null,
        @SerialName(OctoPlugins.SpoolManager) val spoolManager: SpoolManager? = null,
        @SerialName(OctoPlugins.Spoolman) val spoolman: Spoolman? = null,
        @SerialName(OctoPlugins.FilamentManager) val filamentManager: FilamentManager? = null,
        @SerialName(OctoPlugins.Discovery) val discovery: Discovery? = null,
        @SerialName(OctoPlugins.UploadAnything) val uploadAnything: UploadAnything? = null,
        @SerialName(OctoPlugins.Mmu2FilamentSelect) val mmu2FilamentSelect: Mmu2FilamentSelect? = null,
        @SerialName(OctoPlugins.PrusaMmu) val prusaMmu: PrusaMmu? = null,
        @SerialName(OctoPlugins.CancelObject) val cancelObject: CancelObject? = null,
        @SerialName(OctoPlugins.PsuControl) val psuControl: PsuControl? = null,
        @SerialName(OctoPlugins.Wled) val wled: Wled? = null,
        @SerialName(OctoPlugins.OctoCam) val octoCam: OctoCam? = null,
        @SerialName(OctoPlugins.OctoLight) val octoLight: OctoLight? = null,
        @SerialName(OctoPlugins.OctoLightHA) val octoLightHA: OctoLightHA? = null,
        @SerialName(OctoPlugins.Ophom) val ophom: Ophom? = null,
        @SerialName(OctoPlugins.OctoHue) val octoHue: OctoHue? = null,
        @SerialName(OctoPlugins.MyStromSwitch) val myStrom: MyStrom? = null,
        @SerialName(OctoPlugins.Ws281xLedStatus) val wS281x: WS281x? = null,
        @SerialName(OctoPlugins.Enclosure) val enclosure: Enclosure? = null,
        @SerialName(OctoPlugins.GpioControl) val gpioControl: GpioControl? = null,
        @Serializable(with = OctoRelaySerializer::class) @SerialName(OctoPlugins.OctoRelay) val octoRelay: OctoRelay? = null,
        @SerialName(OctoPlugins.Tasmota) val tasmota: Tasmota? = null,
        @SerialName(OctoPlugins.TpLinkSmartPlug) val tpLinkSmartPlug: TpLinkSmartPlug? = null,
        @SerialName(OctoPlugins.IkeaTradfri) val tradfri: Tradfri? = null,
        @SerialName(OctoPlugins.Tuya) val tuya: Tuya? = null,
        @SerialName(OctoPlugins.UsbRelayControl) val usbRelayControl: UsbRelayControl? = null,
        @SerialName(OctoPlugins.WemoSwitch) val wemoSwitch: WemoSwitch? = null,
        @SerialName(OctoPlugins.BedLevelVisualizer) val bedLevelVisualizer: BedLevelVisualizer? = null,
        @SerialName(OctoPlugins.OctoKlipper) val octoKlipper: OctoKlipper? = null,
    )

    @Serializable
    data class GcodeViewerSettings(
        val mobileSizeThreshold: SafeFloat = 0f,
        val sizeThreshold: SafeFloat = 0f
    ) : PluginSettings

    @Serializable
    data class Tradfri(
        @SerialName("selected_devices") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val id: String? = null,
            val name: String? = null,
        )
    }

    @Serializable
    data class Tuya(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val label: String,
        )
    }

    @Serializable
    data class TpLinkSmartPlug(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class WemoSwitch(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class Tasmota(
        @SerialName("arrSmartplugs") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val idx: String,
            val label: String,
        )
    }

    @Serializable
    data class GpioControl(
        @SerialName("gpio_configurations") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val name: String,
        )
    }

    @Serializable
    data class Enclosure(
        @SerialName("rpi_outputs") val outputs: List<Output> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Output(
            val label: String,
            @SerialName("output_type") val type: String,
            @SerialName("index_id") val indexId: Int,
        )
    }

    @Serializable
    data class UsbRelayControl(
        @SerialName("usbrelay_configurations") val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {
        @Serializable
        data class Device(
            val name: String,
        )
    }

    @Serializable
    data class OctoRelay(
        val devices: List<Device> = emptyList()
    ) : Settings.PluginSettings {

        @Serializable
        data class Device(
            val id: String,
            val active: Boolean,
            val labelText: String,
        )
    }

    @Serializable
    class WS281x : PluginSettings

    @Serializable
    class Wled : PluginSettings

    @Serializable
    class OctoCam : PluginSettings

    @Serializable
    class OctoLight : PluginSettings

    @Serializable
    class OctoLightHA : PluginSettings

    @Serializable
    class Ophom : PluginSettings

    @Serializable
    class OctoHue : PluginSettings

    @Serializable
    class MyStrom : PluginSettings

    @Serializable
    class PsuControl : PluginSettings

    @Serializable
    class CancelObject : PluginSettings

    @Serializable
    class Ngrok(
        @SerialName("auth_name") val authName: String? = null,
        @SerialName("auth_pass") val authPassword: String? = null,
    ) : PluginSettings

    @Serializable
    class Obico : PluginSettings

    @Serializable
    class SpoolManager : PluginSettings

    @Serializable
    data class Spoolman(
        val selectedSpoolIds: Map<String, SelectedSpool>? = null
    ) : Settings.PluginSettings {
        @Serializable
        data class SelectedSpool(
            val spoolId: String? = null
        )
    }

    @Serializable
    class FilamentManager : PluginSettings

    @Serializable
    data class OctoAppCompanion(
        @SerialName("encryptionKey") val encryptionKey: String?,
        @SerialName("version") val version: String?,
    ) : PluginSettings

    @Serializable
    data class MultiCam(
        @SerialName("multicam_profiles") val profiles: List<Profile> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Profile(
            val name: String? = null,
            @SerialName("URL") val streamUrl: String? = null,
            val flipH: Boolean = false,
            val flipV: Boolean = false,
            val rotate90: Boolean = false,
            val streamRatio: String = "16:9",
            val snapshotUrl: String? = null,
        )
    }

    @Serializable
    data class Discovery(
        @SerialName("upnpUuid") val uuid: String?
    ) : PluginSettings

    @Serializable
    data class UploadAnything(
        @SerialName("allowed") val allowedExtensions: List<String> = emptyList()
    ) : PluginSettings

    @Serializable
    data class OctoEverywhere(
        @SerialName("PrinterKey") val printerKey: String? = null
    ) : PluginSettings

    @Serializable
    data class Mmu2FilamentSelect(
        val filament1: String? = null,
        val filament2: String? = null,
        val filament3: String? = null,
        val filament4: String? = null,
        val filament5: String? = null,
        val labelSource: LabelSource = LabelSource.Manual,
    ) : PluginSettings {
        @Serializable
        enum class LabelSource {
            @SerialName("manual")
            Manual,

            @SerialName("filamentManager")
            FilamentManager,

            @SerialName("spoolManager")
            SpoolManager,
        }
    }

    @Serializable
    data class OctoKlipper(
        val macros: List<Macro>? = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Macro(
            val macro: String,
            val name: String?,
        )
    }

    @Serializable
    data class PrusaMmu(
        val filament: List<Filament>? = null,
        val filamentSource: FilamentSource = FilamentSource.PrusaMmu,
    ) : PluginSettings {

        @Serializable
        data class Filament(
            val color: HexColor? = null,
            val enabled: Boolean = true,
            val id: Int,
            val name: String = "$id",
        )

        @Serializable
        enum class FilamentSource {
            @SerialName("prusammu")
            PrusaMmu,

            @SerialName("filamentManager")
            FilamentManager,

            @SerialName("spoolManager")
            SpoolManager,
        }
    }

    @Serializable
    data class BedLevelVisualizer(
        val command: String? = null,
        @SerialName("stored_mesh") val mesh: List<List<String?>>? = null,
        @SerialName("stored_mesh_x") val meshX: List<String?>? = null,
        @SerialName("stored_mesh_y") val meshY: List<String?>? = null,
        @SerialName("graph_z_limits") val graphZLimits: String? = null,
        @SerialName("colorscale") val colorscale: String? = null,
    ) : PluginSettings
}