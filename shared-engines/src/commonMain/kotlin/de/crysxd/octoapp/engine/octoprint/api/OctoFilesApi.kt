package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.api.FilesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.files.FileReference
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoFileCommand
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileList
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileObject
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import de.crysxd.octoapp.sharedcommon.Constants
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.onDownload
import io.ktor.client.plugins.onUpload
import io.ktor.client.request.delete
import io.ktor.client.request.forms.InputProvider
import io.ktor.client.request.forms.formData
import io.ktor.client.request.forms.submitFormWithBinaryData
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.prepareGet
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders
import io.ktor.utils.io.ByteReadChannel

internal class OctoFilesApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val eventSink: EventSink,
) : FilesApi {


    override suspend fun getAllFiles(origin: FileOrigin) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "files", origin.map().toSerialName()) {
                parameter("recursive", "true")
            }
        }.body<OctoFileList>()
    }.map(origin)

    override suspend fun getFile(origin: FileOrigin, path: String, isDirectory: Boolean): FileObject = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "files", origin.map().toSerialName(), *path.splitIntoSegments())
        }.body<OctoFileObject>()
    }.map()


    override suspend fun executeFileCommand(file: FileReference, command: FileCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "files", file.origin.map().toSerialName(), *file.path.splitIntoSegments())
            when (command) {
                is FileCommand.CopyFile -> setJsonBody(OctoFileCommand.CopyFile(destination = command.destination))
                is FileCommand.MoveFile -> setJsonBody(OctoFileCommand.MoveFile(destination = command.destination))
                is FileCommand.PrintFile -> setJsonBody(OctoFileCommand.SelectFile(print = true))
            }
        }
    }.let {
        when (command) {
            is FileCommand.CopyFile -> Unit
            is FileCommand.MoveFile -> Unit
            is FileCommand.PrintFile -> if (file is FileObject.File) {
                eventSink.injectInterpolatedEvent { Message.Event.FileSelected(origin = file.origin, path = file.path, name = file.name) }
                eventSink.injectInterpolatedEvent { Message.Event.PrintStarted }
                eventSink.injectInterpolatedPrintStart(file)
            }
        }
    }

    override suspend fun deleteFile(fileObject: FileReference) = baseUrlRotator.request<Unit> {
        httpClient.delete {
            urlFromPath(baseUrl = it, "api", "files", fileObject.origin.map().toSerialName(), *fileObject.path.splitIntoSegments())
        }
    }


    override suspend fun createFolder(parent: FileReference.Folder, name: String) = baseUrlRotator.request<Unit> {
        httpClient.submitFormWithBinaryData(
            formData {
                append(key = "foldername", value = name)
                append(key = "path", value = parent.path)
            }
        ) {
            urlFromPath(baseUrl = it, "api", "files", parent.origin.map().toSerialName())
        }
    }

    override suspend fun uploadFile(parent: FileReference.Folder, input: InputProvider, name: String, progressUpdate: (Float) -> Unit) =
        baseUrlRotator.request<Unit> {
            httpClient.submitFormWithBinaryData(
                formData {
                    append(
                        key = "file",
                        value = input,
                        headers = Headers.build {
                            append(HttpHeaders.ContentDisposition, "filename=${name}")
                            append(HttpHeaders.ContentType, ContentType.Application.OctetStream.contentType)
                        }
                    )
                    append(key = "path", value = parent.path)
                }
            ) {
                urlFromPath(baseUrl = it, "api", "files", parent.origin.map().toSerialName())
                onUpload { bytesSentTotal, contentLength ->
                    progressUpdate(if (contentLength > 0) (bytesSentTotal / contentLength.toFloat()) else -1f)
                }
                attributes.put(Constants.SuppressLogging, true)
            }
        }

    override suspend fun <T> downloadFile(file: FileReference.File, progressUpdate: (Float) -> Unit, consume: suspend (ByteReadChannel) -> T): T =
        baseUrlRotator.request {
            httpClient.prepareGet {
                urlFromPath(baseUrl = it, "downloads", "files", file.origin.map().toSerialName(), *file.path.splitIntoSegments())
                onDownload { bytesSentTotal, contentLength ->
                    val total = contentLength.takeIf { it > 0 } ?: file.size ?: return@onDownload progressUpdate(-1f)
                    val progressPercent = bytesSentTotal / total.toFloat()
                    progressUpdate(progressPercent.coerceIn(0f..1f))
                }

                // There is an issue with KTOR cache as it saves the entire download in memory at first, we can't have this of course
                // with potentially huge files. Also prevent logging on this request as logging will clog up memory as well.
                header(HttpHeaders.CacheControl, "no-store, no-cache")
                attributes.put(Constants.SuppressLogging, true)
            }.execute { response ->
                // We must use this execute function to prevent the entire body to be read into memory before the function returns
                consume(response.bodyAsChannel())
            }
        }

    private fun String.splitIntoSegments() = split("/").toTypedArray()

}