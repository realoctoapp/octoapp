package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerHeaters(
    @SerialName("available_heaters") val availableHeaters: List<String?>? = null,
    @SerialName("available_sensors") val availableSensors: List<String?>? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        availableHeaters = availableHeaters ?: (previous as? MoonrakerHeaters)?.availableHeaters,
        availableSensors = availableSensors ?: (previous as? MoonrakerHeaters)?.availableSensors,
    )
}
