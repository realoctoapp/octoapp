package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerGetDeviceParams(
    @SerialName("device") val device: String,
) : MoonrakerRpcParams