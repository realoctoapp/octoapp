package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoConnectionCommand {

    @Serializable
    data class Connect(
        val port: String? = null,
        val baudrate: Int? = null,
        val printerProfile: String? = null,
        val save: Boolean? = null,
        val autoconnect: Boolean? = null
    ) : OctoConnectionCommand()

    @Serializable
    data object Disconnect : OctoConnectionCommand()

}