package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerDatabaseList(
    @SerialName("namespaces") val namepsaces: List<String>? = null,
)