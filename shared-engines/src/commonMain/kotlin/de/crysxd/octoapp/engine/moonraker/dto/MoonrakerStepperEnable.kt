package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerStepperEnable(
    @SerialName("steppers") val steppers: Map<String, Boolean>? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        steppers = steppers ?: (previous as? MoonrakerStepperEnable)?.steppers
    )
}