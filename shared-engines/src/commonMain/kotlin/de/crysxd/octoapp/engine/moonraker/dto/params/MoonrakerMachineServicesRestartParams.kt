package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerMachineServicesRestartParams(
    @SerialName("service") val service: String,
) : MoonrakerRpcParams