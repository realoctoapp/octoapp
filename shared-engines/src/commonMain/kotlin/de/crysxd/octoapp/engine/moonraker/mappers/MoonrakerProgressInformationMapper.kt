package de.crysxd.octoapp.engine.moonraker.mappers

import de.crysxd.octoapp.engine.models.job.ProgressInformation
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMove
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerVirtualSdCard
import kotlin.math.roundToLong

internal data class MoonrakerProgressInformationMapperInput(
    val virtualSdCard: MoonrakerVirtualSdCard?,
    val printStats: MoonrakerPrintStats?,
    val activeFile: MoonrakerFileMetadata?,
    val gcodeMove: MoonrakerGcodeMove?,
)

internal fun MoonrakerProgressInformationMapperInput.map(): ProgressInformation {
    val printTime = printStats?.printDuration ?: 0f
    val printTimeLeft = if (printTime > 0) {
        listOfNotNull(
            // Time left based on progress and spent time
            calcTimeLeftWithProgress(printTime = printTime, progress = virtualSdCard?.progress?.toFloat()),
            // Time left based on filament spent and total and spent time
            calcTimeLeftWithProgress(printTime = printTime, progress = activeFile?.filamentTotal?.let { total -> printStats?.filamentUsed?.div(total) }),
            // Time left based on slicer time estimate and spend time
            calcTimeLeftWithProgress(printTime = printTime, progress = activeFile?.estimatedTime?.let { total -> printStats?.printDuration?.div(total) }),
        ).average().takeIf { it.isFinite() }?.toFloat()
    } else {
        // We can't calculate this yet, rely on slicer
        activeFile?.estimatedTime
    }

    return ProgressInformation(
        completion = virtualSdCard?.progress?.times(100)?.toFloat(),
        filepos = virtualSdCard?.filePosition?.roundToLong(),
        printTime = printTime.roundToLong(),
        printTimeLeft = printTimeLeft?.times(1f / (gcodeMove?.speedFactor ?: 1f))?.roundToLong(),
        printTimeLeftOrigin = null,
    )
}

private fun calcTimeLeftWithProgress(printTime: Float, progress: Float?): Float? = progress?.let { printTime / it }?.minus(printTime)
