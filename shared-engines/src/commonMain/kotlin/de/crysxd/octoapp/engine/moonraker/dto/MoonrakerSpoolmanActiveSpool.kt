package de.crysxd.octoapp.engine.moonraker.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoonrakerSpoolmanActiveSpool(
    @SerialName("spool_id") val spoolId: Int? = null,
)