package de.crysxd.octoapp.engine.framework.json

import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerLegacyWebcamDatabase
import de.crysxd.octoapp.engine.moonraker.serializer.MoonrakerLegacyWebcamDatabaseSerializer
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileObject
import de.crysxd.octoapp.engine.octoprint.serializer.OctoFileObjectSerializer
import de.crysxd.octoapp.sharedcommon.utils.HexColor
import de.crysxd.octoapp.sharedcommon.utils.HexColorSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule

@OptIn(ExperimentalSerializationApi::class)
internal val EngineJson = Json {
    prettyPrint = false
    isLenient = true
    ignoreUnknownKeys = true
    encodeDefaults = true
    explicitNulls = false
    coerceInputValues = true
    allowSpecialFloatingPointValues = true

    serializersModule = SerializersModule {
        contextual(Instant::class, InstantSerializer())
        contextual(HexColor::class, HexColorSerializer())
        contextual(MoonrakerLegacyWebcamDatabase::class, MoonrakerLegacyWebcamDatabaseSerializer())
        contextual(OctoFileObject::class, OctoFileObjectSerializer())
    }
}