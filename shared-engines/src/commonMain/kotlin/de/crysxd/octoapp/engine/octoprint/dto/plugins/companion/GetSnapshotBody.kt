package de.crysxd.octoapp.engine.octoprint.dto.plugins.companion

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
data class GetSnapshotBody(
    val size: Int,
    val webcamIndex: Int,
) : CommandBody {
    override val command: String = "getWebcamSnapshot"
}