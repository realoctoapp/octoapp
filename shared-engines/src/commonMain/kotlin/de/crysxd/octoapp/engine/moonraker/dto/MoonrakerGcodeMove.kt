package de.crysxd.octoapp.engine.moonraker.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MoonrakerGcodeMove(
    @SerialName("absolute_coordinates") val absoluteCoordinates: Boolean? = null,
    @SerialName("absolute_extrude") val absoluteExtrude: Boolean? = null,
    @SerialName("extrude_factor") val extrudeFactor: Float? = null,
    @SerialName("gcode_position") val gcodePosition: List<Float?>? = null,
    @SerialName("homing_origin") val homingOrigin: List<Float?>? = null,
    @SerialName("position") val position: List<Float?>? = null,
    @SerialName("speed") val speed: Float? = null,
    @SerialName("speed_factor") val speedFactor: Float? = null
) : MoonrakerStatusItem {
    override fun backFillWithPrevious(previous: MoonrakerStatusItem) = copy(
        absoluteCoordinates = absoluteCoordinates ?: (previous as? MoonrakerGcodeMove)?.absoluteCoordinates,
        absoluteExtrude = absoluteExtrude ?: (previous as? MoonrakerGcodeMove)?.absoluteExtrude,
        extrudeFactor = extrudeFactor ?: (previous as? MoonrakerGcodeMove)?.extrudeFactor,
        gcodePosition = gcodePosition ?: (previous as? MoonrakerGcodeMove)?.gcodePosition,
        homingOrigin = homingOrigin ?: (previous as? MoonrakerGcodeMove)?.homingOrigin,
        position = position ?: (previous as? MoonrakerGcodeMove)?.position,
        speed = speed ?: (previous as? MoonrakerGcodeMove)?.speed,
        speedFactor = speedFactor ?: (previous as? MoonrakerGcodeMove)?.speedFactor,
    )
}