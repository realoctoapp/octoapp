package de.crysxd.octoapp.engine.models.tune

data class TuneState(
    val isValuesImprecise: Boolean = false,
    val canResetOffsets: Boolean = false,
    val print: Print? = null,
    val offsets: Offsets? = null,
    val pressureAdvance: PressureAdvance? = null,
    val machineLimits: MachineLimits? = null,
    val fans: List<Fan> = emptyList()
) {

    data class Print(
        val flowRate: Float? = null,
        val feedRate: Float? = null,
    )

    data class Offsets(
        val z: Float? = null,
    )

    data class PressureAdvance(
        val pressureAdvance: Float? = null,
        val smoothTime: Float? = null,
    )

    data class MachineLimits(
        val maxVelocity: Float? = null,
        val maxSquareCornerVelocity: Float? = null,
        val maxAcceleration: Float? = null,
        val maxAccelerationToDeceleration: Float? = null,
    )

    data class Fan(
        val component: String,
        val label: String,
        val speed: Float? = null,
        val isPartCoolingFan: Boolean,
    )
}