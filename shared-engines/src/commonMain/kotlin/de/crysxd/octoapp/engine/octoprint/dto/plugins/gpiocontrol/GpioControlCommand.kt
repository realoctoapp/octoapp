package de.crysxd.octoapp.engine.octoprint.dto.plugins.gpiocontrol

import kotlinx.serialization.Serializable

@Serializable
data class GpioControlCommand(val command: String, val id: Int)