package de.crysxd.octoapp.engine.moonraker.dto.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
data class MoonrakerSpoolmanProxyParams(
    @SerialName("use_v2_response") val v2Response: Boolean,
    @SerialName("request_method") val requestMethod: String?,
    @SerialName("path") val path: String?,
    @SerialName("query") val query: String? = null,
    @SerialName("body") val body: JsonObject? = null,
) : MoonrakerRpcParams