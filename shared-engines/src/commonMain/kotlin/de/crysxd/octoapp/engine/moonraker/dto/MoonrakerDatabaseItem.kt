package de.crysxd.octoapp.engine.moonraker.dto

import de.crysxd.octoapp.engine.framework.json.EngineJson
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement

@Serializable
internal data class MoonrakerDatabaseItem(
    @SerialName("key") val key: String? = null,
    @SerialName("namespace") val namespace: String? = null,
    @SerialName("value") val value: JsonObject
) {
    inline fun <reified T : Any> parseValue() = EngineJson.decodeFromJsonElement<T>(value)
}