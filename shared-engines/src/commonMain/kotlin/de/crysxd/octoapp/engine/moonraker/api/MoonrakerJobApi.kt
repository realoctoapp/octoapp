package de.crysxd.octoapp.engine.moonraker.api

import de.crysxd.octoapp.engine.api.JobApi
import de.crysxd.octoapp.engine.models.commands.JobCommand
import de.crysxd.octoapp.engine.models.job.Job
import de.crysxd.octoapp.engine.models.job.JobInformation
import de.crysxd.octoapp.engine.moonraker.Constants
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerEventHandler
import de.crysxd.octoapp.engine.moonraker.connection.MoonrakerRequestHandler
import de.crysxd.octoapp.engine.moonraker.connection.sendRequest
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerFileMetadata
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerGcodeMove
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerJobList
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrintStats
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerPrinterObjectsQuery
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerVirtualSdCard
import de.crysxd.octoapp.engine.moonraker.dto.MoonrakerWebhooks
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerGetJobListParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerPrinterObjectsQueryParams
import de.crysxd.octoapp.engine.moonraker.dto.params.MoonrakerServerFilesMetadataParams
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerPrintJobMapperInput
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerPrinterStateMapperInput
import de.crysxd.octoapp.engine.moonraker.mappers.MoonrakerProgressInformationMapperInput
import de.crysxd.octoapp.engine.moonraker.mappers.map
import io.github.aakira.napier.Napier
import kotlinx.datetime.Instant

class MoonrakerJobApi internal constructor(
    private val requestHandler: MoonrakerRequestHandler,
    private val eventHandler: MoonrakerEventHandler,
) : JobApi {

    override suspend fun executeJobCommand(command: JobCommand) = eventHandler.alterStatusWhile(
        alter = { current ->
            Napier.v(tag = "MoonrakerJobApi", message = "Altering current message with command: $command")
            current.copy(
                state = current.state.copy(
                    flags = current.state.flags.copy(
                        cancelling = command == JobCommand.CancelJobCommand || current.state.flags.cancelling,
                        pausing = command == JobCommand.PauseJobCommand || current.state.flags.pausing,
                        resuming = command == JobCommand.ResumeJobCommand || current.state.flags.resuming,
                    )
                )
            )
        },
        action = {
            when (command) {
                JobCommand.CancelJobCommand -> {
                    // First pause then cancel so we make use of the pause macro to move the print head away
                    requestHandler.sendRequest<Unit>(method = "printer.print.pause", longTimeout = true)
                    requestHandler.sendRequest<Unit>(method = "printer.print.cancel", longTimeout = true)
                }

                JobCommand.PauseJobCommand -> requestHandler.sendRequest<Unit>(method = "printer.print.pause", longTimeout = true)
                JobCommand.ResumeJobCommand -> requestHandler.sendRequest<Unit>(method = "printer.print.resume", longTimeout = true)
            }
        }
    )

    override suspend fun getJob(): Job {
        //region Query objects
        val status = requestHandler.sendRequest<MoonrakerPrinterObjectsQuery>(
            method = "printer.objects.query",
            params = MoonrakerPrinterObjectsQueryParams(
                objects = mapOf(
                    Constants.PrinterObjects.PrintStats to null,
                    Constants.PrinterObjects.VirtualSdCard to null,
                    Constants.PrinterObjects.Webhooks to null,
                    Constants.PrinterObjects.GcodeMove to null,
                )
            )
        ).status
        //endregion
        //region Query file if needed
        val virtualSdCard = status?.get(Constants.PrinterObjects.VirtualSdCard) as? MoonrakerVirtualSdCard
        val printStats = status?.get(Constants.PrinterObjects.PrintStats) as? MoonrakerPrintStats
        val gcodeMove = status?.get(Constants.PrinterObjects.GcodeMove) as? MoonrakerGcodeMove
        val webhooks = status?.get(Constants.PrinterObjects.Webhooks) as? MoonrakerWebhooks
        val filename = printStats?.filename
        val activeFile = if (!filename.isNullOrBlank()) {
            requestHandler.sendRequest<MoonrakerFileMetadata>("server.files.metadata", MoonrakerServerFilesMetadataParams(filename = filename))
        } else {
            null
        }
        //endregion
        //region Map
        return Job(
            progress = MoonrakerProgressInformationMapperInput(
                virtualSdCard = virtualSdCard,
                printStats = printStats,
                activeFile = activeFile,
                gcodeMove = gcodeMove
            ).map(),
            info = MoonrakerPrintJobMapperInput(
                virtualSdCard = virtualSdCard,
                printStats = printStats,
                file = activeFile,
            ).map() ?: JobInformation(),
            state = MoonrakerPrinterStateMapperInput(
                stats = printStats,
                webhooks = webhooks,
            ).map()
        )
        //endregion
    }

    suspend fun getJobHistory(
        start: Int = 0,
        limit: Int = 50,
        since: Instant = Instant.fromEpochSeconds(0),
        before: Instant = Instant.fromEpochSeconds(4102441200),
        ascending: Boolean = false,
    ) = requestHandler.sendRequest<MoonrakerJobList>(
        method = "server.history.list",
        params = MoonrakerGetJobListParams(
            start = start,
            limit = limit,
            order = if (ascending) "asc" else "desc",
            before = before,
            since = since,
        )
    )
}
