package de.crysxd.octoapp.engine.octoprint.dto.plugins.tradfri

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class TradfriCommand(
    override val command: String,
    val dev: Device
) : CommandBody {
    @Serializable
    data class Device(
        val id: String,
        val name: String
    )
}

