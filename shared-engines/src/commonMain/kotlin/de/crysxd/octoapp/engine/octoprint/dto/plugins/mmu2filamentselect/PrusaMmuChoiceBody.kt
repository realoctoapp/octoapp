package de.crysxd.octoapp.engine.octoprint.dto.plugins.mmu2filamentselect

import de.crysxd.octoapp.engine.octoprint.CommandBody
import kotlinx.serialization.Serializable

@Serializable
internal data class PrusaMmuChoiceBody(
    val choice: Int,
) : CommandBody {
    override val command: String = "select"
}