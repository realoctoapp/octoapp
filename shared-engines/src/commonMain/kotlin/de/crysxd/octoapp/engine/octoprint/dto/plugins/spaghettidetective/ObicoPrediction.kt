package de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ObicoPrediction(
    @SerialName("normalized_p") val normalized: Float,
)