package de.crysxd.octoapp.engine.moonraker.ext

fun String.convertToMoonrakerComponentLabel(): String = this
    // Remove component type
    .split(" ")
    .last()
    // Replace _ with spaces to create words
    .replace("_", " ")
    .split(" ")
    // Replace the first char of each word with the upper case version
    .joinToString(" ") { part ->
        part.lowercase().replaceFirstChar { it.uppercaseChar() }
    }