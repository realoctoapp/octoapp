package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterProfile

internal fun OctoPrinterProfile.map() = PrinterProfile(
    id = id,
    current = current,
    axes = PrinterProfile.Axes(
        e = axes.e.map(),
        x = axes.x.map(),
        y = axes.y.map(),
        z = axes.z.map(),
    ),
    name = name,
    model = model,
    default = default,
    extruders = if (extruder.sharedNozzle) {
        listOf(
            PrinterProfile.Extruder(
                nozzleDiameter = extruder.nozzleDiameter,
                componentName = "tool0",
                extruderComponents = (0 until extruder.count).map { index -> "tool$index" }
            )
        )
    } else {
        (0 until extruder.count).map { index ->
            PrinterProfile.Extruder(
                nozzleDiameter = extruder.nozzleDiameter,
                componentName = "tool$index",
                extruderComponents = listOf("tool$index"),
            )
        }
    },
    heatedBed = heatedBed,
    heatedChamber = heatedChamber,
    volume = volume.let { volume ->
        PrinterProfile.Volume(
            depth = volume.depth,
            height = volume.height,
            width = volume.width,
            origin = when (volume.origin) {
                OctoPrinterProfile.Origin.LowerLeft -> PrinterProfile.Origin.LowerLeft
                OctoPrinterProfile.Origin.Center -> PrinterProfile.Origin.Center
            },
            formFactor = when (volume.formFactor) {
                OctoPrinterProfile.FormFactor.Circular -> PrinterProfile.FormFactor.Circular
                OctoPrinterProfile.FormFactor.Rectangular -> PrinterProfile.FormFactor.Rectangular
            },
            boundingBox = PrinterProfile.CustomBox(
                xMax = volume.customBox?.xMax ?: if (volume.origin == OctoPrinterProfile.Origin.Center) (volume.width / 2) else volume.width,
                xMin = volume.customBox?.xMin ?: if (volume.origin == OctoPrinterProfile.Origin.Center) -(volume.width / 2) else 0f,
                yMax = volume.customBox?.yMax ?: if (volume.origin == OctoPrinterProfile.Origin.Center) (volume.depth / 2) else volume.depth,
                yMin = volume.customBox?.yMin ?: if (volume.origin == OctoPrinterProfile.Origin.Center) -(volume.depth / 2) else 0f,
                zMin = volume.customBox?.zMin ?: 0f,
                zMax = volume.customBox?.zMax ?: volume.height
            )
        )
    }
)

private fun OctoPrinterProfile.Axis.map() = PrinterProfile.Axis(inverted = inverted, speed = speed)
